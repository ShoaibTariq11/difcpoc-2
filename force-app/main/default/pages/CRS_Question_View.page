<apex:page standardController="CRS_Question__c" sidebar="false" >

<style>

.bPageBlock .detailList .labelCol 
{
width:33% !important;
}
.helptext
{
font-size:11px;
color:#707070;
padding-left: 10px;
}
.helptextp
{
font-size:11px;
color:#707070;
padding-left: 0px;
}

.helptext h2
{
font-size:15px;
color:#707070;
font-weight: 900;

}

.helptext ul
{
padding-left: 40px;
}



 /* This is for the full screen DIV */
    .popupBackground {
        /* Background color */
        background-color:black;
        opacity: 0.20;
        filter: alpha(opacity = 20);
    
        /* Dimensions */
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        z-index: 998;
        position: absolute;
        
        /* Mouse */
        cursor:wait;
    }
 
    /* This is for the message DIV */
    .PopupPanel {
        /* Background color */
        border: solid 2px blue;
        background-color: white;
 
        /* Dimensions */
        left: 50%;
        width: 200px;
        margin-left: -100px;
        top: 50%;
        height: 50px;
        margin-top: -25px;
        z-index: 999;
        position: fixed;
        
        /* Mouse */
        cursor:pointer;
        
</style>



<apex:pageMessages ></apex:pageMessages>
<apex:sectionHeader title="CRS COMPLIANCE FORM"/>
 <apex:form >
 
 
  
        
     <p>
  
<p> 
The purpose of this Common Reporting Standard (CRS) Compliance Form is to provide the Registrar of Companies with information relating to the reporting and due diligence procedures of Reporting Financial Institutions under the<a href="https://www.difc.ae/files/7915/2162/8256/Common_Reporting_Standard_Law_DIFC_Law_No._2_of_2018.pdf" target="_blank"> Common Reporting Standards Law 2018</a> and  <a href="https://www.difc.ae/files/6715/2327/6154/Common_Reporting_Standard_Regulations.pdf" target="_blank"> Regulations</a>, in respect to the reporting period ending 31 December 2019. The CRS Law and Regulations primarily apply to financial services entities that are regulated by the DFSA, but certain non-financial 
services entities may be required to report under the DIFC CRS and should therefore complete this form as well. </p>

<p>Your CRS filing for the reporting period ending 31 December 2019 can be filed after completing this form. This form and your CRS and FATCA (where applicable) filing must be submitted by <b>30 June 2020</b>.</p>
<p>Further information on CRS can be found in the <a target="_blank" herf="https://www.difc.ae/files/8915/9007/2577/CRS_Guidance_Notes.pdf"> Guidance</a> and the DIFC CRS <a href="https://www.difc.ae/business/operating/common-reporting-standard/"  target="_blank"> webpage</a> . A sample of the CRS Compliance Form for Financial entities can be viewed <a target="_blank" href="https://difc--c.eu7.visual.force.com/resource/1590936153000/CRS_Form_FS_FINA">here</a>, while the Non-Financial entities form can be can be viewed <a target="_blank" href="https://difc--c.eu7.visual.force.com/resource/1590936206000/CRS_Form_fonFS_FINAL">here</a> .</p>


     
     </p>
     

 <!---mindetail--->
<apex:pageBlock mode="mindetail">

<apex:outputField value="{!CRS_Question__c.Company_Type__c}"  style="display:none;" />
<apex:pageBlockSection columns="1" title="Designated Contact Person" id="MainDesignated" collapsible="false">
<p class="helptext">
The Designated Contact Person can be anyone who has the authority to respond to queries relating to this form and provide further clarification or documentation (if required).
<br />
Examples of a <b>Designated Contact Person</b> may include the entity's:
<ul class="helptext">

<li>CEO, General Manager or Senior Executive Officer;</li>
<li> Director or Authorised Signatories;</li>
<li> Compliance Officer or a member of its in-house legal team</li>

</ul>

</p>

<!----5----> <apex:outputField value="{!CRS_Question__c.Title__c}"/>
<!----6----> <apex:outputField value="{!CRS_Question__c.First_Name__c}"/>
<apex:outputField value="{!CRS_Question__c.Last_Name__c}"/>
<!----6----> <apex:outputField value="{!CRS_Question__c.Designation__c}"/>

<!----7----> <apex:outputField value="{!CRS_Question__c.Telephone_No__c}"/>
<!----8----> <apex:outputField value="{!CRS_Question__c.Email_Address__c}"/>

<apex:outputField value="{!CRS_Question__c.Status__c}"/>

</apex:pageBlockSection>

<apex:pageBlockSection columns="1" title="Reporting Financial Institution Type" collapsible="false">

<p class="helptext">
The following Institutions are Reporting Financial Institutions ("RFI"):
<ul class="helptext">
<li > <b>Custodial Institution</b> means any Entity that holds, as a substantial portion of its business Financial Assets for the account of others. <br /></li>
<li ><b>Depository Institution</b> means any Entity that accepts deposits in the ordinary course of a banking or similar business.<br /></li>
<li ><b>Specified Insurance Company</b> means any Entity that is an insurance company (or the holding company of an insurance company) which issues, or is obligated to make payments with respect to, a Cash Value Insurance Contract or an Annuity Contract. <br /></li>
<li > <b>Investment Entity</b> means any Entity that: 
<ol class="helptext" type="a">

    <li>primarily conducts a business involving managing, investing, reinvesting or trading in Financial Assets on behalf of a customer; or </li>
    <li >if the Entity is managed by a Reporting Financial Institution referred to in (a) above.</li>
     
</ol>
</li>
   <p class="helptextp"><br />
   Please refer to the <a href="https://www.difc.ae/files/8915/9007/2577/CRS_Guidance_Notes.pdf" target="_blank">Guidance</a> for more information on the definition of Investment Entity in particular <br />
Please refer to <a href="https://www.difc.ae/files/6715/2327/6154/Common_Reporting_Standard_Regulations.pdf" target='_blank'>the Common Reporting Standards Regulations ("Regulations") </a> for the full definitions of each of each of the above terms. 
</p>

</ul> 

</p>



    
<!----8----> <apex:outputField value="{!CRS_Question__c.Reporting_Financial_Institution_type__c}"/>

</apex:pageBlockSection>

<apex:outputPanel >
<apex:outputPanel rendered="{!NOT(Contains(CRS_Question__c.Reporting_Financial_Institution_type__c,'Not a Reporting Financial Institution'))}">

<!---title="Confirm RFI Type"--->
 <apex:pageBlockSection columns="1"  id="ConfirmRFIType" collapsible="false">
 
    <!----12----> <apex:outputField label="Was the entity a Custodial Institution during the reporting period?" value="{!CRS_Question__c.Custodial_Institution__c}"/>
    
    <!----13----> <apex:outputField value="{!CRS_Question__c.Depository_Institution__c}"/>
    
    <!----14----> <apex:outputField label="Was the entity an Investment Entity during the  reporting period ?" value="{!CRS_Question__c.Investment_Entity__c}"/>
    
    <!----15----> <apex:outputField label="Was the entity a Specified Insurance Company during the reporting period ?" value="{!CRS_Question__c.Insurance__c}"/>
    
</apex:pageBlockSection>


<!-----===============================================================================================-----> 
<apex:pageBlockSection columns="1" title="Nil/Null Returns" collapsible="false" id="crstype">

 <p class="helptext">
A Nil/Null return is filed to indicate that the RFI did not maintain any Reportable Accounts during the reporting period.
 </p>

<!----16----> <apex:outputField label="Is a Nil/Null return being filed" value="{!CRS_Question__c.Null_return_being_filed__c}"/>
<!----17----> <apex:outputField value="{!CRS_Question__c.Reason_for_filing_a_Null_return__c}"/>
<!----18----> <apex:outputField rendered="{!OR(CRS_Question__c.Reason_for_filing_a_Null_return__c='Other')}"  label="Please provide detailed reasons" value="{!CRS_Question__c.Detailed_reasons__c}"/>

</apex:pageBlockSection>



<!-----===============================================================================================-----> 

<apex:pageBlockSection columns="1" title="Service Provider Details" id="thirdpartylist" collapsible="false">

    <p class="helptext">
    
Section II(E) of the Regulations enables an RFI to use a service provider to fulfill its reporting and due diligence
obligations, however these obligations shall remain the responsibility of the RFI. <br />
Please use this
section to provide details about such third party service provider that was engaged to fulfill the RFI’s due diligence obligations.

   
    </p>
    

    <!----20----> <apex:outputField label="Was a Service Provider used to fulfil the entity's Reporting and Due Diligence obligations for CRS " value="{!CRS_Question__c.Third_Party_Service_Provider__c}"/>
    <!----21----> <apex:outputField rendered="{!AND(CRS_Question__c.Third_Party_Service_Provider__c!=null,Contains(CRS_Question__c.Third_Party_Service_Provider__c,'Yes'))}" value="{!CRS_Question__c.Entity_Name__c}"/>
    <!----22----> <apex:outputField rendered="{!AND(CRS_Question__c.Third_Party_Service_Provider__c!=null,Contains(CRS_Question__c.Third_Party_Service_Provider__c,'Yes'))}" value="{!CRS_Question__c.Registration_No__c}"/>
    <!----23----> <apex:outputField rendered="{!AND(CRS_Question__c.Third_Party_Service_Provider__c!=null,Contains(CRS_Question__c.Third_Party_Service_Provider__c,'Yes'))}" value="{!CRS_Question__c.Jurisdiction__c}"/>
    
    
    <apex:outputPanel styleClass="helptext"  rendered="{!AND(CRS_Question__c.Third_Party_Service_Provider__c!=null,Contains(CRS_Question__c.Third_Party_Service_Provider__c,'Yes'))}" >
  The Reporting Financial Institution remains responsible for fulfilling CRS obligations under the DIFC Common Reporting Standards Law and Regulations where the Due Diligence Obligations are outsourced.
      
    </apex:outputPanel>
  <p class="helptext"><b>If more than one (1) service provider was used, please contact roc.helpdesk@difc.ae</b></p>
</apex:pageBlockSection>

<!-----===============================================================================================-----> 


    
<apex:pageBlockSection columns="1" rendered="{!CRS_Question__c.Company_Type__c=='Financial - related'}" title="Due Diligence Procedures" id="diligencelist" collapsible="false">


<p class="helptext">
This section asks for information relating to the due diligence procedures applied by the RFI during the reporting period. <br />

<ul class="helptext">

<li><b>Pre-existing Accounts</b> are defined in Section VIII(C)(9) of the CRS Regulations</li>
<li><b>Cash Value Insurance Contract </b> means an Insurance Contract (other than an indemnity reinsurance contract between two insurance companies) that has a Cash Value.</li>
<li><b>Annuity Contract</b> means a contract under which the issuer agrees to make payments for a period of time determined in whole or in part by reference to the life expectancy of one or more individuals. </li>
<li><b>Pre-existing Entity Account</b> means a Pre-existing Account held by one or more Entities.</li>
<li><b>Pre-existing Individual Account</b> means a Pre-existing Account held by one or more individuals</li>

<p class="helptextp">
<br />
Please refer to <a href='https://www.difc.ae/files/6715/2327/6154/Common_Reporting_Standard_Regulations.pdf' target="_blank"> the Common Reporting Standards Regulations ("CRS Regulations")</a> for the full definitions of each of these terms.

</p>
</ul>

</p>

<!----24----><apex:outputField label="Were any due diligence procedures applied to <b>Pre-existing Accounts</b> during the reporting period?"
     value="{!CRS_Question__c.Pre_existing_Accounts_during_the_reporti__c}" />
     
<!----25----><apex:outputField rendered="{!CRS_Question__c.Pre_existing_Accounts_during_the_reporti__c='Yes'}"  label="Were the due diligence procedures for <b>New Accounts</b> applied to <u>all</u> Pre-existing Accounts during the reporting period?"
     value="{!CRS_Question__c.due_diligence_procedures__c}" />
     
     
    
     
     </apex:pageBlockSection>
     
     
    
    
     
     <apex:pageBlockSection columns="1"  id="duediligenceq"   collapsible="false">
     
     <apex:outputPanel rendered="{!AND(CRS_Question__c.Pre_existing_Accounts_during_the_reporti__c='Yes',CRS_Question__c.due_diligence_procedures__c='No')}">
     
     <p class="helptext"> <br />
<h2>Pre-existing Accounts</h2>
</p>
     
     </apex:outputPanel>
    
 

 <!----26---->  <apex:outputField label="<b>Self Certification</b> was obtained for all Pre-existing Accounts :"  rendered="{!AND(CRS_Question__c.Pre_existing_Accounts_during_the_reporti__c='Yes',CRS_Question__c.due_diligence_procedures__c='No')}" value="{!CRS_Question__c.Please_select_the_relevant_option__c}" />
 <!----27---->  <apex:outputField rendered="{!AND(CRS_Question__c.Pre_existing_Accounts_during_the_reporti__c='Yes',CRS_Question__c.due_diligence_procedures__c='No',Contains(CRS_Question__c.Please_select_the_relevant_option__c,'clearly identified group'))}"  label="Specify the clearly identified group(s)" value="{!CRS_Question__c.Clearly_identified_group_s__c}"/>
 <!----28---->  <apex:outputField rendered="{!AND(CRS_Question__c.Pre_existing_Accounts_during_the_reporti__c='Yes',CRS_Question__c.due_diligence_procedures__c='No',Contains(CRS_Question__c.Please_select_the_relevant_option__c,'Other'))}" label="If 'other' is selected, please provide details here" value="{!CRS_Question__c.Detailed_reasons__c}"/>
<!----26.1---->  <apex:outputField label="<b>Due diligence</b>  was <u> not</u> conducted for Pre-existing Accounts :"  rendered="{!AND(CRS_Question__c.Pre_existing_Accounts_during_the_reporti__c='Yes',CRS_Question__c.due_diligence_procedures__c='No')}" value="{!CRS_Question__c.Due_diligence_not_conducted__c}" /> 
            

            
    </apex:pageBlockSection>
     <!---- 
       <apex:pageBlockSection columns="1"  id="duediligenceqPre_existing" title="111" >
   
  <apex:outputField  label="Due diligence not conducted on  Pre-existing" value="{!CRS_Question__c.Please_select_relevant_option_2__c}"/>
    <apex:outputField  value="{!CRS_Question__c.Please_provide_detailed_reasons__c}"/> 
    
    </apex:pageBlockSection>
    --->
<!-----===============================================================================================-----> 
    
  <apex:pageBlockSection columns="1"  id="duediligence_a"  collapsible="false" >
  <!----====- title="6. Due diligence procedures on Pre-existing Accounts" ---->
  <apex:outputPanel rendered="{!OR(CRS_Question__c.Pre_existing_Accounts_during_the_reporti__c='No',CRS_Question__c.due_diligence_procedures__c='No')}">
<p class="helptext"><br />
<h2>Lower Value Accounts</h2>
</p>
<ul class="helptext">
<li><b>Lower Value Account</b> means a Pre-existing Individual Account with an aggregate balance or value as of 31 December 2016 that does not exceed USD1,000,000. </li>
<li><b>High Value Account</b> means a Pre-existing Individual Account with an aggregate balance or value that exceeds USD1,000,000 as of 31 December 2016, or 31 December of any subsequent year.</li>
<li><b>Residence Address Test</b> means the residence address test described in of Section III(B)(1) of the CRS Regulations.</li>
<li><b>Documentary Evidence</b> has the meaning given to the term in Section III(E)(6) of the CRS Regulations.</li>

<p class="helptextp">
<br />
Please refer to <a href='https://www.difc.ae/files/6715/2327/6154/Common_Reporting_Standard_Regulations.pdf' target="_blank"> the Common Reporting Standards Regulations ("CRS Regulations")</a> for the full definitions of each of these terms.

</p>

</ul>
</apex:outputPanel>
   
 
    
    
 <!----29----> <apex:outputField rendered="{!OR(CRS_Question__c.Pre_existing_Accounts_during_the_reporti__c='No',CRS_Question__c.due_diligence_procedures__c='No')}" label="Were due diligence procedures for <b> High Value Accounts</b> applied to Lower Value Accounts ?" value="{!CRS_Question__c.High_value_applied_to_Lower_Value__c}"/>
 
 <!----30----><apex:outputField rendered="{!OR(CRS_Question__c.High_value_applied_to_Lower_Value__c=='No')}" label="Were any due diligence procedures applied for <b>Lower Value Accounts</b>?" value="{!CRS_Question__c.Resident_Address_test_used__c}"/>
 
 <!----31----> <apex:outputField rendered="{!OR(CRS_Question__c.Resident_Address_test_used__c=='Yes')}" value="{!CRS_Question__c.due_diligence_the_relevant_option__c}"/>
 
 <!----32----> <apex:outputField rendered="{!AND(CRS_Question__c.due_diligence_the_relevant_option__c!=null,Contains(CRS_Question__c.due_diligence_the_relevant_option__c,'Other'))}" label="If 'other' is selected, please provide details here" value="{!CRS_Question__c.due_diligence_detailed_reasons__c}"/>


    
</apex:pageBlockSection>

 
    
    
    
<apex:pageBlockSection columns="1" title="Undocumented, Reportable and Excluded Accounts for the reporting period ending 31 December 2019"  rendered="{!CRS_Question__c.Company_Type__c=='Financial - related'}" collapsible="false" id="UndocumentedDL">    

<p class="helptext">
<ul class="helptext"> 

<li><b>Undocumented Account</b> exists where the only evidence for the residence status for a Pre-existing Individual Account is a "hold mail" or "in-care-of address" and the Reporting Financial Institution has not been able to obtain a self-certification from the individual. </li>
<li><b>Reportable Account</b> has the meaning given to the term in Section III(E)(6) of the CRS Regulations.</li>
<li><b>Excluded Account</b> has the meaning given to the term in Section VIII(C)(17) of the CRS Regulations.</li>
<p class="helptextp">
<br />
Please refer to <a href='https://www.difc.ae/files/6715/2327/6154/Common_Reporting_Standard_Regulations.pdf' target="_blank"> the Common Reporting Standards Regulations ("CRS Regulations")</a> for the full definitions of each of these terms.

</p>

</ul>
</p>
    
<!----33---->   <apex:outputField label="Were <b>Undocumented Accounts</b> reported ?" value="{!CRS_Question__c.Undocumented_Accounts_being_reported__c}"/>
<!----34---->    <apex:outputField rendered="{!CRS_Question__c.Undocumented_Accounts_being_reported__c='Yes'}" label="Specify the number of <b>Undocumented Accounts</b>" value="{!CRS_Question__c.The_number_of_Undocumented_Reports__c}"/>
<!----35---->    <apex:outputField label="Specify the number of <b>accounts closed</b> during the reporting period" value="{!CRS_Question__c.Closed_during_the_reporting_period__c}"/>
<!----36---->    <apex:outputField label="Specify the total number of <b>Reportable Accounts</b> " value="{!CRS_Question__c.Reportable_Accounts_for_the_reporting_pe__c}"/>

<!----37---->    <apex:outputField label="Specify the total number of <b>Excluded Accounts</b> (non-reportable accounts)" value="{!CRS_Question__c.Total_number_of_Excluded_Accounts__c}"/>
<!----38---->    <apex:outputField rendered="{!CRS_Question__c.Total_number_of_Excluded_Accounts__c='unknown'}" label="Specify the reason(s) why the number is unknown" value="{!CRS_Question__c.Number_is_unknown__c}"/>

</apex:pageBlockSection>

<apex:pageBlockSection columns="1" title="CRS Polices and Procedures" collapsible="false" id="UndocumentedDL11">    


<!----39---->    <apex:outputField label="Have CRS policies and procedures been established and maintained?" value="{!CRS_Question__c.Maintained_for_the_entity_s_obligations__c}"/>
<!----40---->    <apex:outputField label="Were the CRS policies and procedures  implemented and complied with?" value="{!CRS_Question__c.Complied_with_for_the_reporting_period__c}"/>
         
   
</apex:pageBlockSection>

</apex:outputPanel>

<apex:outputPanel rendered="{!Contains(CRS_Question__c.Reporting_Financial_Institution_type__c,'Not a Reporting Financial Institution')}">

 <apex:pageBlockSection columns="1"  id="NotReporting" collapsible="false">
 
 <apex:outputField label="Please specify why the entity is not a Reporting Financial Institution" value="{!CRS_Question__c.Entity_is_not_a_Reporting_Financial_Inst__c}" />
 
 
 </apex:pageBlockSection>


</apex:outputPanel>


</apex:outputPanel>

<!-----===============================================================================================-----> 

<!----41-42----> 
<apex:pageBlockSection columns="1" title="Declaration " collapsible="false">
<apex:pageBlockSectionItem >

<apex:outputField value="{!CRS_Question__c.Shared_with_The_Dubai_Financial_Services__c}"/> 
<b>Declaration: By submitting this request, the entity confirms that the information provided in this notification is accurate and acknowledges that</b><br /><br />
<ul class="helptext">
<li>The information contained in this notification may be shared with the Dubai Financial Services Authority (DFSA), the UAE Competent Authority and other relevant government or regulatory authorities in connection with the Common Reporting Standard Law and Regulations;</li>
<li>It is an offence to provide information that is false, misleading or deceptive under the Common Reporting Standard Law, DIFC Law No. 2 of 2018;</li>
<li>It is also an offence to provide the Registrar with information that is false, misleading or deceptive or to conceal information where the concealment of such information is likely to mislead or deceive, under the Operating Law, DIFC Law No. 7 of 2018; and</li>
<li>The Designated Person nominated on this notification is authorised by the entity to file this notification, submit any required document(s) on the DIFC Client Portal, respond to any queries and provide further information on its behalf to the Registrar of Companies.</li>

</ul>

</apex:pageBlockSectionItem>

    
<!----
<apex:pageBlockSectionItem >
<apex:outputField value="{!CRS_Question__c.Offence_to_provide_information_that_is_f__c}"/>

</apex:pageBlockSectionItem>
<apex:pageBlockSectionItem >
<apex:outputField value="{!CRS_Question__c.Operating_Law_DIFC_Law_No_7_of_2018__c}"/>

</apex:pageBlockSectionItem>

<apex:pageBlockSectionItem >
<apex:outputField value="{!CRS_Question__c.Designated_Person_nominated__c}"/>

</apex:pageBlockSectionItem>
-->

</apex:pageBlockSection>

</apex:pageBlock>
</apex:form>
</apex:page>