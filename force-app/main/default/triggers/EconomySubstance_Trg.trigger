/*********************************************
*Apex Trigger : EconomySubstance_Trg  
*Description: if SR removed or deleted form BPM system will delete the economy substance record automatically
*
*
************************************************/
trigger EconomySubstance_Trg  on Economy_Substance__c (after update) {
List<Economy_Substance__c > ecList = new List<Economy_Substance__c>();
    for(Economy_Substance__c itr:trigger.new){
        if(itr.Service_Request__c!=null && trigger.oldmap.get(itr.Id).Service_Request__c!=null){
            ecList.add(itr);
        }
    }
    if(ecList.size()>0){
        if(label.Delete_Economy_Substance=='TRUE'){
            delete ecList;
        }
    }
}