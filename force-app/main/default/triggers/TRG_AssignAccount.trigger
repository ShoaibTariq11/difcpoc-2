/******************************************************************************************
 *  Name        : TRG_AssignAccount 
 *  Author      : Kaavya Raghuram
 *  Company     : NSI JLT
 *  Date        : 2015-04-25
 *  Description : This trigger is to assign generic account to the contact if account is blank
                  Required for sharing rule      
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.1    31-Aug-2015 Kaavya        Updated for handling RORP contacts       
 V1.2    17-May-2016 Kaavya        Updated for handling BD Contacts
 v2.3    29-Nov-2016 Veera         Added validation rule not allowed to update is important field if its enabled for any contact
 v2.4    07-Oct-2019 Durga         Added If Condition to execute differentiate the After and Before Events, Invoking ContactTriggerHandler to create Portal User
 v2.5    08-Oct-2019 Rajil         Added the condition to skip the trigger validation during changeset validation
*******************************************************************************************/
trigger TRG_AssignAccount on Contact (before insert,after insert, before update,after update) {
    
    
    if(UserInfo.getUserName()!=label.User_WF_Trigger_Disabled){ //v2.5
        if(trigger.IsBefore){ //v2.4
            System.debug('============Start TRG_AssignAccount ==========================');
            Boolean reassign=false;
            for(Contact C:trigger.new){
                if(C.AccountID==null)
                    reassign=true;
            }
            if(reassign){
                //Querying all system accounts
                ID sysrectypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('System Account').getRecordTypeId();        
                String year= string.valueof(system.today().year());
                system.debug('YYY'+year);
                Map<String,Id> AccMap= new Map<String,Id>();
                List<Account> SysAccounts=[select id, name from Account where RecordtypeId=:sysrectypeid];
                for(Account A: SysAccounts){           
                    AccMap.put(A.name,A.id);
                }
                //Getting the record type ids of Contact
                ID ROCrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Individual Contact').getRecordTypeId();
                ID GSrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
                ID RORPrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('RORP Contact').getRecordTypeId();        
                ID BDrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Business Development').getRecordTypeId(); //Added as per V1.2
                for(Contact C:trigger.new){
                    if(C.AccountID==null){                
                        if(C.RecordTypeId==ROCrectypeid){                
                            C.AccountId=AccMap.get('ROC Contacts '+year);
                            if(C.AccountId==null){
                                Account roc= new Account();
                                roc.name= 'ROC Contacts '+year; 
                                roc.RecordtypeId=sysrectypeid;
                                insert roc;
                                C.AccountId=roc.id;
                            }
                        }
                        else if(C.RecordTypeId==GSrectypeid){  
                            C.AccountId=AccMap.get('GS Contacts '+year);
                            if(C.AccountId==null){
                                Account gs= new Account();
                                gs.name= 'GS Contacts '+year; 
                                gs.RecordtypeId=sysrectypeid;
                                insert gs;
                                C.AccountId=gs.id;
                            }
                        }
                        else if(C.RecordTypeId==RORPrectypeid){  
                            C.AccountId=AccMap.get('RORP Contacts '+year); 
                            if(C.AccountId==null){
                                Account rorp= new Account();
                                rorp.name= 'RORP Contacts '+year; 
                                rorp.RecordtypeId=sysrectypeid;
                                insert rorp;
                                C.AccountId=rorp.id;
                            }
                        }else if(C.RecordTypeId==BDrectypeid){  
                            C.AccountId=AccMap.get('BD Contacts '+year); 
                            if(C.AccountId==null){
                                Account bd= new Account();
                                bd.name= 'BD Contacts '+year; 
                                bd.RecordtypeId=sysrectypeid;
                                insert bd;
                                C.AccountId=bd.id;
                            }
                        }
                    }
                    
                }    
            }
            System.debug('============End TRG_AssignAccount ==========================');
            if(trigger.isBefore ){ 
            List<Contact> lstNewCon = new List<Contact>();
            string recordTypeName;
            for(Contact iCon : trigger.New){
                recordTypeName = Schema.getGlobalDescribe().get('Contact').getDescribe().getRecordTypeInfosById().get(iCon.RecordtypeID).getName();
                
                if(recordTypeName  == 'Business Development' && iCon.important__c ) 
                    lstNewCon.add(iCon);
                
            }
            if(!lstNewCon.isEmpty())    
                ContactTriggerHadler.IsImportantCheck(lstNewCon , trigger.IsUpdate);
            }
        }
        
        //v2.4
        if(trigger.IsAfter){
            if(trigger.IsInsert)
                ContactTriggerHadler.onAfterInsert(Trigger.new);
            
            if(trigger.IsUpdate)
                ContactTriggerHadler.onAfterUpdate(Trigger.new,trigger.oldmap,trigger.newmap);
        }
    }
}