/***********************************************************************************
 *  Author   : shoaib tariq
 *  Company  : Tech Carrot
 *  Date     : 01-jan-2020
  --------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------
 Change Description
 Version      ModifiedBy            Date                Description
 V1.0         shoaib tariq        19-01-2020          create failed shipments
 ************************************************************************************/
trigger logTrigger on Log__c (Before insert,after insert) {
    
    List<Id> stepIds = new List<Id>(); 
    Set<String> typeString = new Set<String>();
    
    for(Log__c thisLog : Trigger.new){
        typeString.add(thisLog.Type__c);
    }
    
    Map<String,Integer> countMap = new Map<String,Integer>();
    
    List<AggregateResult> AggregateResultList = [select count(Id)total,Type__c FROM Log__c WHERE Type__c in:typeString AND CreatedDate = Today group by Type__c];
      
    for(AggregateResult aggr:AggregateResultList){             
        countMap.put((String)aggr.get('Type__c'),(Integer)aggr.get('total'));
    } 
    
    //Before Insert
    if(Trigger.isBefore){
        for(Log__c thisLog : Trigger.new){
            if(!countMap.isempty() && 
                 countMap.containsKey(thisLog.Type__c)){
                
                thisLog.Description__c = thisLog.Description__c + ' Called ' +countMap.get(thisLog.Type__c)+ ' Times'; 
            }
            else{
                thisLog.Description__c = thisLog.Description__c + ' Called 1 Time';
            }
        }
    }
    //After Insert
    if(Trigger.isAfter) {     
        for(Log__c thisLog : Trigger.new){
           if(thisLog.Type__c.contains('Shipment Creation Error stepId') 
              && ((countMap.containsKey(thisLog.Type__c) 
                     && countMap.get(thisLog.Type__c) <=3)
                        || !countMap.containsKey(thisLog.Type__c))){
                 
                  if(!test.isRunningTest()) { String thisStepId = thisLog.Description__c.split('stepId')[1].split('#')[0];
                    stepIds.add(thisStepId.trim());
                  }
               }
            }
        
        if(!stepIds.isEmpty()){ LogTriggerHandler.CreateFailedShipment(stepIds);
        }
    }
  }