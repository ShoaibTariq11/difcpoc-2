trigger VisaExceptionTrigger on Visa_Exception__c (after insert) {
  
   if(trigger.isInsert && trigger.isAfter){   
    for(Visa_Exception__c  visaEx : trigger.new){  
       visaExceptionTriggerHandler.updateAccountExceptionQuota(visaEx);
   }
 }
}