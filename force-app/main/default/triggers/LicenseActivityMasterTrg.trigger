/*
    * Created by  : Ravi
    * Description : This trigger will update the Code on License Activity Master, the same Code values are using in all the conditions
--------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By      Description
 ----------------------------------------------------------------------------------------              
 V1.1   21/07/2015      Ravi            Added the logic to updated the Sys Code
 
 ----------------------------------------------------------------------------------------
*/
trigger LicenseActivityMasterTrg on License_Activity_Master__c (before insert) {
    
    string Code = '000';
    for(License_Activity_Master__c objLM : [select Id,Sys_Code__c from License_Activity_Master__c where Sys_Code__c != null order by Sys_Auto_Number__c desc limit 1]){
        Code = objLM.Sys_Code__c;
    }   
    for(License_Activity_Master__c objLAM : trigger.new){
        Code = GenericHelperCls.NextActivityCode(Code);
        objLAM.Sys_code__c = Code;
    }
    /*
    Integer i = [select count() from License_Activity_Master__c];
    i = (i!=null)?(i+1):1;
    
    for(License_Activity_Master__c objLAM : trigger.new){
        if(objLAM.Sys_Code__c == null || objLAM.Sys_Code__c == ''){
            String Code = '000';
            Code = Code+''+i;
            objLAM.Sys_code__c = Code.right(3);
            i++;
        }
    }*/
}