//Arun singh 25/09/2017 :Added code to handle expired activites 
//Dansh farooq 02/02/2018 : Added code to capture activities description on account record tkt # 5015
//                        field name : License_Activity_Description__c  on account object
trigger LicenseActivityTrg on License_Activity__c (before insert,after insert, after update,after delete,before delete) 
{
    
System.debug('==================================LicenseActivityTrg====================================');   
    list<Account> lstAccounts = new list<Account>();
    list<Id> lstAccountIds = new list<Id>();
    if(trigger.isBefore && trigger.isDelete == false){
        for(License_Activity__c objLA : trigger.new){
            if(objLA.Start_Date__c == null)
                objLA.Start_Date__c = system.today();
            if(trigger.isInsert)
                objLA.License_Master_Account_Unique_Id__c = objLA.Account__c+':'+objLA.Activity__c;
            lstAccountIds.add(objLA.Account__c);
        }
    }
    else if(trigger.isAfter)
    {
        
      //  set<Id> ListonOldActivities=new set<Id>();
    
        for(License_Activity__c objLA : trigger.isDelete?trigger.old:trigger.new)
        {
            system.debug('End_Date__c is : '+objLA.End_Date__c);
            if(trigger.isInsert || trigger.isDelete || trigger.isUpdate)
            { //(trigger.isUpdate && (objLA.Activity__c != trigger.oldMap.get(objLA.Id).Activity__c ||  objLA.End_Date__c != trigger.oldMap.get(objLA.Id).End_Date__c) ))
            
            
            lstAccountIds.add(objLA.Account__c);
            
            /*
            //Arun Singh on 25/09/2017 added code to remove expired activies from the account update 
            if(objLA.id!=null && objLA.End_Date__c!=null)
                if(objLA.End_Date__c<=system.today())
                    ListonOldActivities.add(objLA.id);
            System.debug('ListonOldActivities========>'+ListonOldActivities);
            
            */
            
            }
        }
        
        if(!lstAccountIds.isEmpty()){
            map<Id,list<Id>> mapAccountIds = new map<Id,list<Id>>();
            if(trigger.isDelete == false){
                /*for(License_Activity__c objAR : [select Id,Account__c,End_Date__c from License_Activity__c where End_Date__c = null AND Account__c IN : lstAccountIds AND Account__r.Company_Type__c!='Financial - related' order by Account__c]){
                    system.debug('objAR.End_Date__c is : '+objAR.End_Date__c);
                    if(objAR.End_Date__c == null){
                        if(mapAccountIds.containsKey(objAR.Account__c)){
                            trigger.new[0].addError('Only Financial related Clients can have multiple Activities.');
                        }else{
                            list<Id> lst = new list<Id>();
                            lst.add(objAR.Id);
                            mapAccountIds.put(objAR.Account__c,lst);
                        }
                    }
                }*/
                
                /*
                Commented on 15th Jan,2014 as per #442
                for(AggregateResult objAR : [select Account__c,count(Id) NoOfLAs from License_Activity__c where End_Date__c = null AND Account__c IN : lstAccountIds AND Account__r.Company_Type__c!='Financial - related' group by Account__c]){
                    system.debug('NoOfLAs $$$$ '+Integer.valueOf(objAR.get('NoOfLAs')));
                    if(objAR.get('NoOfLAs') != null && Integer.valueOf(objAR.get('NoOfLAs')) > 1){
                       //trigger.new[0].addError('Only Financial related Clients can have multiple Activities.');
                    }
                }
                */
            }
            /*if(!lstAccountIds.isEmpty()){
                for(AggregateResult objAR : [select Account__c,count(Id) NoOfLAs from License_Activity__c where End_Date__c = null AND Account__c IN : lstAccountIds AND Account__r.Company_Type__c!='Financial - related' group by Account__c]){
                    system.debug('NoOfLAs $$$$ '+Integer.valueOf(objAR.get('NoOfLAs')));
                    if(objAR.get('NoOfLAs') != null && Integer.valueOf(objAR.get('NoOfLAs')) > 1){
                        trigger.new[0].addError('Only Financial related Clients can have multiple Activities.');
                    }
                }
            }*/
            
            //Arun Singh on 25/09/2017 Changed  End_Date__c >=:system.today() to End_Date__c >:system.today() 
            for(Account objAccount : [select Id,Name,License_Activity_Description__c,Company_Type__c,(select id,End_Date__c,Activity__r.Name,Activity__r.Sys_Code__c,Activity__r.Activity_Name_Arabic__c from License_Activities__r where End_Date__c = null OR End_Date__c >:system.today() order by Activity__r.Sys_Code__c) from Account where Id IN : lstAccountIds])
            
            {
                if(objAccount.License_Activities__r != null && objAccount.License_Activities__r.size() > 0){
                    Account objAccountTemp = new Account(Id=objAccount.Id);
                    /*if(objAccount.Company_Type__c != 'Financial - related'){
                        objAccountTemp.Business_Activity__c = objAccount.License_Activities__r[0].Activity__r.Name;
                        objAccountTemp.License_Activity__c = '';
                    }else{*/
                        string LicenseActivities = '';
                        objAccountTemp.License_Activity_Description__c = '';
                        for(License_Activity__c objLA : objAccount.License_Activities__r)
                        {
                            System.debug('objLA========>'+objLA);
                           // if(!ListonOldActivities.contains(objLA.id))// Arun Singh on 25/09/2017 Added to avoid activites which are expired and updating this same batch of add 
                            LicenseActivities += objLA.Activity__r.Sys_Code__c+',';
                            objAccountTemp.License_Activity_Description__c += string.format('{0}\n',new List<String>{objLA.Activity__r.Name});
                        }
                        System.debug('LicenseActivities========>'+LicenseActivities);
                        
                        if(LicenseActivities != '')
                            LicenseActivities = LicenseActivities.substring(0,LicenseActivities.length()-1);
                        
                        objAccountTemp.License_Activity__c = LicenseActivities;
                       
                        //objAccountTemp.Business_Activity__c = '';
                    /*}*/
                    /*
                    Commented on 15th Jan,2014 as per #442
                    if(objAccount.Company_Type__c == 'Non - financial' || objAccount.Company_Type__c == 'Retail'){
                        objAccountTemp.Nature_of_business__c = objAccount.License_Activities__r[0].Activity__r.Name;
                        objAccountTemp.Nature_of_Business_Arabic__c = objAccount.License_Activities__r[0].Activity__r.Activity_Name_Arabic__c;
                    }*/
                    lstAccounts.add(objAccountTemp);
                }else{
                    Account objAccountTemp = new Account(Id=objAccount.Id);
                    objAccountTemp.License_Activity__c = '';
                    objAccountTemp.License_Activity_Description__c = '';
                    /*
                    Commented on 15th Jan,2014 as per #442
                    if(objAccount.Company_Type__c == 'Non - financial' || objAccount.Company_Type__c == 'Retail'){
                        objAccountTemp.Nature_of_business__c = '';
                        objAccountTemp.Nature_of_Business_Arabic__c = '';
                    }
                    */   
                    lstAccounts.add(objAccountTemp);
                }
            }
            if(!lstAccounts.isEmpty())
                update lstAccounts;
        }
    }
    /*if(trigger.isDelete && trigger.isBefore){
        map<Id,list<License_Activity__c>> mapAccountActivities = new map<Id,list<License_Activity__c>>();
        for(License_Activity__c objLA : trigger.old){
            list<License_Activity__c> lst = mapAccountActivities.containsKey(objLA.Account__c)?mapAccountActivities.get(objLA.Account__c):new list<License_Activity__c>();
            lst.add(objLA);
            mapAccountActivities.put(objLA.Account__c,lst);
        }
        if(!mapAccountActivities.isEmpty()){
            for(Account objAccount : [select Id,ROC_Status__c from Account where Id IN : mapAccountActivities.keySet() AND Registration_License_No__c != null AND Registration_License_No__c != '']){
                trigger.oldMap.get(mapAccountActivities.get(objAccount.Id)[0].Id).addError('Please do not delete the Activity for active client.');
            }
        }
    }*/
}