/*
    Author      : Durga Prasad
    Date        : 30-Oct-2019
    Description : Trigger on HexaBPM__Step__c object
    ----------------------------------------------------------------
*/
trigger OB_StepTrigger on HexaBPM__Step__c (before insert, before update,after insert) 
{
    
    
    
    if(UserInfo.getUserName()!=label.User_WF_Trigger_Disabled)
    {
        
        if(trigger.isBefore)
        {
            if(trigger.isInsert)
                OB_StepTriggerHandler.OnBeforeInsert(trigger.new);

                if(trigger.isUpdate)
                    OB_StepTriggerHandler.OnBeforeUpdate(Trigger.Old,
                                                        Trigger.New,
                                                        Trigger.OldMap,
                                                        Trigger.NewMap);
        }
         
        if(trigger.isAfter)
        {
                if(trigger.isInsert)
                    OB_StepTriggerHandler.OnAfterInsert(trigger.new);

              
                
        }  

            
             
    }
   
    
    
}