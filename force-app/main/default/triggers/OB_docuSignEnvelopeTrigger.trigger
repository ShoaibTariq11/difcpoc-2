trigger OB_docuSignEnvelopeTrigger on dfsle__Envelope__c (before insert,after insert,before update,after update) {
    
    if(trigger.isBefore) {
      if(trigger.isInsert) {
          
          OB_docuSignEnvelopeTriggerHandler.execute_BI(Trigger.New);
      }
      if(trigger.isUpdate) {

          OB_docuSignEnvelopeTriggerHandler.execute_BU(Trigger.New,Trigger.OldMap);
      }
  }
    
  if(trigger.isAfter) {
      if(trigger.isInsert) {
          OB_docuSignEnvelopeTriggerHandler.execute_AI(Trigger.New);
      }
      if(trigger.isUpdate) {
          
          OB_docuSignEnvelopeTriggerHandler.execute_AU(Trigger.New,Trigger.OldMap);
      }
  }

}