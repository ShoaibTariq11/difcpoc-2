/*
    Author      :   Ravi
    Description :   This trigger will populate the unique License # on each license.
    
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.0    26-01-2015  Ravi            Created   
    V1.1    02-08-2018  Arun            Added filter to disable trigger for specifie profile 
*/
trigger LicenseTrg on License__c (before insert, before update) 
{
 //Check custom label stored user id same as running user id ? if not the same run the trigger 
    if(UserInfo.getUserName()!=label.User_WF_Trigger_Disabled)
    {
    
    if(trigger.isInsert && trigger.isBefore){
        Integer PreviousMaxNo = 0;
        for(License__c objLicense : [select Name,Sys_License_No__c from License__c order by Sys_License_No__c desc limit 1]){
            PreviousMaxNo = (objLicense.Sys_License_No__c != null)?objLicense.Sys_License_No__c.intValue():Integer.valueOf(objLicense.Name);
        }
        system.debug('PreviousMaxNo is : '+PreviousMaxNo);
        for(License__c objLicense : trigger.new){
            string sLNo = '0000'+(PreviousMaxNo+1);
            Integer iLen = (string.valueOf(PreviousMaxNo).length() < 4 )?4:string.valueOf(PreviousMaxNo).length();
            if(sLNo.length() > iLen){
                sLNo = sLNo.substring(sLNo.length()-iLen, sLNo.length());
            }
            system.debug('sLNo is : '+sLNo);
            objLicense.Name = sLNo;
            objLicense.License_No__c = sLNo;
            PreviousMaxNo++;
        }
    }
    if(trigger.isUpdate && trigger.isBefore ){
        for(License__c objLicense : trigger.new){
            if(objLicense.Name != trigger.oldMap.get(objLicense.Id).Name || objLicense.License_No__c != trigger.oldMap.get(objLicense.Id).License_No__c){
                objLicense.addError('License number cannot be change');
            }
        }
    }
    
    
    //if(trigger.isAfter && trigger.isInsert){
      //  LicenseTriggerHandler.updatePendingLandlordNocStep(trigger.new);
    //}
    
    }
}