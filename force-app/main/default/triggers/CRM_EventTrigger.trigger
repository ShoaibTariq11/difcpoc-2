trigger CRM_EventTrigger on Event (before insert, after insert, before update) {

    if(Trigger.isBefore){
        CRM_cls_KPI_Utils.updateActivityDetails( Trigger.new, false, true, Trigger.isInsert);    
    } 
    
    if(Trigger.isAfter){
        CRM_cls_KPI_Utils.updateActivityDetails( Trigger.new, false, false, false);    
    }

}