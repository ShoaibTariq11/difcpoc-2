/*
    Author      : Durga Prasad
    Date        : 13-Nov-2019
    Description : Trigger on HexaBPM_Amendment__c object
    ----------------------------------------------------------------
*/
trigger OB_AmendmentTrigger on HexaBPM_Amendment__c (
                                                     before insert, 
                                                     before update,
                                                     before delete,
                                                     after insert, 
                                                     after update,
                                                     after delete
                                                     
                                                    ) 
{
    // To by pass validation.
    if(UserInfo.getUserName()!=label.User_WF_Trigger_Disabled)
    {
         	if(trigger.isBefore)
            {
                if(trigger.isInsert)
                    OB_AmendmentTriggerHandler.OnBeforeInsert(trigger.new);
                if(trigger.isUpdate)
                    OB_AmendmentTriggerHandler.OnBeforeUpdate(trigger.old,trigger.new,trigger.oldmap,trigger.newmap);
                if(trigger.isDelete)
                    OB_AmendmentTriggerHandler.OnBeforeDelete(trigger.old,trigger.new,trigger.oldmap,trigger.newmap);
            }
            
            
             if(trigger.isAfter)
            {
                if(trigger.isInsert)
                    OB_AmendmentTriggerHandler.OnAfterInsert(trigger.new);
                if(trigger.isUpdate)
                    OB_AmendmentTriggerHandler.OnAfterUpdate(trigger.old,trigger.new,trigger.oldmap,trigger.newmap);
                if(trigger.isDelete)
                    OB_AmendmentTriggerHandler.OnAfterDelete(trigger.old,trigger.new,trigger.oldmap,trigger.newmap);
            }
    }
    
   
    
    
}