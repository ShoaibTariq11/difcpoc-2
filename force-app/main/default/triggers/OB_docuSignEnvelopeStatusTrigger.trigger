/**
* Company : PwC.
* Description : Trigger for dfsle__EnvelopeStatus__c objects
*
* This class provides all case related triggers by extending
* OB_docuSignEnvelopeStatusTriggerHandler. 
* ****************************************************************************************
* History :
* [11.FEB.2020] Prateek Kadkol - Code Creation
*/
trigger OB_docuSignEnvelopeStatusTrigger on dfsle__EnvelopeStatus__c (before insert,after insert,before update,after update) 
{

 //Check custom label stored user id same as running user id ? if not the same run the trigger 
 if(UserInfo.getUserName()!=label.User_WF_Trigger_Disabled || test.isRunningtest())
 {
        if(trigger.isBefore) {
            if(trigger.isInsert) {
                OB_docuSignEnvelopeStatusTriggerHandler.execute_BI(Trigger.New);
            }
            if(trigger.isUpdate) {
                OB_docuSignEnvelopeStatusTriggerHandler.execute_BU(Trigger.New,Trigger.OldMap);
            }
        }
        
        if(trigger.isAfter) {
            if(trigger.isInsert) {
                
                OB_docuSignEnvelopeStatusTriggerHandler.execute_AI(Trigger.New);
            }
            if(trigger.isUpdate) {
                OB_docuSignEnvelopeStatusTriggerHandler.execute_AU(Trigger.New,Trigger.OldMap);
            }
        }
 }



}