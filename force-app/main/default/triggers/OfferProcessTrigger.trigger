/********************************************************
* Trigger Name : OfferProcessTrigger 
* Description : if the offer unit already by any other customer throw the validation message.
* Created Date : 24.07.2018
*********************************************************/
trigger OfferProcessTrigger on Offer_Process__c (before update,before insert) {
    
    if((trigger.isupdate || trigger.isInsert) && trigger.isBefore){
        Map<string,Offer_Process__c> unitIdMap = new Map<string,Offer_Process__c>();
        for(Offer_Process__c itr:trigger.new){
                if(itr.Units__c!=null){
                    unitIdMap.put(itr.Units__c,itr);
                }
        }
        if(unitIdMap.size()>0 && !OfferProcessUtility.isExecuted  && trigger.isInsert){
            OfferProcessUtility.checkOfferUnitSelected(unitIdMap,trigger.oldmap,true,false);
        }
        if(unitIdMap.size()>0 && !OfferProcessUtility.isExecuted  && trigger.isUpdate){
            OfferProcessUtility.checkOfferUnitSelected(unitIdMap,trigger.oldmap,false,true);
        }
    }
    
}