/*
    Author      : Durga Prasad
    Date        : 30-Jan-2020
    Description : Trigger on HexaBPM__SR_Doc__c object
    ----------------------------------------------------------------
*/
trigger OB_ApplicationDocTrigger on HexaBPM__SR_Doc__c (before insert,after insert,after update) {
    if(UserInfo.getUserName()!=label.User_WF_Trigger_Disabled){
        if(trigger.isInsert && trigger.isBefore){
            OB_ApplicationDocTriggerHandler.OnBeforeInsert(Trigger.New);
        }
        if(trigger.isAfter && trigger.isInsert){
             Set<Id> setDocumentID = new Set<Id>();
             for(HexaBPM__SR_Doc__c eachDoc :trigger.new){
                if(eachDoc.HexaBPM__Doc_ID__c !=null){
                    setDocumentID.add(eachDoc.ID);
                }            
            }
            system.debug('!!@#@##'+setDocumentID);
            if(setDocumentID !=null && setDocumentID.size()>0){
                FormReviewSuperAdminController.sendUserAccessFormEmail(setDocumentID);
            }
        }
        if(trigger.isUpdate){
             Set<Id> setDocumentID = new Set<Id>();
             
             for(HexaBPM__SR_Doc__c eachDoc :trigger.new){
             
                system.debug('!!@#@##'+trigger.oldmap.get(eachDoc.ID).HexaBPM__Doc_ID__c + '((())'+eachDoc.HexaBPM__Doc_ID__c);
                if((eachDoc.HexaBPM__Doc_ID__c !=null && trigger.oldmap.get(eachDoc.ID).HexaBPM__Doc_ID__c !=
                   eachDoc.HexaBPM__Doc_ID__c) || Test.isRunningTest()){
                    setDocumentID.add(eachDoc.ID);
                }            
            }
            system.debug('!!@#@##'+setDocumentID);
            if(setDocumentID !=null && setDocumentID.size()>0){
                FormReviewSuperAdminController.sendUserAccessFormEmail(setDocumentID);
            }
        }
    }
       
    
   }