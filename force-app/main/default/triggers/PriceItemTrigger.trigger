/*
    Author      :   Shoaib Tariq
    Description :   This class is update SAP Unique Id for Miscellaneous_Services
    --------------------------------------------------------------------------------------------------------------------------
  Modification History
   --------------------------------------------------------------------------------------------------------------------------
  V.No  Date    Updated By      Description
  --------------------------------------------------------------------------------------------------------------------------             
   V1.0    17-02-2020  shoaib      Created
*/
trigger PriceItemTrigger on SR_Price_Item__c (after update) {
    if(trigger.isAfter && Trigger.isUpdate){
   		PriceItemTriggerHandler.UpdateSAPUniqueField(Trigger.new);
    }}