/*
    Created By  : Ravi on 24th Aug,2015
    Description : To perform the Lease actions 
    
--------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date			Updated By		Description
 ----------------------------------------------------------------------------------------              
 V1.0	24/08/2015		Ravi			Added the logic to send the Singnage email to SR when the Business Center Lease (Tenancy) is Created
 V2.0	01/09/2015		Shabbir			Added the logic to populate email address on lease from SR for Business Center Lease (Tenancy) is Created
 ----------------------------------------------------------------------------------------
*/
trigger TenancyTrg on Tenancy__c (after insert) {
	if(trigger.isAfter && trigger.isInsert){
		
		map<Id,Id> mapSRIds = new map<Id,Id>();
		list<Id> lstAccountIds = new list<Id>();
		map<Id,Service_Request__c> mapSRs = new map<Id,Service_Request__c>();
		Service_Request__c objSR;
		
		map<Id,Id> mapUnitIdWithLeaseId = new map<Id,Id>(); // v2.0
		map<Id,string> mapUnitIdWithSREmail = new map<Id,string>(); // v2.0
		map<Id,Lease__c> mapLeaseIdWithObj = new map<Id,Lease__c>(); //v2.0
		
		for(Tenancy__c objT : trigger.new){
			if(objT.Lease_Status__c == 'Active' && objT.Lease_Types__c == 'Business Centre Lease' && objT.Unit__c != null ){
				mapSRIds.put(objT.Unit__c,null);
				lstAccountIds.add(objT.Customer_Id__c);
				mapUnitIdWithLeaseId.put(objT.Unit__c,objT.Lease__c);
			}
		}
		if(!mapSRIds.isEmpty()){
			//v2.0 Modified SOQL to add SR Email in last.
			for(Amendment__c objAmd : [select Id,ServiceRequest__c,Lease_Unit__c , ServiceRequest__r.Email__c , ServiceRequest__r.Customer__r.ROC_Status__c from Amendment__c where Lease_Unit__c != null AND Lease_Unit__c IN : mapSRIds.keySet() AND ServiceRequest__r.Notify_Signage__c = false AND ServiceRequest__r.Record_Type_Name__c = 'Lease_Application_Request' AND ServiceRequest__r.Customer__c IN : lstAccountIds AND (ServiceRequest__r.Customer__r.ROC_Status__c = 'Active' OR ServiceRequest__r.Customer__r.ROC_Status__c = 'Under Formation')]){
				if(objAmd.ServiceRequest__r.Customer__r.ROC_Status__c == 'Active')
					mapSRIds.put(objAmd.Lease_Unit__c,objAmd.ServiceRequest__c);
				
				mapUnitIdWithSREmail.put(objAmd.Lease_Unit__c,objAmd.ServiceRequest__r.Email__c);
			}
			
			for(Id SRId : mapSRIds.values()){
				if(SRId != null){
					objSR = new Service_Request__c(Id=SRId);
					objSR.Notify_Signage__c = true;
					mapSRs.put(objSR.Id,objSR);
				}
			}
			
			if(!mapSRs.isEmpty()){
				update mapSRs.values();
			}
			
			//v2.0 - Start
			for(Id unitId : mapUnitIdWithLeaseId.keySet()){
				if(!mapLeaseIdWithObj.containsKey(mapUnitIdWithLeaseId.get(unitId)) && mapUnitIdWithSREmail.containsKey(unitId)){
					Lease__c newLease = new Lease__c();
					newLease.Id = mapUnitIdWithLeaseId.get(unitId);
					newLease.Customer_Email__c = mapUnitIdWithSREmail.get(unitId);
					mapLeaseIdWithObj.put(newLease.Id,newLease);
				}
			}
			if(!mapLeaseIdWithObj.isEmpty()){
				update mapLeaseIdWithObj.values();	
			}
			//v2.0 - End
		}
	}	
}