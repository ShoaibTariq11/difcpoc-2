trigger CRM_TaskTrigger on Task (before insert, after insert, before Update) {

    if(Trigger.isBefore){
        CRM_cls_KPI_Utils.updateActivityDetails( Trigger.new, true, true, Trigger.isInsert);    
    } 
    
    if(Trigger.isAfter){
        CRM_cls_KPI_Utils.updateActivityDetails( Trigger.new, true, false, false);    
    }
}