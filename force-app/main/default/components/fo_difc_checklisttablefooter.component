<apex:component layout="none">
    
    <apex:attribute name="noteClass" description="This is any class which will override the styles of the footer note."
                    type="String" required="false"/>
    <apex:attribute name="signatureClass" description="This is any class which will override the styles of the footer signature."
                    type="String" required="false"/>
    <apex:attribute name="rowClass" description="This is any class which will override the styles of the footer row."
                    type="String" required="false"/>
    <apex:attribute name="columnLength" description="This tells how many columns the footer will occupy"
                    type="String" required="false" default="5" />
    <apex:attribute name="pageNumber" description="This will tell the current page in the PDF"
                    type="String" required="false" default="1" />
    <apex:attribute name="lastPage" description="This will tell the last page in the PDF"
                    type="String" required="false" default="2" />
    <apex:attribute name="updateDate" description="This will contain the revision date of the checklist"
                    type="String" required="false" default="01 June 2016" />
    <apex:attribute name="refNum" description="This will store the revision reference number of the checklist template"
                    type="String" required="false" default="DIFCP-PM-FT-FO-45 Rev. 04" /> 
    <apex:attribute name="controlDate" description="This will store the date of the checklist template"
                    type="String" required="false" default="07/06/2016 15:04 " />                     
                    
    <tr>
        <td colspan="{!columnLength}" class='difc-footer-row {!rowClass}'> 
            
            <div class="difc-footer--new" >
        
                <div class="difc-footer-note {!noteClass}">
                    CONFIDENTIALITY NOTICE and DISCLAIMER – This document and any attachment are confidential and may be privileged or otherwise protected from disclosure and solely for the use of Dubai International Financial Centre Authority. No part of this document may be copied, reproduced, or transmitted in any form or by any means without written permission.
                </div>
                
                <div class="difc-footer-signature difc-footer-signature--row {!signatureClass}" >
                    
                    <table class="difc-footer-table" border="0" style="margin-top: 0.36em">
                        
                        <tr>
                            
                            <td class="difc-footer-columns" style="padding-top: 5px">{!refNum}</td>
                            <td class="difc-footer-columns" style="padding-top: 5px">Page {!pageNumber} of {!lastPage}</td>
                            <td class="difc-footer-columns" style="padding-top: 5px">Updated: {!updateDate}</td>
                            <td class="difc-custom-column" style="text-align: center; padding-top: 5px">{!controlDate} <br/> Uncontrolled copy if printed</td>
                            
                        </tr>
                        
                    </table>
                    
                </div>
                
            </div>
        </td>
    </tr>

</apex:component>