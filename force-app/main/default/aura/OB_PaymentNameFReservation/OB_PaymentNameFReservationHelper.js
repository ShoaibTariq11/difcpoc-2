({
    thirdpartyClientCaller : function(component,cardTokenn) { 
        console.log('==thirdpartyClientCaller===');
        var xmlHttp = new XMLHttpRequest();
        let newURL = new URL(window.location.href).searchParams;
        var email= component.get("v.email");
        var amount = newURL.get('amountToPay');//
        component.set("v.amountToPay",amount);
        //var amount = 200;
        
        var url = 'https://sandbox.checkout.com/api2/v2/charges/token';
        xmlHttp.open("POST", url, true);
        var cardToken = cardTokenn;
        xmlHttp.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
        xmlHttp.setRequestHeader('Authorization', 'sk_test_3df560dc-86d5-48f4-af36-20e9be61eada');
        
        var jsonBody = '{"cardToken":"'+cardTokenn+'","currency":"AED","email":"'+email+'","value":"'+amount+'","autoCapture":"n","chargeMode":"2"}';
        xmlHttp.responseType = 'text';
        
        xmlHttp.onload = function () {
            console.log('==ready==='+xmlHttp.readyState);
            console.log('==status=='+xmlHttp.status);
            if (xmlHttp.readyState === 4) {
                console.log(xmlHttp.response);   
                var json = xmlHttp.response;
                var parsed = JSON.parse(json);
                component.set("v.listings", parsed);
                console.log("response inside the attribute listings "+JSON.stringify(component.get("v.listings"))); 
                var a = component.get("v.listings");
                
                var params = 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=600,height=300,left=100,top=100';
                
                var newWindow =  window.open(a[0].redirectUrl,'test',params);
                var payToken = a[0].id;
                
                var myVar = setInterval(function myTimer(){
                    if(newWindow.closed){  
                        console.log('inside closed');
                        var b = component.get("v.listings");
                        console.log('b is'+JSON.stringify(b));
                        var payTokenfun = b[0].id;
                        console.log('payToken inside timer function '+ payTokenfun);
                        checkout(component,payTokenfun);
                        clearInterval(myVar);
                    }                  
                    
                },500,component,newWindow,myVar);
                
                
                function checkout(component,payTokenfun){
                    console.log('inside checkpayment method');
                    var xmlHttp = new XMLHttpRequest();
                    var url = 'https://sandbox.checkout.com/api2/v2/charges/'+payTokenfun;
                    console.log('payment endpoint is '+url);
                    xmlHttp.open("GET", url, true);
                    xmlHttp.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
                    xmlHttp.setRequestHeader('Authorization', 'sk_test_3df560dc-86d5-48f4-af36-20e9be61eada');
                    xmlHttp.responseType = 'text';
                    console.log(xmlHttp);
                    xmlHttp.onload = function () {
                        console.log('payment ready state '+xmlHttp.readyState);
                        console.log('payment response status '+xmlHttp.status);
                        if (xmlHttp.readyState === 4) {  
                            if (xmlHttp.status === 200) {
                                console.log('the payment response is '+xmlHttp.response);
                                var jsonPay = xmlHttp.response;
                                var parsedPay = JSON.parse(jsonPay);
                                component.set("v.payResp", parsedPay);
                                var respPay = component.get("v.payResp");
                                var transactionId = respPay[0].id;
                                alert('transactionId==>'+transactionId);
                                var respStatus = respPay[0].responseMessage;
                                var cvvCheck = respPay[0].card.cvvCheck;
                                var EmailGen= respPay[0].email;
                                var amountPaid = respPay[0].value;
                                var last4= respPay[0].last4;
                                console.log('respStatus is '+respStatus);
                                console.log('cvvCheck is '+cvvCheck);
                                if(respStatus=="Approved"){
                                    console.log("inside response Approved");
                                    var paymentForm  = document.getElementById('payment-form');
                                    // paymentForm.submit();
                                    
 /* ============================================Submission of approval when payment is completed===================================================== */
                                    console.log("transactionId is "+transactionId);
                                    var action = component.get("c.submitForNameApprovalUpdateTransId");   
                                    action.setParams({
                                        transactionId:transactionId,
                                        srId : newURL.get('srId')
                                    });
                                    action.setCallback(this, function(response) {
                                        var retVal = response.getReturnValue();
                                        var state= response.getState();
                                        if(state=="SUCCESS"){
                                        console.log("inside success submit for name submitForApproval");
                                            }
                                    });
                                    $A.enqueueAction(action); 
/* ========================================================================================================================================= */
                                    
                                  var urlEvent = $A.get("e.force:navigateToURL");
                                    urlEvent.setParams({
                                        "url": "/payment-receipt/?amountPaid="+ amountPaid+"?EmailGen="+EmailGen+"?last4="+last4
                                    });
                                    urlEvent.fire();                        
                                }
                                else{
                                    if(cvvCheck=="N"){
                                        component.set("v.ErrorRespCVV","Authorising entity has not attempted card verification or could not verify the CVD due to a security device error"); 
                                    }
                                    if(cvvCheck=="D"){
                                        component.set("v.ErrorRespCVV","Card Verification was done and CVD was invalid");
                                    }
                                    if(cvvCheck=="P"){
                                        component.set("v.ErrorRespCVV","Card Verification not performed, CVD was not on the card. Not all cards have a CVD value encoded");
                                    }
                                    if(cvvCheck=="U"){
                                        component.set("v.ErrorRespCVV","The Issuer has not certified, or has not provided the encryption keys, to the interchange");
                                    }
                                    if(cvvCheck=="X"){
                                        component.set("v.ErrorRespCVV","No CVV2 information available");
                                    }
                                    else{
                                        component.set("v.ErrorResp",respPay[0].responseMessage);
                                    }
                                    component.set("v.isOpen", true);
                                }
                            }
                            else{
                                component.set("v.ErrorResp","The Window is closed either by giving wrong password/no password.");
                                component.set("v.isOpen", true);
                            }
                        }
                        else{
                            component.set("v.ErrorResp","Service down");
                            component.set("v.isOpen", true);
                        }
                    };   
                    xmlHttp.send(null);
                }
            }
        };
        
        xmlHttp.send(jsonBody);
        
        /*Executed first*/
        console.log("Request sent");        
    },
    
    fetchPickListVal: function(component, fieldName, elementId) {
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.CompanyName"),
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                console.log("element id is "+elementId);
                console.log("dom of element id is "+component.find(elementId));
                component.set("v.countryList", opts);
            }
        });
        $A.enqueueAction(action);
    },


    
})