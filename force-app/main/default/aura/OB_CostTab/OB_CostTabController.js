({
  init: function(component, event, helper) {
    component.set("v.totaltoPay", 0);
    helper.getSRHelper(component, event, helper);
  },

  navToPament: function(component, event, helper) {
    var totalCost = component.get("v.totalCost");
    /* console.log(component.get("v.costItemsList"));
    var costItemsAdded = component.get("v.costItemsList");
    const costItemsIds = costItemsAdded.map(costItem => costItem.id);
    console.log(costItemsIds); */

    if (totalCost > 0) {
      window.location.href =
        "topupbalance/?srId=" + component.get("v.relSRselected");
      /* component.set("v.ShowCostTab", false);
      try {
        $A.createComponent(
          "c:DIFC_TopUpBalance",
          {
            amount: totalAmt,
            itemCode: costItemsIds
          },
          function(newButton, status, errorMessage) {
            //Add the new button to the body array
            if (status === "SUCCESS") {
              var body = component.get("v.body");
              body.push(newButton);
              component.set("v.body", body);
            } else if (status === "INCOMPLETE") {
              console.log("No response from server or client is offline.");
              // Show offline error
            } else if (status === "ERROR") {
              console.log("Error: " + errorMessage);
              // Show error message
            }
          }
        );
      } catch (error) {
        console.log(error.message);
        console.log(error.stackTrace);
      } */
    } else {
      var toastEvent = $A.get("e.force:showToast");
      toastEvent.setParams({
        mode: "dismissable",
        message: "Please select amount greater than 0.",
        type: "error",
        duration: 500
      });
      toastEvent.fire();
    }

    // var totalAmt = component.get("v.totaltoPay");

    // if (totalAmt > 0) {
    //   window.location.href = "topupbalance/?amountToPay=" + totalAmt;
    // } else {
    //   var toastEvent = $A.get("e.force:showToast");
    //   toastEvent.setParams({
    //     mode: "dismissable",
    //     message: "Please select amount greater than 0.",
    //     type: "error",
    //     duration: 500
    //   });
    //   toastEvent.fire();
    // }
  },

  payItemClick: function(component, event, helper) {
    var costItemList = [];
    var index = event.currentTarget.dataset.index;
    var srId = event.currentTarget.dataset.srid;
    console.log(srId);
    component.set("v.relSRselected", srId);
    const srWrap = component.get("v.srItemsWrap");
    console.log(index);
    console.log(srWrap[index]);
    var totalPrice = 0;
    for (var costItem of srWrap[index].srPriceItemList) {
      totalPrice += costItem.Total_Price_USD__c;
      var priceobj = {
        name: costItem.Name,
        productname: costItem.Pricing_Line_Name__c,
        cost: costItem.Total_Price_USD__c,
        id: costItem.Id
      };
      costItemList.push(priceobj);
    }
    console.log(totalPrice);
    component.set("v.totalCost", totalPrice);
    component.set("v.costItemsList", costItemList);

    /* var costItemsList = component.get("v.costItemsList");
    var totalCost = component.get("v.totaltoPay");

    var priceItemObj = event.currentTarget.dataset;
    var priceItemName = priceItemObj.name;
    var priceItemCost = parseFloat(priceItemObj.cost);
    var priceItemId = priceItemObj.id;

    var isExistInList = false;

    if (costItemsList.length > 0) {
      for (var costItem of costItemsList) {
        console.log(costItem.id);
        if (costItem.id == priceItemId) {
          isExistInList = true;
          break;
        }
      } 
    }

    console.log(isExistInList);
    if (!isExistInList || costItemsList.length == 0) {
      var priceobj = {
        name: priceItemName,
        cost: priceItemCost,
        id: priceItemId
      };
      costItemsList.push(priceobj);

      component.set("v.costItemsList", costItemsList);
      component.set(
        "v.totaltoPay",
        component.get("v.totaltoPay") + priceItemCost
      );
    }*/
  },

  removePriceIteam: function(component, event, helper) {
    try {
      var costItemsList = component.get("v.costItemsList");
      var totalCost = component.get("v.totalCostItem");

      var priceItemObj = event.currentTarget.dataset;
      var priceItemCost = parseFloat(priceItemObj.cost);
      var priceItemId = priceItemObj.id;

      for (var costItem of costItemsList) {
        if (costItem.id == priceItemId) {
          var index = costItemsList.indexOf(costItem);
          console.log(index);
          component.set(
            "v.totaltoPay",
            component.get("v.totaltoPay") - priceItemCost
          );
          costItemsList.splice(index, 1);
          component.set("v.costItemsList", costItemsList);
        }
      }
    } catch (err) {
      console.log(err.message);
    }
  },

  paytTotalClick: function(component, event, helper) {
    console.log(component.get("v.srItemsWrap"));
    var costItemsList = component.get("v.costItemsList");
    var srWrap = component.get("v.srItemsWrap");
    costItemsList = [];

    for (var srObj of srWrap) {
      for (var costItem of srObj.srPriceItemList) {
        if (costItem.HexaBPM__Status__c == "Added") {
          var priceobj = {
            name: costItem.HexaBPM__Product__r.Name,
            cost: costItem.Total_Price_USD__c,
            id: costItem.Id
          };
          costItemsList.push(priceobj);
        }
      }
    }
    component.set("v.costItemsList", costItemsList);
    component.set("v.totaltoPay", component.get("v.totalCost"));

    /*var costItemsList = component.get("v.costItemsList");
        costItemsList = [];
        var priceobj = {
                name: 'Pay All' ,
                cost: component.get("v.totalCost"),
            	id: 'Pay All'
            }
            costItemsList.push(priceobj);

            component.set("v.costItemsList",costItemsList); 
            component.set("v.totaltoPay",component.get("v.totalCost"));
            */
  }
});