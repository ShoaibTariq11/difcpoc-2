({
	resendEmail : function(component, event, helper) {
        var staticLabel = $A.get("$Label.c.OB_Site_Prefix");
		//var nextPageURL = 'https://dcob1-difcportal.cs100.force.com/digitalOnboarding/forgotPassworddifc';
       var nextPageURL =  '/'+staticLabel+'/s/ob-forgotpasswordconfirm';
        window.location.href = nextPageURL;
	},

    returnToLogin : function(component, event, helper) {
		  var staticLabel = $A.get("$Label.c.OB_Site_Prefix");
        var nextPageURL = '/'+staticLabel+'/s/login';
        window.location.href = nextPageURL;
	}
})