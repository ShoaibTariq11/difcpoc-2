({
    //Getting the value of Entity type, Legal structure, Are you setting up a, Legal type of entity and Sector and showing the ends with
    //based on the values. 
	loadEndsWith: function(component,event,helper){
               let newURL = new URL(window.location.href).searchParams;
        console.log("inside getEntityDetails");
        var action = component.get("c.getEntityDetails");
        action.setParams({
            srId : newURL.get('srId')
        })
        action.setCallback(this,function(response){
            var returnValueCompName = response.getReturnValue();
            var state= response.getState();
            console.log("getEntityDetails comp name entity is "+JSON.stringify(returnValueCompName));
            console.log("state getEntityDetails comp name entity is "+state);
            if(state=="SUCCESS"){
                if(returnValueCompName && returnValueCompName!='undefined'){
                    component.set("v.EntityDetails",returnValueCompName);
                    var legalStructure= component.get("v.EntityDetails[0].legal_structures__c");
                    var NewBranchTransfer = component.get("v.EntityDetails[0].Setting_Up__c");
                    var BDSector=component.get("v.EntityDetails[0].Business_Sector__c");
                    var typeOfentity=component.get("v.EntityDetails[0].Type_of_Entity__c");
                    var Sector_NPIO = $A.get("$Label.c.Sector_NPIO");
                    var SetUpBranch = $A.get("$Label.c.SetUpBranch");
                    var SetUpTransfer = $A.get("$Label.c.SetUpTransfer");
                    component.set('v.LegalStructure'+legalStructure);
                    console.log("type of entity is "+ typeOfentity);
                    /* --------------------------------------------------------------------------------------------------------------------------*/
               /*     console.log("legalStructure comp name entity is "+legalStructure);
                    if(legalStructure=="Company"){
                        component.set("v.typeOfEntity","Company");
                    }
                    else if(legalStructure=="Partnership"){
                        component.set("v.typeOfEntity","Partnership");  
                    }
                        else if(legalStructure=="Foundation"){
                            component.set("v.typeOfEntity","Foundation");
                        }
                            else{
                                component.set("v.typeOfEntity","Entity");
                            } */
                    /* --------------------------------------------------------------------------------------------------------------------------*/
                  /*  if(NewBranchTransfer=="New"){
                        component.set("v.areYouEntity","Proposed Entity");
                    }
                    else if(NewBranchTransfer=="Branch"){
                        component.set("v.areYouEntity","Recognized Entity");  
                    }
                        else if(NewBranchTransfer=="Transfer"){
                            component.set("v.areYouEntity","Transferred Entity");
                        }*/
                    /* ----------------------------------------------------------------------------------------------------------------------------*/
                    
                    if(BDSector=="Special Purpose Company" || BDSector=="Non Profit Incorporated Organisation (NPIO)" || BDSector=="Intermediate Special Purpose Vehicle" || BDSector=="Foundation" ){
                        component.set("v.showTradingName","false"); 
                    }
                    /* ----------------------------------------------------------------------------------------------------------------------------*/
                    if(NewBranchTransfer==SetUpTransfer || NewBranchTransfer==SetUpBranch) {
                        component.set("v.showEndsWith","false");
                    }
                    /* ----------------------------------------------------------------------------------------------------------------------------*/
                    var endsWithOptions=[];
                    if(typeOfentity=="Private" && BDSector!=Sector_NPIO){
                        if(BDSector=="Special Purpose Company"){
                            endsWithOptions.push({
                                class: "optionClass",
                                label: "SPC Limited",
                                value: "SPC Limited"
                            });
                            
                            endsWithOptions.push({
                                class: "optionClass",
                                label: "SPC Ltd",
                                value: "SPC Ltd"
                            });
                            component.set("v.endsWithList",endsWithOptions);
                            console.log("endsWithList Private and SPCis "+component.get("v.endsWithList"));   
                        }
                        else {
                            endsWithOptions.push({
                                class: "optionClass",
                                label: "Limited",
                                value: "Limited"
                            });
                            
                            endsWithOptions.push({
                                class: "optionClass",
                                label: "Ltd",
                                value: "Ltd"
                            });
                            component.set("v.endsWithList",endsWithOptions);
                            console.log("endsWithList Privateis "+component.get("v.endsWithList"));
                        }
                        
                    }
                    
                    if(BDSector==Sector_NPIO){
                        endsWithOptions.push({
                            class: "optionClass",
                            label: "Non-profit Incorporated Organisation",
                            value: "Non-profit Incorporated Organisation"
                        });
                        
                        endsWithOptions.push({
                            class: "optionClass",
                            label: "NPIO",
                            value: "NPIO"
                        });
                        component.set("v.endsWithList",endsWithOptions);
                        console.log("endsWithList NPIO is "+JSON.stringify(component.get("v.endsWithList")));
                    }
                    
                }
            }
        });
        $A.enqueueAction(action);    
    },
    
    //Autosave functionality
    updateEndsWith: function(component,event,helper){
        var CompanyNameId = component.get("v.CompanyNameSecond.Id");
        var action= component.get("c.updateEndsWithApex");
        
        action.setParams({
            CompanyNameId:CompanyNameId,
            EndsWith:component.get("v.CompanyNameSecond.Ends_With__c")
        })
        
        action.setCallback(this,function(response){
            var returnValueEndsWith = response.getReturnValue();
            var state= response.getState();
            if(state=="SUCCESS"){
                console.log("inside success updateEndsWith dynamic comp");
            }
        });
        $A.enqueueAction(action); 
        
    },
    
    //Change the words Company, Partnership, Foundation and Entity according to Legal structure.
    getEntityDetails:function(component,event,helper){
        let newURL = new URL(window.location.href).searchParams;
        console.log("inside getEntityDetails");
        var action = component.get("c.getEntityDetails");
        action.setParams({
            srId : newURL.get('srId')
        })
        action.setCallback(this,function(response){
            console.log(response);
            var returnValueCompName = response.getReturnValue();
            var state= response.getState();
           
            console.log("state getEntityDetails comp name entity is "+state);
            console.log(+JSON.stringify(returnValueCompName));
            if(state=="SUCCESS"){
                if(returnValueCompName && returnValueCompName!='undefined'){
                    component.set("v.EntityDetails",returnValueCompName);
                    var LegalStructure= component.get("v.EntityDetails[0].legal_structures__c");
                    var BDSector=component.get("v.EntityDetails[0].BD_Sector__c");
                    
                    var NewBranchTransfer = component.get("v.EntityDetails[0].Are_you_setting_up_a__c");
                    var typeOfentity=component.get("v.EntityDetails[0].Type_of_Entity__c");
                    var Sector_NPIO = $A.get("$Label.c.Sector_NPIO");
                    var SetUpBranch = $A.get("$Label.c.SetUpBranch");
                    var SetUpTransfer = $A.get("$Label.c.SetUpTransfer");
/* ------------------------------------------------------------------------------------------------------------------------------------------------*/
                    console.log("LegalStructure comp name entity is "+LegalStructure);
                    if(LegalStructure){
                        if(LegalStructure=="Company"){
                            component.set("v.LegalStructure","Company");
                        }
                        else if(LegalStructure=="Partnership"){
                        component.set("v.LegalStructure","Partnership");  
                        }
                        else if(LegalStructure=="Foundation"){
                            component.set("v.LegalStructure","Foundation");
                        }
                        else{
                            component.set("v.LegalStructure","Entity");
                        }
                    }
 /* -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
                    if(BDSector=="Special Purpose Company" || 
                       BDSector=="Non Profit Incorporated Organisation (NPIO)" || 
                       BDSector=="Intermediate Special Purpose Vehicle" || 
                       BDSector=="Foundation" ){
                        component.set("v.showTradingName","false")             
                    }
                    
/* --------------------------------------------------------------------------------------------------------------------------*/
                    if(NewBranchTransfer==SetUpTransfer || NewBranchTransfer==SetUpBranch) {
                        component.set("v.showEndsWith","false");
                    }
 /* ----------------------------------------------------------------------------------------------------------------------------*/
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);     
    }
    
    
})