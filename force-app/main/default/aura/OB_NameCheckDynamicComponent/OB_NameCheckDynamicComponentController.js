({
    //Logic written to fire onload of the component.
    doInit:function(component, event, helper) {
        helper.loadEndsWith(component,event,helper);
        helper.getEntityDetails(component,event,helper);
    },
    
    //Navigate to Search page clicked on Entity name field. The search page is where Naming error evaluation happens
    navigateToSearch: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        var nameToBeSent = component.get("v.CompanyNameSecond.Entity_Name__c");
        var nameId= component.get("v.CompanyNameSecond.Id");
        let newURL = new URL(window.location.href).searchParams;
        var srId = newURL.get('srId');
        urlEvent.setParams({
            "url": "/ob-namecheck/?valueTextEntity="+nameToBeSent+"&NameId="+nameId+"&srId="+srId+"&pageId="+newURL.get('pageId')+"&flowId="+newURL.get('flowId')
        });
        urlEvent.fire();
    },
    
    //Navigate to Search page clicked on Trading name field. The search page is where Naming error evaluation happens
    navigateToTradingSearch: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        var nameToBeSent = component.get("v.CompanyNameSecond.Trading_Name__c");
        var nameId= component.get("v.CompanyNameSecond.Id");
        let newURL = new URL(window.location.href).searchParams;
        var srId = newURL.get('srId');
        console.log("nameToBeSent navigateToTradingSearch dynamic comp is "+nameToBeSent);
        urlEvent.setParams({
            "url": "/ob-tradingnamecheck/?valueTextEntity= " +nameToBeSent+"&NameId="+nameId+"&srId="+srId+"&pageId="+newURL.get('pageId')+"&flowId="+newURL.get('flowId') 
        });
        urlEvent.fire();
    },
    
    //Progress bar when cursor is placed inside a field. To move the progress bar to 'In Progress'
    indicatorOnFocus : function(component, event, helper){
        var currentElement = event.getSource().get('v.name');
        var currentIndex= currentElement[currentElement.length -1];
        component.set("v.currentFieldRank",currentIndex);
        
        console.log("currentElement@@@"+currentElement);
        var currentli = document.getElementById(currentElement);
        console.log("currentli"+currentli);
        var currentImgSpan = document.getElementById(currentElement+"-span");
        console.log("currentImgSpan"+currentImgSpan);
        if(currentli.classList.contains('slds-is-complete')){
            currentli.classList.remove('slds-is-complete');
            currentImgSpan.classList.add('hideSpan');
        }
        currentli.classList.add('slds-is-active');
    },
    
    //Progress bar when moused out of a field.To move the progress bar to Completed/Not Started
    indicatorOnBlur : function(component,event,helper){
        var currentElement = event.getSource().get('v.name');
        var currentval = event.getSource().get('v.value');
        var currentli = document.getElementById(currentElement);
        currentli.classList.remove('slds-is-active');
        var currentImgSpan = document.getElementById(currentElement+"-span");
        var sectionActive = document.getElementById("NCsectionActive");
        var sectionComplete = document.getElementById("NCsectionComplete");
        
        if(currentval == ""){
            currentli.classList.remove('slds-is-complete');
            currentImgSpan.classList.add('hideSpan');  
            sectionComplete.classList.add('showHide');
            sectionActive.classList.remove('showHide');
            
        }else{
            currentli.classList.add('slds-is-complete');
            currentImgSpan.classList.remove('hideSpan');
        }
    },
    
    //Deleting the name by destroying the Lightning component
    componentDestroy: function(component, event, helper) {
        console.log("inside componentDestroy");
        var accessDynamicCompNum = document.getElementsByClassName("dynamicComp");
        var lengthaccessDynamicCompNum = accessDynamicCompNum.length;
        
        if(component.get("v.CompanyNameSecond.Entity_Name__c") && component.get("v.CompanyNameSecond.Trading_Name__c")  && component.get("v.CompanyNameSecond.Ends_With__c")){
            if(lengthaccessDynamicCompNum!=1){
                var CompanyNameId = component.get("v.CompanyNameSecond.Id");
                console.log("id to be deleted "+CompanyNameId);
                var action = component.get("c.DeleteCompanyName");
                action.setParams({
                    CompanyNameId:CompanyNameId
                })
                
                   action.setCallback(this,function(response){
                    var state=response.getState();
                    console.log("state is componentDestroy dynamic comp"+state);
                    var retValue= response.getReturnValue();
                    console.log("retvalue componentDestroy dynamic comp is "+retValue);
                    
                       if(state=="SUCCESS" && retValue.includes('SUCCESS')){
                           component.find("destroyComp").destroy();
                           var toastEvent = $A.get("e.force:showToast");
                           toastEvent.setParams({
                               "title": "Success!",
                               "message": "The Name is deleted"
                           });
                           toastEvent.fire(); 
                       }
                       
                       else if(retValue.includes('ENTITY_IS_LOCKED')){
                           var toastEvent = $A.get("e.force:showToast");
                           toastEvent.setParams({
                               "title": "Error!",
                               "message": "The Name is be submitted for approval and hence cannot be deleted",
                               "type":"error"
                           });
                           toastEvent.fire(); 
                       }
                           else{
                               var toastEvent = $A.get("e.force:showToast");
                               toastEvent.setParams({
                                   "title": "Error!",
                                   "message": "There seems to be an issue in deleting the names. Please try again later",
                                   "type":"error"
                               });
                               toastEvent.fire();  
                           }
                }) 
                $A.enqueueAction(action);
            }
            else if(lengthaccessDynamicCompNum==1){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Alert!",
                    "message": "You must atleast add One name to proceed"
                });
                toastEvent.fire();
            }
        }
        else
        {
            // delete the block if the Entity name, trading name & ends with value is empty without deleting the backend
            if(lengthaccessDynamicCompNum!=1){
                component.find("destroyComp").destroy();
            }
            else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Alert!",
                    "message": "You must atleast add One name to proceed"
                });
                toastEvent.fire(); 
            }    
        }
    },
    
    //Progress bar for Picklist fields(Ends with)
    updateName:function(component,event,helper){
        var currentElement = event.getSource().get('v.name');
        var currentval = event.getSource().get('v.value');
        var currentli = document.getElementById(currentElement);
        currentli.classList.remove('slds-is-active');
        var currentImgSpan = document.getElementById(currentElement+"-span");
        var sectionActive = document.getElementById("NCsectionActive");
        var sectionComplete = document.getElementById("NCsectionComplete");
        
        if(currentval == ""){
            currentli.classList.remove('slds-is-complete');
            currentImgSpan.classList.add('hideSpan');  
            sectionComplete.classList.add('showHide');
            sectionActive.classList.remove('showHide');
            
        }else{
            currentli.classList.add('slds-is-complete');
            currentImgSpan.classList.remove('hideSpan');
        }
         
         if(component.get("v.CompanyName.Entity_Name__c") != "" && component.get("v.CompanyName.Ends_With__c") != ""){
             console.log('conditions passed');
             var sectionActive = document.getElementById("NCsectionActive");
             var sectionComplete = document.getElementById("NCsectionComplete");
             sectionActive.classList.add('showHide');
             sectionComplete.classList.remove('showHide');
         }
         helper.updateEndsWith(component,event,helper);
     }
    
})