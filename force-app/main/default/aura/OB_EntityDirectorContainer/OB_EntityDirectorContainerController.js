({
  doInit: function (cmp, event, helper) {
    try {
      var getFormAction = cmp.get("c.getExistingAmendment");
      let newURL = new URL(window.location.href).searchParams;
      var srIdParam = newURL.get("srId")
        ? newURL.get("srId")
        : cmp.get("v.srId");
      var pageId = newURL.get("pageId")
        ? newURL.get("pageId")
        : cmp.get("v.pageId");
      var flowId = newURL.get("flowId")
        ? newURL.get("flowId")
        : cmp.get("v.flowId");
      cmp.set("v.srId", srIdParam);
      console.log("in init -->" + srIdParam);

      var reqWrapPram = {
        srId: srIdParam,
        pageId: pageId,
        flowId: flowId,
      };

      getFormAction.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram),
      });

      console.log(
        "@@@@@@@@@@33121 reqWrapPram init " + JSON.stringify(reqWrapPram)
      );
      getFormAction.setCallback(this, function (response) {
        var state = response.getState();
        console.log("callback state:... " + state);

        if (cmp.isValid() && state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log("### respWrap", respWrap);

          cmp.set("v.srWrap", respWrap.srWrap);
          cmp.set("v.commandButton", respWrap.ButtonSection);
          cmp.set("v.amedShrHldrWrapLst", respWrap.amedShrHldrWrapLst);
          console.log("### shreholder");
          console.log("### valuse ", respWrap.srWrap.amedWrapLst);

          //for auto scolling
          var activeScreen = document.getElementById("activePage");
          console.log("id of active screen ------>" + activeScreen);
          if (activeScreen) {
            activeScreen.scrollIntoView({
              block: "start",
              behavior: "smooth",
            });
          }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(getFormAction);
    } catch (err) {
      console.log("=error===" + err.message);
    }
  },

  handleComponentEventClear: function (cmp, evt, helper) {
    cmp.set("v.showButton", false);
    //cmp.set("v.showForm", false);
    //cmp.set("v.selectedValue", "");
    cmp.set("v.amedSelWrap", null);
  },

  handleComponentEvent: function (cmp, event, helper) {
    try {
      // get the selected License record from the COMPONETNT event
      var selectedLicenseGetFromEvent = event.getParam("recordByEvent");
      console.log("selected in grandparent");
      console.log(JSON.parse(JSON.stringify(selectedLicenseGetFromEvent)));

      //component.set("v.selectedRecord", selectedLicenseGetFromEvent);

      cmp.set("v.amedSelWrap", selectedLicenseGetFromEvent);
      cmp.set("v.showButton", true);
      //cmp.set("v.amedIdInFocus", "none");
    } catch (error) {
      console.log(error.message);
    }
  },

  handleRefreshEvnt: function (cmp, event, helper) {
    var amedId = event.getParam("amedId");
    console.log("$$$$$$$$$$$ refresh ", amedId);
    cmp.set("v.amedId", amedId);
    cmp.set("v.amedSelWrap", []);
    cmp.set("v.showButton", false);
    $A.enqueueAction(cmp.get("c.doInit"));
  },
  onChange: function (cmp, evt, helper) {
    var amedIdParam = cmp.find("select").get("v.value");

    var amedShrHldrWrapLst = cmp.get("v.srWrap").amedShrHldrWrapLst;
    var amedSelWrap = amedShrHldrWrapLst[amedIdParam];
    /*
    for (let i = 0; i < amedShrHldrWrapLst.length; i++) {
      var amedWrap = amedShrHldrWrapLst[i];
      if (amedWrap.amedObj.Id == amedIdParam) {
        amedSelWrap = amedWrap;
        break;
      }
    }
    */
    console.log("########### amedSelWrap ", amedSelWrap);
    if (amedSelWrap) {
      cmp.set("v.showButton", true);
      cmp.set("v.amedSelWrap", amedSelWrap);
    }
  },
  openForm: function (cmp, evt, helper) {
    cmp.set("v.showForm", true);
    /*
        try{
    	var getFormAction = cmp.get("c.getConvertToDirector");
        var amendmentID =  cmp.get('v.selectedValue');  
        var srIdParam =  (  cmp.get('v.srId') ?  cmp.get('v.srId') : newURL.get('srId'));
            console.log(amendmentID+'===srIdParam==='+srIdParam); 
        var reqWrapPram  =
            {
                srId: srIdParam,
                amendmentID: amendmentID
            }
        getFormAction.setParams({
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
            
        
        getFormAction.setCallback(this, 
			function(response) {
           		var state = response.getState();
                
                if (cmp.isValid() && state === "SUCCESS") 
                {
                    var respWrap = response.getReturnValue();
                    //refresh here.
                    $A.enqueueAction(cmp.get('c.doInit'));
                }
            }
		);
        $A.enqueueAction(getFormAction);
        
        }catch(err)
        {
           
            console.log('=error==='+err.message);
            
        }
        */
  },

  handleButtonAct: function (component, event, helper) {
    try {
      let newURL = new URL(window.location.href).searchParams;

      var srID = newURL.get("srId");
      var pageID = newURL.get("pageId");
      var buttonId = event.target.id;
      console.log(buttonId);
      var action = component.get("c.getButtonAction");

      action.setParams({
        SRID: srID,
        pageId: pageID,
        ButtonId: buttonId,
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log("button succsess");

          console.log(response.getReturnValue());
          window.open(response.getReturnValue().pageActionName, "_self");
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  },
});