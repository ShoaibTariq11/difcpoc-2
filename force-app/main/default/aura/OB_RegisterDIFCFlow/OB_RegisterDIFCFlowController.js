({
  handleApplicationEvent: function(cmp, event, helper) {
    console.log("-page flow-inside handler-");

    var pageId = event.getParam("pageId");
    var srId = event.getParam("srId");
    var flowId = event.getParam("flowId");
    var isButton = event.getParam("isButton");

    console.log("-pageId- handler-" + pageId);
    console.log("-srID- handler-" + srId);

    // set the handler attributes based on event data

    cmp.set("v.pageId", pageId);
    cmp.set("v.srId", srId);
    cmp.set("v.flowId", flowId);
    if (isButton == true) {
      console.log("-flowId- handler-" + isButton);
      helper.getFormDetail(cmp, event);
    }
  },
  init: function(cmp, event, helper) {
    var x = document.getElementsByClassName("register-login-class")[0];
    console.log("==x===" + x);
    //x[0].classList.add("hide-xyz");
    helper.getFormDetail(cmp, event);
  },

  onRender: function(cmp, event, helper) {
    var activeScreen = document.getElementById("activePage");
    console.log("id of active screen ------>" + activeScreen);
    if (activeScreen) {
      activeScreen.scrollIntoView({
        block: "start",
        behavior: "smooth"
      });
    }
  }
});