({
	handleRecordType  : function(cmp, event, helper){
        var index = event.target.dataset.amedtype;
        console.log('###############@@@@@@@@@@'+ index);
        cmp.set("v.recordtypename", index);
        if(index == 'Individual') {
            cmp.set("v.isIndividual", true);
            cmp.set("v.isCorporate", false);
        } else {
            cmp.set("v.isCorporate", true);
            cmp.set("v.isIndividual", false);
        }
        
        var initAmendment = cmp.get('c.initAmendmentDB');
        var reqWrapPram  =
            {
                srId : cmp.get('v.srWrap').srObj.Id,
                recordtypename: cmp.get("v.recordtypename")
            };
        initAmendment.setParams(
            {
                 "reqWrapPram": JSON.stringify(reqWrapPram),
                 
            });
        
        initAmendment.setCallback(this, 
                                      function(response) {
                                          var state = response.getState();
                                          console.log("callback state: " + state);
                                          
                                          if (cmp.isValid() && state === "SUCCESS") 
                                          {
                                              
                                              var respWrap = response.getReturnValue();
                                              console.log('######### new uBO respWrap.amedWrap  ',respWrap.amedWrap);
											  //cmp.set('v.amedWrap',respWrap.amedWrap);	
											  //throw event. 
											                                              
											  var refeshEvnt = cmp.getEvent("refeshEvnt");
                                               console.log('@@@@@ event refeshEvnt ');
                                               refeshEvnt.setParams({
                                                    "isNewAmendment" : true,
                                                   "amedWrap":respWrap.amedWrap});
                                               
                                               refeshEvnt.fire();
                                          }
                                          else
                                          {
                                              console.log('@@@@@ Error '+response.getError()[0].message);
                                             
                                          }
                                      }
                                     );
            $A.enqueueAction(initAmendment);
            
        console.log(cmp.get("v.recordtypename"))
        
    },
   
})