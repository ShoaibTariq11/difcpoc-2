({
    init : function(cmp, event, helper) {
        cmp.set("v.customActions", [
            { label: "Custom action", name: "custom_action" }
        ]);
        
        cmp.set("v.srPriceItemColumns", [
            { label: "Price Item No", fieldName: "LinkName", type: "text" },
            { label: "Pricing Line", fieldName: "SRPriceLine_Text__c", type: "text" },
            { label: "Price (in AED)", fieldName: "Price__c", type: "number" },
            { label: "Price (in USD)", fieldName: "Price_in_USD__c", type: "number" },
            { label: "Status", fieldName: "Status__c", type: "text" },
            { label: "Created Date", fieldName: "CreatedDate", type: "date", typeAttributes:{ year: "numeric", month: "2-digit", day: "2-digit"} }
        ]);
        
        $A.createComponent("c:singleRelatedList", { 
            "currId": cmp.get("v.serviceRequestId"), 
            "sobjectApiName": "SR_Price_Item__c",
            "relatedFieldApiName": "ServiceRequest__c", 
            "sortedBy": "CreatedDate",
            "sortedDirection": "DESC",
            "fields": "Name, SRPriceLine_Text__c, Price__c, Price_in_USD__c, Status__c, CreatedDate",
            "columns": cmp.get('v.srPriceItemColumns')
        }, function(newCmp4, status, errorMessage) {
            if (status === "SUCCESS") {
                var firstPanel = cmp.get("v.firstPanel");
                firstPanel.push(newCmp4);
                cmp.set("v.firstPanel", firstPanel);
            }
        });
    }
})