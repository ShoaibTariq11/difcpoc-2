({
  init: function(component, event, helper) {
    /* var declare =
      "<p>I hereby consent on behalf of the abovementioned entity, to submit all forms related to the Registrar of Companies and Commissioner of Data Protection (“the Statutory Bodies”) electronically on the DIFC Client Portal." +
      "<div id='collapse' >I hereby confirm the following on behalf of the abovementioned entity:<br/>" +
      "<ul>" +
      "<li>1. that any forms lodged on the DIFC Client Portal can be used as if the original form was submitted</li>" +
      "<li>2. that the original form(s) submitted on the DIFC Client Portal shall be retained by the abovementioned entity as evidence of its authenticity, and that if a question of authenticity arises, the relevant Statutory Body may require submission of the original form(s)</li>" +
      "<li>3. the individual whose name is mentioned above is authorized by the abovemnetioned entity to file the applicable form(s) and submit any required document(s) on the DIFC Client Portal on its behalf.</li></ul><br/>" +
      "I understand that is is my responsibility to ensure that the abovementioned indivudal is duly autorized by the abovemnetioned entity to be a portal user and file the applicable form(s) and submit any required document(s) on the DIFC Client Portal on its behalf." +
      "I understand that it is an offence to provide information which is false, misleading or deceptive or to conceal information where the concealment of such information is likely to mislead or deceive.</div></p><a href='#collapse' class='nav-toggle'>Read More</a>";
    component.set("v.sObjectName", declare);
    console.log("===init==="); */

    var action = component.get("c.getConDetails");

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        console.log(response.getReturnValue());
        var respWrap = response.getReturnValue();
        component.set("v.respWrap", respWrap);

        component.set("v.currentUserWrap", respWrap.currentContact);
        if (respWrap.currentContact.Contact.Account.Building_Name__c) {
          component.set(
            "v.address",
            respWrap.currentContact.Contact.Account.Office__c +
              " ," +
              respWrap.currentContact.Contact.Account.Building_Name__c
          );
        } else {
          component.set(
            "v.address",
            respWrap.currentContact.Contact.Account.Office__c
          );
        }
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
    });
    $A.enqueueAction(action);
  },
  entityProfile: function(component, event, helper) {
    var urlEvent = $A.get("$Label.c.OB_SitePrefixURL") + "/apex/CompanyInfo";
    console.log(urlEvent);
    window.open(navURL);
    /*  urlEvent.setParams({
      url: urlEvent
    });

    urlEvent.fire(); */
  },

  navToReceipts: function(component, event, helper) {
    var navURL = $A.get("$Label.c.OB_viewReceiptsLink");

    var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
      url: navURL
    });
    urlEvent.fire();
  },

  navToProfile: function(component, event, helper) {
    window.location.href = "difc-entityprofile";
  },

  navToManageUser: function(component, event, helper) {
    /*  var navURL = $A.get("$Label.c.OB_ManageUserLink");
    console.log(navURL);

    window.open(navURL); */

    /* var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
      url: navURL
    });
    urlEvent.fire(); */

    window.location.href = "manage-users";
  }
});