({
  getPageContent: function (component, event, helper, isInit) {
    let newURL = new URL(window.location.href).searchParams;
    var srId = newURL.get("srId");
    var pageId = newURL.get("pageId");
    var action = component.get("c.loadPageContent");

    var requestWrap = {
      srId: srId,
      pageId: pageId
    };
    action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(respWrap);

        component.set("v.respWrap", respWrap);

        if (isInit == true) {
          helper.setCheckboxField(component, event, helper, respWrap);
        }
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
    });
    $A.enqueueAction(action);
  },

  showToast: function (component, event, helper, msg, type) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      mode: "dismissible",
      message: msg,
      type: type
    });
    toastEvent.fire();
  },

  setCheckboxField: function (component, event, helper, respWrap) {
    try {
      if (respWrap.srObj.Community_User_Roles__c) {
        var selcRolesList = respWrap.srObj.Community_User_Roles__c.split(";");

        for (var checkBOx of component.find("checkBox")) {
          if (selcRolesList.includes(checkBOx.get("v.value"))) {
            checkBOx.set("v.checked", true);
          }

          /* if (selcRolesList.includes("Super User")) {
            component.set("v.showSuperUser", true);
          } */
        }
        console.log(
          "selected Roles :" + " " + respWrap.srObj.Community_User_Roles__c
        );

        component.set(
          "v.rolesSelected",
          respWrap.srObj.Community_User_Roles__c
        );
      }
    } catch (error) {
      console.log(error.message);
    }
  }
});