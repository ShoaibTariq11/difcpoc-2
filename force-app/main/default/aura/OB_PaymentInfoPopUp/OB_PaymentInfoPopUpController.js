({
   
    closeModel: function(component, event, helper) 
    {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.isOpen", false);
    },
    cancel: function(component, event, helper) 
    {
        // redirection to home page.
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          url: "/"
        });
        urlEvent.fire();
    },
    doInit: function(component, event, helper) 
    {

        let newURL = new URL(window.location.href).searchParams;
        var srId = newURL.get('srId');
        var action = component.get("c.getCompanyNameInfo");
        var requestWrap = {
                                srId : srId       
                          };

        action.setParams({"requestWrapParam": JSON.stringify(requestWrap)});
        action.setCallback(this, function(response) 
        {
     
            var state = response.getState();
            if (state === "SUCCESS") {
                var respWrap = response.getReturnValue();
                console.log(respWrap);
                component.set('v.isOpen',respWrap.isOpen);
                component.set('v.accRec',respWrap.accRec);
            }else{
                console.log('@@@@@ Error '+response.getError()[0].message);
                console.log('@@@@@ Error Location '+response.getError()[0].stackTrace);
            }
        });
        $A.enqueueAction(action);

     }

  
   
   


})