({
     scriptsLoaded : function(component, event, helper) {
        var paymentForm  = document.getElementById('payment-form');
        var payNowButton = document.getElementById('pay-now-button');
        var cardTokenn;
        
        var style = {
            '.embedded .card-form .input-group': {
                borderRadius: '5px'
            },
            /* focus */
            '.embedded .card-form .input-group.focus:not(.error)': {
                border: '1px solid green'
            },
            /* icons */
            '.embedded .card-form .input-group .icon': {
                color: 'slategray'
            },
            /* hint icon (CVV) */
            '.embedded .card-form .input-group .hint-icon': {
                color: 'slategray'
            },
            '.embedded .card-form .input-group .hint-icon:hover': {
                color: 'darkslategray'
            },
            /* error */
            '.embedded .card-form .input-group.error': {
                border: '1px solid red'
            },
            '.embedded .card-form .input-group.error .hint.error-message': { /* message container */
                background: 'lightgray', /* make sure to match the pointer color (below) */
                color: 'red'
            },
            '.embedded .card-form .input-group.error .hint.error-message .arrow': { /* message container pointer */
                borderBottomColor: 'lightgray' /* matches message container background */
            },
            '.embedded .card-form .input-group.error .hint-icon:hover': { /* hint icon */
                color: 'red'
            },
            
            '.embedded .card-form .input-group:not(.error) input': {
                color: 'dimgray'
            },
            '.embedded .card-form .input-group.focus input': {
                color: 'black'
            },
            '.embedded .card-form .input-group.error input': {
                color: 'red'
            },
            
            '.embedded .card-form .split-view .left': {
                paddingRight: '3px'
            },
            '.embedded .card-form .split-view .right': {
                paddingLeft: '3px'
            }
        }
        
        Frames.init({
            
            publicKey: 'pk_test_69655dcd-598b-4d52-a240-81fcf4eaaf4a',
            
            containerSelector: '.frames-container',
            
            cardValidationChanged: function() {
                payNowButton.disabled = !Frames.isCardValid();
            },
            
            //https://docs.checkout.com/docs/shipping-and-billing-details
            
            billingDetails: {
                "addressLine1": component.get("v.addressLine1"),  //Address field line 1 — maximum length: 200 characters.
                "addressLine2": component.get("v.addressLine2"),       //Address field line 2 — maximum length: 200 characters.
                "postcode": component.get("v.postalCode"),                 //The city — maximum length: 50 characters.
                "country": component.get("v.city"),                       //The country — must be represented by an ISO2 country code (e.g. US).
                "city": component.get("v.state"),              //The city — maximum length: 50 characters.
                "state": component.get("v.country"),              //The state — maximum length: 50 characters.
                "phone": {
                    "countryCode": component.get("v.countryCode"),               //valid country code for the phone number (e.g. 44 for the United Kingdom).   
                    "number": component.get("v.phNumber")               //cardholder's contact phone number. Must be between 6 and 25 characters.
                }
            },
            
            style: style,
            
            cardTokenised: function(event) {
                cardTokenn = event.data.cardToken;   
                console.log('cardToken inside controller '+ cardTokenn);
                helper.thirdpartyClientCaller(component,cardTokenn);
                Frames.addCardToken(paymentForm, cardTokenn);
                //paymentForm.submit();
                
            },
            
            cardSubmitted: function() {
                payNowButton.disabled = true;
            },
        }); 
        
        paymentForm.addEventListener('submit', function(event) {
            event.preventDefault();
            Frames.submitCard();
        }); 
        
    },
    
    closeModel: function(component, event, helper) { 
        component.set("v.isOpen", false);
    },
    component2Event : function(cmp, event) { 
        //Get the event amount attribute
        console.log('Inside amount');
        var amountEntered = event.getParam("amount"); 
        //Set the handler attributes based on event data 
        cmp.set("v.amount", amountEntered);         
    },
})