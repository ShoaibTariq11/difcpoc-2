({
  openModal: function (component, event, helper) {
    /*
        let newURL = new URL(window.location.href).searchParams;
        var modalName = newURL.get('modalName');
          console.log('########### modalName ');
        console.log('########### modalName ',modalName);
        
        if(  !$A.util.isEmpty(modalName) )
        {
            //'c:OB_ClientCommentOnStep'
            modalName = 'c:'+modalName;
            console.log('########### modalName inside ',modalName);
            //Dynamic creation of lightningModalChild component and appending its markup in a div
            $A.createComponent(modalName , {'showModal':true},
                function(modalComponent, status, errorMessage) {
                    if (status === "SUCCESS") {
                        //Appending the newly created component in div
                        var body = component.find( 'showChildModal' ).get("v.body");
                        body.push(modalComponent);
                        component.find( 'showChildModal' ).set("v.body", body);
                    } else if (status === "INCOMPLETE") {
                        console.log('Server issue or client is offline.');
                    } else if (status === "ERROR") {
                        console.log('error');
                    }
                }
            );
        }
        */
  },
  commLicRenderRule: function (component, event, helper, account) {
    var typeToShow = [
      "Dual license of DED licensed firms with an affiliate in DIFC",
      "ATM",
      "Vending Machines"
    ];
    if (
      account.Is_Commercial_Permission__c == true &&
      !typeToShow.includes(account.OB_Type_of_commercial_permission__c)
    ) {
      component.set("v.showSSTab", false);
    }
  }
});