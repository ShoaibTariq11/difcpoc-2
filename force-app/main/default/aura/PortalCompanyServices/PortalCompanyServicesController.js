({
    doInit : function(component, event, helper) {
        helper.loadRequests(component,event);
        helper.serviceRequestMenu(component,event);
    },
    
    renderForm : function(component, event, helper) {
        console.log('test');
        console.log(event.getParam('name'));
        var selectedItem = event.getParam('name');
        if(selectedItem != 'Financing Statement') {
            event.preventDefault();
            alert('Service temporary unavailable!');
        } else {
            //event.preventDefault();
            alert('navigate to next form component');
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef : "c:FinancingStatementForm1",
                componentAttributes: {
                    //contactName : component.get("v.contact.Name")
                }
            });
            evt.fire();
        }
    },
    
    handleSectionToggle: function (cmp, event) {
        var openSections = event.getParam('openSections');
        if (openSections.length === 0) {
            //cmp.set('v.activeSectionsMessage', "All sections are closed");
        } else {
            //cmp.set('v.activeSectionsMessage', "Open sections: " + openSections.join(', '));
        }
    }
})