({
    loadRequests : function(component,event) {
        var action = component.get("c.fetchPortalServices");
        action.setParams({
            serviceType: "Other Services"
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var results = response.getReturnValue();
                component.set("v.sections", results);
            }
        });
        $A.enqueueAction(action);
    },
    
    serviceRequestMenu : function(component,event) {
        var action = component.get("c.companyServiceRequestsMenu");
        action.setParams({
            serviceType: "Other Services"
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var results = response.getReturnValue();
                var serviceRequestMenuMap = [];
                for(var key in results) {
                    serviceRequestMenuMap.push({value:results[key], key:key});
                }
                component.set("v.serviceRequestsMenu", serviceRequestMenuMap);
            }
        });
        $A.enqueueAction(action);
    }
})