({
    fetchPickListVal: function(component, fieldName) {
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.authDebtorOrParty"),
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        label: "--- None ---",
                        value: ""
                    });
                }
                for(var i = 0; i < allValues.length; i++) {
                    opts.push({
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.set("v.countryOptions", opts);
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchCustomerId : function(component,event) {
    	var action = component.get("c.userAccount");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var whereClause = "Customer__c='"+response.getReturnValue()+"'";
                component.set("v.whereCondition",whereClause);
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchDebtorsPartys : function(component,event) {
        var action = component.get("c.fetchDebtorPartyList");
        action.setParams({
            recordId : component.get("v.initialFinanceStatementId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                console.log('--start--');
                console.log(response.getReturnValue());
                console.log('--end--');
                component.set("v.renderAuthDbtPrtyLst", true);
                component.set("v.masterAuthDbtPrtys", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    
	fetchDebtors : function(component,event) {
        var action = component.get("c.fetchDebtorList");
        console.log('--line no 32 record id--'+component.get("v.initialFinanceStatementId"));
        action.setParams({
            recordId : component.get("v.initialFinanceStatementId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                console.log('--start--');
                console.log(response.getReturnValue());
                console.log('--end--');
                component.set("v.masterDbtPrtys", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    
    fetchPartys : function(component,event) {
        console.log('--line no 46 record id--'+component.get("v.initialFinanceStatementId"));
        var action = component.get("c.fetchPartyList");
        action.setParams({
            recordId : component.get("v.initialFinanceStatementId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.masterDbtPrtys", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    
    pullMasterData : function(component,event) {
        var recName = event.getSource().get("v.value");
        console.log('--line no 49 record id--'+recName);
        var action = component.get("c.fetchMasterData");
        action.setParams({
            recordName : recName
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                //component.set("v.masterDbtPrtys", response.getReturnValue());
                //var masterRec = response.getReturnValue();
                component.set("v.openModel",true);
                component.set("v.masterDebtorOrParty",response.getReturnValue());
                component.set("v.editDebtorOrParty.First_Name__c",response.getReturnValue().First_Name__c);
                component.set("v.editDebtorOrParty.Last_Name__c",response.getReturnValue().Last_Name__c);
                component.set("v.editDebtorOrParty.Middle_Name__c",response.getReturnValue().Middle_Name__c);
                component.set("v.editDebtorOrParty.Suffix__c",response.getReturnValue().Suffix__c);
                component.set("v.editDebtorOrParty.Mailing_Address__c",response.getReturnValue().Mailing_Address__c);
                component.set("v.editDebtorOrParty.City__c",response.getReturnValue().City__c);
                component.set("v.editDebtorOrParty.Country__c",response.getReturnValue().Country__c);
                component.set("v.editDebtorOrParty.Jurisdiction_of_Organisation__c",response.getReturnValue().Jurisdiction_of_Organisation__c);
                component.set("v.editDebtorOrParty.Organisation_Name__c",response.getReturnValue().Organisation_Name__c);
                component.set("v.editDebtorOrParty.Organisational_ID__c",response.getReturnValue().Organisational_ID__c);
                component.set("v.editDebtorOrParty.Type_of_Organisation__c",response.getReturnValue().Type_of_Organisation__c);
                component.set("v.editDebtorOrParty.Status__c","Active");
                component.set("v.editDebtorOrParty.Type__c",response.getReturnValue().Type__c);
                component.set("v.editDebtorOrParty.Master_Id__c",response.getReturnValue().Name);
            }
        });
        $A.enqueueAction(action);
	},
    
    renderState: function(component) {
        var currentStep = component.get("v.currentStep"),
            allSteps = component.get("v.steps"),
            render = [],
            state = "slds-is-complete";
        allSteps.forEach(function(step) {
            if(currentStep === step) {
                state = "slds-is-current";
            } else if(state === "slds-is-current") {
                state = "slds-is-incomplete";
            }
            render.push({ label: step, selected: state === "slds-is-current", state: state });
        });
        component.set("v.renderInfo", render);
    },
    
    clientInfo : function(component, event) {
    	var action = component.get("c.userAccountInfo");
        action.setParams({
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var userinformation = response.getReturnValue();
                component.set("v.accountName", userinformation[0]);
                component.set("v.accountAddress", userinformation[1]);
            }
        });
        $A.enqueueAction(action);
    },
    
    validateFormRequired: function(component, event) {
        var isValid = true;
        var appForm = component.get("v.approvedForm");
        var initialformId = component.get("v.initialFinanceStatementId");
        var authDebtorOrParty = component.get("v.authDebtorOrParty");
        var selectedAmendmentAffected = component.find("selectEffectedAmmendment").get("v.value");
        var selectedCollateralChange = component.find("selectType").get("v.value");
        var selectedAuthType = component.find("selectAuthoriserType").get("v.value");
        if(selectedAmendmentAffected == '' && selectedCollateralChange == '') {
            isValid = false;
            alert('Please select either \'Amendment Affected for\' or \'Collateral Change\'');
        } else if(appForm.Ammendment__c != '' && appForm.Type_of_Amendment__c == '') {
            isValid = false;
            alert('Please select Type of Amendment if you select \'Amendment Affected for\' is selected!');
        } else if(appForm.Ammendment__c != '' && appForm.Type_of_Amendment__c != '') {
            if(appForm.Type_of_Amendment__c == 'Change' || appForm.Type_of_Amendment__c == 'Delete') {
                var selectedUpdateValue = component.find("selectUpdateType").get("v.value");
                var updateDebtorOrParty = component.get("v.updateDebtorOrParty");
                if(selectedUpdateValue == '') {
                	isValid = false;
                    alert('Please select type of Debtor/Secured Party from the drop down in Change/Added Info section!');
                }  else if(selectedUpdateValue == 'Individual' && (updateDebtorOrParty.Last_Name__c == '' || updateDebtorOrParty.First_Name__c == '')) {
                     isValid = false;
                     alert('First Name and Last Name are required in Change/Added Info Section!');
                 } else if(selectedUpdateValue == 'Organisation' && (updateDebtorOrParty.Organisation_Name__c == '' || updateDebtorOrParty.Type_of_Organisation__c == '' || updateDebtorOrParty.Jurisdiction_of_Organisation__c == '' || updateDebtorOrParty.Organisational_ID__c == '')) {
                     isValid = false;
                     alert('All fields are required when Organisation is selected in Change/Added Info Section!');
                 } else if(updateDebtorOrParty.Mailing_Address__c == '') {
                     isValid = false;
                     alert('Mailing Address is required in Change/Added Info Section!');
                 } else if(updateDebtorOrParty.City__c == '') {
                     isValid = false;
                     alert('City is required in Change/Added Info Section!');
                 } else if(updateDebtorOrParty.Country__c == '') {
                     isValid = false;
                     alert('Country is required in Change/Added Info Section!');
                 }
            } else if(appForm.Type_of_Amendment__c == 'Add') {
                var selectedNewValue = component.find("selectNewType").get("v.value");
                var newDebtorOrParty = component.get("v.newDebtorOrParty");
                if(selectedNewValue == '') {
                	isValid = false;
                    alert('Please select type of Debtor/Secured Party from the drop down in Add New Info section!');
                }  else if(selectedNewValue == 'Individual' && (newDebtorOrParty.Last_Name__c == '' || newDebtorOrParty.First_Name__c == '')) {
                     isValid = false;
                     alert('First Name and Last Name are required in Add New Info Section!');
                 } else if(selectedNewValue == 'Organisation' && (newDebtorOrParty.Organisation_Name__c == '' || newDebtorOrParty.Type_of_Organisation__c == '' || newDebtorOrParty.Jurisdiction_of_Organisation__c == '' || newDebtorOrParty.Organisational_ID__c == '')) {
                     isValid = false;
                     alert('All fields are required when Organisation is selected in Add New Info Section!');
                 } else if(newDebtorOrParty.Mailing_Address__c == '') {
                     isValid = false;
                     alert('Mailing Address is required in Add New Info Section!');
                 } else if(newDebtorOrParty.City__c == '') {
                     isValid = false;
                     alert('City is required in Add New Info Section!');
                 } else if(newDebtorOrParty.Country__c == '') {
                     isValid = false;
                     alert('Country is required in Add New Info Section!');
                 }
            } else if(appForm.Type_of_Amendment__c == '') {
                isValid = false;
                alert('Please select a value in Type of Amendment!');
            }
        } else if(selectedCollateralChange != '' && appForm.Collateral_Description__c == '') {
            isValid = false;
            alert('Collateral Description is required when Collateral Change is selected!');
        } else if(selectedAuthType == '') {
            isValid = false;
            alert('Please select Type of Authoriser!');
        } else if(authDebtorOrParty.Type__c != '') {
            if(selectedAuthValue == '') {
                isValid = false;
                alert('Please select type of Debtor/Secured Party from the drop down in Authorising Amendment section!');
            }  else if(selectedAuthValue == 'Individual' && (authDebtorOrParty.Last_Name__c == '' || authDebtorOrParty.First_Name__c == '')) {
                isValid = false;
                alert('First Name and Last Name are required in Authorising Amendment Section!');
            } else if(selectedAuthValue == 'Organisation' && (authDebtorOrParty.Organisation_Name__c == '' || authDebtorOrParty.Type_of_Organisation__c == '' || authDebtorOrParty.Jurisdiction_of_Organisation__c == '' || authDebtorOrParty.Organisational_ID__c == '')) {
                isValid = false;
                alert('All fields are required when Organisation is selected in Authorising Amendment Section!');
            } else if(authDebtorOrParty.Mailing_Address__c == '') {
                isValid = false;
                alert('Mailing Address is required in Authorising Amendment Section!');
            } else if(authDebtorOrParty.City__c == '' || authDebtorOrParty.City__c === undefined) {
                isValid = false;
                alert('City is required in Authorising Amendment Section!');
            } else if(authDebtorOrParty.Country__c == '') {
                isValid = false;
                alert('Country is required in Authorising Amendment Section!');
            }
        }
        return isValid;
    },
    
    validateRequiredSRDocs: function(component, srId) {
    	component.set("v.isValidateOpen",true);
        var action = component.get("c.fetchApprovedForm");
        action.setParams({
            "recId": srId
        });
        action.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS") {
                var apForm = response.getReturnValue();
                console.log(apForm.Documents_Pending__c);
                if(apForm.Documents_Pending__c == true) {
                    component.set("v.documentsUploaded",false);
                    component.set("v.approvedForm.Documents_Pending__c",true);
                } else if(apForm.Documents_Pending__c == false) {
                    component.set("v.documentsUploaded",true);
                    component.set("v.approvedForm.Documents_Pending__c",false);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchSRPricingInfo : function(component, srId) {
        var action = component.get("c.PopulateSR_Pricing");
        action.setParams({
            "recId": srId
        });
        action.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS") {
                var responseWrapper = response.getReturnValue();
                console.log('--wrapper--');
                console.log(responseWrapper.lstSRPriceItems);
                console.log('--wrapper--');
                component.set("v.SRPricingInfo", responseWrapper);
            }
        });
        $A.enqueueAction(action);
    },
    
    loadSR : function(component, srId) {
    	var action = component.get("c.fetchSR");
        action.setParams({
            recId : srId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log(result);
                component.set("v.serviceRequest", result);
                component.set("v.approvedForm", result.Approved_Forms__r[0]);
                component.set("v.newDebtorPartyList", result.Debtors_SecuredParties__r);
            }
        });
        $A.enqueueAction(action);
    },
    
    loadApprovedRequestForm : function(component, srId) {
    	var action = component.get("c.fetchApprovedForm");
        action.setParams({
            recId : srId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.approvedForm", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})