({
	doInIt : function(component, event, helper) {
		helper.clientInfo(component, event);
        helper.renderState(component, event);
        helper.fetchCustomerId(component, event);
        if(component.get("v.serviceRequestId") === undefined || component.get("v.serviceRequestId") === null || component.get("v.serviceRequestId") === '') {
            helper.fetchPickListVal(component,'Country__c');
        }
        if(component.get("v.serviceRequestId") !== undefined || component.get("v.serviceRequestId") !== null || component.get("v.serviceRequestId") !== '') {
            helper.loadSR(component,component.get("v.serviceRequestId"));
            helper.loadApprovedRequestForm(component,component.get("v.serviceRequestId"));
            helper.fetchSRPricingInfo(component,component.get("v.serviceRequestId"));
            
        }
	},
    
    click: function(component, event, helper) {
        component.set("v.isValidateOpen",false);
        var index = parseInt(event.target.closest("[data-index]").dataset.index), 
            value, onselect, steps = component.get("v.steps");
        if(index !== undefined) {
            var formId = component.get("v.approvedFormId");
            var docsUploaded = component.get("v.documentsUploaded");
            if(formId === undefined && component.get("v.serviceRequestId") === null) {
            	alert('Please save Amendment to proceed further...');
            } else if(formId !== undefined && component.get("v.serviceRequestId") === null && docsUploaded === false && steps[index] == 'Pricing') {
                alert('Please upload mandatory documents in documents section...');
            } else if(component.get("v.serviceRequestId") !== null && (component.get("v.approvedForm").Documents_Pending__c === true) && steps[index] == 'Pricing') {
            	alert('Please upload mandatory documents in documents section...');
            } else if((formId !== undefined || component.get("v.serviceRequestId") !== null) && steps[index] == 'Documents') {
                event.preventDefault();
                value = steps[index];
                component.set("v.currentStep", value);
                onselect = component.getEvent("onselect");
                onselect.setParams({ value: value });
                onselect.fire();
                helper.renderState(component);
            } else if(formId !== undefined && component.get("v.serviceRequestId") === null && docsUploaded === true && steps[index] == 'Pricing') {
                event.preventDefault();
                value = steps[index];
                component.set("v.currentStep", value);
                onselect = component.getEvent("onselect");
                onselect.setParams({ value: value });
                onselect.fire();
                helper.renderState(component);
            } else if(component.get("v.serviceRequestId") !== null && (component.get("v.approvedForm").Documents_Pending__c === false || docsUploaded === true) && steps[index] == 'Pricing') {
                event.preventDefault();
                value = steps[index];
                component.set("v.currentStep", value);
                onselect = component.getEvent("onselect");
                onselect.setParams({ value: value });
                onselect.fire();
                helper.renderState(component);
            } else if(steps[index] == 'Amendment') {
                event.preventDefault();
                value = steps[index];
                component.set("v.currentStep", value);
                onselect = component.getEvent("onselect");
                onselect.setParams({ value: value });
                onselect.fire();
                helper.renderState(component);
            }
        }
    },
    
    update: function(component, event, helper) {
        var formId = component.get("v.approvedFormId");
        if(formId !== undefined) {
            helper.renderState(component, event);
        } else if(formId === undefined && component.get("v.serviceRequestId") === null) {
            alert('Please save Amendment to proceed further...');
        }
    },
    
    handleRadioChange : function(component, event, helper) {
		var radioValue = event.getParam("value");
        if(radioValue !== '' && radioValue !== null && radioValue === 'termination') {
            component.set("v.approvedForm.Termination__c",true);
            component.set("v.approvedForm.Continuation__c",false);
        } else if(radioValue !== '' && radioValue !== null && radioValue === 'continuation') {
            component.set("v.approvedForm.Continuation__c",true);
            component.set("v.approvedForm.Termination__c",false);
        } else if(radioValue == '') {
            component.set("v.approvedForm.Continuation__c",false);
            component.set("v.approvedForm.Termination__c",false);
        }
	},
    
    onChangeEffectedAmmendment: function(component, event, helper) {
    	var selectedValue = component.find("selectEffectedAmmendment").get("v.value");
        component.set("v.effectedAmendment",selectedValue);
        if(selectedValue == 'Debtor') {
            component.set("v.approvedForm.Ammendment__c","Debtor");
            //component.set("v.currentDebtorOrParty.Type__c","Debtor");
			component.set("v.updateDebtorOrParty.Type__c","Debtor");
			component.set("v.newDebtorOrParty.Type__c","Debtor");
            component.set("v.renderNewForm",false);
			component.set("v.renderUpdateForm",false);
        } else if(selectedValue == 'Secured Party') {
            component.set("v.approvedForm.Ammendment__c","Secured Party");
            //component.set("v.currentDebtorOrParty.Type__c","Secured Party");
			component.set("v.updateDebtorOrParty.Type__c","Secured Party");
			component.set("v.newDebtorOrParty.Type__c","Secured Party");
            component.set("v.renderNewForm",false);
			component.set("v.renderUpdateForm",false);
        } else {
			component.set("v.approvedForm.Ammendment__c","");
			//component.set("v.currentDebtorOrParty.Type__c","");
			component.set("v.updateDebtorOrParty.Type__c","");
			component.set("v.newDebtorOrParty.Type__c","");
            component.set("v.renderNewForm",false);
			component.set("v.renderUpdateForm",false);
        }
    },
    
    onChangeAmendmentType : function(component, event, helper) {
        var typeSelected = component.find("selectEffectedAmmendment").get("v.value");
    	var selectedValue = component.find("amendmentType").get("v.value");
        if(selectedValue == 'CHANGE name and/or address') {
            component.set("v.renderNewForm",false);
            component.set("v.renderUpdateForm",true);
            component.set("v.approvedForm.Type_of_Amendment__c","Change");
            //component.set("v.currentDebtorOrParty.Existing_Record__c",true);
            component.set("v.updateDebtorOrParty.Existing_Record__c",true);
            component.set("v.updateDebtorOrParty.Status__c","Active");
        } else if(selectedValue == 'DELETE name') {
            component.set("v.renderNewForm",false);
            component.set("v.renderUpdateForm",true);
            component.set("v.approvedForm.Type_of_Amendment__c","Delete");
            //component.set("v.currentDebtorOrParty.Existing_Record__c",true);
            component.set("v.updateDebtorOrParty.Existing_Record__c",true);
            component.set("v.updateDebtorOrParty.Status__c","Inactive");
        } else if(selectedValue == 'ADD name') {
            component.set("v.renderNewForm",true);
            component.set("v.renderUpdateForm",false);
            component.set("v.approvedForm.Type_of_Amendment__c","Add");
            if(typeSelected == 'Debtor') {
                component.set("v.approvedForm.Ammendment__c","Debtor");
            } else if(typeSelected == 'Secured Party') {
                component.set("v.approvedForm.Ammendment__c","Secured Party");
            }
            component.set("v.newDebtorOrParty.Existing_Record__c",false);
            component.set("v.newDebtorOrParty.Status__c","Active");
            component.set("v.updateDebtorOrParty.Existing_Record__c",false);
            component.set("v.updateDebtorOrParty.Status__c","");
        } else {
            component.set("v.renderNewForm",false);
            component.set("v.renderUpdateForm",false);
            component.set("v.renderDbtPrtyLst",false);
            component.set("v.approvedForm.Type_of_Amendment__c","");
            component.set("v.newDebtorOrParty.Status__c","");
            component.set("v.updateDebtorOrParty.Status__c","");
        }
    },
    
    updateDebtorOrParty: function (component, event, helper) {
        helper.pullMasterData(component, event);
        var masterRecord = component.get("v.editDebtorOrParty");
        console.log(masterRecord);
    },
    
    deleteDebtorOrParty: function (component, event, helper) {
    	
    },
    
    //Current Debtor's/Party's Org name or individual name drop down selection
    /*onChangeCurrent: function (component, event, helper) {
        var selectedValue = component.find("selectCurrentType").get("v.value");
        if(selectedValue == 'Organisation') {
            component.set("v.currentDebtorOrParty.First_Name__c","");
            component.set("v.currentDebtorOrParty.Last_Name__c","");
            component.set("v.currentDebtorOrParty.Middle_Name__c","");
            component.set("v.currentDebtorOrParty.Suffix__c","");
            component.set("v.currentDebtorOrParty.Existing_Record__c",true);
        } else if(selectedValue == 'Individual') {
            component.set("v.currentDebtorOrParty.Organisation_Name__c","");
            component.set("v.currentDebtorOrParty.Type_of_Organisation__c","");
            component.set("v.currentDebtorOrParty.Jurisdiction_of_Organisation__c","");
            component.set("v.currentDebtorOrParty.Organisational_ID__c","");
            component.set("v.currentDebtorOrParty.Existing_Record__c",true);
        }
        component.set("v.currentType",selectedValue);
    },*/
	
	//Update Debtor's/Party's Org name or individual name drop down selection
    onChangeUpdate: function (component, event, helper) {
        var selectedValue = component.find("selectUpdateType").get("v.value");
        if(selectedValue == 'Organisation') {
            component.set("v.updateDebtorOrParty.First_Name__c","");
            component.set("v.updateDebtorOrParty.Last_Name__c","");
            component.set("v.updateDebtorOrParty.Middle_Name__c","");
            component.set("v.updateDebtorOrParty.Suffix__c","");
            component.set("v.updateDebtorOrParty.Existing_Record__c",true);
        } else if(selectedValue == 'Individual') {
            component.set("v.updateDebtorOrParty.Organisation_Name__c","");
            component.set("v.updateDebtorOrParty.Type_of_Organisation__c","");
            component.set("v.updateDebtorOrParty.Jurisdiction_of_Organisation__c","");
            component.set("v.updateDebtorOrParty.Organisational_ID__c","");
            component.set("v.updateDebtorOrParty.Existing_Record__c",true);
        }
        component.set("v.updateType",selectedValue);
    },
	
	//New Debtor's/Party's Org name or individual name drop down selection
    onChangeNew: function (component, event, helper) {
        var selectedValue = component.find("selectNewType").get("v.value");
        if(selectedValue == 'Organisation') {
            component.set("v.newDebtorOrParty.First_Name__c","");
            component.set("v.newDebtorOrParty.Last_Name__c","");
            component.set("v.newDebtorOrParty.Middle_Name__c","");
            component.set("v.newDebtorOrParty.Suffix__c","");
            component.set("v.newDebtorOrParty.Existing_Record__c",false);
        } else if(selectedValue == 'Individual') {
            component.set("v.newDebtorOrParty.Organisation_Name__c","");
            component.set("v.newDebtorOrParty.Type_of_Organisation__c","");
            component.set("v.newDebtorOrParty.Jurisdiction_of_Organisation__c","");
            component.set("v.newDebtorOrParty.Organisational_ID__c","");
            component.set("v.newDebtorOrParty.Existing_Record__c",false);
        }
        component.set("v.newType",selectedValue);
    },
    
    onChangeCollateral: function (component, event, helper) {
    	var selectedValue = component.find("selectType").get("v.value");
        component.set("v.approvedForm.Amendment_Collateral_Change__c",selectedValue);
        if(selectedValue != '') {
            component.set("v.isCollateralChange",true);
        } else {
            component.set("v.approvedForm.Collateral_Value__c",0);
            component.set("v.approvedForm.Collateral_Description__c","");
            component.set("v.isCollateralChange",false);
        }
    },
    
    onChangeAuthoriserType: function(component, event, helper) {
        var selectedValue = component.find("selectAuthoriserType").get("v.value");
        component.set("v.authDebtorOrParty.Type__c",selectedValue);
    },
    
    //Authorized Debtor's/Party's Org name or individual name drop down selection
    onChangeAuth: function (component, event, helper) {
        var selectedValue = component.find("selectAuthType").get("v.value");
        if(selectedValue == 'Organisation') {
            component.set("v.authDebtorOrParty.First_Name__c","");
            component.set("v.authDebtorOrParty.Last_Name__c","");
            component.set("v.authDebtorOrParty.Middle_Name__c","");
            component.set("v.authDebtorOrParty.Suffix__c","");
            component.set("v.authDebtorOrParty.Existing_Record__c",true);
        } else if(selectedValue == 'Individual') {
            component.set("v.authDebtorOrParty.Organisation_Name__c","");
            component.set("v.authDebtorOrParty.Type_of_Organisation__c","");
            component.set("v.authDebtorOrParty.Jurisdiction_of_Organisation__c","");
            component.set("v.authDebtorOrParty.Organisational_ID__c","");
            component.set("v.authDebtorOrParty.Existing_Record__c",true);
        }
        component.set("v.authDebtorOrParty.Authorized__c",true);
        component.set("v.authType",selectedValue);
    },
    
    /*handleCurrentCountryChange: function(component, event) {
        // This will contain the string of the "value" attribute of the selected option
        var selectedOptionValue = event.getParam("value");
        component.set("v.currentDebtorOrParty.Country__c",selectedOptionValue);
    },*/
    
    handleUpdateCountryChange: function(component, event) {
        // This will contain the string of the "value" attribute of the selected option
        //var selectedOptionValue = event.getParam("value");
        var selectedOptionValue = component.find('selectUpdateCountry').get('v.value');
        component.set("v.updateDebtorOrParty.Country__c",selectedOptionValue);
    },
    
    handleNewCountryChange: function(component, event) {
        // This will contain the string of the "value" attribute of the selected option
        //var selectedOptionValue = event.getParam("value");
        var selectedOptionValue = component.find('selectNewCountry').get('v.value');
        component.set("v.newDebtorOrParty.Country__c",selectedOptionValue);
    },
    
    handleAuthCountryChange: function(component, event) {
        // This will contain the string of the "value" attribute of the selected option
        //var selectedOptionValue = event.getParam("value");
        var selectedOptionValue = component.find('selectAuthCountry').get('v.value');
        component.set("v.authDebtorOrParty.Country__c",selectedOptionValue);
    },
    
    Save: function(component, event, helper) {
        if(helper.validateFormRequired(component, event)) {
            var appForm = component.get("v.approvedForm");
            var newDebtPartyList = component.get("v.newDebtorPartyList");
            if(appForm.Type_of_Amendment__c == 'Change' || appForm.Type_of_Amendment__c == 'Delete') {
				//var currentdebtOrPrty = component.get("v.currentDebtorOrParty");
                //newDebtPartyList.push({currentdebtOrPrty});
                var updatedebtOrPrty = component.get("v.updateDebtorOrParty");
                newDebtPartyList.push({updatedebtOrPrty});
            }
			if(appForm.Type_of_Amendment__c == 'Add') {
            	var newdebtOrPrty = component.get("v.newDebtorOrParty");
                newDebtPartyList.push({newdebtOrPrty});
			}
            var authdebtOrPrty = component.get("v.authDebtorOrParty");
            newDebtPartyList.push({authdebtOrPrty});
			
            var arrayList = JSON.stringify(newDebtPartyList);
			console.log('--arrayList start--');
			console.log(arrayList);
            console.log('--arrayList end--');
            var selectedAuthoriserType = component.find("selectAuthoriserType").get("v.value");
			arrayList = arrayList.replace('}}]','}]');
            arrayList = arrayList.replace('{"authdebtOrPrty":','');
			arrayList = arrayList.replace('"Debtor"}}','"Debtor"}');
			arrayList = arrayList.replace('"Secured Party"}}','"Secured Party"}');
			if(appForm.Ammendment__c == 'Secured Party') {
                if(appForm.Type_of_Amendment__c == 'Change' || appForm.Type_of_Amendment__c == 'Delete') {
					arrayList = arrayList.replace('}}]','}]');
                	//arrayList = arrayList.replace('{"currentdebtOrPrty":','');
                	arrayList = arrayList.replace('{"updatedebtOrPrty":','');
                    arrayList = arrayList.replace('"Secured Party"}}','"Secured Party"}');
                    arrayList = arrayList.replace('"Secured Party"}}','"Secured Party"}');
					arrayList = arrayList.replace('"Secured Party"}}','"Secured Party"}');
                }
				if(appForm.Type_of_Amendment__c == 'Add') {
                    arrayList = arrayList.replace('}}]','}]');
                	arrayList = arrayList.replace('{"newdebtOrPrty":','');
                    arrayList = arrayList.replace('"Secured Party"}}','"Secured Party"}');
                    arrayList = arrayList.replace('"Secured Party"}}','"Secured Party"}');
					arrayList = arrayList.replace('"Secured Party"}}','"Secured Party"}');
				}
			} else if(appForm.Ammendment__c == 'Debtor') {
				if(appForm.Type_of_Amendment__c == 'Change' || appForm.Type_of_Amendment__c == 'Delete') {
					arrayList = arrayList.replace('}}]','}]');
                	//arrayList = arrayList.replace('{"currentdebtOrPrty":','');
                	arrayList = arrayList.replace('{"updatedebtOrPrty":','');
                    arrayList = arrayList.replace('"Debtor"}}','"Debtor"}');
					arrayList = arrayList.replace('"Debtor"}}','"Debtor"}');
					arrayList = arrayList.replace('"Debtor"}}','"Debtor"}');
				}
				if(appForm.Type_of_Amendment__c == 'Add') {
                    arrayList = arrayList.replace('}}]','}]');
                	arrayList = arrayList.replace('{"newdebtOrPrty":','');
					arrayList = arrayList.replace('"Debtor"}}','"Debtor"}');
					arrayList = arrayList.replace('"Debtor"}}','"Debtor"}');
                }
            }
			console.log('--final arrayList start--');
			console.log(arrayList);
			console.log('--final arrayList end--');
            var finalDebtPartyList = JSON.parse(arrayList);
			var action = component.get("c.saveApprovedForm");
            action.setParams({
                af : appForm,
                ListDebtor : finalDebtPartyList,
				initialFormId : component.get("v.initialFinanceStatementId")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {
                    var results = response.getReturnValue();
                    component.set("v.serviceReqId", results[0]);
                    component.set("v.approvedFormId", results[1]);
                    if(results[2] == 'true') {
                    	component.set("v.documentsUploaded", false);
                        component.set("v.approvedForm.Documents_Pending__c",true)
                    } else if(results[2] == 'false') {
                    	component.set("v.documentsUploaded", true);
                        component.set("v.approvedForm.Documents_Pending__c",false)
                    }
                    component.set("v.approvedForm.Initial_Financial_Statement_No__c",results[4]);
                    component.set("v.messageType","success");
                    component.set("v.messageVariant","inverse");
                    component.set("v.message", "Service Request generated Successfully!");
                    component.set("v.currentStep","Documents");
                    helper.fetchSRPricingInfo(component,results[0]);
                    var currentStep = "Documents",
                        allSteps = component.get("v.steps"),
                        render = [],
                        state = "slds-is-complete";
                    component.set("v.renderInfo", render);
                    allSteps.forEach(function(step) {
                        if(currentStep === step) {
                            state = "slds-is-current";
                        } else if(state === "slds-is-current") {
                            state = "slds-is-incomplete";
                        }
                        render.push({ label: step, selected: state === "slds-is-current", state: state });
                    });
                    component.set("v.renderInfo", render);
                    window.scrollTo(0, 0);
                } else if(state === "ERROR") {
                    component.set("v.messageType","error");
                    var errors = action.getError();
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            component.set("v.message",errors[0].message);
                            component.set("v.messageVariant","inverse");
                            window.scrollTo(0, 0);
                        }
                    } else {
                        component.set("v.message", "Request Failed!");
                        component.set("v.messageVariant","inverse");
                        window.scrollTo(0, 0);
                    }
                } else if (status === "INCOMPLETE") {
                    component.set("v.messageType","error");
                    component.set("v.messageVariant","inverse");
                    component.set("v.message", "No response from server or client is offline.");
                    window.scrollTo(0, 0);
                }
            });
            $A.enqueueAction(action);
		}
    },

	refreshDocuments: function(component, event, helper) {
        component.set("v.isValidateOpen",false);
        var action = component.get("c.fetchApprovedForm");
        var srId;
        if(component.get("v.serviceRequestId") !== null) {
            srId = component.get("v.serviceRequestId");
        } else if(component.get("v.serviceReqId") !== null) {
            srId = component.get("v.serviceReqId");
        }
        action.setParams({
            "recId": srId
        });
        action.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS") {
                var apForm = response.getReturnValue();
                if(apForm.Documents_Pending__c == true) {
                    component.set("v.documentsUploaded",false);
                    component.set("v.approvedForm.Documents_Pending__c",true);
                } else if(apForm.Documents_Pending__c == false) {
                    component.set("v.documentsUploaded",true);
                    component.set("v.approvedForm.Documents_Pending__c",false);
                }
            }
        });
        $A.enqueueAction(action);
    },

	validateForm: function(component, event, helper) {
        var srId;
        if(component.get("v.serviceRequestId") !== null) {
            srId = component.get("v.serviceRequestId");
        } else if(component.get("v.serviceReqId") !== null) {
            srId = component.get("v.serviceReqId");
        }
        helper.validateRequiredSRDocs(component, srId);
    },

	cancelValidation: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isValidateOpen", false);
    },

	updateForm: function(component, event, helper) {
        var appRecordId;
        if(component.get("v.serviceRequestId") !== null) {
            appRecordId = component.get("v.approvedForm").Id;
        } else if(component.get("v.serviceReqId") !== null) {
            appRecordId = component.get("v.approvedFormId");
        }
        var action = component.get("c.updateRequest");
        action.setParams({
            "recordId": appRecordId,
            "description": "",
            "noOfPages": ""
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                //component.set("v.documentsUploaded",true);
                component.set("v.docsapprovedFormId", response.getReturnValue());
                component.set("v.messageType","success");
                component.set("v.messageVariant","inverse");
                component.set("v.message", "Changes saved successfully!");
                component.set("v.currentStep","Pricing");
                var currentStep = "Pricing",
                    allSteps = component.get("v.steps"),
                    render = [],
                    state = "slds-is-complete";
                component.set("v.renderInfo", render);
                allSteps.forEach(function(step) {
                    if(currentStep === step) {
                        state = "slds-is-current";
                    } else if(state === "slds-is-current") {
                        state = "slds-is-incomplete";
                    }
                    render.push({ label: step, selected: state === "slds-is-current", state: state });
                });
                component.set("v.renderInfo", render);
                window.scrollTo(0, 0);
            } else if(state === "ERROR") {
                component.set("v.messageType","error");
                var errors = action.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        component.set("v.message",errors[0].message);
                        component.set("v.messageVariant","inverse");
                        window.scrollTo(0, 0);
                    }
                } else {
                    component.set("v.message", "Request Failed!");
                    component.set("v.messageVariant","inverse");
                    window.scrollTo(0, 0);
                }
            } else if (status === "INCOMPLETE") {
                component.set("v.messageType","error");
                component.set("v.messageVariant","inverse");
                component.set("v.message", "No response from server or client is offline.");
                window.scrollTo(0, 0);
            }
        });
        $A.enqueueAction(action);
    },
	
	submitForm: function(component, event, helper) {
        var insufficientBalance = component.get("v.SRPricingInfo.insufficientBalance");
        if(!insufficientBalance) {
            var serviceId;
            if(component.get("v.serviceRequestId") !== null) {
                serviceId = component.get("v.serviceRequestId");
            } else if(component.get("v.serviceReqId") !== null) {
                serviceId = component.get("v.serviceReqId");
            }
            var action = component.get("c.submitRequest");
            action.setParams({
                "recordId": serviceId,
                "apId" : "",
                "collateral" : ""//component.get("v.approvedForm.Collateral_Description__c")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {
                    var results = response.getReturnValue();
                    component.set("v.messageType","success");
                    component.set("v.messageVariant","inverse");
                    component.set("v.message", "Your Request submitted Successfully!");
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": '/service-request/'+serviceId
                    });
                    urlEvent.fire();
                } else if(state === "ERROR") {
                    component.set("v.messageType","error");
                    var errors = action.getError();
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            component.set("v.message",errors[0].message);
                            component.set("v.messageVariant","inverse");
                            window.scrollTo(0, 0);
                        }
                    } else {
                        component.set("v.message", "Request Failed!");
                        component.set("v.messageVariant","inverse");
                        window.scrollTo(0, 0);
                    }
                } else if (status === "INCOMPLETE") {
                    component.set("v.messageType","error");
                    component.set("v.messageVariant","inverse");
                    component.set("v.message", "No response from server or client is offline.");
                    window.scrollTo(0, 0);
                }
            });
            $A.enqueueAction(action);
        } else {
            alert('You dont have enough balance to Process the Request, Please click on the Top-up Balance in the Sidebar.');
        }
    },

    closeForm: function(component, event, helper) {
        var serviceId = component.get("v.serviceReqId");
        var urlEvent = $A.get("e.force:navigateToURL");
        if(serviceId === null) {
            urlEvent.setParams({
                "url": '/company-services'
            });
        } else {
            urlEvent.setParams({
                "url": '/detail/'+serviceId
            });
        }
        urlEvent.fire();
    },

	showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },
    
    hideSpinner : function(component,event,helper) {   
        component.set("v.Spinner", false);
    }
})