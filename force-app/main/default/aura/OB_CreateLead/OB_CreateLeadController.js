({
  doInit: function(component, event, helper) {
    helper.initLeadCall(component, event, helper);
    /* helper.getpropertyTypePicklist(component, event,helper);
    helper.getlicensingperiodPicklist(component, event,helper);
      
    var action = component.get("c.getPiklistValues");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var result = response.getReturnValue();
                var plValues = [];
                for (var i = 0; i < result.length; i++) {
                    plValues.push({
                        label: result[i],
                        value: result[i]
                    });
                }
                component.set("v.BuildingList", plValues);
            }
        });
        $A.enqueueAction(action); */
  },

  handleBuildingChange: function(component, event, helper) {
    //Get the Selected values
    var selectedValues = event.getParam("value");
    console.log(selectedValues);

    //Update the Selected Values
    component.set("v.selectedBuildingList", selectedValues);
    console.log(component.get("v.selectedBuildingList"));
  },

  serverAction: function(component, event, helper) {
    try {
      if (helper.formValidator(component, event, helper, "leadField")) {
        component.set("v.serverCallInProgress", true);

        var phoneCheck = helper.checkPhonePattern(
          component,
          event,
          helper,
          component.get("v.newLead").Phone
        );

        console.log(phoneCheck);

        if (phoneCheck == false) {
          var mulitipicklist = component.get("v.selectedBuildingList");

          var objUncovered = JSON.parse(JSON.stringify(mulitipicklist));
          console.log(typeof objUncovered);
          console.log(objUncovered);
          console.log(objUncovered.join(";"));

          var mulitipicklistVal = objUncovered.join(";");
          component.set("v.newLead.Building__c", mulitipicklistVal);
          var action = component.get("c.createLead");
          var requestWrap = {
            leadObj: component.get("v.newLead")
          };
          action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

          action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
              var respWrap = response.getReturnValue();
              console.log(respWrap);

              if (!respWrap.errorMessage) {
                helper.showToast(
                  component,
                  event,
                  helper,
                  "Request successfully submitted",
                  "information"
                );
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                  url: "/"
                });
                urlEvent.fire();
              } else {
                helper.showToast(
                  component,
                  event,
                  helper,
                  respWrap.errorMessage,
                  "error"
                );
              }
            } else {
              helper.showToast(
                component,
                event,
                helper,
                response.getError()[0].message,
                "error"
              );
              console.log("@@@@@ Error " + response.getError()[0].message);
              console.log(
                "@@@@@ Error Location " + response.getError()[0].stackTrace
              );
            }
            component.set("v.serverCallInProgress", false);
          });
          $A.enqueueAction(action);
        } else {
          helper.showToast(
            component,
            event,
            helper,
            "Please enter a valid phone number in the format (i.e +XXXXXXXXXXXX)",
            "error"
          );
        }
      } else {
        helper.showToast(
          component,
          event,
          helper,
          "please fill the required fields",
          "error"
        );
      }
    } catch (error) {
      console.log(error.message);
    }
  },
  changeOfpropertyType: function(component, event, helper) {
    var typeOfProperty = component.get("v.newLead.Type_of_property__c");
    if (typeOfProperty == "Lease a Kiosk") {
      //filtering builing list
      var builingList = component.get("v.BuildingList");
      console.log(builingList);
      builingList = builingList.filter(
        build => build.value.includes("5000") || build.value.includes("5500")
      );
      console.log(builingList);
      component.set("v.BuildingList", builingList);
    } else {
      component.set(
        "v.BuildingList",
        component.get("v.UnfilteredBuildingList")
      );
    }
  }
});