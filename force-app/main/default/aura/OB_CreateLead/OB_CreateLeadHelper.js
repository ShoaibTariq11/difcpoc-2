({
  initLeadCall: function(component, event, helper) {
    var action = component.get("c.initLead");
    var requestWrap = {};
    action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        component.set("v.respWrap", respWrap);
        component.set("v.newLead", respWrap.leadObj);
        component.set(
          "v.newLead.company",
          respWrap.loggedInUser.Contact.Account.Name
        );
        helper.setpickListVals(component, event, helper, respWrap);
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
    });
    $A.enqueueAction(action);
  },
  formValidator: function(component, event, helper, fieldsToValidate) {
    try {
      var allValid = true;

      let fields = component.find(fieldsToValidate);
      for (var i = 0; i < fields.length; i++) {
        var inputFieldCompValue = fields[i].get("v.value");
        //console.log(inputFieldCompValue);
        if (!inputFieldCompValue && fields[i] && fields[i].get("v.required")) {
          console.log("no value");
          //console.log('bad');
          //fields[i].reportValidity();
          allValid = false;
        }
      }
      return allValid;
    } catch (error) {
      console.log(error.message);
      console.log(error.stackTrace);
    }
  },

  reportValididty: function(component, event, helper, fieldsToReport) {
    try {
      helper.showToast(
        component,
        event,
        helper,
        "Please fill all the required fields",
        "error"
      );
      let fields = component.find(fieldsToReport);

      if (!Array.isArray(fields)) {
        fields.reportValidity();
      } else {
        for (const field of fields) {
          console.log(field);
          if (field.reportValidity) {
            field.reportValidity();
          }
        }
      }
    } catch (error) {
      console.log(error.message);
    }
  },
  setpickListVals: function(component, event, helper, wrapper) {
    try {
      var plValues = [];
      /* for (var i = 0; i < wrapper.PiklistValues.length; i++) {
        plValues.push({
          label: wrapper.PiklistValues[i],
          value: wrapper.PiklistValues[i]
        });
      } */
      for (var key in wrapper.PiklistValues) {
        plValues.push({ label: wrapper.PiklistValues[key], value: key });
      }
      component.set("v.BuildingList", plValues);
      component.set("v.UnfilteredBuildingList", plValues);

      var industryMap = [];
      for (var key in wrapper.PropertyType) {
        industryMap.push({ key: key, value: wrapper.PropertyType[key] });
      }
      component.set("v.industryMap", industryMap);

      var licensingMap = [];
      for (var key in wrapper.LicensingPeriod) {
        licensingMap.push({ key: key, value: wrapper.LicensingPeriod[key] });
      }
      component.set("v.licensingMap", licensingMap);
    } catch (error) {
      console.log(error.message);
    }
  },

  showToast: function(component, event, helper, msg, type) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      mode: "dismissible",
      message: msg,
      type: type
    });
    toastEvent.fire();
  },
  checkPhonePattern: function(component, event, helper, Phone) {
    //var regPhoneFormat = /^[+][(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
    var regPhoneFormat = /^\+(?:[0-9]\ {0,1}){6,14}[0-9]$/g;
    return Phone.match(regPhoneFormat) == null;
  }
});