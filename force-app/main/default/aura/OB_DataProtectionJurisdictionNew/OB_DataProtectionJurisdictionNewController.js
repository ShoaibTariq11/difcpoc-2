({
	initAmendment : function(cmp, event, helper)
    {
       
        var initAmendment = cmp.get('c.initDataProtectRecord');
        cmp.set('v.spinner',true);
        
        var reqWrapPram  =
            {
                srId : cmp.get('v.srId')
            };
        
        initAmendment.setParams(
            {
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
        
        initAmendment.setCallback(this, 
                                  function(response) {
                                      var state = response.getState();
                                      console.log("callback state:new data " + state);
                                      
                                      if (cmp.isValid() && state === "SUCCESS") 
                                      {
                                          cmp.set('v.spinner',false);    
                                          var respWrap = response.getReturnValue();
                                          
                                          var refeshEvnt = cmp.getEvent("refeshEvnt");
                                          console.log('@@@@@ event refeshEvnt ');
                                          refeshEvnt.setParams({
                                              "isViewRemove" : 'New',
                                              "newJurisdictionWrap":respWrap.jurisdictionWrap});
                                          
                                          refeshEvnt.fire();                                              
                                          
                                      }
                                      else
                                      {
                                          console.log('@@@@@ Error '+response.getError()[0].message);
                                          cmp.set('v.spinner',false);
                                      }
                                  }
                                 );
            $A.enqueueAction(initAmendment);
       
    },
})