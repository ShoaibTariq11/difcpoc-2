({
	initAmendment : function(cmp, event, helper)
    {
       
        var initAmendment = cmp.get('c.initAmendmentDB');
        cmp.set('v.spinner',true);
        var accountID= cmp.get('v.accountID');
        var reqWrapPram  =
            {
                srId : cmp.get('v.srWrap').srObj.Id,
                accountID: accountID
            };
        console.log('=====accountID===='+accountID);
        initAmendment.setParams(
            {
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
        
        initAmendment.setCallback(this, 
                                      function(response) {
                                          var state = response.getState();
                                          console.log("callback state:new share class " + state);
                                          
                                          if (cmp.isValid() && state === "SUCCESS") 
                                          {
                                              cmp.set('v.spinner',false);    
                                              var respWrap = response.getReturnValue();
                                              console.log('######### new shareclass respWrap.amedWrap  ',respWrap.amedWrap);
											  //cmp.set('v.amedWrap',respWrap.amedWrap);	
											  //throw event. 
											                                              
											  var refeshEvnt = cmp.getEvent("refeshEvnt");
                                               console.log('@@@@@ event refeshEvnt ');
                                               refeshEvnt.setParams({
                                                    "isNewAmendment" : true,
                                                   "shareClassWrap":respWrap.shareClassWrap});
                                               
                                               refeshEvnt.fire();                                              
                                             
                                          }
                                          else
                                          {
                                              console.log('@@@@@ Error '+response.getError()[0].message);
                                              cmp.set('v.spinner',false);
                                          }
                                      }
                                     );
            $A.enqueueAction(initAmendment);
       
    },
})