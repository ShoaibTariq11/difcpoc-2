({
	init: function(component, event, helper) {
        var action = component.get("c.bankDetailstoReview");
        action.setCallback(this,function(response){
        var state = response.getState();
            console.log('!!@@@'+response.getReturnValue());
            if(state === "SUCCESS"){
            	component.set("v.reviewWrapper",response.getReturnValue());    
            }
        });
        $A.enqueueAction(action);

    },
    gotoURL : function (component, event, helper) {
    var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
      "url": "https://uatfull-difcportal.cs128.force.com/digitalOnboarding/s/"
    });
    	urlEvent.fire();
	},
   
    sendLeadInformation: function (component, event, helper){
               
        var ischecked = component.find("checkbox");
        var sendOTP = ischecked.get("v.value");
        console.log('@@###'+ischecked.get("v.value"));
        if(sendOTP == true){
            var action = component.get("c.otpPage");
            action.setCallback(this,function(response){
        	var state = response.getState();
            if(state === "SUCCESS"){
                if(response.getReturnValue() === false){
                    
                     var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": "https://uatfull-difcportal.cs128.force.com/digitalOnboarding/s/sendotppage"
                     });
            		urlEvent.fire();
            		component.set("v.detailsSenttoMashreq",response.getReturnValue()); 
                }
                else{
                    component.set("v.detailsSenttoMashreq",true); 
                }
            }
        	});
        }
        else{
            
            component.set('v.isError', true);
        }
		$A.enqueueAction(action);
	},
    
    getSelected: function(component, event, helper) {
    
    var contentId = event.currentTarget.getAttribute("data-recid");
    				
    console.log('!!@@'+contentId);
    var openFileEvent = $A.get("e.lightning:openFiles");
    if (openFileEvent) {
      $A.get("e.lightning:openFiles").fire({
        recordIds: [contentId]
      });
    } else {
      var contentRecordPageLink =
        "/lightning/r/ContentDocument/" + contentId + "/view";
      window.open(contentRecordPageLink, "_blank");

    }
  }
    
    
})