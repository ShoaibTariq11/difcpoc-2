({
  createApplicationCommHelper: function (component, event, helper) {
    try {
      debugger;
      var action = component.get("c.saveApplicationComment");
      var requestWrap = {
        commentMessage: component.get("v.comment"),
        InternalEmail: component.get("v.internalEmail"),
        RelatedSr: component.get("v.selectedValue")
      };
      action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(respWrap);

          if (!respWrap.errorMessage) {
            helper.showToast(
              component,
              event,
              helper,
              "Message sent",
              "information"
            );
            component.set("v.isOpenModal", false);
            helper.clearValues(component, event, helper);
          } else {
            helper.showToast(
              component,
              event,
              helper,
              respWrap.errorMessage,
              "error"
            );
          }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log(
            "@@@@@ Error Location " + response.getError()[0].stackTrace
          );
        }
      });
      $A.enqueueAction(action);
    } catch (error) {
      console.log(error.message);
    }
  },

  getPageFlows: function (component, event, helper, type) {
    var action = component.get("c.loadPageFlow");

    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        component.set("v.pageFlowDeatils", respWrap.pageFlowWrapLst);
        //console.log(respWrap);
        /* helper.setPickListVal(
          component,
          event,
          helper,
          respWrap.pageFlowWrapLst
        ); */
        helper.navToChatter(
          component,
          event,
          helper,
          respWrap.pageFlowWrapLst,
          type
        );
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
    });
    $A.enqueueAction(action);
  },

  navToChatter: function (component, event, helper, pgFlowList, type) {
    var isNavigated = false;
    for (const pgFlow of pgFlowList) {
      if (pgFlow.relatedLatestSR) {
        console.log(pgFlow.relatedLatestSR);
        if (
          type == "BD" &&
          (pgFlow.relatedLatestSR.HexaBPM__Record_Type_Name__c ==
            "In_Principle" ||
            pgFlow.relatedLatestSR.HexaBPM__Record_Type_Name__c ==
              "Commercial_Permission")
        ) {
          isNavigated = true;
          window.location.href = "service-request/" + pgFlow.relatedLatestSR.Id;
        } else if (
          type == "RS" &&
          (pgFlow.relatedLatestSR.HexaBPM__Record_Type_Name__c ==
            "AOR_Financial" ||
            pgFlow.relatedLatestSR.HexaBPM__Record_Type_Name__c == "AOR_NF_R")
        ) {
          isNavigated = true;
          window.location.href = "service-request/" + pgFlow.relatedLatestSR.Id;
        }
      }
    }

    if (isNavigated == false) {
      helper.showToast(
        component,
        event,
        helper,
        "No related request found",
        "error"
      );
    }
  },

  setPickListVal: function (component, event, helper, pgFlowList) {
    var options = [];
    for (const pgFlow of pgFlowList) {
      if (pgFlow.relatedLatestSR) {
        options.push({
          label: pgFlow.pageFlowObj.Name,
          value: pgFlow.relatedLatestSR.Id
        });
      }
    }

    component.set("v.options", options);
  },

  showToast: function (component, event, helper, message, type) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      mode: "dismissable",
      message: message,
      type: type,
      duration: 500
    });
    toastEvent.fire();
  },

  formValidator: function (component, event, helper, fieldsToValidate) {
    try {
      let fields = component.find(fieldsToValidate);

      if (fields == undefined) {
        return true;
      } else if (!Array.isArray(fields)) {
        if (fields.get("v.value") != undefined) {
          return fields.get("v.value").trim().length > 0 ? true : false;
        } else {
          return false;
        }
      } else if (Array.isArray(fields)) {
        for (const field of fields) {
          if (field.get("v.value") != undefined) {
            if (!field.get("v.value").trim().length > 0) {
              return false;
            }
          } else {
            return false;
          }
        }
        return true;
      }
    } catch (error) {
      console.log(error.message);
      console.log(error.stackTrace);
    }
  },

  reportValididty: function (component, event, helper, fieldsToReport) {
    try {
      let fields = component.find(fieldsToReport);

      if (!Array.isArray(fields)) {
        fields.reportValidity();
      } else {
        for (const field of fields) {
          field.reportValidity();
        }
      }
    } catch (error) {
      console.log(error.message);
    }
  },

  clearValues: function (component, event, helper, fieldsToClear) {
    component.set("v.comment", null);
    component.set("v.selectedValue", null);
  }
});