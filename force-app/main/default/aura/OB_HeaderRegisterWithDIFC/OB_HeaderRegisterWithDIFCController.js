({
  doinit: function(component, event, helper) {
    var title = window.document.title;
    component.set("v.pagename", title);
    var loginLink = component.find("Login");
    var registerLink = component.find("Register");
    var pageTitle = component.get("v.pagename");
    if (pageTitle == "Login Template Title") {
      $A.util.addClass(loginLink, "logincss");
      $A.util.removeClass(registerLink, "registercss");
    } else if (
      pageTitle == "" ||
      pageTitle == "RegisterWithDifc" ||
      pageTitle == "undefined"
    ) {
      $A.util.removeClass(loginLink, "logincss");
      $A.util.addClass(registerLink, "registercss");
    } else {
      $A.util.removeClass(registerLink, "registercss");
      $A.util.removeClass(loginLink, "logincss");
    }
  }

  /* registerRedirect : function(component, event, helper) {
        var nextPageURL = 'https://dcob1-portaldifc.cs100.force.com/RegisterWithDIFC';
        //var nextPageURL = 'https://dcob1-difcportal.cs100.force.com/digitalOnboarding/s/registerwithdifc';
        window.location.href =nextPageURL;
    },
    
    loginRedirect : function(component, event, helper) {
        //helper.gotoURL(component);
        //var nextPageURL = 'https://dcob1-difcportal.cs100.force.com/digitalOnboarding/loginDIFC';
        //window.location.href = 'https://dcob1-difcportal.cs100.force.com/digitalOnboarding/loginDIFC';
        var nextPageURL = 'https://dcob1-difcportal.cs100.force.com/digitalOnboarding';
        window.location.href = nextPageURL;
        
    } */
});