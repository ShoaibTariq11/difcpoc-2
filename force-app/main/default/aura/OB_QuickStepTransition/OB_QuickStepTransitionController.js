({
	doInit : function(component, event, helper) {
		var recordId = component.get("v.recordId1");
        var stepId = component.get("v.stepId");
        var action = component.get("c.getLstTransWrapper");
        action.setParams({
            recordId: recordId,
            stepId: stepId
        });
        console.log('something');
        action.setCallback(this,function(a){
            var wrapperObject = a.getReturnValue();
            console.log(wrapperObject);
            component.set("v.flsErrorCheck",wrapperObject.flsErrorCheck);
            console.log(component.get("v.flsErrorCheck"));
            component.set("v.flsErrorMessage",wrapperObject.flsErrorMessage);	
            component.set("v.lstTrnsWrap", wrapperObject.ltngLstTrnsWrap);
            component.set("v.hasAccess", wrapperObject.hasAccess);
            component.set("v.mapStepTransition", wrapperObject.ltngMapStepTransition);
            component.set("v.SRId", wrapperObject.SRId);
            component.set("v.SRRequestType", wrapperObject.SRRequestType);
            component.set("v.SRNumber", wrapperObject.SRNumber);
            component.set("v.isStepOwnedByQueue",wrapperObject.isStepOwnedByQueue);
            if(!wrapperObject.flsErrorCheck){
                component.set("v.step", wrapperObject.step_ltng);
                component.set("v.StepInstructions",wrapperObject.step_ltng.HexaBPM__SR_Step__r.HexaBPM__Step_Instructions__c);
                if(component.get("v.StepInstructions")!= null && component.get("v.StepInstructions")!=''){
                    component.set("v.InstructionsCheck",true);
                }else{
                    component.set("v.InstructionsCheck",false);
                }
            }else{
                if(wrapperObject.flsErrorMessage.indexOf('DML')!=-1){
                    component.set("v.dmlErrorCheck",true);
                }
            }
            console.log(wrapperObject.step_ltng);
            console.log(wrapperObject.ltngLstTrnsWrap);
            if(wrapperObject.ltngLstTrnsWrap.length==0 && !wrapperObject.flsErrorCheck){
                component.set("v.altInstructions","This step is closed.");
                component.set("v.closedStepCheck",true);
            }else if(wrapperObject.ltngLstTrnsWrap.length==0 && wrapperObject.flsErrorCheck){
                component.set("v.altInstructions","Please contact your system administrator for further clarifications.");
            }
            if(wrapperObject.userType && wrapperObject.userType == 'salesforce')
                component.set("v.isSalesforceUser",true);
           
        });
        $A.enqueueAction(action);
    },
    Accept:function(component,event,helper){
        component.set("v.isCustomError","false");
        var step =  component.get("v.step");
        // in the server-side controller
        var action = component.get("c.acceptAction");
        action.setParams({ 
            stepId : step.Id,
            isStepOwnedByQueue: component.get("v.isStepOwnedByQueue")
         });

        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // Alert the user with the value returned 
                // from the server
                console.log("From server: " + response.getReturnValue());
                component.set("v.isStepOwnedByQueue",response.getReturnValue()); //will be assigned false on success
                /*
                if(component.get("v.isStepOwnedByQueue") == false)
                    alert("Action Item Assigned Successfully.");
                else
                    alert("Action Item Released Successfully.");
                    */
                // You would typically fire a event here to trigger 
                // client-side notification that the server-side 
                // action is complete
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        // optionally set storable, abortable, background flag here

        // A client-side action could cause multiple events, 
        // which could trigger other events and 
        // other server-side action calls.
        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(action);
    },
    showOppmodal: function(component, event, helper) {
        console.log('EDITEVENT');
        //helper.AssignParentId(component,event);
		helper.toggleClass(component,'backdrop','slds-backdrop--');
		helper.toggleClass(component,'modaldialog','slds-fade-in-');
		
	},
	hideModal : function(component, event, helper) {
		 //Toggle CSS styles for hiding Modal
		helper.toggleClassInverse(component,'backdrop','slds-backdrop--');
		helper.toggleClassInverse(component,'modaldialog','slds-fade-in-');
	},
    showSpinner: function(component,event,helper){
        /*var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        evt.setParams({isVisible:true});
        evt.fire();*/
        var spinner = component.find('mySpinner');
        $A.util.removeClass(spinner,'slds-hide');
    },
    hideSpinner: function(component,event,helper){
      	/*var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        evt.setParams({isVisible:false});
        evt.fire();*/
        var spinner = component.find('mySpinner');
        $A.util.addClass(spinner,'slds-hide');
        
   	},
       
    SelectStatus: function(component, event, helper){
        
        var target = event.currentTarget;
        var statusId = target.dataset.id;
        var isRejStat = target.dataset.rej;
        var statCode = target.dataset.statuscode;
        component.set("v.selTransition", statusId);
    	var isNotesRequired = component.get("v.isNotesRequired");
        console.log(statusId);
        console.log(statCode);
        console.log(isRejStat);
    	var isSelStatusRej = isRejStat;
        component.set("v.isSelStatusRej", isRejStat);
        console.log(component.get("v.isSelStatusRej"));
        if(statCode!=null && (statCode=='REQUIRED_INFO_UPDATED' || statCode=='RETURN_FOR_MORE_INFO')){
            isNotesRequired = true;
            component.set("v.isNotesRequired", true);
        }
	},
    ProceedClick: function(component, event, helper){
        console.log('Proceed');
        var isStepOwnedByQueue = component.get("v.isStepOwnedByQueue");
        console.log(isStepOwnedByQueue);
        if(isStepOwnedByQueue){
            component.set("v.isCustomError","true");
            component.set("v.customErrorMsg","Please accept the Action item first before proceeding.");
            //alert("Please accept the Action item first before proceeding.");
            return;
        }
        helper.DisableLinks();
        var selStatusId = component.get("v.selTransition");
        console.log(selStatusId);
        component.set("v.RejReason", "");
        component.set("v.StepNotes","");
        var isSelStatusRej = component.get("v.isSelStatusRej");
        var isNotesRequired = component.get("v.isNotesRequired"); 
        if(selStatusId){
            if(document.getElementById('reason').value!=null && document.getElementById('reason').value!='' && document.getElementById('reason').value.trim()!='')
                component.set("v.StepNotes",document.getElementById('reason').value);
            
                if(isSelStatusRej==true || isSelStatusRej=='true'){
                    document.getElementById('notesErrorMsg').style.display = 'none';
                    if(document.getElementById('reason').value!=null && document.getElementById('reason').value!='' && document.getElementById('reason').value.trim()!=''){
                        document.getElementById('errorMsg').style.display = 'none';
                        component.set("v.RejReason",document.getElementById('reason').value);
                        helper.ChangeStatus(component,event);
                    }else{
                        document.getElementById('errorMsg').style.display = '';
                        document.getElementById('reason').focus();
                        helper.EnableLinks();       
                    }
                }else if(isNotesRequired==true){
                    document.getElementById('errorMsg').style.display = 'none';
                    if(document.getElementById('reason').value!=null && document.getElementById('reason').value!='' && document.getElementById('reason').value.trim()!=''){
                        document.getElementById('errorMsg').style.display = 'none';
                        component.set("v.StepNotes",document.getElementById('reason').value);
                        helper.ChangeStatus(component,event);
                    }else{
                        document.getElementById('notesErrorMsg').style.display = '';
                        document.getElementById('reason').focus();
                        helper.EnableLinks();
                    }
                }else{
                    helper.ChangeStatus(component,event);
                }
            
      	}else{
            console.log('not selected');
            component.set("v.flsErrorCheck","true");
            component.set("v.flsErrorMessage","Please select the status to proceed");
        	//alert('please select the status to proceed');
            helper.EnableLinks();
        }
    },
    Cancel: function(component, event, helper){
        //var navEvt = $A.get("e.force:navigateToSObject");
        //navEvt.setParams({ "recordId":component.get("v.SRId")}); 
        //navEvt.fire();
        //
        /*
        var action = component.get("c.CancelAction");
        var SRId = component.get("v.recordId1");
        action.setParams({
            SRID: SRId
        });
        action.setCallback(this, function(a){
        	//alert(a.getReturnValue());
        });
        $A.enqueueAction(action);
        */
        
        window.location.href = '/lightning/r/HexaBPM__Service_Request__c/'+component.get("v.SRId")+'/view';
        /*
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": '/'+component.get("v.SRId");
        });
        urlEvent.fire();
        */
		
    },
    ViewCurrentStep: function(component, event, helper){
        var action = component.get("c.ViewStep");
        var stepId = component.get("v.stepId");
        console.log(stepId);
        window.location.href = '/lightning/r/HexaBPM__Step__c/'+stepId+'/view';
        
        /*
        action.setParams({
            StepID: stepId
        })
        action.setCallback(this, function(a){
            //alert(a.getReturnValue());
        });
        $A.enqueueAction(action);
        */
        
        /*
        var urlEvent = $A.get("e.force:navigateToURL");
        var step = component.get("v.step");
        var stepId = step.Id;
        urlEvent.setParams({
            "url": "/"+stepId
        });
        urlEvent.fire();
        */
    }
    
})