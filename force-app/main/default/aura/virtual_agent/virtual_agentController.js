({
    doInit: function(cmp) {
        
         var UTC_hours = new Date().getUTCHours() +4;
        if (UTC_hours >= 8 && UTC_hours < 15)
        {
      	 cmp.set("v.virtualAgentVisibility",true);
        }
        else
        {
      	 cmp.set("v.virtualAgentVisibility",false);
            
        }
    }
})