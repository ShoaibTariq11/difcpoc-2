({
	
    
    initAmendment : function(cmp, event, helper)
    {
       
        var currTarget = event.currentTarget;
        var amedtype = currTarget.dataset.amedtype;
        
        console.log('######### amedtype ',amedtype);
        var initAmendment = cmp.get('c.initAmendmentDB');
        
        var reqWrapPram  =
                {
                    srId : cmp.get('v.srWrap').srObj.Id,
                amedtype : amedtype
                };
        
        initAmendment.setParams(
            {
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
        
        initAmendment.setCallback(this, 
                                      function(response) {
                                          var state = response.getState();
                                          console.log("callback state: " + state);
                                          
                                          if (cmp.isValid() && state === "SUCCESS") 
                                          {
                                              var respWrap = response.getReturnValue();
                                              console.log('######### respWrap.amedWrap  ',respWrap.amedWrap);
											  cmp.set('v.amedWrap',respWrap.amedWrap);	  
                                              cmp.set('v.amedId','');	  
                                              
                                          }
                                          else
                                          {
                                              console.log('@@@@@ Error '+response.getError()[0].message);
                                             
                                          }
                                      }
                                     );
            $A.enqueueAction(initAmendment);
            
            
       
    },
})