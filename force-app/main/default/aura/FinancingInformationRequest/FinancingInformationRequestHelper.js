({
    renderState: function(component) {
        var currentStep = component.get("v.currentStep"),
            allSteps = component.get("v.steps"),
            render = [],
            state = "slds-is-complete";
        allSteps.forEach(function(step) {
            if(currentStep === step) {
                state = "slds-is-current";
            } else if(state === "slds-is-current") {
                state = "slds-is-incomplete";
            }
            render.push({ label: step, selected: state === "slds-is-current", state: state });
        });
        component.set("v.renderInfo", render);
    },
    
    clientInfo : function(component, event) {
    	var action = component.get("c.userAccountInfo");
        action.setParams({
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var userinformation = response.getReturnValue();
                component.set("v.accountName", userinformation[0]);
                component.set("v.accountAddress", userinformation[1]);
            }
        });
        $A.enqueueAction(action);
    },
    
    createObjectData: function(component, event) {
        // get the contactList from component and add(push) New Object to List  
        var RowItemList = component.get("v.infoRequestList");
        RowItemList.push({
            'sobjectType': 'Information_Request__c',
            'Record_Number__c': '',
            'Date_Record_Filed__c': null,
            'Additional_identifying_Information__c': ''
        });
        console.log('--myinfo--');
        console.log(RowItemList);
        console.log('--myinfo--');
        // set the updated list to attribute (contactList) again
        component.set("v.infoRequestList", RowItemList);
    },
    
    // helper function for check if first Name is not null/blank on save  
    validateRequired: function(component, event) {
        var isValid = true;
        var selectedSearch = component.get("v.selectedSearchType");
        if(selectedSearch == '') {
            isValid = false;
            alert('Please select search type from the drop down!');
        } else if(selectedSearch == 'Search by Debtor Name') {
            var selectedDebtorType = component.find("selectDebtorType").get("v.value");
            var informationReq = component.get("v.infoRequest");
            if(selectedDebtorType == '') {
                isValid = false;
                alert('Please select debtor type!');
            } else if(selectedDebtorType == 'Organisation') {
                var selectedRecordType = component.find("selectRecordType").get("v.value");
                if(informationReq.Organisation_Name__c == '') {
                    isValid = false;
                    alert('Please enter Debor entity name!');
                } else if(selectedRecordType == '') {
                    isValid = false;
                    alert('Please select search response!');
                }
            } else if(selectedDebtorType == 'Individual') {
                var selectedRecordType = component.find("selectRecordType").get("v.value");
                if(informationReq.First_Name__c == '' && informationReq.Last_Name__c == '' && informationReq.Middle_Name__c == '') {
                    isValid = false;
                    alert('Debor Individual name should not be blank!');
                } else if(informationReq.First_Name__c == '' && informationReq.Last_Name__c == '') {
                    isValid = false;
                    alert('Debor Individual First/Last name is required!');
                } else if(selectedRecordType == '') {
                    isValid = false;
                    alert('Please select search response!');
                }
            }
        } else if(selectedSearch == 'Search specific copies') {
        	var allInfoRequestRows = component.get("v.infoRequestList");
            for (var indexVar = 0; indexVar < allInfoRequestRows.length; indexVar++) {
                if (allInfoRequestRows[indexVar].Record_Number__c == '') {
                    isValid = false;
                    alert('Record Number Can\'t be Blank on Row Number ' + (indexVar + 1));
                }
            }
        }
        return isValid;
    },
    
    validateRequiredSRDocs: function(component, srId) {
    	component.set("v.isValidateOpen",true);
        var action = component.get("c.fetchApprovedForm");
        action.setParams({
            "recId": srId
        });
        action.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS") {
                var apForm = response.getReturnValue();
                console.log(apForm.Documents_Pending__c);
                if(apForm.Documents_Pending__c == true) {
                    component.set("v.documentsUploaded",false);
                    component.set("v.approvedForm.Documents_Pending__c",true);
                } else if(apForm.Documents_Pending__c == false) {
                    component.set("v.documentsUploaded",true);
                    component.set("v.approvedForm.Documents_Pending__c",false);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchSRPricingInfo : function(component, srId) {
        var action = component.get("c.PopulateSR_Pricing");
        action.setParams({
            "recId": srId
        });
        action.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS") {
                var responseWrapper = response.getReturnValue();
                console.log('--wrapper--');
                console.log(responseWrapper.lstSRPriceItems);
                console.log('--wrapper--');
                component.set("v.SRPricingInfo", responseWrapper);
            }
        });
        $A.enqueueAction(action);
    },
    
    loadSR : function(component, srId) {
    	var action = component.get("c.fetchSR");
        action.setParams({
            recId : srId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log(result);
                component.set("v.serviceRequest", result);
                component.set("v.approvedForm", result.Approved_Forms__r[0]);
                component.set("v.infoRequestList", result.Information_Requests__r);
            }
        });
        $A.enqueueAction(action);
    },
    
    loadApprovedRequestForm : function(component, srId) {
    	var action = component.get("c.fetchApprovedForm");
        action.setParams({
            recId : srId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.approvedForm", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})