({
    doInIt: function(component, event, helper) {
        helper.clientInfo(component, event);
        helper.renderState(component, event);
        //helper.createObjectData(component, event);
        if(component.get("v.serviceRequestId") !== undefined || component.get("v.serviceRequestId") !== null || component.get("v.serviceRequestId") !== '') {
            helper.loadSR(component,component.get("v.serviceRequestId"));
            helper.loadApprovedRequestForm(component,component.get("v.serviceRequestId"));
            helper.fetchSRPricingInfo(component,component.get("v.serviceRequestId"));
        }
    },
    
    click: function(component, event, helper) {
        component.set("v.isValidateOpen",false);
        var index = parseInt(event.target.closest("[data-index]").dataset.index), 
            value, onselect, steps = component.get("v.steps");
        if(index !== undefined) {
            var formId = component.get("v.approvedFormId");
            var docsUploaded = component.get("v.documentsUploaded");
            if(formId === undefined && component.get("v.serviceRequestId") === null) {
            	alert('Please save Information Request to proceed further...');
            } else if(formId !== undefined && component.get("v.serviceRequestId") === null && docsUploaded === false && steps[index] == 'Pricing') {
                alert('Please upload mandatory documents in documents section...');
            } else if(component.get("v.serviceRequestId") !== null && (component.get("v.approvedForm").Documents_Pending__c === true) && steps[index] == 'Pricing') {
            	alert('Please upload mandatory documents in documents section...');
            } else if((formId !== undefined || component.get("v.serviceRequestId") !== null) && steps[index] == 'Documents') {
                event.preventDefault();
                value = steps[index];
                component.set("v.currentStep", value);
                onselect = component.getEvent("onselect");
                onselect.setParams({ value: value });
                onselect.fire();
                helper.renderState(component);
            } else if(formId !== undefined && component.get("v.serviceRequestId") === null && docsUploaded === true && steps[index] == 'Pricing') {
                event.preventDefault();
                value = steps[index];
                component.set("v.currentStep", value);
                onselect = component.getEvent("onselect");
                onselect.setParams({ value: value });
                onselect.fire();
                helper.renderState(component);
            } else if(component.get("v.serviceRequestId") !== null && (component.get("v.approvedForm").Documents_Pending__c === false || docsUploaded === true) && steps[index] == 'Pricing') {
                event.preventDefault();
                value = steps[index];
                component.set("v.currentStep", value);
                onselect = component.getEvent("onselect");
                onselect.setParams({ value: value });
                onselect.fire();
                helper.renderState(component);
            } else if(steps[index] == 'Information Request') {
                event.preventDefault();
                value = steps[index];
                component.set("v.currentStep", value);
                onselect = component.getEvent("onselect");
                onselect.setParams({ value: value });
                onselect.fire();
                helper.renderState(component);
            }
        }
    },
    
    update: function(component, event, helper) {
        var formId = component.get("v.approvedFormId");
        if(formId !== undefined) {
            helper.renderState(component, event);
        } else if(formId === undefined && component.get("v.serviceRequestId") === null) {
            alert('Please save Information Request to proceed further...');
        }
    },
    
    //Search Debtor's/Party's Org name or individual name drop down selection
    onChangeSearchType: function (component, event, helper) {
        var selectedValue = component.find("selectSearchType").get("v.value");
        component.set("v.selectedSearchType",selectedValue);
        if(selectedValue == 'Search specific copies') {
            helper.createObjectData(component, event);
        }
        component.set("v.approvedForm.Information_Request_Type__c",selectedValue);
    },
    
    //Search Debtor's/Party's Org name or individual name drop down selection
    onChangeDebtorType: function (component, event, helper) {
        var selectedValue = component.find("selectDebtorType").get("v.value");
        if(selectedValue == 'Organisation') {
            component.set("v.infoRequest.First_Name__c","");
            component.set("v.infoRequest.Last_Name__c","");
            component.set("v.infoRequest.Middle_Name__c","");
        } else if(selectedValue == 'Individual') {
            component.set("v.infoRequest.Organisation_Name__c","");
        }
        component.set("v.debtorType",selectedValue);
    },
    
    onChangeResponseType: function (component, event, helper) {
        var selectedValue = component.find("selectRecordType").get("v.value");
        if(selectedValue == 'All') {
            component.set("v.infoRequest.Search_Response__c","All");
        } else if(selectedValue == 'Unlapsed') {
            component.set("v.infoRequest.Search_Response__c","Unlapsed");
        } else {
            component.set("v.infoRequest.Search_Response__c","");
        }
        component.set("v.approvedForm.Search_Response__c",selectedValue);
    },
    
    // function for create new object Row in Contact List 
    addNewRow: function(component, event, helper) {
        // call the comman "createObjectData" helper method for add new Object Row to List  
        helper.createObjectData(component, event);
    },
 
    // function for delete the row 
    removeDeletedRow: function(component, event, helper) {
        // get the selected row Index for delete, from Lightning Event Attribute  
        var index = event.getParam("indexVar");
        // get the all List (contactList attribute) and remove the Object Element Using splice method    
        var AllRowsList = component.get("v.infoRequestList");
        AllRowsList.splice(index, 1);
        // set the contactList after remove selected row element  
        component.set("v.infoRequestList", AllRowsList);
    },
    
    closeForm: function(component, event, helper) {
        var serviceId = component.get("v.serviceReqId");
        var urlEvent = $A.get("e.force:navigateToURL");
        if(serviceId === null) {
            urlEvent.setParams({
                "url": '/company-services'
            });
        } else {
            urlEvent.setParams({
                "url": '/service-request/'+serviceId
            });
        }
        urlEvent.fire();
    },
    
    // function for save the Records 
    SaveInfoRequest: function(component, event, helper) {
        if(helper.validateRequired(component, event)) {
            //var selectedSearch = component.get("v.selectedSearchType");
            var selectedSearchValue = component.find("selectSearchType").get("v.value");
            var newInfoList;
            if(selectedSearchValue == 'Search by Debtor Name') {
                var infoReq = component.get("v.infoRequest");
                var infoList = component.get("v.infoRequestListByName");
                infoList.push({infoReq});
                var arrayList = JSON.stringify(infoList);
                               console.log(arrayList);
				if(infoReq.Search_Response__c == 'All') {
                	arrayList = arrayList.replace('"All"}}','"All"}');
                	arrayList = arrayList.replace('{"infoReq":','');
                	arrayList = arrayList.replace('}}]','}]');
                } else if(infoReq.Search_Response__c == 'Unlapsed') {
                    arrayList = arrayList.replace('"Unlapsed"}}','"Unlapsed"}');
                    arrayList = arrayList.replace('{"infoReq":','');
                    arrayList = arrayList.replace('}}]','}]');
                }
                newInfoList = JSON.parse(arrayList);
			} else if(selectedSearchValue == 'Search specific copies') {
            	newInfoList = component.get("v.infoRequestList");
            }
            var action = component.get("c.saveInformationRequest");
            action.setParams({
				"af": component.get("v.approvedForm"),
                "informationList": newInfoList
            });
            // set call back 
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {
                    var results = response.getReturnValue();
                    component.set("v.serviceReqId", results[0]);
                    component.set("v.approvedFormId", results[1]);
                    if(results[2] == 'true') {
                    	component.set("v.documentsUploaded", false);
                        component.set("v.approvedForm.Documents_Pending__c",true)
                    } else if(results[2] == 'false') {
                    	component.set("v.documentsUploaded", true);
                        component.set("v.approvedForm.Documents_Pending__c",false)
                    }
                    component.set("v.infoRequestList",newInfoList);
                    component.set("v.messageType","success");
                    component.set("v.messageVariant","inverse");
                    component.set("v.message", "Service Request generated Successfully!");
                    var srIRColumns =  [
                        { label: "Organisation", fieldName: "Organisation_Name__c", type: "text" },
                        { label: "First Name", fieldName: "First_Name__c", type: "text" },
                        { label: "Middle Name", fieldName: "Middle_Name__c", type: "text" },
                        { label: "Last Name", fieldName: "Last_Name__c", type: "text" },
                        { label: "Record Number", fieldName: "Record_Number__c", type: "text" },
                        { label: "Date Record Filed", fieldName: "Date_Record_Filed__c", type: "date", typeAttributes:{ year: "numeric", month: "2-digit", day: "2-digit"} },
                        { label: "Additional identifying Information", fieldName: "Additional_identifying_Information__c", type: "text" },
                        { label: "Search Response", fieldName: "Search_Response__c", type: "text" }
                    ];
                    
                    $A.createComponent("c:singleRelatedList", { 
                        "currId": results[0], 
                        "sobjectApiName": "Information_Request__c",
                        "relatedFieldApiName": "Service_Request__c", 
                        "sortedBy": "CreatedDate",
                        "sortedDirection": "ASC",
                        "fields": "Organisation_Name__c,First_Name__c,Middle_Name__c,Last_Name__c,Record_Number__c,Date_Record_Filed__c,Additional_identifying_Information__c,Search_Response__c",
                        "columns": srIRColumns
                    }, function(newCmp, status, errorMessage) {
                        if (status === "SUCCESS") {
                            var firstPanel = component.get("v.firstPanel");
                            firstPanel.push(newCmp);
                            component.set("v.firstPanel", firstPanel);
                        }
                    });
                    component.set("v.currentStep","Documents");
                    helper.fetchSRPricingInfo(component,results[0]);
                    var currentStep = "Documents",
                        allSteps = component.get("v.steps"),
                        render = [],
                        state = "slds-is-complete";
                    component.set("v.renderInfo", render);
                    allSteps.forEach(function(step) {
                        if(currentStep === step) {
                            state = "slds-is-current";
                        } else if(state === "slds-is-current") {
                            state = "slds-is-incomplete";
                        }
                        render.push({ label: step, selected: state === "slds-is-current", state: state });
                    });
                    component.set("v.renderInfo", render);
                    window.scrollTo(0, 0);
                } else if(state === "ERROR") {
                    component.set("v.messageType","error");
                    var errors = action.getError();
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            component.set("v.message",errors[0].message);
                            component.set("v.messageVariant","inverse");
                            window.scrollTo(0, 0);
                        }
                    } else {
                        component.set("v.message", "Request Failed!");
                        component.set("v.messageVariant","inverse");
                        window.scrollTo(0, 0);
                    }
                } else if (status === "INCOMPLETE") {
                    component.set("v.messageType","error");
                    component.set("v.messageVariant","inverse");
                    component.set("v.message", "No response from server or client is offline.");
                    window.scrollTo(0, 0);
                }
            });
            // enqueue the server side action  
            $A.enqueueAction(action);
        }
    },
 
    refreshDocuments: function(component, event, helper) {
        component.set("v.isValidateOpen",false);
        var action = component.get("c.fetchApprovedForm");
        var srId;
        if(component.get("v.serviceRequestId") !== null) {
            srId = component.get("v.serviceRequestId");
        } else if(component.get("v.serviceReqId") !== null) {
            srId = component.get("v.serviceReqId");
        }
        action.setParams({
            "recId": srId
        });
        action.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS") {
                var apForm = response.getReturnValue();
                if(apForm.Documents_Pending__c == true) {
                    component.set("v.documentsUploaded",false);
                    component.set("v.approvedForm.Documents_Pending__c",true);
                } else if(apForm.Documents_Pending__c == false) {
                    component.set("v.documentsUploaded",true);
                    component.set("v.approvedForm.Documents_Pending__c",false);
                }
            }
        });
        $A.enqueueAction(action);
    },

	validateForm: function(component, event, helper) {
        var srId;
        if(component.get("v.serviceRequestId") !== null) {
            srId = component.get("v.serviceRequestId");
        } else if(component.get("v.serviceReqId") !== null) {
            srId = component.get("v.serviceReqId");
        }
        helper.validateRequiredSRDocs(component, srId);
    },

	cancelValidation: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isValidateOpen", false);
    },

	updateForm: function(component, event, helper) {
        var appRecordId;
        if(component.get("v.serviceRequestId") !== null) {
            appRecordId = component.get("v.approvedForm").Id;
        } else if(component.get("v.serviceReqId") !== null) {
            appRecordId = component.get("v.approvedFormId");
        }
        var action = component.get("c.updateRequest");
        action.setParams({
            "recordId": appRecordId,
            "description": "",
            "noOfPages": ""
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                //component.set("v.documentsUploaded",true);
                component.set("v.docsapprovedFormId", response.getReturnValue());
                component.set("v.messageType","success");
                component.set("v.messageVariant","inverse");
                component.set("v.message", "Changes saved successfully!");
                component.set("v.currentStep","Pricing");
                var currentStep = "Pricing",
                    allSteps = component.get("v.steps"),
                    render = [],
                    state = "slds-is-complete";
                component.set("v.renderInfo", render);
                allSteps.forEach(function(step) {
                    if(currentStep === step) {
                        state = "slds-is-current";
                    } else if(state === "slds-is-current") {
                        state = "slds-is-incomplete";
                    }
                    render.push({ label: step, selected: state === "slds-is-current", state: state });
                });
                component.set("v.renderInfo", render);
                window.scrollTo(0, 0);
            } else if(state === "ERROR") {
                component.set("v.messageType","error");
                var errors = action.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        component.set("v.message",errors[0].message);
                        component.set("v.messageVariant","inverse");
                        window.scrollTo(0, 0);
                    }
                } else {
                    component.set("v.message", "Request Failed!");
                    component.set("v.messageVariant","inverse");
                    window.scrollTo(0, 0);
                }
            } else if (status === "INCOMPLETE") {
                component.set("v.messageType","error");
                component.set("v.messageVariant","inverse");
                component.set("v.message", "No response from server or client is offline.");
                window.scrollTo(0, 0);
            }
        });
        $A.enqueueAction(action);
    },
	
	submitForm: function(component, event, helper) {
        var insufficientBalance = component.get("v.SRPricingInfo.insufficientBalance");
        if(!insufficientBalance) {
            var serviceId;
            if(component.get("v.serviceRequestId") !== null) {
                serviceId = component.get("v.serviceRequestId");
            } else if(component.get("v.serviceReqId") !== null) {
                serviceId = component.get("v.serviceReqId");
            }
            var action = component.get("c.submitRequest");
            action.setParams({
                "recordId": serviceId,
                "apId" : "",
                "collateral" : ""//component.get("v.approvedForm.Collateral_Description__c")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {
                    var results = response.getReturnValue();
                    component.set("v.messageType","success");
                    component.set("v.messageVariant","inverse");
                    component.set("v.message", "Your Request submitted Successfully!");
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": '/service-request/'+serviceId
                    });
                    urlEvent.fire();
                } else if(state === "ERROR") {
                    component.set("v.messageType","error");
                    var errors = action.getError();
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            component.set("v.message",errors[0].message);
                            component.set("v.messageVariant","inverse");
                            window.scrollTo(0, 0);
                        }
                    } else {
                        component.set("v.message", "Request Failed!");
                        component.set("v.messageVariant","inverse");
                        window.scrollTo(0, 0);
                    }
                } else if (status === "INCOMPLETE") {
                    component.set("v.messageType","error");
                    component.set("v.messageVariant","inverse");
                    component.set("v.message", "No response from server or client is offline.");
                    window.scrollTo(0, 0);
                }
            });
            $A.enqueueAction(action);
        } else {
            alert('You dont have enough balance to Process the Request, Please click on the Top-up Balance in the Sidebar.');
        }
    },
    
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },
    
    hideSpinner : function(component,event,helper) {   
        component.set("v.Spinner", false);
    }
})