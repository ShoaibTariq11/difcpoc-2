({
    getInput: function (component,event, helper) {
        
    helper.handleLogin(component,event, helper);
        
    },
    
    formpress: function(component,event,helper){
        if (event.getParams().keyCode  == 13) {
			helper.handleLogin(component,event, helper);
		}
    },
    
    setStartUrl: function (component,event, helpler) {
        var startUrl = event.getParam('startURL');
        console.log(startUrl);
        if (startUrl) {
            component.set("v.startUrl", startUrl);
        }
    },
 
        showToast: function(title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            message: message
        });
        toastEvent.fire();
    },
    closeModel: function(cmp, event, helper) {
       
        //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
        console.log("--toast close--");
        cmp.set("v.errorMessage", "");
        cmp.set("v.isError", false);
    }
    
    
})