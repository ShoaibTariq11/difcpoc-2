({
  saveSecurityUser : function(component, event, helper) {
		var action = component.get("c.insertNewSecurityUsers");
        action.setParams({securityUser:component.get("v.securityUsers") });
     	action.setCallback(this, function(response) {
         	var state = response.getState();
            console.log('!!@@##'+response.getReturnValue());
            
			if (state === "SUCCESS") {
             	var urlEvent = $A.get("e.force:navigateToURL");   
                urlEvent.setParams({
      				"url": "https://uatfull-difcportal.cs128.force.com/digitalOnboarding/s/dfsathankyoupage"
    			});
                urlEvent.fire();
            }  
        })   

        $A.enqueueAction(action);
	},
    gotoURL : function (component, event, helper) {
    var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
      "url": "https://uatfull-difcportal.cs128.force.com/digitalOnboarding/s/"
    });
    	urlEvent.fire();
	}
})