({
    getList : function(component,event,helper) {
        var action = component.get("c.checkName");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var returnVal = response.getReturnValue();
                component.set("v.retValue",returnVal);
                console.log("name criterias returned", returnVal);
            }
        }); 
        $A.enqueueAction(action); 
    },
    
    duplicateCheck:function(component,event,helper) {
        var action1 = component.get("c.checkAccountName");
        action1.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var returnValDup = response.getReturnValue();
                component.set("v.retAccName",returnValDup);
                console.log("dup values returned", returnValDup);
            }
        }); 
        $A.enqueueAction(action1);
    },
    
    getReservedNames:function(component,event,helper) {
        var action1 = component.get("c.getReservedNames");
        action1.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var returnValRes= response.getReturnValue();
                component.set("v.retReservedNames",returnValRes);
                console.log("reserved values returned", JSON.stringify(component.get("v.retReservedNames")));
            }
        }); 
        $A.enqueueAction(action1);
    },
    
})