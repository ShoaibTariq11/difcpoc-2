({
    
    doInit : function(component, event, helper) {
        let newURL = new URL(window.location.href).searchParams;
        var valueFromURL = newURL.get('valueTextEntity');
        component.set("v.NameId",newURL.get('NameId'));
        var nameId = component.get("v.NameId");
        
        if(nameId && nameId!='undefined'){
            component.set("v.NameIdtoController",nameId);
        }
        console.log("NameIdtoController is "+component.get("v.NameIdtoController"));
        console.log("value received do init from URL is "+valueFromURL);
        
        //If the Name passed contains only the space(Empty value)
        if(valueFromURL=='%20'){
           valueFromURL = valueFromURL.replace(/%20/g,'');       
        }
        
        //If the Name passed contains undefined(Empty value)
        valueFromURL = valueFromURL.replace(/undefined/g,'');
        
        component.set("v.valueTextEntity",valueFromURL);

        //If the Name passed contains space in between
        valueFromURL = valueFromURL.replace(/%20/g,' ');       
        console.log("value after  do init  replacing space "+valueFromURL);
        console.log("id passed from Namecheckblock is "+nameId);
        component.set("v.valueTextEntity",valueFromURL);
        
        helper.duplicateCheck(component,event,helper);
        helper.getList(component, event, helper);
        helper.getReservedNames(component, event, helper);
    },
    
    handleBlur : function(component, event, helper) { 

        var nameEntered = component.get("v.valueTextEntity");
        component.set("v.valueText",nameEntered);
        var enteredValue = component.get("v.valueText");
        
       //The name criteria evaluation check is executed only if the entered value is not null
       
        if(enteredValue && enteredValue!='undefined'){
            var lenRet = component.get("v.retValue");
            console.log("length of retValue "+lenRet.length); 
            var lenAcc = component.get("v.retAccName");
            console.log("lenAcc is "+lenAcc);
            var lenReservedNames = component.get("v.retReservedNames");
            console.log("lenReservedNames is "+lenReservedNames);
        
        var warn = [];
        var warnAcc = [];
        var warnDoc =[];
        var warnReason =[];
        var warnProh =[];
        var warnRes=[];
        var warnLegal=[];
        
        component.set("v.showConfirmName",true);
        component.set("v.showSubmitName",false);
        
        component.set("v.pValue","");
        component.set("v.pValueDoc","");
        component.set("v.pValueAllow",false);
        component.set("v.pValueReason","");
        component.set("v.pValueDup","");
        component.set("v.pValueProh","");
        component.set("v.pValueReserve","");
        component.set("v.pValueLegal","");
        
        warn = [];
        warnDoc =[];
        warnReason =[];
        warnAcc = [];
        warnProh =[];
        warnRes=[];
        warnLegal=[];
        
        component.set("v.notAllowedName","");
        component.set("v.notAllowedNameDoc","");
        component.set("v.notAllowedNameReason","");
        component.set("v.notAllowedNameDup","");
        component.set("v.notAllowedProh","");
        component.set("v.notAllowedNameReserve","");
        component.set("v.notAllowedNameLegal","");
        
        component.set("v.displayWarn",false);
        component.set("v.displayWarnDoc",false);
        component.set("v.displayWarnReason",false);
        component.set("v.displayWarnDup",false);
        component.set("v.displayWarnProh",false);
        component.set("v.displayWarnReserve",false);
        component.set("v.displayWarnLegal",false);
        
        component.set("v.showName",false);
        
        enteredValue = enteredValue.replace(/À|Á|Â|Ã|Ä|Å|Ā|Ă|Ą|Ǻ/g,'A'); 
        enteredValue = enteredValue.replace(/Æ|Ǽ/g,'AE'); 
        enteredValue = enteredValue.replace(/Ç|Ć|Ĉ|Ċ|Č/g,'C'); 
        enteredValue = enteredValue.replace(/Þ|Ď|Đ/g,'D'); 
        enteredValue = enteredValue.replace(/È|É|Ê|Ë|Ē|Ĕ|Ė|Ę|Ě/g,'E'); 
        enteredValue = enteredValue.replace(/Ĝ|Ğ|Ġ|Ģ/g,'G'); 
        enteredValue = enteredValue.replace(/Ĥ|Ħ/g,'H'); 
        enteredValue = enteredValue.replace(/Ì|Í|Î|Ï|Ĩ|Ī|Ĭ|Į|İ/g,'I'); 
        enteredValue = enteredValue.replace(/Ĵ/g,'J'); 
        enteredValue = enteredValue.replace(/Ķ/g,'K'); 
        enteredValue = enteredValue.replace(/Ĺ|Ļ|Ľ|Ŀ|Ł/g,'L');
        enteredValue = enteredValue.replace(/Ñ|Ń|Ņ|Ň|Ŋ/g,'N');
        enteredValue = enteredValue.replace(/Ò|Ó|Ô|Õ|Ö|Ø|Ō|Ŏ|Ő|Ǿ/g,'O');
        enteredValue = enteredValue.replace(/Ŕ|Ŗ|Ř/g,'R');
        enteredValue = enteredValue.replace(/Ś|Ŝ|Ş|Š/g,'S');
        enteredValue = enteredValue.replace(/Ţ|Ť|Ŧ/g,'T');
        enteredValue = enteredValue.replace(/Ù|Ú|Û|Ü|Ũ|Ū|Ŭ|Ů|Ű|Ų/g,'U');
        enteredValue = enteredValue.replace(/Ŵ|Ẁ|Ẃ|Ẅ/g,'W');
        enteredValue = enteredValue.replace(/Ỳ|Ý|Ŷ|Ÿ/g,'Y');
        enteredValue = enteredValue.replace(/Ź|Ż|Ž/g,'Z');
        
        enteredValue = enteredValue.replace(/à|á|â|ã|ä|å|ā|ă|ą|ǻ/g,'A');       
        enteredValue = enteredValue.replace(/æ|ǽ/g,'AE'); 
        enteredValue = enteredValue.replace(/ç|ć|ĉ|ċ|č/g,'C'); 
        enteredValue = enteredValue.replace(/þ|ď|đ/g,'D'); 
        enteredValue = enteredValue.replace(/è|é|ê|ë|ē|ĕ|ė|ę|ě/g,'E'); 
        enteredValue = enteredValue.replace(/ĝ|ğ|ġ|ģ/g,'G'); 
        enteredValue = enteredValue.replace(/ĥ|ħ/g,'H'); 
        enteredValue = enteredValue.replace(/ì|í|î|ï|ĩ|ī|ĭ|į|i/g,'I'); 
        enteredValue = enteredValue.replace(/ĵ/g,'J'); 
        enteredValue = enteredValue.replace(/ķ/g,'K'); 
        enteredValue = enteredValue.replace(/ĺ|ļ|ľ|ŀ|ł/g,'L');
        enteredValue = enteredValue.replace(/ñ|ń|ņ|ň|ŋ/g,'N');
        enteredValue = enteredValue.replace(/ò|ó|ô|õ|ö|ø|ō|ŏ|ő|ǿ/g,'O');
        enteredValue = enteredValue.replace(/oe/g,'OE');
        enteredValue = enteredValue.replace(/ŕ|ŗ|ř/g,'R');
        enteredValue = enteredValue.replace(/ś|ŝ|ş|š/g,'S');
        enteredValue = enteredValue.replace(/ţ|ť|ŧ/g,'T');
        enteredValue = enteredValue.replace(/ù|ú|û|ü|ũ|ū|ŭ|ů|ű|ų/g,'U');
        enteredValue = enteredValue.replace(/ŵ|ẁ|ẃ|ẅ/g,'W');
        enteredValue = enteredValue.replace(/ỳ|ý|ŷ|ÿ/g,'Y');
        enteredValue = enteredValue.replace(/ź|ż|ž/g,'Z');
        
        var checkProh = enteredValue;
        
        enteredValue = enteredValue.replace(/0/g,'ZERO');
        enteredValue = enteredValue.replace(/1/g,'ONE');
        enteredValue = enteredValue.replace(/2/g,'TWO');
        enteredValue = enteredValue.replace(/3/g,'THREE');
        enteredValue = enteredValue.replace(/4/g,'FOUR');
        enteredValue = enteredValue.replace(/5/g,'FIVE');
        enteredValue = enteredValue.replace(/6/g,'SIX');
        enteredValue = enteredValue.replace(/7/g,'SEVEN');
        enteredValue = enteredValue.replace(/8/g,'EIGHT');
        enteredValue = enteredValue.replace(/9/g,'NINE');
        
        var enteredValueUpper = enteredValue.toUpperCase();
        var enteredValueRemoveSpecial = enteredValueUpper.replace(/[^a-zA-Z0-9{}()[]]/g, ''); 
        var enteredValueRemoveSpecial = enteredValueRemoveSpecial.replace(/ /g, ''); 
        
        var regExpSimple = /\(([^)]+)\)/;
        var matchesSimple = regExpSimple.exec(enteredValueRemoveSpecial);
        
        var regExpSquare = /\[([^)]+)\]/;
        var matchesSquare= regExpSquare.exec(enteredValueRemoveSpecial);
        
        var regExpFlower = /\{([^)]+)\}/;
        var matchesFlower= regExpFlower.exec(enteredValueRemoveSpecial);
        
        console.log("matchesSimple "+ matchesSimple);
        console.log("matchesSquare "+ matchesSquare);
        console.log("matchesFlower "+ matchesFlower);
        
        for(var i=0;i<lenRet.length;i++){
            var str = enteredValue.toUpperCase();
            var removeSpecial = str.replace(/[^a-zA-Z0-9(){}]/g, ''); 
            
            //do not change the value of removeSpecial after this   
            var removeBrac = removeSpecial.replace(/[(){} ]/g, ''); // do not remove space inside square bracket
            var removeFinal =  removeBrac.replace(/AND/g, '');  
            var removeFinal =  removeFinal.replace(/OF/g, ''); 
            
            /*Checking bad words*/
            var strProh = checkProh.toUpperCase();
            var strProh = strProh.replace(/AND/g, '');  	
            var strProh = strProh.replace(/OF/g, ''); 
            var removeFinalProh = strProh.replace(/[^a-zA-Z0-9$ ]/g, ''); 
            
            var rule= lenRet[i].Rule_Name__c.toUpperCase();
            rule = rule.replace(/&| /g, '');            
            rule = rule.replace(/AND/g, ''); 
            rule = rule.replace(/OF/g, ''); 
            
            if(matchesSimple==null){
                matchesSimple= "test";
            }
            
            if(matchesSquare==null){
                matchesSquare= "test";
            }
            
            if(matchesFlower==null){
                matchesFlower= "test";
            }
            
            if(matchesSimple[1].includes("DFSA") || matchesSimple[1].includes("DUBAIFINANCIALSERVICESAUTHORITY") || matchesSimple[1].includes("DIFC") || matchesSimple[1].includes("DUBAIINTERNATIONALFINANCIALCENTRE") || matchesSimple[1].includes("DUBAIINTERNATIONALFINANCIALCENTER") || matchesSimple[1].includes("DUBAIINTERNATIONALFINANCIALCENTREAUTHORITY") || matchesSimple[1].includes("DIFCA") || matchesSimple[1].includes("DIFCCOURT") || matchesSimple[1].includes("DISPUTERESOLUTIONAUTHORITY")  || matchesSimple[1].includes("DRA")  || matchesSimple[1].includes("DIFCWILLSANDPROBATEREGISTRY")|| matchesSquare[1].includes("DFSA") || matchesSquare[1].includes("DUBAIFINANCIALSERVICESAUTHORITY") || matchesSquare[1].includes("DIFC") || matchesSquare[1].includes("DUBAIINTERNATIONALFINANCIALCENTRE") || matchesSquare[1].includes("DUBAIINTERNATIONALFINANCIALCENTER") || matchesFlower[1].includes("DFSA") || matchesFlower[1].includes("DUBAIFINANCIALSERVICESAUTHORITY") || matchesFlower[1].includes("DIFC")|| matchesFlower[1].includes("DUBAIINTERNATIONALFINANCIALCENTRE") || matchesFlower[1]==("DUBAIINTERNATIONALFINANCIALCENTER")){
                var removeSimpleBracketString = removeSpecial.replace(/\([^()]*\)/g, '');
                var removeSquareBracketString = removeSimpleBracketString.replace(/\[.*?\]/g,'');
                var removeFlowerBracketString = removeSquareBracketString.replace(/\{.*?\}/g,'');
                console.log("removeFlowerBracketString "+removeFlowerBracketString);
                var removeAND = removeFlowerBracketString.replace(/AND/g, '');
                removeAND = removeAND.replace(/OF/g, '');
                removeFinal = removeAND.replace(/ /g, '');
            }
            
            var enteredValue= str.replace(/\[.*?\]/g,'');           
            var n = removeFinal.includes(rule);
            var stringSplit= removeFinalProh.split(" ");
            
            for (var j=0;j<stringSplit.length;j++){
                if (rule==stringSplit[j] && lenRet[i].Warning_Type__c=="Error" ){
                    warnProh.push(lenRet[i].Rule_Name__c);
                    console.log("warnProh array", warnProh);
                }
            }
            
            if(lenRet[i].Warning_Type__c=="Warning Message" && n == true) {
                warn.push(lenRet[i].Rule_Name__c);   
                console.log("warn array", warn);
            }
            
            if(lenRet[i].Warning_Type__c=="Warning Message and Upload Doc" && n==true){
                if(removeFinal!="DUBAIINTERNATIONALFINANCIALCENTER" && removeFinal!="DUBAIINTERNATIONALFINANCIALCENTRE" && removeFinal!="DUBAIFINANCIALSERVICESAUTHORITY"){
                    warnDoc.push(lenRet[i].Rule_Name__c);
                    console.log("warnDoc array", warnDoc);
                }
            } 
            
            if(lenRet[i].Warning_Type__c=="Warning Message and Reason Text" && n==true){
                warnReason.push(lenRet[i].Rule_Name__c);
                console.log("warnReason array", warnReason);
            } 
            
            if(lenRet[i].Warning_Type__c=="Legal Structure" && n==true){
                warnLegal.push(lenRet[i].Rule_Name__c);
                console.log("warnLegal array", warnLegal);
            } 
        }
        
        
        for(var i=0;i<lenAcc.length;i++){
            var accUpper = enteredValue.toUpperCase();
            var removeAccFinal = accUpper.replace(/[^a-zA-Z0-9]/g, ''); 
            
            removeAccFinal = removeAccFinal.replace(/AND/g, '');
            removeAccFinal = removeAccFinal.replace(/OF/g, '');
            
            var ruleAcc= lenAcc[i].Name.toUpperCase();
            ruleAcc = ruleAcc.replace(/[^a-zA-Z0-9$]/g, ''); 
            
            ruleAcc = ruleAcc.replace(/AND/g, '');
            ruleAcc = ruleAcc.replace(/OF/g, '');
            
            ruleAcc = ruleAcc.replace(/À|Á|Â|Ã|Ä|Å|Ā|Ă|Ą|Ǻ/g,'A'); 
            ruleAcc = ruleAcc.replace(/Æ|Ǽ/g,'AE'); 
            ruleAcc = ruleAcc.replace(/Ç|Ć|Ĉ|Ċ|Č/g,'C'); 
            ruleAcc = ruleAcc.replace(/Þ|Ď|Đ/g,'D'); 
            ruleAcc = ruleAcc.replace(/È|É|Ê|Ë|Ē|Ĕ|Ė|Ę|Ě/g,'E'); 
            ruleAcc = ruleAcc.replace(/Ĝ|Ğ|Ġ|Ģ/g,'G'); 
            ruleAcc = ruleAcc.replace(/Ĥ|Ħ/g,'H'); 
            ruleAcc = ruleAcc.replace(/Ì|Í|Î|Ï|Ĩ|Ī|Ĭ|Į|İ/g,'I'); 
            ruleAcc = ruleAcc.replace(/Ĵ/g,'J'); 
            ruleAcc = ruleAcc.replace(/Ķ/g,'K'); 
            ruleAcc = ruleAcc.replace(/Ĺ|Ļ|Ľ|Ŀ|Ł/g,'L');
            ruleAcc = ruleAcc.replace(/Ñ|Ń|Ņ|Ň|Ŋ/g,'N');
            ruleAcc = ruleAcc.replace(/Ò|Ó|Ô|Õ|Ö|Ø|Ō|Ŏ|Ő|Ǿ/g,'O');
            ruleAcc = ruleAcc.replace(/Ŕ|Ŗ|Ř/g,'R');
            ruleAcc = ruleAcc.replace(/Ś|Ŝ|Ş|Š/g,'S');
            ruleAcc = ruleAcc.replace(/Ţ|Ť|Ŧ/g,'T');
            ruleAcc = ruleAcc.replace(/Ù|Ú|Û|Ü|Ũ|Ū|Ŭ|Ů|Ű|Ų/g,'U');
            ruleAcc = ruleAcc.replace(/Ŵ|Ẁ|Ẃ|Ẅ/g,'W');
            ruleAcc = ruleAcc.replace(/Ỳ|Ý|Ŷ|Ÿ/g,'Y');
            ruleAcc = ruleAcc.replace(/Ź|Ż|Ž/g,'Z');
            
            ruleAcc = ruleAcc.replace(/à|á|â|ã|ä|å|ā|ă|ą|ǻ/g,'A');       
            ruleAcc = ruleAcc.replace(/æ|ǽ/g,'AE'); 
            ruleAcc = ruleAcc.replace(/ç|ć|ĉ|ċ|č/g,'C'); 
            ruleAcc = ruleAcc.replace(/þ|ď|đ/g,'D'); 
            ruleAcc = ruleAcc.replace(/è|é|ê|ë|ē|ĕ|ė|ę|ě/g,'E'); 
            ruleAcc = ruleAcc.replace(/ĝ|ğ|ġ|ģ/g,'G'); 
            ruleAcc = ruleAcc.replace(/ĥ|ħ/g,'H'); 
            ruleAcc = ruleAcc.replace(/ì|í|î|ï|ĩ|ī|ĭ|į|i/g,'I'); 
            ruleAcc = ruleAcc.replace(/ĵ/g,'J'); 
            ruleAcc = ruleAcc.replace(/ķ/g,'K'); 
            ruleAcc = ruleAcc.replace(/ĺ|ļ|ľ|ŀ|ł/g,'L');
            ruleAcc = ruleAcc.replace(/ñ|ń|ņ|ň|ŋ/g,'N');
            ruleAcc = ruleAcc.replace(/ò|ó|ô|õ|ö|ø|ō|ŏ|ő|ǿ/g,'O');
            ruleAcc = ruleAcc.replace(/oe/g,'OE');
            ruleAcc = ruleAcc.replace(/ŕ|ŗ|ř/g,'R');
            ruleAcc = ruleAcc.replace(/ś|ŝ|ş|š/g,'S');
            ruleAcc = ruleAcc.replace(/ţ|ť|ŧ/g,'T');
            ruleAcc = ruleAcc.replace(/ù|ú|û|ü|ũ|ū|ŭ|ů|ű|ų/g,'U');
            ruleAcc = ruleAcc.replace(/ŵ|ẁ|ẃ|ẅ/g,'W');
            ruleAcc = ruleAcc.replace(/ỳ|ý|ŷ|ÿ/g,'Y');
            ruleAcc = ruleAcc.replace(/ź|ż|ž/g,'Z');
            
            ruleAcc = ruleAcc.replace(/0/g,'ZERO');
            ruleAcc = ruleAcc.replace(/1/g,'ONE');
            ruleAcc = ruleAcc.replace(/2/g,'TWO');
            ruleAcc = ruleAcc.replace(/3/g,'THREE');
            ruleAcc = ruleAcc.replace(/4/g,'FOUR');
            ruleAcc = ruleAcc.replace(/5/g,'FIVE');
            ruleAcc = ruleAcc.replace(/6/g,'SIX');
            ruleAcc = ruleAcc.replace(/7/g,'SEVEN');
            ruleAcc = ruleAcc.replace(/8/g,'EIGHT');
            ruleAcc = ruleAcc.replace(/9/g,'NINE');
            
            
            if(matchesSimple[1].includes("DFSA") || matchesSimple[1].includes("DUBAIFINANCIALSERVICESAUTHORITY") || matchesSimple[1].includes("DIFC") || matchesSimple[1].includes("DUBAIINTERNATIONALFINANCIALCENTRE") || matchesSimple[1].includes("DUBAIINTERNATIONALFINANCIALCENTER") || matchesSimple[1].includes("DUBAIINTERNATIONALFINANCIALCENTREAUTHORITY") || matchesSimple[1].includes("DIFCA") || matchesSimple[1].includes("DIFCCOURT") || matchesSimple[1].includes("DISPUTERESOLUTIONAUTHORITY")  || matchesSimple[1].includes("DRA")  || matchesSimple[1].includes("DIFCWILLSANDPROBATEREGISTRY")|| matchesSquare[1].includes("DFSA") || matchesSquare[1].includes("DUBAIFINANCIALSERVICESAUTHORITY") || matchesSquare[1].includes("DIFC") || matchesSquare[1].includes("DUBAIINTERNATIONALFINANCIALCENTRE") || matchesSquare[1].includes("DUBAIINTERNATIONALFINANCIALCENTER") || matchesFlower[1].includes("DFSA") || matchesFlower[1].includes("DUBAIFINANCIALSERVICESAUTHORITY") || matchesFlower[1].includes("DIFC")|| matchesFlower[1].includes("DUBAIINTERNATIONALFINANCIALCENTRE") || matchesFlower[1]==("DUBAIINTERNATIONALFINANCIALCENTER")){
                var removeBracketStringDup = removeSpecial.replace(/\([^()]*\)/g, '');
                var removeSquareBracketStringDup = removeBracketStringDup.replace(/\[.*?\]/g,'');
                var removeFlowerBracketStringDup = removeSquareBracketStringDup.replace(/\{.*?\}/g,'');
                console.log("removeFlowerBracketStringDup "+removeFlowerBracketStringDup);
                var removeAND = removeBracketStringDup.replace(/AND/g, '');
                removeAND = removeAND.replace(/OF/g, '');
                removeAccFinal = removeAND.replace(/ /g, '');
            }
            
            var nAcc = removeAccFinal.includes(ruleAcc);
            if(nAcc == true){
                warnAcc.push(lenAcc[i].Name);  
            }
        }
        
        
        for(var i=0;i<lenReservedNames.length;i++){
            var accUpperRes = enteredValue.toUpperCase();
            var removeAccFinalRes = accUpperRes.replace(/[^a-zA-Z0-9]/g, ''); 
            
            removeAccFinalRes = removeAccFinalRes.replace(/AND/g, '');
            removeAccFinalRes = removeAccFinalRes.replace(/OF/g, '');
            
            var ruleAccRes= lenReservedNames[i].Entity_Name__c.toUpperCase();
            console.log("ruleAccRes before "+ruleAccRes);
            ruleAccRes = ruleAccRes.replace(/[^a-zA-Z0-9$]/g, ''); 
            
            ruleAccRes = ruleAccRes.replace(/AND/g, '');
            ruleAccRes = ruleAccRes.replace(/OF/g, '');
            
            ruleAccRes = ruleAccRes.replace(/À|Á|Â|Ã|Ä|Å|Ā|Ă|Ą|Ǻ/g,'A'); 
            ruleAccRes = ruleAccRes.replace(/Æ|Ǽ/g,'AE'); 
            ruleAccRes = ruleAccRes.replace(/Ç|Ć|Ĉ|Ċ|Č/g,'C'); 
            ruleAccRes = ruleAccRes.replace(/Þ|Ď|Đ/g,'D'); 
            ruleAccRes = ruleAccRes.replace(/È|É|Ê|Ë|Ē|Ĕ|Ė|Ę|Ě/g,'E'); 
            ruleAccRes = ruleAccRes.replace(/Ĝ|Ğ|Ġ|Ģ/g,'G'); 
            ruleAccRes = ruleAccRes.replace(/Ĥ|Ħ/g,'H'); 
            ruleAccRes = ruleAccRes.replace(/Ì|Í|Î|Ï|Ĩ|Ī|Ĭ|Į|İ/g,'I'); 
            ruleAccRes = ruleAccRes.replace(/Ĵ/g,'J'); 
            ruleAccRes = ruleAccRes.replace(/Ķ/g,'K'); 
            ruleAccRes = ruleAccRes.replace(/Ĺ|Ļ|Ľ|Ŀ|Ł/g,'L');
            ruleAccRes = ruleAccRes.replace(/Ñ|Ń|Ņ|Ň|Ŋ/g,'N');
            ruleAccRes = ruleAccRes.replace(/Ò|Ó|Ô|Õ|Ö|Ø|Ō|Ŏ|Ő|Ǿ/g,'O');
            ruleAccRes = ruleAccRes.replace(/Ŕ|Ŗ|Ř/g,'R');
            ruleAccRes = ruleAccRes.replace(/Ś|Ŝ|Ş|Š/g,'S');
            ruleAccRes = ruleAccRes.replace(/Ţ|Ť|Ŧ/g,'T');
            ruleAccRes = ruleAccRes.replace(/Ù|Ú|Û|Ü|Ũ|Ū|Ŭ|Ů|Ű|Ų/g,'U');
            ruleAccRes = ruleAccRes.replace(/Ŵ|Ẁ|Ẃ|Ẅ/g,'W');
            ruleAccRes = ruleAccRes.replace(/Ỳ|Ý|Ŷ|Ÿ/g,'Y');
            ruleAccRes = ruleAccRes.replace(/Ź|Ż|Ž/g,'Z');
            
            ruleAccRes = ruleAccRes.replace(/à|á|â|ã|ä|å|ā|ă|ą|ǻ/g,'A');       
            ruleAccRes = ruleAccRes.replace(/æ|ǽ/g,'AE'); 
            ruleAccRes = ruleAccRes.replace(/ç|ć|ĉ|ċ|č/g,'C'); 
            ruleAccRes = ruleAccRes.replace(/þ|ď|đ/g,'D'); 
            ruleAccRes = ruleAccRes.replace(/è|é|ê|ë|ē|ĕ|ė|ę|ě/g,'E'); 
            ruleAccRes = ruleAccRes.replace(/ĝ|ğ|ġ|ģ/g,'G'); 
            ruleAccRes = ruleAccRes.replace(/ĥ|ħ/g,'H'); 
            ruleAccRes = ruleAccRes.replace(/ì|í|î|ï|ĩ|ī|ĭ|į|i/g,'I'); 
            ruleAccRes = ruleAccRes.replace(/ĵ/g,'J'); 
            ruleAccRes = ruleAccRes.replace(/ķ/g,'K'); 
            ruleAccRes = ruleAccRes.replace(/ĺ|ļ|ľ|ŀ|ł/g,'L');
            ruleAccRes = ruleAccRes.replace(/ñ|ń|ņ|ň|ŋ/g,'N');
            ruleAccRes = ruleAccRes.replace(/ò|ó|ô|õ|ö|ø|ō|ŏ|ő|ǿ/g,'O');
            ruleAccRes = ruleAccRes.replace(/oe/g,'OE');
            ruleAccRes = ruleAccRes.replace(/ŕ|ŗ|ř/g,'R');
            ruleAccRes = ruleAccRes.replace(/ś|ŝ|ş|š/g,'S');
            ruleAccRes = ruleAccRes.replace(/ţ|ť|ŧ/g,'T');
            ruleAccRes = ruleAccRes.replace(/ù|ú|û|ü|ũ|ū|ŭ|ů|ű|ų/g,'U');
            ruleAccRes = ruleAccRes.replace(/ŵ|ẁ|ẃ|ẅ/g,'W');
            ruleAccRes = ruleAccRes.replace(/ỳ|ý|ŷ|ÿ/g,'Y');
            ruleAccRes = ruleAccRes.replace(/ź|ż|ž/g,'Z');
            
            ruleAccRes = ruleAccRes.replace(/0/g,'ZERO');
            ruleAccRes = ruleAccRes.replace(/1/g,'ONE');
            ruleAccRes = ruleAccRes.replace(/2/g,'TWO');
            ruleAccRes = ruleAccRes.replace(/3/g,'THREE');
            ruleAccRes = ruleAccRes.replace(/4/g,'FOUR');
            ruleAccRes = ruleAccRes.replace(/5/g,'FIVE');
            ruleAccRes = ruleAccRes.replace(/6/g,'SIX');
            ruleAccRes = ruleAccRes.replace(/7/g,'SEVEN');
            ruleAccRes = ruleAccRes.replace(/8/g,'EIGHT');
            ruleAccRes = ruleAccRes.replace(/9/g,'NINE');
            
            if(matchesSimple[1].includes("DFSA") || matchesSimple[1].includes("DUBAIFINANCIALSERVICESAUTHORITY") || matchesSimple[1].includes("DIFC") || matchesSimple[1].includes("DUBAIINTERNATIONALFINANCIALCENTRE") || matchesSimple[1].includes("DUBAIINTERNATIONALFINANCIALCENTER") || matchesSimple[1].includes("DUBAIINTERNATIONALFINANCIALCENTREAUTHORITY") || matchesSimple[1].includes("DIFCA") || matchesSimple[1].includes("DIFCCOURT") || matchesSimple[1].includes("DISPUTERESOLUTIONAUTHORITY")  || matchesSimple[1].includes("DRA")  || matchesSimple[1].includes("DIFCWILLSANDPROBATEREGISTRY")|| matchesSquare[1].includes("DFSA") || matchesSquare[1].includes("DUBAIFINANCIALSERVICESAUTHORITY") || matchesSquare[1].includes("DIFC") || matchesSquare[1].includes("DUBAIINTERNATIONALFINANCIALCENTRE") || matchesSquare[1].includes("DUBAIINTERNATIONALFINANCIALCENTER") || matchesFlower[1].includes("DFSA") || matchesFlower[1].includes("DUBAIFINANCIALSERVICESAUTHORITY") || matchesFlower[1].includes("DIFC")|| matchesFlower[1].includes("DUBAIINTERNATIONALFINANCIALCENTRE") || matchesFlower[1]==("DUBAIINTERNATIONALFINANCIALCENTER")){
                var removeBracketStringRes = removeSpecial.replace(/\([^()]*\)/g, '');
                console.log("removeBracketStringRes "+removeBracketStringRes);
                var removeAND = removeBracketStringRes.replace(/AND/g, '');
                removeAND = removeAND.replace(/OF/g, '');
                removeAccFinalRes = removeAND.replace(/ /g, '');
            }
            
            var nAccRes = removeAccFinalRes.includes(ruleAccRes);
            console.log("ruleAccRes "+ruleAccRes);
            console.log("removeAccFinalRes "+removeAccFinalRes);
            console.log("nAccRes "+nAccRes);
            if(nAccRes == true){
                console.log("inside nAccRes");
                warnRes.push(lenReservedNames[i].Entity_Name__c);  
                console.log("warnRes accarray", warnRes);
            }
        }
        
        console.log("checking length of warn", warn.length);
        console.log("checking length of warnDoc", warnDoc.length);
        console.log("checking length of warnAcc", warnAcc.length);
        console.log("checking length of warnProh", warnProh.length);
        console.log("checking length of warnRes", warnRes.length);
        
        var quotes = "\"";
        
        if(warn.length>0){
            var warnString = [warn.slice(0, -1).join(', '), warn.slice(-1)[0]].join(warn.length < 2 ? '' : ' and ');
            component.set("v.pValue","Warning: The names " + quotes +warnString+ quotes  + " are restricted words or phrases for companies in DIFC. The name might be rejected but you can submit the entity name for review");
            component.set("v.displayWarn","true");
        }
        
        if(warnDoc.length>0){
            var warnDocString = [warnDoc.slice(0, -1).join(', '), warnDoc.slice(-1)[0]].join(warnDoc.length < 2 ? '' : ' and ');
            component.set("v.notAllowedNameDoc",warnDoc); 
            component.set("v.pValueDoc","Warning: The names "+ quotes +warnDocString+ quotes  + " are considered as an authorized body, so you will have to provide evidence that you have permission to use the name.");            
            component.set("v.displayWarnDoc","true");
        }
        
        if(warnReason.length>0){
            var warnReasonString = [warnReason.slice(0, -1).join(', '), warnReason.slice(-1)[0]].join(warnReason.length < 2 ? '' : ' and ');
            component.set("v.pValueReason","Warning: This requires DFSA approval. Please enter the reason for chosing "+ quotes +warnReasonString+ quotes  +" in the name");            
            component.set("v.displayWarnReason","true");
        }
        
        
        if(warnAcc.length>0){
            var warnAccString = [warnAcc.slice(0, -1).join(', '), warnAcc.slice(-1)[0]].join(warnAcc.length < 2 ? '' : ' and ');
            component.set("v.notAllowedNameDup",warnAcc); // To display the list of similar companies to the User
            component.set("v.pValueDup","Warning: The company names "+ quotes +warnAccString+ quotes  +" is considered to be same as existing names shown above. Please provide the evidence of relationship if you wish to proceed");            
            component.set("v.displayWarnDup","true");
        }
        
        
        if(warnProh.length>0){
            var warnProhString = [warnProh.slice(0, -1).join(', '), warnProh.slice(-1)[0]].join(warnProh.length < 2 ? '' : ' and ');
            component.set("v.notAllowedProh",warnProhString);
            component.set("v.pValueProh","Warning: This name contains " + quotes +warnProhString+ quotes  +" which are PROHIBITTED words");            
            component.set("v.displayWarnProh","true");        
        }
        
        if(warnRes.length>0){
            var warnResString = [warnRes.slice(0, -1).join(', '), warnRes.slice(-1)[0]].join(warnRes.length < 2 ? '' : ' and ');
            component.set("v.notAllowedNameReserve",warnResString);
            component.set("v.pValueReserve","Warning: The company names "  + quotes +warnResString+ quotes  + " are considered to be same as reserved names shown above. Please provide the evidence of relationship if you wish to proceed");            
            component.set("v.displayWarnReserve","true");        
        }
        
        if(warnLegal.length>0){
            var warnLegalString = [warnLegal.slice(0, -1).join(', '), warnLegal.slice(-1)[0]].join(warnLegal.length < 2 ? '' : ' and ');
            component.set("v.notAllowedNameLegal",warnLegalString);
            console.log("legal structures present "+component.get("v.notAllowedNameLegal"));
            component.set("v.pValueLegal","Warning: The name contains the legal structure "+ quotes +warnLegalString+ quotes  + " along with it. Please remove the legal strucutre and select the legal structure provided in the picklist");            
            component.set("v.displayWarnLegal","true");  
        }

        if(warn.length==0 && warnDoc.length==0 && warnReason.length==0 && warnAcc.length==0 && warnProh.length==0 && warnRes.length==0 && warnLegal.length==0){
            component.set("v.pValueAllow","true"); 
        }   
         }
    },

    submitName:function(component,event,helper){
        
        var EntityName = component.get("v.valueTextEntity");
        var TradingName = component.get("v.valueTextTrading");
        
        if (EntityName == null || TradingName == null) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Warning!",
                "message": "The Entity name/Trading Name is empty"
            });
            toastEvent.fire();
        }
        
        if (EntityName!=null && TradingName!=null) {    
            console.log("inside submitName lightning controller");
            var action = component.get("c.submitNameApex");
            action.setCallback(this, function(response) {	
                var state = response.getState();
                console.log(state);
                if (component.isValid() && state === "SUCCESS") {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "The names have been submitted successfully."
                    });
                    toastEvent.fire();
                }
            }); 
            $A.enqueueAction(action);
        }
    },
    
   /* handleChange:function(component,event,helper){ 
        var checked =component.get("v.checked");
        if(checked) {
            document.getElementById("disable_button").classList.remove("button--disabled");
        }
        else{
            document.getElementById("disable_button").classList.add("button--disabled");   
        }
    },*/
    
    openModelDoc: function(component, event, helper) {
        component.set("v.isOpenDoc", true);
    },
    
    closeModelDoc: function(component, event, helper) {
        component.set("v.isOpenDoc", false);
    },
    
    closeModelDUP: function(component, event, helper) { 
        component.set("v.isOpenDUP", false);
    },
    
    openModelDUP: function(component, event, helper) {
        var value = event.target.name;
        var action1 = component.get("c.getDupName");
        
        action1.setParams({
            NameDup: value
        });
        
        action1.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var returnValDup = response.getReturnValue();
                component.set("v.showDupDelete",returnValDup);
                console.log("dup name returned", component.get("v.showDupDelete"));
            }
        }); 
        $A.enqueueAction(action1);
        component.set("v.isOpenDUP", true);
    },
    
    navigateToTradingNameCheckBlock:function(component, event, helper) {
        var selectedName= component.get("v.valueText");
        var warnWords= component.get("v.notAllowedName");
        var warnMessage=component.get("v.pValue"); 
        var warnDocWords=component.get("v.notAllowedNameDoc");
        var warnDocMessage=component.get("v.pValueDoc");
        var warnReasonWords=component.get("v.notAllowedNameReason");
        var warnReasonMessage=component.get("v.pValueReason");
        var warnDupWords=component.get("v.notAllowedNameDup");
        var warnDupMessage=component.get("v.pValueDup");
        var warnResWords=component.get("v.notAllowedNameReserve");
        var warnResMessage=component.get("v.pValueReserve");
        var WarnProhWords=component.get("v.notAllowedProh");
        var warnprohMessage=component.get("v.pValueProh");
        var warnLegalWords=component.get("v.notAllowedNameLegal");
        var warnLegalMessage=component.get("v.pValueLegal");
        
          var warningMessage = warnWords + '\n'+ warnMessage + '\n' + '\n' + warnDocWords + '\n'+ warnDocMessage + '\n' + '\n' + warnReasonWords + 
            '\n'+ warnReasonMessage + '\n' + '\n' + warnDupWords + '\n'+ warnDupMessage + '\n' + '\n' + warnResWords + '\n'+ 
                             warnResMessage + '\n' + '\n' + WarnProhWords + '\n'+ warnprohMessage + '\n' + '\n' + warnLegalWords + '\n'+ warnLegalMessage;
        console.log("warningMessage is "+warningMessage);
        
        var action = component.get("c.updateTradingName");
        let newURL = new URL(window.location.href).searchParams;
        action.setParams({
            TradingName:selectedName,
            NameId:component.get("v.NameIdtoController"),
             warningMessage:warningMessage,
             srId : newURL.get('srId')
        })
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            if (component.isValid() && state === "SUCCESS") {
                     console.log("success adding entity name");
            }
        }); 
        $A.enqueueAction(action);
        
        
        var urlEvent = $A.get("e.force:navigateToURL");
        console.log("urlEvent is "+urlEvent);
        alert('Redirect==>'+newURL.get('srId'));
        urlEvent.setParams({
            "url": "/ob-searchentityname/?srId="+newURL.get('srId')+"&pageId="+newURL.get('pageId')+"&flowId="+newURL.get('flowId'),
        });
        urlEvent.fire();
    }
})