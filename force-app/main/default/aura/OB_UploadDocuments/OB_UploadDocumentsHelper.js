({
  /*** Method to check is the logged in user is community user or not ***/
  GetLoggedInUser: function(component, event) {
    var action = component.get("c.CheckIfCommunityUser");
    action.setCallback(this, function(response) {
      console.log("Logged Stat===>" + response.getState());
      console.log("Logged In User===>" + response.getReturnValue());
      if (response.getState() == "SUCCESS")
        component.set("v.IsCommunityUser", response.getReturnValue());
      console.log("check==>" + component.get("v.IsCommunityUser"));
    });

    $A.enqueueAction(action);
  },
  /*** Method to fetch SR Docs related to SR ***/
  loadSRDocs: function(component, event, helper) {
    var SRRecordID = component.get("v.SRId");
    var action = component.get("c.getSRDocs");
    action.setParams({ SRId: SRRecordID });
    action.setCallback(this, function(response) {
      if (response.getState() == "SUCCESS") {
        var result = response.getReturnValue();
        component.set("v.SRDocs", result.SRDocs);
        console.log(result.SRDocs);
        //if(component.get("v.SRDocs")[0].HexaBPM__Doc_ID__c!='' && component.get("v.SRDocs")[0].HexaBPM__Doc_ID__c!=null)
        //component.set("v.SRDocName",component.get("v.SRDocs")[0].Name);
        //console.log('In SRDoc===>'+JSON.stringify(response.getReturnValue());
      }
    });
    $A.enqueueAction(action);
  },
  /*** Method to dynamically fetch the status picklist values ***/
  GetStatusPicklistValues: function(component, event) {
    var action = component.get("c.getSRDocStatus");
    var opts = [];
    action.setCallback(this, function(response) {
      if (response.getState() == "SUCCESS") {
        for (var i = 0; i < response.getReturnValue().length; i++) {
          opts.push({
            class: "optionClass",
            label: response.getReturnValue()[i],
            value: response.getReturnValue()[i]
          });
        }
        component.set("v.statusNameValues", opts);
        console.log("In Picklist==>" + component.get("v.statusNameValues"));
      }
    });

    $A.enqueueAction(action);
  },
  /*** Method to get the Attachment URL to display in preview section***/
  ShowDocumentPreview: function(component, event) {
    console.log("ShowDocumentPreview");
    var contentId = component.get("v.SelectedAttachmentID");
    console.log(contentId);
    /*
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": contentId,
            "slideDevName": "detail"
        });
        navEvt.fire();
        */

    var pageReference = {
      type: "standard__namedPage",
      attributes: {
        pageName: "filePreview"
      },
      state: {
        recordIds: contentId,
        selectedRecordId: contentId
      }
    };
    var navService = component.find("navService");
    event.preventDefault();
    navService.navigate(pageReference);

    /*
        component.set("v.AttachmentURL","");
        var SRRecordID = component.get("v.SRId") ;
        var DocID = component.get("v.SelectedAttachmentID");
        var action = component.get("c.getAttachmentURL");
        action.setParams({
            SRID: SRRecordID,
            AttachmentID: DocID
        });
        action.setCallback(this, function(response) {
            if(response.getState()=="SUCCESS"){
                var ResponseElements = (response.getReturnValue()).split('&');
                component.set("v.AttachmentURL", ResponseElements[0]+ResponseElements[1]);
                component.set("v.SelectedAttachmentID",ResponseElements[1]);
                if(ResponseElements[1]!='')
                    document.getElementById('showNext').disabled = true; 
            }
        });
        $A.enqueueAction(action);
        */
  },
  /*** Method to show the toggle to pop up ***/
  toggleClass: function(component, componentId, className) {
    var modal = component.find(componentId);
    $A.util.removeClass(modal, className + "hide");
    $A.util.addClass(modal, className + "open");
  },
  /*** Method to toggle inverse the pop up ***/
  toggleClassInverse: function(component, componentId, className) {
    var modal = component.find(componentId);
    $A.util.addClass(modal, className + "hide");
    $A.util.removeClass(modal, className + "open");
  },
  /*** Method to fetch the next attachment ***/
  ShowNextAttachment: function(component, event) {
    component.set("v.AttachmentURL", "");
    var SRRecordID = component.get("v.SRId");
    var CurrentAttachmentID = component.get("v.SelectedAttachmentID");
    var action = component.get("c.getNextAttachment");
    action.setParams({
      SRID: SRRecordID,
      AttachmentID: CurrentAttachmentID
    });
    action.setCallback(this, function(response) {
      if (response.getState() === "SUCCESS") {
        document.getElementById("showPrevious").disabled = false;

        var AttachmentLength = response.getReturnValue().length;
        var returnedResponse = response.getReturnValue();

        var ResponseElements = returnedResponse.split("&");
        if (
          ResponseElements[3] != "" &&
          ResponseElements[3] == "LastAttachment"
        ) {
          document.getElementById("showNext").disabled = true;
        }
        component.set("v.SRDocName", ResponseElements[2]);
        component.set("v.SelectedAttachmentID", ResponseElements[1]);
        component.set(
          "v.AttachmentURL",
          ResponseElements[0] + ResponseElements[1]
        );
      } else {
        $A.log("Errors", response.getError());
      }
    });
    $A.enqueueAction(action);
  },
  /*** Method to show the previous attachment ***/
  ShowPreviousAttachment: function(component, event) {
    component.set("v.AttachmentURL", "");
    var SRRecordID = component.get("v.SRId");
    var CurrentAttachmentID = component.get("v.SelectedAttachmentID");
    var action = component.get("c.getPreviousAttachment");
    action.setParams({
      SRID: SRRecordID,
      AttachmentID: CurrentAttachmentID
    });
    action.setCallback(this, function(response) {
      if (response.getState() === "SUCCESS") {
        document.getElementById("showPrevious").disabled = false;

        var AttachmentLength = response.getReturnValue().length;
        var returnedResponse = response.getReturnValue();
        var ResponseElements = returnedResponse.split("&");
        if (
          ResponseElements[3] != "" &&
          ResponseElements[3] == "FirstAttachment"
        ) {
          document.getElementById("showPrevious").disabled = true;
          document.getElementById("showNext").disabled = false;
        }
        component.set("v.SRDocName", ResponseElements[2]);
        component.set("v.SelectedAttachmentID", ResponseElements[1]);
        component.set(
          "v.AttachmentURL",
          ResponseElements[0] + ResponseElements[1]
        );
      } else {
        $A.log("Errors", response.getError());
      }
    });
    $A.enqueueAction(action);
  },
  /*** Method to Save the SR Docs when Save is clicked from the header section ***/
  SaveSRDocs: function(component, event) {
    var SRID = component.get("v.SRId");
    var StepID = component.get("v.StepId");
    var action = component.get("c.UpdateSRDocs");
    //var Slist = $A.util.json.encode(component.get("v.SRDocs"))
    action.setParams({
      SRDocList: JSON.stringify(component.get("v.SRDocs")),
      SRId: SRID
    });
    action.setCallback(this, function(response) {
      if (response.getState() === "SUCCESS") {
        //location.href = '/' + SRID;
        window.location.href =
          "/lightning/r/HexaBPM__Service_Request__c/" + SRID + "/view";
      }
    });
    //console.log(JSON.parse(JSON.stringify(component.get("v.SRDocs"))));
    $A.enqueueAction(action);
  },
  /*** Method to enable disable Next & Previous buttons in the preview section ***/
  EnableDisableButtons: function(component, event) {
    var SRRecordID = component.get("v.SRId");
    var selectedItemID = event.currentTarget.dataset.attachmentid;
    var action = component.get("c.EnableDisableButtons");
    action.setParams({
      SRID: SRRecordID,
      AttachmentID: selectedItemID
    });
    action.setCallback(this, function(response) {
      var result = response.getReturnValue();
      if (result == "disablePrevious-disableNext") {
        document.getElementById("showPrevious").disabled = true;
        document.getElementById("showNext").disabled = true;
      } else if (result == "disablePrevious-enableNext") {
        document.getElementById("showPrevious").disabled = true;
        document.getElementById("showNext").disabled = false;
      } else if (result == "disableNext-enablePrevious") {
        document.getElementById("showPrevious").disabled = false;
        document.getElementById("showNext").disabled = true;
      } else if (result == "enablePrevious-enableNext") {
        document.getElementById("showPrevious").disabled = false;
        document.getElementById("showNext").disabled = false;
      }
    });
    $A.enqueueAction(action);
  },
  /*** Method to save the attachment under the selected SR Doc from the Upload pop up ***/
  saveAttachment: function(component, event, helper) {
    try {
      console.log("saveAttachment");
      var MAX_FILE_SIZE = 750000; /* 1 000 000 * 3/4 to account for base64 */
      var fileInput = document.getElementById("file");
      var file = fileInput.files[0];
      console.log(file);
      if (file.size > this.MAX_FILE_SIZE) {
        alert(
          "File size cannot exceed " +
            this.MAX_FILE_SIZE +
            " bytes.\n" +
            "Selected file size: " +
            file.size
        );
        return;
      }
      var fr = new FileReader();
      var self = this;
      var comments = "";
      if (component.get("v.IsCommunityUser"))
        comments = component.get("v.SRDocument.HexaBPM__Customer_Comments__c");
      else
        comments = component.get("v.SRDocument.HexaBPM__Rejection_Reason__c");
      fr.onload = function() {
        var fileContents = fr.result;
        var base64Mark = "base64,";
        var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
        fileContents = fileContents.substring(dataStart);
        var action = component.get("c.saveTheFile");
        action.setParams({
          SRId: component.get("v.SRId"),
          parentId: component.get("v.SRDocument.Id"),
          fileName: file.name,
          base64Data: encodeURIComponent(fileContents),
          contentType: file.type,
          CustomerComments: comments //document.getElementById("CustomerComments").value
        });
        action.setCallback(this, function(a) {
          if (a.getState() === "SUCCESS") {
            console.log("file onsucess");
            //self.GetSRDocs(component, event);
            //this.loadSRDocs(component, event);
            helper.loadSRDocs(component, event, helper);
            helper.toggleClassInverse(component, "backdrop", "slds-backdrop--");
            helper.toggleClassInverse(
              component,
              "modaldialog",
              "slds-fade-in-"
            );

            //this.toggleClassInverse(component, "backdrop", "slds-backdrop--");
            //this.toggleClassInverse(component, "modaldialog", "slds-fade-in-");
          } else if (a.getState() === "ERROR") {
            $A.log("Errors", a.getError());
            console.log("a==>" + a);
          }
        });
        $A.getCallback(function() {
          $A.enqueueAction(action);
        })();
      };
      fr.readAsDataURL(file);
    } catch (error) {
      console.log(error.message);
    }
  }
});