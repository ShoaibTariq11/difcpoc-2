({
  doInit: function(component, event, helper) {
    console.log("inint");
    helper.GetLoggedInUser(component, event);
    helper.GetStatusPicklistValues(component, event);
    helper.loadSRDocs(component, event, helper);
    //helper.ShowDocumentPreview(component, event);
  },
  PreviewDocument: function(component, event, helper) {
    var selectedItem = event.currentTarget.dataset.attachmentid;
    var selectedItemName = event.currentTarget.dataset.docname;

    component.set("v.SelectedAttachmentID", selectedItem);
    component.set("v.SRDocName", selectedItemName);

    helper.ShowDocumentPreview(component, event);
    helper.EnableDisableButtons(component, event);
  },
  showOppmodal: function(component, event, helper) {
    var selectedIndex = event.currentTarget.dataset.index;
    var attachmentObj = component.get("v.SRDocs");

    component.set("v.SRDocument", attachmentObj[selectedIndex]);
    helper.toggleClass(component, "backdrop", "slds-backdrop--");
    helper.toggleClass(component, "modaldialog", "slds-fade-in-");
  },
  hideModal: function(component, event, helper) {
    helper.toggleClassInverse(component, "backdrop", "slds-backdrop--");
    helper.toggleClassInverse(component, "modaldialog", "slds-fade-in-");
  },
  getFileName: function(component, event, helper) {
    // var fileName = document.getElementById('file').value;
    // document.getElementById('FileName').innerHTML=fileName;
    var files = document.getElementById("file").files;
    var fileName = "";
    for (var i = 0; i < files.length; i++) fileName += files[i].name + " ";
    console.log(fileName);
    component.find("fileName").set("v.value", fileName);
  },
  Save: function(component, event, helper) {
    helper.saveAttachment(component, event, helper);
  },
  showSpinner: function(component, event, helper) {
    component.set("v.Spinner", true);
  },
  hideSpinner: function(component, event, helper) {
    component.set("v.Spinner", false);
  },
  ShowNextAttachment: function(component, event, helper) {
    helper.ShowNextAttachment(component, event);
  },
  ShowPreviousAttachment: function(component, event, helper) {
    helper.ShowPreviousAttachment(component, event);
  },
  GoToSR: function(component, event, helper) {
    var SRRecordID = component.get("v.SRId");
    //location.href = '/' + SRRecordID;
    window.location.href =
      "/lightning/r/HexaBPM__Service_Request__c/" + SRRecordID + "/view";
  },
  SaveSRDocs: function(component, event, helper) {
    helper.SaveSRDocs(component, event);
  }
});