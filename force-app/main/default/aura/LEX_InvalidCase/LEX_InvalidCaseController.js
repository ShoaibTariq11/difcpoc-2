({
    doInit: function (component, event, helper) {
        
    
    },
    afterYesButton : function(component,event,helper){
    	var caseId = component.get("v.recordId");
        // Prepare the action to load Case record
        var action = component.get("c.getCase");
        action.setParams({"caseId": caseId});

        // Configure response handler
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var caseRecord = response.getReturnValue();
                component.set("v.showConfirm",false);
                console.log(caseRecord);
                if(caseRecord){
                    console.log(caseRecord.IsClosed);
                    if (caseRecord.IsClosed == false) {
                        
                        //Save the case obj with in invalid status
                        var upsertAction = component.get("c.invalidCase"); // method in the apex class
                        upsertAction.setParams({"caseId": caseId});
                        upsertAction.setCallback(this, function(a) {
                            var state = a.getState();
                            
                            if(state === "SUCCESS") {
                                console.log('set..');
                                component.set("v.caseMsg", a.getReturnValue());
                                //$A.get("e.force:closeQuickAction").fire();
                                //$A.get('e.force:refreshView').fire();
                            }
                            else {
                                console.log('Problem saving case, response state: ' + state);
                            }
                        });
                        $A.enqueueAction(upsertAction);
                    } else {
                        component.set("v.caseMsg", 'Case is already closed.');
                    }
                }
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
	cancelAction:function (component, event, helper) {
        //Close the popup
        $A.get("e.force:closeQuickAction").fire();
    },
    closeAction:function (component, event, helper) {
        //Close the popup
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
        $A.get('e.force:refreshView').fire();
    }

    })