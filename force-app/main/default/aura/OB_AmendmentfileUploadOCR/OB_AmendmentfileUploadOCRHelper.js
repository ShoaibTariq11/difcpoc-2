({
  loadSRDocs: function (component, event, helper) {
    var srId = component.get("v.srId");
    var documentMaster = component.get("v.documentMaster");
    var amedId = component.get("v.amedId");
    var RelamedId = component.get("v.relatedAmendID");
    console.log(" loadSRDocs srId-----", srId);
    console.log(" loadSRDocs documentMaster -----", documentMaster);
    console.log(amedId);
    console.log(amedId == null);
    //console.log(component.get("v.relatedAmendID"));

    if (amedId == null && RelamedId != null) {
      helper.loadRelSRDoc(component, event, helper);
    } else {
      var action = component.get("c.viewSRDocs");
      var reqWrapPram = {
        srId: srId,
        docMasterCode: documentMaster,
        amedId: amedId,
      };
      // docMasterContentDocMap : docMasterContentDocMap
      console.log("######### reqWrapPram LoadSR ", reqWrapPram);
      action.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram),
      });

      // set call back
      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var result = response.getReturnValue();
          console.log("in load srDocs>>");
          console.log(JSON.stringify(result));
          console.log(result);

          var mapSRDocs = result.mapSRDocs;
          console.log("$$$$$$$ mapSRDocs ", mapSRDocs);
          if (mapSRDocs && mapSRDocs[documentMaster].File_Name__c) {
            component.set("v.fileName", mapSRDocs[documentMaster].File_Name__c);
            //fileUploaded
            component.set("v.fileUploaded", true);
          }
        } else if (state === "INCOMPLETE") {
          alert("From server: " + response.getReturnValue());
        } else if (state === "ERROR") {
          var errors = response.getError();
          if (errors) {
            if (errors[0] && errors[0].message) {
              helper.displayToast("Error", errors[0].message);
              console.log("Error message: " + errors[0].message);
            }
          } else {
            console.log("Unknown error");
          }
        }
      });
      // enqueue the action
      $A.enqueueAction(action);
    }
  },

  loadRelSRDoc: function (component, event, helper) {
    try {
      console.log(" in load rel");
      var srId = component.get("v.srId");
      var documentMaster = component.get("v.documentMaster");
      var amedId = component.get("v.amedId");
      var RelamedId = component.get("v.relatedAmendID");
      console.log(" loadSRDocs srId-----", srId);
      console.log(" loadSRDocs documentMaster -----", documentMaster);
      console.log(component.get("v.relatedAmendID"));

      var action = component.get("c.viewRelatedSRDocs");
      var reqWrapPram = {
        srId: srId,
        docMasterCode: documentMaster,
        amedId: RelamedId,
      };
      // docMasterContentDocMap : docMasterContentDocMap
      console.log("######### reqWrapPram LoadSR ", reqWrapPram);
      action.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram),
      });

      // set call back
      action.setCallback(this, function (response) {
        try {
          var state = response.getState();
          if (state === "SUCCESS") {
            var result = response.getReturnValue();
            console.log("in load rel srDocs>>");
            console.log(JSON.stringify(result));
            console.log(result);
            // debugger;
            if (result.relDoc) {
              var docker = result.relDoc;
              console.log(docker);
              if (docker.HexaBPM__Doc_ID__c) {
                component.set("v.contentDocumentId", docker.HexaBPM__Doc_ID__c);
                console.log(
                  "v.contentDocumentId" + component.get("v.contentDocumentId")
                );

                var mapSRDocs = result.mapSRDocs;
                console.log("$$$$$$$ mapSRDocs ", mapSRDocs);
                if (mapSRDocs && mapSRDocs[documentMaster].File_Name__c) {
                  component.set(
                    "v.fileName",
                    mapSRDocs[documentMaster].File_Name__c
                  );
                  //fileUploaded
                  component.set("v.fileUploaded", true);
                }
              }
            } else {
              if (result.CDl) {
                console.log("in old amend");
                component.set("v.contentDocumentId", result.CDl);
                component.set("v.fileName", result.fileName);
                component.set("v.fileUploaded", true);
              }
            }
          } else if (state === "INCOMPLETE") {
            //alert("From server: " + response.getReturnValue());
          } else if (state === "ERROR") {
            var errors = response.getError();
            if (errors) {
              if (errors[0] && errors[0].message) {
                //helper.displayToast("Error", errors[0].message);
                console.log("Error message: " + errors[0].message);
                console.log("Error message: " + errors[0]);
              }
            } else {
              console.log("Unknown error");
            }
          }
        } catch (error) {
          console.log(error.message);
        }
      });
      // enqueue the action
      $A.enqueueAction(action);
    } catch (error) {
      console.log(error.message);
    }
  },

  callOCR: function (cmp, event, helper, documentIdParam) {
    var parseFile = cmp.get("v.parseFile");
    //Calling OCR comes here.....parseFile == true.... remaining
    if (parseFile && documentIdParam) {
      console.log("==call OCR===");
      helper.showSpinner(cmp, event, helper);
      var OCRAction = cmp.get("c.processOCR");

      OCRAction.setParams({
        fileId: documentIdParam,
      });

      OCRAction.setCallback(this, function (response) {
        console.log(
          "@@@@@@@@@@@@ respOCR ",
          JSON.stringify(response.getReturnValue())
        );
        var state = response.getState();

        if (cmp.isValid() && state === "SUCCESS") {
          helper.hideSpinner(cmp, event, helper);
          console.log(
            "@@@@@@@@@@@@ respOCR ",
            JSON.stringify(response.getReturnValue())
          );
          var result = JSON.stringify(response.getReturnValue());
          if (result != "null" && !$A.util.isEmpty(result)) {
            var ocrEvent = cmp.getEvent("ocrEvent");
            ocrEvent.setParams({
              ocrWrapper: response.getReturnValue(),
            });
            ocrEvent.fire();
          }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          helper.hideSpinner(cmp, event, helper);
          //alert('Error  ',response.getError()[0].message);
        }
      });
      $A.enqueueAction(OCRAction);
    } else {
      console.log("=no ocr creation==" + parseFile);
      helper.hideSpinner(cmp, event, helper);
    }
  },
  // function automatic called by aura:waiting event
  showSpinner: function (component, event, helper) {
    // make Spinner attribute true for displaying loading spinner
    component.set("v.spinner", true);
  },

  // function automatic called by aura:doneWaiting event
  hideSpinner: function (component, event, helper) {
    // make Spinner attribute to false for hiding loading spinner
    component.set("v.spinner", false);
  },
});