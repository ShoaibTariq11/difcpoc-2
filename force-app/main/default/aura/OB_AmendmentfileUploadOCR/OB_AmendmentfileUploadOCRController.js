({
  doInit: function(cmp, event, helper) {
    let newURL = new URL(window.location.href).searchParams;
    var flowIdParam = cmp.get("v.flowId")
      ? cmp.get("v.flowId")
      : newURL.get("flowId");
    var srIdParam = cmp.get("v.srId") ? cmp.get("v.srId") : newURL.get("srId");
    var pageIdParam = cmp.get("v.pageId")
      ? cmp.get("v.pageId")
      : newURL.get("pageId");
    // set if comming from url
    cmp.set("v.pageId", pageIdParam);
    cmp.set("v.flowId", flowIdParam);
    cmp.set("v.srId", srIdParam);

    console.log("####@@@@@@ pageId ", pageIdParam);
    console.log("####@@@@@@ flowIdParam ", flowIdParam);
    console.log("####@@@@@@ srIdParam ", srIdParam);

    console.log("######### in detail do init ");
    helper.loadSRDocs(cmp, event, helper);
  },

  handleUploadFinished: function(cmp, event, helper) 
  {
    
    var uploadedFiles = event.getParam("files");
    var fileName = uploadedFiles[0].name;
    var contentDocumentId = uploadedFiles[0].documentId;
    if (fileName) {
      cmp.set("v.fileName", fileName);
    }
    if (contentDocumentId) {
      cmp.set("v.contentDocumentId", contentDocumentId);
      // fileUploaded
      cmp.set("v.fileUploaded", true);
      cmp.set("v.requiredText", "");
    }

    //calling OCR
    helper.callOCR(cmp, event, helper, contentDocumentId);
    // Document master.
      /*
            var docMasterCode = cmp.get("v.documentMaster");  
            var mapDocValues = {};
            var docMap = cmp.get("v.docMasterContentDocMap");
            for(var key in docMap)
            {
                mapDocValues[key] = docMap[key];
            }
            
            mapDocValues[docMasterCode] = contentDocumentId;
            console.log(mapDocValues);
    
            console.log('@@@@@@ docMasterContentDocMap ');
            cmp.set("v.docMasterContentDocMap",mapDocValues);
            console.log(JSON.parse(JSON.stringify(cmp.get("v.docMasterContentDocMap"))));
       */ 
  }
});