({
  doInit: function(component, event, helper) {
    helper.GetEntityData(component, event, helper);
  },

  handleFilter: function(component, event, helper) {
    var relAccountList = component.get("v.unfilteredAccList");
    var searchText = component.get("v.searchString");
    var filteredType = component.get("v.selectedValue");

    console.log(filteredType);

    if (searchText.trim()) {
      relAccountList = relAccountList.filter(acc =>
        acc.Name.toLowerCase().includes(searchText.toLowerCase())
      );
    }
    if (filteredType && filteredType != "All") {
      relAccountList = relAccountList.filter(acc =>
        acc.Legal_Entity_Type__c.includes(filteredType)
      );
    }

    component.set("v.respWrap.relatedAccounts", relAccountList);

    console.log(relAccountList);
  },

  createEntity: function(component, event, helper) {
    component.set("v.showSpinner", true);
    var navUrl = component.get("v.respWrap.pageFlowURl");
    var relSr = component.get("v.respWrap.relEntitySr");

    if (relSr) {
      window.location.href = "ob-processflow" + navUrl + "&srId=" + relSr.Id;
      return;
    }

    var action = component.get("c.createNewEntitySr");
    var requestWrap = {
      contactId: component.get("v.respWrap.loggedInUser.ContactId")
    };
    action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        window.location.href =
          "ob-processflow" + navUrl + "&srId=" + respWrap.insertedSr.Id;
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
      component.set("v.showSpinner", false);
    });
    $A.enqueueAction(action);
  },

  viewAsSelectedAccount: function(component, event, helper) {
    component.set("v.showSpinner", true);
    var selectedAccount = event.currentTarget.dataset.selaccid;
    var contactId = component.get("v.respWrap.loggedInUser.ContactId");

    var action = component.get("c.setContactMainAccount");
    var requestWrap = {
      mainAccIdToSet: selectedAccount,
      contactId: contactId
    };
    console.log(requestWrap);
    action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        if (!respWrap.errorMessage) {
          var urlEvent = $A.get("e.force:navigateToURL");
          urlEvent.setParams({
            url: "/"
          });
          urlEvent.fire();
        } else {
          helper.showToast(
            component,
            event,
            helper,
            "error",
            respWrap.errorMessage
          );
        }
        component.set("v.showSpinner", false);
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
      component.set("v.showSpinner", false);
    });
    $A.enqueueAction(action);
  }
});