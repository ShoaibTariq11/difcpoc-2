({
  GetEntityData: function(component, event, helper) {
    component.set("v.showSpinner", true);
    var action = component.get("c.fetchEntityData");

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        helper.setFilterVals(
          component,
          event,
          helper,
          respWrap.relatedAccounts
        );
        component.set("v.respWrap", respWrap);
        component.set("v.unfilteredAccList", respWrap.relatedAccounts);
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
      component.set("v.showSpinner", false);
    });
    $A.enqueueAction(action);
  },

  setFilterVals: function(component, event, helper, accList) {
    var options = [
      {
        label: "All",
        value: "All"
      }
    ];
    for (const account of accList) {
      if (account.Legal_Entity_Type__c) {
        options.push({
          label: account.Legal_Entity_Type__c,
          value: account.Legal_Entity_Type__c
        });
      }
    }

    component.set("v.options", options);
  },

  showToast: function(component, event, helper, type, message) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      message: message,
      duration: " 5000",
      key: "info_alt",
      type: type,
      mode: "pester"
    });
    toastEvent.fire();
  }
});