({
  doInit: function(component, event, helper) {
    helper.getExistingAmend(component, event, helper);
  },

  onFocus: function(cmp, evt, helper) {},

  onChange: function(cmp, evt, helper) {
    console.log("onchange of the picklicst");

    //set selectedValue to the id of the selected amendobj from dropdown
    var otherList = cmp.get("v.AmendListWrapp.otherRolesAmendList");
    var index = cmp.find("select").get("v.value");
    console.log(index);

    var selectedAmend = otherList.find(amend => amend.Id === index);
    console.log(selectedAmend);
    cmp.set("v.selectedAmend", selectedAmend);
    cmp.set("v.amedIdInFocus", "none");

    //var selectedAmend = otherList[index];

    //console.log(selectedAmend);

    /* if (selectedAmend != undefined) {
      cmp.set("v.selectedValue", selectedAmend.Id);
    } else {
      cmp.set("v.selectedValue", "");
    }
    console.log("selected value " + cmp.get("v.selectedValue"));
    console.log("amend in focus " + cmp.get("v.amedIdInFocus")); */
  },

  showConfirmForm: function(cmp, evt, helper) {
    var selectedAmend = cmp.get("v.selectedAmend");

    cmp.set("v.amedIdInFocus", selectedAmend.Id);
    //cmp.set("v.ShowConvertForm", true);
  },

  createNewAppPerson: function(component, event, helper) {
    console.log("show create new amend rec");

    try {
      var action = component.get("c.initNewAmendment");

      var requestWrap = {
        srId: component.get("v.srId")
      };
      action.setParams({
        requestWrapParam: JSON.stringify(requestWrap)
      });

      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(response.getReturnValue());
          component.set("v.newAmendWrap", respWrap.newAmendToCreate);
          component.set("v.amedIdInFocus", "newRec");
          component.set("v.ShowNewForm", true);
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }

    //component.set("v.amedIdInFocus", "newRec");
  },

  handleButtonAct: function(component, event, helper) {
    try {
      let newURL = new URL(window.location.href).searchParams;

      var srID = newURL.get("srId");
      var pageID = newURL.get("pageId");
      var buttonId = event.target.id;
      console.log(buttonId);
      var action = component.get("c.getButtonAction");

      action.setParams({
        SRID: srID,
        pageId: pageID,
        ButtonId: buttonId
      });

      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log("button succsess");

          console.log(response.getReturnValue());
          window.open(response.getReturnValue().pageActionName, "_self");
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  }
});