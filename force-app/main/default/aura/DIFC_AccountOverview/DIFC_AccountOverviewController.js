({
	init: function(component, event, helper){
        var declare= "<p>I hereby consent on behalf of the abovementioned entity, to submit all forms related to the Registrar of Companies and Commissioner of Data Protection (“the Statutory Bodies”) electronically on the DIFC Client Portal."
        +"<div id='collapse' >I hereby confirm the following on behalf of the abovementioned entity:<br/>"
+"<ul>"
+"<li>1. that any forms lodged on the DIFC Client Portal can be used as if the original form was submitted</li>"
+"<li>2. that the original form(s) submitted on the DIFC Client Portal shall be retained by the abovementioned entity as evidence of its authenticity, and that if a question of authenticity arises, the relevant Statutory Body may require submission of the original form(s)</li>"
+"<li>3. the individual whose name is mentioned above is authorized by the abovemnetioned entity to file the applicable form(s) and submit any required document(s) on the DIFC Client Portal on its behalf.</li></ul><br/>"
+"I understand that is is my responsibility to ensure that the abovementioned indivudal is duly autorized by the abovemnetioned entity to be a portal user and file the applicable form(s) and submit any required document(s) on the DIFC Client Portal on its behalf."
+"I understand that it is an offence to provide information which is false, misleading or deceptive or to conceal information where the concealment of such information is likely to mislead or deceive.</div></p><a href='#collapse' class='nav-toggle'>Read More</a>";
    	component.set("v.sObjectName",declare);
        console.log('===init===');
    },
    entityProfile: function(component, event, helper){
    	var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/difc-enityprofile"
        });
    
        urlEvent.fire();
    }
})