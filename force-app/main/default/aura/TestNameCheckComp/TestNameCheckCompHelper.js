({
    expandAndCollapseAccordian : function(component, event,helper) {
        
        console.log('inside helper expandAndCollapseAccordian');
        
        
        //document.getElementById("staticPathway").style.display = "none";
        var cmpTar= document.getElementById('nameCheckCard');
        var difcOpenCardSections = document.querySelectorAll('.difc-card--expanded');
        difcOpenCardSections &&
            difcOpenCardSections.length>0 &&
            difcOpenCardSections.forEach(function(openSection){
                
                openSection.classList.remove('difc-card--expanded');
                var uiCards = openSection.querySelector('.ui-form-card','ui-card');
                if(uiCards == cmpTar ){
                    return;
                }
                uiCards && uiCards.classList.add('ui-form-card--collapsed' ,'ui-form-card--collapsedd');
            });
        if (cmpTar.classList.contains('ui-form-card--collapsed')) {
            console.log("inside if collapsed");
            //document.getElementById("staticPathway").style.display = "none";
            component.set("v.subtitle","Reserve your entity name");
            cmpTar.classList.remove('ui-form-card--collapsed');
            
            
            //Remove opacity if the card is expanded
            var cmpTar= document.getElementById('nameCheckCard');
            cmpTar.classList.remove('theme_change');
            
            //Dynamic pathway guide
            var difcCardDesc = cmpTar.querySelector('.ui-form-card__title-panel').id;
            if (difcCardDesc){ 
                difcCardDesc = difcCardDesc.replace('','.');
                cmpTar.closest('.difc-card-section').querySelector(difcCardDesc).style.display='block';
            }
            cmpTar.closest('.difc-card-section').classList.add('difc-card--expanded');
            cmpTar.scrollIntoView(true);
        } else {
            component.set("v.subtitle","Start now");
            cmpTar.classList.add('ui-form-card--collapsed');
            cmpTar.closest('.difc-card-section').classList.remove('difc-card--expanded');
        }
    },
    
    fetchPickListVal: function(component, fieldName, elementId) {
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.CompanyName"),
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                
                //console.log("opts is "+JSON.stringify(opts));
                //console.log("element id is "+elementId);
                
                if(fieldName=='Country_Of_Registration__c'){
                    component.set("v.countryList", opts);
                    //console.log("countryList is "+component.get("v.countryList"));
                }
                
                if(fieldName=='Check_Identical_to_existing__c'){
                    component.set("v.yesNo", opts);
                    //console.log("countryList is "+component.get("v.countryList"));
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    createDynamicLightningCompHelper:function(component, event,helper){
        let newURL = new URL(window.location.href).searchParams;
        var action= component.get("c.returnRecordsCompanyName");
        action.setParams({
            "srId": newURL.get('srId')
        });
        action.setCallback(this,function(response){
            var state= response.getState();
            var retValue= response.getReturnValue();
            //console.log("inside createDynamicLightningCompHelper callback");
            //console.log("retValue createDynamicLightningCompHelper is "+JSON.stringify(retValue));
            var respLength = retValue.length;
            //console.log("respLength createDynamicLightningCompHelper is "+respLength);
            
            var accessDynamicComp = document.getElementsByClassName("dynamicComp");
            var numberofDynamicComp = accessDynamicComp.length;
            //console.log("numberofDynamicComp is "+numberofDynamicComp);
            
            if(respLength<3 && numberofDynamicComp==respLength){
                $A.createComponent(
                    "c:OB_NameCheckDynamicComponent",
                    {},
                    function(dynamicCompCreate){                
                        if (component.isValid()) {
                            var targetCmp = component.find('induceNameCheckDynamicComponent');
                            //console.log("targetCmp createDynamicLightningCompHelper "+targetCmp);
                            //console.log("dynamicCompCreate createDynamicLightningCompHelper "+dynamicCompCreate);
                            var body = targetCmp.get("v.body");
                            body.push(dynamicCompCreate);
                            targetCmp.set("v.body", body); 
                        }
                    }
                ); 
            }
            
            else{
                if(numberofDynamicComp!=respLength){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Alert!",
                        "message": "Please enter the name in the current block"
                    });
                    toastEvent.fire();
                }
                
                if(respLength>=3){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Alert!",
                        "message": "You have already added three names"
                    });
                    toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    commitNameRecordHelper: function(component, event,helper) {
        console.log('commitNameRecordHelper');
        let newURL = new URL(window.location.href).searchParams;
        //console.log("inside commitNameApex DIFC_NameCheckblock");
        //console.log(JSON.parse(JSON.stringify(component.get("v.TransactinalStageSR"))));
        console.log(component.get("v.CompanyName"));
        var action = component.get("c.commitNameApex");   
        action.setParams({
            srId : newURL.get('srId'),
            companyName : component.get("v.CompanyName"),
            docMap: component.get("v.docMasterContentDocMap")
        })
        action.setCallback(this, function(response) {
            var retVal = response.getReturnValue();
            console.log(retVal);
            var lstCompany = retVal.lstCompany;
            var state=response.getState();
            //console.log("response from commitNameApex DIFC_NameCheckblock"+retVal);
            //console.log("state is "+state);
            
            if(state=="SUCCESS" && retVal.lstCompany ){
                /*
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The Names are saved successfully"
                });
                toastEvent.fire();  
                */
                var cmpEvent = component.getEvent("companyNameEvent");
                console.log(cmpEvent);
                cmpEvent.setParams({
                    "showNewPanel" : "false",
                    "companyList" : retVal.lstCompany
                });
                cmpEvent.fire();
            }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": retVal.errorMessage,
                        "type":"error"
                    });
                    toastEvent.fire();
                }
            //console.log("inside success submit for name commitNameRecordHelper");
        });
        
        $A.enqueueAction(action); 
        //helper.expandAndCollapseAccordian(component, event, helper);
    },
    translateSourceText:function(component,event, helper, sourceText){
        var translateMap = {};
        translateMap["Trading_Name__c"] = "Arabic_Trading_Name__c";
        translateMap["Entity_Name__c"] = "Arabic_Entity_Name__c";
        var a = event.getSource();
        var id ;
        if(sourceText)
            id = sourceText;
        else
            id = a.getLocalId();

        var attributeInstance = "v.CompanyName.";
        var sourceAttributeInstance = attributeInstance + id;
        var destAttributeInstance = attributeInstance + translateMap[id];
        var translateText = component.get(sourceAttributeInstance);
        console.log(sourceAttributeInstance);
        console.log(destAttributeInstance);
        console.log(translateText);
        if (translateText) {
            var action = component.get("c.translateToArabic");
            action.setParams({
                text: translateText,
            });

            // set call back 
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    component.set(destAttributeInstance,result);
                    console.log(result);
                } else if (state === "INCOMPLETE") {
                    alert("From server: " + response.getReturnValue());
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            helper.displayToast('Error', errors[0].message);
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            // enqueue the action 
            $A.enqueueAction(action);
        }
    },
    /**
     * Display a message
     */
    displayToast: function (title, message) {
        console.log('displayToast');
        var toast = $A.get('e.force:showToast');

        // For lightning1 show the toast
        if (toast) {
            //fire the toast event in Salesforce1
            toast.setParams({
                'title': title,
                'message': message,
                mode: 'sticky'
            });

            toast.fire();
        } else { // otherwise throw an alert        
            alert(title + ': ' + message);
        }
    },
    updateProgressIndicatorOnExpand : function(component,event){
        //console.log('inside helper updateOnLoad');
        var totalElements = component.get("v.totalElements");
        var liPrefix = component.get("v.liPrefix");
        var sectionDone = true;
        for(var i=1;i<=totalElements;i++){
            //console.log('inside loop'+i);
            var currentId = liPrefix+'-'+i;
            //console.log('currentId'+currentId);
            var eachVal = component.find(currentId).get("v.value");
            //console.log('eachVal'+eachVal);
            if(eachVal != "" && eachVal != undefined){
                var eachli = document.getElementById(currentId);
                eachli.classList.add('slds-is-complete');
                var eachSpan = document.getElementById(currentId+'-span');
                eachSpan.classList.remove('hideSpan');
            }else{
                sectionDone = false;
            }
        }
        if(sectionDone){
            var sectionActive = document.getElementById("NCsectionActive");
            var sectionComplete = document.getElementById("NCsectionComplete");
            sectionActive.classList.add('showHide');
            sectionComplete.classList.remove('showHide');
        }
    },
    
    loadDynamicComponents: function(component,event,helper){
        //console.log("inside doInit loadDynamicComponents");
        let newURL = new URL(window.location.href).searchParams;
        var action= component.get("c.returnRecordsCompanyName");
        action.setParams({
            "srId": newURL.get('srId')
        });
        action.setCallback(this,function(response){
            var state= response.getState();
            var retValue= response.getReturnValue();
            //console.log("inside callback loadDynamicComponents");
            //console.log("retValue is loadDynamicComponents"+JSON.stringify(retValue));
            var respLength = retValue.length;
            //console.log("respLength loadDynamicComponents is "+respLength);
           
            if(retValue[0]){
                component.set("v.CompanyName",retValue[0]);
            }
            //console.log("first name prefilled is "+JSON.stringify(component.get("v.CompanyName")));
            //Loop starts from i=1 so that prefilling for dynamic components start from second name whereas first name
            //is prefilled to the existing markup
            
            for(var i=1;i<respLength;i++){
                $A.createComponent(
                    "c:OB_NameCheckDynamicComponent",
                    {
                        "CompanyNameSecond":retValue[i]
                    },
                    function(loadDynamicComp){                
                        if (component.isValid()) {
                            var targetCmp = component.find('induceNameCheckDynamicComponent');
                            //console.log("targetCmp "+targetCmp);
                            //console.log("loadDynamicComp "+loadDynamicComp);
                            var body = targetCmp.get("v.body");
                            body.push(loadDynamicComp);
                            targetCmp.set("v.body", body); 
                        }
                    }
                );
            }
        })
        $A.enqueueAction(action);
    },
    
    updateEndsWith: function(component,event,helper){
        //Setting the Arabic Ends With - Value on change of EndsWith English name.
        console.log(component.find('endswithCheck').get('v.value'));
        var endsWith = component.get("v.endsWithList"), 
        value = component.find("endswithCheck").get("v.value"),index;
        console.log(endsWith);
        endsWith.forEach(function(v,i,a) {             
            if(v.value == value) {
                index = i;
            }
        });
        console.log(endsWith[index].arabicValue);
        component.set("v.CompanyName.Arabic_Ends_With__c",endsWith[index].arabicValue);
        //contactName = contacts[index].Name;
        //console.log(event.getSource().getElement());
        //console.log(event.getParam("value"));
        console.log(event.getSource().get('v.id'));
        //console.log(event.getParam("selectedIndex"));
        /* 
        var CompanyNameId = component.get("v.CompanyName.Id");
        var action= component.get("c.updateEndsWithApex");
        
        action.setParams({
            CompanyNameId:CompanyNameId,
            EndsWith:component.get("v.CompanyName.Ends_With__c")
        })
        
        action.setCallback(this,function(response){
            var returnValueEndsWith = response.getReturnValue();
            var state= response.getState();
            if(state=="SUCCESS"){
                console.log("inside success updateEndsWith");
            }
        });
        $A.enqueueAction(action); 
        */
        
    },
    
    upsertSRFieldsNameCheck:function(component){
        var action = component.get("c.upsertTransactionStageFieldsNameCheckApex");
        //console.log("inside upsertSRFieldsNameCheck>>>>>>>");
        
        action.setParams({
            nameCheckTransStage: component.get("v.TransactinalStageSR")
        })
        //console.log("record sent to apex upsertTransactionStageFieldsNameCheck is "+JSON.stringify(component.get("v.TransactinalStageSR")));
        action.setCallback(this,function(response){
            var returnValueNameCheckTransStage = response.getReturnValue();
            var state= response.getState();
            //console.log("returnValueNameCheckTransStage is "+JSON.stringify(returnValueNameCheckTransStage));
            //console.log("state upsertTransactionStageFieldsNameCheck is "+state);
            
            if(state=="SUCCESS"){
                if(returnValueNameCheckTransStage && returnValueNameCheckTransStage!='undefined'){
                    component.set("v.TransactinalStageSR",returnValueNameCheckTransStage);
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    loadTransactionalStageCompNameValues:function(component,event,helper){
        console.log("inside loadTransactionalStageCompNameValues");
        //component.set("v.CompanyName.Entity_Name__c","test");
        //component.get("v.CompanyName.Entity_Name__c").set("v.disabled",true);
        var returnValueTransCompName = component.get("v.ResponseResult");
        
        //console.log("loadTransactionalStageCompNameValues det "+JSON.stringify(returnValueTransCompName));
        //console.log("state returnValueTransCompName is >>>"+returnValueTransCompName);
        console.log(returnValueTransCompName);
        console.log("inside success >>");
        component.set('v.commandButton', returnValueTransCompName.ButtonSection);
        console.log(returnValueTransCompName.lstCompany.length);
        component.set("v.companyList",returnValueTransCompName.lstCompany);
        //console.log("inside success loadTransactionalStageCompNameValues" + returnValueTransCompName.sr.HexaBPM__SR_Docs__r[0].Id);
        component.set("v.srWrap",returnValueTransCompName);
        component.set("v.TransactinalStageSR",returnValueTransCompName.sr);
        component.set("v.srId",returnValueTransCompName.sr.Id);
        if(returnValueTransCompName.sr.HexaBPM__SR_Docs__r && returnValueTransCompName.sr.HexaBPM__SR_Docs__r.length > 0){
            component.set("v.franchiseDoc",returnValueTransCompName.sr.docId);
            var srDocs = returnValueTransCompName.sr.HexaBPM__SR_Docs__r;
            console.log(srDocs);
            for(var i = 0 ; i < srDocs.length ; i++){
                if(srDocs[i].HexaBPM__Document_Master__r.HexaBPM__Code__c == 'Franchise_agreement')
                    component.set("v.franchiseDoc",srDocs[i]);
                if(srDocs[i].HexaBPM__Document_Master__r.HexaBPM__Code__c == "Evidence_of_relationship_and_consent"){
                    component.set("v.evidenceDoc",srDocs[i]);
                    component.set("v.evidenceDocFileName",srDocs[i].File_Name__c)
                }
            }
        }
        var endsWithListItems = returnValueTransCompName.lstEndsWith;
        var options = [];
        var option=[];
        if(endsWithListItems.length > 0){
            option=[];
            option["value"] = '';
            option["label"] = 'None';
            option["arabicValue"] = '';
            options.push(option);
        }
        for(var i=0;i<endsWithListItems.length;i++){
            option=[];
            option["value"] = endsWithListItems[i].endsWithEnglish;
            option["label"] = endsWithListItems[i].endsWithEnglish;
            option["arabicValue"] = endsWithListItems[i].endsWithArabic;
            options.push(option);
        }
        console.log('init options');
        console.log(options.length);
        console.log(options);
        component.set("v.endsWithList",options);

        //Move to createobjectdata
        //If setup='branch', then the proposed company name should be the same as the parent company name
        
        console.log('returnValueTransCompName.sr.Setting_Up__c'+returnValueTransCompName.sr.Setting_Up__c);
        if(returnValueTransCompName.sr.Setting_Up__c && returnValueTransCompName.sr.Setting_Up__c.toLowerCase() == 'branch'){
            var entityName = component.get("v.CompanyName.Entity_Name__c");
            var tradingName = component.get("v.CompanyName.Trading_Name__c");
            console.log('entityName>'+entityName);
            component.set("v.isBranchSR","true");

            //if entity name = blank, then populate the foreign name.
            if(!entityName){
                console.log('setting value fk'+returnValueTransCompName.sr.Foreign_Entity_Name__c);
                //component.find("Entity_Name__c").set("v.value",returnValueTransCompName.sr.Foreign_Entity_Name__c);
                component.set("v.CompanyName.Entity_Name__c",returnValueTransCompName.sr.Foreign_Entity_Name__c);
                console.log('value set');
                helper.translateSourceText(component,event,helper,'Entity_Name__c');  
            }
             //if trading name = blank, then populate the foreign name.
            if(!tradingName){
                component.set("v.CompanyName.Trading_Name__c",returnValueTransCompName.sr.Foreign_Entity_Name__c);  
                helper.translateSourceText(component,event,helper,'Trading_Name__c');  
            }
            //If setup=branch and sector = 'representative office', Trading Name must end with the phrase “(DIFC Representative Office)” at the end of your Trading Name
            if(returnValueTransCompName.sr.Business_Sector__c && returnValueTransCompName.sr.Business_Sector__c.toLowerCase() == 'representative office'){
                component.set("v.addRepOffice","true");
            }
        }
        //If setup='transfer',Disable the company name and trading name field 
        else if(returnValueTransCompName.sr.Setting_Up__c && returnValueTransCompName.sr.Setting_Up__c.toLowerCase() == 'transfer'){
            console.log('returnValueTransCompName.sr.Foreign_Entity_Name__c'+returnValueTransCompName.sr.Foreign_Entity_Name__c);
            
            //Setting the Entity Name and Trading name - Values
            component.set("v.CompanyName.Entity_Name__c",returnValueTransCompName.sr.Foreign_Entity_Name__c);
            component.set("v.CompanyName.Trading_Name__c",returnValueTransCompName.sr.Foreign_Entity_Name__c);
            
            component.find("Entity_Name__c").set("v.disabled",true);
            component.find("Trading_Name__c").set("v.disabled",true);
        }
        //The following Persons are not permitted to have Trading Names:hide the trading name selection
        var hideTradingNames = ['non profit incorporated organisation (npio)','foundation','prescribed companies'];
        if(returnValueTransCompName.sr.Business_Sector__c && 
            hideTradingNames.indexOf(returnValueTransCompName.sr.Business_Sector__c.toLowerCase()) > -1){
            component.set("v.showTradingName","false");
        }

        //The word trading name should be removed in case of NPIO, Prescribed Company and Foundation
        var removeTradingNameForBusinessSector = ['non profit incorporated organisation (npio)','prescribed companies','foundation'];
        if(returnValueTransCompName.sr.Business_Sector__c  
            && removeTradingNameForBusinessSector.indexOf(returnValueTransCompName.sr.Business_Sector__c.toLowerCase() > -1)){
            component.set("v.proposedEntityLabel","Entity");
        }
        component.set("v.isCompanyReserved",returnValueTransCompName.isCompanyReserved);
        //Translate the Entity Name on load to Arabic

        
    },
    clearSRValues : function(component,event,helper){
        //component.set('v.TransactinalStageSR.Check_Identical_to_existing__c','');
        //component.set('Fall_within_existing_entities__c','');
        component.set('v.TransactinalStageSR.Parent_Entity_Name__c','');
        component.set('v.TransactinalStageSR.Former_Name__c','');
        component.set('v.TransactinalStageSR.Date_of_Registration__c','');
        component.set('v.TransactinalStageSR.Place_of_Registration__c','');
        component.set('v.TransactinalStageSR.Country_of_Registration__c','');
        component.set('v.TransactinalStageSR.Registered_No__c','');
        component.set('v.TransactinalStageSR.Family_Group__c','');
        component.set('v.TransactinalStageSR.Registered_address__c','');
        component.set('v.TransactinalStageSR.Address_Line_1__c','');
        component.set('v.TransactinalStageSR.Address_Line_2__c','');
        component.set('v.TransactinalStageSR.Country__c','');
        component.set('v.TransactinalStageSR.City_Town__c','');
        component.set('v.TransactinalStageSR.State_Province_Region__c','');
        component.set('v.TransactinalStageSR.Po_Box_Postal_Code__c','');
    },
    searchName:function(component,event,helper){
        var urlEvent = $A.get("e.force:navigateToURL");
        var nameToBeSent = component.get("v.CompanyName.Entity_Name__c") ? component.get("v.CompanyName.Entity_Name__c") : '';
        var nameId= component.get("v.CompanyName.Id");
        let newURL = new URL(window.location.href).searchParams;
        var srId = newURL.get('srId');
        var pageId = newURL.get('pageId');
        var flowId = newURL.get('flowId');
        urlEvent.setParams({
                "url": "/ob-namecheck/?valueTextEntity= " +nameToBeSent+"&NameId="+nameId+"&srId="+srId+"&pageId="+pageId+"&flowId="+flowId
        });
        urlEvent.fire();
    },
    getEntityDetailsLoadEndsWith:function(component,event,helper){
        let newURL = new URL(window.location.href).searchParams;
        //console.log("inside getEntityDetails");
        var action = component.get("c.getEntityDetails");
        action.setParams({
            srId : newURL.get('srId')
        })
        action.setCallback(this,function(response){
            var returnValueCompName = response.getReturnValue();
            var state= response.getState();
            //console.log("getEntityDetails comp name entity is "+JSON.parse(JSON.stringify(returnValueCompName)));
            //console.log("state getEntityDetails comp name entity is "+state);
            
            if(state=="SUCCESS"){
                if(returnValueCompName && returnValueCompName!='undefined'){

                    //console.log(returnValueCompName);
                    component.set("v.EntityDetails",returnValueCompName);
                    if(returnValueCompName.HexaBPM__Customer__r.OB_Franchise__c)
                        component.set("v.isFranchise",returnValueCompName.HexaBPM__Customer__r.OB_Franchise__c.toLowerCase());
                    var legalStructure= component.get("v.EntityDetails[0].legal_structures__c");
                    var NewBranchTransfer = component.get("v.EntityDetails[0].Setting_Up__c");
                    var BDSector=component.get("v.EntityDetails[0].Business_Sector__c");
                    var typeOfentity=component.get("v.EntityDetails[0].Type_of_Entity__c");
                    var Sector_NPIO = $A.get("$Label.c.Sector_NPIO");
                    var SetUpBranch = $A.get("$Label.c.SetUpBranch");
                    var SetUpTransfer = $A.get("$Label.c.SetUpTransfer");
                    
                    //console.log("type of entity is "+ typeOfentity);
                    /* --------------------------------------------------------------------------------------------------------------------------*/
                    //console.log("legalStructure comp name entity is "+legalStructure);
                    if(legalStructure=="Company"){
                        component.set("v.typeOfEntity","Company");
                    }
                    else if(legalStructure=="Partnership"){
                        component.set("v.typeOfEntity","Partnership");  
                    }
                        else if(legalStructure=="Foundation"){
                            component.set("v.typeOfEntity","Foundation");
                        }
                            else{
                                component.set("v.typeOfEntity","Entity");
                            }
                    /* --------------------------------------------------------------------------------------------------------------------------*/
                    if(NewBranchTransfer=="New"){
                        component.set("v.areYouEntity","Proposed Entity");
                    }
                    else if(NewBranchTransfer=="Branch"){
                        component.set("v.areYouEntity","Recognized Entity");  
                    }
                        else if(NewBranchTransfer=="Transfer"){
                            component.set("v.areYouEntity","Transferred Entity");
                        }
                    /* ----------------------------------------------------------------------------------------------------------------------------*/
                    
                    if(BDSector=="Special Purpose Company" || BDSector=="Non Profit Incorporated Organisation (NPIO)" || BDSector=="Intermediate Special Purpose Vehicle" || BDSector=="Foundation" ){
                        component.set("v.showTradingName","false"); 
                    }
                    /* ----------------------------------------------------------------------------------------------------------------------------*/
                    if(NewBranchTransfer==SetUpTransfer || NewBranchTransfer==SetUpBranch) {
                        component.set("v.showEndsWith","false");
                    }
                    /* ----------------------------------------------------------------------------------------------------------------------------*/
                    /*
                    var endsWithOptions=[];
                    if(typeOfentity=="Private" && BDSector!=Sector_NPIO){
                        if(BDSector=="Special Purpose Company"){
                            endsWithOptions.push({
                                class: "optionClass",
                                label: "SPC Limited",
                                value: "SPC Limited"
                            });
                            
                            endsWithOptions.push({
                                class: "optionClass",
                                label: "SPC Ltd",
                                value: "SPC Ltd"
                            });
                            component.set("v.endsWithList",endsWithOptions);
                            //console.log("endsWithList Private and SPCis "+component.get("v.endsWithList"));   
                        }
                        else {
                            endsWithOptions.push({
                                class: "optionClass",
                                label: "Limited",
                                value: "Limited"
                            });
                            
                            endsWithOptions.push({
                                class: "optionClass",
                                label: "Ltd",
                                value: "Ltd"
                            });
                            component.set("v.endsWithList",endsWithOptions);
                            //console.log("endsWithList Privateis "+component.get("v.endsWithList"));
                        }
                        
                    }
                    
                    if(BDSector==Sector_NPIO){
                        endsWithOptions.push({
                            class: "optionClass",
                            label: "Non-profit Incorporated Organisation",
                            value: "Non-profit Incorporated Organisation"
                        });
                        
                        endsWithOptions.push({
                            class: "optionClass",
                            label: "NPIO",
                            value: "NPIO"
                        });
                        component.set("v.endsWithList",endsWithOptions);
                        //console.log("endsWithList NPIO is "+JSON.stringify(component.get("v.endsWithList")));
                    }
                    */
                    
                }
            }
        });
        $A.enqueueAction(action);     
    },
    afterRenderExpandPage: function(component,event,helper){
        var urlString = window.location.href;
        var sURLVariables = urlString.split('submit-to-difc/?');
        var passedAttri = sURLVariables[1];
        if(passedAttri.includes("checkAvail")){
            var cmpTar= document.getElementById('nameCheckCard');
            if (cmpTar.classList.contains('ui-form-card--collapsed')) {
                cmpTar.classList.remove('ui-form-card--collapsed');
                cmpTar.closest('.difc-card-section').classList.add('difc-card--expanded');
            }else{
                cmpTar.classList.add('ui-form-card--collapsed');
            }
        }
    },
    
    getURLParameter : function(param) {
        var result=decodeURIComponent
        ((new RegExp('[?|&]' + param + '=' + '([^&;]+?)(&|#|;|$)').
          exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
        //console.log('Param ' + param + ' from URL = ' + result);
        return result;
    }
    
})