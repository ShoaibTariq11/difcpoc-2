({
    doInit: function(component, event, helper) {
        console.log('init');
        
         var vfOrigin = "https://uatfull-difcportal.cs128.force.com";
         window.addEventListener("message", function(event) {
            if (event.origin !== vfOrigin) {
                return;
            }
            console.log('----'+event.data.action);
            if(event.data.action == 'alohaCallingCAPTCHA' && event.data.alohaResponseCAPTCHA == 'NOK'){
                alert('Please do the captcha before submit!');
            }
            else if(event.data.action == 'alohaCallingCAPTCHA' && event.data.alohaResponseCAPTCHA == 'OK'){
                var urlEvent = $A.get("e.force:navigateToURL");
               urlEvent.setParams({
                        "url": 'https://uatfull-difcportal.cs128.force.com/digitalOnboarding/s/registerwithdifc'
                });
                urlEvent.fire();
            }
        }, false);
        
    },
    saveData : function(component, event, helper) {
      
        helper.showSpinner(component, event, helper);
        if(helper.validateServiceRequest(component)){
            // Create the new Service Request
            //var newSR = component.get('v.newSR');
            helper.createServiceRequest(component);
    }
          
    },   
    closeModel: function(component, event, helper) {
    
      component.set("v.isOpen", false);
      component.set("v.isRegistration",false);
      
   },
    showToast: function(title, message) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      title: title,
      message: message
    });
    toastEvent.fire();
  },
  /*closeModel: function(cmp, event, helper) {
    //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
    console.log("--toast close--");
    cmp.set("v.errorMessage", "");
    cmp.set("v.isError", false);
  }*/
})