({
  validateServiceRequest: function (component) {
    var validRecord = true;

    // Name must not be blank
    var firstnameField = component.find("FirstName");
    var fName = firstnameField.get("v.value");
    if ($A.util.isEmpty(fName)) {
      validRecord = false;
      firstnameField.set("v.errors", [
        { message: "First name cannot be blank" }
      ]);
    } else {
      firstnameField.set("v.errors", null);
    }
    var lastNameField = component.find("LastName");
    var Lname = lastNameField.get("v.value");

    if ($A.util.isEmpty(Lname)) {
      validRecord = false;
      lastNameField.set("v.errors", [
        { message: "Last name cannot be blank." }
      ]);
    } else {
      lastNameField.set("v.errors", null);
    }
    var EmailField = component.find("Email");
    var email = EmailField.get("v.value");
    var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if ($A.util.isEmpty(email) || $A.util.isUndefined(email)) {
      validRecord = false;
      EmailField.set("v.errors", [{ message: "Email cannot be blank." }]);
    } else if (
      !$A.util.isEmpty(email) &&
      !$A.util.isUndefined(email) &&
      email.match(regExpEmailformat) == null
    ) {
      validRecord = false;
      EmailField.set("v.errors", [
        { message: "Please enter the correct email." }
      ]);
    } else {
      EmailField.set("v.errors", null);
    }

    var PhoneField = component.find("Phone");
    var phone = PhoneField.get("v.value");
    var regPhoneFormat = /^[+][(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
    console.log("=phone==12==" + phone.match(regPhoneFormat));

    if ($A.util.isEmpty(phone) || $A.util.isUndefined(phone)) {
      validRecord = false;
      PhoneField.set("v.errors", [
        { message: "Mobile number cannot be blank." }
      ]);
    } else if (
      !$A.util.isEmpty(phone) &&
      !$A.util.isUndefined(phone) &&
      phone.match(regPhoneFormat) == null
    ) {
      console.log("==== error==");
      validRecord = false;
      PhoneField.set("v.errors", [
        {
          message:
            "Please enter a valid phone number in the format (i.e +971XXXXXXXXX)"
        }
      ]);
    } else if (
      !$A.util.isEmpty(phone) &&
      !$A.util.isUndefined(phone) &&
      phone.includes("+") == false
    ) {
      console.log("==== error==");
      validRecord = false;
      PhoneField.set("v.errors", [
        { message: "Mobile Number Should satrt with +" }
      ]);
    } else {
      PhoneField.set("v.errors", null);
    }

    if (validRecord == false) {
      console.log("hide");
      this.hideSpinner(component);
    }
    return validRecord;
  },

  createServiceRequest: function (component) {
    var newService = component.get("v.newSR");
    var action = component.get("c.saveService");
    action.setParams({
      service: newService
    });
    action.setCallback(this, function (response) {
      // Getting the response state
      var state = response.getState();
      // Check if response state is success
      if (state === "SUCCESS") {
        debugger;
        var respWrap = response.getReturnValue();
        console.log("@@@@@@@ rep", JSON.stringify(respWrap));

        if (!$A.util.isEmpty(respWrap.errorMessage)) {
          //alert(respWrap.errorMessage);
          console.log("Error");
          console.log(respWrap.errorMessage);
          this.hideSpinner(component);
        } else {
          component.set("v.serviceWrapper", response.getReturnValue());
          var emailExists = component.get("v.serviceWrapper");
          var pageID = emailExists.pageID;
          var srID = emailExists.serviceID;
          var flowID = emailExists.pageFlowID;

          if (emailExists.isEmailExisting) {
            component.set("v.isRegistration", true);
            this.hideSpinner(component);
          } else {
            var nextPageURL =
              "/" +
              $A.get("$Label.c.OB_Site_Prefix") +
              "/s/ob-processflow?pageId=" +
              pageID +
              "&flowId=" +
              flowID +
              "&srId=" +
              srID;
            //var nextPageURL = 'https://dcob1-portaldifc.cs100.force.com/RegisterDIFC?pageId='+pageID+'&flowId='+flowID+'&srId='+srID;

            this.hideSpinner(component);
            window.location.href = nextPageURL;
          }
        }
      } else {
        // Show an alert if the state is incomplete or error
        // convert this to show tost.
        console.log("@@@@@@@@ ", response.getReturnValue().errorMessage);

        // alert('response',response.getReturnValue().errorMessage);
        component.set("v.errorMessage", response.getError()[0].message);
        component.set("v.isError", true);
        this.hideSpinner(component);
      }
    });

    $A.enqueueAction(action);
  },
  // function automatic called by aura:waiting event
  showSpinner: function (component, event, helper) {
    // make Spinner attribute true for displaying loading spinner
    component.set("v.spinner", true);
  },

  // function automatic called by aura:doneWaiting event
  hideSpinner: function (component) {
    // make Spinner attribute to false for hiding loading spinner
    component.set("v.spinner", false);
  },
  showToast: function (component, event, helper, msg, type) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      mode: "dismissible",
      message: msg,
      type: type
    });
    toastEvent.fire();
  }
});