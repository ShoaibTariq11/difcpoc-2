({
	afterRender: function (component, helper) {
        this.superAfterRender();
        // interact with the DOM here
        helper.loadTransactionalStageCompNameValues(component,helper);
        helper.showHideIdenticalField(component, helper);
        helper.setEndsWithForSinglePicklistValue(component, helper);
        if(component.get("v.CompanyName.Id")) //Load the SRDoc action for the existing Company Details record
            helper.viewSRDocument(component,helper);
    }
})