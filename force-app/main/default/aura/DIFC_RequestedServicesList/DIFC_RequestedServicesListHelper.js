({
	SearchHelper: function(component, event,reqWrapPram) {
        // show spinner message
        //alert('==entered');
        component.find("Id_spinner").set("v.class" , 'slds-show');
        var action = component.get("c.getRequestedServices");
        action.setParams({
            "reqWrapPram": JSON.stringify(reqWrapPram)});
        
        action.setCallback(this, function(response) {
            
           // hide spinner when response coming from server 
            component.find("Id_spinner").set("v.class" , 'slds-hide');
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
              	component.set("v.requestedServices",storeResponse);
              
                // if storeResponse size is 0 ,display no record found message on screen.
                if (storeResponse.length == 0) {
                    component.set("v.Message", true);
                } else {
                    component.set("v.Message", false);
                }
                
                // set numberOfRecord attribute value with length of return value from server
                component.set("v.TotalNumberOfRecord", storeResponse.length);
                
            }else if (state === "INCOMPLETE") {
                alert('Response is Incompleted');
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    alert("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        
    },
})