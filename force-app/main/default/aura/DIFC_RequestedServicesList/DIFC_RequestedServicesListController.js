({
	fireApplicationEvent : function(cmp, event) {
        // Get the application event by using the
        // e.<namespace>.<event> syntax
        console.log('Request fireApplicationEvent--');
        var appEvent = $A.get("e.c:aeEvent");
        appEvent.fire();
    },
    callServer : function(component, event, helper) {
		//component.set("v.callInit",event.getParam("serviceTab"));
        //var callInit = component.get("v.callInit");
        //console.log('=requested service home='+callInit);
        //if(callInit == true){
            console.log('=requested service home indie ======');
            var reqWrapPram  =
            {
                serviceNumber: component.get("v.searchKeyword"),  
                requestType: component.get("v.requestType"),
                serviceStatus:component.get("v.statusValue")
            }
            
            helper.SearchHelper(component, event,reqWrapPram); 
        //}
	},
    handleEvent : function(component, event, helper) {
		component.set("v.callInit",event.getParam("serviceTab"));
        var callInit = component.get("v.callInit");
        if(callInit === true){
            console.log('=Requested service tab='+callInit);
            var reqWrapPram  =
            {
                serviceNumber: component.get("v.searchKeyword"),  
                requestType: component.get("v.requestType"),
                serviceStatus:component.get("v.statusValue")
            }
            
            helper.SearchHelper(component, event,reqWrapPram); 
        }
	}
})