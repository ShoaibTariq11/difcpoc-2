({
  doInit: function (component, event, helper) {
    helper.SearchHelper(component, event, component.get("v.recordLimit"));
  },

  validateAccount: function (component, event, helper) {
    try {
      var action = component.get("c.getCurrentAccountId");
      var ctarget = event.currentTarget;
      var redirectURL = ctarget.dataset.value;
      var isCPRenewal = ctarget.dataset.iscp;

      action.setCallback(this, function (a) {
        var state = a.getState(); // get the response state
        if (state == "SUCCESS") {
          if (a.getReturnValue()) {
            var currentUser = a.getReturnValue();
            var currentAccId = currentUser.Contact.AccountId;
            var currentAccName = currentUser.Contact.Account.Name;
            var initialAccId = component.get("v.initialAccId");
            console.log("currentAccId==>" + currentAccId);
            console.log("initialAccId==>" + initialAccId);
            if (initialAccId == currentAccId) {
              if (isCPRenewal == "true") {
                component.set("v.showSpinner", true);
                console.log("her?");

                var action2 = component.get("c.createCPRenewal");
                /* var requestWrap = {
              variableName: component.get("v.attributeName")
            };
            action.setParams({ requestWrapParam: JSON.stringify(requestWrap) }); */

                action2.setCallback(this, function (response) {
                  var state = response.getState();
                  if (state === "SUCCESS") {
                    component.set("v.showSpinner", false);
                    var respWrap = response.getReturnValue();
                    console.log(respWrap);
                    if (respWrap.errorMessage == null) {
                      /* window.location.href =
                        "ob-processflow" + respWrap.pageFlowURl; */
                      window.location.href =
                        respWrap.communityPage + respWrap.pageFlowURl;
                    } else {
                      helper.showToast(
                        component,
                        event,
                        helper,
                        respWrap.errorMessage,
                        "error"
                      );
                    }
                  } else {
                    component.set("v.showSpinner", false);
                    console.log(
                      "@@@@@ Error " + response.getError()[0].message
                    );
                    console.log(
                      "@@@@@ Error Location " +
                        response.getError()[0].stackTrace
                    );
                  }
                });
                $A.enqueueAction(action2);
              } else {
                if (redirectURL.startsWith("/")) {
                  var urlEvent = $A.get("e.force:navigateToURL");
                  urlEvent.setParams({
                    url: redirectURL,
                  });
                  urlEvent.fire();
                } else {
                  window.open(redirectURL, "_self");
                }
              }
            } else {
              var toastEvent = $A.get("e.force:showToast");
              toastEvent.setParams({
                title: "Error Message",
                message:
                  "The account that you are managing has been changed to " +
                  currentAccName +
                  ". Please refresh the page to continue.",
                duration: " 5000",
                key: "info_alt",
                type: "error",
                mode: "pester",
              });
              toastEvent.fire();
            }
          }
        } else if (state == "FAILURE") {
          console.log("Some problem ocurred");
        }
      });
      $A.enqueueAction(action);
    } catch (error) {
      console.log(error.message);
    }
  },
});