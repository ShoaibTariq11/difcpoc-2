({
  SearchHelper: function(component, event, limitRow) {
    console.log("SearchHelper");
    var action = component.get("c.getNextStep");
    /*
        action.setParams({
            "limitRow": limitRow
        });
        */
    action.setCallback(this, function(response) {
      // hide spinner when response coming from server
      //component.find("Id_spinner").set("v.class" , 'slds-hide');
      var state = response.getState();
      if (state === "SUCCESS") {
        var resp = response.getReturnValue();
        console.log(resp);

        component.set("v.displayComponent", resp.displayComponent);
        component.set("v.profileName", resp.profileName);
        component.set("v.initialAccId", resp.initialAccountId);

        var storeResponse = resp.lstPendingApproval;
        console.log(storeResponse);
        console.log(storeResponse.length);
        var entityActionList = [];
        if (storeResponse && storeResponse.length > 0) {
          for (var i = 0; i < limitRow; i++) {
            if (storeResponse[i]) {
              console.log(
                "storeResponse==>" + i + JSON.stringify(storeResponse[i])
              );
              entityActionList.push(storeResponse[i]);
            }
          }
        }

        component.set("v.entityAction", entityActionList);

        // if storeResponse size is 0 ,display no record found message on screen.
        if (storeResponse.length == 0) {
          component.set("v.Message", true);
        } else {
          component.set("v.Message", false);
        }

        // set numberOfRecord attribute value with length of return value from server
        component.set("v.TotalNumberOfRecord", storeResponse.length);
      } else if (state === "INCOMPLETE") {
        console.log("Response is Incompleted");
      } else if (state === "ERROR") {
        var errors = response.getError();
        console.log(errors);
        if (errors) {
          if (errors[0] && errors[0].message) {
            //alert("Error message: " + errors[0].message);
          }
        } else {
          console.log("Unknown error");
        }
      }
    });
    $A.enqueueAction(action);
  }
});