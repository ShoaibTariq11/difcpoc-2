({
	showToast: function(component, event, helper, msg, type) {
    // Use \n for line breake in string
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      mode: "sticky",
      message: msg,
      type: type
    });
    toastEvent.fire();
  },
  onClose : function(cmp, event, helper) 
    {
        var siteDomain =  $A.get("$Label.c.OB_Site_Prefix");
        var homePage = '/'+siteDomain+'/s/';
        console.log('######## homePage '+homePage);
        window.open(homePage,"_self");
        
    }
})