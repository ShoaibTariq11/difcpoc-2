({
    renderState: function(component) {
        var currentStep = component.get("v.currentStep"),
            allSteps = component.get("v.steps"),
            render = [],
            state = "slds-is-complete";
        allSteps.forEach(function(step) {
            if(currentStep === step) {
                state = "slds-is-current";
            } else if(state === "slds-is-current") {
                state = "slds-is-incomplete";
            }
            render.push({ label: step, selected: state === "slds-is-current", state: state });
        });
        component.set("v.renderInfo", render);
    },
    
    fetchPickListVal: function(component, fieldName) {
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.newParty"),
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        label: "Select an Option",
                        value: ""
                    });
                }
                for(var i = 0; i < allValues.length; i++) {
                    opts.push({
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.set("v.countryOptions", opts);
            }
        });
        $A.enqueueAction(action);
    },
    
    clientInfo : function(component, event) {
    	var action = component.get("c.userAccountInfo");
        action.setParams({
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var userinformation = response.getReturnValue();
                component.set("v.accountName", userinformation[0]);
                component.set("v.accountAddress", userinformation[1]);
                component.set("v.accountBPNo",userinformation[2]);
            }
        });
        $A.enqueueAction(action);
    },
    
    validateFormRequired: function(component, event) {
    	var isValid = true;
        var newParty = component.get("v.newParty");
        var openDebtor = component.get("v.createNew");
        var AllRowsList = component.get("v.debtList");
        var partyType = component.find("selectPartyType").get("v.value");
        //alert('party selected: '+partyType);
         if(AllRowsList.length == 0) {
            isValid = false;
            alert('Please add atleast one debtor to submit the request!');
        } else if(openDebtor) {
            isValid = false;
            alert('Please save/cancel debtor info to save the form.');
        } else if(partyType == '') {
            isValid = false;
            alert('Please select type of party from the drop down!');
        } else if(partyType == 'Individual' && (newParty.Last_Name__c == '' || newParty.First_Name__c == '')) {
        	isValid = false;
            alert('First Name and Last Name are required in Party Section!');
        } else if(partyType == 'Organisation' && (newParty.Organisation_Name__c == '' || newParty.Type_of_Organisation__c == '' || newParty.Jurisdiction_of_Organisation__c == '' || newParty.Organisational_ID__c == '')) {
            isValid = false;
            alert('All fields are required when Organisation is selected in Party Section!');
        } else if(newParty.Mailing_Address__c == '') {
            isValid = false;
            alert('Mailing Address is required in Party Section!');
        } else if(newParty.City__c == '') {
            isValid = false;
            alert('City is required in Party Section!');
        } else if(newParty.Country__c == '') {
            isValid = false;
            alert('Country is required in Party Section!');
        }
        return isValid;
    },
    
    validateRequiredSRDocs: function(component, srId) {
    	component.set("v.isValidateOpen",true);
        var action = component.get("c.fetchApprovedForm");
        action.setParams({
            "recId": srId
        });
        action.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS") {
                var apForm = response.getReturnValue();
                console.log(apForm.Documents_Pending__c);
                if(apForm.Documents_Pending__c == true) {
                    component.set("v.documentsUploaded",false);
                    component.set("v.approvedForm.Documents_Pending__c",true);
                } else if(apForm.Documents_Pending__c == false) {
                    component.set("v.documentsUploaded",true);
                    component.set("v.approvedForm.Documents_Pending__c",false);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchSRPricingInfo : function(component, srId) {
        var action = component.get("c.PopulateSR_Pricing");
        action.setParams({
            "recId": srId
        });
        action.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS") {
                var responseWrapper = response.getReturnValue();
                console.log('--wrapper--');
                console.log(responseWrapper.lstSRPriceItems);
                console.log('--wrapper--');
                component.set("v.SRPricingInfo", responseWrapper);
            }
        });
        $A.enqueueAction(action);
    },
    
    loadSR : function(component, srId) {
    	var action = component.get("c.fetchSR");
        action.setParams({
            recId : srId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log(result);
                component.set("v.serviceRequest", result);
                component.set("v.approvedForm", result.Approved_Forms__r[0]);
                component.set("v.debtList", result.Debtors_SecuredParties__r);
            }
        });
        $A.enqueueAction(action);
    },
    
    loadApprovedRequestForm : function(component, srId) {
    	var action = component.get("c.fetchApprovedForm");
        action.setParams({
            recId : srId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.approvedForm", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})