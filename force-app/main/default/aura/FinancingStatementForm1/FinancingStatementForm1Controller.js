({
    doInIt: function(component, event, helper) {
        helper.clientInfo(component,event);
        helper.renderState(component, event);
        if(component.get("v.serviceRequestId") === undefined || component.get("v.serviceRequestId") === null || component.get("v.serviceRequestId") === '') {
            helper.fetchPickListVal(component,'Country__c');
        }
        if(component.get("v.serviceRequestId") !== undefined || component.get("v.serviceRequestId") !== null || component.get("v.serviceRequestId") !== '') {
            helper.loadSR(component,component.get("v.serviceRequestId"));
            helper.loadApprovedRequestForm(component,component.get("v.serviceRequestId"));
            helper.fetchSRPricingInfo(component,component.get("v.serviceRequestId"));
        }
    },
    
    click: function(component, event, helper) {
        component.set("v.isValidateOpen",false);
        var index = parseInt(event.target.closest("[data-index]").dataset.index), 
            value, onselect, steps = component.get("v.steps");
        if(index !== undefined) {
            var formId = component.get("v.approvedFormId");
            var docsUploaded = component.get("v.documentsUploaded");
            if(formId === undefined && component.get("v.serviceRequestId") === null) {
            	alert('Please save Debtor/Secured Party information to proceed further...');
            } else if(formId !== undefined && component.get("v.serviceRequestId") === null && docsUploaded === false && steps[index] == 'Pricing') {
                alert('Please upload mandatory documents in documents section...');
            } else if(component.get("v.serviceRequestId") !== null && (component.get("v.approvedForm").Documents_Pending__c === true) && steps[index] == 'Pricing') {
            	alert('Please upload mandatory documents in documents section...');
            } else if((formId !== undefined || component.get("v.serviceRequestId") !== null) && steps[index] == 'Documents') {
                event.preventDefault();
                value = steps[index];
                component.set("v.currentStep", value);
                onselect = component.getEvent("onselect");
                onselect.setParams({ value: value });
                onselect.fire();
                helper.renderState(component);
            } else if(formId !== undefined && component.get("v.serviceRequestId") === null && docsUploaded === true && steps[index] == 'Pricing') {
                event.preventDefault();
                value = steps[index];
                component.set("v.currentStep", value);
                onselect = component.getEvent("onselect");
                onselect.setParams({ value: value });
                onselect.fire();
                helper.renderState(component);
            } else if(component.get("v.serviceRequestId") !== null && (component.get("v.approvedForm").Documents_Pending__c === false || docsUploaded === true) && steps[index] == 'Pricing') {
                event.preventDefault();
                value = steps[index];
                component.set("v.currentStep", value);
                onselect = component.getEvent("onselect");
                onselect.setParams({ value: value });
                onselect.fire();
                helper.renderState(component);
            } else if(steps[index] == 'Debtor/Secured Party Info') {
                event.preventDefault();
                value = steps[index];
                component.set("v.currentStep", value);
                onselect = component.getEvent("onselect");
                onselect.setParams({ value: value });
                onselect.fire();
                helper.renderState(component);
            }
        }
    },
    
    update: function(component, event, helper) {
        var formId = component.get("v.approvedFormId");
        if(formId !== undefined || component.get("v.serviceRequestId") === null) {
            helper.renderState(component, event);
        } else if(formId === undefined && component.get("v.serviceRequestId") === null) {
            alert('Please save Debtor/Secured Party information to proceed further...');
        }
    },
    
    onChangeParty: function (component, event, helper) {
        var selectedValue = component.find("selectPartyType").get("v.value");
        if(selectedValue == 'Organisation') {
            component.set("v.newParty.First_Name__c","");
            component.set("v.newParty.Last_Name__c","");
            component.set("v.newParty.Middle_Name__c","");
            component.set("v.newParty.Suffix__c","");
        } else if(selectedValue == 'Individual') {
            component.set("v.newParty.Organisation_Name__c","");
            component.set("v.newParty.Type_of_Organisation__c","");
            component.set("v.newParty.Jurisdiction_of_Organisation__c","");
            component.set("v.newParty.Organisational_ID__c","");
        }
        component.set("v.partyType",selectedValue);
    },
    
    handleCountryChange: function(component, event) {
        // This will contain the string of the "value" attribute of the selected option
        //var selectedOptionValue = event.getParam("value");
        var selectedOptionValue = component.find('selectInlineCountry').get('v.value');
        component.set("v.newParty.Country__c",selectedOptionValue);
    },
    
    // Add new debtor
    addNewDebtor: function(component, event, helper) {
    	component.set("v.newDebtor",{'sobjectType':'Debtor_Secured_Party__c', 'Organisation_Name__c':'', 'Last_Name__c':'', 'First_Name__c':'', 'Middle_Name__c':'', 'Suffix__c':'', 'Mailing_Address__c':'', 'City__c':'', 'Country__c':'', 'Type_of_Organisation__c':'', 'Jurisdiction_of_Organisation__c':'', 'Organisational_ID__c':'', 'Service_Request__c':'', 'Type__c':'Debtor'});
        component.set("v.createNew",true);
    },
    
    // function to save record temp to the List
    saveRow: function(component, event, helper) {
        var RowItemList = component.get("v.debtList");
        var newDebt = component.get("v.newDebtor");
        RowItemList.push({newDebt});
		component.set("v.createNew",false);
        var arrayList = JSON.stringify(RowItemList);
        console.log(arrayList);
        arrayList = arrayList.replace('"Debtor"}}','"Debtor"}');
        arrayList = arrayList.replace('{"newDebt":','');
        var debList = JSON.parse(arrayList);
        console.log(debList);
        component.set("v.debtList", debList);
    },
    
    // function for delete the row
    removeDeletedRow: function(component, event, helper) {
        var index = event.getParam("indexVar");
        var AllRowsList = component.get("v.debtList");
        AllRowsList.splice(index, 1);
        component.set("v.debtList", AllRowsList);
    },
    
    // funciton to delete saved Debtor
    deleteEntity: function(component, event, helper) {
    	var listIndex = event.getSource().get("v.value");
        var AllRowsList = component.get("v.debtList");
        AllRowsList.splice(listIndex, 1);
        component.set("v.debtList", AllRowsList);
        component.set("v.createNew",true);
    },

	// function to cancel row
	cancelRow: function(component, event, helper) {
    	component.set("v.createNew",false);
	},

    closeForm: function(component, event, helper) {
        var serviceId = component.get("v.serviceReqId");
        var urlEvent = $A.get("e.force:navigateToURL");
        if(serviceId === null) {
            urlEvent.setParams({
                "url": '/company-services'
            });
        } else {
            urlEvent.setParams({
                "url": '/service-request/'+serviceId
            });
        }
        urlEvent.fire();
    },
                              
    Save: function(component, event, helper) {
        var RowItemList = component.get("v.debtList");
        var partyList = component.get("v.partyList");
        var allList = component.get("v.allList");
        var party = component.get("v.newParty");
        var allDebPartyList = RowItemList.concat(partyList);
        if(helper.validateFormRequired(component, event)) {
            partyList.push({party});
            var arrayList = JSON.stringify(partyList);
            arrayList = arrayList.replace('"Secured Party"}}','"Secured Party"}');
            arrayList = arrayList.replace('{"party":','');
            var finalList = JSON.parse(arrayList);
            console.log(finalList);
			var allDebPartyList = RowItemList.concat(finalList);       
            component.set("v.debtList", RowItemList);
            var action = component.get("c.saveApprovedForm");
            action.setParams({
                "af": component.get("v.approvedForm"),
                "ListDebtor": allDebPartyList,
                "initialFormId": ''
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {
                    var results = response.getReturnValue();
                    component.set("v.serviceReqId", results[0]);
                    component.set("v.approvedFormId", results[1]);
                    if(results[2] == 'true') {
                    	component.set("v.documentsUploaded", false);
                        component.set("v.approvedForm.Documents_Pending__c",true)
                    } else if(results[2] == 'false') {
                    	component.set("v.documentsUploaded", true);
                        component.set("v.approvedForm.Documents_Pending__c",false)
                    }
                    component.set("v.messageType","success");
                    component.set("v.messageVariant","inverse");
                    component.set("v.message", "Service Request generated Successfully!");
                    component.set("v.currentStep","Documents");
                    helper.fetchSRPricingInfo(component,results[0]);
                    var currentStep = "Documents",
                        allSteps = component.get("v.steps"),
                        render = [],
                        state = "slds-is-complete";
                    component.set("v.renderInfo", render);
                    allSteps.forEach(function(step) {
                        if(currentStep === step) {
                            state = "slds-is-current";
                        } else if(state === "slds-is-current") {
                            state = "slds-is-incomplete";
                        }
                        render.push({ label: step, selected: state === "slds-is-current", state: state });
                    });
                    component.set("v.renderInfo", render);
                    window.scrollTo(0, 0);
                } else if(state === "ERROR") {
                    component.set("v.messageType","error");
                    var errors = action.getError();
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            component.set("v.message",errors[0].message);
                            component.set("v.messageVariant","inverse");
                            window.scrollTo(0, 0);
                        }
                    } else {
                        component.set("v.message", "Request Failed!");
                        component.set("v.messageVariant","inverse");
                        window.scrollTo(0, 0);
                    }
                } else if (status === "INCOMPLETE") {
                    component.set("v.messageType","error");
                    component.set("v.messageVariant","inverse");
                    component.set("v.message", "No response from server or client is offline.");
                    window.scrollTo(0, 0);
                }
            });
            $A.enqueueAction(action);
        }
    },

	refreshDocuments: function(component, event, helper) {
        component.set("v.isValidateOpen",false);
        var action = component.get("c.fetchApprovedForm");
        var srId;
        if(component.get("v.serviceRequestId") !== null) {
            srId = component.get("v.serviceRequestId");
        } else if(component.get("v.serviceReqId") !== null) {
            srId = component.get("v.serviceReqId");
        }
        action.setParams({
            "recId": srId
        });
        action.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS") {
                var apForm = response.getReturnValue();
                if(apForm.Documents_Pending__c == true) {
                    component.set("v.documentsUploaded",false);
                    component.set("v.approvedForm.Documents_Pending__c",true);
                } else if(apForm.Documents_Pending__c == false) {
                    component.set("v.documentsUploaded",true);
                    component.set("v.approvedForm.Documents_Pending__c",false);
                }
            }
        });
        $A.enqueueAction(action);
    },

	validateForm: function(component, event, helper) {
        var srId;
        if(component.get("v.serviceRequestId") !== null) {
            srId = component.get("v.serviceRequestId");
        } else if(component.get("v.serviceReqId") !== null) {
            srId = component.get("v.serviceReqId");
        }
        helper.validateRequiredSRDocs(component, srId);
    },

	cancelValidation: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isValidateOpen", false);
    },

	updateForm: function(component, event, helper) {
        var appRecordId;
        if(component.get("v.serviceRequestId") !== null) {
            appRecordId = component.get("v.approvedForm").Id;
        } else if(component.get("v.serviceReqId") !== null) {
            appRecordId = component.get("v.approvedFormId");
        }
        var action = component.get("c.updateRequest");
        action.setParams({
            "recordId": appRecordId,
            "description": "",
            "noOfPages": ""
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                //component.set("v.documentsUploaded",true);
                component.set("v.docsapprovedFormId", response.getReturnValue());
                component.set("v.messageType","success");
                component.set("v.messageVariant","inverse");
                component.set("v.message", "Changes saved successfully!");
                component.set("v.currentStep","Pricing");
                var currentStep = "Pricing",
                    allSteps = component.get("v.steps"),
                    render = [],
                    state = "slds-is-complete";
                component.set("v.renderInfo", render);
                allSteps.forEach(function(step) {
                    if(currentStep === step) {
                        state = "slds-is-current";
                    } else if(state === "slds-is-current") {
                        state = "slds-is-incomplete";
                    }
                    render.push({ label: step, selected: state === "slds-is-current", state: state });
                });
                component.set("v.renderInfo", render);
                window.scrollTo(0, 0);
            } else if(state === "ERROR") {
                component.set("v.messageType","error");
                var errors = action.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        component.set("v.message",errors[0].message);
                        component.set("v.messageVariant","inverse");
                        window.scrollTo(0, 0);
                    }
                } else {
                    component.set("v.message", "Request Failed!");
                    component.set("v.messageVariant","inverse");
                    window.scrollTo(0, 0);
                }
            } else if (status === "INCOMPLETE") {
                component.set("v.messageType","error");
                component.set("v.messageVariant","inverse");
                component.set("v.message", "No response from server or client is offline.");
                window.scrollTo(0, 0);
            }
        });
        $A.enqueueAction(action);
    },
	
	submitForm: function(component, event, helper) {
        var insufficientBalance = component.get("v.SRPricingInfo.insufficientBalance");
        if(!insufficientBalance) {
            var serviceId;
            if(component.get("v.serviceRequestId") !== null) {
                serviceId = component.get("v.serviceRequestId");
            } else if(component.get("v.serviceReqId") !== null) {
                serviceId = component.get("v.serviceReqId");
            }
            var action = component.get("c.submitRequest");
            action.setParams({
                "recordId": serviceId,
                "apId" : "",
                "collateral" : ""//component.get("v.approvedForm.Collateral_Description__c")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {
                    var results = response.getReturnValue();
                    component.set("v.messageType","success");
                    component.set("v.messageVariant","inverse");
                    component.set("v.message", "Your Request submitted Successfully!");
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": '/service-request/'+serviceId
                    });
                    urlEvent.fire();
                } else if(state === "ERROR") {
                    component.set("v.messageType","error");
                    var errors = action.getError();
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            component.set("v.message",errors[0].message);
                            component.set("v.messageVariant","inverse");
                            window.scrollTo(0, 0);
                        }
                    } else {
                        component.set("v.message", "Request Failed!");
                        component.set("v.messageVariant","inverse");
                        window.scrollTo(0, 0);
                    }
                } else if (status === "INCOMPLETE") {
                    component.set("v.messageType","error");
                    component.set("v.messageVariant","inverse");
                    component.set("v.message", "No response from server or client is offline.");
                    window.scrollTo(0, 0);
                }
            });
            $A.enqueueAction(action);
        } else {
            if(confirm("You don\'t have enough balance to Process the Request, Please click Ok to Top-up Balance from home page in the Sidebar.")) {
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": '/servlet/networks/switch?networkId=0DB20000000PBv8&startURL=/apex/home'
                });
                urlEvent.fire();
            }
            //alert('You dont have enough balance to Process the Request, Please click on the Top-up Balance in the Sidebar.');
        }
    },

	showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },
    
    hideSpinner : function(component,event,helper) {   
        component.set("v.Spinner", false);
    }
})