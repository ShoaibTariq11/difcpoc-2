({
	myAction : function(component, event, helper) {
		var action = component.get("c.otpValidation");
		action.setParams({otpData : v.sendOTP});
		action.setCallback(this, function(response) {
			
			var state = response.getState();
			if (state === "SUCCESS") {
                component.set("v.otpValidation", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
		}
		
		$A.enqueueAction(action);
	}
})