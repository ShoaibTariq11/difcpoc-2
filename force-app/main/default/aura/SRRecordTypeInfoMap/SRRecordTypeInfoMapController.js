({
	doInit : function(component, event, helper) {
		var lstRecordTypeInfo = component.get("v.lstRecordTypeInfo");
        var objectpretetss = helper.objPrefix(component,event);
        var objectprefix = component.get("v.objPrefix");
        var listRecordTypeInfo = [];
        var arrayList = JSON.stringify(lstRecordTypeInfo);
        var recordTypeList = JSON.parse(arrayList);
        console.log(recordTypeList);
        for(var i=0; i<recordTypeList.length; i++) {
            var thisRecord = recordTypeList[i];
            if(thisRecord.isFlow==true && thisRecord.strFlowId!=null && thisRecord.MenuTitle != 'Financing Statement (Form 1)' && thisRecord.MenuTitle != 'Financing Statement Addendum (Form 2)' && thisRecord.MenuTitle != 'Financing Statement Additional Party (Form 3)' && thisRecord.MenuTitle != 'Financing Statement Amendment (Form 4)' && thisRecord.MenuTitle != 'Financing Statement Amendment Addendum (Form 5)' && thisRecord.MenuTitle != 'Financing Statement Amendment Additional Party (Form 6)' && thisRecord.MenuTitle != 'Correction Statement (Form 7)' && thisRecord.MenuTitle != 'Information Request (Form 8)') {
                var targetLink = "/servlet/networks/switch?networkId=0DB20000000PBv8&startURL=/apex/Process_Flow?FlowId="+thisRecord.strFlowId+"&Id="+thisRecord.RecordId;
                listRecordTypeInfo.push({
                    label: thisRecord.MenuTitle,
                    name: thisRecord.API_NAME,
                    href: targetLink
                });
            } else if(thisRecord.isFlow==false && thisRecord.strFlowId==null && thisRecord.MenuTitle != 'Financing Statement (Form 1)' && thisRecord.MenuTitle != 'Financing Statement Addendum (Form 2)' && thisRecord.MenuTitle != 'Financing Statement Additional Party (Form 3)' && thisRecord.MenuTitle != 'Financing Statement Amendment (Form 4)' && thisRecord.MenuTitle != 'Financing Statement Amendment Addendum (Form 5)' && thisRecord.MenuTitle != 'Financing Statement Amendment Additional Party (Form 6)' && thisRecord.MenuTitle != 'Correction Statement (Form 7)' && thisRecord.MenuTitle != 'Information Request (Form 8)') {
                var targetLink = "/servlet/networks/switch?networkId=0DB20000000PBv8&startURL=/a1I/e?RecordType="+thisRecord.RecordId+"&type="+thisRecord.API_NAME+"&service_request_title="+thisRecord.PortalServiceTitle;
                listRecordTypeInfo.push({
                    label: thisRecord.MenuTitle,
                    name: thisRecord.API_NAME,
                    href: targetLink
                });
            } else if(thisRecord.MenuTitle == 'Financing Statement (Form 1)') {
                var targetLink = "/s/financing-statement-form";
                listRecordTypeInfo.push({
                    label: thisRecord.MenuTitle,
                    name: thisRecord.API_NAME,
                    href: targetLink
                });
            } else if(thisRecord.MenuTitle == 'Financing Statement Addendum (Form 2)') {
                var targetLink = "/s/financing-statement-addendum";
                listRecordTypeInfo.push({
                    label: thisRecord.MenuTitle,
                    name: thisRecord.API_NAME,
                    href: targetLink
                });
            } else if(thisRecord.MenuTitle == 'Financing Statement Additional Party (Form 3)') {
                var targetLink = "/s/financing-statement-additional-party";
                listRecordTypeInfo.push({
                    label: thisRecord.MenuTitle,
                    name: thisRecord.API_NAME,
                    href: targetLink
                });
            } else if(thisRecord.MenuTitle == 'Financing Statement Amendment (Form 4)') {
                var targetLink = "/s/financing-statement-amendment";
                listRecordTypeInfo.push({
                    label: thisRecord.MenuTitle,
                    name: thisRecord.API_NAME,
                    href: targetLink
                });
            } else if(thisRecord.MenuTitle == 'Financing Statement Amendment Addendum (Form 5)') {
                var targetLink = "/s/financing-statement-amendment-addendum";
            	listRecordTypeInfo.push({
                    label: thisRecord.MenuTitle,
                    name: thisRecord.API_NAME,
                    href: targetLink
                });
            } else if(thisRecord.MenuTitle == 'Financing Statement Amendment Additional Party (Form 6)') {
                var targetLink = "/s/financing-statement-amendment-add-party";
                listRecordTypeInfo.push({
                    label: thisRecord.MenuTitle,
                    name: thisRecord.API_NAME,
                    href: targetLink
                });
            }  else if(thisRecord.MenuTitle == 'Correction Statement (Form 7)') {
                var targetLink = "/s/correction-statement";
                listRecordTypeInfo.push({
                    label: thisRecord.MenuTitle,
                    name: thisRecord.API_NAME,
                    href: targetLink
                });
            }  else if(thisRecord.MenuTitle == 'Information Request (Form 8)') {
                var targetLink = "/s/information-request";
                listRecordTypeInfo.push({
                    label: thisRecord.MenuTitle,
                    name: thisRecord.API_NAME,
                    href: targetLink
                });
            }
        }
        //component.set("v.recordTypeInfoLst",listRecordTypeInfo);
        component.set("v.listMenuItems",listRecordTypeInfo);
	}
})