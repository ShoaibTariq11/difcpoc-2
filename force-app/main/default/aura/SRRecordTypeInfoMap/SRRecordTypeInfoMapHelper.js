({
	objPrefix : function(component,event) {
        var prefixObj = "";
        var action = component.get("c.getObjectPrefix");
        action.setParams({
            objAPI: "Service_Request__c"
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var objPrefix = response.getReturnValue();
                component.set("v.objPrefix", response.getReturnValue());
                prefixObj = response.getReturnValue();
                return response.getReturnValue();
            }
        });
        $A.enqueueAction(action);
	}
})