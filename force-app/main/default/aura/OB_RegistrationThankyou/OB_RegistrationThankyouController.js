({
	init : function(component, event, helper) {
        console.log('==init==');
        let newURL = new URL(window.location.href).searchParams;
        var srID = newURL.get('srId');
         component.set('v.srId',srID);
         var action = component.get('c.getServiceEmailAddress');
        action.setParams({
             "srId": srID
        });
         console.log('==srID===='+srID);  
         action.setCallback(this, function(response){
            var state = response.getState();
            console.log('==state===='+state);
            if (state === "SUCCESS"){
                var respWrap = response.getReturnValue();
                console.log('==respWrap===='+respWrap);
                console.log(respWrap.entityType);
                component.set('v.email',respWrap.emailAddress);
                component.set('v.entityType',respWrap.entityType);
                component.set('v.businessSector',respWrap.businessSector);
                
            }
            
        });
		// enqueue the action
        $A.enqueueAction(action);
	},
    loginRedirect : function(component, event, helper) {
        //var urlParam = '/' +$A.get("$Label.c.OB_Site_Prefix") + '/s/login';
        /*
        urlEvent.setParams({
            "url": '/' +$A.get("$Label.c.OB_Site_Prefix") + '/s/login'
        });
        urlEvent.fire();
        */
        var nextPageURL = '/' +$A.get("$Label.c.OB_Site_Prefix") + '/s/login';
        window.location.href = nextPageURL;
    }
})