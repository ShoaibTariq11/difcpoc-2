(
{
    init: function(component, event, helper) 
    {
        
        // call the apex method 'saveChunk'
        var documentMasterCode = component.get('v.documentMaster');
        var parentId= component.get("v.parentId");
        // method get file name
        var action = component.get("c.getFileName");
        
        action.setParams({
            parentId: parentId,
            documentMasterCode:documentMasterCode
        });
        console.log(parentId+'====getFileName=documentMasterCode='+documentMasterCode);
        // set call back
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            var result = response.getReturnValue();
            console.log('====getFileName=result='+result);
            console.log(parentId+'====getFileName=documentMasterCode='+documentMasterCode);
            if (state === "SUCCESS")
            {
                if(!$A.util.isUndefined(result) && 
                   !$A.util.isEmpty(result)){
                    component.set("v.fileName", result);
                }
                //document.getElementById("spinner").className +=" difc-display-none";
            }else{
            	//document.getElementById("spinner").className +=" difc-display-none";
                console.log('===error==');
            }
        });
        
        // enqueue the action
        $A.enqueueAction(action);
    },
    handleFilesChange: function(component, event, helper) 
    {
        component.set('v.spinner',true);
        //var element = document.getElementById("spinner");
        //element.classList.remove("difc-display-none");
        
        var fileName = 'No File Selected..';
        
        if (event.getSource().get("v.files").length > 0) 
        {
            fileName = event.getSource().get("v.files")[0]['name'];
        }
        
        component.set("v.fileName", fileName);
        
        if(component.find("fileId").get("v.files").length > 0) 
        {
            helper.uploadHelper(component, event);
            
        } 
        else 
        { 
        	//document.getElementById("spinner").className +=" difc-display-none";
            component.set('v.spinner',false);
            alert('Please Select a Valid File');
        }
        
    },
    
    doSave: function(component, event, helper) 
    {
        console.log('inside Save');
        if (component.find("fileId").get("v.files").length > 0) {
            helper.uploadHelper(component, event);
        } else {
            alert('Please Select a Valid File');
        }
    }
   
}
)