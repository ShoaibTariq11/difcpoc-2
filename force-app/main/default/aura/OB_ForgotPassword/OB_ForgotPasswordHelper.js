({
	validateUsername :  function(component){
         var validRecord = true;
        
        // Username must not be blank
        var usernameField = component.find('Username');
        var uName = usernameField.get('v.value');
        if ($A.util.isEmpty(uName)){
            validRecord = false;
            usernameField.set('v.errors', [{message:'Please enter username.'}]);       
        }
        else {
            usernameField.set('v.errors', null);
        }
		 return validRecord;
	}
    
})