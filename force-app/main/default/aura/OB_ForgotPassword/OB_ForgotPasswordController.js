({
    getUsername : function(component, event, helper) {
        
        var Reuser = component.get("v.Username");
        var action = component.get("c.forgotPassowrd");
        if(helper.validateUsername(component))
        {
        action.setParams({
            username: Reuser
        });
        action.setCallback(this,function (response) {
            var rtnValue = response.getReturnValue();
            var state = response.getState();
            console.log(state);
             var staticLabel = $A.get("$Label.c.OB_Site_Prefix");
            // component.set("v.mylabel1",'We’ve sent you an email with a link to finish resetting your password.');
            if (rtnValue !==null &&  state === "SUCCESS") {
                var nextPageURL = '/'+staticLabel+'/s/ob-forgotpasswordconfirm';
               
               window.location.href = nextPageURL;
             // helper.gotoURL(component,event);
            }
             else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        }
             
        $A.enqueueAction(action);
        
        
    },
    redirectCancel : function(component, event, helper) {
         var staticLabel = $A.get("$Label.c.OB_Site_Prefix");
        var nextPageURL = '/'+staticLabel+'/s/login';
        window.location.href = nextPageURL;
    }
    
})