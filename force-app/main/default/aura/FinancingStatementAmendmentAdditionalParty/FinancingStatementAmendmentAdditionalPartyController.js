({
	doInit : function(component, event, helper) {
		helper.renderState(component, event);
        helper.fetchCustomerId(component, event);
        if(component.get("v.serviceRequestId") !== undefined || component.get("v.serviceRequestId") !== null || component.get("v.serviceRequestId") !== '') {
            helper.loadSR(component,component.get("v.serviceRequestId"));
            helper.loadApprovedRequestForm(component,component.get("v.serviceRequestId"));
            helper.fetchSRPricingInfo(component,component.get("v.serviceRequestId"));
        }
	},
    
    click: function(component, event, helper) {
        component.set("v.isValidateOpen",false);
        var index = parseInt(event.target.closest("[data-index]").dataset.index), 
            value, onselect, steps = component.get("v.steps");
        if(index !== undefined) {
            var formId = component.get("v.approvedFormId");
            var docsUploaded = component.get("v.documentsUploaded");
            if(formId === undefined && component.get("v.serviceRequestId") === null) {
            	alert('Please save Additional Party information to proceed further...');
            } else if(formId !== undefined && component.get("v.serviceRequestId") === null && docsUploaded === false && steps[index] == 'Pricing') {
                alert('Please upload mandatory documents in documents section...');
            } else if(component.get("v.serviceRequestId") !== null && (component.get("v.approvedForm").Documents_Pending__c === true) && steps[index] == 'Pricing') {
            	alert('Please upload mandatory documents in documents section...');
            } else if((formId !== undefined || component.get("v.serviceRequestId") !== null) && steps[index] == 'Documents') {
                event.preventDefault();
                value = steps[index];
                component.set("v.currentStep", value);
                onselect = component.getEvent("onselect");
                onselect.setParams({ value: value });
                onselect.fire();
                helper.renderState(component);
            } else if(formId !== undefined && component.get("v.serviceRequestId") === null && docsUploaded === true && steps[index] == 'Pricing') {
                event.preventDefault();
                value = steps[index];
                component.set("v.currentStep", value);
                onselect = component.getEvent("onselect");
                onselect.setParams({ value: value });
                onselect.fire();
                helper.renderState(component);
            } else if(component.get("v.serviceRequestId") !== null && (component.get("v.approvedForm").Documents_Pending__c === false || docsUploaded === true) && steps[index] == 'Pricing') {
                event.preventDefault();
                value = steps[index];
                component.set("v.currentStep", value);
                onselect = component.getEvent("onselect");
                onselect.setParams({ value: value });
                onselect.fire();
                helper.renderState(component);
            } else if(steps[index] == 'Additional Party') {
                event.preventDefault();
                value = steps[index];
                component.set("v.currentStep", value);
                onselect = component.getEvent("onselect");
                onselect.setParams({ value: value });
                onselect.fire();
                helper.renderState(component);
            }
        }
    },
    
    update: function(component, event, helper) {
        var formId = component.get("v.approvedFormId");
        if(formId !== undefined) {
            helper.renderState(component, event);
        } else if(formId === undefined && component.get("v.serviceRequestId") === null) {
            alert('Please save Additional Party information to proceed further...');
        }
    },
    
    // Add new debtor
    addNewDebtor: function(component, event, helper) {
        component.set("v.newDebtor",{'sobjectType':'Debtor_Secured_Party__c', 'Organisation_Name__c':'', 'Last_Name__c':'', 'First_Name__c':'', 'Middle_Name__c':'', 'Suffix__c':'', 'Mailing_Address__c':'', 'City__c':'', 'Country__c':'', 'Additional_Info_Re_Org_Debtor__c':'', 'Type_of_Organisation__c':'', 'Jurisdiction_of_Organisation__c':'', 'Organisational_ID__c':'', 'Service_Request__c':component.get("v.serviceReqId"), 'Type__c':'Debtor'});
        component.set("v.createNewDebtor",true);
    },
    
    //Add new party
    addNewParty: function(component, event, helper) {
    	component.set("v.newParty",{'sobjectType':'Debtor_Secured_Party__c', 'Organisation_Name__c':'', 'Last_Name__c':'', 'First_Name__c':'', 'Middle_Name__c':'', 'Suffix__c':'', 'Mailing_Address__c':'', 'City__c':'', 'Country__c':'', 'Additional_Info_Re_Org_Debtor__c':'', 'Type_of_Organisation__c':'', 'Jurisdiction_of_Organisation__c':'', 'Organisational_ID__c':'', 'Service_Request__c':component.get("v.serviceReqId"), 'Type__c':'Secured Party'});
        component.set("v.createNewParty",true);
    },
    
    // function to save new debtor temporarily to the List
    saveDebtorRow: function(component, event, helper) {
        var isValid = true;
        var newDebtor = component.get("v.newDebtor");
        var openDebtor = component.get("v.createNewDebtor");
        if(openDebtor && newDebtor.Organisation_Name__c == '' && newDebtor.Last_Name__c == '' && newDebtor.First_Name__c == '') {
        	isValid = false;
            alert('Organisation Name/Individual\'s name is required in Debtor Section!');
        } else if(openDebtor && newDebtor.Mailing_Address__c == '') {
            isValid = false;
            alert('Mailing Address should not be empty in Debtor Section!');
        }
        if(isValid) {
            var newDebtorList = component.get("v.newDebtList");
            var debtor = component.get("v.newDebtor");
            newDebtorList.push({debtor});
            var arrayList = JSON.stringify(newDebtorList);
            arrayList = arrayList.replace('"Debtor"}}','"Debtor"}');
            arrayList = arrayList.replace('{"debtor":','');
            var finalDebtList = JSON.parse(arrayList);
            component.set("v.newDebtList",finalDebtList);
			component.set("v.createNewDebtor",false);
		}
    },
    
    // function to save new debtor temporarily to the List and add new
    saveAndAddNewDebtorRow: function(component, event, helper) {
        var isValid = true;
        var newDebtor = component.get("v.newDebtor");
        var openDebtor = component.get("v.createNewDebtor");
        if(openDebtor && newDebtor.Organisation_Name__c == '' && newDebtor.Last_Name__c == '' && newDebtor.First_Name__c == '') {
        	isValid = false;
            alert('Organisation Name/Individual\'s name is required in Debtor Section!');
        } else if(openDebtor && newDebtor.Mailing_Address__c == '') {
            isValid = false;
            alert('Mailing Address should not be empty in Debtor Section!');
        }
        if(isValid) {
            var newDebtorList = component.get("v.newDebtList");
            var debtor = component.get("v.newDebtor");
            newDebtorList.push({debtor});
            var arrayList = JSON.stringify(newDebtorList);
            arrayList = arrayList.replace('"Debtor"}}','"Debtor"}');
            arrayList = arrayList.replace('{"debtor":','');
            var finalDebtList = JSON.parse(arrayList);
            component.set("v.newDebtList",finalDebtList);
			component.set("v.createNewDebtor",true);
            component.set("v.newDebtor",{'sobjectType':'Debtor_Secured_Party__c', 'Organisation_Name__c':'', 'Last_Name__c':'', 'First_Name__c':'', 'Middle_Name__c':'', 'Suffix__c':'', 'Mailing_Address__c':'', 'City__c':'', 'Country__c':'', 'Additional_Info_Re_Org_Debtor__c':'', 'Type_of_Organisation__c':'', 'Jurisdiction_of_Organisation__c':'', 'Organisational_ID__c':'', 'Service_Request__c':component.get("v.serviceReqId"), 'Type__c':'Debtor'});
        }
    },

	// function to save new debtor temporarily to the List
    savePartyRow: function(component, event, helper) {
        var isValid = true;
        var newParty = component.get("v.newParty");
        var openParty = component.get("v.createNewParty");
        if(openParty && newParty.Organisation_Name__c == '' && newParty.Last_Name__c == '' && newParty.First_Name__c == '') {
        	isValid = false;
            alert('Organisation Name/Individual\'s name is required in Party Section!');
        } else if(openParty && newParty.Mailing_Address__c == '') {
            isValid = false;
            alert('Mailing Address should not be empty in Party Section!');
        }
        if(isValid) {
            component.set("v.createNewParty",false);
            var newPartyList = component.get("v.newPartyList");
            var party = component.get("v.newParty");
            newPartyList.push({party});
            var arrayList = JSON.stringify(newPartyList);
            arrayList = arrayList.replace('"Secured Party"}}','"Secured Party"}');
            arrayList = arrayList.replace('{"party":','');
            var finalPartyList = JSON.parse(arrayList);
            component.set("v.newPartyList",finalPartyList);
		}
    },
    
    // function to save new debtor temporarily to the List and add new
    saveAndAddNewPartyRow: function(component, event, helper) {
        var isValid = true;
        var newParty = component.get("v.newParty");
        var openParty = component.get("v.createNewParty");
        if(openParty && newParty.Organisation_Name__c == '' && newParty.Last_Name__c == '' && newParty.First_Name__c == '') {
        	isValid = false;
            alert('Organisation Name/Individual\'s name is required in Party Section!');
        } else if(openParty && newParty.Mailing_Address__c == '') {
            isValid = false;
            alert('Mailing Address should not be empty in Party Section!');
        }
        if(isValid) {
            component.set("v.createNewParty",true);
            var newPartyList = component.get("v.newPartyList");
            var party = component.get("v.newParty");
            newPartyList.push({party});
            var arrayList = JSON.stringify(newPartyList);
            arrayList = arrayList.replace('"Secured Party"}}','"Secured Party"}');
            arrayList = arrayList.replace('{"party":','');
            var finalPartyList = JSON.parse(arrayList);
            component.set("v.newPartyList",finalPartyList);
			component.set("v.newParty",{'sobjectType':'Debtor_Secured_Party__c', 'Organisation_Name__c':'', 'Last_Name__c':'', 'First_Name__c':'', 'Middle_Name__c':'', 'Suffix__c':'', 'Mailing_Address__c':'', 'City__c':'', 'Country__c':'', 'Additional_Info_Re_Org_Debtor__c':'', 'Type_of_Organisation__c':'', 'Jurisdiction_of_Organisation__c':'', 'Organisational_ID__c':'', 'Service_Request__c':component.get("v.serviceReqId"), 'Type__c':'Secured Party'});
        }
    },

	// funciton to delete saved Debtor
    deleteDebtorEntity: function(component, event, helper) {
    	var listIndex = event.getSource().get("v.value");
        var AllRowsList = component.get("v.newDebtList");
        AllRowsList.splice(listIndex, 1);
        component.set("v.newDebtList", AllRowsList);
    },
                               
	// funciton to delete saved Party
    deletePartytEntity: function(component, event, helper) {
    	var listIndex = event.getSource().get("v.value");
        var AllRowsList = component.get("v.newPartyList");
        AllRowsList.splice(listIndex, 1);
        component.set("v.newPartyList", AllRowsList);
    },

	// function to cancel debtor row
	cancelDebtorRow: function(component, event, helper) {
    	component.set("v.createNewDebtor",false);
	},
                              
	// function to cancel party row
	cancelPartyRow: function(component, event, helper) {
    	component.set("v.createNewParty",false);
	},

	Save: function(component, event, helper) {
        if(helper.validateFormRequired(component, event)) {
        	var debtorList = component.get("v.newDebtList");
            var partyList = component.get("v.newPartyList");
            var allDebPartyList = debtorList.concat(partyList);
            console.log(allDebPartyList);
			var appForm = component.get("v.approvedForm");
			var formFilled = JSON.stringify(appForm);
			var parsedForm = JSON.parse(formFilled);
			console.log(parsedForm);
            var action = component.get("c.saveApprovedForm");
            action.setParams({
                af : parsedForm,
                ListDebtor : allDebPartyList,
				initialFormId : component.get("v.initialFinanceStatementId")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {
                    var results = response.getReturnValue();
                    component.set("v.serviceReqId", results[0]);
                    component.set("v.approvedFormId", results[1]);
                    if(results[2] == 'true') {
                    	component.set("v.documentsUploaded", false);
                        component.set("v.approvedForm.Documents_Pending__c",true)
                    } else if(results[2] == 'false') {
                    	component.set("v.documentsUploaded", true);
                        component.set("v.approvedForm.Documents_Pending__c",false)
                    }
                    component.set("v.approvedForm.Initial_Financial_Statement_No__c",results[4]);
                    component.set("v.messageType","success");
                    component.set("v.messageVariant","inverse");
                    component.set("v.message", "Service Request generated Successfully!");
                    component.set("v.currentStep","Documents");
                    helper.fetchSRPricingInfo(component,results[0]);
                    var currentStep = "Documents",
                        allSteps = component.get("v.steps"),
                        render = [],
                        state = "slds-is-complete";
                    component.set("v.renderInfo", render);
                    allSteps.forEach(function(step) {
                        if(currentStep === step) {
                            state = "slds-is-current";
                        } else if(state === "slds-is-current") {
                            state = "slds-is-incomplete";
                        }
                        render.push({ label: step, selected: state === "slds-is-current", state: state });
                    });
                    component.set("v.renderInfo", render);
                    window.scrollTo(0, 0);
                } else if(state === "ERROR") {
                    component.set("v.messageType","error");
                    var errors = action.getError();
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            component.set("v.message",errors[0].message);
                            component.set("v.messageVariant","inverse");
                            window.scrollTo(0, 0);
                        }
                    } else {
                        component.set("v.message", "Request Failed!");
                        component.set("v.messageVariant","inverse");
                        window.scrollTo(0, 0);
                    }
                } else if (status === "INCOMPLETE") {
                    component.set("v.messageType","error");
                    component.set("v.messageVariant","inverse");
                    component.set("v.message", "No response from server or client is offline.");
                    window.scrollTo(0, 0);
                }
            });
            $A.enqueueAction(action);
		}
    },
                               
	refreshDocuments: function(component, event, helper) {
        component.set("v.isValidateOpen",false);
        var action = component.get("c.fetchApprovedForm");
        var srId;
        if(component.get("v.serviceRequestId") !== null) {
            srId = component.get("v.serviceRequestId");
        } else if(component.get("v.serviceReqId") !== null) {
            srId = component.get("v.serviceReqId");
        }
        action.setParams({
            "recId": srId
        });
        action.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS") {
                var apForm = response.getReturnValue();
                if(apForm.Documents_Pending__c == true) {
                    component.set("v.documentsUploaded",false);
                    component.set("v.approvedForm.Documents_Pending__c",true);
                } else if(apForm.Documents_Pending__c == false) {
                    component.set("v.documentsUploaded",true);
                    component.set("v.approvedForm.Documents_Pending__c",false);
                }
            }
        });
        $A.enqueueAction(action);
    },

	validateForm: function(component, event, helper) {
        var srId;
        if(component.get("v.serviceRequestId") !== null) {
            srId = component.get("v.serviceRequestId");
        } else if(component.get("v.serviceReqId") !== null) {
            srId = component.get("v.serviceReqId");
        }
        helper.validateRequiredSRDocs(component, component.get("v.serviceReqId"));
    },

	cancelValidation: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isValidateOpen", false);
    },
    
    updateForm: function(component, event, helper) {
        var appRecordId;
        if(component.get("v.serviceRequestId") !== null) {
            appRecordId = component.get("v.approvedForm").Id;
        } else if(component.get("v.serviceReqId") !== null) {
            appRecordId = component.get("v.approvedFormId");
        }
        var action = component.get("c.updateRequest");
        action.setParams({
            "recordId": appRecordId,
            "description": "",
            "noOfPages": ""
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                //component.set("v.documentsUploaded",true);
                component.set("v.docsapprovedFormId", response.getReturnValue());
                component.set("v.messageType","success");
                component.set("v.messageVariant","inverse");
                component.set("v.message", "Changes saved successfully!");
                component.set("v.currentStep","Pricing");
                var currentStep = "Pricing",
                    allSteps = component.get("v.steps"),
                    render = [],
                    state = "slds-is-complete";
                component.set("v.renderInfo", render);
                allSteps.forEach(function(step) {
                    if(currentStep === step) {
                        state = "slds-is-current";
                    } else if(state === "slds-is-current") {
                        state = "slds-is-incomplete";
                    }
                    render.push({ label: step, selected: state === "slds-is-current", state: state });
                });
                component.set("v.renderInfo", render);
                window.scrollTo(0, 0);
            } else if(state === "ERROR") {
                component.set("v.messageType","error");
                var errors = action.getError();
                if(errors) {
                    if(errors[0] && errors[0].message) {
                        component.set("v.message",errors[0].message);
                        component.set("v.messageVariant","inverse");
                        window.scrollTo(0, 0);
                    }
                } else {
                    component.set("v.message", "Request Failed!");
                    component.set("v.messageVariant","inverse");
                    window.scrollTo(0, 0);
                }
            } else if (status === "INCOMPLETE") {
                component.set("v.messageType","error");
                component.set("v.messageVariant","inverse");
                component.set("v.message", "No response from server or client is offline.");
                window.scrollTo(0, 0);
            }
        });
        $A.enqueueAction(action);
    },
	
	submitForm: function(component, event, helper) {
        var insufficientBalance = component.get("v.SRPricingInfo.insufficientBalance");
        if(!insufficientBalance) {
            var serviceId;
            if(component.get("v.serviceRequestId") !== null) {
                serviceId = component.get("v.serviceRequestId");
            } else if(component.get("v.serviceReqId") !== null) {
                serviceId = component.get("v.serviceReqId");
            }
            var action = component.get("c.submitRequest");
            action.setParams({
                "recordId": serviceId,
                "apId" : "",
                "collateral" : ""//component.get("v.approvedForm.Collateral_Description__c")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {
                    var results = response.getReturnValue();
                    component.set("v.messageType","success");
                    component.set("v.messageVariant","inverse");
                    component.set("v.message", "Your Request submitted Successfully!");
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": '/service-request/'+serviceId
                    });
                    urlEvent.fire();
                } else if(state === "ERROR") {
                    component.set("v.messageType","error");
                    var errors = action.getError();
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            component.set("v.message",errors[0].message);
                            component.set("v.messageVariant","inverse");
                            window.scrollTo(0, 0);
                        }
                    } else {
                        component.set("v.message", "Request Failed!");
                        component.set("v.messageVariant","inverse");
                        window.scrollTo(0, 0);
                    }
                } else if (status === "INCOMPLETE") {
                    component.set("v.messageType","error");
                    component.set("v.messageVariant","inverse");
                    component.set("v.message", "No response from server or client is offline.");
                    window.scrollTo(0, 0);
                }
            });
            $A.enqueueAction(action);
        } else {
            alert('You dont have enough balance to Process the Request, Please click on the Top-up Balance in the Sidebar.');
        }
    },

	closeForm: function(component, event, helper) {
        var serviceId = component.get("v.serviceReqId");
        var urlEvent = $A.get("e.force:navigateToURL");
        if(serviceId === null) {
            urlEvent.setParams({
                "url": '/company-services'
            });
        } else {
            urlEvent.setParams({
                "url": '/detail/'+serviceId
            });
        }
        urlEvent.fire();
    },

	showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },
    
    hideSpinner : function(component,event,helper) {   
        component.set("v.Spinner", false);
    }
})