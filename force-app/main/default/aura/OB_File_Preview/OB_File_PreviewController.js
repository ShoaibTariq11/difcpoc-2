({
	doInit : function(component, event, helper) { 
        let newURL = new URL(window.location.href).searchParams;
        component.set('v.contentDocId', newURL.get('id'));
    }
})