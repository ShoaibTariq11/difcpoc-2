({
    doInit: function(component, event, helper) { 
        
        helper.fetchPickListVal(component, 'Country_Of_Registration__c', 'accIndustryFirstName');
        helper.fetchPickListVal(component, 'Check_Identical_to_existing__c', 'checkidenticaltoExisting');

        var urlString = window.location.href;
        console.log('init>>');
        helper.loadDynamicComponents(component,event,helper);
        helper.loadTransactionalStageCompNameValues(component,event,helper);
        helper.getEntityDetailsLoadEndsWith(component,event,helper);
        
    },
    
    expandAndCollapse : function(component, event, helper) {
        console.log('inside controller');
        helper.expandAndCollapseAccordian(component, event, helper);
        helper.updateProgressIndicatorOnExpand(component,event,helper);
    },
    
    createDynamicLightningComp: function(component, event, helper) {
        helper.createDynamicLightningCompHelper(component, event, helper);
    },
    
    navigateToSearch: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        var nameToBeSent = component.get("v.CompanyName.Entity_Name__c");
        var nameId= component.get("v.CompanyName.Id");
         let newURL = new URL(window.location.href).searchParams;
        var srId = newURL.get('srId');
        var pageId = newURL.get('pageId');
        var flowId = newURL.get('flowId');
        urlEvent.setParams({
                "url": "/ob-namecheck/?valueTextEntity= " +nameToBeSent+"&NameId="+nameId+"&srId="+srId+"&pageId="+pageId+"&flowId="+flowId
        });
        urlEvent.fire();
    },
    
    navigateToTradingSearch: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        var nameToBeSent = component.get("v.CompanyName.Trading_Name__c");
        var nameId= component.get("v.CompanyName.Id");
 		let newURL = new URL(window.location.href).searchParams;
        var srId = newURL.get('srId');        
        urlEvent.setParams({
            "url": "/ob-tradingnamecheck/?valueTextEntity= " +nameToBeSent+"&NameId="+nameId+"&srId="+srId +"&pageId="+newURL.get('pageId')+"&flowId="+newURL.get('flowId')
        });
        urlEvent.fire();
    },
    
    onSimilarChangeNameshowUpload: function(component, event, helper) {
       console.log("after is is "+component.get("v.TransactinalStageSR.Check_Identical_to_existing__c"));
        console.log("inside onSimilarChangeName DIFC_NameCheckblock");
        var selectedValue = event.getSource().get("v.value");
        console.log("selectedValue for showUploadIdenticalName is "+selectedValue);
        if(selectedValue=='Yes'){
            component.set("v.showUploadIdenticalName","true");
        }
        if(selectedValue=='No'){
            component.set("v.showUploadIdenticalName","false");
        }        
    },
    
    FallChangeName: function(component, event, helper) {
        var selectedValue = event.getSource().get("v.value");
        console.log("selected FallChangeName value of identical is "+selectedValue);
        if(selectedValue=='Yes'){
            component.set("v.showFallFieldsName","true");
        }
        if(selectedValue=='No'){
            component.set("v.showFallFieldsName","false");
            helper.clearSRValues(component, event, helper);
           
        }
    },
    
    DeleteFirstName: function(component, event, helper) {        
        var action = component.get("c.DeleteCompanyName");
        var accessDynamicCompNum = document.getElementsByClassName("dynamicComp");
        var lengthaccessDynamicCompNum = accessDynamicCompNum.length;
        
        if(component.get("v.CompanyName.Entity_Name__c")  || component.get("v.CompanyName.Trading_Name__c") || component.get("v.CompanyName.Ends_With__c")){
            if(lengthaccessDynamicCompNum!=1){
                console.log("inside lengthaccessDynamicCompNum!=1 namecheckblock");
                var CompanyNameId = component.get("v.CompanyName.Id");
                console.log("id to be deleted "+CompanyNameId);
                action.setParams({
                    CompanyNameId:CompanyNameId
                })
                action.setCallback(this,function(response){
                    var state=response.getState();
                    console.log("state is "+state);
                    var retValue= response.getReturnValue();
                    console.log("retvalue DeleteFirstName is "+retValue);
                    
                    if(state=="SUCCESS" && retValue.includes('SUCCESS')){
                        component.find("firstNameCompDestroy").destroy();
                         var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "message": "The Name is deleted"
                        });
                        toastEvent.fire(); 
                    }
                    
                    else if(retValue.includes('ENTITY_IS_LOCKED')){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": "The Name is be submitted for approval and hence cannot be deleted",
                            "type":"error"
                        });
                        toastEvent.fire(); 
                    }
                        else{
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Error!",
                                "message": "There seems to be an issue in deleting the names. Please try again later",
                                "type":"error"
                            });
                            toastEvent.fire();  
                        }
                }) 
                $A.enqueueAction(action);
            }
            else if(lengthaccessDynamicCompNum==1){
                console.log("inside lengthaccessDynamicCompNum==1 namecheckblock");
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Alert!",
                    "message": "You must atleast add one name to proceed"
                });
                toastEvent.fire();
            }
        }
        else{
            // delete the block if the Entity name, trading name & ends with value is empty without deleting the backend
            console.log("inside else DeleteFirstName namecheckblock");
            if(lengthaccessDynamicCompNum!=1){
                component.find("firstNameCompDestroy").destroy(); 
            }
            
            else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Alert!",
                    "message": "You must atleast add one name to proceed"
                });
                toastEvent.fire();
                
            }
        }
    },
    
    navigateToCheckoutAndPay: function(component, event, helper) {
        console.log("inside navigateToCheckoutAndPay");
      //  helper.expandAndCollapseAccordian(component, event, helper);
        var allValid = component.find('requiredNameCheck').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && !inputCmp.get('v.validity').valueMissing;
        }, true);
        console.log("inside navigateToCheckoutAndPay 2");
        if (allValid) {
            console.log("inside if allValid navigateToCheckoutAndPay");
            var amount;
            var accessDynamicCompNum = document.getElementsByClassName("dynamicComp");
            var lengthaccessDynamicCompNum = accessDynamicCompNum.length;
            
            if(lengthaccessDynamicCompNum==1){
                amount="200";
            }
            
            if(lengthaccessDynamicCompNum==2){
                amount="400"; 
            }
            
            if(lengthaccessDynamicCompNum==3){
                amount="600";
            }
            console.log("lengthaccessDynamicCompNum is "+lengthaccessDynamicCompNum);
            console.log("amount is "+amount);
            let newURL = new URL(window.location.href).searchParams;
             var srId = newURL.get('srId');
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/difc-checkouttopupbalance?category=nameReserve&amountToPay="+ amount+"&srId="+srId
            });
            urlEvent.fire();
            

            
        } else {

            console.log("inside else checkMandatoryFieldsandClose");
            alert('Please fill the mandatory fields');
        }
    },
    
    submit : function(component, event, helper){
        console.log("inside commitNameRecord");
        var allValid = component.find('requiredNameCheck').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && !inputCmp.get('v.validity').valueMissing;
        }, true);
        console.log("inside commitNameRecord 2");
        
        if (allValid) {
            console.log("inside if allValid commitNameRecord");
            helper.commitNameRecordHelper(component,event,helper);
            
            var cmpTar= document.getElementById('nameCheckCard');
            cmpTar.classList.add('theme_change');
            
        } else {
            var cmpTar= document.getElementById('nameCheckCard');
            cmpTar.classList.remove('theme_change');
            
            console.log("inside else allValid commitNameRecord");
            alert('Please fill the mandatory fields');
        } 
    },
    
    indicatorOnFocus : function(component, event, helper)	{
        var currentElement = event.getSource().get('v.name');
        var currentIndex= currentElement[currentElement.length -1];
        component.set("v.currentFieldRank",currentIndex);
        
        //console.log("before is "+component.get("v.TransactinalStageSR.Check_Identical_to_existing__c"));
        var selectedValue = event.getSource().get("v.value");
        //console.log("selectedValue for showUploadIdenticalName onfocus is "+selectedValue);
        
        var OldREgDate= component.get("v.TransactinalStageSR.Date_of_Registration__c")
        component.set("v.oldDateOfRegistration",OldREgDate);
        
        //console.log("currentElement@@@"+currentElement);
        var currentli = document.getElementById(currentElement);
        //console.log("currentli"+currentli);
        var currentImgSpan = document.getElementById(currentElement+"-span");
        //console.log("currentImgSpan"+currentImgSpan);
        if(currentli.classList.contains('slds-is-complete')){
            currentli.classList.remove('slds-is-complete');
            currentImgSpan.classList.add('hideSpan');
        }
        currentli.classList.add('slds-is-active');
    },
    
    indicatorOnBlur : function(component,event,helper){
        var currentElement = event.getSource().get('v.name');
        var currentval = event.getSource().get('v.value');
        var currentli = document.getElementById(currentElement);
        currentli.classList.remove('slds-is-active');
        var currentImgSpan = document.getElementById(currentElement+"-span");
        var sectionActive = document.getElementById("NCsectionActive");
        var sectionComplete = document.getElementById("NCsectionComplete");
        
        if(currentval == ""){
            currentli.classList.remove('slds-is-complete');
            currentImgSpan.classList.add('hideSpan');  
            sectionComplete.classList.add('showHide');
            sectionActive.classList.remove('showHide');
            
        }else{
            currentli.classList.add('slds-is-complete');
            currentImgSpan.classList.remove('hideSpan');
        }
        
        /*if(component.get(v.CompanyName.Entity_Name__c) != "" && component.get(v.CompanyName.Ends_With__c) != "" 
           && component.get(v.CompanyName.Check_Identical_to_existing__c) != "" && component.get(v.CompanyName.Fall_within_existing_entities__c) != "" 
           && component.get(v.CompanyName.Parent_Entity_Name__c) != "" && component.get(v.CompanyName.Former_Name__c) != "" 
           && component.get(v.CompanyName.Date_Of_Registration__c) != "" && component.get(v.CompanyName.Place_of_Registration) != "" 
           && component.get(v.CompanyName.Country_Of_Registration__c) != "" && component.get(v.CompanyName.Registered_No__c) != "" 
           && component.get(v.CompanyName.Address_Line_1__c) != "" && component.get(v.CompanyName.Address_Line_1__c) != "" 
           && component.get(v.CompanyName.CompanyName.Address_Line_2__c) != ""){*/
        
        if(component.get("v.CompanyName.Entity_Name__c") != "" && component.get("v.CompanyName.Ends_With__c") != ""){
            console.log('conditions passed');
            var sectionActive = document.getElementById("NCsectionActive");
            var sectionComplete = document.getElementById("NCsectionComplete");
            sectionActive.classList.add('showHide');
            sectionComplete.classList.remove('showHide');
        }
    },
    
    indicatorOnBlurTrnasactionalStage:function(component,event,helper){
        
        //console.log("after indicatorOnBlurTrnasactionalStage is is "+component.get("v.TransactinalStageSR.Check_Identical_to_existing__c"));
        console.log('in indicatorOnBlurTrnasactionalStage>');
        var selectedValue = event.getSource().get("v.value");
        var selectedName =event.getSource().get("v.name");
        console.log('selectedName'+selectedName);
        console.log("selectedValue for showUploadIdenticalName indicatorOnBlurTrnasactionalStage is "+selectedValue);
        
        if(selectedName=='namecheck-element-25'){
            if(selectedValue=='Yes'){
                component.set("v.showUploadIdenticalName","true");
            }
            if(selectedValue=='No'){
                component.set("v.showUploadIdenticalName","false");
            } 
        }
        
        if(selectedName=='namecheck-element-5'){
            if(selectedValue=='Yes'){
                component.set("v.showFallFieldsName","true");
            }
            if(selectedValue=='No'){
                component.set("v.showFallFieldsName","false");
            } 
        }
        
        var currentElement = event.getSource().get('v.name');
        var currentval = event.getSource().get('v.value');
        var currentli = document.getElementById(currentElement);
        currentli.classList.remove('slds-is-active');
        var currentImgSpan = document.getElementById(currentElement+"-span");
        var sectionActive = document.getElementById("NCsectionActive");
        var sectionComplete = document.getElementById("NCsectionComplete");
        
        if(currentval == ""){
            currentli.classList.remove('slds-is-complete');
            currentImgSpan.classList.add('hideSpan');  
            sectionComplete.classList.add('showHide');
            sectionActive.classList.remove('showHide');
            
        }else{
            currentli.classList.add('slds-is-complete');
            currentImgSpan.classList.remove('hideSpan');
        }
        
        /*if(component.get(v.CompanyName.Entity_Name__c) != "" && component.get(v.CompanyName.Ends_With__c) != "" 
           && component.get(v.CompanyName.Check_Identical_to_existing__c) != "" && component.get(v.CompanyName.Fall_within_existing_entities__c) != "" 
           && component.get(v.CompanyName.Parent_Entity_Name__c) != "" && component.get(v.CompanyName.Former_Name__c) != "" 
           && component.get(v.CompanyName.Date_Of_Registration__c) != "" && component.get(v.CompanyName.Place_of_Registration) != "" 
           && component.get(v.CompanyName.Country_Of_Registration__c) != "" && component.get(v.CompanyName.Registered_No__c) != "" 
           && component.get(v.CompanyName.Address_Line_1__c) != "" && component.get(v.CompanyName.Address_Line_1__c) != "" 
           && component.get(v.CompanyName.CompanyName.Address_Line_2__c) != ""){*/
        /*
        if(component.get("v.CompanyName.Entity_Name__c") != "" && component.get("v.CompanyName.Ends_With__c") != ""){
            
            var sectionActive = document.getElementById("NCsectionActive");
            var sectionComplete = document.getElementById("NCsectionComplete");
            sectionActive.classList.add('showHide');
            sectionComplete.classList.remove('showHide');
        }
        */
       //Commenting the below code. To move to final Submit button
        //helper.upsertSRFieldsNameCheck(component);
    },
    
    updateName:function(component,event,helper){
        //console.log(event.getSource().get('v.index'));
        /*
        var currentElement = event.getSource().get('v.name');
        var currentval = event.getSource().get('v.value');
        var currentli = document.getElementById(currentElement);
        currentli.classList.remove('slds-is-active');
        var currentImgSpan = document.getElementById(currentElement+"-span");
        var sectionActive = document.getElementById("NCsectionActive");
        var sectionComplete = document.getElementById("NCsectionComplete");
        
        if(currentval == ""){
            currentli.classList.remove('slds-is-complete');
            currentImgSpan.classList.add('hideSpan');  
            sectionComplete.classList.add('showHide');
            sectionActive.classList.remove('showHide');
            
        }else{
            currentli.classList.add('slds-is-complete');
            currentImgSpan.classList.remove('hideSpan');
        }
        */
        /*if(component.get(v.CompanyName.Entity_Name__c) != "" && component.get(v.CompanyName.Ends_With__c) != "" 
           && component.get(v.CompanyName.Check_Identical_to_existing__c) != "" && component.get(v.CompanyName.Fall_within_existing_entities__c) != "" 
           && component.get(v.CompanyName.Parent_Entity_Name__c) != "" && component.get(v.CompanyName.Former_Name__c) != "" 
           && component.get(v.CompanyName.Date_Of_Registration__c) != "" && component.get(v.CompanyName.Place_of_Registration) != "" 
           && component.get(v.CompanyName.Country_Of_Registration__c) != "" && component.get(v.CompanyName.Registered_No__c) != "" 
           && component.get(v.CompanyName.Address_Line_1__c) != "" && component.get(v.CompanyName.Address_Line_1__c) != "" 
           && component.get(v.CompanyName.CompanyName.Address_Line_2__c) != ""){*/
        /*
        if(component.get("v.CompanyName.Entity_Name__c") != "" && component.get("v.CompanyName.Ends_With__c") != ""){
            console.log('conditions passed');
            var sectionActive = document.getElementById("NCsectionActive");
            var sectionComplete = document.getElementById("NCsectionComplete");
            sectionActive.classList.add('showHide');
            sectionComplete.classList.remove('showHide');
        }
        */
        helper.updateEndsWith(component,event,helper);
    },
    
    checkFutureDate:function(component,event,helper){	
        var today = new Date();        
        var dd = today.getDate();
        var mm = today.getMonth() + 1; 
        var yyyy = today.getFullYear();
        if(dd < 10){
            dd = '0' + dd;
        } 
        if(mm < 10){
            mm = '0' + mm;
        }
        
        var todayFormattedDate = yyyy+'-'+mm+'-'+dd;
        
        if(component.get("v.TransactinalStageSR.Date_of_Registration__c") != '' && component.get("v.TransactinalStageSR.Date_of_Registration__c") > todayFormattedDate){
            alert("Date of registration cannot be future dated");
            var oldRegDate=component.get("v.oldDateOfRegistration");
            component.set("v.TransactinalStageSR.Date_of_Registration__c",oldRegDate);
        }
        else{
            var currentElement = event.getSource().get('v.name');
            var currentval = event.getSource().get('v.value');
            var currentli = document.getElementById(currentElement);
            currentli.classList.remove('slds-is-active');
            var currentImgSpan = document.getElementById(currentElement+"-span");
            var sectionActive = document.getElementById("NCsectionActive");
            var sectionComplete = document.getElementById("NCsectionComplete");
            
            if(currentval == ""){
                currentli.classList.remove('slds-is-complete');
                currentImgSpan.classList.add('hideSpan');  
                sectionComplete.classList.add('showHide');
                sectionActive.classList.remove('showHide');
                
            }else{
                currentli.classList.add('slds-is-complete');
                currentImgSpan.classList.remove('hideSpan');
            }
            
            /*if(component.get(v.CompanyName.Entity_Name__c) != "" && component.get(v.CompanyName.Ends_With__c) != "" 
           && component.get(v.CompanyName.Check_Identical_to_existing__c) != "" && component.get(v.CompanyName.Fall_within_existing_entities__c) != "" 
           && component.get(v.CompanyName.Parent_Entity_Name__c) != "" && component.get(v.CompanyName.Former_Name__c) != "" 
           && component.get(v.CompanyName.Date_Of_Registration__c) != "" && component.get(v.CompanyName.Place_of_Registration) != "" 
           && component.get(v.CompanyName.Country_Of_Registration__c) != "" && component.get(v.CompanyName.Registered_No__c) != "" 
           && component.get(v.CompanyName.Address_Line_1__c) != "" && component.get(v.CompanyName.Address_Line_1__c) != "" 
           && component.get(v.CompanyName.CompanyName.Address_Line_2__c) != ""){*/
            
            if(component.get("v.CompanyName.Entity_Name__c") != "" && component.get("v.CompanyName.Ends_With__c") != ""){
                console.log('conditions passed');
                var sectionActive = document.getElementById("NCsectionActive");
                var sectionComplete = document.getElementById("NCsectionComplete");
                sectionActive.classList.add('showHide');
                sectionComplete.classList.remove('showHide');
            }
            helper.upsertTransactionStageFieldsNameCheck(component,event,helper);
        }
    },
    handleUploadFinished: function (component, event) {
        // Get the uploaded file
        var uploadedFile = event.getParam("files")[0];
        console.log(uploadedFile);
        var documentId = uploadedFile.documentId;
        component.set("v.franchiseDoc.File_Name__c",uploadedFile.name);

        // Now perform a DML operation to save the description along with the document. Method will be something like this -
        //saveDescription(documentId, description); // Use aura actions to call this method. This prototype is supposed to be implemented on the apex controller.
    },
    handleButtonAct : function(component, event, helper) {
        
        
        try{
            
        let newURL = new URL(window.location.href).searchParams;
        
        var srID = newURL.get('srId');
        var buttonId =  event.target.id;
        console.log(buttonId);
            
        var action = component.get("c.getButtonAction");
        
        action.setParams({
            "SRID": srID,
            "ButtonId": buttonId
        });
        action.setCallback(this, function(response) {
        
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("button succsess")
            	
                console.log(response.getReturnValue());
                window.open(response.getReturnValue().pageActionName,"_self");
            }else{
            	console.log('@@@@@ Error '+response.getError()[0].message);
			}
        });
        $A.enqueueAction(action);
          
        }catch(err) {
           console.log(err.message) 
        } 
        

    }
    
})