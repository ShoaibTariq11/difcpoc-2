({
  doInit: function(component, event, helper) {
    let newURL = new URL(window.location.href).searchParams;
    var srId = newURL.get("srId");
    component.set("v.srId", srId);
  }
});