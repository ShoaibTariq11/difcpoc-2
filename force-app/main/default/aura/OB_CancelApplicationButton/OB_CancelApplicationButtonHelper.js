({
  checkSrStatus: function(component, event, helper) {
    var action = component.get("c.applicationStatusCheck");
    var requestWrap = {
      srId: component.get("v.recordId")
    };
    action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        component.set("v.respWrap", respWrap);
        if (respWrap.errorMessage && respWrap.errorMessage != "") {
          component.set("v.CanCancel", false);
        }
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
    });
    $A.enqueueAction(action);
  },

  showToast: function(component, event, helper, type, message) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      message: message,
      duration: " 5000",
      key: "info_alt",
      type: type,
      mode: "pester"
    });
    toastEvent.fire();
  }
});