({
  doInit: function(component, event, helper) {
    helper.checkSrStatus(component, event, helper);
  },

  handleCancelApplication: function(component, event, helper) {
    try {
      var cancellationReason = component.get("v.cancellationReason");
      var cancellationType = component.find("cancellationType").get("v.value");
      if (cancellationReason && cancellationType) {
        var action = component.get("c.cancelApplication");
        var requestWrap = {
          srId: component.get("v.recordId"),
          reasonForCancellation: component.get("v.cancellationReason"),
          cancelationType: component.find("cancellationType").get("v.value")
        };
        console.log(requestWrap);

        action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });
        component.set("v.serverCallInProgress", true);
        action.setCallback(this, function(response) {
          var state = response.getState();
          if (state === "SUCCESS") {
            var respWrap = response.getReturnValue();
            console.log(respWrap);
            component.set("v.respWrap", respWrap);
            if (!respWrap.errorMessage && respWrap.errorMessage == "") {
              helper.showToast(
                component,
                event,
                helper,
                "success",
                "Application Closed Successfully"
              );
              var dismissActionPanel = $A.get("e.force:closeQuickAction");
              dismissActionPanel.fire();
              $A.get("e.force:refreshView").fire();
              $A.get("e.force:refreshView").fire();
            } else {
              helper.showToast(
                component,
                event,
                helper,
                "error",
                respWrap.errorMessage
              );
            }
          } else {
            console.log("@@@@@ Error " + response.getError()[0].message);
            console.log(
              "@@@@@ Error Location " + response.getError()[0].stackTrace
            );
          }
          component.set("v.serverCallInProgress", false);
        });

        $A.enqueueAction(action);
      } else {
        helper.showToast(
          component,
          event,
          helper,
          "error",
          "Please fill all required fields"
        );
      }
    } catch (error) {
      console.log(error.message);
    }
  },

  closeAction: function(component, event, helper) {
    var dismissActionPanel = $A.get("e.force:closeQuickAction");
    dismissActionPanel.fire();
    $A.get("e.force:refreshView").fire();
  }
});