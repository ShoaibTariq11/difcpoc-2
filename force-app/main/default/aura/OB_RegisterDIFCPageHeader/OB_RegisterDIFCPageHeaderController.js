({
  fireApplicationEvent: function(cmp, event) {
    console.log("--page header -event fire-");

    var pageNameParam = cmp.get("v.pageWarp.pageObj.Community_Page__c");
    var pageIdParam = cmp.get("v.pageWarp.pageObj.Id");

    let newURL = new URL(window.location.href).searchParams;

    var srId = cmp.get("v.srId") ? cmp.get("v.srId") : newURL.get("srId");
    var flowId = cmp.get("v.flowId")
      ? cmp.get("v.flowId")
      : newURL.get("flowId");

    console.log("==Community_Page__c===" + pageNameParam);
    console.log("=event==page header==srId===" + srId);

    if ($A.util.isUndefined(pageNameParam) || $A.util.isEmpty(pageNameParam)) {
      var appEvent = $A.get("e.c:OB_RenderPageFlowAppEvent");
      cmp.set("v.autoExpand", true);
      appEvent.setParams({
        pageId: pageIdParam,
        srId: srId,
        flowId: flowId
      });

      appEvent.fire();
    } else {
      console.log("---redirect field-");
      var pathname = window.location.origin;
      console.log("==pathname===" + pathname);
      var newPathName =
        pathname +
        "/" +
        $A.get("$Label.c.OB_Site_Prefix") +
        "/s/" +
        pageNameParam +
        "?srId=" +
        srId +
        "&flowId=" +
        flowId +
        "&pageId=" +
        pageIdParam;
      console.log("==newPathName===" + newPathName);
      window.open(newPathName, "_self");
    }
  },
  init: function(cmp, event, helper) {
    console.log("---------page header init=========");
    console.log(cmp.get("v.pageWarp.isLocked"));

    if (cmp.get("v.autoExpand")) {
      //Current target.

      var pageIdParam = cmp.get("v.pageWarp.pageObj.Id");

      let newURL = new URL(window.location.href).searchParams;
      var flowIdParam = cmp.get("v.flowId")
        ? cmp.get("v.flowId")
        : newURL.get("flowId");
      var srIdParam = cmp.get("v.srId")
        ? cmp.get("v.srId")
        : newURL.get("srId");
      //var pageIdParam = (  cmp.get('v.pageId') ?  cmp.get('v.pageId') : newURL.get('pageId'));
      console.log("===page header==srIdParam===" + srIdParam);
      //Ob_LicenseActivity // OB_RegisterDIFCPages
      $A.createComponent(
        "c:OB_RegisterDIFCPages",
        {
          flowId: flowIdParam,
          pageId: pageIdParam,
          srId: srIdParam
        },
        function(newButton, status, errorMessage) {
          //Add the new button to the body array
          if (status === "SUCCESS") {
            var body = cmp.get("v.body");
            console.log("$$$$$$$$$ ", JSON.stringify(body));
            body.push(newButton);
            cmp.set("v.body", body);
            cmp.set("v.showDescription", true);
            cmp.set("v.showBody", true);
            cmp.set("v.showPageButton", false);
          } else if (status === "INCOMPLETE") {
            console.log("No response from server or client is offline.");
            // Show offline error
          } else if (status === "ERROR") {
            console.log("Error: " + errorMessage);
            // Show error message
          }
        }
      );
    }
  }
});