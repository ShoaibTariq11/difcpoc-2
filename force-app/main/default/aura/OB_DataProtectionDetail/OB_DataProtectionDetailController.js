({
	
    validatefields: function(cmp, event, helper) {
        var allValid = true;
        cmp.set("v.errorMessage", "");
        cmp.set("v.isError", false);
        let fields = cmp.find("dataProtectionkey"); 
        
        for (var i = 0; i < fields.length; i++) {
            var inputFieldCompValue = fields[i].get("v.value");
            //console.log(inputFieldCompValue);
            if(!inputFieldCompValue && fields[i] && fields[i].get("v.required")){
                
                console.log('no value');
                //console.log('bad');
                fields[i].reportValidity();
                allValid = false;
            }   
        }
        
        if (allValid == false) {
            console.log("Error MSG");
            cmp.set("v.errorMessage", "Please fill required field");
            cmp.set("v.isError", true);
            //helper.showToast(cmp,event,"Please fill required field",'error');
            var refeshEvnt = cmp.getEvent("refeshEvnt");
            refeshEvnt.setParams({
                "isViewRemove":"errorMessage"});
            refeshEvnt.fire();
            //return;
        } 
    },
  closeModel: function(cmp, event, helper) {
    //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
    console.log("--toast close--");
    cmp.set("v.errorMessage", "");
    cmp.set("v.isError", false);
    
  }
})