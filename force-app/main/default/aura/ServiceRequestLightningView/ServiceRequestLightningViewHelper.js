({
	clientAddress : function(component, event) {
    	var action = component.get("c.fetchAddressDetails");
        action.setParams({
            recId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.accountAddress", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    clientName : function(component, event) {
    	var action = component.get("c.fetchClientName");
        action.setParams({
            recId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.accountName", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    loadDebtorParty : function(component, event) {
    	var action = component.get("c.fetchSRDebtorPartyList");
        action.setParams({
            recordId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.debtList", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },

    loadApprovedRequestForm : function(component, event) {
    	var action = component.get("c.fetchApprovedForm");
        action.setParams({
            recId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.appForm", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})