({
	doInIt: function(component, event, helper) {
        component.set("v.serviceReqId",component.get("v.recordId"));
        helper.loadApprovedRequestForm(component, event);
        //helper.loadDebtorParty(component, event);
    },
    
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },
    
    hideSpinner : function(component,event,helper) {   
        component.set("v.Spinner", false);
    }
})