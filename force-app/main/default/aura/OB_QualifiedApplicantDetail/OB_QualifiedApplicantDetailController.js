({
  doInit: function(component, event, helper) {
    debugger;
    let newURL = new URL(window.location.href).searchParams;
    var srId = newURL.get("srId");
    component.set("v.srId", srId);
    console.log(component.get("v.AmendWrap.Id"));

    if (component.get("v.AmendWrap.Id") == null) {
      component.set("v.objectAPIName", "");
    }

    if (
      component.get("v.AmendWrap.Id") != null ||
      component.get("v.AmendWrap.Id") != undefined
    ) {
      var action = component.get("c.viewSRDocs");

      action.setParams({
        srId: component.get("v.srId"),
        amendmentId: component.get("v.AmendWrap.Id")
      });
      action.setCallback(this, function(response) {
        var state = response.getState();
        console.log(state);

        if (state === "SUCCESS") {
          var result = response.getReturnValue();
          console.log(result);

          component
            .find("Certificate_of_Incorporation")
            .set("v.value", result.Certificate_of_Incorporation.FileName);
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    }
  },

  newSave: function(component, event, helper) {
    var allValid = true;
    //component.set("v.errorMessage", "");
    //component.set("v.isError", false);
    var qualAppTypevalue = component.get(
      "v.AmendWrap.Select_the_Qualified_Applicant_Type__c"
    );
    var fileUploaded = component.find("Certificate_of_Incorporation");
    var fileUploadedDIFCEntity = component.find("Certificate_of_Incorporation_affiliated_entity");
    var fileUploadedAffiliatedEntity = component.find("Certificate_of_Incorporation_affiliated_entity");
      
    let fields = component.find("requiredField");
    for (var i = 0; i < fields.length; i++) {
      var inputFieldCompValue = fields[i].get("v.value");
      //console.log(inputFieldCompValue);
      if (!inputFieldCompValue && fields[i] && fields[i].get("v.required")) {
        console.log("no value");
        //console.log('bad');
        fields[i].reportValidity();
        allValid = false;
      }
        console.log("qualAppTypevalue####"+qualAppTypevalue);
      if (qualAppTypevalue == "Authorised Firm") {
        if (!fileUploaded.get("v.value")) {
          allValid = false;
          component.find("fileMissing").set("v.value", "upload file");
        }
      }
      if (qualAppTypevalue == "Affiliates of a DIFC Qualifying Applicant") {
         console.log("qualAppTypevalue%%%####"+fileUploadedDIFCEntity.get("v.value"));
          if (!fileUploadedDIFCEntity.get("v.value")) {
          allValid = false;
          component.find("fileMissing").set("v.value", "upload file");
          }
           if (!fileUploadedAffiliatedEntity.get("v.value")) {
          allValid = false;
          component.find("fileMissing").set("v.value", "upload file");
          }         
      } 
          
    }
    if (!allValid) {
      console.log("Error MSG");
      //component.set("v.errorMessage", "Please fill required field");
      //component.set("v.isError", true);
      helper.createToast(component, event, "Please fill required field");
      return;
    }

    try {
      // var isError = component.get("v.isError");

      let newURL = new URL(window.location.href).searchParams;
      var pageId = newURL.get("pageId");
      var flowId = newURL.get("flowId");

      console.log("======flowId=====");
      debugger;

      var action = component.get("c.amendSave");
      console.log(JSON.parse(JSON.stringify(component.get("v.AmendWrap"))));
      action.setParams({
        lookupvalue: component.get("v.lookupvalue"),
        sericeobj: component.get("v.serviceWrap"),
        amdobj: component.get("v.AmendWrap"),
        pageId: pageId,
        flowId: flowId,
        docMap: component.get("v.docMasterContentDocMap")
      });

      // console.log('--12--'+component.get("v.serviceWrap").Id);
      //console.log('===23='+component.get("v.AmendWrap").Role__c);

      action.setCallback(this, function(response) {
        console.log("===123442===");
        var state = response.getState();
        console.log("state--------", state);
        if (component.isValid() && state === "SUCCESS") {
          console.log("in succsess");
          var responsewrap = response.getReturnValue();
          console.log("responsewrap", responsewrap);

          if (responsewrap.errorMessage == "This Entity already exist") {
            // component.set("v.errorMessage", responsewrap.errorMessage);
            // component.set("v.isError", true);
            helper.createToast(component, event, responsewrap.errorMessage);
          } else if (responsewrap.errorMessage == "Success") {
            component.set(
              "v.amendList",
              responsewrap.ServiceWrapper.amendWrapList
            );
            component.set("v.updatedAmendObj", responsewrap.updatedAmendObj);

            component.set("v.showFormAmend", false);
          } else {
            // component.set("v.errorMessage", responsewrap.errorMessage);
            //component.set("v.isError", true);
            helper.createToast(component, event, responsewrap.errorMessage);
          }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log("@@@@@ Error " + response.getError()[0].stackTrace);
          //component.set("v.errorMessage", response.getError()[0].message);
          // component.set("v.isError", true);
          helper.createToast(component, event, response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (error) {
      console.log(error);
    }
  },
  getQualifiedAppType: function(component, event, helper) {
    helper.getAccountFilter(component, event);
  },
  clearRegistrationNumber: function(component, event, helper) {
    var accObj = event.getParam("instanceId");

    component.set("v.AmendWrap.Registration_No__c", "");
  },

  handleLookupEvent: function(component, event, helper) {
    console.log("handleLookupEvent");
    var accObj = event.getParam("sObject");
    console.log("lookup--" + JSON.stringify(accObj));
    if (accObj.Registration_License_No__c) {
      component.set(
        "v.AmendWrap.Registration_No__c",
        accObj.Registration_License_No__c
      );
    } else {
      if (accObj.Account__r && accObj.Account__r.Registration_License_No__c)
        component.set(
          "v.AmendWrap.Registration_No__c",
          accObj.Account__r.Registration_License_No__c
        );
    }
  },

  cancel: function(component, event, helper) {
    component.set("v.showFormAmend", false);
  },
  handleFilesChange: function(component, event, helper) {
    console.log("@@@@@@ in handleFilesChange ");
    var fileName = "No File Selected..";
    if (event.getSource().get("v.files").length > 0) {
      fileName = event.getSource().get("v.files")[0].name;
      console.log(fileName);
    }
    component.set("v.fileName", fileName);
  },

  handleFileUploadFinished: function(component, event) {
    // Get the list of uploaded files
   // debugger;
    var uploadedFiles = event.getParam("files");
      console.log('=====uploadedFiles'+uploadedFiles);
    var fileName = uploadedFiles[0].name;
       console.log('=====fileName'+fileName);
    var contentDocumentId = uploadedFiles[0].documentId;
    console.log('=======contentDocumentId'+contentDocumentId);
    var docMasterCode = event.getSource().get("v.name");
    console.log('=====docMasterCode'+docMasterCode);
   // component.find("fileMissing").set("v.value", "");
    component.find(docMasterCode).set("v.value", fileName);
    
    var mapDocValues = {};
    var docMap = component.get("v.docMasterContentDocMap");
      console.log('=====docMapLoop here');
      console.log('=====docMap'+docMap);
    for (var key in docMap) {
        
      mapDocValues[key] = docMap[key];
    }
   // debugger;
    mapDocValues[docMasterCode] = contentDocumentId;
    console.log('======mapDocValues'+mapDocValues);
    component.set("v.docMasterContentDocMap", mapDocValues);
    console.log('========Final',
      JSON.stringify(component.get("v.docMasterContentDocMap"))
    );

    //alert("Files uploaded : " + uploadedFiles.length);
  }
});