({
  onSelect: function (cmp, event, helper) {
    cmp.set("v.serverCallInProgress", true);
    var passOn = false;
    setTimeout(() => {
      var onAmedSave = cmp.get("c.onAmedSaveToDB");
      var reqWrapPram = {
        oprLocWrap: cmp.get("v.oprLocWrap"),
        srWrap: cmp.get("v.srWrap")
      };
      console.log("########### reqWrapPram ", JSON.stringify(reqWrapPram));
      onAmedSave.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram)
      });

      onAmedSave.setCallback(this, function (response) {
        var state = response.getState();
        console.log("callback state: " + state);

        if (cmp.isValid() && state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log("@@@@@ respWrap " + JSON.stringify(respWrap));

          if (respWrap.errorMessage) {
            cmp.set("v.serverCallInProgress", false);
            helper.showToast(
              cmp,
              event,
              helper,
              respWrap.errorMessage,
              "error"
            );
            //alert(respWrap.errorMessage);
          } else {
            //helper.showToast(cmp, event, helper, 'Address Added', 'info');
            //alert('Address Added.');
            var refeshEvnt = cmp.getEvent("refeshEvnt");
            refeshEvnt.setParams({
              message: "refresh event"
            });
            refeshEvnt.fire();
            cmp.set("v.serverCallInProgress", false);
          }
        } else {
          cmp.set("v.serverCallInProgress", false);
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(onAmedSave);
    }, 2000);
  }
});