({
	loadSRDocs : function(component,event, helper){ 
        //var amendmentWrapper = component.get("v.amedWrap");
        //console.log('amendmentWrapper----'+amendmentWrapper);
        //var amedId = amendmentWrapper.amedObj.Id;
        //console.log('amedId-----'); 
        
        
    },
    /**
     * Display a message
     */
    displayToast : function (title, message) {
		console.log('displayToast');
        var toast = $A.get('e.force:showToast');

        // For lightning1 show the toast
        if (toast) {
            //fire the toast event in Salesforce1
            toast.setParams({
                'title': title,
                'message': message,
                mode:'sticky'
            });

            toast.fire();
        } else { // otherwise throw an alert        
            alert(title + ': ' + message);
        }
	},
    show: function (cmp, event) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
    },
    hide:function (cmp) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-show");
        $A.util.addClass(spinner, "slds-hide");
    },
    handleUploadFinished: function (cmp, event,helper) 
    {
        
    }
})