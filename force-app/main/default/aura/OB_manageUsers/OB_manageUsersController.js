({
  doInit: function(component, event, helper) {
    helper.GetUserData(component, event, helper);
  },

  handleEdit: function(component, event, helper) {
    try {
      var contId = event.currentTarget.dataset.contactid;
      var roles = event.currentTarget.dataset.role;
      var loggedInContId = component.get("v.respWrap.loggedInUser.ContactId");
      if (contId == loggedInContId || (roles && roles.includes("Super User"))) {
        helper.promoteUserHelper(component, event, helper);
      } else {
        helper.openModal(component, event, helper);
      }
    } catch (error) {
      console.log(error.message);
    }
  },

  openModal: function(component, event, helper) {
    var dataSet = event.currentTarget.dataset;
    console.log(dataSet);
    var loggedInContactId = component.get("v.respWrap.loggedInUser.ContactId");

    if (dataSet.mode == "Deactivate") {
      if (dataSet.contactid === loggedInContactId) {
        helper.showToast(
          component,
          event,
          helper,
          "Cannot Deactivate current logged in user",
          "error"
        );
        return;
      }
    }

    $A.createComponent(
      "c:OB_AddNewUser",
      {
        modalHeader: dataSet.modalheader,
        mode: dataSet.mode,
        relToUpdate: dataSet.relid,
        accId: component.get("v.respWrap.accId"),
        loggedInUser: component.get("v.respWrap.loggedInUser")
      },
      function(modal) {
        if (component.isValid()) {
          var targetCmp = component.find("ModalDialogPlaceholder");
          var body = targetCmp.get("v.body");
          body.push(modal);
          targetCmp.set("v.body", body);
        }
      }
    );
  },

  refreshHandler: function(component, event, helper) {
    if (event.getParam("isHardRefresh") === true) {
      location.reload(true);
    } else {
      helper.GetUserData(component, event, helper);
    }
  },

  promoteUser: function(component, event, helper) {
    var dataSet = event.currentTarget.dataset;
    debugger;
    var isblocked = dataSet.isblocked;
    console.log(isblocked);
    console.log(typeof isblocked);

    if (isblocked && isblocked == "true") {
      helper.showToast(
        component,
        event,
        helper,
        "selected user cannot be promoted",
        "error"
      );
    } else {
      helper.promoteUserHelper(component, event, helper);
    }
  },

  handleFilter: function(component, event, helper) {
    try {
      //var userList = component.get("v.respWrap.relatedUserList");
      var relUserList = component.get("v.unfilteredUserList");

      var searchText = component.get("v.searchString");
      var selectedRole = component.get("v.selectedValue");

      if (searchText && searchText.trim()) {
        relUserList = relUserList.filter(acc =>
          acc.Contact.Name.toLowerCase().includes(searchText.toLowerCase())
        );
      }
      if (selectedRole) {
        relUserList = relUserList.filter(
          user => user.Roles && user.Roles.includes(selectedRole)
        );
      }
      console.log(relUserList);

      component.set("v.respWrap.relatedUserList", relUserList);
    } catch (error) {
      console.log(error.message);
      console.log(error.stackTrace);
    }
  },

  navToPromoteSr: function(component, event, helper) {
    window.location.href =
      "topupbalance/?srId=" + component.get("v.relSRselected");
  }
});