({
    doInit : function(component, event, helper) {
        
        var test = "\"v.srId\"";
        console.log(test);
        console.log(component.get("v.srId"));
        //console.log(component.get(test));
        
        
    },
    
    handleSubmit :function(component, event, helper) {
        
        component.set('v.showSpinner', true);
        if(component.get("v.isNew", true)) {
            
            event.preventDefault(); // stop the form from submitting
            
            var fields = event.getParam('fields');
            console.log(fields);
            console.log(event.getSource());
            /*
            fields.ServiceRequest__c = component.get("v.srId");
            fields.RecordTypeId = component.get("v.recTypeId");
            fields.Amendment_Type__c = component.get("v.amendType");

            var defaultFieldParam = component.get("v.defaultFieldParam")
            if(defaultFieldParam.length > 0) {
            for(var defaultField of defaultFieldParam) {
                console.log(fields[defaultField.fieldName]);
                if(component.get("v.isConvert", true)) {
                    fields[defaultField.fieldName] += '; ' + defaultField.fieldValue;
                }else{
                    fields[defaultField.fieldName] = defaultField.fieldValue;
                }
                
            }    
            }

            component.find('difcForm').submit(fields);
            */
        } 
        
  },
    
    handleSuccess:function(component, event, helper) {
        
        component.set('v.showSpinner', false);
        var payload = event.getParams().response;
        console.log(payload.id);
        
        if(component.get("v.isNew", true)) {
            
            // adding to list of existing amendment record
            var toAddArray = component.get("v.amendListToAdd");
            if(component.get("v.isConvert", true)) {  
                toAddArray.push({Id:payload.id, Name__c:payload.Name__c});
            }else {
            	toAddArray.push(payload.id);    
            }
            
            component.set("v.amendListToAdd",toAddArray);
            
            
        }   
        // removing from dropdown
         if(component.get("v.isConvert", true)) {   
            var toRemoveArray = component.get("v.amendListToRemove");
            var amendID = component.get('v.amendId');
            console.log(amendID);
             
             for(var rec of toRemoveArray) {
                 console.log(rec);
                 if(rec.Id == amendID) {
                 	var index = toRemoveArray.indexOf(rec);  
                    toRemoveArray.splice(index, 1);
                 }  
             }
            /*var index = toRemoveArray.indexOf(amendID);
            console.log(index);
            if (index > -1) {
                toRemoveArray.splice(index, 1);
            }*/
            component.set("v.amendListToRemove",toRemoveArray);
            component.set("v.selectedValue", '');
        }
                    
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode : "dismissable",
            message : "Saved Succsessfully",
            type : "success",
            duration : 500
        });
        toastEvent.fire();
        component.set("v.isVisible",false);

},
    
    handleError: function(cmp, event, helper) {
        component.set('v.showSpinner', false);
    },
    
    closeForm:function(component, event, helper) {
        component.set("v.isVisible",false);
  
},
	
})