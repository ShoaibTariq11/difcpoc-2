({
  doInit: function (component, event, helper) {
    let newURL = new URL(window.location.href).searchParams;
    var flowIdParam = component.get("v.flowId")
      ? component.get("v.flowId")
      : newURL.get("flowId");
    var srIdParam = component.get("v.srId")
      ? component.get("v.srId")
      : newURL.get("srId");
    var pageIdParam = component.get("v.pageId")
      ? component.get("v.pageId")
      : newURL.get("pageId");
    // set if comming from url
    component.set("v.pageId", pageIdParam);
    component.set("v.flowId", flowIdParam);
    component.set("v.srId", srIdParam);
    /* console.log(component.get("v.srId"));
    if (component.get("v.amendWrap.amedObj.Id")) {
      var action = component.get("c.viewSRDocs");

      action.setParams({
        srId: component.get("v.srId"),
        amendmentId: component.get("v.amendId")
      });
      action.setCallback(this, function(response) {
        var state = response.getState();
        debugger;
        console.log("ji");
        console.log(state === "SUCCESS");

        if (state === "SUCCESS") {
          var result = response.getReturnValue();
          console.log(result);

          component
            .find("Passport_Copy_Individual")
            .set("v.value", result.Passport_Copy_Individual.FileName);
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });

      $A.enqueueAction(action);
    } */
  },
  handleOCREvent: function (cmp, event, helper) {
    var ocrWrapper = event.getParam("ocrWrapper");
    var mapFieldApiObject = ocrWrapper.mapFieldApiObject;
    console.log("in chache refresh ---> " + mapFieldApiObject);
    console.log(mapFieldApiObject);

    // Change the key here
    var fields = cmp.find("approvedFields");
    console.log("in chache refresh ---> " + fields);

    //parsing of code.
    for (var key in mapFieldApiObject) {
      for (var i = 0; i < fields.length; i++) {
        console.log(fields[i].get("v.fieldName"));
        if (fields[i].get("v.fieldName")) {
          console.log(fields[i].get("v.fieldName"));
          if (fields[i].get("v.fieldName").toLowerCase() == key.toLowerCase()) {
            fields[i].set("v.value", mapFieldApiObject[key]);
          }
        }
      }
    }
    //helper.hideSpinner(cmp, event, helper);
  },

  handleSubmit: function (component, event, helper) {
    try {
      var senMangRole =
        component.get("v.roleToAssign") == "Senior Management" ? true : false;
      //  console.log("0000000000000000" + fields);
      var allValid = true;

      let fields = component.find("approvedFields");
      //  console.log(fields);
      // console.log(component.get("v.isNew"));
      
     /* if(component.get("v.roleToAssign") == "Emergency contact"){
    	  component.get("v.isNew") = true;
      }*/
      
      for (var i = 0; i < fields.length; i++) {
        var inputFieldCompValue = fields[i].get("v.value");
        console.log(component.get("v.isNew"));
        if (!component.get("v.isNew")) {
          if (
            !inputFieldCompValue &&
            fields[i] &&
            fields[i].get("v.required") &&
            (fields[i].get("v.fieldName") == "Job_Title_Capacity__c" || (component.get("v.roleToAssign") == "Emergency contact" && fields[i].get("v.fieldName") == "Mobile__c"))
          ) {
            console.log("no value");
            //console.log('bad');
            fields[i].reportValidity();
            allValid = false;
          }
        } else {
          if (
            !inputFieldCompValue &&
            fields[i] &&
            fields[i].get("v.required")
          ) {
            console.log(fields[i].get("v.fieldName"));
            console.log("no value");
            console.log("bad");
            fields[i].reportValidity();
            allValid = false;
          }
        }
      }

      if (!allValid) {
        console.log("Error MSG");

        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
          mode: "dismissable",
          message: "Please fill required field",
          type: "error",
          duration: 500,
        });
        toastEvent.fire();

        return;
      }

      if (senMangRole && component.get("v.isNew")) {
        //handle err is remaining.(for ocr validation)
        var fileError = helper.fileUploadHelper(component, event, helper);
        console.log(fileError);

        if (fileError) {
          return;
        }
      }

      component.set("v.showSpinner", true);
      var toAssign = component.get("v.toAssign");

      if (component.get("v.toAssign")) {
        var role = component.get("v.amendWrap.amedObj.Role__c");
        role += "; " + component.get("v.roleToAssign");
        component.set("v.amendWrap.amedObj.Role__c", role);
      }

      var action = component.get("c.amenSaveAction");
      var requestWrap = {
        amendObj: component.get("v.amendWrap.amedObj"),
        srId: component.get("v.srId"),
        docMap: component.get("v.docMasterContentDocMap"),
      };
      action.setParams({
        requestWrapParam: JSON.stringify(requestWrap),
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log(response.getReturnValue());
          var respWrapp = response.getReturnValue();
          if (response.getReturnValue().errorMessage == "no error recorded") {
            component.set("v.showSpinner", false);
            /* component.set(
            "v.AmendListWrapp",
            response.getReturnValue().amendWrap.amedObjp
          ); */
            /* var toastEvent = $A.get("e.force:showToast");
          toastEvent.setParams({
            mode: "dismissable",
            message: "Saved Succsessfully",
            type: "success",
            duration: 500
          }); 
          toastEvent.fire();*/
            component.set("v.respWrap", response.getReturnValue());
            component.set("v.isVisible", false);
            component.set("v.selectedValue", "");
            component.set("v.amedIdInFocus", "none");
          } else {
            component.set("v.showSpinner", false);
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
              mode: "dismissable",
              message: respWrapp.errorMessage,
              type: "error",
              duration: 500,
            });
            toastEvent.fire();
          }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (error) {
      console.log(error.message);
    }
  },

  handleSuccess: function (component, event, helper) {},

  handleError: function (cmp, event, helper) {
    component.set("v.showSpinner", false);
  },

  handleFileUploadFinished: function (component, event) {
    // Get the list of uploaded files

    var uploadedFiles = event.getParam("files");
    var fileName = uploadedFiles[0].name;
    var contentDocumentId = uploadedFiles[0].documentId;
    console.log(contentDocumentId);
    var docMasterCode = event.getSource().get("v.name");
    component.find(docMasterCode).set("v.value", fileName);
    var mapDocValues = {};
    var docMap = component.get("v.docMasterContentDocMap");
    for (var key in docMap) {
      mapDocValues[key] = docMap[key];
    }
    debugger;
    mapDocValues[docMasterCode] = contentDocumentId;
    console.log(mapDocValues);
    component.set("v.docMasterContentDocMap", mapDocValues);
    console.log(
      "Doc@@@@@@$$$$$$$$$$$$$$$$",
      JSON.parse(JSON.stringify(component.get("v.docMasterContentDocMap")))
    );

    //alert("Files uploaded : " + uploadedFiles.length);
  },

  closeForm: function (component, event, helper) {
    //component.set("v.isVisible",false);
    component.set("v.amedIdInFocus", "none");
  },
});