({
    doInIt : function(component, event, helper) {
    	var today = new Date();
        var monthDigit = today.getMonth() + 1;
        if (monthDigit <= 9) {
            monthDigit = '0' + monthDigit;
        }
        component.set('v.today', today.getFullYear() + "-" + monthDigit + "-" + today.getDate());
    },
    
    dateChange: function(component, event, helper) {
        var rawDate = component.get("v.informationRequest.Date_Record_Filed__c");
        var isInserted = component.get("v.inserted");
        if(!isInserted) {
            var formattedDate = $A.localizationService.formatDate(rawDate, "yyyy-MM-dd");
            console.log('--formated date--');
            console.log(formattedDate);
            component.set("v.informationRequest.Date_Record_Filed__c", formattedDate);
        } else {
            component.set("v.informationRequest.Date_Record_Filed__c", "2000-01-01");
        }
    },
    
    AddNewRow : function(component, event, helper) {
       // fire the AddNewRowEvt Lightning Event
        component.getEvent("AddRowEvt").fire();
    },
    
    removeRow : function(component, event, helper) {
     // fire the DeleteRowEvt Lightning Event and pass the deleted Row Index to Event parameter/attribute
       component.getEvent("DeleteRowEvt").setParams({"indexVar" : component.get("v.rowIndex") }).fire();
    }, 
  
})