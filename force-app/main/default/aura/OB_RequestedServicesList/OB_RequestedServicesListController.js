({
  onchange: function (component, event, helper) {
    //console.log('=event=='+event.which );
    //if (event.which == 13) {
    console.log(
      component.get("v.searchKeyword") +
        "=requested service home indie ======" +
        component.get("v.statusValue")
    );
    var reqWrapPram = {
      serviceNumber: component.get("v.searchKeyword"),
      requestType: component.get("v.requestType"),
      serviceStatus: component.get("v.statusValue"),
      inProgressHeaderDisplay: component.get("v.inProgressHeaderDisplay"),
      isInit: false,
      userAccessTypeSet: component.get("v.userAccessTypeSet")
    };

    helper.SearchHelper(component, event, helper, reqWrapPram, false);
    //}
  },
  onEnter: function (component, event, helper) {
    //console.log('=event=='+event.which );
    if (event.which == 13) {
      console.log(
        component.get("v.searchKeyword") +
          "=requested service home indie ======" +
          component.get("v.statusValue")
      );
      var reqWrapPram = {
        serviceNumber: component.get("v.searchKeyword"),
        requestType: component.get("v.requestType"),
        serviceStatus: component.get("v.statusValue"),
        inProgressHeaderDisplay: component.get("v.inProgressHeaderDisplay"),
        isInit: false,
        userAccessTypeSet: component.get("v.userAccessTypeSet")
      };

      helper.SearchHelper(component, event, helper, reqWrapPram, false);
    }
  },
  fireApplicationEvent: function (cmp, event) {
    // Get the application event by using the
    // e.<namespace>.<event> syntax
    console.log("Request fireApplicationEvent--");
    var appEvent = $A.get("e.c:aeEvent");
    appEvent.setParams({ statusValue: "In Progress" });
    appEvent.fire();
  },
  callServer: function (component, event, helper) {
    //component.set("v.callInit",event.getParam("serviceTab"));
    //var callInit = component.get("v.callInit");
    //console.log('=requested service home='+callInit);
    //if(callInit == true){
    console.log("----onload---");
    var reqWrapPram = {
      serviceNumber: component.get("v.searchKeyword"),
      requestType: component.get("v.requestType"),
      serviceStatus: component.get("v.statusValue"),
      inProgressHeaderDisplay: component.get("v.inProgressHeaderDisplay"),
      isInit: true,
      oldFieldSortingQuery: "Name DESC NULLS LAST",
      NewFieldSortingQuery: "SR_Number__c DESC NULLS LAST",
      userAccessTypeSet: component.get("v.userAccessTypeSet")
    };

    helper.SearchHelper(component, event, helper, reqWrapPram, true);
    //}
  },

  renderPage: function (component, event, helper) {
    helper.renderPage(component, event);
  },
  handleEvent: function (component, event, helper) {
    component.set("v.callInit", event.getParam("serviceTab"));
    var callInit = component.get("v.callInit");
    if (callInit === true) {
      console.log("=Requested service tab=" + callInit);
      var reqWrapPram = {
        serviceNumber: component.get("v.searchKeyword"),
        requestType: component.get("v.requestType"),
        serviceStatus: component.get("v.statusValue"),
        inProgressHeaderDisplay: component.get("v.inProgressHeaderDisplay"),
        isInit: false,
        userAccessTypeSet: component.get("v.userAccessTypeSet")
      };

      helper.SearchHelper(component, event, helper, reqWrapPram, false);
    }
  },

  handleSort: function (component, event, helper) {
    if (!component.get("v.inProgressHeaderDisplay")) {
      var fieldToSort = event.target.dataset;
      var oldFieldName = fieldToSort.oldfieldname;
      var newFieldName = fieldToSort.newfieldname;

      var currentfield = component.get("v.CurrentFieldBeingSorted");

      console.log(newFieldName);
      console.log(currentfield);

      if (!currentfield) {
        component.set("v.CurrentFieldBeingSorted", newFieldName);
      } else if (currentfield == newFieldName) {
        if (component.get("v.CurrentSortingorder") == "ASC") {
          component.set("v.CurrentSortingorder", "DESC NULLS LAST");
          component.set("v.sortingiconName", "utility:down");
        } else {
          component.set("v.CurrentSortingorder", "ASC");
          component.set("v.sortingiconName", "utility:up");
        }
      } else {
        component.set("v.CurrentFieldBeingSorted", newFieldName);
        component.set("v.CurrentSortingorder", "ASC");
        component.set("v.sortingiconName", "utility:up");
      }

      var oldFieldSortingQuery =
        oldFieldName + " " + component.get("v.CurrentSortingorder");
      var NewFieldSortingQuery =
        newFieldName + " " + component.get("v.CurrentSortingorder");

      console.log(oldFieldName + "  " + newFieldName);
      console.log(oldFieldSortingQuery + "  " + NewFieldSortingQuery);
      var reqWrapPram = {
        serviceNumber: component.get("v.searchKeyword"),
        requestType: component.get("v.requestType"),
        serviceStatus: component.get("v.statusValue"),
        inProgressHeaderDisplay: component.get("v.inProgressHeaderDisplay"),
        oldFieldSortingQuery: oldFieldSortingQuery,
        NewFieldSortingQuery: NewFieldSortingQuery,
        isInit: false,
        userAccessTypeSet: component.get("v.userAccessTypeSet")
      };

      helper.SearchHelper(component, event, helper, reqWrapPram, false);
    }
  }
});