({
  doInit: function(component, event, helper) {
    let newURL = new URL(window.location.href).searchParams;
    var valueFromURL = newURL.get("valueTextEntity");
    console.log("setting nameid" + newURL.get("NameId"));
    component.set("v.NameId", newURL.get("NameId"));
    component.set("v.srId", newURL.get("srId"));
    var nameId = component.get("v.NameId");
    if (nameId && nameId != "undefined") {
      console.log("inside if undefined");
      component.set("v.NameIdtoController", nameId);
    }
    console.log(
      "NameIdtoController is " + component.get("v.NameIdtoController")
    );
    console.log("value received do init from URL is " + valueFromURL);

    //If the Name passed contains only the space
    if (valueFromURL == "%20") {
      valueFromURL = valueFromURL.replace(/%20/g, "");
    }

    //If the Name passed contains space in between
    valueFromURL = valueFromURL.replace(/%20/g, " ");
    component.set("v.valueTextEntity", valueFromURL);
    console.log("value after  do init  replacing space " + valueFromURL);
    console.log("id passed from Namecheckblock is " + nameId);
    component.set("v.valueTextEntity", valueFromURL);

    //helper.loadInit(component,event,helper);
    //helper.duplicateCheck(component,event,helper);//Apex method:checkAccountName
    //helper.getList(component, event, helper); //Apex method : checkName
    //helper.getReservedNames(component, event, helper); //Apex method: getReservedNames
    helper.loadSRDocs(component, event, helper);
  },

  searchName: function(component, event, helper) {
    var nameEntered = component.get("v.valueTextEntity");
    component.set("v.valueText", nameEntered);
    component.set("v.showConfirmName", "false");
    component.set("v.isOpenDocRequired", "false");
    component.set("v.displayWarnReason", "false");
    component.set("v.warningMessages", []);
    component.set("v.isOpenDocRequired", "false");
    component.set("v.displayWarnReason", "false");
    component.set("v.displayWarnDup", "false");
    component.set("v.notAllowedNameDup", []);
    var enteredValue = component.get("v.valueText");
    console.log("enteredValue" + enteredValue);
    //The name criteria evaluation check is executed only if the entered value is not null
    if (enteredValue) {
      //validateName
      helper.show(component, event);
      var action = component.get("c.validateName");
      action.setParams({
        searchTerm: enteredValue,
        srId: component.get("v.srId")
      });
      action.setCallback(this, function(response) {
        //store state of response
        var state = response.getState();
        if (state === "SUCCESS") {
          var responseResult = response.getReturnValue();
          console.log(responseResult);
          component.set("v.warningMessages", responseResult.lstWarningMessage);
          component.set("v.warningTypes", responseResult.lstWarningType);
          component.set(
            "v.isCompanyReserved",
            responseResult.isCompanyReserved
          );
          console.log(responseResult.lstWarningMessage.length);
          var warningTypesArr = responseResult.lstWarningType;
          var warningMsgArr = responseResult.lstWarningMessage;
          var identicalNamesArr = responseResult.lstIdenticalName;
          var reservedExistingNamesArr = responseResult.lstReservedName;
          var showConfirmName = true;
          if (warningMsgArr) {
            for (var index in warningMsgArr) {
              if (warningMsgArr[index].warningType == "Error") {
                showConfirmName = false;
                break;
              }
            }
          }
          var lstWarningMsg = responseResult.lstWarningMessage;
          //Check for reserved and existing name
          if (reservedExistingNamesArr && reservedExistingNamesArr.length > 0) {
            showConfirmName = false;
            //push code

            lstWarningMsg.push({
              message:
                "Warning: The selected company name is considered to be same as reserved names. Please choose another name."
            });
          } else {
            if (
              warningTypesArr &&
              warningTypesArr.indexOf("Warning Message and Upload Doc") > -1
            )
              component.set("v.isOpenDocRequired", "true");
            if (
              warningTypesArr &&
              warningTypesArr.indexOf("Warning Message and Reason Text") > -1
            )
              component.set("v.displayWarnReason", "true");
            if (identicalNamesArr && identicalNamesArr.length > 0) {
              component.set("v.displayWarnDup", "true");
              component.set("v.notAllowedNameDup", identicalNamesArr);
              lstWarningMsg.push({
                message:
                  "Warning: The selected company name  is considered to be same as existing names shown below. Please provide the evidence of relationship if you wish to proceed"
              });
            }
          }

          component.set("v.warningMessages", lstWarningMsg);
          component.set("v.showConfirmName", showConfirmName);

          //Hide the spinner
          helper.hide(component, event);
        } else if (state === "ERROR") {
          // Handle any error by reporting it
          var errors = response.getError();
          if (errors) {
            if (errors[0] && errors[0].message) {
              helper.displayToast("Error", errors[0].message, "error");
            }
          } else {
            helper.displayToast("Error", "Unknown error.", "error");
          }
          helper.hide(component, event);
        }
      });
      $A.enqueueAction(action);
      //validateName Ends
    }
  },

  submitName: function(component, event, helper) {
    var EntityName = component.get("v.valueTextEntity");
    var TradingName = component.get("v.valueTextTrading");

    if (EntityName == null || TradingName == null) {
      var toastEvent = $A.get("e.force:showToast");
      toastEvent.setParams({
        title: "Warning!",
        message: "The Entity name/Trading Name is empty"
      });
      toastEvent.fire();
    }

    if (EntityName != null && TradingName != null) {
      console.log("inside submitName lightning controller");
      var action = component.get("c.submitNameApex");
      action.setCallback(this, function(response) {
        var state = response.getState();
        console.log(state);
        if (component.isValid() && state === "SUCCESS") {
          var toastEvent = $A.get("e.force:showToast");
          toastEvent.setParams({
            title: "Success!",
            message: "The names have been submitted successfully."
          });
          toastEvent.fire();
        }
      });
      $A.enqueueAction(action);
    }
  },

  /* handleChange:function(component,event,helper){ 
        var checked =component.get("v.checked");
        if(checked) {
            document.getElementById("disable_button").classList.remove("button--disabled");
        }
        else{
            document.getElementById("disable_button").classList.add("button--disabled");   
        }
    },*/

  handleFileUploadFinished: function(component, event) {
    // Get the list of uploaded files
    var uploadedFiles = event.getParam("files");
    var contentDocumentId = uploadedFiles[0].documentId;
    var fileName = uploadedFiles[0].name;
    console.log(contentDocumentId);
    var docMasterCode = event.getSource().get("v.name");
    //component.set("v.relationshipDocumentId",contentDocumentId);
    //component.set("v.relationshipDocMasterCode",docMasterCode);

    var mapDocValues = {};
    var docMap = component.get("v.docMasterContentDocMap");
    for (var key in docMap) {
      mapDocValues[key] = docMap[key];
    }

    mapDocValues[docMasterCode] = contentDocumentId;
    console.log(mapDocValues);

    //Setting the file name
    if (component.find(docMasterCode))
      component.find(docMasterCode).set("v.value", fileName);

    component.set("v.docMasterContentDocMap", mapDocValues);
    console.log(
      JSON.parse(JSON.stringify(component.get("v.docMasterContentDocMap")))
    );

    //alert("Files uploaded : " + uploadedFiles.length);
  },

  openModelDoc: function(component, event, helper) {
    component.set("v.isOpenDoc", true);
  },

  closeModelDoc: function(component, event, helper) {
    var a = component.get("c.navigateToNameCheckBlock");
    $A.enqueueAction(a);
    component.set("v.isOpenDoc", false);
  },

  //SendRequest Action
  closeModelDUP: function(component, event, helper) {
    //Validate the form
    var warningTypes = component.get("v.warningTypes");
    if (warningTypes.length > 0) {
      var docMap = component.get("v.docMasterContentDocMap");
      var isEvidenceDocRequired = component.get("v.isOpenDocRequired");
      var isEvidenceForIdenticalRequired = component.get("v.displayWarnDup");
      console.log(isEvidenceForIdenticalRequired);
      if (warningTypes.indexOf("Warning Message and Upload Doc") > -1) {
        //Check if file is uploaded
        if (isEvidenceDocRequired == "true" && !docMap) {
          helper.displayToast("Error", "Please provide the evidence", "error");
          return;
        }
      }
      if (warningTypes.indexOf("Warning Identical") > -1) {
        if (isEvidenceForIdenticalRequired == "true" && !docMap) {
          helper.displayToast("Error", "Please provide the evidence", "error");
          return;
        }
      }
    }
    var a = component.get("c.navigateToNameCheckBlock");
    $A.enqueueAction(a);
    component.set("v.isOpenDUP", false);
  },
  dismissModelDUP: function(component, event, helper) {
    component.set("v.isOpenDUP", false);
  },
  openModelDUP: function(component, event, helper) {
    var accountRecordId = event.currentTarget.dataset.recordid;
    component.set("v.relatedEntityId", accountRecordId);
    /*
        var value = component.get("v.valueTextEntity"); //event.target.name;
        console.log(value);
        var action1 = component.get("c.getDupName");
        action1.setParams({
            NameDup: value.trim()
        });
        action1.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var returnValDup = response.getReturnValue();
                component.set("v.showDupDelete", returnValDup);
            }
        });
        $A.enqueueAction(action1);
        */
    component.set("v.isOpenDUP", true);
  },
  // function automatic called by aura:waiting event
  showSpinner: function(component, event, helper) {
    // make Spinner attribute true for displaying loading spinner
    component.set("v.spinner", true);
  },

  // function automatic called by aura:doneWaiting event
  hideSpinner: function(component, event, helper) {
    // make Spinner attribute to false for hiding loading spinner
    component.set("v.spinner", false);
  },
  navigateToNameCheckBlock: function(component, event, helper) {
    try {
      //helper.show(component, event);
      console.log(event);

      //Validate the form
      var warningTypes = component.get("v.warningTypes");
      if (warningTypes.length > 0) {
        var docMap = component.get("v.docMasterContentDocMap");
        var isEvidenceDocRequired = component.get("v.isOpenDocRequired");
        //var isEvidenceForIdenticalRequired = component.get("v.displayWarnDup");
        //console.log(isEvidenceForIdenticalRequired);
        if (warningTypes.indexOf("Warning Message and Upload Doc") > -1) {
          //Check if file is uploaded
          if (isEvidenceDocRequired == "true" && !docMap) {
            helper.displayToast(
              "Error",
              "Please provide the evidence",
              "error"
            );
            return;
          }
        }
        /*
                if(warningTypes.indexOf("Warning Identical") > -1){
                    if(isEvidenceForIdenticalRequired == "true" && !docMap  ){
                        helper.displayToast('Error','Please provide the evidence');
                        return;
                    }
                }
                */
        if (warningTypes.indexOf("Warning Message and Reason Text") > -1) {
          if (!component.find("reason").get("v.value")) {
            helper.displayToast("Error", "Please provide the Reason", "error");
            return;
          }
        }
      }

      var selectedName = component.get("v.valueText");
      var warningMessage = JSON.stringify(component.get("v.warningMessages"));
      var relatedToEntity = component.get("v.relatedEntityId");
      relatedToEntity = relatedToEntity ? relatedToEntity : " ";

      let newURL = new URL(window.location.href).searchParams;
      var action = component.get("c.addEntityName");
      console.log(component.get("v.NameIdtoController"));
      console.log(selectedName);
      action.setParams({
        EntityName: selectedName,
        NameId: component.get("v.NameIdtoController"),
        warningMessage: warningMessage,
        srId: newURL.get("srId"),
        reason: component.get("v.reason"),
        relatedTo: relatedToEntity,
        relationText: component.get("v.relation"),
        docMap: component.get("v.docMasterContentDocMap")
        //contentDocId: component.get("v.relationshipDocumentId"),
        //docMasterCode: component.get("v.relationshipDocMasterCode")
      });
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (component.isValid() && state === "SUCCESS") {
          console.log(response.getReturnValue());
          var companyNameId = response.getReturnValue();
          console.log("success adding entity name" + companyNameId);
          console.log(newURL.get("srId"));
          console.log(newURL.get("pageId"));
          console.log(newURL.get("flowId"));
          var urlEvent = $A.get("e.force:navigateToURL");
          urlEvent.setParams({
            url:
              "/ob-searchentityname?srId=" +
              newURL.get("srId") +
              "&pageId=" +
              newURL.get("pageId") +
              "&flowId=" +
              newURL.get("flowId") +
              "&flowId=" +
              newURL.get("flowId")
          });

          window.location.href =
            "/" +
            $A.get("$Label.c.OB_Site_Prefix") +
            "/s/ob-searchentityname?srId=" +
            newURL.get("srId") +
            "&pageId=" +
            newURL.get("pageId") +
            "&flowId=" +
            newURL.get("flowId") +
            "&NameId=" +
            companyNameId;
        } else {
          console.log("Fail-->" + JSON.stringify(response.getError()));
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  },
  openEvidence: function(component, event, hepler) {
    component.set("v.isOpenDoc", "true");
  }
});