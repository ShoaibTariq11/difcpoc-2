({
  doInit: function(component, event, helper) {
    var addCss = component.get("v.AddOptionalCss");
    if (addCss == true) {
      var dataTable = component.find("relList");
      $A.util.addClass(dataTable, "optionalCSS");
    }

    helper.loadDataInit(component, event, helper);
  }
});