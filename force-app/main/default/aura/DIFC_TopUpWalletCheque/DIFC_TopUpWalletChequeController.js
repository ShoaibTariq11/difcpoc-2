({
	doInit : function(component, event, helper) {
		let newURL = new URL(window.location.href).searchParams;
		var srID = newURL.get('srId');
		var category = newURL.get("category") ? newURL.get("category") : '';
		component.set("v.amount",getUrlParameter('amount'));
		console.log('init cheque');
		if(srID && category){
			var action = component.get("c.loadPriceItem");
			action.setParams({
				"srId":srID,
				"category":category
			});
			action.setCallback(this, function(response) {
			
				var state = response.getState();
				if (state === "SUCCESS") {
					console.log("button succsess")
					var result = response.getReturnValue();
					component.set("v.recordTypeId",result.receiptRecordTypeId);
					component.set("v.amount",result.amount);
					component.find("amount").set("v.disabled",true);
					console.log(response.getReturnValue());
				}else if (state === 'ERROR') { // Handle any error by reporting it
					var errors = response.getError();
					console.log(response.getError());
					
					if (errors) {
						if (errors[0] && errors[0].message) {
							helper.displayToast('Error', errors[0].message);
						}
					} else {
						helper.displayToast('Error', 'Unknown error.');
					}
				}
			});
			$A.enqueueAction(action);
		}
		function getUrlParameter(name) {
			name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
			var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
			var results = regex.exec(location.search);
			return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
		};
	},
	onRecordSubmit: function(component, event, helper) {
		event.preventDefault(); // stop form submission
		var eventFields = event.getParam("fields");
		// Show error messages if required fields are blank
		var allValid = true;
		/*
		allValid = component.find('requiredField').reduce(function (validFields,inputCmp) {
			inputCmp.showHelpMessageIfInvalid();
			return validFields && inputCmp.get('v.validity').valid;
		}, true);
		console.log(allValid);
		*/
		if(allValid)
			component.find('chequeform').submit(eventFields);
		else{
			helper.displayToast('Error','Please fill the mandatory fields','error');
		}
	},
	navigateToPaymentSelection : function(component, event, helper) {
		/*
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/topupbalance",
        });
		urlEvent.fire();
		*/
		window.history.back();
	},
	onRecordSuccess : function(component, event, helper) {
		//Make price items to blocked.
		var record = event.getParam("response");
		console.log(JSON.parse(JSON.stringify(record.fields)));
		console.log(record.id);
		let newURL = new URL(window.location.href).searchParams;
		var srID = newURL.get('srId');
		var category = newURL.get("category") ? newURL.get("category") : '';
		if(srID && category){
			var action = component.get('c.updatePriceItem');
			action.setParams({
				"srId":srID,
				"category":category
			});
			action.setCallback(this, function(response) {
				//store state of response
				var state = response.getState();
				console.log(state);
				if (state === "SUCCESS") {
					var urlEvent = $A.get("e.force:navigateToURL");
					urlEvent.setParams({
						"url": '/paymentwalletconfirmation?srId='+srID+'&category='+category,
					});
					urlEvent.fire();
				}
				else if (state === 'ERROR') { // Handle any error by reporting it
					var errors = response.getError();
					console.log(response.getError());
					
					if (errors) {
						if (errors[0] && errors[0].message) {
							helper.displayToast('Error', errors[0].message,'error');
						}
					} else {
						helper.displayToast('Error', 'Unknown error.','error');
					}
				}
			});
			$A.enqueueAction(action);
		}

        
	}
})