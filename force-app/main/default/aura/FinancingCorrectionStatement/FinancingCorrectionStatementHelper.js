({
    fetchPickListVal: function(component, fieldName) {
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.authDebtorOrParty"),
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        label: "--- None ---",
                        value: ""
                    });
                }
                for(var i = 0; i < allValues.length; i++) {
                    opts.push({
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.set("v.countryOptions", opts);
            }
        });
        $A.enqueueAction(action);
    },
    
    renderState: function(component) {
        var currentStep = component.get("v.currentStep"),
            allSteps = component.get("v.steps"),
            render = [],
            state = "slds-is-complete";
        allSteps.forEach(function(step) {
            if(currentStep === step) {
                state = "slds-is-current";
            } else if(state === "slds-is-current") {
                state = "slds-is-incomplete";
            }
            render.push({ label: step, selected: state === "slds-is-current", state: state });
        });
        component.set("v.renderInfo", render);
    },
    
    fetchCustomerId : function(component,event) {
    	var action = component.get("c.userAccount");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var whereClause = "Customer__c='"+response.getReturnValue()+"'";
                component.set("v.whereCondition",whereClause);
            }
        });
        $A.enqueueAction(action);
    },
    
    clientInfo : function(component, event) {
    	var action = component.get("c.userAccountInfo");
        action.setParams({
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var userinformation = response.getReturnValue();
                component.set("v.accountName", userinformation[0]);
                component.set("v.accountAddress", userinformation[1]);
            }
        });
        $A.enqueueAction(action);
    },
    
    validateChildRequired: function(component, event) {
        var isValid = true;
        var newDebtor = component.get("v.newDebtor");
        var openDebtor = component.get("v.createNew");
        if(openDebtor && newDebtor.Organisation_Name__c == '' && newDebtor.Last_Name__c == '' && newDebtor.First_Name__c == '') {
        	isValid = false;
            alert('Organisation Name/Individual\'s name is required in Debtor Section!');
        } else if(openDebtor && newDebtor.Mailing_Address__c == '') {
            isValid = false;
            alert('Mailing Address should not be empty in Debtor Section!');
        }
        return isValid;
    },
    
    validateFormRequired: function(component, event) {
    	var isValid = true;
        var authRec = component.get("v.authDebtorOrParty");
        if(authRec.Organisation_Name__c == '' && authRec.Last_Name__c == '' && authRec.First_Name__c == '') {
        	isValid = false;
            alert('Organisation Name/Individual\'s name is required in Debtor/Secured Party Section!');
        } else if(authRec.Mailing_Address__c == '') {
            isValid = false;
            alert('Mailing Address should not be empty in Debtor/Secured Party Section!');
        }
        return isValid;
    },
    
    validateRequiredSRDocs: function(component, srId) {
    	component.set("v.isValidateOpen",true);
        var action = component.get("c.fetchApprovedForm");
        action.setParams({
            "recId": srId
        });
        action.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS") {
                var apForm = response.getReturnValue();
                console.log(apForm.Documents_Pending__c);
                if(apForm.Documents_Pending__c == true) {
                    component.set("v.documentsUploaded",false);
                    component.set("v.approvedForm.Documents_Pending__c",true);
                } else if(apForm.Documents_Pending__c == false) {
                    component.set("v.documentsUploaded",true);
                    component.set("v.approvedForm.Documents_Pending__c",false);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchSRPricingInfo : function(component, srId) {
        var action = component.get("c.PopulateSR_Pricing");
        action.setParams({
            "recId": srId
        });
        action.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS") {
                var responseWrapper = response.getReturnValue();
                console.log('--wrapper--');
                console.log(responseWrapper.lstSRPriceItems);
                console.log('--wrapper--');
                component.set("v.SRPricingInfo", responseWrapper);
            }
        });
        $A.enqueueAction(action);
    },
    
    loadSR : function(component, srId) {
    	var action = component.get("c.fetchSR");
        action.setParams({
            recId : srId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log(result);
                component.set("v.serviceRequest", result);
                component.set("v.approvedForm", result.Approved_Forms__r[0]);
                component.set("v.authtList", result.Debtors_SecuredParties__r);
            }
        });
        $A.enqueueAction(action);
    },
    
    loadApprovedRequestForm : function(component, srId) {
    	var action = component.get("c.fetchApprovedForm");
        action.setParams({
            recId : srId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.approvedForm", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})