({
    thirdpartyClientCaller : function(component,cardTokenn) { 
        console.log('==thirdpartyClientCaller method==');
        var xmlHttp = new XMLHttpRequest();
        var privateKey = $A.get("$Label.c.Frames_Private_Key");
        var email= component.get("v.email");
        var amount = component.get("v.amountToPay");
        var url = 'https://sandbox.checkout.com/api2/v2/charges/token';
        xmlHttp.open("POST", url, true);
        var cardToken = cardTokenn;
        console.log(amount);
        xmlHttp.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
        //xmlHttp.setRequestHeader('Authorization', 'sk_test_28ce4bef-24b3-4eac-ad21-d14383c179fa');
        xmlHttp.setRequestHeader('Authorization', privateKey);
        //xmlHttp.setRequestHeader('Authorization', 'sk_test_b300e550-60fa-43d8-a8dd-2f7a80b30df3');
        
        var jsonBody = '{"cardToken":"'+cardTokenn+'","currency":"AED","email":"'+email+'","value":"'+amount+'","autoCapture":"n","chargeMode":"2"}';
        xmlHttp.responseType = 'text';
        console.log('===jsonBody===='+jsonBody);
        xmlHttp.onload = function () {
            console.log(xmlHttp.readyState);
            console.log(xmlHttp.status);
            if (xmlHttp.readyState === 4) {
                console.log(xmlHttp.response);   
                var json = xmlHttp.response;
                var parsed = JSON.parse(json);
                component.set("v.listings", parsed);
                console.log("response inside the attribute listings "+JSON.stringify(component.get("v.listings"))); 
                var a = component.get("v.listings");
                
                var params = 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=600,height=300,left=100,top=100';
                
                //var newWindow =  window.open(a[0].redirectUrl,'test',params);
                window.open(a[0].redirectUrl,"_self");
                
                var payToken = a[0].id;
                console.log(payToken);
                /*
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                        "url": a[0].redirectUrl
                    });
                    urlEvent.fire();
                    */
                }
        };
        
        xmlHttp.send(jsonBody);
        
        /*Executed first*/
        console.log("Request sent");        
    },
    
    fetchPickListVal: function(component, fieldName, elementId) {
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.CompanyName"),
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                
                //console.log("opts is "+JSON.stringify(opts));
                //console.log("element id is "+elementId);
                //console.log("dom of element id is "+component.find(elementId));
                component.set("v.countryList", opts);
                //console.log("countryList is "+component.get("v.countryList"));
            }
        });
        $A.enqueueAction(action);
    },
   	createReceiptRecord : function(amountToPay,checkoutPaymentID,cardType){
    	console.log('=createReceiptRecord ======');
        var reqWrapPram  =
        {
            amount: amountToPay,  
            paymentType: 'Card',
            checkoutPaymentID: checkoutPaymentID,
            receiptStatus: 'Success',
            cardType:cardType
        }
        var action = component.get("c.makePayment");
    	action.setParams({
        "reqWrapPram": JSON.stringify(reqWrapPram)});
    	action.setCallback(this, function(response) {
       
            var state = response.getState();
            if (state === "SUCCESS") {
                               
                console.log('sucess create receipt');
            }else if (state === "INCOMPLETE") {
                alert('Response is Incompleted');
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    alert("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
    
})