({
    doInit:function(component, event, helper) {
        console.log($A.get("$Label.c.Frames_Public_Key"));
        component.set("v.amountToPay",getUrlParameter('amountToPay'));
        component.set("v.category",getUrlParameter('category'));
        let newURL = new URL(window.location.href).searchParams;
        var srID = newURL.get('srId');
        component.set("v.srId",srID); 
        console.log('amountPayedfddd'+getUrlParameter('srId') + component.get("v.srId"));
        component.set("v.email",$A.get("$SObjectType.CurrentUser.Email"));
       /* var action = component.get('c.getAmount');
        action.setParams({
            "recordId" : 'a390E0000007iWF'
        });
        action.setCallback(this, function(a){
            var state = a.getState(); // get the response state
            console.log('state'+state);
            if(state == 'SUCCESS') {
                console.log('a.123getReturnValue()'+a.getReturnValue());
                component.set("v.Tempamount",a.getReturnValue());
            }
        });
        $A.enqueueAction(action);*/
        helper.fetchPickListVal(component, 'Country_Of_Registration__c', 'accIndustryFirstName');
        function getUrlParameter(name) {
			name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
			var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
			var results = regex.exec(location.search);
			return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
		};

    },
    scriptsLoaded : function(component, event, helper) {
        var paymentForm  = document.getElementById('payment-form');
        var payNowButton = document.getElementById('pay-now-button');
        var cardTokenn;
        
        var style = {
            '.embedded .card-form .input-group': {
                borderRadius: '5px'
            },
            /* focus */
            '.embedded .card-form .input-group.focus:not(.error)': {
                border: '1px solid green'
            },
            /* icons */
            '.embedded .card-form .input-group .icon': {
                color: 'slategray'
            },
            /* hint icon (CVV) */
            '.embedded .card-form .input-group .hint-icon': {
                color: 'slategray'
            },
            '.embedded .card-form .input-group .hint-icon:hover': {
                color: 'darkslategray'
            },
            /* error */
            '.embedded .card-form .input-group.error': {
                border: '1px solid red'
            },
            '.embedded .card-form .input-group.error .hint.error-message': { /* message container */
                background: 'lightgray', /* make sure to match the pointer color (below) */
                color: 'red'
            },
            '.embedded .card-form .input-group.error .hint.error-message .arrow': { /* message container pointer */
                borderBottomColor: 'lightgray' /* matches message container background */
            },
            '.embedded .card-form .input-group.error .hint-icon:hover': { /* hint icon */
                color: 'red'
            },
            
            '.embedded .card-form .input-group:not(.error) input': {
                color: 'dimgray'
            },
            '.embedded .card-form .input-group.focus input': {
                color: 'black'
            },
            '.embedded .card-form .input-group.error input': {
                color: 'red'
            },
            
            '.embedded .card-form .split-view .left': {
                paddingRight: '3px'
            },
            '.embedded .card-form .split-view .right': {
                paddingLeft: '3px'
            }
        }
        
        Frames.init({
            
            publicKey: $A.get("$Label.c.Frames_Public_Key"),
            //publicKey: 'pk_test_0afbdf4d-416e-43ff-a087-e3c671d0d0cc',
            //publicKey:  'pk_test_dc66490a-47df-4f8b-ad13-3bba291e6801',
            containerSelector: '.frames-container',
            cardValidationChanged: function() {
                payNowButton.disabled = !Frames.isCardValid();
            },
            
            //https://docs.checkout.com/docs/shipping-and-billing-details
            descriptionTest: 'test',
            reference : component.get("v.srId"),
            cardholder : {
                name: "John Smith"},
            billingDetails: {
                "addressLine1": component.get("v.srId"),  //Address field line 1 — maximum length: 200 characters.
                "addressLine2": component.get("v.category"),       //Address field line 2 — maximum length: 200 characters.
                "postcode": component.get("v.srId"),                 //The city — maximum length: 50 characters.
                "country": "US",                       //The country — must be represented by an ISO2 country code (e.g. US).
                "city": "Schmittchester",              //The city — maximum length: 50 characters.
                "state": "Jakubowskiton",              //The state — maximum length: 50 characters.
                "descriptionTest":"test",
                "phone": {
                    "countryCode": "44",               //valid country code for the phone number (e.g. 44 for the United Kingdom).   
                    "number": "12345678"               //cardholder's contact phone number. Must be between 6 and 25 characters.
                }
            },
            
            style: style,
            
            cardTokenised: function(event) {
                cardTokenn = event.data.cardToken;   
                console.log('cardToken inside controller-- '+ cardTokenn);
                helper.thirdpartyClientCaller(component,cardTokenn);
                Frames.addCardToken(paymentForm, cardTokenn);
                //paymentForm.submit();
                
            },
            
            cardSubmitted: function() {
                payNowButton.disabled = true;
            },
        }); 
        
        paymentForm.addEventListener('submit', function(event) {
            event.preventDefault();
            Frames.submitCard();
        }); 
        
    },
    
    closeModel: function(component, event, helper) { 
        component.set("v.isOpen", false);
    },
    component2Event : function(cmp, event) { 
        //Get the event amount attribute
        console.log('Inside amount');
        var amountEntered = event.getParam("amount"); 
        //Set the handler attributes based on event data 
        cmp.set("v.amount", amountEntered);         
    },
    
    cancelName: function(component, event, helper) { 
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/"
        });
        urlEvent.fire();
    }
})