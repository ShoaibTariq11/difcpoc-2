({
   //Redirection to the index page (Pathway page) from the Register page
	goToPathway : function(component, event, helper){
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/difc-indexpage",
        });
        urlEvent.fire();
    }
    
})