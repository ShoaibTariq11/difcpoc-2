({
    thirdpartyClientCaller : function(component,cardTokenn) { 
        console.log('==thirdpartyClientCaller method==');
        var xmlHttp = new XMLHttpRequest();
        var privateKey = $A.get("$Label.c.Frames_Private_Key");
        var email= component.get("v.email");
        var amount = component.get("v.amountToPay");
        var url = $A.get("$Label.c.OB_Frames_Payment_URL")+'/payments';
        
        xmlHttp.open("POST", url, true);
        var cardToken = cardTokenn;
        console.log(amount);
        xmlHttp.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
        xmlHttp.setRequestHeader('Authorization', privateKey);
        
        amount = amount * 100;
        if(amount)
            amount = Math.round(amount);
        var customerName = component.get("v.currentUser.Contact.Name") + '-' + component.get("v.currentUser.Contact.Account.BP_No__c");
        var billingAddressJSON = '{"address":{'+
                                    '"address_line1":"'+component.get("v.addressLine1")+'",'+
                                    '"address_line2":"'+component.get("v.addressLine2")+'",'+
                                    '"city":"'+component.get("v.cityTown")+'",'+
                                    '"state":"'+component.get("v.stateProvince")+'",'+
                                    '"zip":"'+component.get("v.postalCode")+'",'+ 
                                    '"country":"'+component.get("v.country")+'"'+
                                '}}';
                                
        var customerDetailJSON = '{'+
                                    '"email":"'+email+'",'+
                                    '"name":"'+customerName+'"'+
                                '}';
        
        var jsonBody = '{"source": '+
            '{"type": "token","token":"'+ cardToken+'"},'+
            '"shipping":'+billingAddressJSON + ','+
            '"customer":'+customerDetailJSON + ','+
            '"amount":'+ amount+','+
            '"currency": "AED",'+
            '"3ds": {"enabled": true},'+
            '"metadata":{'+
                '"category":"'+component.get("v.category")+'",'+
                '"receiptReference":"'+component.get("v.receiptId")+'",'+
            '},'+
            '"reference": "'+component.get("v.srId")+'"'+
        '}';
        
        xmlHttp.responseType = 'text';
        
        xmlHttp.onload = function () {
            if (xmlHttp.readyState === 4) {
                var json = xmlHttp.response;
                var parsed = JSON.parse(json);
                component.set("v.listings", parsed);
                //console.log("response inside the attribute listings "+JSON.stringify(component.get("v.listings"))); 
                var a = component.get("v.listings");
                
                if(parsed._links){
                    var redirectLinkJSON = parsed._links;
                    window.open(redirectLinkJSON.redirect.href,"_self");
                }
                else{
                    helper.displayToast('Error', JSON.stringify(component.get("v.listings"), 'error'));
                }
                var payToken = a[0].id;
            }
        };
        //Sends the request to Payment Gateway
        xmlHttp.send(jsonBody);    
    },
    /**
     * Display a message
     */
    displayToast : function (title, message, type) {
		console.log('displayToast');
        var toast = $A.get('e.force:showToast');

        // For lightning1 show the toast
        if (toast) {
            //fire the toast event in Salesforce1
            toast.setParams({
                'type': type,
                'title': title,
                'message': message
            });

            toast.fire();
        } else { // otherwise throw an alert        
            alert(title + ': ' + message);
        }
	},
    fetchCountry: function(component) {
        console.log('fetchCountry');
        var action = component.get("c.getCountryValues");
        var opts = [];
        action.setCallback(this, function(response) {
            console.log(response.getReturnValue());
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if (allValues) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                for(var key in allValues) {
                //for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[key],
                        value: key
                    });
                }
                
                component.set("v.countryList", opts);
            }
            else if (state === "INCOMPLETE") {
                alert('Response is Incompleted');
            }else if (state === "ERROR") {
                var errors = response.getError();
                console.log(errors);
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    alert("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        console.log('done');
    },
   	createReceiptRecord : function(amountToPay,checkoutPaymentID,cardType){
    	console.log('=createReceiptRecord ======');
        var reqWrapPram  =
        {
            amount: amountToPay,  
            paymentType: 'Card',
            checkoutPaymentID: checkoutPaymentID,
            receiptStatus: 'Success',
            cardType:cardType
        }
        var action = component.get("c.makePayment");
    	action.setParams({
        "reqWrapPram": JSON.stringify(reqWrapPram)});
    	action.setCallback(this, function(response) {
       
            var state = response.getState();
            if (state === "SUCCESS") {
                               
                console.log('sucess create receipt');
            }else if (state === "INCOMPLETE") {
                alert('Response is Incompleted');
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    alert("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
    
})