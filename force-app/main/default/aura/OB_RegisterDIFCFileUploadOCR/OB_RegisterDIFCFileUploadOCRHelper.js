({
  loadSRDocs: function(component, event, helper) {
    let newURL = new URL(window.location.href).searchParams;
    //var srId = component.get("v.srId");
    var srId = component.get("v.srId")
      ? component.get("v.srId")
      : newURL.get("srId");

    var documentMaster = component.get("v.documentMaster");
   
    console.log(
      JSON.parse(JSON.stringify(component.get("v.docMasterContentDocMap")))
    );
    var docMasterContentDocMap = component.get("v.docMasterContentDocMap");
    var action = component.get("c.viewSRDocs");
    var reqWrapPram = {
      srId: srId,
      docMasterCode: documentMaster,
      docMasterContentDocMap: docMasterContentDocMap
    };
    // docMasterContentDocMap : docMasterContentDocMap
    action.setParams({
      reqWrapPram: JSON.stringify(reqWrapPram)
    });

    // set call back
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var result = response.getReturnValue();
        console.log("in load srDocs>>");
        console.log(JSON.stringify(result));

        var mapSRDocs = result.mapSRDocs;
        console.log("$$$$$$$ mapSRDocs ", mapSRDocs);
        if (mapSRDocs && mapSRDocs[documentMaster]) {
          component.set("v.fileName", mapSRDocs[documentMaster]);
          //requiredDocument
          // if the file is there then no need of required check.
          component.set("v.requiredDocument", false);
        }
      } else if (state === "INCOMPLETE") {
        //alert("From server: " + response.getReturnValue());
      } else if (state === "ERROR") {
        var errors = response.getError();
        if (errors) {
          if (errors[0] && errors[0].message) {
            helper.displayToast("Error", errors[0].message);
            console.log("Error message: " + errors[0].message);
          }
        } else {
          console.log("Unknown error");
        }
      }
    });
    // enqueue the action
    $A.enqueueAction(action);
  },

  handleUploadFinished: function(cmp, event, helper) {
    // Get the list of uploaded files
    var uploadedFile = event.getParam("files")[0];

    //reparenting code.
    console.log("$$$$$$$$$ secDetId ");
    console.log("$$$$$$$$$ secDetId ", event.getSource().get("v.class"));
    var secDetId = event.getSource().get("v.class");

    if (!secDetId) {
      //alert("Missing Section Detail Configuration.");
      return;
    }
    if (!uploadedFile) {
      //alert("Missing File Details.");
      return;
    }

    var documentIdParam = uploadedFile.documentId;
    //Modified by Rajil
    if (documentIdParam) cmp.set("v.fileUploaded", true);

    /*Modified by : Rajil
     *Store the content id in Map
     */
    var mapDocValues = {};
    console.log(event.getSource().get("v.name"));
    var docMasterCode = event.getSource().get("v.name");
    var docMapEvent = cmp.getEvent("docMapEvent");
    mapDocValues[docMasterCode] = documentIdParam;
    console.log("mapDocValues>>>>>>>");
    console.log(mapDocValues);
    cmp.set("v.docMasterContentDocMap", mapDocValues);
    docMapEvent.setParams({
      docMap: mapDocValues
    });
    docMapEvent.fire();
    /**Ends here  */

    var fileName = uploadedFile.name;
    console.log("-====documentIdParam=======" + documentIdParam);
    console.log("-====fileName=======" + fileName);
    cmp.set("v.fileName", fileName);

    try {
      var getFormAction = cmp.get("c.reparentToSRDocs");
      let newURL = new URL(window.location.href).searchParams;

      var flowIdParam = cmp.get("v.flowId")
        ? cmp.get("v.flowId")
        : newURL.get("flowId");
      var pageIdParam = cmp.get("v.pageId")
        ? cmp.get("v.pageId")
        : newURL.get("pageId");
      var srIdParam = cmp.get("v.srId")
        ? cmp.get("v.srId")
        : newURL.get("srId");

      var reqWrapPram = {
        flowId: flowIdParam,
        pageId: pageIdParam,
        srId: srIdParam,
        documentId: documentIdParam,
        secObjID: secDetId
      };

      getFormAction.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram)
      });

      console.log(
        "@@@@@@@@@@33 reqWrapPram init " + JSON.stringify(reqWrapPram)
      );
      getFormAction.setCallback(this, function(response) {
        var state = response.getState();
        console.log("callback state file state: " + state);

        if (cmp.isValid() && state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(">Wrap.....>");
          console.log(respWrap);

          var secItmWrap = respWrap.secItmWrap;
          //cmp.set('v.fileUploaded',true); //Commented by Rajil
          //v.requiredText
          cmp.set("v.requiredText", "");

          //Displaying file upload name.
          console.log(
            "####### secItmWrap.sectionDetailOb.Id ",
            secItmWrap.sectionDetailObj.Id
          );
          console.log(
            "####### secItmWrap.sectionDetailOb.Parse_File__c ",
            secItmWrap.sectionDetailObj.Parse_File__c
          );
          console.log("####### documentIdParam ", documentIdParam);

          //Calling OCR.
          //check is parse check remaining.

          helper.callOCR(cmp, event, helper, documentIdParam, secItmWrap);
        } else {
          console.log(JSON.parse(JSON.stringify(response.getError())));
          console.log(response.getError());
          console.log("@@@@@ Error " + response.getError()[0].message);
          //alert('Error  ',response.getError()[0].message);
        }
      });
      $A.enqueueAction(getFormAction);
    } catch (err) {
      console.log("=error===" + err.message);
    }
  },
  callOCR: function(cmp, event, helper, documentIdParam, secItmWrap) {
    //Calling OCR comes here.....parseFile == true.... remaining
    if (secItmWrap.sectionDetailObj.Parse_File__c && documentIdParam) {
      console.log("==call OCR===");
      helper.showSpinner(cmp, event, helper);
      var OCRAction = cmp.get("c.processOCR");
      var sObjectToProcess = cmp.get("v.sObjectToProcess");
      console.log("@@@@@@@@@@ sObjectToProcess ", sObjectToProcess);
      OCRAction.setParams({
        fileId: documentIdParam,
        sObjectToProcess: sObjectToProcess
      });

      OCRAction.setCallback(this, function(response) {
        console.log(
          "@@@@@@@@@@@@ respOCR ",
          JSON.stringify(response.getReturnValue())
        );
        var state = response.getState();

        if (cmp.isValid() && state === "SUCCESS") {
          /*var ocrWrapper = response.getReturnValue();
                                                ocrWrapper = ocrWrapper.mapFieldApiObject;
                                                console.log('ocrWrapper===>'+JSON.stringify(ocrWrapper));
                                                
                                                var secWrapLst = cmp.get('v.secWrapLst');
                                                secWrapLst.forEach(secWrap);
                                                function secWrap(sec, index) {
                                                   
                                                    if(!$A.util.isEmpty(sec.SIWraps)){
                                                        
                                                        sec.SIWraps.forEach(secItemWrap);
                                                        function secItemWrap(secItem, itemIndex) 
                                                        {
                                                            if(!$A.util.isEmpty(secItem.FieldApi) && !$A.util.isEmpty(ocrWrapper[secItem.FieldApi.toLowerCase()])){
                                                                secItem.FieldValue = ocrWrapper[secItem.FieldApi.toLowerCase()];
                                                                console.log('secItem.FieldValue==>'+secItem.FieldValue);
                                                            }
                                                        }
                                                    }
                                                }
                                                cmp.set('v.secWrapLst',secWrapLst);
                                                helper.hideSpinner(cmp, event, helper);
                                              */

          helper.hideSpinner(cmp, event, helper);
          console.log(
            "@@@@@@@@@@@@ respOCR ",
            JSON.stringify(response.getReturnValue())
          );
          var result = JSON.stringify(response.getReturnValue());
          if (result != "null" && !$A.util.isEmpty(result)) {
            var ocrEvent = cmp.getEvent("ocrEvent");
            ocrEvent.setParams({
              ocrWrapper: response.getReturnValue()
            });
            ocrEvent.fire();
          }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          helper.hideSpinner(cmp, event, helper);
          //alert('Error  ',response.getError()[0].message);
        }
      });
      $A.enqueueAction(OCRAction);
    } else {
      console.log("=no ocr creation==" + parseFile);
      helper.hideSpinner(cmp, event, helper);
    }
  },
  // function automatic called by aura:waiting event
  showSpinner: function(component, event, helper) {
    // make Spinner attribute true for displaying loading spinner
    component.set("v.spinner", true);
  },

  // function automatic called by aura:doneWaiting event
  hideSpinner: function(component, event, helper) {
    // make Spinner attribute to false for hiding loading spinner
    component.set("v.spinner", false);
  }
});