({
    doInit : function(component, event, helper) {
        var progress = [];
        component.set("v.finalProgress",progress);
        //get History
        var visitHistory=component.get("c.getVisitHistory");
        visitHistory.setParams({
            "recordId": component.get("v.recordId")
        });
        console.log('visitHistory---'+visitHistory);
        visitHistory.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var progress = component.get("v.finalProgress");
                if(response.getReturnValue().length > 0){
                    component.set("v.historyList", response.getReturnValue());
                    component.set("v.finalProgress",response.getReturnValue());
                    var linkedClassName = 'previousLink';
                    var classvalueName = 'slds-is-complete';
                    for(var i=0;i<response.getReturnValue().length;i++)
                    {
                        console.log('response.getReturnValue()[i].OldValue ??? '+ response.getReturnValue()[i].name);
                        //alert('response.getReturnValue()[i].OldValue ?????  ' + response.getReturnValue()[i].OldValue);
                        if(response.getReturnValue()[i].name === undefined){
                          // no value..
                        }
                        else{
                            if(response.getReturnValue()[i].selected ==='true'){
                               linkedClassName = 'currentLink';
                                classvalueName = 'slds-is-current slds-is-active';
                            }else{
                                linkedClassName = 'previousLink';
                                classvalueName = 'slds-is-complete';
                            }                           
                            progress.push({                                
                                value: response.getReturnValue()[i].name,
                                value1 : $A.util.isEmpty(response.getReturnValue()[i].approvedDate) ? 'Submitted : ' + response.getReturnValue()[i].submittedDate : ('Submitted : '+ response.getReturnValue()[i].submittedDate+'\n'+ 'Approved : '+ response.getReturnValue()[i].approvedDate),
                                classvalue:classvalueName,
                                linkClass:linkedClassName,
                                selected:response.getReturnValue()[i].selected
                           });  
                        }    
                    }
                }else{
                    
                }
                component.set("v.finalProgress",progress);
            }
            else
                console.log('Error LAccountInvoiceDetailController:doInit:action');
        });
        $A.enqueueAction(visitHistory);       
    },
    refreshProgressbar : function(component , event , helper){
}
})