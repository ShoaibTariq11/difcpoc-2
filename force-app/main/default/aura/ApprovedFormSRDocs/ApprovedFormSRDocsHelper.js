({
	fetchSRDocuments : function(component, event) {
        var recId; 
        if(component.get("v.recordId") != null) {
            recId = component.get("v.recordId");
        } else {
            recId = component.get("v.srId");
        }
		var action = component.get("c.fetchSRDocs");
        action.setParams({
            "serviceRequestId": recId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.srDocs", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	}
})