({
    doInIt: function(component, event, helper) {
        helper.fetchSRDocuments(component,event);
    },
    
    uploadFile: function(component, event, helper) {
        var nameNRecId = event.getSource().get("v.value");
        //var nameNRecId = event.target.id;
        var totalStr = nameNRecId.split('_');
        component.set("v.srDocId",totalStr[0]);
        component.set("v.docName",totalStr[1]);
        component.set("v.doupload",totalStr[2]);
        component.set("v.isOpen", true);
    },
    
    previewFile: function(component, event, helper) {
        var nameNAttachmentId = event.getSource().get("v.value");
        //var nameNAttachmentId = event.target.id;
        component.set("v.hasModalOpen",true);
        component.set("v.selectedDocumentId",nameNAttachmentId)
		/*$A.get('e.lightning:openFiles').fire({
		    recordIds: [nameNAttachmentId]
		});*/
	},
    
    closeModel: function(component, event, helper) {
        component.set("v.hasModalOpen", false);
        component.set("v.isOpen", false);
        component.set("v.selectedDocumentId", null); 
    },
    
    cancelUpload: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
    },
    
    handleUploadFinished: function (component, event, helper) {
        var comments = component.get("v.comments");
        var docId = component.get("v.srDocId");
        console.log('--docId--');
        console.log(docId);
        console.log('--docId--');
        // This will contain the List of File uploaded data and status
        var uploadedFiles = event.getParam("files");
        var documentId = uploadedFiles[0].documentId;
        var action = component.get("c.saveSRDoc");
        action.setParams({
            srDocId :  docId,
            comments : comments,
            attachmentId : documentId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                helper.fetchSRDocuments(component,event);
                component.set("v.isOpen", false);
                component.getEvent("refreshParent").fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    saveFile: function(component, event, helper) {
        component.set("v.isOpen", false);
    }
})