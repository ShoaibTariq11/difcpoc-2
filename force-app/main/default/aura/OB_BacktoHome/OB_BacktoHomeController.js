({
  doInit: function (component, event, helper) {
    let newURL = new URL(window.location.href).searchParams;
    var srId = newURL.get("srId");

    if (srId) {
      var action = component.get("c.getSrInfo");

      var requestWrap = {
        srId: srId,
      };
      action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(respWrap);
          component.set("v.respWrap", respWrap);
          if (
            respWrap.srObj &&
            respWrap.srObj.RecordType &&
            respWrap.srObj.RecordType.DeveloperName == "Superuser_Authorization"
          ) {
            component.set("v.labelValue", "Back to manage users");
            component.set(
              "v.recordType",
              respWrap.srObj.HexaBPM__Record_Type_Name__c
            );
          }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log(
            "@@@@@ Error Location " + response.getError()[0].stackTrace
          );
        }
      });
      $A.enqueueAction(action);
    }
  },
  backtohome: function (component, event, helper) {
    try {
      var recordType = component.get("v.recordType");
      var resp = component.get("v.respWrap");
      if (recordType && recordType == "Superuser_Authorization") {
        window.location.href = "manage-users";
        return;
      }

      var path = window.location.pathname;
      if (
        resp &&
        resp.srObj &&
        resp.srObj.HexaBPM__Record_Type_Name__c ==
          "Commercial_Permission_Renewal" &&
        !path.includes("ob-processflow") &&
        !path.includes("payment-Confirmation") &&
        !(
          resp.srObj.HexaBPM__Internal_Status_Name__c == "Approved" ||
          resp.srObj.HexaBPM__Internal_Status_Name__c.toUpperCase() ==
            "SUBMITTED"
        )
      ) {
        console.log(resp.srObj.HexaBPM__Record_Type_Name__c);
        window.location.href = "ob-processflow" + resp.cpLInk;
        return;
      }

      if (
        resp &&
        resp.srObj &&
        resp.srObj.HexaBPM__Record_Type_Name__c ==
          "Commercial_Permission_Renewal" &&
        path.includes("topupbalance")
      ) {
        console.log("topupbalance");
        window.location.href = "ob-processflow" + resp.cpLInk;
        return;
      }

      var urlEvent = $A.get("e.force:navigateToURL");
      var pageURL = component.get("v.pageURL");
      console.log(pageURL);
      var paramValue = component.get("v.param");
      console.log(">" + paramValue);
      console.log(paramValue);
      if (paramValue) {
        var paramString = pageURL + "?";
        var paramArr = paramValue.split(";");
        console.log(paramArr);
        for (var i = 0; i < paramArr.length; i++) {
          paramArr[i] = paramArr[i]
            .replace(/[\[]/, "\\[")
            .replace(/[\]]/, "\\]");
          var regex = new RegExp("[\\?&]" + paramArr[i] + "=([^&#]*)");
          var results = regex.exec(location.search);
          var resValue =
            results === null
              ? ""
              : decodeURIComponent(results[1].replace(/\+/g, " "));
          paramString = paramString + paramArr[i] + "=" + resValue + "&";
        }
        console.log(paramString);
        pageURL = paramString;
      }
      console.log(pageURL);
      urlEvent.setParams({
        url: pageURL,
      });
      urlEvent.fire();
    } catch (error) {
      console.log(error.message);
    }
  },
});