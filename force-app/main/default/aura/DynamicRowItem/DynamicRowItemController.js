({
    doInit : function(component, event, helper) {
        helper.fetchPickListVal(component, 'Country__c');
        var recType = component.get("v.typeOfRecord");
        console.log('--record type--');
        console.log(recType);
        console.log('--record type--');
        component.set("v.DebtorInstance.Type__c",recType);
        var rowIndex = parseInt(component.get("v.rowIndex"))+1;
        component.set("v.indexNumber",rowIndex);
    },
    
    onChange: function (component, event, helper) {
        var selectedValue = component.find("selectType").get("v.value");
        if(selectedValue == 'Organisation') {
            component.set("v.DebtorInstance.First_Name__c","");
            component.set("v.DebtorInstance.Last_Name__c","");
            component.set("v.DebtorInstance.Middle_Name__c","");
            component.set("v.DebtorInstance.Suffix__c","");
        } else if(selectedValue == 'Individual') {
            component.set("v.DebtorInstance.Organisation_Name__c","");
            component.set("v.DebtorInstance.Type_of_Organisation__c","");
            component.set("v.DebtorInstance.Jurisdiction_of_Organisation__c","");
            component.set("v.DebtorInstance.Organisational_ID__c","");
        }
        component.set("v.debtorType",selectedValue);
    },
    
    handleCountryChange: function(component, event) {
        // This will contain the string of the "value" attribute of the selected option
        //var selectedOptionValue = event.getParam("value");
        var selectedOptionValue = component.find('selectCountry').get('v.value');
        component.set("v.DebtorInstance.Country__c",selectedOptionValue);
    },
    
    saveDebtorRow : function(component, event, helper) {
        var newDebtor = component.get("v.DebtorInstance");
        var selectedValue = component.find("selectType").get("v.value");
        if(selectedValue == '') {
            alert('Please select Type of Debtor!');
        } else if(selectedValue == 'Organisation' && (newDebtor.Organisation_Name__c == '' || newDebtor.Type_of_Organisation__c == '' || newDebtor.Jurisdiction_of_Organisation__c == '' || newDebtor.Organisational_ID__c == '')) {
        	alert('All fields are required when Organisation is selected in Debtor section!');
        } else if(selectedValue == 'Individual' && (newDebtor.First_Name__c == '' || newDebtor.Last_Name__c == '')) {
        	alert('First Name and Last Name are required in Debtor Section');
        } else if(newDebtor.Mailing_Address__c == '') {
            alert('Mailing Address is required in Debtor Section!');
        } else if(newDebtor.City__c == '') {
            alert('City is required in Debtor Section!');
        } else if(newDebtor.Country__c == '') {
            alert('Country is required in Debtor Section!');
        } else {
            var allValid = component.find('fieldId').reduce(function (validSoFar, inputCmp) {
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && !inputCmp.get('v.validity').valueMissing;
            }, true);
            if(allValid) {
                // fire the AddNewRowEvt Lightning Event 
                component.getEvent("SaveDebtorRowEvt").fire();
            } else {
                alert('Please update the invalid form entries and try again.');
            }
        }
    },
    
    cancelDebtorRow : function(component, event, helper) {
    	// fire the CancelRowEvt Lightning Event 
        component.getEvent("CancelDebtorRowEvt").fire();
    },
    
    savePartyRow : function(component, event, helper) {
        var newDebtor = component.get("v.DebtorInstance");
        var selectedValue = component.find("selectType").get("v.value");
        if(selectedValue == '') {
            alert('Please select Type of Party!');
        } else if(selectedValue == 'Organisation' && (newDebtor.Organisation_Name__c == '' || newDebtor.Type_of_Organisation__c == '' || newDebtor.Jurisdiction_of_Organisation__c == '' || newDebtor.Organisational_ID__c == '')) {
        	alert('All fields are required when Organisation is selected in Party section!');
        } else if(selectedValue == 'Individual' && (newDebtor.First_Name__c == '' || newDebtor.Last_Name__c == '')) {
        	alert('First Name and Last Name are required in Party Section');
        } else if(newDebtor.Mailing_Address__c == '') {
            alert('Mailing Address is required in Party Section!');
        } else if(newDebtor.City__c == '') {
            alert('City is required in Party Section!');
        } else if(newDebtor.Country__c == '') {
            alert('Country is required in Party Section!');
        } else {
            // fire the AddNewRowEvt Lightning Event
            component.getEvent("SavePartyRowEvt").fire();
        }
    },
    
    cancelPartyRow : function(component, event, helper) {
    	// fire the CancelRowEvt Lightning Event 
        component.getEvent("CancelPartyRowEvt").fire();
    },
    
    removeRow : function(component, event, helper) {
        // fire the DeleteRowEvt Lightning Event and pass the deleted Row Index to Event parameter/attribute
        component.getEvent("DeleteRowEvt").setParams({"indexVar" : component.get("v.rowIndex") }).fire();
    }
})