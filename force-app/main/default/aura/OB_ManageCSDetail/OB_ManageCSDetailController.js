({
  doInit: function (cmp, event, helper) {
    try {
      console.log(cmp.get("v.srWrap"));
      let newURL = new URL(window.location.href).searchParams;
      var flowIdParam = cmp.get("v.flowId")
        ? cmp.get("v.flowId")
        : newURL.get("flowId");
      var srIdParam = cmp.get("v.srId")
        ? cmp.get("v.srId")
        : newURL.get("srId");
      var pageIdParam = cmp.get("v.pageId")
        ? cmp.get("v.pageId")
        : newURL.get("pageId");
      // set if comming from url
      cmp.set("v.pageId", pageIdParam);
      cmp.set("v.flowId", flowIdParam);
      cmp.set("v.srId", srIdParam);

      console.log("####@@@@@@ pageId ", pageIdParam);
      console.log("####@@@@@@ flowIdParam ", flowIdParam);
      console.log("####@@@@@@ srIdParam ", srIdParam);

      var amedWrapperResult = JSON.parse(JSON.stringify(cmp.get("v.amedWrap")));
      console.log("init cs detail");
      console.log(JSON.parse(JSON.stringify(cmp.get("v.amedWrap"))));
      console.log(cmp.get("v.amedWrap.amedObj"));
      console.log(cmp.get("v.reAssign"));
      console.log(cmp.get("v.amedWrap.amedObj.Id"));
      cmp.set("v.amendId", cmp.get("v.amedWrap.amedObj.Id"));
      cmp.set("v.hasOtherRoles", cmp.get("v.amedWrap.hasOtherRoles"));
      cmp.set("v.declaration", cmp.get("v.amedWrap.declaration"));
      var reAssignBool = cmp.get("v.reAssign");
      var hidePEPFieldForBusinessSector = [
        "legal services",
        "prescribed companies",
        "foundation",
        "single family office",
      ];
      console.log(reAssignBool);
      if (reAssignBool) {
        console.log("reAssignBool inside");
        helper.setRecordTypeChange(cmp, event);
      }
      var businessSector = amedWrapperResult.serviceRequest.Business_Sector__c
        ? amedWrapperResult.serviceRequest.Business_Sector__c.toLowerCase()
        : "";
      if (
        (amedWrapperResult.serviceRequest.Entity_Type__c &&
          amedWrapperResult.serviceRequest.Entity_Type__c.toLowerCase() ==
            "financial - related") ||
        amedWrapperResult.serviceRequest.DNFBP__c ||
        hidePEPFieldForBusinessSector.indexOf(businessSector) > -1
      ) {
        cmp.set("v.showPEPField", "false");
      }
      //load SRDocs
      // Commented by Merul.
      //helper.viewSRDocs(cmp, event);
    } catch (error) {
      console.log(error.message);
    }
  },
  onChangeCertifiedPassportCopy: function (cmp, event, helper) {
    //var amedWrap = cmp.get("v.amedWrap");
    var certifiedPassportCopy = cmp.get(
      "v.amedWrap.amedObj.Certified_passport_copy__c"
    );

    if (certifiedPassportCopy == "No") {
      var toastEvent = $A.get("e.force:showToast");
      toastEvent.setParams({
        title: "Information",
        message: "Please visit DIFC for citation of original passport.",
      });
      toastEvent.fire();
      return;
    }
  },
  handleCreateLoad: function (cmp, event, helper) {
    try {
      var hasOtherRolesAlongWithCS = cmp.get("v.hasOtherRoles");
      var reAssignBool = cmp.get("v.reAssign");
      var amedWrap = cmp.get("v.amedWrap");

      console.log(reAssignBool);
      console.log(
        "######### amedWrap.disableAllFlds ",
        amedWrap.disableAllFlds
      );

      if (
        (reAssignBool || hasOtherRolesAlongWithCS) &&
        amedWrap.disableAllFlds
      ) {
        helper.disableAllFields(cmp, event);
      }
    } catch (error) {
      log(error.message);
    }
  },
  handleLookupEvent: function (component, event, helper) {
    console.log("handleLookupEvent");
    var accObj = event.getParam("sObject");
    console.log("lookup--" + JSON.stringify(accObj));
    component.set(
      "v.amedWrap.amedObj.Registration_No__c",
      accObj.Registration_License_No__c
    );
  },
  onAmedSave: function (cmp, event, helper) {
    console.log("######### onAmedSave ");
    console.log(cmp.get("v.recordtypename"));
    console.log("isIndividual" + cmp.get("v.isIndividual"));
    console.log("isCorporate" + cmp.get("v.isCorporate"));
    var recordtyeindi = cmp.get("v.isIndividual");
    var recordtypecor = cmp.get("v.isCorporate");
    var reAssignBool = cmp.get("v.reAssign");
    console.log(reAssignBool);

    var allValid = true;
    //Add validation only for new/existing records - not required for reassign role.
    if (reAssignBool == false) {
      var requiredFields = cmp.find("requiredField");
      if (requiredFields && Array.isArray(requiredFields)) {
        for (var key in requiredFields) {
          console.log(key);
          if (requiredFields[key]) {
            console.log(requiredFields[key]);
            var inputFieldCompValue = requiredFields[key].get("v.value");
            if (
              !inputFieldCompValue &&
              requiredFields[key] &&
              requiredFields[key].get("v.required")
            ) {
              console.log("no value");
              //console.log('bad');
              requiredFields[key].reportValidity();
              allValid = false;
            }
          }
        }
      } else {
        if (
          requiredFields &&
          requiredFields.get("v.required") &&
          !requiredFields.get("v.value")
        ) {
          requiredFields.reportValidity();
          allValid = false;
        }
      }
      console.log(allValid);

      //Validating lookups
      console.log("lookup");
      console.log(cmp.get("v.recordtypename"));
      if (cmp.get("v.recordtypename") == "Body_Corporate") {
        console.log("inside validation ");
        if (cmp.find("lookup")) {
          var lookupValue = cmp.find("lookup").get("v.selectedValue");
          console.log(lookupValue);
          if (!lookupValue) {
            allValid = false;
          }
        }
      }

      console.log("About to statrt file validation. ");
      var fileError = helper.fileUploadHelper(cmp, event, helper);
      if (fileError) {
        allValid = false;
      }
    }

    if (
      allValid &&
      cmp.get("v.amedWrap.amedObj.I_agree_Company_Secretary__c")
    ) {
      console.log("-----no error---");

      var onAmedSave = cmp.get("c.onAmedSaveDB");

      let newURL = new URL(window.location.href).searchParams;
      var srId = cmp.get("v.srId") ? cmp.get("v.srId") : newURL.get("srId");
      var reqWrapPram = {
        srId: srId,
        amedWrap: cmp.get("v.amedWrap"),
        recordtypename: cmp.get("v.recordtypename"),
        isReassign: reAssignBool,
        docMap: cmp.get("v.docMasterContentDocMap"),
      };

      onAmedSave.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram),
      });
      console.log(reqWrapPram);
      console.log(
        JSON.parse(JSON.stringify(cmp.get("v.docMasterContentDocMap")))
      );
      console.log(JSON.parse(JSON.stringify(cmp.get("v.amedWrap"))));
      onAmedSave.setCallback(this, function (response) {
        var state = response.getState();

        if (cmp.isValid() && state === "SUCCESS") {
          var respWrap = response.getReturnValue();

          if (!$A.util.isEmpty(respWrap.errorMessage)) {
            console.log("=====error on sve====" + respWrap.errorMessage);
            cmp.set("v.spinner", false);
            //this.showToast("Error",respWrap.errorMessage);
            cmp.set("v.errorMessage", respWrap.errorMessage);
            cmp.set("v.isError", true);
          } else {
            if (respWrap.amedWrap.amedObj.id != "") {
              console.log(
                "######### respWrap.amedWrap.amedObj.id  ",
                respWrap.amedWrap.amedObj.id
              );
              console.log(
                "######### respWrap.amedWrap.amedObj.Is_this_member_a_designated_Member__c",
                respWrap.amedWrap.amedObj.Is_this_member_a_designated_Member__c
              );
              //this.closeForm(cmp, event, helper);
              cmp.set("v.amedWrap.amedObj", null);
              //cmp.set("v.authfieldShow",true);
              var refeshEvnt = cmp.getEvent("refeshEvnt");
              var srWrap = respWrap.srWrap;
              console.log("@@@@@ srWrap ", srWrap);
              refeshEvnt.setParams({
                srWrap: srWrap,
              });
              refeshEvnt.fire();
            }
          }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          cmp.set("v.spinner", false);
          cmp.set("v.errorMessage", response.getError()[0].message);
          cmp.set("v.isError", true);
        }
      });
      $A.enqueueAction(onAmedSave);
    } else {
      helper.displayToast(
        "Error",
        "Please complete the mandatory field.",
        "error"
      );
    }
  },
  clearLookupEvent: function (component, event, helper) {
    console.log("handleLookupEvent");
    //var accObj = event.getParam("sObject");
    //console.log(JSON.parse(JSON.stringify(accObj)));
    component.set("v.amedWrap.amedObj.Registration_No__c", "");
  },
  handleFileUploadFinished: function (component, event) {
    // Get the list of uploaded files

    var uploadedFiles = event.getParam("files");
    var fileName = uploadedFiles[0].name;
    //component.find("Passport_Copy_Individual").set("v.value",fileName);
    var contentDocumentId = uploadedFiles[0].documentId;
    console.log(contentDocumentId);
    var docMasterCode = event.getSource().get("v.name");
    console.log("docMasterCode" + docMasterCode);
    var docFileMissing = docMasterCode + "_fileMissing";
    component.find(docFileMissing).set("v.value", "");
    console.log("setting file name" + fileName);
    var docFileNameId = docMasterCode + "_filename";
    component.find(docFileNameId).set("v.value", fileName);
    var mapDocValues = {};
    var docMap = component.get("v.docMasterContentDocMap");
    for (var key in docMap) {
      mapDocValues[key] = docMap[key];
    }
    mapDocValues[docMasterCode] = contentDocumentId;
    console.log(mapDocValues);
    component.set("v.docMasterContentDocMap", mapDocValues);
    console.log(
      "Doc@@@@@@$$$$$$$$$$$$$$$$",
      JSON.parse(JSON.stringify(component.get("v.docMasterContentDocMap")))
    );

    //alert("Files uploaded : " + uploadedFiles.length);
  },
  closeForm: function (cmp, event, helper) {
    //throw event.
    /*var refeshEvnt = cmp.getEvent("refeshEvnt");
        var amedId = cmp.get("v.amedWrap").amedObj.Id ;
        console.log('@@@@@ amedId ',amedId);
        refeshEvnt.setParams({
            "amedId" : '' });
        refeshEvnt.fire();*/

    cmp.set("v.spinner", true);
    var isNew = cmp.get("v.isNew");
    cmp.set("v.isCorporate", false);
    cmp.set("v.isIndividual", false);
    if (isNew == true) {
      console.log("===new record====");
      cmp.set("v.amedWrap.amedObj", null);
    }
    var refeshEvnt = cmp.getEvent("refeshEvnt");
    refeshEvnt.setParams({
      amedId: "",
      isNewAmendment: isNew,
      amedWrap: cmp.get("v.amedWrap.amedObj"),
    });
    cmp.set("v.spinner", false);
    refeshEvnt.fire();
  },
  showToast: function (title, message) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      title: title,
      message: message,
    });
    toastEvent.fire();
  },
  closeModel: function (cmp, event, helper) {
    //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
    console.log("--toast close--");
    cmp.set("v.errorMessage", "");
    cmp.set("v.isError", false);
  },

  handleRecordType: function (cmp, event, helper) {
    var index = event.target.dataset.amedtype;
    console.log("###############@@@@@@@@@@" + index);
    cmp.set("v.recordtypename", index);
    console.log(cmp.get("v.recordtypename"));
    if (index == "Individual") {
      cmp.set("v.amedWrap.amedObj.Amendment_Type__c", "Individual");
      cmp.set("v.isIndividual", true);
      cmp.set("v.isCorporate", false);
    } else {
      cmp.set("v.amedWrap.amedObj.Amendment_Type__c", "Body Corporate");
      cmp.set("v.isCorporate", true);
      cmp.set("v.isIndividual", false);
    }
  },

  showAuthFields: function (cmp, event, helper) {
    var toggleVal = cmp.find("inputToggle").get("v.value");
    console.log(toggleVal);
    cmp.set("v.authfieldShow", toggleVal);
    if (toggleVal == "No") {
      cmp.set("v.authfieldShow", true);
      cmp.set("v.authfieldHide", false);
      console.log("in IF");
      console.log("authfieldShow-------" + toggleVal);
      console.log("authfieldHide-------" + toggleVal);
    } else {
      cmp.set("v.authfieldShow", false);
      cmp.set("v.authfieldHide", true);
      console.log("in else");
      console.log("authfieldShow-------" + toggleVal);
      console.log("authfieldHide-------" + toggleVal);
      helper.resetAuthIndivalues(cmp);
    }
  },
  showAuthFieldsCor: function (cmp, event, helper) {
    var toggleValCor = cmp.find("inputToggleCor").get("v.value");
    console.log(toggleValCor);
    cmp.set("v.authfieldShowCor", toggleValCor);
    if (toggleValCor == "No") {
      cmp.set("v.authfieldShowCor", true);
      cmp.set("v.authfieldHideCor", false);
      console.log("in IF");
      console.log("authfieldShowCor-------" + toggleValCor);
      console.log("authfieldHideCor-------" + toggleValCor);
    } else {
      cmp.set("v.authfieldShowCor", false);
      cmp.set("v.authfieldHideCor", true);
      console.log("in else");
      console.log("authfieldShowCor-------" + toggleValCor);
      console.log("authfieldHideCor-------" + toggleValCor);
      helper.resetAuthCorvalues(cmp);
    }
  },

  handleOCREvent: function (cmp, event, helper) {
    var ocrWrapper = event.getParam("ocrWrapper");
    var mapFieldApiObject = ocrWrapper.mapFieldApiObject;
    // Change the key here
    var fields = cmp.find("requiredField");

    //parsing of code.
    for (var key in mapFieldApiObject) {
      for (var i = 0; i < fields.length; i++) {
        console.log(fields[i].get("v.fieldName"));
        if (fields[i].get("v.fieldName")) {
          console.log(fields[i].get("v.fieldName"));
          if (fields[i].get("v.fieldName").toLowerCase() == key.toLowerCase()) {
            fields[i].set("v.value", mapFieldApiObject[key]);
          }
        }
      }
    }
  },
});