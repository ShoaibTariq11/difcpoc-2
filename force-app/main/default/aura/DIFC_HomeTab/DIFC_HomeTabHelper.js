({
	onchekVATTaxNumber: function(component, event) {
        component.find("Id_spinner").set("v.class" , 'slds-show');
        var action = component.get("c.checkVATNumber");
        
        action.setCallback(this, function(response) {
            //alert('==inside--');
           // hide spinner when response coming from server 
            component.find("Id_spinner").set("v.class" , 'slds-hide');
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var respWrap = response.getReturnValue();
                component.set("v.isRegistration", respWrap.isVATNotRegistered);
                
            }else if (state === "INCOMPLETE") {
                alert('Response is Incompleted');
            }else if (state === "ERROR") {
                    alert("Unknown error");
            }
        });
        $A.enqueueAction(action);
    },closeWelcomeScreen : function(component, event) {
         console.log('test');
         document.getElementById('welcomeUSer').style.display = "none";
         document.getElementById("welcomeUSer").style.display = "none";
     },
    updateAccount: function(component, event,helper) {
        //alert('==entered');
        component.find("Id_spinner").set("v.class" , 'slds-show');
        var action = component.get("c.updateAccountField");
        
        action.setCallback(this, function(response) {
            //alert('==inside--');
           // hide spinner when response coming from server 
            component.find("Id_spinner").set("v.class" , 'slds-hide');
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.isOpen", false);
                helper.closeWelcomeScreen(component, event, helper);
            }else if (state === "INCOMPLETE") {
                alert('Response is Incompleted');
            }else if (state === "ERROR") {
                    alert("Unknown error");
            }
        });
        $A.enqueueAction(action);
        
    },
})