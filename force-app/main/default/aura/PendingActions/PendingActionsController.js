({
	doInIt : function(component, event, helper) {
		var action = component.get("c.fetchPendingActions");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.pendingActions", response.getReturnValue());
                console.log(response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	}
})