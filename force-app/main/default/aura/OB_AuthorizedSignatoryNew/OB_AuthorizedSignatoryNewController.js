({
    initAmendments : function(cmp, event, helper)
    {
        
        //cmp.set('v.amedWrap',"{ 'amedObj'  : {'sObjectType': 'HexaBPM_Amendment__c'}}"); 
        //{!v.srWrap.srObj.Id}
        
        var initAmendment = cmp.get('c.initAmendmentDB');
        var refeshEvntAuth = cmp.getEvent("refeshEvntAuth");
            
        
        
        var reqWrapPram  =
            {
                srId : cmp.get('v.srWrap').srObj.Id,
                
            };
        
        initAmendment.setParams(
            {
                "reqWrapPram": JSON.stringify(reqWrapPram),
                
            });
        
        initAmendment.setCallback(this, 
                                  function(response) {
                                      var state = response.getState();
                                      console.log("callback state: " + state);
                                      
                                      if (cmp.isValid() && state === "SUCCESS") 
                                      {
                                          var respWrap = response.getReturnValue();
                                          console.log('######### respWrap.amedWrap  ',respWrap.amedWrap);
                                          cmp.set('v.amedWrap',respWrap.amedWrap);
                                          refeshEvntAuth.setParams({
                                              "isNewAmendment" : true,
                                          });
                                          debugger;
                                          
                                          refeshEvntAuth.fire();
                                      }
                                      else
                                      {
                                          console.log('@@@@@ Error '+response.getError()[0].message);
                                          helper.createToast(cmp,event,response.getError()[0].message);
                                      }
                                  }
                                 );
        $A.enqueueAction(initAmendment);
        
    }
    
})