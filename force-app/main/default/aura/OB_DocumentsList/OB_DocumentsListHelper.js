({
	getSRDocs : function(component, event, helper) {
       
		let newURL = new URL(window.location.href).searchParams;
        var srId = newURL.get('srId');
        var pageId = newURL.get('pageId');
        console.log(srId);

        component.set("v.srId", srId);
        component.set("v.srId", srId);
        
        var action = component.get("c.getSrDocs");
        var requestWrap = {
        
			srId: srId,
            pageId: pageId,
        };
        action.setParams({
        
           	"requestWrapParam": JSON.stringify(requestWrap)
        });
        action.setCallback(this, function(response) {
        
            var state = response.getState();
            if (state === "SUCCESS") {
            	console.log(response.getReturnValue());
                component.set("v.commandButton",response.getReturnValue().ButtonSection);
                component.set("v.docWrapp",response.getReturnValue().docWrap);
                console.log(component.get('v.docWrapp'));
               
            }else{
            	console.log('@@@@@ Error '+response.getError()[0].message);
			}
        });
        $A.enqueueAction(action);
    },
})