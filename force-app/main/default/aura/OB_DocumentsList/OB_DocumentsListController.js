({
	init : function(component, event, helper) {
		helper.getSRDocs(component, event, helper);
	},
    handleButtonAct : function(component, event, helper) {
        
        
        
        try{
            
        let newURL = new URL(window.location.href).searchParams;
        
        var srID = newURL.get('srId');
        var buttonId =  event.target.id;
        console.log(buttonId);
        var action = component.get("c.getButtonAction");
        
        action.setParams({
        
           	"SRID": srID,
            "ButtonId": buttonId
        });
     
        action.setCallback(this, function(response) {
        
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("button succsess")
            	
                console.log(response.getReturnValue());
                window.open(response.getReturnValue().pageActionName,"_self");
            }else{
            	console.log('@@@@@ Error '+response.getError()[0].message);
			}
        });
        $A.enqueueAction(action);
          
        }catch(err) {
           console.log(err.message) 
        } 
        

    }

})