({
    doInit : function(component, event, helper) {
        console.log("inside method");
        var action = component.get("c.getTopUpBalanceAmount");
        action.setCallback(this, function(a){
            var state = a.getState(); // get the response state
            if(state == 'SUCCESS') {
                console.log('return value is trans1234 '+JSON.stringify(a.getReturnValue()));
                if(a.getReturnValue()){
                    var returnValue = a.getReturnValue();
                    console.log('returnValue'+returnValue);
                    component.set("v.wrapRecords",returnValue);
                    console.log("balanceUpdated12"+component.get("v.wrapRecords"));
                }
            }
            else if(state == 'FAILURE'){
                console.log('Some problem ocurred');
            }
        });
        $A.enqueueAction(action);
        
    },
	goToTopUpBalance : function(component, event, helper){
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/topupbalance/",
        });
        urlEvent.fire();
    }
})