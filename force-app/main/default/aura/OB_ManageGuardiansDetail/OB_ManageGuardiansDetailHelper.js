({
    loadSRDocs : function(component,event, helper){ 
      /*var amendmentWrapper = component.get("v.amedWrap");
       console.log('amendmentWrapper----'+amendmentWrapper);
       var amedId = amendmentWrapper.amedObj.Id;
      //var srId = component.get("v.srId");
       var srId = component.get("v.srWrap").srObj.Id;
         console.log(' loadSRDocs srId-----',srId);
       var action = component.get("c.viewSRDocs");
       var self = this;
       action.setParams({
           srId: srId,
           amendmentId: amedId
       });
        // set call back 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log('srDocs>>');
                console.log(result.Passport_Copy_Individual)
                console.log(result.Passport_Copy_Individual.FileName);
                component.set("v.srDocs",result);
                if(result.Passport_Copy_Individual){
                    component.set("v.fileName",result.Passport_Copy_Individual.FileName);
                	component.set("v.srDocId",result.Passport_Copy_Individual.Id);
                }
            } else if (state === "INCOMPLETE") {
                alert("From server: " + response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        helper.showToast(component,event, helper, errors[0].message,'error');
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        // enqueue the action 
        $A.enqueueAction(action);*/
        
    },
    showToast: function(component, event, helper, msg, type) 
    {
        console.log('######## ocrWrapper.errorMessage inside ',msg);
                                                    
        // Use \n for line breake in string
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
          mode: "dismissible",
          message: msg,
          type: type
        });
        toastEvent.fire();
 	},
	handleUploadFinished:function(cmp, event,helper) 
    {
    	/*//new code 
        //handleUploadFinished(cmp, event,helper);
    	console.log('helper calling handleUploadFinished');
    	var uploadedFile = event.getParam("files")[0];
		var documentIdParam = uploadedFile.documentId;
        console.log('helper calling handleUploadFinished documentIdParam ',documentIdParam);
    	//helper.callOCR(cmp, event,helper,documentIdParam); */
    
	},
    callOCR: function (cmp, event,helper,documentIdParam) 
    {},
    fileUploadHelper : function(cmp, event, helper)
    {
        //New method.
        console.log("########### fileHolder Start");
    
        // For file upload and validation.
        var fileUploadValid = true;
        var fileHolder = cmp.find("filekey");
    
        if (fileHolder && Array.isArray(fileHolder)) {
          for (var i = 0; i < fileHolder.length; i++) {
            if (fileHolder[i].get("v.requiredDocument")) {
              helper.fileUploadProcess(cmp, event, helper, fileHolder[i]);
              fileUploadValid =
                fileUploadValid && fileHolder[i].get("v.fileUploaded");
              //requiredText
              if (!fileUploadValid && !fileHolder[i].get("v.fileUploaded")) {
                fileHolder[i].set("v.requiredText", "Please upload document.");
              }
            }
          }
        } else if (fileHolder) {
          console.log(
            "########### fileHolder content docID ",
            fileHolder.get("v.contentDocumentId")
          );
          //here
          helper.fileUploadProcess(cmp, event, helper, fileHolder);
    
          if (fileHolder.get("v.requiredDocument")) {
            fileUploadValid = fileHolder.get("v.fileUploaded");
            console.log("@@@@@@@@@@@ fileUploadValid ", fileUploadValid);
            if (!fileUploadValid) {
              fileHolder.set("v.requiredText", "Please upload document.");
            }
          }
        }
    
        console.log(" Final file upload ", fileUploadValid);
        if (!fileUploadValid) {
          helper.showToast(
            cmp,
            event,
            helper,
            "Please upload required files",
            "error"
          );
          helper.hide( cmp,event);
          return "file error";
        }
        console.log("########### fileHolder end");
      
    },
    fileUploadProcess : function(cmp, event, helper,fileHolder)
    {
        //New Method
        var contentDocumentId = fileHolder.get("v.contentDocumentId");
        var documentMaster = fileHolder.get("v.documentMaster");
    
        if (contentDocumentId && documentMaster) {
          var mapDocValues = {};
          var docMap = cmp.get("v.docMasterContentDocMap");
          for (var key in docMap) {
            mapDocValues[key] = docMap[key];
          }
    
          mapDocValues[documentMaster] = contentDocumentId;
          console.log(mapDocValues);
          console.log("@@@@@@ docMasterContentDocMap ");
          cmp.set("v.docMasterContentDocMap", mapDocValues);
          console.log(
            JSON.parse(JSON.stringify(cmp.get("v.docMasterContentDocMap")))
          );
        }
        
    },
    show: function (cmp, event) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
    },
    hide:function (cmp, event) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-show");
        $A.util.addClass(spinner, "slds-hide");
    }
})