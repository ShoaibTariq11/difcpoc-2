({
	doInit : function(component, event, helper) {
        var srRecordTypesResults = component.get("v.recordTypeListmap");
        var listSRRecordTypeInfo = [];
        for(var key in srRecordTypesResults) {
            listSRRecordTypeInfo.push({value:srRecordTypesResults[key], key:key});
        }
        component.set("v.listRecordTypeInfo", listSRRecordTypeInfo);
	}
})