/* DMCC
*
* DMCC login page dashboard
* Author Bilal Nazir March 20, 2014
*/

DMCCLoginDashboard = (function(){

		var init = function(){
			renderTiles("main-content", 100, true );
		}

		var renderTiles = function(container, limit, isHome, isMore){

			var isMore = isMore || false;

			if(!isMore){
				$('#'+container).empty();
			}else{
				dmcc.counter++;
			}
		
		//$.getJSON('http://dxb.mblm.com/dmcc/js/d ata/news.json', function(data){
		$.getJSON('resource/1395323510000/CommunityLogin_News', function(data){
		//var data = $.parse('{ "newsJSON" : [ { "title": "Page Title lorem ipsum dolor sit amet, consectetur adipiscing elit.", "postdate": "September 29, 2013", "category": "gold", "categoryText": "", "subCategory": "Video", "mediaType": "video", "linkText": "Read more", "image": "images/sample/sample13.jpg", "dataFilter": "gold", "isFav" : false }, { "title": "Page Title lorem ipsum dolor sit amet", "postdate": "September 29, 2013", "category": "tea", "categoryText": "", "subCategory": "Event", "mediaType": "event", "linkText": "Read more", "image": "images/sample/sample14.jpg", "dataFilter": "tea", "isFav" : false }, { "title": "Page Title lorem ipsum dolor sit amet, consectetur adipiscing elit.", "postdate": "September 29, 2013", "category": "gold", "categoryText": "", "subCategory": "News", "mediaType": "photo", "linkText": "Read more", "image": "images/sample/sample12.jpg", "dataFilter": "gold", "isFav" : false }, { "title": "Page Title lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent elementum eros nec dapibus condimentum.", "postdate": "", "category": "freezone", "categoryText": "Freezone", "subCategory": "Apply Online", "mediaType": "", "linkText": "Read more", "image": "images/sample/sample14.jpg", "dataFilter": "freezone", "isFav" : false } ] }');

			$.each(data.newsJSON, function(i, news){

				if(isHome && news.category == "notif"){
					var notif = $('<div class="box col3 hoverable" data-filter-class="other" data-sortable/>');

					var tileNotif = '<a href="#">'+
										'<img src="'+news.image+'" />'+
										'<span class="remove"><i class="icon icon-remove"></i></span>'+
										'<div class="content bg-light-grey gradient">'+
											'<p>'+(news.title).substr(0, 80)+'</p>'+
											'<div class="ct-info">'+
												'<span class="ct-left">'+news.postdate+'</span>'+
												'<span class="ct-right">'+news.categoryText+'</span>'+
												//'<span class="bottom-icon notif"><i class="icon icon-alert"></i></span>'+
											'</div>	'+									
										'</div>'+
									'</a>';

					notif.append(tileNotif);

					notif.append('<a href="#" class="hover-link bg-dark-blue gradient">'+news.linkText+'...</a>');

					$('#'+container).append(notif);

				}else if(news.category != "notif"){

					if((i + 1) <= limit){

						var lastTile = ((i + 1) == limit) ? " last-tile-" + dmcc.counter : "";

						var template = $('<div class="box col3 hoverable'+lastTile+'" data-filter-class="' + news.dataFilter + '"/>');

						var templateLink = $('<a href="#"></a>');

						if(news.mediaType == "" || news.mediaType != "gallery"){
							templateLink.append('<img src="' + news.image + '" />');

							switch(news.mediaType){
								case "video":
									templateLink.append('<span class="bg-video"></span>');
									break;
								case "audio":
									templateLink.append('<span class="bg-audio"></span>');
									break;
								case "photo":
									templateLink.append('<span class="bg-images"></span>');
									break;
								case "event":
									templateLink.append('<span class="bg-calendar"></span>');
									break;
							}

							if(news.isFav){
								templateLink.append('<span class="star"><i class="icon icon-star"></i></span>');
							}

							var tileContent = $('<div class="content bg-light-grey gradient" />'),
								tileInfo = $('<div class="ct-info" />');


							tileContent.append('<p>' + (news.title).substr(0, 80) + '</p>');

							if(news.postdate != ""){
								tileInfo.append('<span class="ct-left">' + news.postdate +'</span>');
							}else{
								tileInfo.append('<span class="ct-left">' + news.categoryText +'</span>');
							}

							tileInfo.append('<span class="ct-right">' + news.subCategory + '</span>');

							var hoverColor, tileIcon;

							switch(news.category){
								case "commodities":
									hoverColor = " bg-tosca gradient";
									//tileIcon = " icon-commodities-hover";
									break;
								case "dmcc":
									hoverColor = " bg-dark-blue gradient";
									//tileIcon = " icon-dmcc";
									break;
								case "tea":
									hoverColor = " bg-purple gradient";
									//tileIcon = " icon-tea-hover";
									break;
								case "gold":
									hoverColor = " bg-tosca gradient";
									//tileIcon = " icon-gold-hover";
									break;
								case "diamond":
									hoverColor = " bg-light-blue gradient";
									//tileIcon = " icon-diamond-hover";
									break;
								case "freezone":
									hoverColor = " bg-purple gradient";
									//tileIcon = " icon-reload";
									break;

							}

							//tileInfo.append('<span class="bottom-icon"><i class="icon' + tileIcon + '"></i></span>');
							tileContent.append(tileInfo);
							templateLink.append(tileContent);
							template.append(templateLink);
							template.append('<a href="#" class="hover-link' + hoverColor + '">' + news.linkText + '...</a>');



						}else{
							//if gallery
							var tileSlider = $('<div class="tile-slider" />'),
								tileList = $('<ul data-orbit data-options="bullets:false;slide_number:false;timer:false;" />');

								tileSlider.append('<div class="preloader"></div>');

								$(news.image).each(function(idx, image){
									tileList.append('<li><img src="' + image + '"/></li>');
								});

								tileSlider.append(tileList);
								tileSlider.append('<div class="orbit-nav">'+
										            '<a class="orbit-prev-custom" href="#">Prev</a>'+
										            '<a class="orbit-next-custom" href="#">Next</a>'+
										          '</div>');
								template.append(tileSlider);

								//template.append('<a href="#" class="hover-link bg-dark-blue gradient">' + news.linkText + '...</a>');

						}

						$('#'+container).append(template);


					}

				}

			});	//loop news

			reinitNewsTiles('#'+container, '.box');
			$(document).foundation('orbit').init();
			dmcc.sliderNavigation();
			$('.loadmore-loader').hide();

			if(!$('a.loadmore').is(':visible')){
				$('a.loadmore').show();
			}


			if(isMore){

				setTimeout(function(){
					var scrollTop = $('#' + container + ' .last-tile-'+dmcc.counter).offset().top;
					//scroll to the new tiles
					$('html,body').stop().delay(100).animate({
						'scrollTop': (scrollTop-840)+'px'
					},800);
				},500);
				
			}

		});//get jSON
				
	}

	function reinitNewsTiles (element, tiles){

		var $container = $(element);

		$container.imagesLoaded( function() {
			$container.masonry('destroy').masonry();
			$container.masonry({
				columnWidth:10,
				itemSelector: tiles,
	        	isAnimated: !Modernizr.csstransitions,
	        	isFitWidth: true
			});	

			//data filter
			$container.filteredMasonry({
				itemSelector: tiles,
				filtersGroupSelector: '.filter-nav'
			});

			$container.masonry('reloadItems');	

		});		
		
	}

	return {
		initDashboard : init
	}

}())

$(function(){

	DMCCLoginDashboard.initDashboard();
})