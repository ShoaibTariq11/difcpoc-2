/**
 * small plugin to click anywhere
 * bind click event to the document
 */

;(function($){
    $.fn.outside = function(ename, cb){
        return this.each(function(){
            var $this = $(this),
                self = this;

            $(document).bind(ename, function tempo(e){
                if(e.target !== self && !$.contains(self, e.target)){
                    cb.apply(self, [e]);
                    if(!self.parentNode) $(document.body).unbind(ename, tempo);
                }
            });
        });
    };
}(this.jQuery || this.Zepto));

/**
 * small plugin to fix browser behavior on window resize
 */
 
 ;(function ($) {
    var on = $.fn.on, debounce;
    $.fn.on = function () {
        var args = Array.apply(null, arguments);
        var first = args[0];
        var last = args[args.length - 1];
        var isObj = typeof first === 'object';


        if (!isObj && isNaN(last) || (last === 1 && args.pop())) return on.apply(this, args);

        if (isObj) {
            for (var event in first) {
                this.on(event, last, first[event], args[1]);
            }
            return this;
        }

        var delay = args.pop();
        var fn = args.pop();

        args.push(function () {
            var self = this, params = arguments;
            clearTimeout(debounce);
            debounce = setTimeout(function () {
                fn.apply(self, params);
            }, delay);
        });

        return on.apply(this, args);
    };
}(this.jQuery || this.Zepto));


var dmcc = {

	animateTO: null,

	tiles : null,

	counter: 0,

	init: function(){
		this.IESecondaryMenu();
		this.tabletSecondaryMenu();
		this.settingClick();
		this.showDesktopSearch();
		this.mobileSearchClick();
		this.notification();
		this.login();
		this.mobileLogin();
		this.showNextMenu();
		this.formTooltip();
		this.clickOutside();
	},

	clickOutside: function(){
	
		//click outside

		$('nav.top-bar ul li.settings > a').outside('click', function(){
			$(this).parent('.settings').removeClass('clicked');
		});
		
		$('.primary-nav ul.right li.has-dropdown > a').outside('click', function(e){
			$(this).parent('li.has-dropdown').removeClass('hover').children('#drop1').removeClass('open');
		});
		
		$('.secondary-menu ul.left li.has-dropdown > a').outside('click', function(e){
			$(this).parent('li.has-dropdown').removeClass('hover');
		});
		
		//click to open
		$('.primary-nav ul.right li.has-dropdown > a').click(function(){
			dmcc.hideDesktopSearch();
			$('nav.top-bar ul li.settings').removeClass('clicked');
			$('.secondary-menu ul.left li.has-dropdown').removeClass('hover');
		});
		
		$('.secondary-menu ul.left li.has-dropdown > a').click(function(){		
			
			$(this).parent('li.has-dropdown').siblings().removeClass('hover');
			$('nav.top-bar ul li.settings').removeClass('clicked');
			$('.primary-nav ul.right li.has-dropdown').removeClass('hover').children('#drop1').removeClass('open');
		});

		$('.has-dropdown a').outside('click',function(){
			$(this).next().hide();
		});
	},
	
	primaryNavClick: function(){
		$('.primary-nav	.top-bar-section ul > li > a').click(function(e){
			//e.stopPropagation();
			//clear all active state
			//$('.primary-nav	.top-bar-section > ul > li').each(function(){
			//		if($(this).children('a').hasClass('active')) $(this).children('a').removeClass('active');
			//});
			$(this).toggleClass('active').parent('li').siblings().children('a.active').removeClass('active');

			if(!$(this).hasClass('search')){
				//hide desktop search
				dmcc.hideDesktopSearch();
			}
		
		});
	},

	
	IESecondaryMenu: function(){

		if($('html').hasClass('lt-ie9')){
			$('.secondary-menu .top-bar-section > ul > li.has-dropdown').click(function(){
				$(this).toggleClass('hover');
			});
		}
	},

	tabletSecondaryMenu: function(){
		$('.tablet-secondary-nav .top-bar-section > ul > li.has-dropdown > a').click(function(e){
		
			e.preventDefault();
		
			$(this).toggleClass('expanded');
			
			if($(this).hasClass('expanded')){
				$(this).siblings('.dropdown').css({'visibility':'visible'});
			}else{
				$(this).siblings().css({'visibility':'hidden'});
			}
		
				   
		});
	},
	
	settingClick: function(){
		$('nav.top-bar ul li.settings > a').click(function(e){
			e.preventDefault();
			$('.primary-nav ul.right li.has-dropdown').removeClass('hover').children('#drop1').removeClass('open');
			$('.secondary-menu ul.left li.has-dropdown').removeClass('hover');
			$(this).parent('.settings').toggleClass('clicked');
		});		
	},

	showDesktopSearch: function(){
		$('.primary-nav .top-bar-section ul > li.desktop-search a').click(function(e){

			e.preventDefault();
			
			$('.primary-nav	.top-bar-section ul > li.has-dropdown').removeClass('hover');

			var wiWidth = $(window).width();

			$(this).toggleClass('showed').parent('li.desktop-search').toggleClass('search-open');

			if($(this).hasClass('showed')){			

				$(this).siblings('form').children(':text').val("");

				if((wiWidth + 17) >= 981){
					$(this).siblings('form').css({'margin-right':'10px','display':'inline'}).children(':text').css({
						'height': '2em',
						'padding': '0 10px',
						'border': 'solid 1px #ccc'						
					});

					$(this).siblings('form').animate({
						'width':'230px'
					},300);

				}

			}else{
				dmcc.hideDesktopSearch();
			}
			
		});
	},

	hideDesktopSearch: function(){

		var search = $('.primary-nav .top-bar-section ul > li.desktop-search');

		$(search).children('form').css({'margin-right':'0px'}).children(':text').css({
			'padding': '0',
			'border': 'none',
			'height': '0px'
		});

		$(search).children('form').animate({
			'width':'0'
		},300, function(){
			$(this).css('display','none');
		});

		$(search).children('a.showed').removeClass('showed');
		$(search).removeClass('search-open');

	},

	mobileSearchClick: function(){
		$('.mobile-primary-nav ul.title-area li.mobile-search a.search').click(function(e){
			e.preventDefault();

			$('.mobile-primary-nav').removeClass('expanded-nav');

			$(this).toggleClass('active').parent('.mobile-search').toggleClass('search-open');

			if($(this).hasClass('active')){
				$('.mobile-primary-nav').css({
					'overflow':'visible'
				});
			}else{
				$('.mobile-primary-nav').css({
					'overflow':'hidden'
				});
			}

		});
	},


		
	login: function(){
		$('a.btn-login').click(function(){

			$('form.login .icon-checklist').fadeIn();
			$('form.login .icon-delete').fadeIn();
			$('form .password-error').fadeIn();

			setTimeout(function(){
				$('form.login .icon-checklist').fadeOut();
				$('form.login .icon-delete').fadeOut();
				$('form .password-error').fadeOut();
			},5000);

			
		});
	},

	notification: function(){
		$('a.notification').click(function(){

			$(this).toggleClass('show-tooltip');

			if($(this).hasClass('show-tooltip')){
				$(this).siblings('.ctooltip').fadeIn();
			}else{
				$(this).siblings('.ctooltip').fadeOut();
			}

			
		});
	},

	mobileLogin: function(){
		$('.mobile-submit a').click(function(){

			$('form.login .icon-checklist').fadeIn();
			$('form.login .icon-delete').fadeIn();

			setTimeout(function(){
				$('form.login .icon-checklist').fadeOut();
				$('form.login .icon-delete').fadeOut();
			},5000);

			
		});
	},

	showNextMenu: function(){
		if($('body.news').length){
			$('.secondary-menu .top-bar-section ul.next-menu li a').click(function(e){
				e.preventDefault();

				$('.secondary-menu').toggleClass('show-next-menu');


			});
		}
	},

	formTooltip: function(){
		$('i.icon').click(function(e){
			e.preventDefault();

			$(this).toggleClass('show-tooltip');
			if($(this).hasClass('show-tooltip')){
				$(this).parent().siblings('.ctooltip').fadeIn();
			}else{
				$(this).parent().siblings('.ctooltip').fadeOut();
			}
		});
	}

};

$(document).ready(function(){
	dmcc.init();
	
	/*
	$(".table tbody tr:odd").addClass("even");
	// Table Sorting
	$(".table").tablesorter({
		headers: {
			0:{sorter: false}
		}
	});

	$(".table").bind("sortEnd",function() {
		$(".table tbody tr").removeClass("even");
		$(".table tbody tr:odd").addClass("even");
	});	
	
	//Table Responsive
	$('table').footable();
	

	//Accordian
	$('.accordion dd > a,.menu-title').click(function(e){
		e.preventDefault();
		$(this).toggleClass('expanded').next().slideToggle();
	});

	//Accordian for sublist
	$('dd ul ul li span').click(function(){
		$(this).toggleClass('expanded').next().slideToggle();
	});
	*/

	//Navigation
	$('.has-dropdown a').click(function(e){
		e.preventDefault();
		$(this).next().slideToggle().parent().siblings().find('ul').hide();
	})

	//Profile Steps
	$('.collapsable span').click(function(){
		$(this).parent().parent().parent().toggleClass('expanded').next().slideToggle();
	});

	$('.toggle-topbar').click(function(){
		$(this).next().removeClass('search-open').parent().parent().toggleClass('expanded-nav');
		$(this).parent().next().css('left','0');
	});
	$('.activity-row').click(function(){
		$(this).toggleClass('expanded').next().slideToggle();
	});
	$('.activity-row .columns a,.action-to-do a').click(function(e){
		e.preventDefault();
	});

	$('.menu-content .sub label').click(function(){
		$(this).toggleClass('expanded').parent().next().slideToggle();
	});

	$('.top-nav .toggle-topbar a').click(function(){
		$(this).parent().parent().parent().toggleClass('expanded-nav');
		$('.mobile-primary-nav').removeClass('expanded-nav');
		$('.mobile-search').removeClass('search-open');
	});

	/*
	//Select
	$(".custom.dropdown a:contains('Please select...')").addClass('grey-text');

	$(".custom.dropdown li").click(function(){
		if($(".custom.dropdown a.current").not(":contains('Please select...')").addClass('grey-text'));
		if($(".custom.dropdown a.current:contains('Please select...')").removeClass('grey-text'));
	});

	
	//Date Selector
	var picker = new Pikaday({
        field: document.getElementById('start-date-calendar'),
        format: 'DD/MM/YYYY'
    });

   	var picker = new Pikaday({
        field: document.getElementById('end-date-calendar'),
        format: 'DD/MM/YYYY'
    });
   	


	//Filter in accounts page
	$('.sub-nav dd').click(function(){
		$(this).addClass('active').siblings().removeClass('active')
	});
	

	//To check OS
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		$("body").addClass("phone");
		$('#start-date-calendar').attr("type","date");
		$('#end-date-calendar').attr("type","date");
	}

	//Date picker label

	$(".date-range-col label").inFieldLabels();
	*/
});