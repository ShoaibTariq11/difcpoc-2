/***
 * masonry extend sort
 * author: Erwin Yusrizal
 * modified by Erwin Yusrizal
 * data: 26/09/2013
 * 
 */



!function($){
   $.fn.sortedMasonry = function(options){
     var cache=[];

     var init = function($container){

        /**
         * I prefer use data attibute here than class (previously used class)
         */

        $container.find('[data-sortable]').each(function(){
            cache.push($(this));
        });

        $container.masonry(options);
     }
     //filter items in cache
     var sortItems = function(sortby){
        var result=[];

        var cached = cache;

        for(i = 0; i < cache.length; i++){

            for(a = 0; a < cached.length; a++){
                if($(cached[a]).data('sortable') == (i+1)){
                    result.push(cached[a]);
                }
            }
        }

        switch(sortby){
            case "desc":
                result.reverse();
                break;
            case "random":
                result = cache;
                break;
        }

        return result;

        
     }

     //reload masonry
     var reload = function($container,items, ck){
        $container.empty();

        $(items).each(function(){
            $($container).append($(this));
        });

        //just reinit masonry so it could be animated
        $container.masonry('destroy').masonry();
        $container.masonry({
            columnWidth: 100,
            itemSelector: '.box',
            isAnimated: true,
            isFitWidth: true
        }).masonry('reloadItems');
     }
 
     var proc = function($container){

        $(options.sortGroupSelector).find('[data-sort]').each(function(i,a){

            $(this).on('click',function(){

                var sortby = $(this).data('sort');
                var items = sortItems(sortby);

                reload($container, items);
                
            })
        })
     }

     
     /**
      * create indexOf function so IE8 and below understand it
      */

      var indexOf = function(needle) {
        if(typeof Array.prototype.indexOf === 'function') {
            indexOf = Array.prototype.indexOf;
        } else {
            indexOf = function(needle) {
                var i = -1, index = -1;

                for(i = 0; i < this.length; i++) {
                    if(this[i] === needle) {
                        index = i;
                        break;
                    }
                }

                return index;
            };
        }

        return indexOf.call(this, needle);
     }
 
     return this.each(function() {
        var $$ = $(this);
        init($$);
        proc($$);
     });
   }
}(window.jQuery)