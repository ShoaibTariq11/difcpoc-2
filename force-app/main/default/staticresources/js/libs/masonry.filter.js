/***
 * masonry extend filter
 * author: ? http://hikev.in/2013/06/add-filtering-functionality-to-masonry-jquery-plugin/
 * modified by Erwin Yusrizal
 * data: 26/09/2013
 *
 * ===================================================================================
 *
 * This extended masonry plugin has been modified to support multi filter also support mighty IE7/8
 *
 * 
 */



!function($){
   $.fn.filteredMasonry = function(options){
     var cache=[],
         activeFilter=[]; //set active filter as an array so it will support multi filter

     var init = function($container){

        /**
         * I prefer use data attibute here than class (previously used class)
         */

        $container.find('[data-filter-class]').each(function(){
            cache.push($(this));
        });

        $container.masonry(options);
     }
     //filter items in cache
     var filterItems = function(selector){
        var result=[];

        //check if nothing selected
        if(selector.length == 0)
            return result = cache;

        $(cache).each(function(item){

            var keywords = $(cache[item]).data('filter-class').split(",");

            if(selector.length >= keywords.length){
                for(i = 0; i < selector.length; i++){
                    if(indexOf.call(keywords, $.trim(selector[i])) > -1){
                        result.push(cache[item]);
                    }
                }
            }else{
                for(i = 0; i < keywords.length; i++){
                    if(indexOf.call(selector, $.trim(keywords[i])) > -1){
                        result.push(cache[item]);
                    }
                }
            }
            

        })

        return result;
     }



     //reload masonry
     var reload = function($container,items){
        $container.empty();

        $(items).each(function(){
            $($container).append($(this));
        });

        //just reinit masonry so it could be animated, dont use reload || reloadItems method
        $container.masonry('destroy').masonry();
        $container.masonry({
            columnWidth: 10,
            itemSelector: '.box',
            isAnimated: true,
            isFitWidth: true
        }).masonry('reload');
     }
 
     var proc = function($container){

        $(options.filtersGroupSelector).find('[data-filter]').each(function(i,a){

            $(this).on('click',function(){

                //get filter keywords
                var selector = $.trim($(this).attr('data-filter'));

                //set active state for this filter trigger
                $(this).toggleClass('active-filter');

                if($(this).hasClass('active-filter')){
                    //check if filter keywords already in active filter
                    if(indexOf.call(activeFilter, selector) < 0){
                        activeFilter.push(selector);
                    }

                    var items = filterItems(activeFilter);
                    
                }else{

                    //if filter already unchecked, remove it from activeFilter
                    var index = indexOf.call(activeFilter, selector);

                    activeFilter.splice(index,1);

                    var items = filterItems(activeFilter);

                }

                reload($container,items);
                
            })
        })
     }

     
     /**
      * create indexOf function so IE8 and below understand it
      */

      var indexOf = function(needle) {
        if(typeof Array.prototype.indexOf === 'function') {
            indexOf = Array.prototype.indexOf;
        } else {
            indexOf = function(needle) {
                var i = -1, index = -1;

                for(i = 0; i < this.length; i++) {
                    if(this[i] === needle) {
                        index = i;
                        break;
                    }
                }

                return index;
            };
        }

        return indexOf.call(this, needle);
     }
 
     return this.each(function() {
        var $$ = $(this);
        init($$);
        proc($$);
     });
   }
}(window.jQuery)