
var j$ = jQuery.noConflict();
j$(document).ready(function(){
	
	//removes the target blank hrefs.
	/*
	j$('a[target="_blank"]').removeAttr('href');
	j$('a[target="_blank"]').removeAttr('target');
	
	j$('a[target="_top"]').removeAttr('href');
	console.log("top cleared");
	j$('a[target="_top"]').removeAttr('target');
	
	j$('a[target="_self"]').removeAttr('href');
	j$('a[target="_self"]').removeAttr('target');
	
	j$('a[target="_parent"]').removeAttr('href');
	j$('a[target="_parent"]').removeAttr('target');
	*/
	
	//disable the below hyperlinks
	j$('a[href="mailto:portal@difc.ae"]').removeAttr('href');
	j$('a[href="https://portal.difc.ae/signin"]').removeAttr('href');
	
	//Opens the first according section on load.
	jQuery(".accordion .accordion-section:first-child .accordion-section-title:first-child .accordion-image").addClass("icon-21");
	jQuery(".accordion .accordion-section:first-child .accordion-section-title:first-child").addClass("active");
	j$(".accordion .accordion-section:first-child .accordion-section-content").slideDown(100).addClass('open'); 
	
	 j$('.expand-collapse-link').click(function(e) {
		 // Grab current anchor value
		//var currentAttrValue = j$(this).attr('href');
		//console.log(currentAttrValue);
		
		//  Checks the tag for active.
		if(j$(this).is('.active')) { 
			console.log(j$(this).find('.expand-collapse-text'));
			j$(this).find('.expand-collapse-text').text("+");
			j$(this).removeClass('active');
			j$(this).next().slideUp(100).removeClass('open');
			console.log(j$(this).next());
		}else {
			j$(this).find('.expand-collapse-text').text("-");
			j$(this).addClass('active');
			
			//console.log(j$(this).next());
			// Open up the hidden content panel
			j$(this).next().slideDown(100).addClass('open'); 
		
		}
		e.preventDefault();
	 });
	 
	 
	
});