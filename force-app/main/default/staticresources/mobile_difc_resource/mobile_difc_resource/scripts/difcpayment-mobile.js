 /********************* DIFC Payment Module ************************/
DIFCPayment = (function(){
    
    var PaymentGateway_URL = '',
        CustomerId         = '',
        ReceiptPrefix      = '',
        AccountBPNumber    = '',
        receipt_amount_id  = '';

    function setup(gatewayURL, accountId, receiptPrefix, BPNum, receipt_amountId ) {
        PaymentGateway_URL  = gatewayURL;
        CustomerId          = accountId;
        ReceiptPrefix       = receiptPrefix;
        AccountBPNumber     = BPNum;
        receipt_amount_id   = receipt_amountId;
		
		console.log("DIFC Payment Module Init event!!");
        initEvents();
    }

	var j$ = jQuery.noConflict();
    function initEvents() {

        payment_optionChanged();

        j$('#portaltopupbutton').click(function(){
				console.log("portaltopupbutton clicked");
                //e.preventDefault();
                 //var $topupBalanceForm = j$(this);
                //$paymentOption = $topupBalanceForm.find("input[name='pBalance']:checked").parent().attr('paymentType');
                var $topup_amount = j$('#payment-amount-textbox').val();
                
                var $paymentOption = "";
                j$( "#payment-option-dropdown option:selected" ).each(function() {
                    $paymentOption += j$( this ).text();
                });
                
                switch($paymentOption){
                    case 'BANK PAYMENT':
                        //pay_by_bank($topup_amount);
                        break;
                    case 'PAY BY CASH':
                        // pay_by_cash($topup_amount);
                        break;
                    case 'PAY BY CHEQUE':
                        pay_by_cheque($topup_amount);
                        break;
                    case 'PAY BY CARD':
                        pay_by_card($topup_amount);                        
                        break;
                    default:
                        break;
                }
                
            });
            /*.on('invalid', function () {
                var invalid_fields = j$(this).find('[data-invalid]');
                console.log(invalid_fields);
            })
            .on('valid', function () {
                console.log('valid!');*/
			/*
            var $topupBalanceForm = j$(this);
               
             var $topup_amount = $topupBalanceForm.find("input#payment-amount-textbox").val();
			 
			 var paymentOption = "";
                    j$( "select option:selected" ).each(function() {
                      paymentOption += j$( this ).text() + " ";
                    });
			 
            switch($paymentOption){
                case 'BANK PAYMENT':
                    //pay_by_bank($topup_amount);
                    break;
                case 'PAY BY CASH':
                    // pay_by_cash($topup_amount);
                    break;
                case 'PAY BY CHEQUE':
                    pay_by_cheque($topup_amount);
                    break;
                case 'PAY BY CARD':
                    pay_by_card($topup_amount);                        
                    break;
                default:
                    break;
            }    */            
      //  }); 

    }

	/*
	This method is used to validate the drop down values selected
	BANK PAYMENT AND PAY BY CASH: disable the textbox and topup balance button and show informational message to userAgent
	ELSE: enable the textbox and popup button
	
	*/
    function payment_optionChanged() {

			  j$('#portaltopupbutton').css('opacity','.7')
              j$('#payment-amount-textbox').attr('disabled',true);

				console.log("j$('#payment-option-dropdown').change added");
				
				console.log(j$('#payment-option-dropdown'));
				
             j$('#payment-option-dropdown').change(function(){
                 
                 var str = "";
                    j$( "#payment-option-dropdown option:selected" ).each(function() {
                      str += j$( this ).text();
                    });
                 
                 //display the messages based on the dropdown selection
               	j$('#portaltopupbutton').css('opacity','1');
                 if(str == "BANK PAYMENT"){
					 console.log(str);
					 
					 j$('#payment-amount-textbox').val('');
					 
                     j$("[id$=error_message]").html("Your bank partner is "+AccountBPNumber+" .Please include this number in your bank transfer remarks for faster processing.");
				
					 
					 loadInfoDialog();
					 disableFields();
					 j$('#popup_box').dialog('option', 'position', 'center');
                     
                 } else if(str == "PAY BY CASH"){
					 
					 j$('#payment-amount-textbox').val('');
					 
                     j$("[id$=error_message]").html("Please proceed to RoC counter to deposit the cash amount. Once the cash amout is recieved the account balance will be updated.");
                     
					 loadInfoDialog();
					 disableFields();
      
                     
                 }else if(str == "SELECT ONE"){
					 
					 disableFields();
				 }
				 else{
                    
						enableFields();
                     
                  }
                    
             });
             
    }
	
	
	//This is used to display the dialog box
	function loadInfoDialog(){
	
	  j$('#popup_box').dialog({
		  modal: true,
		  overlay: {
			opacity: 0.7
		} ,
		position: {
               my: "center",
			   at: "center",
			   of: j$('.c-menu__items')
                
         },
		
		 open: function () {
			var $parent = j$( this ).parent();
			var $button = $parent.find( ".ui-dialog-titlebar-close" );
			j$(this).parents(".ui-dialog:first").find(".ui-dialog-titlebar").addClass("dialog-title-bar");
			j$(this).parents(".ui-dialog").css('padding',0);
			j$(this).parents(".ui-dialog .ui-dialog-content").css('padding-top',0);
			j$(this).parents(".ui-dialog").css('border-radius',0);
			$parent.find(".ui-corner-all").removeClass("ui-corner-all");
			
			j$('.ui-dialog .ui-dialog-title').addClass('dialog-title');
			j$('.ui-dialog .ui-dialog-title').css('width','50%');
			$button.removeClass( "ui-dialog-titlebar-close" )
			   .css( "float", "right" )
			   .button( "option", {
				   icons: {primary: false},
				   text: true,
				   label: "X"
			   });
			 $button.css('border','#1D2859');
			 j$('.ui-button-text-only .ui-button-text').css('padding',0);
			 j$('.ui-button-text-only .ui-button-text').css('padding-left',10);
			 j$('.ui-button-text-only .ui-button-text').css('padding-top',6);
			 j$('.ui-button-text-only .ui-button-text').addClass('dialog-close');
			 j$('.ui-widget-overlay ').css('opacity',0.7);
			 j$('.ui-widget-overlay').css('background', '#000000');
			 j$('.ui-widget-content').css('border','none');
			 j$('.ui-widget-content').css('border-radius',0);
			 j$('.ui-widget-header').css('border','none');
			 var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;	
			 var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
			 var popupbox_height = j$('#popup_box').height();
			 
			 //300px is the width of the dialog box.(screen width - width of dialogbox)/2 gives the left margin.
			 j$('.ui-dialog').css('left',(width-300)/2); 
			 
			 var iOS = /iPhone|iPod/.test(navigator.userAgent);
			 //for iOS device if top is added it is not getting centered
			 if(!iOS)
			 j$('.ui-dialog').css('top',(height-popupbox_height)/2); 
			 
			
		}
	  });
		
	}
	
	function disableFields(){
		
		j$('#portaltopupbutton').attr('disabled',true);
        j$('#payment-amount-textbox').attr('disabled',true);

		
	}
	
	function enableFields(){
		
		j$('#portaltopupbutton').attr('disabled',false);
        j$('#payment-amount-textbox').attr('disabled',false);

	}
	
	function showConfirmDialog(){
		
		j$('#confirm').dialog({
		 modal: true,
		  overlay: {
			opacity: 0.7
		} ,
		position: {
               my: "center",
			   at: "center",
			   of: j$('.c-menu__items')
                
         },
		 open: function () {
			var $parent = j$( this ).parent();
			var $button = $parent.find( ".ui-dialog-titlebar-close" );
			j$(this).parents(".ui-dialog:first").find(".ui-dialog-titlebar").addClass("dialog-title-bar");
			j$(this).parents(".ui-dialog").css('padding',0);
			j$(this).parents(".ui-dialog .ui-dialog-content").css('padding-top',0);
			j$(this).parents(".ui-dialog").css('border-radius',0);
			$parent.find(".ui-corner-all").removeClass("ui-corner-all");
			
			j$('.ui-dialog .ui-dialog-title').addClass('dialog-title');
			j$('.ui-dialog .ui-dialog-title').css('width','50%');
			$button.removeClass( "ui-dialog-titlebar-close" )
			   .css( "float", "right" )
			   .button( "option", {
				   icons: {primary: false},
				   text: true,
				   label: "X"
			   });
			 $button.css('border','#1D2859');
			  j$('.ui-button-text-only .ui-button-text').css('padding',0);
			 j$('.ui-button-text-only .ui-button-text').css('padding-left',10);
			 j$('.ui-button-text-only .ui-button-text').css('padding-top',6);
			 j$('.ui-button-text-only .ui-button-text').addClass('dialog-close');
			 j$('.ui-widget-overlay ').css('opacity',0.7);
			 j$('.ui-widget-overlay').css('background', '#000000');
			 j$('.ui-widget-content').css('border','none');
			 j$('.ui-widget-content').css('border-radius',0);
			 j$('.ui-widget-header').css('border','none');
			 
			 var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;	
			 var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
			 var popupbox_height = j$('#popup_box').height();
			 
			 //300px is the width of the dialog box.(screen width - width of dialogbox)/2 gives the left margin.
			 j$('.ui-dialog').css('left',(width-300)/2); 
			 j$('.ui-dialog').css('top',(height-popupbox_height)/2); 
			 
		}
	  });
	}
    
    
    function pay_by_card(amount){
			
			var pattern = new RegExp("^[0-9]+(\.[0-9]{1,2})?$");
			debugger;
		   if(pattern.test(amount)){
			   j$("[id$=warning_message]").html("Please note a service charge of 2.5% of the top-up amount will be charged on your card.\n\n Do you want to proceed?");
			   
			   showConfirmDialog();
			  
			   j$('#confirm-ok').click(function(){
				   try {
					
					 j$('#payment-amount-textbox').val('');
					 
					 j$('#confirm').dialog('close');

					 var reqval = sforce.apex.execute("CLS_Payment","Paymentformsubmit",{accId:CustomerId,amt:amount,serv:"PortalDeposit"}),
					 loc =  PaymentGateway_URL, //"{!$Label.Transaction_URL}";
					 form = document.createElement("form");
					
					var req = document.createElement("input");
					req.setAttribute("type","hidden");
					req.setAttribute("name","requestParameter");
					req.setAttribute("value",reqval);
					form.appendChild(req);
					
					form.setAttribute("method", "post");
					form.setAttribute("action", loc);
					//console.log(form.serialize());
					form.setAttribute("target", "formresult");
					
					
					//j$('.o-wrapper').append(form);
					window.parent.document.body.appendChild(form);
					document.getElementsByTagName('body')[0].appendChild(form);
					//window.open("https://www.google.com", "formresult");
					form.submit();
					//form.submit(function(){
						/*j$.ajax({
						  url: loc,//form.attr('action'),
						  type: 'post',
						  useDefaultXhrHeader : false,
						  dataType: 'jsonp',
						  data : "requestParameter="+reqval,
						  success: function(response){
							console.log('form submitted.');
							//console.log(response);
							j$('.o-wrapper').html(response);
						  }
						});*/
						//return false;
					//});
				}catch(ex){
					console.log("Payment Submit error : "  + ex);
					j$("[id$=error_message]").html("Sorry! Something wrong happened. Please inform DIFC Support team about the issue.");
					
					loadInfoDialog();
				
				}

			  });
			  
			  j$('#confirm-cancel').click(function(){
				   j$('#confirm').dialog('close');
			  });
			
			
		   }
		   else{
			   
			   j$("[id$=error_message]").html("Please enter a valid amount");
			   loadInfoDialog();
			  
		   }
        
            
    }
    
    function pay_by_cheque(amount){
		
		debugger;
		var pattern = new RegExp("^[0-9]+(\.[0-9]{1,2})?$");
		
		if(pattern.test(amount)){
			   j$('#payment-amount-textbox').val('');
				if(!window.location.origin)
					window.location.origin = window.location.protocol+"//"+window.location.host;
				/*var receipt_url = window.location.origin + UserContext.siteUrlPrefix + '/'+ ReceiptPrefix + '/' +
								'e?receipt_id='+ receipt_amount_id +'&recipt_type=Cheque&amount=' + amount + '&retURL='+encodeURIComponent('/customers/MobileHome')+'&saveURL='+ encodeURIComponent('/customers/MobileHome');*/
				var receipt_url = window.location.origin + UserContext.siteUrlPrefix + '/'+ ReceiptPrefix + '/' +
								'e?receipt_id='+ receipt_amount_id +'&recipt_type=Cheque&amount=' + amount + '&retURL='+encodeURIComponent(window.location.pathname) + encodeURIComponent(window.location.search);				
				console.log(receipt_url);
				window.parent.location = receipt_url;
				
				
		   }
		    else{
			   
			   j$("[id$=error_message]").html("Please enter a valid amount");
			   loadInfoDialog();
			   
		   }
    }

    
    return {
        init: setup
    };
    
}()); 
/********************************************************************************/
    