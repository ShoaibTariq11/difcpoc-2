 /**
	* Push left instantiation and action used for menu
	*/

var pushLeft;
var j$ = jQuery.noConflict();
var body ;
var wrapper ;
var menu ;
var closeBtn;


 var j$ = jQuery.noConflict();
     

(function(window) {
	
	
  'use strict';
  
  var is_menu_open;

  /**
   * Extend Object helper function.
   */
  function extend(a, b) {
    for(var key in b) { 
      if(b.hasOwnProperty(key)) {
        a[key] = b[key];
      }
    }
    return a;
  }

  /**
   * Each helper function.
   */
  function each(collection, callback) {
    for (var i = 0; i < collection.length; i++) {
      var item = collection[i];
      callback(item);
    }
  }

  /**
   * Menu Constructor.
   */
  function Menu(options) {
    this.options = extend({}, this.options);
    extend(this.options, options);
    this._init();
  }

  /**
   * Menu Options.
   */
  Menu.prototype.options = {
    wrapper: '.o-wrapper',          // The content wrapper            
	menuClose:'#c-menu__close'
  };
  


  /**
   * Initialise Menu.
   */
  Menu.prototype._init = function() {
	  
	 
 body = document.body;
  wrapper = document.querySelector(this.options.wrapper);
  menu = document.querySelector('#c-menu--push-left');
   closeBtn = document.querySelector('.c-menu__close');
	is_menu_open = true;
  };


  /**
   * Open Menu or close menu.
   */
  Menu.prototype.open = function() {
	
	 if(is_menu_open){

		body.classList.add('has-active-menu');
	    wrapper.classList.add('has-push-left');
		menu.classList.add('is-active');
        
	    is_menu_open = false;
	 }
	 else{
		 
		 this.close();
		 is_menu_open = true;
	 }
  };

  /**
   * Close Menu.
   */
  Menu.prototype.close = function() {

    body.classList.remove('has-active-menu');
    wrapper.classList.remove('has-push-left');
    menu.classList.remove('is-active');
	
  };


  /**
   * Add to global namespace.
   */
  window.Menu = Menu;
  
    
    pushLeft = new Menu({
            wrapper: '.o-wrapper',
            type: 'push-left',
            menuClose:'#c-menu__close'
          });


})(window);

 j$(document).ready(function(){
	 var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
	
	//add the stylings based on device height.
	 document.getElementById('portal-top-up-container').className='portal-top-up-container';
	 if(height<600){
		 
		 
		 document.getElementById('paymentoptionlabel').className='payment-option-label';
		 document.getElementById('payment-option-dropdown').className='payment-option-dropdown';
		 document.getElementById('payment-option-spacer').className='payment-option-spacer';
		 document.getElementById('payment-amount-textbox').className='payment-amount-textbox';
		 document.getElementById('paymentamountcurrencylabel').className='payment-amount-currency-label';
		 document.getElementById('payment-option-spacer-button').className='payment-option-spacer';
		 document.getElementById('portaltopupbutton').className='portal-top-up-button';
		 j$('#paymentamountcurrencylabel').css('top',30);
		 
	 }
	 else{
		 
		 document.getElementById('paymentoptionlabel').className='payment-option-label-largescreen';
		 document.getElementById('payment-option-dropdown').className='payment-option-dropdown-largescreen';
		 document.getElementById('payment-option-spacer').className='payment-option-spacer-largescreen';
		 document.getElementById('payment-amount-textbox').className='payment-amount-textbox-largescreen';
		 document.getElementById('paymentamountcurrencylabel').className='payment-amount-currency-label-largescreen';
		 document.getElementById('payment-option-spacer-button').className='payment-option-spacer-largescreen';
		 document.getElementById('portaltopupbutton').className='portal-top-up-button-largescreen';
		 // document.getElementById('popup_box').className = 'popup_box-largescreen';
		 //document.getElementById('confirm').className = 'confirm-largescreen';
		 
		 
	 }

	  //on click of portal balance top up, show or hide the portal top up section 
		j$("#portal-balance-top-up").click(function(){

		 if(j$ ('#portal-top-up-container').css('display')=='block'){
			 
			j$ ("#portal-top-up-container").slideUp(); 
		 
		 }
		 else{
			 
			 j$ ("#portal-top-up-container").slideDown();
			 j$ ('#portal-top-up-container').css('display','block');
			
		 }
		 
		 
	 }); 
	
    //on click of menu show the menu items
	var pushLeftBtn = document.getElementById('c-button--push-left');
	body = document.body;
	wrapper = document.querySelector('.o-wrapper');
	menu = document.querySelector('#c-menu--push-left');
	closeBtn = document.querySelector('.c-menu__close');
			  pushLeftBtn.addEventListener('click', function(e) {
				
				e.preventDefault;
				pushLeft.open();
	});
 });