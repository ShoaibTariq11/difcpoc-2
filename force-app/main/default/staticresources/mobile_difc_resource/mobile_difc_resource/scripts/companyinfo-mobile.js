var j$ = jQuery.noConflict();
j$(document).ready(function(){

	//	Opens the Key Details section on load
	j$('.accordion #keydetails-anchor').addClass('active');
	j$('.accordion #keyDetails').slideDown(0).addClass('open');
	
	//Set the Visa Quota -- Value retrieved from the hidden field set in Menu Page
	j$("#divVisaQuota").text(j$("#hdnVisaQuota").val());
	
	//Set the Number Of Employees -- Value retrieved from the hidden field set in Menu Page
	console.log(j$("#hdnVisaIssued").val(),j$("#hdnPipelineQuota").val());
	j$("#noofEmp").text(j$("#hdnVisaIssued").val()+"+"+j$("#hdnPipelineQuota").val());
	
	
   function close_accordion_section() {
	   
		//	Toggle the dropdown image on close accordion section.
		//console.log("{!URLFOR($Resource.mobile_difc_resource,'mobile_difc_resource/images/drop_down.png')}")
		//j$('.accordion-image').attr('src', dropDownImageURL);
		j$('.accordion-image').removeClass('icon-21'); //Up arrow
		j$('.accordion-image').addClass('icon-11'); //Drop down image.
		
        j$('.accordion .accordion-section-title').removeClass('active');
        j$('.accordion .accordion-section-content').slideUp(100).removeClass('open');
    }
 
    j$('.accordion-section-title').click(function(e) {
        // Grab current anchor value
        var currentAttrValue = j$(this).attr('href');
		
		//	Checks the tag for active.
        if(j$(this).is('.active')) { 
            close_accordion_section();
        }else {
            close_accordion_section();
 
            // Add active class to section title
            j$(this).addClass('active');
			
			//j$(this).find('img').attr('src', dropDownUpwardsImageURL);
			j$(this).find('.accordion-image').removeClass('icon-11');
			j$(this).find('.accordion-image').addClass('icon-21');
			
			
            // Open up the hidden content panel
            j$('.accordion ' + currentAttrValue).slideDown(100).addClass('open'); 
        }
 
        e.preventDefault();
		
		j$('.operating-location-link').click(function(e) {
		 
			j$('.overlay').css({ opacity: 1 });
			j$('.overlay').css({ "visibility": "visible" });
			console.log(j$(this).find("[name='unit-section']").val());
			var addressDtl;
			//addressDtl = "Unit#: " + j$(this).find("[name='unit-section']").val() + " Floor #: " + j$(this).find("[name='floor-section']").val() +" Building #: " + j$(this).find("[name='building-section']").val();
			//j$( ".address-content" ).text(addressDtl);
			j$( ".address-content" ).find(".unit-row").text(j$(this).find("[name='unit-section']").val());
			j$( ".address-content" ).find(".floor-row").text(j$(this).find("[name='floor-section']").val());
			j$( ".address-content" ).find(".bld-row").text(j$(this).find("[name='building-section']").val());
			j$( ".address-content" ).find(".bld-area").text(j$(this).find("[name='building-occupiedArea']").val());
			//e.preventDefault();
		});
		
		j$('.popup-header-link').click(function(e) {
		 
			j$('.overlay').css({ opacity: 0 });
			j$('.overlay').css({ "visibility": "hidden" });
			e.preventDefault();
		});
    });
	
});
