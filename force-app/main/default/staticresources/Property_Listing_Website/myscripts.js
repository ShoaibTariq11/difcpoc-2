var LEASING_TYPE = "Leasing Type";
var PROPERTY_TYPE = "Property Type";
var BUILDING_NAME = "Building Name";
var SALE_OR_RENT = "Sale or Rent";
var FNB_NON_FNB = "F&B / Non F&B";

var RETAIL = "Retail";
var OFFICE = "Office";
var DATA_CENTRE = "Data Centre (Co-Location Service)";
var RESIDENTIAL = "Residential";
var BUSINESS_CENTRE = "Business Centre";
var STORAGE = "Storage";

var LONG_TERM = "Long Term";
var OFFICE_LONG_TERM = "Office Long Term";
var SHORT_TERM = "Short Term";

var AddressDetails = "AddressDetails";
var AgentDetails = "AgentDetails";
var Amenities = "Amenities";
var BuildingName = "BuildingName";
var CageNo = "CageNo";
var CompanyName = "CompanyName";
var CompanyProfile = "CompanyProfile";
var EndDate = "EndDate";
var IsFeatured = "IsFeatured";
var FnB = "FnB";
var LeasingType = "LeasingType";
var ListingDescription = "ListingDescription";
var ListingTitle = "ListingTitle";
var ListingType = "ListingType";
var Location = "Location";
var NoOfDays = "NoOfDays";
var NoOfBedRooms = "NoOfBedRooms";
var NoOfPAX = "NoOfPAX";
var Price = "Price";
var PropertyType = "PropertyType";
var SqrtFeet = "SqrtFeet";
var StartDate = "StartDate";
var Status = "Status";
var Unit = "Unit";
var WebSite = "WebSite";
var WorkPhone = "WorkPhone";
var ContactEmail = "ContactEmail";
var ContactPhone = "ContactPhone";
var ContactTimezone = "ContactTimezone";
var UnitDeliveryCondition = "UnitDeliveryCondition";
var FirstName = "FirstName";
var LastName = "LastName";
var StreetAddress = "StreetAddress";
var CreatedbyDIFCBackend = "CreatedbyDIFCBackend";
var Longitude = "Longitude";
var Latitude = "Latitude";
var FitoutManual = "FitoutManual";
var TenantManual = "TenantManual";
var ClientOffer = "ClientOffer";
var FloorPlan = "FloorPlan";


var advancedSearch = "advanced-search";
var mapView = "map";
var searchResults = "search-results";
var forSale = "/for-sale/fs-";
var forRent = "/for-rent/fr-";

var carouselDuration = 5000;
var resultsPerPage = 24;
var hdc;
var buildingHashMap = {};

function initMapView() {
    $('map').imageMapResize();
    //Get the buulding unit mapping for the tooltips
    getBuildingUnitMapping();

    //Typing effect
    $(".verticalLine").typed({
        strings: ["<h2>A comprehensive view<br/>of properties to lease<br/>and for sale in the DIFC</h2><p><b>Select a building to explore further</b></p>"],
        contentType: 'html' // or 'text'
    });

    initializeTooltips();
}

var buildingNumToUnits = {};
var buildingNumToDisplayName = {};
function getBuildingUnitMapping() {
    //console.log(BuildingJSONStr);
    var buildingDetail = [];
    for (var i = 0; i < BuildingJSONStr.length; i++) {
        var displayName = BuildingJSONStr[i]['BuildingOnWebsite'];
        if(buildingHashMap[displayName]!=null && buildingHashMap[displayName]!=undefined) {
            buildingDetail = buildingHashMap[displayName];
            buildingDetail[0] = buildingDetail[0] + "," +BuildingJSONStr[i]['BuildingNo'];
            buildingDetail[1] = Number(buildingDetail[1]) + Number(BuildingJSONStr[i]['AvailableUnits']);
            buildingDetail[2] = buildingDetail[2] + "," +BuildingJSONStr[i]['BuildingName'];
        } else {
            buildingDetail = [];
            buildingDetail[0] = BuildingJSONStr[i]['BuildingNo'];
            buildingDetail[1] = Number(BuildingJSONStr[i]['AvailableUnits']);
            buildingDetail[2] = BuildingJSONStr[i]['BuildingName'];
        }
        
        buildingHashMap[displayName] = buildingDetail;
        
    }

    //console.log(buildingHashMap);
    var buildingHashMapSize = Object.size(buildingHashMap);
    var buildingHashMapKeys = Object.keys(buildingHashMap);
    for(var j=0; j < buildingHashMapSize; j++) {
        var buildingName = buildingHashMapKeys[j];
        var numbers = buildingHashMap[buildingName][0].split(',');
        for(var k=0; k<numbers.length;k++) {
            var tmp = numbers[k];
            buildingNumToUnits[tmp] = buildingHashMap[buildingName][1];
            buildingNumToDisplayName[tmp] = buildingName;
        }
    }
    //console.log(buildingNumToUnits);
    //console.log(buildingNumToDisplayName);
}

function initializeTooltips() {
    $('area[alt]').each(function () {
        var elem = $(this),
            map = elem.parent(),
            image = $('img[usemap="#' + map.attr('name') + '"]');

        var areaId = elem.attr('building-num');
        var areaName = elem.attr('alt');
        var isRight = elem.attr('right-most');
        var isLeft = elem.attr('left-most');
        var isParkTowerLeft = elem.attr('park-tower-left');
        var isParkTowerRight = elem.attr('park-tower-right');
        var displayText;
        var tooltipClass;
        //On click, load the search page with the building field autofilled
        //alert(buildingNum);
        if (areaName.indexOf("Development") <= -1 && areaName.indexOf("Construction") <= -1) {

            $(this).attr('qtip-building', buildingNumToDisplayName[areaId]);
            elem.click(function () {
                loadUrlWithParam(buildingNumToDisplayName[areaId], buildingHashMap[buildingNumToDisplayName[areaId]][2]);
            });
            
            var tooltiptemplate = byId("tool-tip").cloneNode(true);
            tooltiptemplate.style.display = 'inline';
            tooltiptemplate.querySelector('#tooltip-building').textContent = "" + buildingNumToDisplayName[areaId];
            if(areaId != "50000000000200000009")
            tooltiptemplate.querySelector('#tooltip-units').textContent = "" + buildingNumToUnits[areaId] + " units available";
            displayText = tooltiptemplate;
            tooltipClass = 'qtip-blue';
            
        } else {
            
            var underConstruction = byId("under-construction").cloneNode(true);
            underConstruction.style.display = 'inline';

            underConstruction.getElementsByClassName('under-development')[0].innerHTML = elem.attr('alt');
            displayText = underConstruction;
            if(elem.hasClass("uc"))
                tooltipClass = 'qtip-yellow';
            else
                tooltipClass = 'qtip-orange';
        }

        var toolTipCorner = 'left center';
        var toolTipMimic = 'left bottom';
        var toolTipHieght = 20;
        var positionMy = 'left left';
        var  positionAt = 'top';
        if (isRight != undefined) {
            toolTipCorner = 'bottom center';
            toolTipMimic = 'bottom center';
            positionMy = 'bottom center';
            positionAt = 'top';
            toolTipHieght = 12;
        } else if(isLeft != undefined) {
            toolTipCorner = 'left center';
            toolTipMimic = 'left bottom';
            positionMy = 'left center';
            positionAt = 'top center';
        } else if(isParkTowerRight != undefined) {
            var toolTipCorner = 'left center';
        var toolTipMimic = 'left bottom';
            positionMy = 'left';
            positionAt = 'top right';
            toolTipHieght = 18;
        }

        if(isParkTowerLeft != undefined) {
            return true; //No tooltip for the right park tower
        }
        
        elem.qtip({
            content: {
                text: function (event, api) {
                    return displayText;
                }
            },
            position: {
                my: positionMy, // Position my top left...
                at: positionAt, // at the bottom right of...
                viewport: image
            },
            style: {
                classes: tooltipClass,
                def: true,
                tip: {
                    corner: toolTipCorner,
                    mimic: toolTipMimic,
                    color: '#fff',
                    width: 5,
                    offset: -4,
                    height: toolTipHieght
                }
            }

        });
    });

}



// shorthand func
function byId(e) {
    'use strict';
    return document.getElementById(e);
}
// takes a string that contains coords eg - "227,307,261,309, 339,354, 328,371, 240,331"
// draws a line from each co-ord pair to the next - assumes starting point needs to be repeated as ending point.
function drawPoly(coOrdStr) {
    'use strict';
    var mCoords = coOrdStr.split(','),
        i, n;
    n = mCoords.length;

    hdc.beginPath();
    hdc.moveTo(mCoords[0], mCoords[1]);
    for (i = 2; i < n; i += 2) {
        hdc.lineTo(mCoords[i], mCoords[i + 1]);
    }
    hdc.lineTo(mCoords[0], mCoords[1]);
    hdc.fill();
}

function drawRect(coOrdStr) {
    'use strict';
    var mCoords = coOrdStr.split(','),
        top, left, bot, right;
    left = mCoords[0];
    top = mCoords[1];
    right = mCoords[2];
    bot = mCoords[3];
    hdc.strokeRect(left, top, right - left, bot - top);
}

function myHovers(element) {
    if((element.className).indexOf("strip")>-1) {
        $('.strip').each(function () {
            myHover(this);
        });
    } else if((element.className).indexOf("park-tower")>-1) {
        $('.park-tower').each(function () {
            var isParkTowerRight = $(this).attr("park-tower-right");
            if(isParkTowerRight != undefined) { //Even for the right park tower, show the left one's tooltip
                $(this).trigger('mouseenter');
            }        
            myHover(this);
        });
    } else if((element.className).indexOf("currency-house")>-1) {
        $('.currency-house').each(function () {
            myHover(this);
        });
    } else if((element.className).indexOf("central-park-comm")>-1) {
        $('.central-park-comm').each(function () {
            myHover(this);
        });
    } 
    else
        myHover(element);
}

function myHover(element) {
    'use strict';
    var hoveredElement = element,
        coordStr = element.getAttribute('coords'),
        areaType = element.getAttribute('shape');

    var building = element.getAttribute('qtip-building');
    if (building == undefined && (element.className).indexOf("uc")<=-1) { //Orange color for future development building
        hdc.fillStyle = '#FF6600';
        hdc.strokeStyle = '#FF6600';
        hdc.globalAlpha = 0.6;
    } else if (building == undefined && (element.className).indexOf("uc")>-1) {
        hdc.fillStyle = '#00a3e0';
        hdc.strokeStyle = '#00a3e0';
        hdc.globalAlpha = 0.6;
    } else { // Green for others
        hdc.fillStyle = '#bcd19b';
        hdc.strokeStyle = '#bcd19b';
        hdc.globalAlpha = 0.7;
    }

    switch (areaType) {
        case 'polygon':
        case 'poly':
            drawPoly(coordStr);
            break;
        case 'rect':
            drawRect(coordStr);
            break;
    }
}

function leaveParkTower(element) {
    $('.park-tower').each(function () {
        var isParkTowerRight = $(this).attr("park-tower-right");
        if(isParkTowerRight != undefined) {
            $(this).trigger('mouseleave');
        }
    });
    myLeave();
}

function myLeave() {
    'use strict';
    var canvas = byId('myCanvas');
    hdc.clearRect(0, 0, canvas.width, canvas.height);
}

function myInit() {
    'use strict';
    // get the target image
    var img = byId('image3d'),
        x, y, w, h, imgParent = img.parentNode,
        can = byId('myCanvas');

    // get it's position and width+height
    x = img.offsetLeft;
    y = img.offsetTop;
    w = img.clientWidth;
    h = img.clientHeight;

    // move the canvas, so it's contained by the same parent as the image
    imgParent.appendChild(can);

    // place the canvas in front of the image
    can.style.zIndex = 1;

    // position it over the image
    can.style.left = x + 'px';
    can.style.top = y + 'px';

    // make same size as the image
    can.setAttribute('width', w + 'px');
    can.setAttribute('height', h + 'px');
    can.globalAlpha = 0.2;

    // get it's context
    hdc = can.getContext('2d');
    hdc.globalAlpha = 0.2;

    // set the 'default' values for the colour/width of fill/stroke operations

    hdc.lineWidth = 2;
}

function switchToSearchMode() {
    dataLayer.push({
        'event': 'switch-to-advanced-click'
    });
    window.open(getBaseUrl() + '/' + advancedSearch, "_self");
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};
function initializeDynamicSearch() {
    getBuildingUnitMapping();
    fillBuildingNames();
    showHideProperties(document.getElementById("propertyType").value);
    //Listener to change search options dynamically
    $('#propertyType').change(function () {
        var e = document.getElementById("#propertyType");
        var type = $(this).find("option:selected").val();
        showHideProperties(type);
    });

    $('#leasingType').change(function () {
        var e = document.getElementById("#leasingType");
        var type = $(this).find("option:selected").val();
        leaseTypeChanged(type);
    });
}

function fillBuildingNames() {
    var buildingName = document.getElementById("buildingName");
    var buildingList = Object.keys(buildingHashMap);
    var buildingNos = Object.size(buildingHashMap);
    
    for (var i = 0; i < buildingNos; i++) {
        var opt = document.createElement('option');
        var name = buildingList[i];
        if(name != null && name != 'null') {
            opt.value = name;
            opt.innerHTML = name;
            buildingName.appendChild(opt);
        }
    }
}

function leaseTypeChanged(type) {
//    if (type == BUSINESS_CENTRE) {
//        $("#bedrooms").addClass('hidden');
//        $("#pax").removeClass('hidden');
//        $("#blank").addClass('hidden');
//        $("#fnb").addClass('hidden');
//    } else if (type == OFFICE_LONG_TERM) {
//        $("#bedrooms").addClass('hidden');
//        $("#pax").addClass('hidden');
//        $("#blank").removeClass('hidden');
//        $("#fnb").addClass('hidden');
//    }
}

function showHideProperties(type) {
    //clear lease type and populate it according to property type
    var leasingType = document.getElementById("leasingType");
    leasingType.value = "Leasing Type";
    var length = leasingType.length;
    $('#leasingType')
        .find('option')
        .remove()
        .end()
        .append('<option value="Leasing Type">Leasing Type</option>')
        .val('Leasing Type');

    var opt = document.createElement('option');
    opt.value = '---------';
    opt.innerHTML = '---------';
    opt.setAttribute("disabled", "disabled");
    opt.className += " select-dash";
    leasingType.appendChild(opt);

    leasingType.disabled = false;
    if (type == OFFICE) {
        $("#bedrooms").addClass('hidden');
        $("#pax").addClass('hidden');
        $("#blank").removeClass('hidden');
        $("#fnb").addClass('hidden');
        $("#cage").addClass('hidden');

        var opt = document.createElement('option');
        opt.value = OFFICE_LONG_TERM;
        opt.innerHTML = OFFICE_LONG_TERM;
        leasingType.appendChild(opt);

//        opt = document.createElement('option');
//        opt.value = BUSINESS_CENTRE;
//        opt.innerHTML = BUSINESS_CENTRE;
//        leasingType.appendChild(opt);

    } else if (type == RESIDENTIAL) {
        $("#bedrooms").removeClass('hidden');
        $("#pax").addClass('hidden');
        $("#blank").addClass('hidden');
        $("#fnb").addClass('hidden');
        $("#cage").addClass('hidden');
        var opt = document.createElement('option');
        opt.value = LONG_TERM;
        opt.innerHTML = LONG_TERM;
        leasingType.appendChild(opt);
    } else if (type == RETAIL) {
        $("#fnb").removeClass('hidden');
        $("#blank").addClass('hidden');
        $("#pax").addClass('hidden');
        $("#bedrooms").addClass('hidden');
        $("#cage").addClass('hidden');
        var opt = document.createElement('option');
        opt.value = LONG_TERM;
        opt.innerHTML = LONG_TERM;
        leasingType.appendChild(opt);
    } else if (type == DATA_CENTRE) {
        $("#fnb").addClass('hidden');
        $("#blank").addClass('hidden');
        $("#pax").addClass('hidden');
        $("#bedrooms").addClass('hidden');
        $("#cage").removeClass('hidden');
        var opt = document.createElement('option');
        opt.value = LONG_TERM;
        opt.innerHTML = LONG_TERM;
        leasingType.appendChild(opt);
    } else if (type == BUSINESS_CENTRE) {
        $("#fnb").addClass('hidden');
        $("#blank").addClass('hidden');
        $("#pax").removeClass('hidden');
        $("#bedrooms").addClass('hidden');
        $("#cage").addClass('hidden');
        var opt = document.createElement('option');
        opt.value = SHORT_TERM;
        opt.innerHTML = SHORT_TERM;
        leasingType.appendChild(opt);
    } else {
        $("#blank").removeClass('hidden');
        $("#pax").addClass('hidden');
        $("#bedrooms").addClass('hidden');
        $("#fnb").addClass('hidden');
        $("#cage").addClass('hidden');
        var opt = document.createElement('option');
        opt.value = LONG_TERM;
        opt.innerHTML = LONG_TERM;
        leasingType.appendChild(opt);

        if (type == "Property Type")
            leasingType.disabled = true;
    }
}

function initializeCarousel(featuredPerPage, aboutPerPage) {
    generateCarousel(featuredPerPage, "featured");
    generateCarousel(aboutPerPage, "about");
    $(".carousel").swiperight(function () {
        $(this).carousel('prev');
    });
    $(".carousel").swipeleft(function () {
        $(this).carousel('next');
    });
}

function switchToMapMode() {
    dataLayer.push({
        'event': 'switch-to-map-click'
    });
    window.open(getBaseUrl() + '/' + mapView, "_self");
}

function search() {
    var parameters = getCurrentSearchParameters();
    if (parameters == 0)
        return;
    parameters = parameters + "&filter=published&page=1";
    window.open(getBaseUrl() + '/' + searchResults + "?" + parameters, "_self");
}

function onClear() {
    var sPageURL = window.location.href.split('?')[0];
    window.open(sPageURL, "_self");
}

function openClientPortal() {
    dataLayer.push({
        'event': 'list-property-click'
    });
    if(window.location.href.indexOf("uatfull") > -1)
        window.open("https://uatfull-difcportal.cs17.force.com/customers/signin", "_self");
    else
        window.open("http://portal.difc.ae", "_self");
}

function getCurrentSearchParameters() {
    var parameters;
    var buildingName = document.getElementById('buildingName').value;
    var keyword = document.getElementById('keyword').value;
    var rentOrSale = document.getElementById('rentOrSale').value;
    var propertyType = document.getElementById('propertyType').value;
    var minPrice = document.getElementById('minPrice').value;
    var maxPrice = document.getElementById('maxPrice').value;
    var minSize = document.getElementById('minSize').value;
    var maxSize = document.getElementById('maxSize').value;
    var pax = document.getElementById('paxNum').value;
    var bedroom = document.getElementById('bedroomsNum').value;

    if (isNaN(minPrice) || isNaN(maxPrice) || isNaN(minSize) || isNaN(maxSize) || isNaN(pax) || isNaN(bedroom)) {

        alert("Please enter a valid number");
        return 0;
    }
    var leasingType = document.getElementById('leasingType').value;

    rentOrSale = (rentOrSale == SALE_OR_RENT) ? '' : rentOrSale;
    propertyType = (propertyType == PROPERTY_TYPE) ? '' : propertyType;
    leasingType = (leasingType == LEASING_TYPE) ? '' : leasingType;
    buildingName = (buildingName == BUILDING_NAME) ? '' : buildingName;

    parameters = "keyword=" + keyword;
    parameters += "&" + "rentOrSale=" + rentOrSale;
    parameters += "&" + "buildingName=" + buildingName;
    parameters += "&" + "propertyType=" + propertyType;
    parameters += "&" + "minPrice=" + minPrice;
    parameters += "&" + "maxPrice=" + maxPrice
    parameters += "&" + "minSize=" + minSize;
    parameters += "&" + "maxSize=" + maxSize;
    parameters += "&" + "leasingType=" + leasingType;
    
    /*if(buildingHashMap[buildingName] != undefined)
        parameters += "&" + "buildingNames=" + buildingHashMap[buildingName][2];*/

    if (propertyType == RESIDENTIAL)
        parameters += "&" + "bedroomsNum=" + document.getElementById('bedroomsNum').value;
    if (propertyType == RETAIL) {
        var fnbNonFnb = document.getElementById('fnbNonFnb').value;
        fnbNonFnb = (fnbNonFnb == FNB_NON_FNB) ? '' : fnbNonFnb;
        parameters += "&" + "fnbNonFnb=" + fnbNonFnb;
    }
    if (propertyType == DATA_CENTRE) {
        parameters += "&" + "cageNo=" + document.getElementById('cageNum').value;
    }
    if (propertyType == BUSINESS_CENTRE)
        parameters += "&" + "pax=" + document.getElementById('paxNum').value;

    dataLayer.push({
        'event': 'search-click',
        'buildingName': buildingName,
        'keyword': keyword,
        'rentOrSale': rentOrSale,
        'propertyType': propertyType,
        'minPrice': minPrice,
        'maxPrice': maxPrice,
        'minSize': minSize,
        'maxSize': maxSize,
        'leasingType': leasingType
    });
    return parameters;
}

function generateCarousel(num, type) {

    $('#featured-carousel').carousel({
        interval: carouselDuration
    });
    $('#about-carousel').carousel({
        interval: carouselDuration
    }); 

    //Get the list of total featured properties
    var properties, featuredCount;
    var carousel, indicator, target;
    if (type == "featured") {
        carousel = $('#featured-carousel-inner');
        indicator = $('#featured-carousel-indicators');
        target = '#featured-carousel';
        properties = featuredJSONStr;
        featuredCount = properties.length;
    } else {
        carousel = $('#about-carousel-inner');
        indicator = $('#about-carousel-indicators');
        target = '#about-carousel';
        properties = getAboutDIFCProperties();
        featuredCount = properties.length;
    }

    var pages = featuredCount / num;
    while (carousel.firstChild) {
        carousel.removeChild(carousel.firstChild);
    }
    for (var i = 0; i < pages; i++) {
        var item = document.createElement('div');
        item.className += 'item';

        if (i == 0)
            item.className += ' active';
        else {
            var li = document.createElement('li');
            li.setAttribute('data-target', target);
            li.setAttribute('data-slide-to', i);
            indicator.append(li);
        }
        var container = document.createElement('div');
        container.className += ' ';
        var row = document.createElement('div');
        row.className += 'row';

        var entries = Math.min(num, featuredCount - num * i);

        for (var e = 0; e < entries; e++) {
            var col = document.createElement('div');
            col.className += ' col';
            col.className += ' col-xs-12 col-sm-4 col-md-4 col-lg-4';

            if (e == 3) {
                container.appendChild(row);
                row = document.createElement('div');
                row.className += 'row';
            }
            if (type == "featured")
                var tmpl = getFeaturedTemplate(properties[i * num + e]);
            else
                var tmpl = getAboutDIFCTemplate(properties[i * num + e]);

            col.appendChild(tmpl);

            row.appendChild(col);
        }

        container.appendChild(row);
        item.appendChild(container);
        carousel.append(item);

    }

    if (pages <= 1) {
        indicator.hide();
        $('.carousel-control').hide();
    }
}

function getFeaturedTemplate(property) {
    var tmpl = document.getElementById('featured-template').cloneNode(true);
    tmpl.classList.remove('template');
    tmpl.querySelector('#featured-heading').textContent = property['ListingTitle'];
    tmpl.querySelector('#featured-description').textContent = property['PropertyType'] + " - " + numberWithCommas(property['Price']) + " AED";
    tmpl.querySelector('#featured-image').src = property['Photo1'];
    tmpl.querySelector('#propertyId').textContent = property['RecordId'];
    tmpl.querySelector('#leasing-type').textContent = property['ListingType'];
    return tmpl;
}

function getAboutDIFCTemplate(property) {
    var tmpl = document.getElementById('about-difc-template').cloneNode(true);
    tmpl.classList.remove('template');
    tmpl.querySelector('h2').textContent = property.heading;
    tmpl.querySelector('h5').textContent = property.description;
    tmpl.querySelector('.image-container').style.backgroundImage = 'url(' + property.src + ')';
    tmpl.style.cursor = 'pointer';
    tmpl.onclick = function () {
        window.open(property['url'], '_self');
    };
    return tmpl;
}

function findBootstrapEnvironment() {
    var envs = ['xs', 'sm', 'md', 'lg'];

    var $el = $('<div>');
    $el.appendTo($('body'));

    for (var i = envs.length - 1; i >= 0; i--) {
        var env = envs[i];

        $el.addClass('hidden-' + env);
        if ($el.is(':hidden')) {
            $el.remove();
            return env;
        }
    }
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function getBaseUrl() {
    pathArray = location.href.split( '/' );
    protocol = pathArray[0];
    host = pathArray[2];
    url = protocol + '//' + host;
    return url;
}
;/*! Image Map Resizer
 *  Desc: Resize HTML imageMap to scaled image.
 *  Copyright: (c) 2014-15 David J. Bradshaw - dave@bradshaw.net
 *  License: MIT
 */

(function(){
    'use strict';

    function scaleImageMap(){

        function resizeMap() {
            function resizeAreaTag(cachedAreaCoords){
                function scaleCoord(e){
                    return e * scallingFactor[(1===(isWidth = 1-isWidth) ? 'width' : 'height')];
                }

                var isWidth = 0;

                return cachedAreaCoords.split(',').map(Number).map(scaleCoord).map(Math.floor).join(',');
            }

            var scallingFactor = {
                width  : displayedImage.width  / sourceImage.width,
                height : displayedImage.height / sourceImage.height
            };

            for (var i=0; i < areasLen ; i++) {
                areas[i].coords = resizeAreaTag(cachedAreaCoordsArray[i]);
            }
        }

        function start(){
            var
                displayedWidth  = null,
                displayedHeight = null;

            //WebKit asyncs image loading, so we have to catch the load event.
            sourceImage.onload = function sourceImageOnLoadF(){
                displayedWidth = displayedImage.width;
                displayedHeight = displayedImage.height;

                if ((displayedWidth !== sourceImage.width) || (displayedHeight !== sourceImage.height)) {
                    resizeMap();
                }
            };

            //IE11 can late load this image, so make sure we have the correct sizes (#10)
            displayedImage.onload = function() {
                if (null !== displayedWidth && displayedImage.width !== displayedWidth) {
                    resizeMap();
                }
            };

            //Make copy of image, so we can get the actual size measurements
            sourceImage.src = displayedImage.src;
        }

        function listenForResize(){
            function debounce() {
                clearTimeout(timer);
                timer = setTimeout(resizeMap, 250);
            }
            if (window.addEventListener) { window.addEventListener('resize', debounce, false); }
            else if (window.attachEvent) { window.attachEvent('onresize', debounce); }
        }

        function listenForFocus(){
            if (window.addEventListener) { window.addEventListener('focus', resizeMap, false); }
            else if (window.attachEvent) { window.attachEvent('onfocus', resizeMap); }
        }

        function getCoords(e){
            // normalize coord-string to csv format without any space chars
            return e.coords.replace(/ *, */g,',').replace(/ +/g,',');
        }

        var
            /*jshint validthis:true */
            map                   = this,
            areas                 = map.getElementsByTagName('area'),
            areasLen              = areas.length,
            cachedAreaCoordsArray = Array.prototype.map.call(areas, getCoords),
            displayedImage        = document.querySelector('img[usemap="#'+map.name+'"]'),
            sourceImage           = new Image(),
            timer                 = null;

        start();
        listenForResize();
        listenForFocus();
    }



    function factory(){
        function init(element){
            if(!element.tagName) {
                throw new TypeError('Object is not a valid DOM element');
            } else if ('MAP' !== element.tagName.toUpperCase()) {
                throw new TypeError('Expected <MAP> tag, found <'+element.tagName+'>.');
            }

            scaleImageMap.call(element);
        }

        return function imageMapResizeF(target){
            switch (typeof(target)){
                case 'undefined':
                case 'string':
                    Array.prototype.forEach.call(document.querySelectorAll(target||'map'),init);
                    break;
                case 'object':
                    init(target);
                    break;
                default:
                    throw new TypeError('Unexpected data type ('+typeof(target)+').');
            }
        };
    }


    if (typeof define === 'function' && define.amd) {
        define([],factory);
    } else if (typeof exports === 'object') { //Node for browserfy
        module.exports = factory();
    } else {
        window.imageMapResize = factory();
    }


    if('jQuery' in window) {
        jQuery.fn.imageMapResize = function $imageMapResizeF(){
            return this.filter('map').each(scaleImageMap).end();
        };
    }

})();

var urlParameters = {
    'keyword': '',
    'rentOrSale': '',
    'buildingName': '',
    'propertyType': '',
    'minPrice': '',
    'maxPrice': '',
    'minSize': '',
    'maxSize': '',
    'leasingType': '',
    'bedroomsNum': '',
    'cageNo':'',
    'pax': '',
    'fnbNonFnb': '',
    'filter': 'published',
    'page': '1'
};
var currentPage = 0;
var pageCount = 0;
var featuredResults;

function initSearchResults() {
    fillSearch();
    featuredResults = featuredJSONStr;
    showFeaturedResults();
    showSearchResults();
    initializeFilterAndPage();

    $(".carousel").swiperight(function () {
        $(this).carousel('prev');
    });
    $(".carousel").swipeleft(function () {
        $(this).carousel('next');
    });
}

function fillSearch() {

    getUrlParameters();
    document.getElementById('keyword').value = urlParameters['keyword'];
    document.getElementById('rentOrSale').value = urlParameters['rentOrSale'] == '' ? SALE_OR_RENT : urlParameters['rentOrSale'];
    document.getElementById('propertyType').value = urlParameters['propertyType'] == '' ? PROPERTY_TYPE : urlParameters['propertyType'];
    document.getElementById('minPrice').value = urlParameters['minPrice'];
    document.getElementById('maxPrice').value = urlParameters['maxPrice'];
    document.getElementById('minSize').value = urlParameters['minSize'];
    document.getElementById('maxSize').value = urlParameters['maxSize'];

    var propertyType = urlParameters['propertyType'];
    if (propertyType == RESIDENTIAL)
        document.getElementById('bedroomsNum').value = urlParameters['bedroomsNum'];
    else if (propertyType == RETAIL) {
        document.getElementById('fnbNonFnb').value = urlParameters['fnbNonFnb'] == '' ? FNB_NON_FNB : urlParameters['fnbNonFnb'];
    } else if (propertyType == BUSINESS_CENTRE)
        document.getElementById('paxNum').value = urlParameters['pax'];
    else if (propertyType == DATA_CENTRE) {
        document.getElementById('cageNum').value = urlParameters['cageNo'];
    }

    initializeDynamicSearch();
    document.getElementById('leasingType').value = urlParameters['leasingType'] == '' ? LEASING_TYPE : urlParameters['leasingType'];
    leaseTypeChanged(urlParameters['leasingType']);
    document.getElementById('buildingName').value = urlParameters['buildingName'] == '' ? BUILDING_NAME : urlParameters['buildingName'];

}

function getUrlParameters() {
    var sPageURL = window.location.search.substring(1);
    sPageURL = decodeURIComponent(sPageURL);
    sPageURL = sPageURL.replace(/\+/g, ' ');
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        urlParameters[sParameterName[0]] = sParameterName[1];
    }
}

function loadUrl() {
    var parameters = getParameterString();
    window.open(getBaseUrl() + '/' + searchResults + "?" + parameters, "_self");
}

function loadUrlWithParam(buildingName, buildingNames) {
    urlParameters['buildingName'] = buildingName;
    /*if(buildingNames != undefined)
        urlParameters['buildingNames'] = buildingNames;*/
    var parameters = getParameterString();
    window.open(getBaseUrl() + '/' + searchResults + "?" + parameters, "_self");
}

function getParameterString() {
    var keys = (Object.keys(urlParameters));
    var parameters = "";
    for (var i = 0; i < keys.length; i++) {
        parameters += "&" + keys[i] + "=" + encodeURIComponent(urlParameters[keys[i]]);
    }
    return parameters;
}

function onPrevious() {
    if (currentPage != 1) {
        urlParameters['page'] = parseInt(currentPage) - 1;
        loadUrl();
    }
}

function onNext() {
    if (currentPage != pageCount) {
        urlParameters['page'] = parseInt(currentPage) + 1;
        loadUrl();
    }
}

function initializeFilterAndPage() {
    document.getElementById(urlParameters['filter']).className += " active";

    var filterButton = null;
    var ele = document.querySelectorAll(".filter-button > .btn");
    for (var i = 0; i < ele.length; i++) {
        ele[i].addEventListener("click", function () {
            filterButton = this.id;
            urlParameters['filter'] = filterButton;
            urlParameters['page'] = 1;
            loadUrl();
        });
    }

    var page = null;
    var ele = document.querySelectorAll(".pagination-button > .btn");
    for (var i = 0; i < ele.length; i++) {
        ele[i].addEventListener("click", function () {
            page = this.getAttribute('value');
            urlParameters['page'] = page;
            loadUrl();
        });
    }

    if (currentPage == 1)
        enableButton(false, document.querySelectorAll("#previous-button"));
    else
        enableButton(true, document.querySelectorAll("#previous-button"));

    if (currentPage == pageCount)
        enableButton(false, document.querySelectorAll("#next-button"));
    else
        enableButton(true, document.querySelectorAll("#next-button"));
}

function enableButton(enable, elem) {
    for (var i = 0; i < elem.length; i++) {
        elem[i].disabled = !enable;
        var color = enable ? "#141b4d" : "#a7a8aa";
        elem[i].querySelector("p").style.color = color;
        elem[i].querySelector(".icon-font").style.color = color;
    }
}


function showSearchResults() {

    var resultCount = searchResultCount;

    //Add pages in pagination
    addAndShowCurrentPage(urlParameters['page'], Math.ceil(resultCount / resultsPerPage)); 

    var results = resultsJSONStr;
    //Update count in UI
    document.getElementById('search-count').innerHTML = "<b>" + results.length + " out of " + resultCount + " results</b>";

    var searchResultContainer = document.getElementById("search-result-container");

    if (resultCount == 0) {
        displayNoResultContent();
    } else {
        var row;
        row = document.createElement('div');
        row.className+='row';
        for (var i = 0; i < results.length; i++) {
            var col = document.createElement('div');
            col.className += 'col col-xs-10 col-xs-offset-1 col-sm-offset-0 col-sm-4 col-md-4 col-lg-4';
            var tmpl = getPropertyTile(results[i]);
            col.appendChild(tmpl);
            row.appendChild(col);
        }
        searchResultContainer.appendChild(row);
        clonePagination();
    }
}

function displayNoResultContent() {

    if (isResults == false) {
        $('#no-content-container').css("display", "inherit");
        resizeCaptchaSearchResult();
        $(window).resize(function () {
            resizeCaptchaSearchResult();
        });
    }
    //Hide previos and next button
    $('#previous-button').css("display", "none");
    $('#next-button').css("display", "none");
    $('.search-result-container').css("display", "none");
    $('.search-filter-container').hide();
}

function addAndShowCurrentPage(currentPage, pageCount) {

    this.currentPage = currentPage;
    this.pageCount = pageCount;

    var showDots = true;
    var paginationContainer = $('.pagination-button');

    var firstPage = Math.max(currentPage - 1, 1);
    var lastPage = Math.min(firstPage + 2, pageCount);

    if (currentPage == pageCount && pageCount >= 3)
        firstPage = currentPage - 2;

    //Show dots or not
    if (pageCount <= 3 || currentPage == pageCount || lastPage == pageCount) {
        showDots = false;
    }

    for (var i = firstPage; i <= lastPage; i++) {
        var label = document.createElement('label');
        label.className += "btn btn-primary btn-responsive";

        if (i == currentPage)
            label.className += " active";
        var input = document.createElement('input');
        input.setAttribute('type', 'radio');
        label.innerHTML = i;
        label.setAttribute('value', i);
        label.appendChild(input);
        paginationContainer.append(label);
    }

    /*    if(showDots == true) {
            var div = document.createElement('div');
            var dots = document.createElement('p');
            dots.innerHTML = "...";
            div.appendChild(dots);
            paginationContainer.appendChild(div);
        }*/
}

function showFeaturedResults() {

    $('.carousel').carousel({
        interval: carouselDuration
    });

    var num; //featured prop per page
    if (findBootstrapEnvironment() == "xs") {
        num = 1;
    } else
        num = 3;


    var featuredCount = featuredResults.length;

    if (featuredCount == 0) {
        $('#featured-gray-background').hide();
        return;
    }

    var pages = featuredCount / num;
    var carousel = $('#featured-result-carousel-inner');

    for (var i = 0; i < pages; i++) {
        var item = document.createElement('div');
        item.className += 'item';

        if (i == 0) //Activate the first page
            item.className += ' active';

        var container = document.createElement('div');
        container.className += ' ';
        var row = document.createElement('div');
        row.className += 'row';

        var entries = Math.min(num, featuredCount - num * i);

        for (var e = 0; e < entries; e++) {
            var col = document.createElement('div');
            col.className += 'col col-xs-10 col-xs-offset-1 col-sm-offset-0 col-sm-4 col-md-4 col-lg-4';

            var tmpl = getPropertyTile(featuredResults[i * num + e]);
            col.appendChild(tmpl);
            row.appendChild(col);
        }

        container.appendChild(row);
        item.appendChild(container);
        carousel.append(item);
    }

    if (pages <= 1) {
        $('.carousel-control').hide();
    }
}

function getPropertyTile(property) {
    var tmpl = document.getElementById('property-tile').cloneNode(true);
    tmpl.classList.remove('template');
    tmpl.querySelector('#heading').textContent = property['ListingTitle'];
    tmpl.querySelector('#description').textContent = property['Location'];
    tmpl.querySelector('#image').src = property['Photo1'];
    tmpl.querySelector('#area').textContent = property['SqrtFeet'] == "" ? "-" : Number(property['SqrtFeet']).toFixed(0) + " sqft";
    tmpl.querySelector('#tile-price').textContent = numberWithCommas(Number(property['Price']).toFixed(0)) + " AED";
    tmpl.querySelector('#photoCount').textContent = property['NoOfPhotos'];
    tmpl.querySelector('#propertyId').textContent = property['RecordId'];
    tmpl.querySelector('#leasing-type').textContent = property['ListingType'];

    return tmpl;
}

function onTileClick(tile) {
    var listingType = tile.querySelector('#leasing-type').textContent;
    var id = tile.querySelector('#propertyId').textContent;
    if(listingType == 'Sale') {
        window.open(getBaseUrl() + forSale + id + "?" + getParameterString() + "&id=" + id, "_self");
    } else {
        window.open(getBaseUrl() + forRent + id + "?" + getParameterString() + "&id=" + id, "_self");
    }    
    
}

function clonePagination() {
    $(".pagination-button-row").clone().appendTo(".search-result-container");
}

function searchResultsCount() {
    return resultCount;
}


function resizeCaptchaSearchResult() {
    var recaptcha = $("#recaptcha-noresult");
    var newScaleFactor = recaptcha.parent().innerWidth() / 340;
    if (newScaleFactor > 1 && newScaleFactor < 1.5) {
        var newScaleFactorY = newScaleFactor * 0.9;
        newScaleFactor = newScaleFactor * 1.01;
    } else if (newScaleFactor > 1.5)
        var newScaleFactorY = newScaleFactor * 0.4;
    else if (newScaleFactor < 0.7) {
        newScaleFactor = newScaleFactor * 0.96;
        var newScaleFactorY = newScaleFactor * 1.1;
    } else {
        var newScaleFactorY = newScaleFactor * 0.8;
        newScaleFactor = newScaleFactor * 0.99;
    }
    recaptcha.css('transform', 'scale(' + newScaleFactor + ',' + newScaleFactorY + ')');
    recaptcha.css('transform-origin', '0 0');

}

function registerNotification() {
    verify();
}

;var details;

function initPropertyDetail() {
    populatePropertyDetails();
    resizeCaptcha();
    enablePhoneButton();
    fillSearch();
    $(".carousel").swiperight(function () {
        $(this).carousel('prev');
    });
    $(".carousel").swipeleft(function () {
        $(this).carousel('next');
    });
    $(window).resize(function () {
        resizeCaptcha();
    });

    $('.fb-share-button').attr('data-href', window.location.href);
}

function sendReply() {
    verify(details[ContactEmail]);
}

function populatePropertyDetails() {
    details = propertyDetailJSONStr;
    
    //Set title and description
    document.getElementById("page-title").innerHTML = details[ListingTitle] + " | DIFC Property Finder | difc.ae";
    document.getElementById("page-description").setAttribute("content",details[ListingDescription]);

    document.getElementById("property-heading").innerHTML = details[ListingTitle];
    document.getElementById("property-description").innerHTML = details[Unit] + ', ' + details[Location];

    if (details[CompanyName] == null || details[CompanyName] == "") {
        document.getElementById("company-text").style.display = "none";
        document.getElementById("company-name").style.display = "none";
    } else
        document.getElementById("company-name").innerHTML = details[CompanyName];

    if (details[AgentDetails] == null || details[AgentDetails] == "") {
        document.getElementById("landlord-text").style.display = "none";
        document.getElementById("landlord-name").style.display = "none";
    } else
        document.getElementById("landlord-name").innerHTML = details[AgentDetails];

    document.getElementById("price").innerHTML = numberWithCommas(Number(details[Price]).toFixed(0)) + " AED";

    if (details[ListingType] == 'Sale' || details[CreatedbyDIFCBackend] == "true") {
        //TODO: ADD DIFC PHONE NUMBER
        document.getElementById("phone-num").innerHTML = "";
    } else {
        document.getElementById("phone-num").innerHTML = details[ContactPhone];
    }
    document.getElementById("time-zone").innerHTML = details[ContactTimezone];

    //Fill Facts table
    var sqft = details[SqrtFeet];
    var pricePerSqft = (sqft != "" && sqft != null) ? ((Number(details[Price]) / Number(sqft)).toFixed(2) + " AED") : '-';
    document.getElementById("listing-description").innerHTML = details[ListingDescription];
    document.getElementById("table-price").innerHTML = numberWithCommas(Number(details[Price]).toFixed(0)) + " AED";
    document.getElementById("table-sale-or-rent").innerHTML = details[ListingType];
    document.getElementById("table-area").innerHTML = sqft == "" ? "-" : (Number(sqft).toFixed(0) + " sqft");
    document.getElementById("table-price-per-sqft").innerHTML = numberWithCommas(pricePerSqft);
    document.getElementById("table-property-type").innerHTML = details[PropertyType];
    document.getElementById("table-unit").innerHTML = details[Unit];
    document.getElementById("table-building-name").innerHTML = details[BuildingName] == null ? "-" : details[BuildingName];
    if(details[UnitDeliveryCondition] == null || details[UnitDeliveryCondition] == "")
        document.getElementById("delivery-condition-col").style.display = "none";
    else
    document.getElementById("table-delivery-condition").innerHTML = details[UnitDeliveryCondition];

    var optionalName;
    var optionalValue;
    if (details[PropertyType] == RESIDENTIAL) {
        optionalName = "Bedrooms";
        optionalValue = details[NoOfBedRooms];
    } else if (details[PropertyType] == BUSINESS_CENTRE) {
        optionalName = "PAX";
        optionalValue = details[NoOfPAX];
    } else if (details[PropertyType] == RETAIL) {
        optionalName = "F&B";
        optionalValue = details[FnB] == "" ? "-" : details[FnB];
    } else if (details[PropertyType] == DATA_CENTRE) {
        optionalName = "Cage No.";
        optionalValue = details[CageNo] == "" ? "-" : details[CageNo];
    } else {
        optionalName = "";
        optionalValue = "";
    }

    if (optionalName != "") {
        document.getElementById('table-optional-section').style.display = "initial";
    }
    document.getElementById("table-optional-name").innerHTML = optionalName;
    document.getElementById("table-optional-value").innerHTML = optionalValue;

    //Amenities
    if (details[Amenities] == null || details[Amenities] == "") {

    } else {
        document.getElementById("amenities-section").style.display = "initial";
        var amenitySection = document.getElementById("amenities-points");
        var res = details[Amenities].split(",");
        for (var i = 0; i < res.length; i++) {
            var icon = document.createElement('span');
            icon.className += 'icon-font';
            icon.innerHTML = "g";

            var amenity = document.createElement('p');
            amenity.style.display = "inline";
            amenity.innerHTML = res[i].trim();

            amenitySection.appendChild(icon);
            amenitySection.appendChild(amenity);

            var br = document.createElement('br');
            amenitySection.appendChild(br);
        }
    }


    //Fill company profile

    var companyProfileSection = document.getElementById('company-profile-section');
    if (details[CompanyProfile] != null && details[CompanyProfile] != "") {
        document.getElementById("company-profile").innerHTML = details[CompanyProfile];
        companyProfileSection.style.display = "initial";
    }

    //Owner Details
    if ((details[FirstName] != null || details[StreetAddress] != null) && details[ListingType] != 'Sale' && details[CreatedbyDIFCBackend] != "true") {
        var ownerDetails = document.getElementById('owner-detail-section');
        ownerDetails.style.display = "initial";

        var firstName = details[FirstName];
        var lastName = details[LastName];
        var address = details[StreetAddress];
        document.getElementById('owner-detail-name').innerHTML = (firstName != null ? firstName : "") + " " + (lastName != null ? lastName : "");
        document.getElementById('owner-detail-address').innerHTML = address != null ? address : "-";

    }

    //Company Details

    if (details[CreatedbyDIFCBackend] == "true") { //TODO: DIFC Details, not owners
        document.getElementById('company-details-section').style.display = "initial";
        document.getElementById("company-address").innerHTML = "Address: " + "DIFC, Dubai";
        
        if(details[PropertyType] == OFFICE || details[PropertyType] == RESIDENTIAL)
            document.getElementById("company-email").innerHTML = "Email: " + "officelisting@difc.ae";
        if(details[PropertyType] == RETAIL || details[PropertyType] == STORAGE)
            document.getElementById("company-email").innerHTML = "Email: " + "retaillisting@difc.ae";
        if(details[PropertyType] == BUSINESS_CENTRE)
            document.getElementById("company-email").innerHTML = "Email: " + "businesscentrelisting@difc.ae";
        if(details[PropertyType] == DATA_CENTRE)
            document.getElementById("company-email").innerHTML = "Email: " + "datacentrelisting@difc.ae";
        
        document.getElementById("company-website").innerHTML = "Website: " + "www.difc.ae";
        document.getElementById("company-phone").innerHTML = "Mobile No.: " + "-";
        document.getElementById("company-name-detail").innerHTML = "Company Name: " + "Dubai International Financial Centre";

    } else if(details[ListingType] == 'Sale') {
        document.getElementById('company-details-section').style.display = "initial";
        document.getElementById("company-address").innerHTML = "Address: " + "DIFC, Dubai";
        document.getElementById("company-email").innerHTML = "Email: " + "officelisting@difc.ae";
        document.getElementById("company-website").innerHTML = "Website: " + "www.difc.ae";
        document.getElementById("company-phone").innerHTML = "Mobile No.: " + "-";
        document.getElementById("company-name-detail").innerHTML = "Company Name: " + "Dubai International Financial Centre";
    } else {
        if (details[AddressDetails] != null || details[CompanyName] != null || details[WorkPhone] != null || details[WebSite] != null) {
            document.getElementById('company-details-section').style.display = "initial";
            document.getElementById("company-address").innerHTML = "Address: " + (details[AddressDetails] == null ? "-" : details[AddressDetails]);
            document.getElementById("company-website").innerHTML = "Website: " + (details[WebSite] == null ? "-" : details[WebSite]);
            document.getElementById("company-phone").innerHTML = "Mobile No.: " + (details[WorkPhone] == null ? "-" : details[WorkPhone]);
            document.getElementById("company-name-detail").innerHTML = "Company Name: " + (details[CompanyName] == null ? "-" : details[CompanyName]);
        }
    }


    // Download documents

    var downloadDoc = document.getElementById("download-doc-section");

    if (details[ClientOffer] != null) {
        downloadDoc.style.display = "block";
        var clientOffer = document.getElementById("client-offer");
        clientOffer.setAttribute("href", details[ClientOffer]);
        clientOffer.style.display = "initial";
    }
    if (details[FloorPlan] != null) {
        downloadDoc.style.display = "block";
        var floorPlan = document.getElementById("floor-plan");
        floorPlan.setAttribute("href", details[FloorPlan]);
        floorPlan.style.display = "initial";
    }
    if (details[TenantManual] != null) {
        downloadDoc.style.display = "block";
        var TenantManual = document.getElementById("tenant-manual");
        TenantManual.setAttribute("href", details[TenantManual]);
        TenantManual.style.display = "initial";
    }
    if (details[FitoutManual] != null) {
        downloadDoc.style.display = "block";
        var FitoutManual = document.getElementById("fitout-manual");
        FitoutManual.setAttribute("href", details[FitoutManual]);
        FitoutManual.style.display = "initial";
    }

    initMap();
    initializePropertyGallery();
}

function initializePropertyGallery() {
    var innerCarousel = $(".carousel-inner");
    var carouselIndicators = $(".carousel-indicators");

    var photoCount = 0;
    for (var i = 0; i <= 9; i++) {
        var photoSrc = details['Photo' + (i + 1)];
        if (photoSrc == null)
            continue;
        var item = document.createElement('div');
        item.className = "item";
        if (i == 0)
            item.className += " active";
        var image = document.createElement('img');
        image.setAttribute("src", photoSrc);
        image.setAttribute("alt", "");
        item.appendChild(image);
        innerCarousel.append(item);

        //Indicators
        var li = document.createElement('li');
        if (i == 0)
            li.className += " active";
        li.setAttribute("data-target", "#carousel-custom");
        li.setAttribute("data-slide-to", photoCount);
        var indicatorImage = image.cloneNode(true);;
        li.appendChild(indicatorImage);
        carouselIndicators.append(li);
        photoCount++;
    }
}

/*function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}*/


function resizeCaptcha() {
    var recaptcha = $(".g-recaptcha");
    var newScaleFactor = recaptcha.parent().innerWidth() / 304;
    if (newScaleFactor > 1 && newScaleFactor < 1.5)
        var newScaleFactorY = newScaleFactor * 0.8;
    else if (newScaleFactor > 1.5)
        var newScaleFactorY = newScaleFactor * 0.4;
    else
        var newScaleFactorY = newScaleFactor * 1.2;
    recaptcha.css('transform', 'scale(' + newScaleFactor + ',' + newScaleFactorY + ')');
    recaptcha.css('transform-origin', '0 0');

}

function initMap() {
    var latitude = (details[Latitude] == null) ? 25.214434 : details[Latitude]; //Gate Building Coordinates
    var longitude = (details[Longitude] == null) ? 55.282187 : details[Longitude];
    var myLatLng = {
        lat: latitude,
        lng: longitude
    };
    var map = new google.maps.Map(document.getElementById('map'), {
        center: myLatLng,
        zoom: 14
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: details[BuildingName]
    });

    marker.addListener('click', function () {
        window.open("https://www.google.com/maps/place/" +latitude + "," + longitude + "/@" + latitude + "," + longitude + "," + "14z","_blank");
    });
}

function enablePhoneButton() {
    var phoneNum = document.getElementById('phone-num').innerHTML;
    var phoneButton = document.getElementById('show-phone');
    if (phoneNum.length != 0) {
        phoneButton.disabled = false;
    } else {
        phoneButton.disabled = true;
    }
}

function showPhoneNumber() {
    dataLayer.push({
        'event': 'show-phone-click',
        'show-phone-for-building': document.getElementById('property-heading').innerHTML
    });
    var phoneNum = document.getElementById('phone-num').innerHTML;
    if (phoneNum != undefined && phoneNum != '')
        document.getElementById('show-phone').innerHTML = phoneNum;
    var timezone = document.getElementById("time-zone").innerHTML;

    //Considering format: (GMT+09:00) Seoul
    if (timezone == null || timezone == "")
        return;
    if (timezone.substring(8, 10) == "30")
        var dec = "5";
    else if(timezone.substring(8, 10) == "45")
        var dec = "75";
    else
        var dec = "0";
    var offset = timezone.substring(4, 7) + '.' + dec;
    document.getElementById('agent-time').innerHTML = "Owner's Current Time: " + calcTime(offset);
}

function calcTime(offset) {
    // create Date object for current location
    var d = new Date();
    var utc = d.getTime() + (d.getTimezoneOffset() * 60000);

    // create new Date object for different city
    // using supplied offset
    var nd = new Date(utc + (3600000 * offset));

    // return time as a string
    var localeTime = nd.toLocaleTimeString();
    return localeTime.replace(/:\d+ /, ' ');
}

function shareFB() {
    var url = window.location.href.split('?')[0];
    window.open("https://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(url), '_blank');
}

function shareTwitter() {
    var url = window.location.href.split('?')[0];
    window.open("http://twitter.com/share?text=" + "Have a look at this property in the DIFC "+"&hashtags=DIFC&url="+ encodeURIComponent(url), "_blank");
}

function shareLinkedIn() {
    var url = window.location.href.split('?')[0];
    window.open("https://www.linkedin.com/cws/share?url=" + encodeURIComponent(url), "_blank");
}

function shareEmail() {
    /*$('#email-friend-container').show();
    $('#email-friend-container').scrollView();*/
    var url = window.location.href.split('?')[0];
    var propertyName = details[ListingTitle];
    var subject = "You may be interested in this DIFC property";
    var bodyText = "Have a look at this property in the Dubai International Financial Centre (DIFC), I thought you may be interested in it. \n"
    window.open("mailto:?subject=" + subject + "&body=" + bodyText + encodeURIComponent(url), "_blank");
}

$.fn.scrollView = function () {
    return this.each(function () {
        $('html, body').animate({
            scrollTop: $(this).offset().top
        }, 1000);
    });
}

//Area converter


function checnum(as) {
    var dd = as.value;
    if (isNaN(dd)) {
        dd = dd.substring(0, (dd.length - 1));
        as.value = dd;
    }
}

function inchconv(val) {
    var m = document.getElementById("m");
    var feet = document.getElementById("feet");
    var inch = document.getElementById("inch");
    var miles = document.getElementById("miles");
    m.value = (Math.round(inch.value * 6.4516129 * 0.0001)).toFixed(2);
    feet.value = (Math.round(inch.value * 0.0069444)).toFixed(2);
    miles.value = ((inch.value * 2.49097669546596) * Math.pow(10, -10)).toFixed(2);
}


function feetconv(val) {
    var m = document.getElementById("m");
    var feet = document.getElementById("feet");
    var inch = document.getElementById("inch");
    var miles = document.getElementById("miles");

    m.value = (Math.round(feet.value * 929.022668 * 0.0001)).toFixed(2);
    inch.value = (Math.round(feet.value * 144.00)).toFixed(2);
    miles.value = ((feet.value / 27.878400) * Math.pow(10, -6)).toFixed(2);
}

function mconv(val) {
    var m = document.getElementById("m");
    var feet = document.getElementById("feet");
    var inch = document.getElementById("inch");
    var miles = document.getElementById("miles");

    feet.value = (Math.round(m.value * 10.764)).toFixed(2);
    inch.value = (Math.round(m.value * 1550.0)).toFixed(2);
    miles.value = ((m.value * 3.8610216) * Math.pow(10, -7)).toFixed(2);
}

function milesconv(val) {
    var m = document.getElementById("m");
    var feet = document.getElementById("feet");
    var inch = document.getElementById("inch");
    var miles = document.getElementById("miles");

    feet.value = ((miles.value * 27.878400) * Math.pow(10, 6)).toFixed(2);
    inch.value = ((miles.value * 40.1450) * Math.pow(10, 8)).toFixed(2);
    m.value = ((miles.value / 3.8610216) * 0.0001 / Math.pow(10, -11)).toFixed(2);
}

function openCurrencyConverter() {
    window.open(getBaseUrl() + '/' + 'CurrencyConverter','1451280461275','width=280,height=270,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');
}
function openAreaConverter() {
    window.open(getBaseUrl() + '/' + 'AreaConverter','1451280461275','width=350,height=320,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');
}
;//@prepros-prepend constants.js
//@prepros-prepend mapHighlight.js
//@prepros-prepend advancedSearch.js
//@prepros-prepend imageMapResizer.js
//@prepros-prepend searchResults.js
//@prepros-prepend propertyDetail.js
