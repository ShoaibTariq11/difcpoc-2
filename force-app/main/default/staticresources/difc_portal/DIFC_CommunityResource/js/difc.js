
//var jQuery = $.noConflict(true);

window.DIFC = {};

DIFC = (function () {
    var portalSettings = {
        PortalUrlPrefix: UserContext.siteUrlPrefix,
        PortalName: UserContext.siteUrlPrefix.split("/")[1]
        // SRIdPrefix: "a00",
        // lookupInput_id: "CF00N11000000qRuU",
        // account_lkid  : "CF00N10000004HiGy",        
        // SRStepsElementId: "00N10000004HiVD_body"
    };

    function loadSettings (){

            DIFC.Util.load_settings_IFrame_async(PortalUrlPrefix + "/apex/DIFC_ConnectionMgr",'sidebar_frame', settings_loaded);
    }


    function loadGSSettings() {

        var strURL = jQuery('#save_new_url').val() || window.location.href;
        if(strURL.indexOf('retURL')!=-1 && strURL.indexOf('save_new_url')!=-1){
            var temp = DIFC.Util.getURLParameterByName('save_new_url', strURL);
            retainURL = DIFC.Util.getURLParameterByName('retURL', temp);
        }else{
            retainURL   =  jQuery('#save_new_url').val() || window.location.href;
        }
 
        var contid      =  DIFC.Util.getURLParameterByName('ContactId',retainURL),
            sponsorid   =  DIFC.Util.getURLParameterByName('SponsorId',retainURL),
            recType     =  DIFC.Util.getURLParameterByName('RecordType',retainURL),
            autofill    =  DIFC.Util.getURLParameterByName('autofill',retainURL),
            isSR        =  DIFC.Util.getURLParameterByName('isSR',retainURL),
            upgrade     =  DIFC.Util.getURLParameterByName('upgrade',retainURL),
            UnitId      =  '',
            UnitName    =  '',
            urlParam    = "?ContactId="+contid+"&SponsorId="+sponsorid+"&autofill="+autofill+"&isSR="+isSR+"&upgrade="+upgrade;

            if(DIFC.Util.getURLParameterByName('UnitId',retainURL)!='' && DIFC.Util.getURLParameterByName('UnitName',retainURL)!=''){
                UnitId      = DIFC.Util.getURLParameterByName('UnitId',retainURL); //changes for Property services
                UnitName    = DIFC.Util.getURLParameterByName('UnitName',retainURL);//changes for Property services
            }else if(DIFC.Util.getURLParameterByName('UnitId',window.location.href)!='' && DIFC.Util.getURLParameterByName('UnitName',window.location.href)!=''){
                UnitId      = DIFC.Util.getURLParameterByName('UnitId',window.location.href); //changes for Property services
                UnitName    = DIFC.Util.getURLParameterByName('UnitName',window.location.href);//changes for Property services
            }

            if(DIFC.Util.getURLParameterByName('RecordType',retainURL)!=''){
                recType      = DIFC.Util.getURLParameterByName('RecordType',retainURL); //changes for Property services
            }else if(DIFC.Util.getURLParameterByName('RecordType',window.location.href)!=''){
                recType      = DIFC.Util.getURLParameterByName('RecordType',window.location.href); //changes for Property services
            }

            urlParam = urlParam+'&UnitId='+UnitId+'&UnitName='+UnitName;

        console.log(retainURL);
        console.log("contid    "+ contid);
        console.log("sponsorid    "+ sponsorid);   //changes for GS
        console.log("urlParam    " + urlParam);
        
        //changes for GS        
        DIFC.Util.load_settings_IFrame_async(PortalUrlPrefix + "/apex/DIFC_ConnectionMgr"+urlParam+"&RecordType="+recType,'gs_settings', settings_loaded); 
    }    

    function setup_topbar (){

        // initialize user profile menu
        jQuery(".globalHeaderNameMink .name").text(jQuery("#globalHeaderNameMink").text());
        jQuery("nav.top-bar section.top-bar-section ul.settings ul.dropdown").append(jQuery(".globalHeaderBar .zen-options li"));

        // Apply Firefox dropdown hover fix
        if(isFirefox) {
            var user_settings_elem = jQuery('nav.top-bar section ul.settings > li.has-dropdown');
            if(!user_settings_elem.hasClass('.not-click')) {
                jQuery(user_settings_elem).on('mouseenter', function(){
                    jQuery(this).children('ul.dropdown').css('clip', 'auto');
                }).on('mouseleave', function(){
                    jQuery(this).children('ul.dropdown').css('clip','rect(1px, 1px, 1px, 1px)');
                });
            }           
        }
        jQuery("form#searchForm").attr('action', PortalUrlPrefix + '/_ui/search/ui/UnifiedSearchResults')
    }


    function autopopulate_new_SRFormFields () {
        //Update SR Title header
        console.log("Auto Populating fields... ");            

        if(jQuery("body.editPage").length > 0 && 
            typeof(DIFC.settings.auto_populate_fields) != 'undefined') {

            //console.log("Auto Populating fields... " +  typeof(DIFC.settings.auto_populate_fields) != 'undefined');            

            // if(typeof(DIFC.settings.auto_populate_fields) != 'undefined') {
                var css_text = '';

                console.log("Auto Populating fields: applying settings...");                

                var autopop_fields = DIFC.settings.auto_populate_fields;
                for(var fieldNum in autopop_fields.fields){

                    var fieldInfo = autopop_fields.fields[fieldNum];

                    if(fieldInfo.isLookup && fieldInfo.target_viewElement != null && fieldInfo.target_viewElement != ''){
                         
                        var customerlk_elem = jQuery(':input[id$="'+ fieldInfo.target_viewElement +'_lkid"]'),
                            customerlold_elem = jQuery(':input[id$="'+ fieldInfo.target_viewElement +'_lkold"]'),
                            customermod_elem = jQuery(':input[id$="'+ fieldInfo.target_viewElement +'_mod"]'),
                            lookup_Val        = jQuery(customerlk_elem).val();

                        // console.log("Target Element" + customerlk_elem);
                        
                        var tmp_elem =  jQuery(".lookupInput input#"+ fieldInfo.target_viewElement),
                            tmp_val  =  tmp_elem && tmp_elem.val() || '';

                        if(!lookup_Val || lookup_Val=='000000000000000'|| lookup_Val==undefined || lookup_Val.trim().length < 0){

                            customerlk_elem.attr("value", fieldInfo.value);
                            customerlold_elem.attr("value", fieldInfo.field_value);
                            customermod_elem.attr("value",0);

                        }

                        if(tmp_elem && tmp_val.length == 0 ){
                            jQuery(tmp_elem).val(fieldInfo.field_value).css("display", "inline");                        
                        }
                        jQuery(tmp_elem).parent().addClass("hidden");

                        if(fieldInfo.disable == "disabled"){
                            jQuery(tmp_elem).attr("disabled", "");
                            // jQuery('.lookupInput > a').remove();

                            //.lookupInput.customer > input#CF00N11000000qRuU ~ *                            
                            css_text +='.lookupInput.hidden > input#' + fieldInfo.target_viewElement + ' ~  * ,';
                        }
                    }else if(fieldInfo.target_viewElement != null || fieldInfo.target_viewElement != ""){
                        //if(window.location.pathname == portalSettings.PortalUrlPrefix + fieldInfo.URLPrefix_pattern) 
                        {  
                            var receipt_amt_id = DIFC.Util.getURLParameterByName('receipt_id');
                            var targetElement = jQuery(':input[id$="'+ fieldInfo.target_viewElement +'"]');
                            var targetElement_val = jQuery(targetElement).val();

                            if(receipt_amt_id === fieldInfo.target_viewElement ) {
                                fieldInfo.value = DIFC.Util.getURLParameterByName('amount');
                                targetElement.attr('value', fieldInfo.value);
                            }

                            if(!targetElement_val || targetElement_val.trim().length < 0 || targetElement_val=='000000000000000'|| targetElement_val==undefined)
                                targetElement.val(fieldInfo.value);

                            if(fieldInfo.disable == "disabled" && targetElement.val()!='' && targetElement.val()!=undefined)  //changes for disable or enable                            
                                targetElement.attr('disabled', "disabled");
                            targetElement.addClass('autopoulated');                            
                        }
                    }
                }

                if(css_text.length > 0){
                    css_text = css_text.replace(/(?:,|\s+)+$/g, '');
                    css_text +='{display: none !important;}';

                    DIFC.Util.embedCSSStyleTag(css_text);
                }


                //if(disabledOptions.length > 0) {
                    var elem = document.getElementById("editPage");

                    if(elem) {
                        var sforceSubmit = elem.onsubmit;
                        var difcSubmit = function() {

                            var disabledOptions = jQuery("#editPage select.autopoulated, #editPage input.autopoulated"),
                                receipt_amt_id = DIFC.Util.getURLParameterByName('receipt_id');

                            disabledOptions.removeAttr('disabled');
                            if(receipt_amt_id.length > 0) {
                                jQuery('#' + receipt_amt_id ).removeAttr('disabled');
                            } 

                            sforceSubmit();
                        }
                        elem.onsubmit = difcSubmit;                        
                    }
                                                    
                //}
        }
    }

    function initEvents() {

        // Sidebar toggled
        jQuery("#handlebarContainer").on("click", function (e) {            
            // $(this).parent().toggleClass("sidebar");
            // $("#sidebarCell").toggleClass("main-row");
            
            if (jQuery("body").hasClass("listPage")) {                
                jQuery(".refreshListButton").click();
            }
            jQuery.publish("sidebarToggled", null)
        })

        jQuery.subscribe("sidebarToggled", refreshMainPageLayout);
    }

    /*
    *   Portal DOM methods.
    *   
    */
    /*******************************************************************************/
    
    function Change_SR_Page() {
        if(jQuery("body.editPage").length > 0) {
            var sr_type = DIFC.Util.getURLParameterByName('type');
            if(sr_type == 'License_Renewal') {
                jQuery('#head_2_ep').next().append(
                    jQuery('<div><strong>Note* :</strong> '+
                        '<p style="font-style: italic; display:inline;">&nbsp;If you are registered as education provider i.e University, please make sure to apply for approval from KHDA prior to renewing the license. The said NOC will be sent to Registrar of Company once ready.</p> ' +
                        '</div>')
                );
            }
            else if(sr_type=='Replacement_of_Commercial_License'){
                jQuery('.bPageTitle.form-header').append('<p>This is to request the replacement of the commercial license. Please click on save and submit.</p>');
            }

            var person_details_section = jQuery('div#head_3_ep h3:contains("Details of the Person Signing the Request")');
            if(person_details_section.length > 0){
               var label_elem = jQuery(person_details_section).parents('div#head_3_ep').next().find('table.detailList tr:first-child td.labelCol.empty');
                jQuery(label_elem).attr('style', 'width:100%; padding: 0; font-weight: 300;');
                jQuery(label_elem).html('Example: Director / Company Secretary / Member / General Partner / Founding Member / Secretary where applicable.');
            }
        }
    }

    function updateSFDCCalendar(){
        var select = document.getElementById('calYearPicker');
        if (!select) return;
        
          select.innerHTML = '';
          var startYear = new Date().getFullYear() - 70;
          var endYear   = new Date().getFullYear();
          for (var year = startYear; year <= endYear; year++) {
            select.options[select.options.length] = new Option(year, year);
          }          
    }

    function refreshMainPageLayout() {
        var sidebarToggleTimer = setTimeout(refreshMasonry, 300);

        function refreshMasonry(){
            jQuery(".masonry").masonry();

            clearTimeout(sidebarToggleTimer);
        }
    }

    function override_hoverHighlight (){
        setFocusOnLoad = function(){};

        window.hiOn = function(a) {
            null !== a && jQuery(a).addClass("highlight-hover");
        };

        window.hiOff = function(a) {
            null !== a && jQuery(a).removeClass("highlight-hover");
        };
    }

    var disableUserAccountLinks = function () {
        //disable user profile links
        jQuery("#bodyCell a").each(function() {
            var reg = new RegExp('^\\'+ DIFC.settings.PortalUrlPrefix +'\\/(005|001)\\w*\\/*', 'g');
            var userProfiles = reg.exec(jQuery(this).attr('href'));
            if(null != userProfiles && userProfiles.length > 0) {
                jQuery(this).contents().unwrap();
            }
        })
    }

    function resizeIframes(){

        var iframes = jQuery("#ep .pbSubsection iframe");

        for(var iframeIndex = 0; iframeIndex < iframes.size(); iframeIndex++ ){
            var iframeElem = iframes[iframeIndex];

            jQuery(iframeElem).load(function(){
                var iframeElem = this;
                var newheight=iframeElem.contentWindow.document.body.offsetHeight;
                var newwidth=iframeElem.contentWindow.document.body.scrollWidth;

                iframeElem.height = (newheight) + "px";
                // iframeElem.width= (newwidth) + "px";

            });

        }
    }

    function updateSRRelatedLists(relatedList) {
        for(var list_num in relatedList){
            var list_name = relatedList[list_num];

            if(list_name && list_name.sr_docs){
                if(list_name.sr_docs.length > 0 && jQuery('#docs-download').length == 0){
                    var elem_id = list_name.sr_docs.substr(2);
                    
                    DIFC.Util.embedCSSStyleTag('div[id$="'+ elem_id +'_body"] table tr th:nth-child(2),div[id$="'+ elem_id +'_body"] table tr td:nth-child(2){display: none;}');
                    var sr_docSection = jQuery('div[id$='+ elem_id +'_body]');
                    //sr_docSection.find('table tr th:nth-child(2), table tr td:nth-child(2)').attr('style', 'display: none;');

                    jQuery(sr_docSection).parent('.bPageBlock').find('.pbHeader').append(

                        jQuery('<div id="docs-download" class="sub_header" style="padding: 0px 5px;">' +
                            '<p style="font-weight: bold;">Please click on Download/Upload Doc to upload the required documents.</p>' +
                        '</div>')
                    );
                }
            }
        }
    }    

    /****************************************************************************************/
    function stop_animation(){
        jQuery(".spin-container").remove(); 
        jQuery("body").removeClass('no-scroll'); 
        console.log('stopping Animation ===> ' + Date.now());              
    }

    function settings_loaded(id){
        var settings = document.getElementById(id).contentWindow.get_settings();
        console.log("Portal settings loaded: " + settings);
        //$.jStorage.set("portalSettings", settings)
        jQuery.extend(portalSettings, settings);
        
        sforce.connection.sessionId = DIFC.settings.ApiSessionId;
        DIFC.PageHeading.setPageHeading();
        autopopulate_new_SRFormFields();
        
        stop_animation();

        Change_SR_Page();
        updateSRRelatedLists(DIFC.settings.relatedLists);

        //Apply page level rules
        // if(Page.currentPage == Page.Types.SRView) {
        //     if(Page.SRPageInfo != null && Page.SRPageInfo.SRId != null)
        //         loadUserSteps();            
        // }   

        /**
        * Added by: Claude Manahan. NSI DMCC. 14-04-2016
        * Additional query for getting user's contact role  https://cs17.salesforce.com/resource/1460554476000/difc_portal
        */
        // Claude - Start of Revisions

        var currentUser = sforce.connection.getUserInfo();                                                               // get current user

        var currentUserResult = sforce.connection.query("SELECT ContactId from user WHERE Id='"+currentUser.userId+"'"); // query the user record

        var currentUserContactId = currentUserResult.getArray("records")[0].ContactId;                                   // get the contact id                                // get the contact ID, if any

        if(!!currentUserContactId){ // prevents undefined variable exception
            var currentUserContactRoleResult = sforce.connection.query("SELECT Role__c from Contact WHERE Id='"+currentUserContactId+"'");      // query the contact record

            var currentUserContactRole = currentUserContactRoleResult.getArray("records")[0].Role__c;

            if(!!currentUserContactRole){// prevents undefined variable exception

                jQuery('#contentWrapper').addClass(currentUserContactRole == 'Fit-Out Services' || currentUserContactRole == 'Event Services' ? 'contentWrapper--contractor' : '' );

            }

        } 
        // Claude - End of Revisions      
    }

    function setup_page() {
        autopopulate_new_SRFormFields();
        initEvents();
    }

    function onReady(){
        DIFC.Page.init();
        setup_page(); 
        disableUserAccountLinks();
        DIFC.SercurityManager.apply_security();
        resizeIframes();
        updateSFDCCalendar();
    
        var strURL = jQuery('#save_new_url').val() || window.location.href;
        var temp = "";
        if(strURL.indexOf('save_new_url')!=-1){
            temp = DIFC.Util.getURLParameterByName('save_new_url', strURL);
            temp = DIFC.Util.getURLParameterByName('retURL', temp);
        }else{
            temp   =  jQuery('#save_new_url').val() || window.location.href;
        }
        var shouldLoadGSSettings = DIFC.Util.getURLParameterByName('autofill', temp) || 
                                   DIFC.Util.getURLParameterByName('autofill', temp);
    
        if(shouldLoadGSSettings)
            loadGSSettings();


        jQuery('.pbBody').find('.detailList tr').find('td:contains("ApplicantId_hidden")').addClass('hidden');
        jQuery('.pbBody').find('.detailList tr').find('td:contains("ApplicantId_hidden")').next().addClass('hidden');
        jQuery('.pbBody').find('.detailList tr').find('td:contains("SponsorId_hidden")').addClass('hidden');
        jQuery('.pbBody').find('.detailList tr').find('td:contains("SponsorId_hidden")').next().addClass('hidden');
        jQuery('.pbBody').find('.detailList tr').find('td:contains("Docdetail_hidden")').addClass('hidden');
        jQuery('.pbBody').find('.detailList tr').find('td:contains("Docdetail_hidden")').next().addClass('hidden');
        jQuery('.pbBody').find('.detailList tr').find('td:contains("Req No")').addClass('hidden');
        jQuery('.pbBody').find('.detailList tr').find('td:contains("Req No")').next().addClass('hidden');
        
        if(jQuery("body.editPage").length > 0){
            jQuery('#ep').find('td > label[for=CF00N200000067iNW]').hide();//SR no. field
            jQuery('#ep').find('td span.lookupInput Input#CF00N200000067iNW').hide();
            jQuery('#ep').find('td span.lookupInput Input#CF00N200000067iNW').next().hide();
            //Property & Freehold Changes
            jQuery('#ep').find('td > label[for=CF00Ng0000001aOvv]').hide(); //Request no. field
            jQuery('#ep').find('td span.lookupInput Input#CF00Ng0000001aOvv').hide();
            jQuery('#ep').find('td span.lookupInput Input#CF00Ng0000001aOvv').next().hide();
        }
        // SearchClickLoggingUtil.setClickLoggingServletPath("/customers/_ui/search/logging/SearchClickLoggingServlet");
        // new UnifiedSearchAutoCompleteElement("phSearchInput", "/customers/_ui/common/search/client/ui/UnifiedSearchAutoCompleteServlet", 1, {}, true, null, "phSearchForm", ["div", "searchOwner", "asPhrase", "sen"], {}, true, 3, 100);
        // new UnifiedSearchButton("searchButtonContainer", "phSearchButton", "headerSearchRightRoundedCornerMouseOver", "phSearchForm");
    }

    return {

        init        : onReady,
        setup       : setup_page,        
        load_settings : loadSettings,
        settings    : portalSettings,
        disableLinks : disableUserAccountLinks
    };   

}());



/*******************************************************************************************/

DIFC.PageHeading = (function(){

    function update_existing_SRPageTitle () {
        var srNumParserRegExp = '';
        
        //var SRPage_URLComponents = /^\/customers\/(a0O\w*)\/*/g.exec(decodeURIComponent(sfdcPage.getHrefAsRetURL()));
        if(DIFC.settings.PortalUrlPrefix.length > 0)
            srNumParserRegExp = new RegExp('^\\'+ DIFC.settings.PortalUrlPrefix +'\\/('+ DIFC.settings.ServiceReq_prefix +'\\w*)\\/*', 'g');        
        else
            srNumParserRegExp = new RegExp('^\\/('+ DIFC.settings.ServiceReq_prefix +'\\w*)\\/*', 'g');

        var SRPage_URLComponents = srNumParserRegExp.exec(decodeURIComponent(sfdcPage.getHrefAsRetURL()));

        var serviceRequestId = '';

        if(SRPage_URLComponents !== null && SRPage_URLComponents.length >= 2) {
            serviceRequestId = SRPage_URLComponents[1];

            var serviceRequestTitle = '', 
                instruction_url     = '';

            try {
                if(serviceRequestId.length >= 15) {
                    var pageTitleResult = sforce.connection.query("SELECT Portal_Service_Request_Name__c, External_Status_Name__c from Service_Request__c WHERE id='"+serviceRequestId+"'");

                    var pageTitleRecords = pageTitleResult.getArray("records");

                     if(pageTitleRecords.length > 0) {
                        serviceRequestTitle = pageTitleRecords[0].Portal_Service_Request_Name__c;

                        //(id, title, _status, _serviceType)
                        DIFC.Page.SRPageInfo = new DIFC.SRPage();
                        DIFC.Page.SRPageInfo.init(serviceRequestId, serviceRequestTitle, 
                                    pageTitleRecords[0].External_Status_Name__c, "");
                        DIFC.SercurityManager.apply_security();
                    }
                }else {

                    var saveNewUrl = jQuery("#save_new_url").attr("value");
                    serviceRequestTitle = DIFC.Util.getURLParameterByName("service_request_title", saveNewUrl );

                    // instruction_url = decodeURIComponent(DIFC.Util.getURLParameterByName ("instruction_url"
                    //     , saveNewUrl));
                }

                if(serviceRequestTitle && typeof(serviceRequestTitle) != "undefined") {
                    jQuery(".bPageTitle").addClass("form-header");
                    jQuery(".bPageTitle .ptBody").addClass("row");

                    if(serviceRequestTitle.length > 0)
                        jQuery(".bPageTitle .ptBody .content").html('<h3 style="font-size:18px; display: block; margin-bottom: 15px;">' + serviceRequestTitle  +'</h3>');

                    // if(instruction_url.length > 0)
                    //     $(".bPageTitle .ptBody .content").append($('<h3 style="display: block; font-size: 18px;"><a target="_blank" href="'+ instruction_url +'" style="font-size: 14px;text-decoration: underline;">Click Here to View Application Guidelines</a></h3>'));

                    jQuery(".bPageTitle .ptBody .content").addClass("medium-12 columns title").removeClass("content");
                    // $(".bPageTitle .ptBody .links").addClass("medium-3 columns");
                }

            }catch(e) {
                console.log("SR Page title error. Detail:" + e);
            }
        }
    }

    function load_SRDescription(record_type) {
        try {
            var description_result = sforce.connection.query("SELECT Name, SR_Description__c FROM SR_Template__c WHERE SR_RecordType_API_Name__c='"+record_type+"'");
            var descriptionRecords = description_result.getArray("records");
            if(descriptionRecords!=null && descriptionRecords.length > 0){
                var description_text = descriptionRecords[0].SR_Description__c;
                if(description_text != null && description_text.length > 0 && jQuery(".bPageTitle .ptBody .description").length == 0)
                    jQuery(".bPageTitle .ptBody").append(jQuery('<div class="description" style="margin-bottom: 15px;">' + description_text +'</div>'));
            }
        }
        catch(e){
            console.log("SR Page description error. Detail:" + e);
        }
    }

    function intialize_page (pageTitle) {
        if(pageTitle != null && pageTitle.length > 0) {
            jQuery(".bPageTitle").addClass("form-header");

            var pageTitleBody = jQuery(".bPageTitle .ptBody");
            jQuery(pageTitleBody).addClass("row");

            var pageContent = jQuery(pageTitleBody).children(".content");

            jQuery(pageContent).html('<h3 style="display: block; font-size: 18px; margin-bottom: 15px;">' + pageTitle  +'</h3>');

            jQuery(pageContent).addClass("medium-12 columns title").removeClass("content");
            //$(pageContent).siblings(".links").addClass("medium-3 columns");
        }else {

            //$(".bPageTitle").addClass("form-header");

            var pageTitleBody = jQuery(".bPageTitle .ptBody");
            jQuery(pageTitleBody).addClass("row");

            var pageTitleContent = jQuery(pageTitleBody).children(".content");
            jQuery(pageTitleContent).children(".pageTitleIcon").remove();
            var pageTitle = jQuery(pageTitleContent).children(".pageType").text().split(":")[0];

            var pageTiteDescription = jQuery(pageTitleContent).children(".pageDescription").text();

            jQuery(pageTitleContent).html('<h3 style="display: block; font-size: 18px; margin-bottom: 15px;">' + pageTitle  +'</h3><h4 style="display: block; font-size: 16px;">'+ pageTiteDescription +'</h4>');

            jQuery(pageTitleContent).addClass("medium-9 columns title").removeClass("content");
            jQuery(pageTitleContent).siblings(".links").addClass("medium-3 columns");
        }

    }

    //Initialize Service Request Title
    var initialize_pageHeader = function (heading_update_callback) {

        if(jQuery("body.editPage, body.detailPage").length > 0) {
            var parameterVal = DIFC.Util.getURLParameterByName ("service_request_title");
            if((parameterVal != undefined || parameterVal != null) && parameterVal.length > 0) {
                intialize_page(parameterVal);

                load_SRDescription(DIFC.Util.getURLParameterByName ("type"));
                // $(".bPageTitle .ptBody .title").append($('<h3 style="display: block; font-size: 18px; margin-bottom: 15px;"><a target="_blank" href="'+ 
                //     decodeURIComponent(DIFC.Util.getURLParameterByName ("instruction_url")) +'" style="font-size: 14px;text-decoration: underline;">Click Here to View Application Guidelines</a></h3>'));

            }else if (jQuery("body .bPageTitle .ptBody .pageType:contains('Service Request')").length > 0){

                update_existing_SRPageTitle();
            }else {
                if(jQuery("body.detailPage").length > 0) {
                    window.pageHeaderTitle = jQuery(".bPageBlock .pbHeader .pbTitle .mainTitle");
                    jQuery("body.detailPage .bDetailBlock.bPageBlock[id^='ep'] .pbHeader table td.pbTitle").remove();
                    
                    jQuery(".bPageBlock .pbHeader table td#topButtonRow").children().
                    wrapAll("<div class='large-8 medium-10 small-12 large-centered medium-centered small-centered columns'><div class='buttonContainer' style='padding: 0 18%;'></div></div>");
                }
            }

        }else {
            intialize_page(null);
        }

        if(typeof(heading_update_callback) == 'function'){
            heading_update_callback();
        }
    };

    return {
        setPageHeading : initialize_pageHeader
    }

}());

DIFC.SRPage = (function () {
    this.pageTitle = "";
    this.SRId = 0;
    this.status = "";
    this.serviceAccess = ""
});
DIFC.SRPage.prototype.init = function (id, title, _status, _serviceType) {
    this.pageTitle = title;
    this.SRId = id;
    this.status = _status;
    if (_serviceType) {
        this.serviceAccess = _serviceType.toLowerCase()
    }
};

DIFC.Page = (function () {
    var _srPage = null;
    var pageTypes = {
        Other: -1,
        SREdit: 0,
        SRView: 1,
        Home: 2,
        Services: 3,
        Cases: 4,
        Accounts: 5,
        CompanyInfo: 6
    };

    var thisPage = function () {

        var _currentPage = 0;  
        if (jQuery("body.homeTab.homePage").length > 0) {
            _currentPage = pageTypes.SR;
        } else {
            if (jQuery("body.detailPage").length > 0) {
                if (jQuery(".bPageBlock .pbHeader .pbTitle h2:contains('Service Request')").length > 0) {
                    _currentPage = pageTypes.SRView;
                }
            } else {
                if (jQuery("body.editPage").length > 0) {
                    if (jQuery(".bPageBlock .pbHeader .pbTitle h2:contains('Service Request')").length > 0) {
                        _currentPage = pageTypes.SREdit;
                    }
                } else {
                    if (jQuery("body.homeTab.menu-services").length > 0) {
                        _currentPage = pageTypes.Services;
                    } else {
                        if (jQuery("body.accountTab.overviewPage")) {
                            _currentPage = pageTypes.Accounts;
                        } else {
                            if (jQuery("body.accountTab.overviewPage")) {
                                _currentPage = pageTypes.Accounts;
                            } else {
                                _currentPage = pageTypes.Other;
                            }
                        }
                    }
                }
            }
        }
        return _currentPage
    }
    return {
        init        : thisPage,
        currentPage : thisPage(),
        Types       : pageTypes,
        SRPageInfo  : _srPage
    }
}());

DIFC.SercurityManager = (function () {

    var SRStep_section = null;

    function init () {

        if(!SRStep_section){
            //Hide default steps
            var relListSteps = jQuery(".listRelatedObject .pbHeader .pbTitle h3:contains('Steps')");
            if(relListSteps.length > 0){
                relListSteps.parents('.bPageBlock').children('.pbBody').find('table.list tr.dataRow').addClass('hide');

                var stepElem_id = relListSteps.attr('id');
                stepElem_id = stepElem_id.substr(0, stepElem_id.indexOf('_title'));

                SRStep_section = jQuery('div.bRelatedList div[id$="'+ stepElem_id +'_body"]');
                jQuery(SRStep_section).find('div.pShowMore').addClass('hide');                
            }            
        }
    }
    function apply_settings(){
        init();
        loadUserSRSteps();  
        validate_SRActionBtn();      
    }

    function loadUserSRSteps() {
        try {

            var sr_id = DIFC.Util.getSRPageSRId();
            if(sr_id.length === 0)return;
            
            DIFC.Page.SRPageInfo.SRId = sr_id;

            sforce.connection.query("SELECT id, Name, Step_No__c, Step_Name__c, Step_Status__c, Owner__c, Owner.Name, CreatedDate, LastModifiedDate from Step__c Where SR__c='"+ DIFC.Page.SRPageInfo.SRId +"'  and (OwnerId='"+ DIFC.settings.userId +"' OR Owner.Name = 'Client Entry User' OR Owner.Name = 'Client Entry Approver')", {

            onSuccess : function(stepsResult){

                var userSteps = stepsResult.getArray('records');
                var stepList = jQuery(SRStep_section).find('table.list');
                jQuery(stepList).find('tr.dataRow').remove();

                for(var stepIndex = 0; stepIndex < userSteps.length; stepIndex++){

                    var step = userSteps[stepIndex];

                    jQuery('<tr class="dataRow" onblur="if (window.hiOff){hiOff(this);}" onfocus="if (window.hiOn){hiOn(this);}" onmouseout="if (window.hiOff){hiOff(this);}" onmouseover="if (window.hiOn){hiOn(this);}" style="display: table-row;">' +
                        '<td class="dataCell"></td>'+ //action column empty
                        '<td class="dataCell"><a href="' + DIFC.settings.PortalUrlPrefix + '/' + step.Id +'">'+ step.Name + '</a></td>' +
                        // '<td class="dataCell numericalColumn">' + step.Step_No__c + '</td>' +
                        '<td class="dataCell">' + step.Step_Name__c + '</td>' +
                        '<td class="dataCell">' + step.Step_Status__c + '</td>' +
                        //'<td class="dataCell">' + step.Owner.Name + '</td>' +
                        '<td class="dataCell">' + step.Owner__c + '</td>' +
                        // '<td class="dataCell DateElement">' + step.CreatedDate.split('T')[0] + '</td>' +
                        // '<td class="dataCell DateElement">' + step.LastModifiedDate.split('T')[0] + '</td>' +
                        '<td class="dataCell"></td></tr>').appendTo(stepList.find('tbody'));
                }

            }
           , onFailure: stepLoadingFailed});
        }catch(e) {
            console.log('SR steps: ' + e);
        }
    }
    /*
    function validate_SRActionBtn(){
        if(DIFC.Page.SRPageInfo && DIFC.Page.SRPageInfo.status != 'Draft'){
            jQuery('#topButtonRow, #bottomButtonRow').addClass('hide');
        }
    }*/
    
    function validate_SRActionBtn(){
        var isSRTemp = DIFC.Util.getURLParameterByName('isSR', window.location.href);
        var displaybtn = DIFC.Util.getURLParameterByName('upgrade', window.location.href) || '';
        console.log('isSR:  '+isSRTemp);
        jQuery('.dataCol > a').replaceWith(function() { return this.innerHTML; });
        if(DIFC.Page.SRPageInfo && DIFC.Page.SRPageInfo.status != 'Draft'){
            //console.log("SRPageInfo:  "+DIFC.Page.SRPageInfo);
            jQuery("td input[name='edit']").addClass('hide');
            jQuery("td input[name='submit']").addClass('hide');
            if(isSRTemp !='GS')
                jQuery("td input[name='cancel_sr']").addClass('hide');
        }
        if(DIFC.Page.SRPageInfo && isSRTemp=='GS' && (DIFC.Page.SRPageInfo.status == 'Draft')){
                jQuery("td input[name='upgrade_to_change']").addClass('hide');
                jQuery("td input[name='upgrade_to_vip']").addClass('hide');
                //jQuery("td#topButtonRow").addClass('hide');
                //jQuery("td#bottomButtonRow").addClass('hide');
        }

    }

    function stepLoadingFailed(error) {
        console.log("User Steps loading failed:" + error);
    } 

    return {
        init : init,
        apply_security : apply_settings
    }
}());
/********************************************************************************************/

DIFC.Util = (function(){

    Number.prototype.formatMoney = function(c, d, t){
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
       return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + 
                                                    (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
     };

    function getParameterByName(name,url) {

        //Defult url page url
        if(typeof(url)==='undefined')
            url = location.search;

        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(url);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    function getHashparameterByName(name) {
        var lochash    = location.hash.substr(1),
            mylocation = lochash.substr(lochash.indexOf(name +'='))
                          .split('&')[0]
                          .split('=')[1];

        return ((mylocation == 'undefined' || !mylocation) ?  mylocation : decodeURIComponent(mylocation));
    }

    function escapeCharacters(unsafe) {

        if(unsafe && typeof unsafe != 'undefined'){
            return unsafe
                 .replace(/&/g, "&amp;")
                 .replace(/</g, "&lt;")
                 .replace(/>/g, "&gt;")
                 .replace(/"/g, "&quot;")
                 .replace(/'/g, "&#039;");
        }
        return unsafe;
     }

    function loadIframe(url, id, callback) {
        var sidebar_frame = document.createElement("iframe");
        sidebar_frame.id = id;
        sidebar_frame.src = url;
        if (navigator.userAgent.indexOf("MSIE") > -1 && !window.opera) {
            sidebar_frame.onreadystatechange = function () {
                if (sidebar_frame.readyState == "loaded" || sidebar_frame.readyState == "complete") {
                    sidebar_frame.onreadystatechange = null;
                    callback(id);
                }
            };
        } else {
            sidebar_frame.onload = function () {
                callback(id);
            };
        }
        var contentWrapper = document.getElementById("contentWrapper");
        if (typeof contentWrapper != "undefined" && contentWrapper !== null) {
            jQuery(sidebar_frame).insertBefore(contentWrapper);
            jQuery(sidebar_frame).attr("style", "display: none !important");
        }
    } 

    function embedCSSTyle (css) {
        var head = document.head || document.getElementsByTagName('head')[0],
            style = document.createElement('style');

        style.type = 'text/css';
        if (style.styleSheet){
          style.styleSheet.cssText = css;
        } else {
          style.appendChild(document.createTextNode(css));
        }

        head.appendChild(style);        
    } 

    function SRPage_SRId(){
        var srNumParserRegExp = new RegExp('^\\'+ DIFC.settings.PortalUrlPrefix +'\\/('+ DIFC.settings.ServiceReq_prefix +'\\w*)\\/*', 'g'),
            SRPage_URLComponents = srNumParserRegExp.exec(decodeURIComponent(sfdcPage.getHrefAsRetURL())),
            serviceRequestId = '';

        if(SRPage_URLComponents !== null && SRPage_URLComponents.length >= 2)
            serviceRequestId = SRPage_URLComponents[1];
        return serviceRequestId;        
    }

    return {
        getURLParameterByName       : getParameterByName,
        getUrlHashparameterByName   : getHashparameterByName,
        escapeHtmlCharacters        : escapeCharacters,
        embedCSSStyleTag            : embedCSSTyle,
        getSRPageSRId               : SRPage_SRId,
        load_settings_IFrame_async  : loadIframe
    };

}());



/* jQuery Tiny Pub/Sub - v0.7 - 10/27/2011
 * http://benalman.com/
 * Copyright (c) 2011 "Cowboy" Ben Alman; Licensed MIT, GPL */
(function(a){var b=a({});a.subscribe=function(){b.on.apply(b,arguments)},a.unsubscribe=function(){b.off.apply(b,arguments)},a.publish=function(){b.trigger.apply(b,arguments)}})(jQuery)


/********************* DIFC Payment Module ************************/
DIFCPayment = (function(){
    
    var PaymentGateway_URL = '',
        CustomerId         = '',
        ReceiptPrefix      = '',
        AccountBPNumber    = '',
        receipt_amount_id  = '';

    function setup(gatewayURL, accountId, receiptPrefix, BPNum, receipt_amountId ) {
        PaymentGateway_URL  = gatewayURL;
        CustomerId          = accountId;
        ReceiptPrefix       = receiptPrefix;
        AccountBPNumber     = BPNum;
        receipt_amount_id   = receipt_amountId;

        initEvents();
    }

    function initEvents() {
        /*
        $('form#topupBalance').on("submit", function(e){

            e.preventDefault();                
            var $topupBalanceForm = $(this),
                $paymentOption = $topupBalanceForm.find("input[name='pBalance']:checked").parent().attr('paymentType');
            $topup_amount = $topupBalanceForm.find("input#balance-input").val();
            switch($paymentOption){
                case 'bank':
                    pay_by_bank($topup_amount);
                    break;
                case 'cash':
                    pay_by_cash($topup_amount);
                    break;
                case 'cheque':
                    pay_by_cheque($topup_amount);
                    break;
                case 'online':
                    pay_by_card($topup_amount);                        
                    break;
                default:
                    break;
            }                          
        }); 
        */

        payment_optionChanged();

        jQuery('form#topupBalance')
            .on("submit", function(e) {e.preventDefault();})
            .on('invalid', function () {
                var invalid_fields = jQuery(this).find('[data-invalid]');
                console.log(invalid_fields);
            })
            .on('valid', function () {
                console.log('valid!');

            var $topupBalanceForm = jQuery(this),
                $paymentOption = $topupBalanceForm.find("input[name='pBalance']:checked").parent().attr('paymentType');
            $topup_amount = $topupBalanceForm.find("input#balance-input").val();
            switch($paymentOption){
                case 'bank':
                    //pay_by_bank($topup_amount);
                    break;
                case 'cash':
                    // pay_by_cash($topup_amount);
                    break;
                case 'cheque':
                    pay_by_cheque($topup_amount);
                    break;
                case 'online':
                    pay_by_card($topup_amount);                        
                    break;
                default:
                    break;
            }                
        }); 

    }

    function payment_optionChanged() {

        jQuery("form#topupBalance input[type='radio']").on("change", function(){
            var $paymentOption = jQuery(this).parent().attr('paymentType');

            switch($paymentOption) {
               case 'bank':jQuery
                    //alert('Payment Option -bank- selected!');                        
                    toggle_topup(false); 
                    pay_by_bank();                                           
                    break;                        
               case 'cash':                       
                    toggle_topup(false);
                    pay_by_cash();                    
                    //alert('Payment Option -cash- selected!');                        
                    break;
               case 'cheque':
               case 'online':
                    //enable amount field  
                    toggle_topup(true);
                    break;
            }
         });
        
        function toggle_topup(enable) {
            var $amount_input = jQuery("form#topupBalance input#balance-input"), 
                $topup_btn = jQuery('form#topupBalance input[type="submit"]');
            if(!enable) {
                //$amount_input.parent().removeClass('error');
                $amount_input.attr('disabled', '');
                $topup_btn.attr('disabled', '');
            }else {
                //$amount_input.parent().removeClass('error');
                $amount_input.removeAttr('disabled');
                $topup_btn.removeAttr('disabled');
            }
        }        
    }
    
    
    function pay_by_card(amount){

        if (confirm('Please note a service charge of 2.5% of the top-up amount will be charged on your card.\n\n Do you want to proceed?')) {
            
            try {

                var reqval = sforce.apex.execute("CLS_Payment","Paymentformsubmit",{accId:CustomerId,amt:amount,serv:"PortalDeposit"}),
                    loc =  PaymentGateway_URL, //"{!$Label.Transaction_URL}";
                    form = document.createElement("form");
                
                var req = document.createElement("input");
                req.setAttribute("type","hidden");
                req.setAttribute("name","requestParameter");
                req.setAttribute("value",reqval);
                form.appendChild(req);
                
                form.setAttribute("method", "post");
                form.setAttribute("action", loc);
                form.setAttribute("target", "_blank");
                window.parent.document.body.appendChild(form);
                form.submit();
            }catch(ex){
                console.log("Payment Submit error : "  + ex);
                alert("Sorry! Something wrong happened. Please inform DIFC Support team about the issue.")
            }

        } else {
            // Do nothing!
        } 
    }
    
    function pay_by_bank(amount){
        var message = "Your Business Partner Number is " + AccountBPNumber + ".\nPlease include this " + 
            "number in your bank transfer remarks for faster processing."
        window.parent.alert(message);                
    }
    
    function pay_by_cheque(amount){
        if(!window.location.origin)
            window.location.origin = window.location.protocol+"//"+window.location.host;
        var receipt_url = window.location.origin + UserContext.siteUrlPrefix + '/'+ ReceiptPrefix + '/' +
                        'e?receipt_id='+ receipt_amount_id +'&recipt_type=Cheque&amount=' + amount + '&retURL=%2Fcustomers%2Fhome';
        window.parent.location = receipt_url;
    }

    function pay_by_cash(amount){
        var message = "Please proceed to RoC counter to deposit the cash amount. " + 
            "Once cash amount is received the account balance will be updated."
        window.parent.alert(message);                

        // if(!window.location.origin)
        //     window.location.origin = window.location.protocol+"//"+window.location.host;
        // var receipt_url = window.location.origin + UserContext.siteUrlPrefix + '/' + ReceiptPrefix + '/' +
        //                 'e?recipt_type=Cash&payment_status=Pending&amount=' + amount;
        // window.parent.location = receipt_url;                
    }            
    
    return {
        init: setup
    };
    
}()); 
/********************************************************************************/
