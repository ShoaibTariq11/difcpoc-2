/*
    Author      : Durga Prasad
    Date        : 15-Dec-2019
    Description : Custom code to check the validations on ROS Manager step in AOR
    ---------------------------------------------------------------------------------------
*/
global without sharing class CC_AOR_RS_ManagerValidation implements HexaBPM.iCustomCodeExecutable 
{
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) 
    {
        system.debug('@@@@@@@@@@@ CC_AOR_RS_ManagerValidation ');
        
        string strResult = 'Success';
        HexaBPM__Service_Request__c srRec;

        if(stp!=null && stp.HexaBPM__SR__c!=null)
        {
            system.debug('@@@@@@@@@@@ stp.HexaBPM__SR__c  '+stp.HexaBPM__SR__c);
            for(HexaBPM__Service_Request__c objSR:[SELECT Id,
                                                          Authorized_Signatory_Combinations__c,
                                                          Authorized_Signatory_Combinations_Arabic__c,
                                                          (
                                                              SELECT id,
                                                                     Amendment_Type__c 
                                                                FROM Amendments__r
                                                               WHERE Amendment_Type__c = 'Operating Location'
                                                          ) 
                                                     FROM HexaBPM__Service_Request__c 
                                                    WHERE Id=:stp.HexaBPM__SR__c])
            {
                system.debug('@@@@@@@@@@@ objSR '+objSR);
                srRec= objSR;
                if(objSR.Authorized_Signatory_Combinations__c == null || objSR.Authorized_Signatory_Combinations_Arabic__c == null)
                {
                    strResult = 'Authorised Signatory Combinations are required';
                    return strResult;
                }
                    
            }
            system.debug('@@@@@@@@@@@ srRec '+srRec);
            /*
                for(Account acc:[Select Id,Hosting_Company__c,(Select Id from Operating_Locations__r where Status__c='Active' and IsRegistered__c=true limit 1) from Account where Id=:stp.HexaBPM__SR__r.HexaBPM__Customer__c])
                {
                    if(stp.HexaBPM__SR__r.Name_of_the_Hosting_Affiliate__c==null && (acc.Operating_Locations__r==null || (acc.Operating_Locations__r!=null && acc.Operating_Locations__r.size()==0))){
                        if(strResult=='Success')
                            strResult = 'No active operating location / hosting company under the account to proceed';
                        else
                            strResult = strResult+' & No active operating location / hosting company under the account to proceed';
                    }
                }
            */
            
            // Valication to check wether SR have Amendment of type Operating Location.
            if( srRec!= NULL 
                    && srRec.Amendments__r== NULL
                        || (srRec.Amendments__r != NULL && srRec.Amendments__r.size() <= 0 )   
              )
             {
                   strResult = 'Please make sure the entity selects a registered address.';
                   return strResult; 
             }

        }
        return strResult;
    }
}