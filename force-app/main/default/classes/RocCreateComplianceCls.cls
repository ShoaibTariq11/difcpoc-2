/*
    * Created By : Ravi
    * Description : This class will create the Compliance records for various notification like License, Data Protection,Annual reture,appointing an auditor etc
    * Modification History : 
        - on 29th Nov, changed the Data Protection Renewal Compliance name to "Notification Renewal" as per #329
        - ticket-2036, In compliance calendar of RLLP, the appointment of auditor is not required (Swati)
  V1.3  - Restricted 'Allotment of shares and Membership intrest'compliance creation if Registration Type of Account is 'Transfer' as per # 3775 
  V1.4  - Extending the Compliance calender of Allotment of shares compliance from 14 days to 30 days as per # 2553
  V1.5  - Restricted 'Allotment of shares and Membership intrest'compliance creation if Registration Type of Account legal activity is Investmentfund or Legal type of entity is LTD IC as per tkt # 3667
  v.1.6 -Changed to 90 days  
  v1.7  assign account owner when compliance created tkt # 5100
  V1.8 -Added for fintech company Ticket #5367
    V1.9 - New compnay law 5858
*/
public without sharing class RocCreateComplianceCls {
    public static string CreateComplianceAOR(Step__c objStep)
    {
        if(objStep != null && objStep.SR__r.Customer__c != null){
            try{
                Account objAccount;
                list<Compliance__c> lstCompliance = new list<Compliance__c>();
                Compliance__c objCompliance;
                Set<String> recognizedStructure = new Set<String>{'RLLP','RP','RLP'};
                //Account.Active_License__c
                for(Account objAcc : [select Id,Name,OwnerID,Owner.ManagerID,Legal_Type_of_Entity__c,E_mail__c,Principal_User__c,Has_Permit_1__c,Has_Permit_2__c,Has_Permit_3__c,Next_Renewal_Date__c,ROC_reg_incorp_Date__c,License_Activity__c,FinTech_Classification__c,Registration_Type__c from Account where Id=:objStep.SR__r.Customer__c]){  // V1.3 Added Registration_Type__c 
                    objAccount = objAcc;
                }
                if(objAccount != null)
                {
                    list<Contact> lstContacts = [select Id from Contact where AccountId=:objAccount.Id AND RecordType.DeveloperName='Portal_User' AND Id IN (select ContactId from User where Contact.AccountId=:objAccount.Id AND IsActive=true AND Community_User_Role__c INCLUDES ('Company Services'))];
                    
                    if(objAccount.Legal_Type_of_Entity__c !='LTD SPC')
                    {
                        //License Renewal Compliance
                        objCompliance = new Compliance__c();
                        objCompliance.Name = 'License Renewal';
                        objCompliance.Account__c = objAccount.Id;
                        objCompliance.Start_Date__c = objAccount.Next_Renewal_Date__c.addMonths(-1);
                        objCompliance.End_Date__c = objAccount.Next_Renewal_Date__c;
                        objCompliance.Exception_Date__c = objCompliance.End_Date__c.addDays(30);
                        objCompliance.Status__c = 'Created';
                        //objCompliance.Customer_Email__c =  objAccount.E_mail__c;
                        //objCompliance.Principal_User__c = objAccount.Principal_User__c;
                        objCompliance = PopulatePortalUsers(objAccount,objCompliance,lstContacts);
                        lstCompliance.add(objCompliance);
                        
                        //Notification Renewal Compliance
                        if(objAccount.Has_Permit_1__c || objAccount.Has_Permit_2__c || objAccount.Has_Permit_3__c){
                            objCompliance = new Compliance__c();
                            objCompliance.Name = 'Notification Renewal';
                            objCompliance.Account__c = objAccount.Id;
                            objCompliance.Start_Date__c = objAccount.Next_Renewal_Date__c.addMonths(-1);
                            objCompliance.End_Date__c = objAccount.Next_Renewal_Date__c;
                            objCompliance.Exception_Date__c = objCompliance.End_Date__c.addDays(30);
                            objCompliance.Status__c = 'Created';
                            //objCompliance.Customer_Email__c =  objAccount.E_mail__c;
                            //objCompliance.Principal_User__c = objAccount.Principal_User__c;
                            objCompliance = PopulatePortalUsers(objAccount,objCompliance,lstContacts);
                            lstCompliance.add(objCompliance);
                        }
                    }
                    
                    /*
                     V1.9
                    if((objAccount.Legal_Type_of_Entity__c !='LTD SPC' && objAccount.Legal_Type_of_Entity__c.contains('LTD')) || (objAccount.Legal_Type_of_Entity__c == 'LLC' || objAccount.Legal_Type_of_Entity__c == 'FRC')){
                        //Annual Return Compliance 
                        objCompliance = new Compliance__c();
                        objCompliance.Name = 'Annual Return';
                        objCompliance.Account__c = objAccount.Id;
                        Date NextYearDate = system.today().addYears(1).addDays(-1);
                        if(system.today().daysBetween(NextYearDate) > 365)
                            objCompliance.Start_Date__c = Date.newInstance(system.today().addYears(1).year(), 1, 1);    
                        else
                            objCompliance.Start_Date__c = Date.newInstance(system.today().addYears(2).year(), 1, 1);
                        objCompliance.End_Date__c = Date.newInstance(objCompliance.Start_Date__c.year(), 3, 31);
                        objCompliance.Status__c = 'Created';
                        //objCompliance.Customer_Email__c =  objAccount.E_mail__c;
                        //objCompliance.Principal_User__c = objAccount.Principal_User__c;
                        objCompliance = PopulatePortalUsers(objAccount,objCompliance,lstContacts);
                        if(objAccount.License_Activity__c != null && objAccount.License_Activity__c.indexOf('012') == -1 ){
                            lstCompliance.add(objCompliance);
                        }
                    }
                    
                    */
                    if(objAccount.Legal_Type_of_Entity__c =='LTD SPC')
                    {
                        //Annual Return Compliance 
                        objCompliance = new Compliance__c();
                        objCompliance.Name = 'Annual Reporting';
                        objCompliance.Account__c = objAccount.Id;
                        //objCompliance.Start_Date__c = Date.newInstance(system.today().addYears(1).year(), 1, 1);
                        Date NextYearDate = system.today().addYears(1).addDays(-1);
                        if(system.today().daysBetween(NextYearDate) > 365)
                            objCompliance.Start_Date__c = Date.newInstance(system.today().addYears(1).year(), 1, 1);    
                        else
                            objCompliance.Start_Date__c = Date.newInstance(system.today().addYears(2).year(), 1, 1);
                        objCompliance.End_Date__c = Date.newInstance(objCompliance.Start_Date__c.year(), 1, 31);
                        objCompliance.Status__c = 'Created';
                        //objCompliance.Customer_Email__c =  objAccount.E_mail__c;
                        //objCompliance.Principal_User__c = objAccount.Principal_User__c;
                        objCompliance = PopulatePortalUsers(objAccount,objCompliance,lstContacts);
                        lstCompliance.add(objCompliance);
                    }
                    
                    //if(objAccount.Legal_Type_of_Entity__c != 'LTD SPC' && !recognizedStructure.contains(objAccount.Legal_Type_of_Entity__c) 
                      //  && (objAccount.Legal_Type_of_Entity__c.contains('Public Company') || objAccount.Legal_Type_of_Entity__c.contains('LLP') )
                    //)
                    if(objAccount.Legal_Type_of_Entity__c=='Public Company')
                    {
                        //Appointing an Auditor Compliance
                        objCompliance = new Compliance__c();
                        objCompliance.Name = 'Appointing an Auditor';
                        objCompliance.Account__c = objAccount.Id;
                        objCompliance.Start_Date__c = system.today();
                        objCompliance.End_Date__c = system.today().addMonths(18);
                        objCompliance.Status__c = 'Open';
                        //objCompliance.Customer_Email__c =  objAccount.E_mail__c;
                        //objCompliance.Principal_User__c = objAccount.Principal_User__c;
                        objCompliance = PopulatePortalUsers(objAccount,objCompliance,lstContacts);
                        
                      //  if(objAccount.Legal_Type_of_Entity__c !='LTD')//Auditor not required after new law 5858  V1.9
                        lstCompliance.add(objCompliance);
                    
                    }
                    
                    // V1.9 disable 
                    /*
                    //Allotment of Shares/Membership Interest  Compliance
                    string investmentFundLicenseActivity ='042'; // V1.5                     
                    if((objAccount.Legal_Type_of_Entity__c =='LTD' || objAccount.Legal_Type_of_Entity__c =='LLC' || objAccount.Legal_Type_of_Entity__c =='SFO LTD') && objAccount.Registration_Type__c !='Transfer' && objAccount.License_Activity__c !=null && !objAccount.License_Activity__c.Contains(investmentFundLicenseActivity)  && objAccount.FinTech_Classification__c!='FinTech'){   // V1.3 // V1.5
                        objCompliance = new Compliance__c();
                        objCompliance.Name = 'Allotment of Shares/Membership Interest';
                        objCompliance.Account__c = objAccount.Id;
                        objCompliance.Start_Date__c = system.today();
                        //objCompliance.End_Date__c = system.today().addDays(14);  V1.4
                        objCompliance.End_Date__c = system.today().addDays(90);  // V1.4//v.17
                        objCompliance.Status__c = 'Open';
                        //objCompliance.Customer_Email__c =  objAccount.E_mail__c;
                        //objCompliance.Principal_User__c = objAccount.Principal_User__c;
                        objCompliance = PopulatePortalUsers(objAccount,objCompliance,lstContacts);
                        lstCompliance.add(objCompliance);   
                    }
                    */
                    
                    
                        //V 1.9 enable
                    if(objAccount.Legal_Type_of_Entity__c !='LTD SPC') 
                    {
                        objCompliance = new Compliance__c();
                        objCompliance.Name = 'Confirmation Statement';
                        objCompliance.Account__c = objAccount.Id;
                       
                       objCompliance.Start_Date__c = objAccount.Next_Renewal_Date__c.addMonths(-1);
                        objCompliance.End_Date__c = objAccount.Next_Renewal_Date__c;
                        objCompliance.Exception_Date__c = objCompliance.End_Date__c.addDays(30);
                        objCompliance.Status__c = 'Created';
                        
                        
                        objCompliance = PopulatePortalUsers(objAccount,objCompliance,lstContacts);
                        lstCompliance.add(objCompliance); 
                    }
                    
                    
                    //Filing Financial Statements Compliance
                    /* Commented as per #1406
                    if(objAccount.Legal_Type_of_Entity__c =='NPIO'){
                        objCompliance = new Compliance__c();
                        objCompliance.Name = 'Filing Financial Statements';
                        objCompliance.Account__c = objAccount.Id;
                        if(system.today().month() > 3)
                            objCompliance.Start_Date__c = Date.newInstance(system.today().addYears(1).Year(),4,1);
                        else
                            objCompliance.Start_Date__c = Date.newInstance(system.today().Year(),4,1);
                        objCompliance.End_Date__c = objCompliance.Start_Date__c.addMonths(6).addDays(-1);
                        objCompliance.Status__c = 'Created';
                        //objCompliance.Customer_Email__c =  objAccount.E_mail__c;
                        //objCompliance.Principal_User__c = objAccount.Principal_User__c;
                        objCompliance = PopulatePortalUsers(objAccount,objCompliance,lstContacts);
                        lstCompliance.add(objCompliance);   
                    }*/
                    if(!lstCompliance.isEmpty())
                        insert lstCompliance;
                }
                
            }catch(Exception ex){
                system.debug('Exception is : '+ex.getMessage()+ex.getLineNumber()+ex.getCause());
                Log__c objLog = new Log__c();
                objLog.Account__c = objStep.SR__r.Customer__c;
                objLog.Description__c = ex.getMessage();
                objLog.Type__c = 'Compliance Creation';
                insert objLog;
                return ''+ex.getMessage();
            }
        }
        return 'Success';
    }
    
    public static string CreateComplianceAOA(Step__c objStep){
        Account objAccount;
        list<Compliance__c> lstCompliance = new list<Compliance__c>();
        Compliance__c objCompliance;
        try{
            for(Account objAcc : [select Id,Name,Legal_Type_of_Entity__c,OwnerID,Owner.ManagerID,Principal_User__c,(select Id from Contacts where RecordType.DeveloperName='Portal_User') from Account where Id=:objStep.SR__r.Customer__c]){
                objAccount = objAcc;
            }
            list<Contact> lstContacts = [select Id from Contact where AccountId=:objAccount.Id AND RecordType.DeveloperName='Portal_User' AND Id IN (select ContactId from User where Contact.AccountId=:objAccount.Id AND IsActive=true)];
            //if(objAccount != null){
                //Amendment of the articles of association
                objCompliance = new Compliance__c();
                objCompliance.Name = 'Amendment of the Articles of Association';
                objCompliance.Account__c = objStep.SR__r.Customer__c;//objAccount.Id;
                objCompliance.Start_Date__c = system.today();
                objCompliance.End_Date__c = system.today().addDays(14);
                objCompliance.Status__c = 'Open';
                objCompliance.Amendment_Type__c = objStep.SR__r.Record_Type_Name__c;
                //objCompliance.Customer_Email__c =  objAccount.E_mail__c;
                //objCompliance.Principal_User__c = objAccount.Principal_User__c;
                objCompliance = PopulatePortalUsers(objAccount,objCompliance,lstContacts);
                lstCompliance.add(objCompliance);
            //}
        }catch(Exception ex){
            Log__c objLog = new Log__c();
            objLog.Account__c = objStep.SR__r.Customer__c;
            objLog.Description__c = ex.getMessage();
            objLog.Type__c = 'Compliance Creation';
            insert objLog;
            return ''+ex.getMessage();              
        }
        return 'Success';
    }
    
    public static Compliance__c PopulatePortalUsers(Account objAccount,Compliance__c objComp,list<Contact> lstContacts){
        if(lstContacts != null && lstContacts.isEmpty() == false){
            if(lstContacts.size() > 0)
                objComp.Principal_User__c = lstContacts[0].Id;
            if(lstContacts.size() > 1)
                objComp.Principal_User_2__c = lstContacts[1].Id;
            if(lstContacts.size() > 2)
                objComp.Principal_User_3__c = lstContacts[2].Id;
            if(lstContacts.size() > 3)
                objComp.Principal_User_4__c = lstContacts[3].Id;
            if(lstContacts.size() > 4)
                objComp.Principal_User_5__c = lstContacts[4].Id;
            
            objComp.Relationship_Manager__c = objAccount.OwnerID;
            
            if(objAccount.Owner.ManagerID != null)
                objComp.Manager__c = objAccount.Owner.ManagerID;
            
        }else if(objAccount != null){
            
                objComp.Principal_User__c = objAccount.Principal_User__c;
               
        }

        return objComp;
    }
    
}