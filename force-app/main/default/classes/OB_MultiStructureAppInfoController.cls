/******************************************************************************************
*  Name        : OB_MultiStructureAppInfoController 
*  Author      : Maruf Bagwan
*  Company     : PwC
*  Date        : 24-March-2020
*  Description : Controller for Multi Structure Application Controller 
----------------------------------------------------------------------------------------                               
Modification History 
----------------------------------------------------------------------------------------
V.No      Date          Updated By          Description
----------------------------------------------------------------------------------------              
V1.0   24-March-2020   Maruf Bagwan         Initial Version
*******************************************************************************************/
public without sharing class OB_MultiStructureAppInfoController {
    
    
    public class RespondWrap {
        @AuraEnabled
        public list<License_Activity__c> listLiscAct { get; set; }
        @AuraEnabled
        public HexaBPM__Section_Detail__c ButtonSection { get; set; }
        @AuraEnabled
        public string licenseApplicationId { get; set; }
        @AuraEnabled
        public boolean hasError { get; set; }
        @AuraEnabled
        public string ErrorMessage { get; set; }
        @AuraEnabled
        public string uploadedFileName { get; set; }
        
        @AuraEnabled
        public string SearchBarLable { get; set; }
        @AuraEnabled
        public HexaBPM__Service_Request__c serviceObj { get; set; }
        @AuraEnabled
        public List<HexaBPM__Service_Request__c> serviceObjList { get; set; }
        public RespondWrap() {
            hasError = false;
            serviceObj = new HexaBPM__Service_Request__c();
            ErrorMessage = '';
            listLiscAct = new list<License_Activity__c>();
            serviceObjList = new List<HexaBPM__Service_Request__c>();
            ButtonSection = new HexaBPM__Section_Detail__c();
        }
    }
    
    @AuraEnabled
    public static RespondWrap getEntityRecords(String srId,String pageId, String docMasterCode){
        RespondWrap respWrap = new RespondWrap();
        try{
            List<HexaBPM__Service_Request__c> entityRecList = new List<HexaBPM__Service_Request__c>();
            entityRecList = [SELECT Id,Entity_Name__c,Entity_Type__c,Business_Sector__c,legal_structures__c,Source_of_income__c,Explanation_of_Sources_and_Funds__c,HexaBPM__Parent_SR__c,RecordTypeId FROM HexaBPM__Service_Request__c WHERE HexaBPM__Parent_SR__c=:srId];
            respWrap.serviceObjList = entityRecList;
            
            for(HexaBPM__Section_Detail__c sectionObj :[SELECT id, HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection'
                                                        AND HexaBPM__Section__r.HexaBPM__Page__c = :pageId LIMIT 1]) {
                                                            respWrap.ButtonSection = sectionObj;
                                                        }
            
            for(HexaBPM__SR_Doc__c srDoc : [SELECT id,File_Name__c,
                                            HexaBPM__Document_Master__r.HexaBPM__Code__c 
                                            FROM HexaBPM__SR_Doc__c
                                            WHERE HexaBPM__Service_Request__c = :srId 
                                            AND HexaBPM__Document_Master__r.HexaBPM__Code__c =:docMasterCode
                                            LIMIT 1]){
                                                respWrap.uploadedFileName =  srDoc.File_Name__c;
                                            }
            
        } catch(Exception exp){
            respWrap.hasError = true;
            respWrap.ErrorMessage = exp.getMessage();
        }
        return respWrap;
    }
    
    @AuraEnabled
    public static HexaBPM__Service_Request__c getCurrentEntity(String srId, string currentSrId){
        if(currentSrId != null && currentSrId != ''){
            List<HexaBPM__Service_Request__c> entityRecList = new List<HexaBPM__Service_Request__c>();
            entityRecList = [SELECT Id,Entity_Name__c,Entity_Type__c,Setting_Up__c,Type_of_Entity__c,Business_Sector__c,legal_structures__c,Source_of_income__c,Explanation_of_Sources_and_Funds__c,HexaBPM__Parent_SR__c,RecordTypeId FROM HexaBPM__Service_Request__c WHERE Id =:currentSrId];
            if(entityRecList != null && entityRecList.size() > 0){
                return entityRecList[0];
            } else {
                HexaBPM__Service_Request__c entityRec = new HexaBPM__Service_Request__c();
                return entityRec;
            }
        } else {
            HexaBPM__Service_Request__c entityRec = new HexaBPM__Service_Request__c();
            entityRec.Entity_Type__c = 'Non - financial';
            return entityRec;
        }
    }
    
    @AuraEnabled
    public static HexaBPM__Service_Request__c createEntityRecInstance(){
        HexaBPM__Service_Request__c entityRec = new HexaBPM__Service_Request__c();
        return entityRec;
    }
    
    @AuraEnabled
    public static HexaBPM__Service_Request__c  saveEntityRecord(HexaBPM__Service_Request__c entityRec, String srId){
        try{
            Id userAccountID = null;
            User currentUser = new User();
            for(User usrObj : OB_AgentEntityUtils.getUserById(userInfo.getUserId())) {
                //1.1
                if(usrObj.ContactId!=null && usrObj.contact.AccountId!=null) {
                    userAccountID = usrObj.contact.AccountId;
                }
            }
            Id multiStructRecTypeId = null; 
            multiStructRecTypeId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('Multi Structure').getRecordTypeId();
            entityRec.RecordTypeId = multiStructRecTypeId;
            entityRec.HexaBPM__Parent_SR__c = srId;
            entityRec.HexaBPM__Customer__c = userAccountID;
            upsert entityRec;
            return entityRec;
        } catch(Exception exp){
            return null;
        }
    }
    @AuraEnabled
    public static RespondWrap deleteEntityAndActivity(String srId, String currentSrId){
        RespondWrap respWrap = new RespondWrap();
        String status = '';
        List<HexaBPM__Service_Request__c> entityDeletRecList = new List<HexaBPM__Service_Request__c>();
        List<License_Activity__c> activityList = new List<License_Activity__c>();
        try{
            if(srId != null && srId != ''){
                for(HexaBPM__Service_Request__c srRec:[SELECT Id,(SELECT Id FROM Activities__r) FROM HexaBPM__Service_Request__c WHERE Id=:currentSrId]){
                    entityDeletRecList.add(srRec);
                    for(License_Activity__c la:srRec.Activities__r){
                        activityList.add(la);
                    }
                }
                if(activityList != null && activityList.size() > 0){
                    Delete activityList;
                }
                if(entityDeletRecList != null && entityDeletRecList.size() > 0){
                    Delete entityDeletRecList;
                }
                List<HexaBPM__Service_Request__c> entityRecList = new List<HexaBPM__Service_Request__c>();
                entityRecList = [SELECT Id,Entity_Name__c,Entity_Type__c,Business_Sector__c,legal_structures__c,Source_of_income__c,Explanation_of_Sources_and_Funds__c,HexaBPM__Parent_SR__c,RecordTypeId FROM HexaBPM__Service_Request__c WHERE HexaBPM__Parent_SR__c=:srId];
                respWrap.serviceObjList = entityRecList;
            } else {
                respWrap.hasError = true;
                respWrap.ErrorMessage = 'Blank EntityId.';
            }
        } catch(Exception exp){
            respWrap.hasError = true;
            respWrap.ErrorMessage = exp.getMessage();
        }
        return respWrap;
    }
    
    @AuraEnabled
    public static RespondWrap addNewEntity(string srId){
        RespondWrap respWrap = new RespondWrap();
        List<HexaBPM__Service_Request__c> entityRecList = new List<HexaBPM__Service_Request__c>();
        HexaBPM__Service_Request__c entityRec = new HexaBPM__Service_Request__c();
        try{
            entityRecList = [SELECT Id,Entity_Name__c,Entity_Type__c,Business_Sector__c,legal_structures__c,Source_of_income__c,Explanation_of_Sources_and_Funds__c,HexaBPM__Parent_SR__c,RecordTypeId FROM HexaBPM__Service_Request__c WHERE HexaBPM__Parent_SR__c=:srId];
            entityRec.RecordTypeId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('Multi Structure').getRecordTypeId();
            entityRec.HexaBPM__Parent_SR__c = srId;
            entityRecList.add(entityRec);
            respWrap.serviceObjList = entityRecList;
        } catch(Exception exp){
            respWrap.hasError = true;
            respWrap.ErrorMessage = exp.getMessage();
        }
        return respWrap;
    }
    
    // dynamic button section
    @AuraEnabled
    public static ButtonResponseWrapper getButtonAction(string SRID, string ButtonId, string pageId) {
        
        
        ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
        HexaBPM__Service_Request__c objRequest = OB_QueryUtilityClass.QueryFullSR(SRID);
        PageFlowControllerHelper.objSR = objRequest;
        PageFlowControllerHelper objPB = new PageFlowControllerHelper();
        
        respWrap.tester = OB_QueryUtilityClass.minRecValidationCheck(SRID, 'Approved Person');
        objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, SRID, pageId);
        upsert objRequest;
        
        OB_RiskMatrixHelper.CalculateRisk(SRID);
        
        PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
        system.debug('@@@@@@@@2 responseNextPage ' + responseNextPage);
        respWrap.pageActionName = responseNextPage.pg;
        respWrap.communityName = responseNextPage.communityName;
        respWrap.CommunityPageName = responseNextPage.CommunityPageName;
        respWrap.sitePageName = responseNextPage.sitePageName;
        respWrap.strBaseUrl = responseNextPage.strBaseUrl;
        respWrap.srId = objRequest.Id;
        respWrap.flowId = responseNextPage.flowId;
        respWrap.pageId = responseNextPage.pageId;
        respWrap.isPublicSite = responseNextPage.isPublicSite;
        
        system.debug('@@@@@@@@2 respWrap.pageActionName ' + respWrap.pageActionName);
        return respWrap;
    }
    
    //upload Structure diagram
    @AuraEnabled
    public static string uploadStructureDiagram(string srId,string contentDocumentId){
        try{
            List<HexaBPM__SR_Doc__c> existingSRDoc = new List<HexaBPM__SR_Doc__c>();
            List<HexaBPM__SR_Doc__c> srDocToUpdate = new List<HexaBPM__SR_Doc__c>();
            HexaBPM__SR_Doc__c srDocsObj = new HexaBPM__SR_Doc__c();
            string docMasterCode = 'Structure_diagram';
            string srTemplateId = '';
            HexaBPM__SR_Template_Docs__c srTempDoc = new HexaBPM__SR_Template_Docs__c();
            for(HexaBPM__Service_Request__c sr :[SELECT id, 
                                                 HexaBPM__SR_Template__c
                                                 FROM HexaBPM__Service_Request__c
                                                 WHERE id = :srId
                                                 LIMIT 1]) {
                                                     srTemplateId = sr.HexaBPM__SR_Template__c;
                                                 }
            system.debug('srTemplateId==>'+srTemplateId);
            for(HexaBPM__SR_Template_Docs__c srTempDocs :[SELECT id, 
                                                          HexaBPM__Document_Master__r.Name, 
                                                          HexaBPM__Document_Master__c, 
                                                          HexaBPM__Document_Master__r.HexaBPM__Code__c
                                                          FROM HexaBPM__SR_Template_Docs__c
                                                          WHERE HexaBPM__SR_Template__c = :srTemplateId
                                                          AND HexaBPM__Document_Master__r.HexaBPM__Code__c =: docMasterCode LIMIT 1]) {
                                                              srTempDoc = srTempDocs;
                                                               system.debug('srTempDocs==>'+srTempDocs);
                                                          }
            system.debug('srTempDoc==>'+srTempDoc);
             for(HexaBPM__SR_Doc__c srDoc : [SELECT id,File_Name__c,
                                            HexaBPM__Document_Master__r.HexaBPM__Code__c 
                                            FROM HexaBPM__SR_Doc__c
                                            WHERE HexaBPM__Service_Request__c = :srId 
                                            AND HexaBPM__Document_Master__r.HexaBPM__Code__c =:docMasterCode LIMIT 1]){
                                                existingSRDoc.add(srDoc);
                                            }
            if(existingSRDoc != null && existingSRDoc.size() > 0){
                srDocToUpdate.addAll(existingSRDoc);
            } else {
                srDocsObj.Name = 'Structure Diagram';//srTempDoc.HexaBPM__Document_Master__r.Name;
                srDocsObj.HexaBPM__Service_Request__c = srId;
                srDocsObj.HexaBPM__Document_Master__c = srTempDoc.HexaBPM__Document_Master__c;
                srDocsObj.HexaBPM__SR_Template_Doc__c = srTempDoc.Id;
                srDocsObj.HexaBPM__From_Finalize__c = true;
                srDocToUpdate.add(srDocsObj);
            }
             system.debug('srDocToUpdate==>'+srDocToUpdate);
            
            if(srDocToUpdate != NULL && srDocToUpdate.size() > 0) {
                upsert srDocToUpdate;
            }
            
            system.debug('srDocToUpdate2==>'+srDocToUpdate);
            List<ContentDocumentLink> conDocLnkToInsert = new List<ContentDocumentLink>();
            
            //reparenting
            for(HexaBPM__SR_Doc__c srDoc :srDocToUpdate) {
                ContentDocumentLink cDe = new ContentDocumentLink();
                cDe.ContentDocumentId = contentDocumentId;
                cDe.LinkedEntityId = srDoc.Id; // you can use objectId,GroupId etc
                cDe.ShareType = 'V'; // Viewer permission, checkout description of ContentDocumentLink object for more details
                cDe.Visibility = 'AllUsers';
                conDocLnkToInsert.add(cDe);
            }
            
            if(conDocLnkToInsert.size() > 0) {
                insert conDocLnkToInsert;
            }

             
            return conDocLnkToInsert[0].Id;
        } catch(exception exp){
            return exp.getMessage();
        }
        
    }
    
    public class ButtonResponseWrapper {
        @AuraEnabled public String pageActionName { get; set; }
        @AuraEnabled public string communityName { get; set; }
        @AuraEnabled public String errorMessage { get; set; }
        @AuraEnabled public string CommunityPageName { get; set; }
        @AuraEnabled public string sitePageName { get; set; }
        @AuraEnabled public string strBaseUrl { get; set; }
        @AuraEnabled public string srId { get; set; }
        @AuraEnabled public string flowId { get; set; }
        @AuraEnabled public string pageId { get; set; }
        @AuraEnabled public boolean isPublicSite { get; set; }
        @AuraEnabled public boolean tester { get; set; }
        
    }
}