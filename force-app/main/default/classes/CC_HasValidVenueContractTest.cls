@istest
public class CC_HasValidVenueContractTest {

    
public static testMethod void CC_hasActiveCommercialPermit() {
    
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
      
        
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
       insert objsrTemp;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
   
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
        
        OB_Event_Contract__c objev = new OB_Event_Contract__c ();
       objev.Valid_To__c = system.today()+1;
       objev.Account__c = objHexaSR.HexaBPM__Customer__c;
       insert objev;
        
       
        
        Test.startTest();
         HexaBPM__Step__c step = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c ,HexaBPM__SR__r.Type_of_commercial_permission__c                               
                                  from HexaBPM__Step__c where Id=:objHexastep.Id];
        
     CC_HasValidVenueContract tester = new CC_HasValidVenueContract();
	tester.EvaluateCustomCode(objHexaSR,step);  
     objHexaSR.Type_of_commercial_permission__c ='ATM';
        update objHexaSR;
    
     HexaBPM__Step__c step2 = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c ,HexaBPM__SR__r.Type_of_commercial_permission__c                               
                                  from HexaBPM__Step__c where Id=:objHexastep.Id];
    
    tester.EvaluateCustomCode(objHexaSR,step2); 
        Test.stopTest();
       
    
            
          
       
    }
    
}