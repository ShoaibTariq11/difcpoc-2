/*
    Author      : Durga Prasad
    Date        : 13-Mar-2020
    Description : Helper class to Query HexaBPM related Applications
    ---------------------------------------------------------------------------------------------------------------------------
*/
public without sharing class OB_RequestedServicesControllerHelper {
    public static list<HexaBPM__Service_Request__c> ApplicationQueryResult(string strQuery){
    	return database.query(strQuery);
    }
}