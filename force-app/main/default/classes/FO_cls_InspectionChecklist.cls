/******************************************************************************************
 *  Author   : Swati Sehrawat
 *  Company  : NSI JLT
 *  Date     : 05-Jan-2016
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    01/05/2016	Swati		  Created
V1.1	03/29/2016  Claude		  Revised logic for getting contractor details
V1.2    04/05/2016  Claude		  Moved SR Doc generation to CC_cls_FitOutandEventCustomCode; Revised Question List query
V1.3	05/23/2016	Claude		  Added logic to prevent clients to edit the checklist
V1.4	09/06/2016	Claude		  Added logic to get checklist information
V1.5	16/08/2016	Claude		  Added datetime formatting logic
****************************************************************************************************************/
//Class for HSE Precautions in DIFC
public without sharing class FO_cls_InspectionChecklist {

    public Service_Request__c srObj 						{get;set;}
    
    public Fit_Out_Inspection__c foObj						{get;set;}
    
    public list<questionWrapper> questionsList 				{get;set;}
    public list<Fit_Out_Inspection__c> inspectionList 		{get;set;}
    
    public string checkListType 							{get;set;}
    public string InspectionType 							{get;set;}
    public string serviceRequestId 							{get;set;}
    public string typeOfSR 									{get;set;}
    public string consultant 								{get;set;}
    public string version 									{get;set;}
    public string showresult 								{get;set;}
    public string currentVersion;
    public string contractor 								{get;set;}
    public String formattedDate								{get;set;} // V1.5 - Claude - Added String variable to display formatted date
    
    public boolean iscontractor 							{get;set;}
    public boolean inspectionDone 							{get;set;}
    public boolean doneByIdama 								{get;set;}
    public boolean doneByContractor 						{get;set;}
    public boolean selectAllCon 							{get;set;}
    public boolean selectAllYes 							{get;set;}
    public boolean selectAllNo 								{get;set;}
    public boolean selectAllNa 								{get;set;}
    public boolean isDraft									{get;set;}
    
    public String qualityCompany							{get;set;}
    
    //V1.3
    public Boolean isClient								    {get;set;}
    
    public String sectionName {get; set;}
    public String fieldName {get; set;}
    public String value {get; set;}
    
    //V1.4 - Claude
    public String refNum											{get;set;}
    public String updatedDate										{get;set;}
    //public String checkListHeader									{get;set;}
    
    /**
     * Initializes the variables
     */
    private void init(){
    	
    	foObj = new Fit_Out_Inspection__c();			// initialize inspection object
        foObj.Inspection_Date__c = System.now();		// set the date to NOW for new inspections
    	
        questionsList = new list<questionWrapper>();	// initialize the wrapper
        
		//V1.4 - Claude - initialized info strings
    	refNum = '';
    	updatedDate = '';
    	//checkListHeader = '';       

        contractor = '';								// initialize the contractor name list
        currentVersion = '';							// initialize the version 
        
        initializeSR();									// initialize the SR
        initializeCheckList();							// initialize checklist to get questions
        setChecklistInfo();
    }
    
    /**
     * Sets the checklist informatiom
     * @author		Claude Manahan. NSI-DMCC
     * @date		06/09/2016
     */
    void setChecklistInfo(){
    	
    	Checklist_Information__c checkListInfo = getChecklistInformation(checkListType,typeOfSr);
    	
    	if(checkListInfo != null){ 
    		refNum = checkListInfo.Reference_Number__c;
    		updatedDate = checkListInfo.Update_Date__c.format();
    		InspectionType = checkListInfo.Checklist_Header__c;
    	}
    }
    
    public void setValue(){
        
        if(sectionName.equals('Air Quality') && fieldName.equals('Tester')){
            foObj.Quality_Company__c = String.valueOf(value);
        }
        
        if(sectionName.equals('Air Quality') && fieldName.equals('Company')){
            foObj.Quality_Name__c = String.valueOf(value);
        }
        
        if(sectionName.equals('Air Quality') && fieldName.equals('Mobile')){
            foObj.Quality_Mobile__c = String.valueOf(value);
        }
        
        if(sectionName.equals('ILOTO') && fieldName.equals('Tester')){
            foObj.ILOTO_Company__c = String.valueOf(value);
        }
        
        if(sectionName.equals('ILOTO') && fieldName.equals('Company')){
            foObj.ILOTO_Name__c = String.valueOf(value);
        }
        
        if(sectionName.equals('ILOTO') && fieldName.equals('Mobile')){
            foObj.ILOTO_Mobile__c = String.valueOf(value);
        }
        
    }
    
    /**
     * Checks if the form is viewing the results 
     */
    private Boolean isResult(){
    	return string.isNotBlank(ShowResult) && ShowResult.contains('true');
    }
    
    /**
     * Sets the checklist values
     */
    private void setChecklist(){
    	
    	inspectionList = new list<Fit_Out_Inspection__c>();
        inspectionList = relatedInspectionList(serviceRequestId,currentVersion);
        initializeInspectionList(inspectionList);
    }
    
    /**
     * Class Constructor
     */
    public FO_cls_InspectionChecklist(){
   		
   		try{
   			
   			init();
   			
   			//V1.3 - Start - Claude - Added check for User role
            if(string.isNotBlank(InspectionType)){
                checkUserRoles();
            }
            //V1.3 - End - Claude
            
            /* Check if the checklist is for showing results */
            ShowResult = ApexPages.CurrentPage().GetParameters().Get('ShowResult');
            
            /* Get the curren version */
            currentVersion = ApexPages.CurrentPage().GetParameters().Get('version');
            
            /* Set the checklist */
            if(isResult()){
               setChecklist();
            }
            
            doneBycontractor = srObj.External_Status_Name__c.equals('Draft') ? false : doneBycontractor;
            
        } catch(exception ex){
			system.debug('---line---'+ex.getLineNumber() + '; The error: ' + ex.getMessage());
        }
    }
    
    /**
     * Checks if the User Role of the current User
     * V1.3 - Claude
     */
    private void checkUserRoles(){
    	
    	List<User> currentUser = [SELECT Contact.Role__c FROM User WHERE ID = :UserInfo.getUserId()];
           
        isClient = !currentUser.isEmpty() && currentUser[0].Contact != null &&
        	currentUser[0].Contact.Role__c != null &&
        	!currentUser[0].Contact.Role__c.contains('Fit-Out Services') && 
    		!currentUser[0].Contact.Role__c.contains('Event Services') &&
    		currentUser[0].Contact.Role__c.contains('Company Services'); 
        
        isContractor = currentUser!=null && 
        			!currentUser.isEmpty() && 
        			currentUser[0].Contact != null && 
        			currentUser[0].Contact.Role__c != null &&
        			(currentUser[0].contact.role__c == 'Fit-Out Services' || currentUser[0].contact.role__c == 'Event Services');
    }
    
    /**
     * Submits the checklist
     */
    public pageReference submitInspection(){
        
        Savepoint sp = Database.setSavepoint();
        
        try{
        	
            pageReference redirectPage = null;
            
            /* Set the checklist version */
            if(currentVersion==null || String.isBlank(currentVersion)){
                
                if(srObj.Business_Name__c!=null){
                    srObj.Business_Name__c = 'HSE'+(srObj.No_of_Shares__c+1);
                    srObj.No_of_Shares__c = srObj.No_of_Shares__c+1;
                    version = srObj.Business_Name__c;
                }
                else{
                    srObj.Business_Name__c = 'HSE1';
                    srObj.No_of_Shares__c = 1;
                    version = 'HSE1';
                }
            }
            
            list<Fit_Out_Inspection__c> quesToInsert = new list<Fit_Out_Inspection__c>();
            
            if(!questionsList.isEmpty()){
                
                for(questionWrapper tempObj : questionsList){
                    Fit_Out_Inspection__c fitOutObj = new Fit_Out_Inspection__c();
                    
                    if(tempObj.recordid!=null){
						fitOutObj.id = tempObj.recordid;
                    } else{
                        fitOutObj.Service_Request__c = srObj.id;
                    }
                    
                    fitOutObj.description__c = tempObj.lookUpObj.description__c;	
                    
                    if(tempObj.lookUpObj!=null){
                        fitOutObj.Lookup__c = tempObj.lookUpObj.id;
                        fitOutObj.name = tempObj.lookUpObj.name;
                    }
                    
                    fitOutObj.Comments__c = tempObj.comments;
                    
                    if(tempObj.lookUpObj == null && tempObj.description!=''){
                        fitOutObj.description__c = tempObj.description;
                        fitOutObj.name = tempObj.name;
                    }
                    
                    if(tempObj.yes == true){
                        fitOutObj.Options__c = 'Yes';
                        fitOutObj.done_by_IDAMA__c = true;
                        donebyidama = true;
                    }
                    
                    if(tempObj.no == true){
                        fitOutObj.Options__c = 'No';
                        fitOutObj.done_by_IDAMA__c = true;
                        donebyidama = true;
                    }
                    
                    if(tempObj.na == true){
                        fitOutObj.Options__c = 'N/A';
                        fitOutObj.done_by_IDAMA__c = true;
                        donebyidama = true;
                    }
                    
                    if(tempObj.contractorCheckList!=null){
                        fitOutObj.Contractor_Checklist__c = tempObj.contractorCheckList;
                        fitOutObj.done_by_contractor__c = true;
                        donebycontractor = true;
                    }
                    
                    if(tempObj.other!=''){
                        fitOutObj.other__c = tempObj.other;
                    }
                    
                    System.debug('The other value: ' + tempObj.other);
                    
                    if(foObj!=null){
                    	
                        fitOutObj.version__c = srObj.Business_Name__c;
                        //fitOutObj.Inspection_Date__c = foObj.Inspection_Date__c;
                        
                        if(checkListType.contains('Isolation of Life Safety System') 
                        	&& (String.isBlank(tempObj.other) || 
                        		(String.isNotBlank(tempObj.other) 
	                        		&& !tempObj.other.equals('Other PPE HSE') 
	                        		&& !tempObj.other.equals('Other Pre HSE') 
	                        		&& !tempObj.other.equals('Other Post HSE')
                        			)
                        		)
                        	){
                            fitOutObj.description__c = foObj.description__c;
                            fitOutObj.Identification__c = foObj.Identification__c;
                        }
                        
                        if(String.isNotBlank(tempObj.other) ){
                        	 System.debug('@@ ' + tempObj.other);
                        } else {
                        	System.debug('No Other field value detected.');
                        }
                        
                        if(checkListType.contains('Confined Space Entry')){
                            fitOutObj.quality_Company__c = foObj.quality_Company__c;
                            fitOutObj.quality_Name__c = foObj.quality_Name__c;
                            fitOutObj.quality_Mobile__c = foObj.quality_Mobile__c;
                            fitOutObj.ILOTO_Company__c = foObj.ILOTO_Company__c;
                            fitOutObj.ILOTO_Mobile__c = foObj.ILOTO_Mobile__c;
                            fitOutObj.ILOTO_Name__c = foObj.ILOTO_Name__c;
                        }
                        
                    } 
                    
                    quesToInsert.add(fitOutObj);
                }
                
                if(!quesToInsert.isEmpty() && quesToInsert!=null){
                	
                	// Set the timezone correctly
                	Datetime localDateTime = System.now();
                	Datetime formattedDateTime = localDateTime.date();
                	
                	formattedDateTime = formattedDateTime.addHours(localDateTime.hour());
                	formattedDateTime = formattedDateTime.addMinutes(localDateTime.minute());
                	formattedDateTime = formattedDateTime.addSeconds(localDateTime.second());
                	
                	System.debug('Formatted Time: ' + formattedDateTime);
                	
                	for(Fit_Out_Inspection__c fo : quesToInsert){
                		fo.Inspection_Date__c  = formattedDateTime;
                	}
                	
                    upsert quesToInsert;
                    update srObj;
                }
                
            }
            
            redirectPage = new PageReference(Site.getPathPrefix()+'/'+srObj.id);
            redirectPage.setRedirect(true);
            return redirectPage;
            
        } catch(Exception ex){
        	
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,ex.getMessage()));
            system.debug('---ex------'+ex.getLineNumber());
            Database.rollback(sp);
            return null;
        } 
        
        return null;
    } 
    
    /**
     * Redirects the page to the SR
     */
    public pageReference cancelInspection(){
        
        try{
            pageReference redirectPage = null;
            redirectPage = new pagereference('/'+srObj.id);
            redirectPage.setRedirect(true);
            return redirectPage;
        } catch(Exception ex){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,ex.getMessage()));
            return null;
        } 
        return null;
    }
    
    /**
     * Initialize the SR Details
     */
    public void initializeSR(){
        
        serviceRequestId = Apexpages.currentPage().getParameters().get('srId');
        
        if(string.isNotBlank(serviceRequestId)){
            srObj = relatedServiceRequest(serviceRequestId);
            consultant = relatedConsultant(serviceRequestId);
        } else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Service Request Missing'));
        }
    }
    
    /**
     * Initializes the checklist
     */
    public void initializeCheckList(){
        
        checkListType = Apexpages.currentPage().getParameters().get('CLType');
        
        if(string.isNotBlank(checkListType)){
            questionsList = relatedQuestionList(checkListType);
            //InspectionType = relatedInspectionType(checkListType);
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'CheckList Type is Missing'));
        }
    }
    
    /**
     * Get the details of the related service request
     * @param		serviceRequestId		the id of the service request
     * @return		relatedServiceRequest	the related service request
     */
    public service_request__c relatedServiceRequest(string serviceRequestId){
        
        List<Service_Request__c> tempSrList = [select id,External_Status_Name__c,linked_sr__r.linked_sr__r.linked_sr__r.sponsor_first_name__c,
                                                      linked_sr__r.linked_sr__r.linked_sr__r.sponsor_last_name__c,
                                                      linked_sr__r.linked_sr__r.linked_sr__r.first_name__c,
                                                      linked_sr__r.linked_sr__r.linked_sr__r.last_name__c,
                                                      first_name__c,last_name__c,Event_Name__c ,linked_sr__r.location__c,Customer__r.name ,Customer__r.RORP_License_No__c,
                                                      License_Number__c,linked_sr__r.Event_Name__c ,Company_Name__c,linked_sr__r.record_type_name__c,
                                                      Date__c,Position__c ,Business_Name__c,No_of_Shares__c,contractor__r.name,
                                                      type_of_request__c,linked_sr__r.contractor__r.name,linked_sr__r.type_of_request__c,
                                                      sponsor_first_name__c,
                                                      sponsor_last_name__c,
                                                      Current_Registered_Name__c
                                               from Service_Request__c where id=:serviceRequestId];
        if(!tempSrList.isEmpty()){
            
            srObj = tempSrList[0];
            string firstname= '';
            string lastname='';
            isDraft = false;
            isDraft = srObj.External_Status_Name__c.equals('Draft');
            
            //isIdamaApproved = srObj.External_Status_Name__c.equals('Approved');
            /*V1.1 - Start // Chaged fields to refer directly to contractor fields
            if(string.isNotBlank(srObj.linked_sr__r.linked_sr__r.linked_sr__r.sponsor_first_name__c)){
                firstname = srObj.linked_sr__r.linked_sr__r.linked_sr__r.sponsor_first_name__c;
            }
            if(string.isNotBlank(srObj.linked_sr__r.linked_sr__r.linked_sr__r.sponsor_last_name__c)){
                lastname = srObj.linked_sr__r.linked_sr__r.linked_sr__r.sponsor_last_name__c;
            }
            if(string.isNotBlank(srObj.linked_sr__r.linked_sr__r.linked_sr__r.first_name__c)){
                firstname = srObj.linked_sr__r.linked_sr__r.linked_sr__r.sponsor_first_name__c;
            }
            if(string.isNotBlank(srObj.linked_sr__r.linked_sr__r.linked_sr__r.last_name__c)){
                lastname = srObj.linked_sr__r.linked_sr__r.linked_sr__r.sponsor_last_name__c;
            }*/
            
            /*if(string.isNotBlank(srObj.sponsor_first_name__c)){
                firstname = srObj.sponsor_first_name__c;
            }
            if(string.isNotBlank(srObj.sponsor_last_name__c)){
                lastname = srObj.sponsor_last_name__c;
            }
            if(string.isNotBlank(srObj.first_name__c)){
                firstname = srObj.sponsor_first_name__c;
            }
            if(string.isNotBlank(srObj.last_name__c)){
                lastname = srObj.sponsor_last_name__c;
            }*/
            
            //V1.1 - End
            
            contractor = srObj.contractor__r.name;
            if(srObj.linked_sr__r.record_type_name__c!=null && srObj.linked_sr__r.record_type_name__c=='Fit_Out_Service_Request'){
                typeOfSr = 'fitOut';    
            }
            if(srObj.linked_sr__r.record_type_name__c!=null && srObj.linked_sr__r.record_type_name__c=='Event_Service_Request'){
                typeOfSr = 'Event'; 
            }
            
            return srObj;
        }
        
        return null;
    }
    
    /**  
     * Get the consultant of the service request
     * @param		serviceRequestId		the service request ID
     * @return		consultant				the name of the consultant
     */
    public string relatedConsultant(string serviceRequestId){
        
        list<Amendment__c> tempAmendList = [select id, Name_DDP__c from Amendment__c where Record_Type_Name__c=:'Fit_Out_Contact' and Amendment_Type__c=:'Consultant/Project Manager' and ServiceRequest__c=:serviceRequestId]; 
        
        if(!tempAmendList.isEmpty()){
            consultant = tempAmendList[0].name_DDP__c;
            return consultant;
        }
        
        return null;
    }
    
    /**
     * Initialize the related questions for the checklist to be displayed
     */
    public list<questionWrapper> relatedQuestionList(string checkListType){
        
        list<questionWrapper> questionsList = new list<questionWrapper>();
        
        for(lookup__c a : [select id,name,Description__c ,Check_List_classification__c,Check_List_Type__c from lookup__c 
                           where Check_List_Type__c =: checkListType]){
            questionsList.add(new questionWrapper(a));
        }
        
        questionWrapper otherPPE = new questionWrapper(null); 
        otherPPE.other = 'Other PPE HSE';   
        otherPPE.name = 'Other PPE HSE';
        questionsList.add(otherPPE);
         
        questionWrapper otherPRE = new questionWrapper(null); 
        otherPRE.other = 'Other Pre HSE';   
        otherPRE.name = 'Other Pre HSE';
        questionsList.add(otherPRE);
        
        questionWrapper otherPOST = new questionWrapper(null); 
        otherPOST.other = 'Other Post HSE'; 
        otherPOST.name = 'Other Post HSE';
        questionsList.add(otherPOST);
        
        questionWrapper otherCLR = new questionWrapper(null); 
        otherCLR.other = 'Other clearance HSE'; 
        otherCLR.name = 'Other clearance HSE';
        questionsList.add(otherCLR);
        
        system.debug('---questionsList----'+questionsList);
        system.debug('---questionsList---- SIZE: '+questionsList.size());
        
        return questionsList;
    }
    
    /**
     * Get the inspection checklist items 
     * @param		serviceRequestId	the related service request ID
     * @param		currentVersion		the version of the checklist
     * @return 		tempList			list of fit-out inspection items
     */
    public list<Fit_Out_Inspection__c> relatedInspectionList(string serviceRequestId,string currentVersion){
        
        System.debug('@@System Current Date: ' + DateTime.now());
        System.debug('@@Former Current Date: ' + foObj.Inspection_Date__c);
        
        list<Fit_Out_Inspection__c> tempList;
        tempList = new list<Fit_Out_Inspection__c>();
        
       /*
       Changed by ternary operator 
       if(isContractor==true){
            tempList = [select id, other__c,service_request__c, name, lookup__c, lookup__r.description__c, options__c, contractor_checklist__c ,
                                   lookup__r.Check_List_Type__c, lookup__r.Check_List_classification__c, Comments__c,
                                   description__c, done_by_idama__c, done_By_contractor__c, Client_Name__c, Client_Position__c,
                                   Contractor_Name__c,IDAMA_Comments__c,IDAMA_FO_Civil__c,IDAMA_FO_Electrical__c,
                                   IDAMA_FO_Mechanical__c,IDAMA_FO_Name__c,IDAMA_FO_Position__c,IDAMA_HSE_Name__c,IDAMA_HSE_Position__c,
                                   Inspection_Status__c,Event_Premise_Status__c,Inspection_Date__c,Identification__c,
                                   quality_Company__c,quality_Name__c,quality_Mobile__c,ILOTO_Company__c,ILOTO_Mobile__c,ILOTO_Name__c
                        from Fit_Out_Inspection__c 
                        where service_request__c =: serviceRequestId and version__c =: currentVersion];
        } else{
            
            //V1.2 - Claude - Replaced with different External Status
            // tempList = [select id, other__c,service_request__c, name, lookup__c, options__c, contractor_checklist__c ,
                                   // lookup__r.Check_List_Type__c, lookup__r.Check_List_classification__c, Comments__c,
                                   // description__c, done_by_idama__c, done_By_contractor__c, Client_Name__c, Client_Position__c,
                                   // Contractor_Name__c,IDAMA_Comments__c,IDAMA_FO_Civil__c,IDAMA_FO_Electrical__c,
                                   // IDAMA_FO_Mechanical__c,IDAMA_FO_Name__c,IDAMA_FO_Position__c,IDAMA_HSE_Name__c,IDAMA_HSE_Position__c,
                                   // Inspection_Status__c,Event_Premise_Status__c,Inspection_Date__c,Identification__c,
                                   // quality_Company__c,quality_Name__c,quality_Mobile__c,ILOTO_Company__c,ILOTO_Mobile__c,ILOTO_Name__c
                        // from Fit_Out_Inspection__c 
                        // where service_request__c =: serviceRequestId and version__c =: currentVersion
                        // and (service_request__r.External_Status_Name__c =: 'Submitted' OR service_request__r.External_Status_Name__c =: 'Under Verification')];
            //V1.2 - Claude - end of Query revision
            
            tempList = [select id, other__c,service_request__c, name, lookup__c, lookup__r.description__c, options__c, contractor_checklist__c ,
                                   lookup__r.Check_List_Type__c, lookup__r.Check_List_classification__c, Comments__c,
                                   description__c, done_by_idama__c, done_By_contractor__c, Client_Name__c, Client_Position__c,
                                   Contractor_Name__c,IDAMA_Comments__c,IDAMA_FO_Civil__c,IDAMA_FO_Electrical__c,
                                   IDAMA_FO_Mechanical__c,IDAMA_FO_Name__c,IDAMA_FO_Position__c,IDAMA_HSE_Name__c,IDAMA_HSE_Position__c,
                                   Inspection_Status__c,Event_Premise_Status__c,Inspection_Date__c,Identification__c,
                                   quality_Company__c,quality_Name__c,quality_Mobile__c,ILOTO_Company__c,ILOTO_Mobile__c,ILOTO_Name__c
                        from Fit_Out_Inspection__c 
                        where service_request__c =: serviceRequestId and version__c =: currentVersion
                        and service_request__r.External_Status_Name__c !=: 'Draft'];
        }*/
        
        // Build the query string
        String fitoutChecklistQuery = 'select id, other__c,service_request__c, name, lookup__c, lookup__r.description__c, options__c, contractor_checklist__c ,'
                                   	  +'lookup__r.Check_List_Type__c, lookup__r.Check_List_classification__c, Comments__c,'
                                   	  +'description__c, done_by_idama__c, done_By_contractor__c, Client_Name__c, Client_Position__c,'
                                   	  +'Contractor_Name__c,IDAMA_Comments__c,IDAMA_FO_Civil__c,IDAMA_FO_Electrical__c,'
                                   	  +'IDAMA_FO_Mechanical__c,IDAMA_FO_Name__c,IDAMA_FO_Position__c,IDAMA_HSE_Name__c,IDAMA_HSE_Position__c,'
                                   	  +'Inspection_Status__c,Event_Premise_Status__c,Inspection_Date__c,Identification__c,'
                                   	  +'quality_Company__c,quality_Name__c,quality_Mobile__c,ILOTO_Company__c,ILOTO_Mobile__c,ILOTO_Name__c '
                        			  +'from Fit_Out_Inspection__c WHERE service_request__c = \''+serviceRequestId+'\' and version__c = \''+currentVersion+'\'';

		// Add any condition
		String additionalCondition = !isContractor ? ' AND service_request__r.External_Status_Name__c != \'Draft\' ' : '';
		fitoutChecklistQuery += additionalCondition;               			  
        
        // Query the items
        tempList = Database.query(fitoutChecklistQuery);
        
        if(!tempList.isEmpty()){
            
            if(checkListType.contains('Isolation of Life Safety System')){
                foObj.description__c = tempList[0].description__c;
                foObj.Identification__c = tempList[0].Identification__c;
            }
            
            if(checkListType.contains('Confined Space Entry')){
                foObj.quality_Company__c = tempList[0].quality_Company__c;
                foObj.quality_Name__c = tempList[0].quality_Name__c;
                foObj.quality_Mobile__c = tempList[0].quality_Mobile__c;
                foObj.ILOTO_Company__c = tempList[0].ILOTO_Company__c;
                foObj.ILOTO_Mobile__c = tempList[0].ILOTO_Mobile__c;
                foObj.ILOTO_Name__c = tempList[0].ILOTO_Name__c;
            }
            
            foObj.Inspection_Date__c = tempList[0].Inspection_Date__c == null ? foOBj.Inspection_Date__c : tempList[0].Inspection_Date__c;
            formattedDate = foObj.Inspection_Date__c.format('dd/MM/YYYY hh:mm a','Asia/Dubai'); //V1.6 - Claude - Added formatting logic for inspection date
        }
        
        System.debug('@@Current Date: ' + foObj.Inspection_Date__c);
        System.debug('@@@The collection size: ' + tempList.size());
        
        return tempList;
    }
    
    /**
     * Initializes the inspection checklist items
     * @param		inspectionList		the list of fit-out checklist items
     */
    public void initializeInspectionList(list<fit_out_inspection__c> inspectionList){
    	
    	System.debug('The list is empty? ' + inspectionList.isEMpty());
    	System.debug('The lsie ' + inspectionList.size());
        
        questionsList = new list<questionWrapper>();			// initialize the wrapper list
        
        if(inspectionList!=null && !inspectionList.isEmpty()){
        	
        	System.debug('Creating Wrapper questions: ');
        	
            for(Fit_Out_Inspection__c obj : inspectionList){
            	
            	doneByIdama = obj.done_by_idama__c == true ? obj.done_by_idama__c : doneByIdama;
            	doneBycontractor = obj.done_by_contractor__c == true ? obj.done_by_contractor__c : doneByContractor;
            	
                /*
                26-05-2016 - replaced by assignment - Claude
                if(obj.done_by_idama__c == true){
                    doneByIdama = true;
                }
                if(obj.done_by_contractor__c == true){
                    doneBycontractor = true;
                }*/
                
                /* Initialize the checklist items */
                questionWrapper tempObj = new questionWrapper(null); 
                tempObj = relatedQuestion(obj);
                
                System.debug('The Temp Obj >>' + tempObj);
                
                questionsList.add(tempObj);
            }
        }
        
        // Check if the inspection is completed
        inspectionDone = doneByIdama==true && doneBycontractor == true;
    }
    
    /**
     * Set the related question
     * @param	obj			the fit-out object inspection instance
     * @return	tempObj		a question wrapper instance
     */
    public questionWrapper relatedQuestion(Fit_Out_Inspection__c obj){
        
        questionWrapper tempObj = new questionWrapper(null); 
        
        if(obj.lookup__c!=null){
            tempObj.lookupObj = new lookup__c();
            tempObj.lookupObj.id = obj.lookup__c;
            tempObj.lookupObj.Check_List_classification__c = obj.lookup__r.Check_List_classification__c;
            tempObj.lookupObj.Check_List_Type__c = obj.lookup__r.Check_List_Type__c;
            tempObj.lookupObj.description__c = obj.lookup__r.description__c;
        }
        
        tempObj.comments = obj.Comments__c;
        tempObj.name = obj.Name;
        tempObj.description = obj.description__c;
        
        if(obj.Options__c == 'Yes'){
            tempObj.yes = true;
        }
        if(obj.Options__c == 'No'){
            tempObj.no = true;
        }
        if(obj.Options__c == 'N/A'){
            tempObj.na = true;
        }
        if(obj.other__c != ''){
            tempObj.other = obj.other__c;
        }
        if(obj.contractor_checklist__c!=null){
            tempObj.ContractorChecklist = obj.contractor_checklist__c;
        }
        
        tempObj.recordId = obj.id;
        tempObj.doneByIdama = obj.done_by_Idama__c;
        tempObj.doneBycontractor = obj.done_by_contractor__c;
        
        return tempObj;
    }
    
    /**
     * Gets the checklist information 
     * @author		Claude Manahan. NSI-DMCC
     * @date		09/06/2016
     */
    public static Checklist_Information__c getChecklistInformation(String keyword){
        return getChecklistInformation(keyword,null);
    }
    
    /**
     * Gets the checklist information 
     * @author		Claude Manahan. NSI-DMCC
     * @date		09/06/2016
     */
    public static Checklist_Information__c getChecklistInformation(String keyword, String checklistType){
        
        Map<String,Checklist_Information__c> checklistInfoMap 
    		= Checklist_Information__c.getAll(); // get all the checklist values
    		
    	if(checklistInfoMap.containsKey(keyword)){
    	    // Get the details based on name
    	    return checklistInfoMap.get(keyword);
    	} else {
    	    
    	    if(checkListType == null){
    	        checkListType = '';
    	    }
    	    
    	    Map<String,List<Checklist_Information__c>> checklistInfoTypeMap = new Map<String,List<Checklist_Information__c>>();
    	    
    	    /* Create a new map which uses the keyword */
    	    for(Checklist_Information__c c : checklistInfoMap.values()){
    	        
    	        List<Checklist_Information__c> checkListValues = checklistInfoTypeMap.get(c.Checklist_Keyword__c) == null ? 
    	            new List<Checklist_Information__c>() : checklistInfoTypeMap.get(c.Checklist_Keyword__c);
    	        
    	        checkListValues.add(c);
    	        
    	        checklistInfoTypeMap.put(c.Checklist_Keyword__c,checkListValues);
    	        
    	    }
    	    
    	    System.debug('Checklist info map >> ' +  checklistInfoTypeMap);
    	    System.debug('Keyword inside map >> ' +  checklistInfoTypeMap.containsKey(keyWord));
    	    
    	    if(checklistInfoTypeMap.containsKey(keyword)){
    	        
    	        /* Start a search and if there's any, return the settings */
    	        for(Checklist_Information__c c : checklistInfoTypeMap.get(keyWord)){
    	            
    	            if(c.Type__c == checkListType || c.type__c.contains(checkListType)){
    	                return c;
    	            }
    	        }
    	        
    	    } 
    	    
    	}
    	
    	return null;
        
    }
    
    /* Wrapper class for the inspection items */
    public class questionWrapper{
       
        public lookup__c lookUpObj			{get;set;}
        public boolean yes 					{get;set;}
        public boolean no 					{get;set;}
        public boolean na 					{get;set;}
        public boolean contractorCheckList 	{get;set;}
        public boolean doneByContractor 	{get;set;}
        public boolean doneByIdama 			{get;set;}
        public string comments 				{get;set;}
        public string other 				{get;set;}
        public string name 					{get;set;}
        public string description 			{get;set;}
        public id recordId 					{get;set;}
        public String qualityCompany		{get;set;}
            
        public questionWrapper(lookup__c a){
        	
            lookUpObj = a;
            yes = false;
            no = false;
            na = false;
            doneByContractor = false;
            doneByIdama = false;
            contractorCheckList = false;
            comments = '';
            name = '';
            description = '';
            other = '';
            recordId = null;
        }
    }  
}