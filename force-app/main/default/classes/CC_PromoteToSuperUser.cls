/*
    Author      : Durga Prasad
    Date        : 13-Mar-2020
    Description : Custom code to promote normal user to super user
    ---------------------------------------------------------------------------------------------------------------------------
*/
global without sharing class CC_PromoteToSuperUser implements HexaBPM.iCustomCodeExecutable{
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        if(stp!=null && stp.HexaBPM__SR__c!=null && stp.HexaBPM__SR__r.HexaBPM__Contact__c!=null && stp.HexaBPM__SR__r.HexaBPM__Customer__c!=null){
            try{
                AccountContactRelation objAccContactRel = new AccountContactRelation();
                for(AccountContactRelation objRel:[Select Id,Roles from AccountContactRelation where AccountId=:stp.HexaBPM__SR__r.HexaBPM__Customer__c and ContactId=:stp.HexaBPM__SR__r.HexaBPM__Contact__c and IsActive=true]){
                    objAccContactRel = objRel;
                }        
                objAccContactRel.Roles= stp.HexaBPM__SR__r.Community_User_Roles__c;             
                if(objAccContactRel.Id!=null)
                    update objAccContactRel;
            }catch(Exception e){
                strResult = e.getMessage()+'';
            }
        }
        return strResult;
    }

}