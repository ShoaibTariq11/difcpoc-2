/******************************************************************************************
 *  Author   : Shoaib Tariq
 *  Company  : 
 *  Date     : 15/12/2019
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0     15/12/2019  shoaib         Created
**********************************************************************************************************************/
public class InspectionTriggerHandler {
    
    /*
     * Description : Custom validation to the inspection record if it doesn't meet the following conditions
     */
    public static void validateInspectionRecord(List<Inspection__c> currentInsectionList,Map<Id,Inspection__c> oldmap) {
        for (Inspection__c thisInspection: currentInsectionList) {
                if(thisInspection.Status__c == 'Submitted for Approval' 
                         && oldMap.get(thisInspection.Id).Status__c == 'Awaiting for Client Response'){
                             
                    thisInspection.addError('You cannot submit the record when the status is "Awaiting for Client Response"');
                }
                if(thisInspection.Status__c == 'Submitted for Approval' 
                        && (thisInspection.NonRegulated_Q24__c=='Yes' &&  oldMap.get(thisInspection.Id).Status__c != 'Information Updated')){
                            
                    thisInspection.addError('You cannot submit the record when the  the entity is in "good standing"');
                }
               if(thisInspection.Status__c == 'Submitted'){
                   
                    thisInspection.addError('This record cannot be submitted.Please check the status of the record');
                }
              if(thisInspection.Status__c == 'Submitted for Approval' 
                             && oldMap.get(thisInspection.Id).Status__c == 'Completed'){
                   
                    thisInspection.addError('This record cannot be submitted.Please check the status of the record');
                }
            }
    }
    
     /*
     * Description : Update the status the Completed if the status is Approved.
     */
    @future
    public static void updateStatusToApproved(Set<Id> inspectionIds) {
        List<Inspection__c> thisInspectionList = new List<Inspection__c>();
            for(Inspection__c thisInspection :[SELECT Id,Status__c FROM Inspection__c WHERE Id IN:inspectionIds]){
                if(thisInspection.Status__c == 'Approved'){
                    thisInspection.Status__c ='Completed';
                    system.debug('Approved');
                }
                
                thisInspectionList.add(thisInspection);
                system.debug('Approved');
            }
        if(!thisInspectionList.isEmpty()){
            update thisInspectionList;
        }
    }
}