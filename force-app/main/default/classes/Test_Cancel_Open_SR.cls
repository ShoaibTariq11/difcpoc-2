/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_Cancel_Open_SR {

    static testMethod void myUnitTest() {
    	
    	SR_Status__c objStat = new SR_Status__c();
    	objStat.Name = 'Draft';
    	objStat.Code__c = 'DRAFT';
    	insert objStat;
    	
        Account objAccount = new Account();
        objAccount.Name = 'Cancel SR Account';
        objAccount.AccountNumber = '12345';
        insert objAccount;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        insert objSR;
        
        Cancel_Open_SR.Cancel_SR(objSR.Id,'Company');
        
        Service_Request__c objSR2 = new Service_Request__c();
        objSR2.Customer__c = objAccount.Id;
        objSR2.Internal_SR_Status__c = objStat.Id;
        objSR2.External_SR_Status__c = objStat.Id;
        insert objSR2;
        
        Cancel_Open_SR.Cancel_SR(objSR2.Id,'Company');
    }
    static testMethod void myUnitTest4() {
    	
    	SR_Status__c objStat = new SR_Status__c();
    	objStat.Name = 'Submitted';
    	objStat.Code__c = 'Submitted';
    	insert objStat;
    	
        Account objAccount = new Account();
        objAccount.Name = 'Cancel SR Account';
        objAccount.AccountNumber = '12345';
        insert objAccount;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        insert objSR;
        
        Cancel_Open_SR.Cancel_SR(objSR.Id,'Company');
        
        Service_Request__c objSR2 = new Service_Request__c();
        objSR2.Customer__c = objAccount.Id;
        objSR2.Internal_SR_Status__c = objStat.Id;
        objSR2.External_SR_Status__c = objStat.Id;
        insert objSR2;
        
        Cancel_Open_SR.Cancel_SR(objSR2.Id,'Company');
    }
      static testMethod void myUnitTest2() {
    	
    	SR_Status__c objStat = new SR_Status__c();
    	objStat.Name = 'Draft';
    	objStat.Code__c = 'DRAFT';
    	insert objStat;
    	
        Account objAccount = new Account();
        objAccount.Name = 'Cancel SR Account';
        objAccount.AccountNumber = '12345';
        insert objAccount;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        insert objSR;
        
        Cancel_Open_SR.Cancel_SR(objSR.Id,'Company');
        
        Service_Request__c objSR2 = new Service_Request__c();
        objSR2.Customer__c = objAccount.Id;
        objSR2.Internal_SR_Status__c = objStat.Id;
        objSR2.External_SR_Status__c = objStat.Id;
        insert objSR2;
        
        Cancel_Open_SR.isPortalLogin(objSR2.Id,'Company');
    }
}