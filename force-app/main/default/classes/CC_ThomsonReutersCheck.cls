/*
    Date        : 21-Nov-2019
    Description : all the amendment record and push it to the TR system
*                 and create TRS record for each request
Version     Modification                                    Date            comments
v1.1        Restrict TR Check Response characters #8822     30/02/2020      added by selva
v1.2        Duplicate TR contacts should not be created     05/04/2020      added by selva #8570
v1.3        Update the logic to handle multiuple TR records 23/05/2020      added by selva #9883
    --------------------------------------------------------------------------------------
*/
global without sharing class CC_ThomsonReutersCheck implements HexaBPM.iCustomCodeExecutable {
    
    global static Thompson_Reuters_Check__c TRCheckObject = new Thompson_Reuters_Check__c();
    global static list<Thompson_Reuters_Check__c> lstTRCheckObject = new list<Thompson_Reuters_Check__c>();
    global static string caseNumber =null;
    
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
       system.debug('===CC_ThompsonReutersCheck------='); 
        HexaBPM__Step__c step = new HexaBPM__Step__c(Id=stp.Id);
        List<TR_Contact__c> trContact = new List<TR_Contact__c>();
        
        HexaBPM__Service_Request__c objSR = new HexaBPM__Service_Request__c(Id=stp.HexaBPM__SR__c);
        try{
            
        if(String.isNotBlank(objSR.Id)){
             //v1.2  Added check for multi structure application - created srIdSet and updated query 
            Set<Id> srIdSet = new Set<Id>();
            srIdSet.add(objSR.Id);
            List<HexaBPM_Amendment__c> hexaAmendmentlst = [Select Id,ServiceRequest__c,OB_MultiStructure_SR__c,Recordtype.Name,Gender__c,Date_of_Birth__c,Nationality_list__c,Registration_Date__c,Company_Name__c,Place_of_Registration__c,Country_of_Registration__c,Name__c,Individual_Corporate_Name__c,Entity_Name__c,Entity_Name__r.Place_of_Registration__c,Entity_Name__r.ROC_reg_incorp_Date__c From HexaBPM_Amendment__c Where (ServiceRequest__c =:objSR.Id OR OB_MultiStructure_SR__c=:objSR.Id) AND  cessation_date__c = null];
            
             List<TR_Contact__c> trList=[select id,HexaBPM_ServiceRequest__c,OB_Amendment__c,Gender__c,Date_of_Birth__c,Nationality_list__c,Registration_Incorporation_Date__c,Company_Name__c,Place_of_Registration__c from TR_Contact__c where HexaBPM_ServiceRequest__c=:objSR.Id];
             
            if(trList.size()>0){ //v1.2
                trContact = checkexistingAmendmentList(hexaAmendmentlst,trList); //v1.2
            }
            else{
                system.debug('====enter==inside=');
                
                
                for(HexaBPM_Amendment__c itr : hexaAmendmentlst){
                    string ApplicationId;
                    if(itr.ServiceRequest__c!=null)
                        ApplicationId = itr.ServiceRequest__c;
                    else
                        ApplicationId = itr.OB_MultiStructure_SR__c;
                    system.debug('!!@@@!'+itr.Recordtype.Name);
                    TR_Contact__c trc = new TR_Contact__c(HexaBPM_ServiceRequest__c=ApplicationId,OB_Amendment__c =itr.id,
                    Gender__c=itr.Gender__c,
                    Date_of_Birth__c=itr.Date_of_Birth__c,
                    Nationality_list__c=itr.Nationality_list__c,
                    Registration_Incorporation_Date__c = itr.Registration_Date__c,
                    Company_Name__c=itr.Company_Name__c,
                    Place_of_Registration__c=itr.Place_of_Registration__c);
                    
                   
                    if(itr.Name__c !=null ){
                        trc.Given_Name__c = itr.Name__c;
                    }
                        
                    system.debug('!!@@#@@'+trc.Given_Name__c);
                    if(itr.Recordtype.Name.equalsIgnorecase('Individual')){
                        trc.TR_Contact_type__c = 'Individual';
                        trc.Given_Name__c = itr.Individual_Corporate_Name__c;
                    }else {
                        trc.TR_Contact_type__c = 'Body Corporate'; 
                        trc.Company_Name__c = itr.Individual_Corporate_Name__c;
                        if(String.isBlank(itr.Place_of_Registration__c) && String.IsNotBlank(itr.Entity_Name__c) && String.IsNotBlank(itr.Entity_Name__r.Place_of_Registration__c)){
                            trc.Place_of_Registration__c = itr.Entity_Name__r.Place_of_Registration__c;
                        }
                        if(itr.Registration_Date__c != null && String.IsNotBlank(itr.Entity_Name__c) && itr.Entity_Name__r.ROC_reg_incorp_Date__c != null){
                            trc.Registration_Incorporation_Date__c = itr.Entity_Name__r.ROC_reg_incorp_Date__c;
                        }
                    }
                    trContact.add(trc);
                }
                if(trContact.size()>0){
                    upsert trContact;
                }
            }
            system.debug('=====trContact===='+trContact);
            if(trContact.size()>0){
                //for(TR_Contact__c tr:trContact){
                    //if(tr.id!=null){
                        //ThompsonReutersValidation.checkTRCallout(tr.OB_Amendment__c,objSR.Id,stp.Id,'step');
                        // Queuable Job
                        system.debug('=====queue====');
                        //System.enqueueJob(new OB_ThompsonReuters_Queuable(tr.OB_Amendment__c,stp.Id,objSR.Id)); 
                        //checkTRCallout(tr.OB_Amendment__c,stp.Id,objSR.Id); 
                        checkTRCallout(null,stp.Id,objSR.Id); 
                        //strResult =  'World Check Initiated';
                        //strResult =  'Success';
                   // }
                //}
            }
        }else{
            strResult = 'Service Request cannot be blank';
        }
        }catch(Exception e){
            strResult = e.getMessage();
            Thompson_Reuters_Check__c trc = new Thompson_Reuters_Check__c ();
            if(stp != null && stp.Id != null){
            trc.HexaBPM_Step__c = stp.Id;
            }
            if(objSR != null && objSR.Id != null){
            trc.HexaBPM_ServiceRequest__c = objSR.Id;
            }
            trc.TR_Check_Response__c='Exception'+'--'+strResult;
            insert trc;
            
            insert LogDetails.CreateLog(null, 'CC_ThompsonReutersCheck : EvaluateCustomCode', 'Line Number : '+e.getLineNumber()+'\nException is : '+strResult);
            
        }
        
        system.debug('=====strResult===='+strResult);
        return strResult;
    }
    /*
    * webservice callout for each TR contact (Includes Individual as well Corporate)
    * after success creates TR check for each Tr contact
    **/
    @future(callout=true)
    public static void checkTRCallout(string amendId,string stepID,string servId){ 
        system.debug('=====checkTRCallout====');
        string subStringXML = '';
        string contactName = '';
        string contactType = 'INDIVIDUAL';
        string bcType = 'ORGANISATION';
        string contactDOBwithoutTime = '';
        string contactGender = ''; 
        string Nationality = '';
        string clientID ='';
        string companyName = '';
        string companyRegDate = '';
        string TRContacttype = '';
        string placeofReg = '';
        String authorizationHeader = system.label.ThompsonReuters_Authorization;
        //String url = 'https://trsd.difc.ae/transwatchwebapp/webresources/sdqueryservice/search'; // single request
        String url = 'https://trsd.difc.ae/transwatchwebapp/webresources/sdqueryservice/searches'; // multiple request
        try{
            List<TR_Contact__c> trContactReq = [Select id,Company_Name__c,Registration_Incorporation_Date__c,Name,Nationality_list__c,
                                                Gender__c,OB_Amendment__c,
                                                Given_Name__c,Date_of_Birth__c,
                                                HexaBPM_ServiceRequest__c,
                                                Place_of_Registration__c,
                                                TR_Contact_type__c,
                                                Thomson_Reuters_Check__c 
                                             From TR_Contact__c 
                                             Where HexaBPM_ServiceRequest__c=:servId 
                                             Order by createddate DESC];
                                             //OB_Amendment__c=:amendId
             string XMLmulString = '';
            for(TR_Contact__c trAmenRec : trContactReq){
              if(string.isNotBlank(trAmenRec.TR_Contact_type__c)) {
                TRContacttype = trAmenRec.TR_Contact_type__c; 
              }
              if(string.isNotBlank(trAmenRec.Given_Name__c)) {
                contactName = trAmenRec.Given_Name__c; 
              }
              if(string.isNotBlank(trAmenRec.Gender__c)) {
                contactGender = trAmenRec.Gender__c.toUpperCase();
              }
              if(trAmenRec.Date_of_Birth__c != Null) {
                contactDOBwithoutTime = string.valueOf(trAmenRec.Date_of_Birth__c);
                contactDOBwithoutTime = contactDOBwithoutTime.replace(' 00:00:00', '');
              }
              if(string.isNotBlank(trAmenRec.Company_Name__c)){
                  companyName  = trAmenRec.Company_Name__c;
              }
              if(trAmenRec.Registration_Incorporation_Date__c!=null){
                   companyRegDate  = string.valueOf(trAmenRec.Registration_Incorporation_Date__c);
                   companyRegDate = companyRegDate.replace(' 00:00:00', '');
              }
              if(string.isNotBlank(trAmenRec.Nationality_list__c)){
                   Nationality  = trAmenRec.Nationality_list__c;
              }
              if(string.isNotBlank(trAmenRec.Place_of_Registration__c)){
                   placeofReg  = trAmenRec.Place_of_Registration__c;
              }
              clientID = trAmenRec.Name;
               subStringXML += '<sdQuery><clientType>UNSPECIFIED</clientType><createCaseOnMatches>UNSPECIFIED</createCaseOnMatches><types><type>'+contactType+'</type></types><name>'+contactName+'</name></sdQuery>';
               //Single request
                if(String.IsNotBlank(TRContacttype) && TRContacttype.equalsIgnorecase('Individual')){
                 XMLmulString += '<sdQuery><types><type>'+contactType+'</type></types><name>'+contactName+'</name><indDob>'+contactDOBwithoutTime+'</indDob><clientID>'+clientID+'</clientID><nationalityName>'+Nationality+'</nationalityName><indGender>'+contactGender+'</indGender><clientType>'+'CUSTOMER'+'</clientType><createCaseOnMatches>'+'CREATECASE'+'</createCaseOnMatches></sdQuery>';
                }
                //BC XML
                else if(String.IsNotBlank(TRContacttype) && TRContacttype.equalsIgnorecase('Body Corporate')){
                 XMLmulString += '<sdQuery><types><type>'+bcType+'</type></types><name>'+companyName+'</name><indDob>'+companyRegDate+'</indDob><clientID>'+clientID+'</clientID><nationalityName>'+placeofReg+'</nationalityName><clientType>'+'CUSTOMER'+'</clientType><createCaseOnMatches>'+'CREATECASE'+'</createCaseOnMatches></sdQuery>';
                }
            }
            
            system.debug('=====contactName==='+contactName);
            system.debug('=====contactGender==='+contactGender);
            system.debug('=====contactDOBwithoutTime==='+contactDOBwithoutTime);

            string XMLString = '';
            //Single request
            if(String.IsNotBlank(TRContacttype) && TRContacttype.equalsIgnorecase('Individual')){
             //XMLString = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><sdQuery><types><type>'+contactType+'</type></types><name>'+contactName+'</name><indDob>'+contactDOBwithoutTime+'</indDob><clientID>'+clientID+'</clientID><nationalityName>'+Nationality+'</nationalityName><indGender>'+contactGender+'</indGender><clientType>'+'CUSTOMER'+'</clientType><createCaseOnMatches>'+'CREATECASE'+'</createCaseOnMatches></sdQuery>';
            }
            //BC XML
            else if(String.IsNotBlank(TRContacttype) && TRContacttype.equalsIgnorecase('Body Corporate')){
             //XMLString = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><sdQuery><types><type>'+bcType+'</type></types><name>'+companyName+'</name><indDob>'+companyRegDate+'</indDob><clientID>'+clientID+'</clientID><nationalityName>'+placeofReg+'</nationalityName><clientType>'+'CUSTOMER'+'</clientType><createCaseOnMatches>'+'CREATECASE'+'</createCaseOnMatches></sdQuery>';
            }
            
            string multipleXMLString = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><sdQueries><queries>'+XMLmulString+'</queries></sdQueries>';
            
            Http h = new Http();
            HttpRequest req = new HttpRequest();

            req.setEndpoint(url);
            req.setMethod('POST'); //GET, POST
            req.setTimeout(120000); //2 min, 120000 ms
            req.setHeader('Accept', 'Application/XML');
            req.setHeader('Authorization', authorizationHeader);
            req.setHeader('Content-Type', 'Application/xml; chartset=UTF-8');
            req.setHeader('Username', 'api'); //W001
            req.setHeader('Password', 'Trsd_123A'); //TEST1NG

            //req.setBody(XMLString);
            req.setBody(multipleXMLString);
            system.debug('====Request Body===='+req.getBody());

            HttpResponse res = h.send(req);
            system.debug('===RESPONSE STATUS CODE==='+res.getStatusCode());
            system.debug('===RESPONSE BODY==='+res.getBody());
            //system.debug('===RESPONSE BODY==res='+res);
            
            if(res.getstatusCode() == 200 && res.getbody() != null) {
                
                //Document Class to process the content in the body of the XML document.
                Dom.Document doc = res.getBodyDocument();
                
                DOM.XmlNode rootNode = doc.getRootElement(); //Retrieve the root element for this document. 
                system.debug('====rootNode==='+rootNode);
                system.debug('===RESPONSE BODY==getBodyDocument='+doc.getRootElement());
                //parseResponseXML(rootNode,servId,stepID);
                parsechildXMLResponse(rootNode,servId,stepID);
                /****************************** 
                //v1.3 commented out
                
                system.debug('=caseNumber=out='+caseNumber);
                if(string.isBlank(TRCheckObject.Entity_Id__c)) {
                    TRCheckObject.TR_Check_Result__c = 'TR Check Unsuccesful: Match Not Found';
                }
                TRCheckObject.HexaBPM_Step__c = stepID;
                TRCheckObject.HexaBPM_ServiceRequest__c = servId;
                  
                Integer maxSize = 131050;
                if(res.getBody().length() > maxSize ){
                    TRCheckObject.TR_Check_Response__c = res.getBody().substring(0, maxSize);
                }else{
                    TRCheckObject.TR_Check_Response__c = res.getBody();
                }
                lstTRCheckObject.add(TRCheckObject);
                
                system.debug('=inserted===TRCheckObject==='+lstTRCheckObject.size());
                ***********************/
                if(!lstTRCheckObject.isEmpty()) {
                    insert lstTRCheckObject;
                    
                    Attachment att = new Attachment(); //v1.3
                    att.ParentId = stepID;
                    att.name = 'Thompson Reuters Response.txt';
                    att.ContentType = 'text/plain';
                    att.Body = Blob.valueOf(res.getBody());
                    insert att;
                    system.debug('===TR record inserted=='+lstTRCheckObject);
                    //successMessage = 'TR check successfully completed, check the related TR records.';
                    HexaBPM__Service_Request__c sr = new HexaBPM__Service_Request__c(id=servId,Response_Code__c='AML');
                    update sr;
                }
                List<TR_Contact__c> trcontactLstUpdate  = new List<TR_Contact__c>();
                for(Thompson_Reuters_Check__c itr:lstTRCheckObject){
                    for(TR_Contact__c tc:trContactReq){
                        if(itr.HexaBPMAmendment__c  == tc.OB_Amendment__c){
                            TR_Contact__c  tcUpdate = new TR_Contact__c(id=tc.id,Thomson_Reuters_Check__c=itr.id);
                            trcontactLstUpdate.add(tcUpdate);
                            itr.TR_Contact__c  = tc.Id;
                        }
                    }
                }
                system.debug('===trcontactLstUpdate='+trcontactLstUpdate.size());
                if(trcontactLstUpdate.size()>0){
                    //update trcontactLstUpdate;
                }
                 system.debug('===lstTRCheckObject='+lstTRCheckObject.size());
                if(lstTRCheckObject.size()>0){
                        update lstTRCheckObject;
                }
                
            }else{
                //v1.1 start
                Integer maxSize = 131050;
                string resBody=null;
                if(res.getBody().length() > maxSize ){
                    resBody = res.getBody().substring(0, maxSize);
                }else{
                    resBody = res.getBody();
                }
                //v1.1 end
                Thompson_Reuters_Check__c trc = new Thompson_Reuters_Check__c (HexaBPM_Step__c = stepID,HexaBPM_ServiceRequest__c = servId,TR_Check_Response__c=resBody);
                insert trc;
            }
        }catch(Exception e){
            string errresult = e.getMessage();
            Thompson_Reuters_Check__c trc = new Thompson_Reuters_Check__c (HexaBPM_Step__c = stepID,HexaBPM_ServiceRequest__c = servId,TR_Check_Response__c=errresult);
            insert trc;
            insert LogDetails.CreateLog(null, 'CC_ThompsonReutersCheck : checkTRCaseStatus', 'Line Number : '+e.getLineNumber()+'\nException is : '+errresult);
        }
    }
    public static void parsechildXMLResponse(DOM.XMLNode node, string servId,string stepID) {
        String currentNodeName;
            for (Dom.XMLNode child : node.getChildElements()) {
                currentNodeName = child.getName();
                if(currentNodeName == 'resultSets'){
                    parsechildXMLsdResultSet(child,servId,stepID);
                }
            }
    }
    public static void parsechildXMLsdResultSet(DOM.XMLNode node, string servId,string stepID) { //v1.3
        String currentNodeName;
            for (Dom.XMLNode child : node.getChildElements()) {
                currentNodeName = child.getName();
                if(currentNodeName == 'sdResultSet'){
                    parseResponseXML(child,servId,stepID);
                }
            }
    }
    
     public static void parseResponseXML(DOM.XMLNode nodeList, string servId,string stepID) {
        system.debug('=parseResponseXML==');
        try{
        if(nodeList.getNodeType() == DOM.XMLNodeType.ELEMENT) { //v1.3
                 system.debug('---420----'+nodeList.getName());
                 Thompson_Reuters_Check__c TRCheckChilLst = new Thompson_Reuters_Check__c();
                 for(Dom.XMLNode sdRes : nodeList.getChildElements()) {  
                    if (sdRes.getNodeType() == DOM.XMLNodeType.ELEMENT) {
                        if(sdRes.getName() == 'caseNum') {
                            if(caseNumber==null){
                                caseNumber = sdRes.getText();
                            }
                        }
                        if(sdRes.getName() == 'clientCode') {
                            TRCheckChilLst.TR_Contact_Name__c = sdRes.getText();
                        }
                    }
                    if(sdRes.getName() == 'sdQuery'){
                        for(Dom.XMLNode childP : sdRes.getChildElements()) {  
                            if (childP.getNodeType() == DOM.XMLNodeType.ELEMENT) {
                                 system.debug('---436----'+childP.getName());
                                if(childP.getName() == 'type') {
                                    TRCheckChilLst.Entity_Type__c = childP.getText();
                                }
                                if(childP.getName() == 'name') {
                                    TRCheckChilLst.Name__c = childP.getText();
                                }
                                if(childP.getName() == 'nationalityName') {
                                    TRCheckChilLst.Nationality__c = childP.getText();
                                }
                            }
                        }
                    }
                }
                TRCheckChilLst.HexaBPM_Step__c = stepID;
                TRCheckChilLst.HexaBPM_ServiceRequest__c = servId;
                TRCheckChilLst.TRCase_No__c = caseNumber;
                if(caseNumber==null){
                    TRCheckChilLst.TR_Check_Result__c = 'TR Check Unsuccesful: Match Not Found';
                }
                caseNumber =null; //clearing the old values
                lstTRCheckObject.add(TRCheckChilLst);
            }
            /******************* //v1.3 commented out
            if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
                system.debug('=caseNum==');
                if(node.getName() == 'caseNum') {
                    if(caseNumber==null){
                        caseNumber = node.getText();
                    }
                }
                system.debug('=caseNumber=='+caseNumber);
                if(node.getName() == 'sdResult'){
                    TRCheckObject = new Thompson_Reuters_Check__c();
                    //if(objectName =='Step' ){
                      TRCheckObject.HexaBPM_Step__c = stepID;
                      TRCheckObject.HexaBPM_ServiceRequest__c = servId;
                    //}
                    TRCheckObject.TRCase_No__c = caseNumber;// assign the casenumber in child transaction
                }
                if(node.getName() == 'entityId') {
                  TRCheckObject.Entity_Id__c = node.getText().trim();
                  TRCheckObject.TR_Check_Result__c = 'TR Check Successful: Match Found';
                }
                if(node.getName() == 'category'){
                  TRCheckObject.Category__c = node.getText().trim(); 
                }
                if(node.getName() == 'modifiedDate') {
                  TRCheckObject.Modified_Date__c = date.valueOf(node.getText().trim());
                }
                if(node.getName() == 'name') {
                  TRCheckObject.Name__c = node.getText().trim();
                }
                if(node.getName() == 'originalScript') {
                  TRCheckObject.Original_Script__c = node.getText().trim();
                }
                if(node.getName() == 'score') {
                  TRCheckObject.Score__c = node.getText().trim();
                }
                if(node.getName() == 'secondaryAddrInfo') {
                  TRCheckObject.Secondary_Address_Info__c = node.getText().trim();
                }
                if(node.getName() == 'secondaryCityInfo') {
                  TRCheckObject.Secondary_City_Info__c = node.getText().trim();
                }
                if(node.getName() == 'secondaryCountryInfo') {
                  TRCheckObject.Secondary_Country_Info__c = node.getText().trim();
                }
                if(node.getName() == 'secondaryDateOfBirthInfo') {
                  TRCheckObject.Secondary_Date_Of_Birth_Info__c = node.getText().trim();
                }
                if(node.getName() == 'secondaryGenderInfo') {
                  TRCheckObject.Secondary_Gender_Info__c = node.getText().trim();
                }
                if(node.getName() == 'secondaryNationalityInfo') {
                  TRCheckObject.Secondary_Nationality_Info__c = node.getText().trim();
                }
                if(node.getName() == 'secondaryResidencyInfo') {
                  TRCheckObject.Secondary_Residency_Info__c = node.getText().trim();
                }
                if(node.getName() == 'secondaryRuleInfo') {
                  TRCheckObject.Secondary_Rule_Info__c = node.getText().trim();
                }
                if(node.getName() == 'secondaryScore') {
                  TRCheckObject.Secondary_Score__c = node.getText().trim();
                }
                if(node.getName() == 'source') {
                  TRCheckObject.Source__c = node.getText().trim();
                }
                if(node.getName() == 'totalScore') {
                  TRCheckObject.Total_Score__c = node.getText().trim();
                }
                if(node.getName() == 'type') {
                  TRCheckObject.Entity_Type__c = node.getText().trim();
                }
                if(node.getName() == 'clientID'){
                    TRCheckObject.TR_Contact_Name__c =node.getText().trim();
                }
                //Added 21 may - start
                if(string.isBlank(TRCheckObject.Entity_Id__c)) {
                    TRCheckObject.TR_Check_Result__c = 'TR Check Unsuccesful: Match Not Found';
                }
                TRCheckObject.HexaBPM_Step__c = stepID;
                TRCheckObject.HexaBPM_ServiceRequest__c = servId;
                Integer maxSize = 131050;
                string responseString = null;
                if(node.getText().length() > maxSize ){
                    responseString = node.getText().substring(0, maxSize);
                }else{
                    responseString = node.getText();
                }
               TRCheckObject.TR_Check_Response__c = responseString;
                
                //Added 21 may - end
                 lstTRCheckObject.add(TRCheckObject);
            }
            
            String currentNodeName1;
            for (Dom.XMLNode child : node.getChildElements()) {
                currentNodeName1 = child.getName();
                if(currentNodeName1 == 'sdResultSet'){
                    parseResponseXML(child,servId,stepID);
                }
            }
            ***************************************************/
        }
        catch(exception e){
            insert LogDetails.CreateLog(null, 'CC_ThompsonReutersCheck : parseResponseXML', 'Line Number : '+e.getLineNumber()+'\nException is : '+e.getMessage());
            system.debug('Exception Caught at line : '+e.getLineNumber()+' in ThompsonReutersValidation.parseResponseXML method. Message: '+e.getMessage());
        }
    
     }
     
     //v1.2 start
      public static List<TR_Contact__c> checkexistingAmendmentList(List<HexaBPM_Amendment__c > amendmentList,List<TR_Contact__c> trcontactUpdatelst){
        
        List<TR_Contact__c> updatelist = new List<TR_Contact__c>();
        for(HexaBPM_Amendment__c itr:amendmentList){ //lstAmendment
           //system.debug('!!@@@!'+itr.Recordtype.Name);
           for(TR_Contact__c trcVar :trcontactUpdatelst){
               if(itr.id== trcVar.OB_Amendment__c ){
                    TR_Contact__c trc = new TR_Contact__c(HexaBPM_ServiceRequest__c=itr.ServiceRequest__c,OB_Amendment__c  =itr.id,
                    Gender__c=itr.Gender__c,Date_of_Birth__c=itr.Date_of_Birth__c,
                    Nationality_list__c=itr.Nationality_list__c,Registration_Incorporation_Date__c = itr.Registration_Date__c,
                    Company_Name__c=itr.Company_Name__c,Place_of_Registration__c=itr.Place_of_Registration__c,id=trcVar.Id);
                    //system.debug('!!@@!!'+itr);
                   
                    if(itr.Name__c !=null ){
                        trc.Given_Name__c = itr.Name__c;
                    }
                        
                    system.debug('!!@@#@@'+trc.Given_Name__c);
                    if(itr.Recordtype.Name.equalsIgnorecase('Individual')){
                        trc.TR_Contact_type__c = 'Individual';
                        trc.Given_Name__c = itr.Individual_Corporate_Name__c;
                    }else {
                        trc.TR_Contact_type__c = 'Body Corporate'; 
                        trc.Company_Name__c = itr.Individual_Corporate_Name__c;
                        if(String.isBlank(itr.Place_of_Registration__c) && String.IsNotBlank(itr.Entity_Name__c) && String.IsNotBlank(itr.Entity_Name__r.Place_of_Registration__c)){
                            trc.Place_of_Registration__c = itr.Entity_Name__r.Place_of_Registration__c;
                        }
                        if(itr.Registration_Date__c != null && String.IsNotBlank(itr.Entity_Name__c) && itr.Entity_Name__r.ROC_reg_incorp_Date__c != null){
                            trc.Registration_Incorporation_Date__c = itr.Entity_Name__r.ROC_reg_incorp_Date__c;
                        }
                    }
                    updatelist.add(trc);
                }
           }
        }
        if(updatelist.size()>0){
            upsert updatelist;
        }
        return updatelist;
    }  //v1.2 start
     
      /***************Use this method to check the TR case status****************/
    /*
    * This is called form OB_CheckTRCaseStatusSchedule  
    */
   /* public static void checkTRCaseStatus(string caseNumber){
     
         
    
        string trResponseCaseNumber=null;
        String authorizationHeader = system.label.ThompsonReuters_Authorization;
        String url = 'https://trsd.difc.ae/transwatchwebapp/webresources/sdqueryservice/caseQuery';
        //'http://127.0.0.1:8080/transwatchwebapp/webresources/sdqueryservice/caseQuery';
        
        try{
            if(caseNumber!=null){
                Http h = new Http();
                HttpRequest req = new HttpRequest();

                req.setEndpoint(url);
                req.setMethod('POST'); //GET, POST
                req.setTimeout(120000); //2 min, 120000 ms
                req.setHeader('Accept', 'Application/XML');
                req.setHeader('Authorization', authorizationHeader);
                req.setHeader('Content-Type', 'Application/xml; chartset=UTF-8');
                req.setHeader('Username', 'api'); //W001
                req.setHeader('Password', 'Trsd_123A'); //TEST1NG

                req.setBody(caseNumber);
                system.debug('====Request Body===='+req.getBody());

                HttpResponse res = h.send(req);
                system.debug('===RESPONSE STATUS CODE==='+res.getStatusCode());
                system.debug('===RESPONSE BODY==='+res.getBody());
                    
                if(res.getstatusCode() == 200 && res.getbody() != null) {
                
                    //Document Class to process the content in the body of the XML document.
                    Dom.Document doc = res.getBodyDocument();
                    DOM.XmlNode rootNode = doc.getRootElement(); //Retrieve the root element for this document. 
                    //system.debug('====rootNode==='+rootNode);
                   
                    CaseStatusResponseXML(rootNode,caseNumber);
                }
            }
        }
        catch(Exception e){
            string errorresult = e.getMessage();
            insert LogDetails.CreateLog(null, 'CC_ThompsonReutersCheck : checkTRCaseStatus', 'Line Number : '+e.getLineNumber()+'\nException is : '+errorresult);  
        }
    }*/
    /*
    * Loop on TR check and read the DOM node to populate the status on TR check
    */
    /*public static void CaseStatusResponseXML(DOM.XMLNode node,string trcaseNumber){
        List<Thompson_Reuters_Check__c> trcList =[select id,TR_Case_Status__c from Thompson_Reuters_Check__c where TRCase_No__c=:trcaseNumber];
        
         for (Dom.XMLNode child : node.getChildElements()) {
            parseCaseStatus(child,trcaseNumber,trcList);
         }
         update trcList;
    }
    public static void parseCaseStatus(DOM.XMLNode node,string trcaseNumber,List<Thompson_Reuters_Check__c> trcList){
        //Thompson_Reuters_Check__c trc = new Thompson_Reuters_Check__c();
        for(Thompson_Reuters_Check__c itr:trcList){
            if (node.getNodeType() == DOM.XMLNodeType.ELEMENT){
                if(node.getName() == 'caseCode'){
                }
                if(node.getName() == 'caseCreationDate'){
                }
                if(node.getName() == 'caseReferTo'){
                }
                if(node.getName() == 'caseStatus'){
                    system.debug('------case status-----'+node.getText().trim());
                    itr.TR_Case_Status__c = node.getText().trim(); 
                }
                if(node.getName() == 'custName'){
                }
                if(node.getName() == 'numOfFalse'){
                }
                if(node.getName() == 'numOfNew'){
                }
                if(node.getName() == 'numOfPossible'){
                }
                if(node.getName() == 'numOfPostive'){
                }
            }
        }
    }*/
}