/******************************************************************************************
 *  Author     : Ghanshyam Yadav
 *  Date       : 15-08-2018
 *  Description  : CancellationStepClosedBatch schedule
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    15-08-2018  Ghanshyam      Created
****************************************************************************************************************/
global class CancellationStepClosedBatchschedule implements Schedulable {

   global void execute(SchedulableContext SC) {
   
        CancellationStepClosedBatch cstp = new CancellationStepClosedBatch(); 
        
        database.executebatch(cstp,200);
   }
}