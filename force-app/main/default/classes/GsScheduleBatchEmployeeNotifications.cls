//V1.1  Swati   05/10/2016  As per ticket 2851
//V1.2  Swati   15/10/2016  As per ticket 3764
//V1.3  Shoaib  10/01/2020  As per ticket 8034

global with sharing class GsScheduleBatchEmployeeNotifications implements Database.Batchable<sObject>,Database.AllowsCallouts,Schedulable {
   
   global static final Date expiryDate5       = system.today().addDays(5);
   global static final Date expiryDate        = system.today();
   global static final Date expiryDate15      = system.today().addDays(-15);
   global static final Date expiryDateTemp    = system.today().addMonths(1);
    
    global void execute(SchedulableContext ctx) {
        Database.executeBatch(new GsScheduleBatchEmployeeNotifications(), 100);
    }
    
    global list<Contact> start(Database.BatchableContext BC ){
  
        list<Contact> lstContacts = [select Id,Name,AccountId from Contact where RecordType.DeveloperName = 'GS_Contact' 
            AND Id IN (select Contact__c from Document_Details__c where (EXPIRY_DATE__c =: expiryDate5 OR EXPIRY_DATE__c =: expiryDate OR EXPIRY_DATE__c =:expiryDate15 ) AND Document_Type__c = 'Employment Visa'  AND Contact__c != null)
            AND Id NOT IN (select Contact__c from Document_Details__c where (EXPIRY_DATE__c >: expiryDateTemp) AND Document_Type__c = 'Employment Visa' AND Contact__c != null )
            AND AccountId != null AND Is_Absconder__c = false];
       
        return lstContacts;
    }
    
    global void execute(Database.BatchableContext BC, list<Contact> lstContacts){
        
        list<Id> lstAccountIds                  = new list<Id>();
        map<Id,list<string>> mapEmpServiceUsers = new map<Id,list<string>>();
        Map<String,Document_Details__c> documentMap = new Map<String,Document_Details__c>();
        list<Contact> lstContactInsert          = new list<Contact>();
        Set<Id> contactIds                      = new Set<Id>(); 
        Set<Id> newContactIds                   = new Set<Id>();
        
        for(Contact objCon : lstContacts){
            lstAccountIds.add(objCon.AccountId);
            contactIds.add(objCon.Id);
        }
     
        //V1.3  Shoaib  10/01/2020  As per ticket 8034
      
        //Consider the scanerio where there are two Document_Details__c for same contact
        for(Document_Details__c thisDetails : [select Contact__c,EXPIRY_DATE__c from Document_Details__c WHERE Contact__c IN: contactIds AND Document_Type__c = 'Employment Visa' AND  (EXPIRY_DATE__c =: expiryDate5 OR EXPIRY_DATE__c =: expiryDate OR EXPIRY_DATE__c =:expiryDate15) ]){
               documentMap.put(thisDetails.Contact__c,thisDetails ); 
        }
        
       
        for(Relationship__c thisRelationship : [SELECT Id,Object_Contact__c,End_Date__c,Status__c FROM Relationship__c WHERE Object_Contact__c  IN : contactIds]){
            if(documentMap.ContainsKey(thisRelationship.Object_Contact__c)
               && documentMap.get(thisRelationship.Object_Contact__c).EXPIRY_DATE__c != null
               && documentMap.get(thisRelationship.Object_Contact__c).EXPIRY_DATE__c == thisRelationship.End_Date__c
               && thisRelationship.Status__c  != 'Cancelled'){
                   
                      newContactIds.add(thisRelationship.Object_Contact__c);
             }
        }
        
        list<Contact> lstContactsNew = [select Id,Sys_Send_Email__c,Name,AccountId from Contact where RecordType.DeveloperName = 'GS_Contact' AND AccountId != null AND Is_Absconder__c = false AND ID IN:newContactIds];
     
        for(User objUser : [select Id,Email,Contact.AccountId from User where IsActive = true AND User.Community_User_Role__c includes ('Employee Services') AND ContactId != null AND ContactId IN (select Id from Contact where AccountId IN : lstAccountIds AND RecordType.DeveloperName = 'Portal_User') ]){
            list<string> lstUserIds = mapEmpServiceUsers.containsKey(objUser.Contact.AccountId) ? mapEmpServiceUsers.get(objUser.Contact.AccountId) : new list<string>();
            lstUserIds.add(objUser.Id);
            mapEmpServiceUsers.put(objUser.Contact.AccountId,lstUserIds);
        }
        
        for(Contact objCon : lstContactsNew){
            list<string> lst = mapEmpServiceUsers.get(objCon.AccountId);
            if(lst != null && !lst.isEmpty()){
                Contact objTemp = new Contact(Id=objCon.Id);
                objTemp.Sys_Send_Email__c = true;
                if(lst.size() > 0)
                    objTemp.Portal_User_1__c = lst[0];
                if(lst.size() > 1)
                    objTemp.Portal_User_2__c = lst[1];
                if(lst.size() > 2)
                    objTemp.Portal_User_3__c = lst[2];
                if(lst.size() > 3)
                    objTemp.Portal_User_4__c = lst[3];
                if(lst.size() > 4)
                    objTemp.Portal_User_5__c = lst[4];
                lstContactInsert.add(objTemp);
            }
        }
        if(!lstContactInsert.isEmpty()){
            update lstContactInsert;
        }
    }
    
    global void finish(Database.BatchableContext BC){
         checkEstablishmentCardExpiry();
    }
    
    public void checkEstablishmentCardExpiry(){//V1.1
        list<Id> lstAccountIds = new list<Id>();
        map<Id,list<string>> mapEmpServiceUsers = new map<Id,list<string>>();
        list<Identification__c> lstIdentificationUpdate = new list<Identification__c>();
        
        list<Identification__c> lstIdentification = [select Account__c from Identification__c where (Valid_To__c =: expiryDate5 OR Valid_To__c =: expiryDate OR Valid_To__c =:expiryDate15) and Account__r.Index_Card_Status__c =: 'Active'];//V1.2
        
        if(lstIdentification!=null && !lstIdentification.isEmpty()){
            for(Identification__c obj : lstIdentification){
                lstAccountIds.add(obj.Account__c);
            }
            
            if(lstAccountIds!=null && !lstAccountIds.isEmpty()){
                for(User objUser : [select Id,Email,Contact.AccountId from User where IsActive = true AND User.Community_User_Role__c includes ('Employee Services') AND ContactId != null AND ContactId IN (select Id from Contact where AccountId IN : lstAccountIds AND RecordType.DeveloperName = 'Portal_User') ]){
                    list<string> lstUserIds = mapEmpServiceUsers.containsKey(objUser.Contact.AccountId) ? mapEmpServiceUsers.get(objUser.Contact.AccountId) : new list<string>();
                    lstUserIds.add(objUser.Id);
                    if(lstUserIds!=null && !lstUserIds.isEmpty()){
                        mapEmpServiceUsers.put(objUser.Contact.AccountId,lstUserIds);
                    }
                }
            }   
            
            if(mapEmpServiceUsers!=null && !mapEmpServiceUsers.isEmpty()){
                for(Identification__c obj : lstIdentification){
                    list<string> lst = mapEmpServiceUsers.get(obj.Account__c);
                    if(lst != null && !lst.isEmpty()){
                        Identification__c objTemp = new Identification__c(Id=obj.Id);
                        objTemp.Send_Expiry_Mail__c = true;
                        if(lst.size() > 0)
                            objTemp.Portal_User_1__c = lst[0];
                        if(lst.size() > 1)
                            objTemp.Portal_User_2__c = lst[1];
                        if(lst.size() > 2)
                            objTemp.Portal_User_3__c = lst[2];
                        if(lst.size() > 3)
                            objTemp.Portal_User_4__c = lst[3];
                        if(lst.size() > 4)
                            objTemp.Portal_User_5__c = lst[4];
                        lstIdentificationUpdate.add(objTemp);
                    }
                }
            }
            
            if(lstIdentificationUpdate!=null && !lstIdentificationUpdate.isEmpty()){
                update lstIdentificationUpdate;
            }
        }
    }
}