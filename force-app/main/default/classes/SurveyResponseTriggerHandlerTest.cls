@isTest
public with sharing class SurveyResponseTriggerHandlerTest {
    
    public static List<Survey_Response__c> resList;
    public static Survey__c survey;
    public static void init(){
      
      survey= new Survey__c();
      survey.Session_ID__c='1234';
      survey.Survey_Title__c='Test';
      insert survey;
      
      resList= new List<Survey_Response__c>();
      Survey_Response__c res= new Survey_Response__c();
      res.Session_ID__c='1234';
      res.Question__c='Are You Testing?';
      res.Sub_Question__c='yes';
      res.Response__c='1 - Poor';
      res.Survey__c=survey.id;
      res.Comments__c = 'test';
      resList.add(res);
      
      insert resList;
    }
    
    @isTest static void test(){
      
      init();
      
      SurveyResponseTriggerHandler.assigntToSurvey(reslist);
      SurveyResponseTriggerHandler.CreateCase(resList);
      
    }
}