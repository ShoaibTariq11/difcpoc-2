public class NorBlockDataVerificationController{
    
    public Account accountInformation {get;set;}
    public List<User> lstContacts {get;set;}
    
    public void dataVerificationMethod(){
        
        if(ApexPages.currentPage().getParameters().get('id') !=null){
            String AccountID = ApexPages.currentPage().getParameters().get('id');
            
            accountInformation = new Account();
            accountInformation  = [SELECT Name,Legal_Type_of_Entity__c,Registration_Type__c ,License_Issue_Date__c,
                                   Next_Renewal_Date__c,Registration_License_No__c,ROC_reg_incorp_Date__c,
                                   Tax_Registration_Number_TRN__c,Exempted_from_UBO_Regulations__c,Building_Name__c,
                                   Office__c,Street__c,City__c,Lease_Types__c,BP_No__c FROM Account where ID=:AccountID]; 
            system.debug('!@@@@'+accountInformation);
        
            lstContacts  = new List<User>();
            lstContacts   = [SELECT Contact.Name,Contact.BirthDate,Contact.Passport_No__c,Contact.Nationality__c,Contact.Email,Contact.Phone,Contact.Country__c FROM User where Contact.AccountID=:AccountID];
            
        }
      
    }
}