/*
Author      :   Ravi
Description :   This class will be controller for the Listing website map Viewl page
--------------------------------------------------------------------------------------------------------------------------

Modification History
--------------------------------------------------------------------------------------------------------------------------
V.No	Date		Updated By    	Description
--------------------------------------------------------------------------------------------------------------------------             
*/
public with sharing class ListingMapView {
	
	public string buildingJSONString {get;set;}
	
	public ListingMapView(){
		list<MapViewDetails> lstMVD = new list<MapViewDetails>();
		MapViewDetails objMVD;
		map<Id,Integer> mapLCount = new map<Id,Integer>();
		for(Listing__c obj : [select Id,Unit__c,Unit__r.Building__c from Listing__c where Unit__r.Building__c != null AND Status__c = 'Active']){
			Integer i = mapLCount.containsKey(obj.Unit__r.Building__c) ? (mapLCount.get(obj.Unit__r.Building__c)+1) : 1;
			mapLCount.put(obj.Unit__r.Building__c,i);
		}
		for(Building__c objB : [select Id,Name,Building_No__c from Building__c where Building_No__c != null]){
			objMVD = new MapViewDetails();
			objMVD.BuildingName = objB.Name;
			objMVD.BuildingNo = objB.Building_No__c;
			objMVD.AvailableUnits = mapLCount.containsKey(objB.Id) ? (mapLCount.get(objB.Id)+'') : '0';
			lstMVD.add(objMVD);
		}
		if(!lstMVD.isEmpty())
			buildingJSONString = JSON.serialize(lstMVD);
		system.debug('JSONString is : '+buildingJSONString);
	}
	
	public class MapViewDetails{
		public string BuildingName {get;set;}
		public string BuildingNo {get;set;}
		public string AvailableUnits {get;set;}
	}
}