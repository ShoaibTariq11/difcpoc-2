/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ContactTriggerHandlerTest {
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        list<Account> insertNewAccounts = new list<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
         //create contact
        list<Contact> insertNewContacts = new list<Contact>();
        insertNewContacts = OB_TestDataFactory.createContacts(1, insertNewAccounts);
        insertNewContacts[0].Send_Portal_Login_Link__c = 'Yes';
        for(RecordType RC:[Select Id from RecordType where DeveloperName='Portal_User']){
        	insertNewContacts[0].RecordTypeId = RC.Id;
        }
        insert insertNewContacts;
        
        insertNewAccounts = new list<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        insertNewContacts[0].AccountId = insertNewAccounts[0].Id;
		update insertNewContacts;
        
    }
    static testMethod void myUnitTest2() {
        // TO DO: implement unit test
        list<Account> insertNewAccounts = new list<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
         //create contact
        list<Contact> insertNewContacts = new list<Contact>();
        insertNewContacts = OB_TestDataFactory.createContacts(1, insertNewAccounts);
        insertNewContacts[0].Send_Portal_Login_Link__c = 'No';
        for(RecordType RC:[Select Id from RecordType where DeveloperName='Portal_User']){
        	insertNewContacts[0].RecordTypeId = RC.Id;
        }
        insert insertNewContacts;
        
        insertNewContacts[0].Send_Portal_Login_Link__c = 'Yes';
        update insertNewContacts;
        
        insertNewContacts = OB_TestDataFactory.createContacts(1, insertNewAccounts);
        insertNewContacts[0].Send_Portal_Login_Link__c = 'No';
        insertNewContacts[0].OB_Self_Registered_Contact__c = true;
        for(RecordType RC:[Select Id from RecordType where DeveloperName='Portal_User' and sobjectType='Contact' and IsActive=true]){
        	insertNewContacts[0].RecordTypeId = RC.Id;
        }
        insert insertNewContacts;
    }
    static testMethod void myUnitTest3() {
        // TO DO: implement unit test
        list<Account> insertNewAccounts = new list<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
         //create contact
        list<Contact> insertNewContacts = new list<Contact>();
        insertNewContacts = OB_TestDataFactory.createContacts(1, insertNewAccounts);
        insertNewContacts[0].important__c  = true;
        for(RecordType RC:[Select Id from RecordType where DeveloperName='Business_Development' and sobjectType='Contact' and IsActive=true]){
        	insertNewContacts[0].RecordTypeId = RC.Id;
        }
        insert insertNewContacts;
    }
}