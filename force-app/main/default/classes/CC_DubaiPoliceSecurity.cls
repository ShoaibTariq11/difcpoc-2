/*
* push information (Contact, document and Amendment record) to dubai police
**/
global without sharing class CC_DubaiPoliceSecurity implements HexaBPM.iCustomCodeExecutable {
  
 
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
      string strResult = 'Success';
       Boolean isSecondCall = false; 
        //HexaBPM__Step__c step = new HexaBPM__Step__c(Id=stp.Id);
        system.debug('=======step id========'+stp.Id);
        system.debug('=======service request id========'+stp.HexaBPM__SR__c);
      
        if(String.isNotBlank(stp.Id) ){
        	
        	String summary = stp.Step_Template_Code__c;
        	system.debug('====summary======'+summary);
        	//if(String.IsBlank(stp.HexaBPM__SR__c)){
        		for(HexaBPM__Step__c st2: [Select Id,HexaBPM__SR__c,HexaBPM__Summary__c,Step_Template_Code__c From HexaBPM__Step__c Where Id =: stp.Id]){
        			stp.HexaBPM__SR__c = st2.HexaBPM__SR__c;
        			summary = st2.Step_Template_Code__c;
        		}
        	//}
        	system.debug('=======inside step query==summary======'+summary);
        	if(String.IsNotBlank(stp.HexaBPM__SR__c) && String.ISnotBlank(summary) && summary.equalsIgnorecase(Label.SECURITY_REVIEW_STEP)){
        	system.debug('====123===inside step query==summary======'+summary);
	        	List<HexaBPM__Step__c>	stepList = [SELECT HexaBPM__SR_Step__c,HexaBPM__SR__c,
	                          						HexaBPM__Summary__c,Step_Template_Code__c
	                            					FROM HexaBPM__Step__c 
	                            					Where HexaBPM__SR__c =: stp.HexaBPM__SR__c
	                            					AND (Step_Template_Code__c =: Label.SECURITY_REVIEW_STEP OR Step_Template_Code__c =: Label.SECURITY_RETRIGGER_STEP)
	                            					Limit 2];
	            system.debug('---124--------'+stepList.size());                					
	           if(stepList != null && stepList.size() > 1){
	           	system.debug('=======inside step query===12=====');
	           	isSecondCall=true;
	           }
        	}
        }
        system.debug('=======isSecondCall========'+isSecondCall);
        //HexaBPM__Service_Request__c objSR = new HexaBPM__Service_Request__c(Id=stp.HexaBPM__SR__c);
        
        
        try{
          if(String.isNotBlank(stp.Id) && !isSecondCall){
          	
            stepHealperMethod(stp.Id);
          }
        }catch(Exception e){
            strResult = e.getMessage();
            insert LogDetails.CreateLog(null, 'CC_DubaiPoliceSecurity : EvaluateCustomCode', 'Line Number : '+e.getLineNumber()+'\nException is : '+strResult);
        }
        
        system.debug('=====strResult===='+strResult);
      return strResult;
    }
    
    private final static String USERNAME = Label.DYFC_API_User_Name;   //This Variable used to Store API user User Name
    private final static String PASSWORD = Label.DYFC_Password;        //This Variable used to Store API user PASSWORD
    private final static String CONSUMER_KEY = Label.DIFC_Consumer_KEY; //This Variable used to Store CONSUMER_KEY
    private final static String CONSUMER_SECRET = Label.DIFC_Consumer_Secret; //This Variable used to Store CONSUMER_SECRET
  
     @future(callout=true)
    public static void stepHealperMethod(String stepID){
        
        try{
            system.debug('!!==stepID==>'+stepID);
            HexaBPM__Step__c stepRecord = new HexaBPM__Step__c();
            stepRecord = [SELECT HexaBPM__SR_Step__c,HexaBPM__SR_Step__r.Name,HexaBPM__Step_Status__c,
                          HexaBPM__Summary__c,Step_Template_Code__c
                            FROM HexaBPM__Step__c where Id=:stepID];
            system.debug('!!==stepRecord==>'+stepRecord);
            String AmendmentBody = OB_IssuanceJSON.issuanceJSONBODY(stepID);
            system.debug('!!==AmendmentBody==>'+AmendmentBody);
            Blob headerValue = Blob.valueOf(USERNAME + ':' + PASSWORD);
            String authorizationHeader = Label.DIFC_AuthorizationHeader+'&client_secret='+CONSUMER_SECRET+'&username='+USERNAME+'&password='+PASSWORD;
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(authorizationHeader);
            request.setMethod('POST');
            request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            system.debug('!!HttpResponse==request==>'+request);
            HttpResponse response = http.send(request);
           
            system.debug('!!==response==>'+response);
            
            if(response.getStatusCode() == 200){
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
                Object BearerCode1 = results.get('access_token');
                String BearerCode = String.valueOf(BearerCode1);
                BearerCode = 'Bearer '+BearerCode;
                
                System.debug('!!==Bearer==>'+BearerCode);
                 
                String authorizationHeaderIssuence ;
                System.debug('!!==out check 1==>'+stepRecord.Step_Template_Code__c);
                System.debug('!!==out check 2==>'+stepRecord.HexaBPM__Step_Status__c);
                if((stepRecord.Step_Template_Code__c == Label.SECURITY_REVIEW_STEP ||
                stepRecord.Step_Template_Code__c == Label.SECURITY_RETRIGGER_STEP)&& 
                   stepRecord.HexaBPM__Step_Status__c ==Label.SECURITY_IN_PROGRESS){
                    authorizationHeaderIssuence = Label.NewDIFCPoliceIssuanceURL;
                       System.debug('!!==inside authorization==>'+authorizationHeaderIssuence);
                }
                else if(stepRecord.Step_Template_Code__c == Label.Security_Internal_Assessment_Step && 
                        stepRecord.HexaBPM__Step_Status__c == Label.Re_Submit_Security){
                    authorizationHeaderIssuence = Label.UpdateDIFCIssuance;
                }
                
                System.debug('!!==out authorization==>'+authorizationHeaderIssuence);
                Http httpIssuence = new Http();
                HttpRequest requesthttpIssuence = new HttpRequest();
                requesthttpIssuence.setEndpoint(authorizationHeaderIssuence);
                requesthttpIssuence.setMethod('POST');
                requesthttpIssuence.setHeader('Content-Type', 'application/json');
                requesthttpIssuence.setHeader('Authorization',BearerCode);
                requesthttpIssuence.setBody(AmendmentBody);  
                system.debug('!!==AmendmentBody==>'+AmendmentBody);
                HttpResponse finalresponse = httpIssuence.send(requesthttpIssuence);
                system.debug('!!==finalresponse==>'+finalresponse);
                system.debug('!!==finalresponse=body=>'+finalresponse.getBody());
                if(finalresponse.getStatusCode() == 200){
                    String requestID = OB_IssuanceJSON.requestId;
                    system.debug('======requestID======'+requestID);
                    Set<Id> setAttachmentId = new set<Id>();
                    setAttachmentId = OB_IssuanceJSON.setAttachIds;
                    
                    Map<Id,Id> setAttachIdsSrddocIdMap = new Map<Id,Id>();
                    setAttachIdsSrddocIdMap = OB_IssuanceJSON.setAttachIdsSrddocIdMap;

                    system.debug('======setAttachmentId======'+setAttachmentId);
                    system.debug('======setAttachIdsSrddocIdMap======'+setAttachIdsSrddocIdMap);
                    //SELECT Id,Body,Name,ParentID
                    for(ContentVersion eachAttachment:[SELECT Id,ContentDocumentId,VersionData,Title,
                    									PathOnClient 
                                                     FROM ContentVersion 
                                                    Where ContentDocumentId IN:setAttachIdsSrddocIdMap.keyset()]){
                    
                    	
                        Blob file_body=eachAttachment.VersionData;
                        String file_name=eachAttachment.PathOnClient;
                        String ParentID=setAttachIdsSrddocIdMap.get(eachAttachment.ContentDocumentId);
                        system.debug('======ParentID======'+ParentID);
                        system.debug('======file_name======'+file_name);
                        String reqEndPoint=Label.DIFCAttachmentURL+requestID+'/'+ParentID;
                        string Authorization=BearerCode;
                        String boundary = Label.DIFCPoliceImageBoundry;
                        String header = '--'+boundary+'\nContent-Disposition: form-data; name="file"; filename="'+file_name+'";\nContent-Type: application/octet-stream';
                        String footer = '--'+boundary+'--';             
                        String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
                        
                        while(headerEncoded.endsWith('='))
                          {
                           header+=' ';
                           headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
                          }
                          
                          String bodyEncoded = EncodingUtil.base64Encode(file_body);
                          Blob bodyBlob = null;
                          String last4Bytes = bodyEncoded.substring(bodyEncoded.length()-4,bodyEncoded.length());
                                if(last4Bytes.endsWith('==')) {
                           
                            last4Bytes = last4Bytes.substring(0,2) + '0K';
                            bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
                            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
                            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);
                          } else if(last4Bytes.endsWith('=')) {
       
                            last4Bytes = last4Bytes.substring(0,3) + 'N';
                            bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
                            footer = '\n' + footer;
                            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
                            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);              
                          } else {
                            footer = '\r\n' + footer;
                            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
                            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);  
                          }

                          HttpRequest req = new HttpRequest();
                          req.setHeader('Content-Type','multipart/form-data; boundary='+boundary);
                          req.setMethod('POST');
                          req.setHeader('Authorization',Authorization);
                          req.setEndpoint(reqEndPoint);
                          req.setBodyAsBlob(bodyBlob);
                          req.setTimeout(120000);
                          Http httpAttachment = new Http();
                          HTTPResponse httpResponse1 = http.send(req);
                         
                         system.debug('====org=123=='+httpResponse1.getStatusCode());
                         system.debug('====org=1234=='+httpResponse1.getBody());
                         
                          if(httpResponse1.getStatusCode() == 200){
                            Http httpfinalResult = new Http();
                            HttpRequest requesthttpSubmit = new HttpRequest();
                            String endPoint = Label.DIFCPoliceWorkflowURL+requestID;
                            requesthttpSubmit.setEndpoint(endPoint);
                            requesthttpSubmit.setMethod('POST');
                            requesthttpSubmit.setHeader('Content-Type', 'application/json');
                            requesthttpSubmit.setHeader('Authorization',BearerCode);
                            List<Organization> org = [SELECT Id,IsSandbox FROM Organization Where Id =: UserInfo.getOrganizationId() LIMIT 1];
                            system.debug('====org==='+org);
                            String IsRun = system.Label.IS_DUBAIPOLICERUN;
                            
                            if(org != null && org.size() != 0 && !org[0].IsSandbox && String.IsNotBlank(IsRun) && IsRun.equalsIgnorecase('true')){
                            	system.debug('===inside=production==');
                           		HttpResponse finalresponse1 = httpfinalResult.send(requesthttpSubmit);
                            }
                           
                          }
                    }
                }
                system.debug('====AmendmentBody==='+AmendmentBody);
                if(String.isNotBlank(AmendmentBody)){
                    HexaBPM__Step__c eachStep = new HexaBPM__Step__c();
                    eachStep = [SELECT Id,DIFC_Security_Email_Request__c,DIFC_Security_Email_Response__c 
                                FROM HexaBPM__Step__c where ID=:stepID];
                    eachStep.DIFC_Security_Email_Request__c =  AmendmentBody;
                    eachStep.DIFC_Security_Email_Response__c = finalresponse.getBody();
                    UPDATE eachStep;
                }
            }
        }
       catch(Exception e){
          system.debug('$$$$$'+e.getMessage());
          string errorresult = e.getMessage();
          insert LogDetails.CreateLog(null, 'CC_DubaiPoliceSecurity : stepHealperMethod', 'Line Number : '+e.getLineNumber()+'\nException is : '+errorresult);
       }
    }
    
}