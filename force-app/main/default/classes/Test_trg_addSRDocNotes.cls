/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_trg_addSRDocNotes {

    static testMethod void myUnitTest() {
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@dmcc.com';
        insert objContact;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'New Lease';
        objTemplate.SR_RecordType_API_Name__c = 'Client_Information';
        objTemplate.Active__c = true;
        objTemplate.Available_for_menu__c = true;
        insert objTemplate;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Test Doc Master';
        insert objDocMaster;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.SR_Template__c = objTemplate.Id;
        insert objSR;
      	
      	Status__c objStatus = new Status__c();
	    objStatus.Name = 'Closed';
	    objStatus.Type__c = 'Closed';
	    objStatus.Code__c = 'Closed';
	    insert objStatus;
    
      	Step_Template__c  stpTemp = new Step_Template__c ();
		stpTemp.Name ='Entry Permit With Airport Entry Stamp Upload';
		stpTemp.CODE__C  ='Entry Permit With Airport Entry Stamp Upload';
		stpTemp.Step_RecordType_API_Name__c ='General';
		insert stpTemp;
      
        Step__c stp = new Step__c();
        stp.SR__c = objSR.Id;
        stp.Step_Notes__c = 'Sample step Note';
        stp.step_template__c = stpTemp.Id;
        insert stp;
      
        stp.Step_Notes__c = 'Durga Added new comments';
        update stp;
      
        Test.startTest();
      
        SR_Doc__c objSRDoc = new SR_Doc__c();
        objSRDoc.Service_Request__c = objSR.Id;
        objSRDoc.Is_Not_Required__c = false;
        objSRDoc.Group_No__c = 1;
        
        //insert objSRDoc;
        
        SR_Doc__c objSRDoc1 = new SR_Doc__c();
        objSRDoc1.Service_Request__c = objSR.Id;
        objSRDoc1.Is_Not_Required__c = false;
        objSRDoc1.Group_No__c = 1;
        
        //insert objSRDoc1;
        
        SR_Doc__c objSRDoc2 = new SR_Doc__c();
        objSRDoc2.Service_Request__c = objSR.Id;
        objSRDoc2.Is_Not_Required__c = false;
        objSRDoc2.Group_No__c = 2;
        
        //insert objSRDoc2;
        
        insert new List<Sr_doc__C>{objSRDoc,objSRDoc1,objSRDoc2};
      
        blob AttachData = blob.valueof('Sample data');
        Attachment attch = new Attachment();
        attch.Name = 'sAMPLE aTTACH';
        attch.Body = AttachData;
        attch.ParentId = objSRDoc.Id;
        attch.ContentType = 'txt';
        insert attch;
      
        objSRDoc.Is_Not_Required__c = false;
        objSRDoc.Status__c = 'Re-Upload Doc';
        objSRDoc.Customer_Comments__c = 'Doc Uploaded';
        objSRDoc.DIFC_Comments__c = 'Invalid Docs';
        update objSRDoc;
      
        delete objSRDoc;
        
        Test.stopTest();
    }
    
    
     static testmethod void MyUnitTestMethod1(){
        
        
       
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@dmcc.com';
        insert objContact;
        
         Map<string,RecordType> recTypInfo = new Map<string,RecordType>();
        for(RecordType rcd : [select id,DeveloperName from recordType where SobjectType='Service_Request__c']){
            recTypInfo.put(rcd.DeveloperName,rcd);
        }        
        
        Expire_ELicenses__c ee = new Expire_ELicenses__c();
        ee.Service_Request_Record_Type__c = 'Change_of_Address_Details;Change_Add_or_Remove_Authorized_Signatory;Change_of_Entity_Name;Change_of_Business_Activity;Application_of_Registration';
        ee.SR_Doc_to_Expire__c = 'E-Commercial License-ATM;E - NPIO License;E-Commercial License';
        insert ee;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        //objSR.SR_Template__c = objTemplate.Id;
        objSR.RecordTypeId = recTypInfo.get('Change_of_Entity_Name').id;
        insert objSR;
        
        Status__c objStatus = new Status__c();
	    objStatus.Name = 'Closed';
	    objStatus.Type__c = 'Closed';
	    objStatus.Code__c = 'Closed';
	    insert objStatus;
        
        Step_Template__c  stpTemp = new Step_Template__c ();
		stpTemp.Name ='Entry Permit With Airport Entry Stamp Upload';
		stpTemp.CODE__C  ='Entry Permit With Airport Entry Stamp Upload';
		stpTemp.Step_RecordType_API_Name__c ='General';
		insert stpTemp;
		
        Step__c stp = new Step__c();
        stp.SR__c = objSR.Id;
        stp.Step_Notes__c = 'Sample step Note';
        stp.step_template__c = stpTemp.id;
        insert stp;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Test Doc Master';
        insert objDocMaster;
        
        
        List<SR_Doc__c> srdocList = new List<SR_doc__c>();
        SR_Doc__c objSRDoc = new SR_Doc__c();
        objSRDoc.Service_Request__c = objSR.Id;
        objSRDoc.Is_Not_Required__c = false;
        objSRDoc.Name = 'E-Commercial License-ATM';
        objSRDoc.Group_No__c = 1;
        ObjSRDoc.Customer__c = objAccount.id;
        srdocList.add(objSRDoc);
        SR_Doc__c objSRDoc2 = new SR_Doc__c();
        objSRDoc2.Service_Request__c = objSR.Id;
        objSRDoc2.Is_Not_Required__c = false;
        objSRDoc2.Name = 'E-Commercial License';
        objSRDoc2.Group_No__c = 1;
        ObjSRDoc2.Customer__c = objAccount.id;
        srdocList.add(objSRDoc2);
        
        objSRDoc = new SR_Doc__c(); 
        objSRDoc.Service_Request__c = objSR.Id;
        objSRDoc.Is_Not_Required__c = false;
        objSRDoc.Name = 'Contractor Authorisation';
        objSRDoc.Group_No__c = 1;
        ObjSRDoc.Customer__c = objAccount.id;
        srdocList.add(objSRDoc);
        
        insert srdocList;
        
        
        
        
        
        srdocList[0].DOc_ID__c = 'isadjsaduhasd';
        srdocList[0].Customer_Comments__c = 'test';
        srdocList[1].DOc_ID__c = 'isadjsadasauhasd';
        srdocList[1].DIFC_Comments__c= 'test';
        srdocList[1].DIFC_Comments__c= 'test';        
        
        Update srdocList;
        
        
        
        
        
        
        
        
        
        
    }
}