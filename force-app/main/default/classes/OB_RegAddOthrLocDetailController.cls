/**
*Author : Merul Shah
*Description : This class takes the form/Details of Ind/BC Shareholder and save them to SF as amendments..
**/
public without sharing class OB_RegAddOthrLocDetailController
{
    
    
    @AuraEnabled   
    public static ResponseWrapper  onAmedSaveDB(String reqWrapPram)
    {
        //reqest wrpper
        OB_RegAddOthrLocDetailController.RequestWrapper reqWrap = (OB_RegAddOthrLocDetailController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_RegAddOthrLocDetailController.RequestWrapper.class);
        
        //response wrpper
        OB_RegAddOthrLocDetailController.ResponseWrapper respWrap = new OB_RegAddOthrLocDetailController.ResponseWrapper();
        HexaBPM_Amendment__c amedObj = reqWrap.amedWrap.amedObj;
        //amedObj.Is_Director__c  = true;
        Boolean isDuplicate = false;
        String srId = amedObj.ServiceRequest__c;
        String amendID = amedObj.Id;
        
        String RecordTypename = amedObj.RecordType.DeveloperName;
        
             
             //if(reqWrap.amedWrap.amedObj.Passport_No__c != NULL && reqWrap.amedWrap.amedObj.Nationality_list__c != NULL)
             //{
                 /*
                 if(String.IsNotBlank(RecordTypename) && RecordTypename.equalsIgnoreCase('Individual') )
                 {
                        String passport = String.isNotBlank(reqWrap.amedWrap.amedObj.Passport_No__c)?reqWrap.amedWrap.amedObj.Passport_No__c:'';
                        String nationality = String.IsNotBlank(reqWrap.amedWrap.amedObj.Nationality_list__c)?reqWrap.amedWrap.amedObj.Nationality_list__c.toLowerCase():'';
                        String passportNationlity = (String.isNotBlank(passport)?passport+'_':'')+(String.IsNotBlank(nationality)?nationality:'');
                        system.debug('===passportNationlity=='+passportNationlity);
                        
                        for(HexaBPM_Amendment__c amd : [Select ID,First_Name__c,Passport_No__c,Nationality_list__c,
                                        recordtype.developername,Role__c 
                                        From HexaBPM_Amendment__c
                                        Where ServiceRequest__c =: srId
                                        AND (recordtype.developername != 'Body_Corporate' AND Id !=: amendID)
                                        AND Passport_No__c =: passport
                                        AND Nationality_list__c =: nationality
                                        Limit 1 ]){
                            system.debug('===duplicate==');
                            isDuplicate = true;
                            break;
                        }
                   }
                   else if(String.IsNotBlank(RecordTypename) && RecordTypename.equalsIgnoreCase('Body_Corporate'))
                   {
                        String registrationNo = String.isNotBlank(reqWrap.amedWrap.amedObj.Registration_No__c)?reqWrap.amedWrap.amedObj.Registration_No__c:'';
                        system.debug(amendID+'====registrationNo====='+registrationNo);
                        for(HexaBPM_Amendment__c amd : [Select ID,Registration_No__c,
                                        recordtype.developername,Role__c 
                                        From HexaBPM_Amendment__c
                                        Where ServiceRequest__c =: srId
                                        AND (recordtype.developername = 'Body_Corporate' AND Id !=: amendID)
                                        AND Registration_No__c =: registrationNo
                                        Limit 1 ])
                        {
                            system.debug('==registrationNo=duplicate==');
                            isDuplicate = true;
                            break;
                        }
                    }
             }
             
                
              system.debug(isDuplicate+'====isDuplicate=amendID===='+amendID);
              // duplicate record throw an error 
              
              if(isDuplicate)
              {
                respWrap.errorMessage = 'This person/corporate already exist';
              }
              else
              {
			*/
        
        
        			amedObj.Role__c = 'Operating Location';
                    system.debug('######## amedObj.Role__c '+amedObj.Role__c);
                    upsert amedObj;
                    respWrap.amedWrap = reqWrap.amedWrap;
              
             // }
        
              OB_RiskMatrixHelper.CalculateRisk(srId);
        
        return respWrap;
    }
    
    
   
    
    
    
   
    
    public class AmendmentWrapper 
    {
        @AuraEnabled public HexaBPM_Amendment__c amedObj { get; set; }
        //@AuraEnabled public Id amedID { get; set; }
        @AuraEnabled public Boolean isIndividual { get; set; }
        @AuraEnabled public Boolean isBodyCorporate { get; set; }
        //@AuraEnabled public String lookupLabel { get; set; }
        
        //lookupLabel
        
        public AmendmentWrapper()
        {}
    }
    
    
    public class RequestWrapper 
    {
        //@AuraEnabled public Id flowId { get; set; }
        //@AuraEnabled public Id pageId {get;set;}
        @AuraEnabled public Id srId { get; set; }
        @AuraEnabled public AmendmentWrapper amedWrap{get;set;}
       
      
        public RequestWrapper()
        {}
    }

    public class ResponseWrapper 
    {
        //@AuraEnabled public SRWrapper  srWrap{get;set;}
        @AuraEnabled public AmendmentWrapper amedWrap{get;set;}
        //@AuraEnabled public String errorMessage {get;set;}
        
        
        public ResponseWrapper()
        {}
    }

}