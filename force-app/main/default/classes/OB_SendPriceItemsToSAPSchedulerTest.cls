@isTest
public without sharing class OB_SendPriceItemsToSAPSchedulerTest {
    
    @isTest
    private static void testSendPriceItemsToSAPScheduler()
    {  
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;

        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insert insertNewContacts;

        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle','New_User_Registration'});
        insert createSRTemplateList;

       //create SR status
       list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
       listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
       insert listSRStatus;


       List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
       insertNewSRs = OB_TestDataFactory.createSR(1, new List<string> {'New_User_Registration', 'In_Principle'}, insertNewAccounts, 
                                                                        new List<string>{'Non - financial','Retail'}, 
                                                                        new List<string>{'General Partner for a Limited Partnership Fund','Services'}, 
                                                                        new List<string>{'Partnership','Company'}, 
                                                                        new List<string>{'Recognized Limited Partnership (RLP)','Recognized Company'});

        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[2].id;
        insertNewSRs[0].HexaBPM__External_SR_Status__c = listSRStatus[2].id;
        insertNewSRs[0].recordtypeid = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','In_Principle');
        insertNewSRs[0].HexaBPM__Auto_Submit__c = true;
        insert insertNewSRs;

        List<Product2>  prodList = OB_TestDataFactory.createProduct(new list<String>{'TEST'});
        insert prodList;

        HexaBPM__Pricing_Line__c lineItem = OB_TestDataFactory.createSRPriceItem();
        insert lineItem;
        
        List<HexaBPM__SR_Price_Item__c> pricList = OB_TestDataFactory.createSRPriceItem(prodList,insertNewSRs[0].Id,lineItem.Id);
        pricList[0].HexaBPM__Status__c='Consumed';
        pricList[0].HexaBPM__Price__c=2;
        pricList[0].HexaBPM__ServiceRequest__c=insertNewSRs[0].Id;
        insert pricList;


        test.startTest();

        OB_SendPriceItemsToSAPScheduler sh1 = new OB_SendPriceItemsToSAPScheduler();
        //DataBase.executeBatch(obj);
        String sch = '0 0 0/24 ? * * *';
        system.schedule('Test sch', sch, sh1);
        test.stopTest();
                                                                          

        
    }
}