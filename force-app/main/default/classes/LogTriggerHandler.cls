/***********************************************************************************
 *  Author   : shoaib tariq
 *  Company  : Tech Carrot
 *  Date     : 19-01-2020
  --------------------------------------------------------------------------------------------------------------------------
 Description : Thic class handels the error exception  
 ---------------------------------------------------------------------------------------------------------------------------
 Change Description
 Version      ModifiedBy            Date                Description
 V1.0         shoaib tariq        19-01-2020          create failed shipments
 ************************************************************************************/
public class LogTriggerHandler  {
   /*
    * Method to call logQueueableApex class to create failed shipment
    */ 
   public static void CreateFailedShipment(list <id> objId ){
      System.enqueueJob(new logQueueableApex(objId[0]));   
   }
   //Queueable class 
   public class logQueueableApex implements Queueable ,Database.AllowsCallouts {
    Id stepId ;
    public logQueueableApex ( Id stepId) {
    	this.stepId  = stepId;
    }
    public void execute(QueueableContext context) {
        if(!Test.isRunningTest()) CC_CourierCls.CreateShipment(stepId);  
    }
  }
}