@isTest
global class NorBlocOTPValidationControllerMockTest implements HttpCalloutMock {

    global HTTPResponse respond(HTTPRequest req){
		
		HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        res.setBody('{"Authorization":"3322233","email":"test@test.com","code":"111444","expires_in":600,"scope":"customer.manage","setMethod":"Post","grant_type":"authorization_code"}');
        res.setStatusCode(200);
        system.debug('@@!!@@##'+res);
        return res;
	}
}