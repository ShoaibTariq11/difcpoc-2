global class UpdatePendingBatch implements Database.Batchable<SObject>, Database.Stateful{  


    global Database.QueryLocator start(Database.BatchableContext BC){
    
    
      return Database.getQueryLocator('Select Id,service_request__r.customer__c from pending__c');
    
    }
    
      global void execute(Database.BatchableContext BC, List<pending__c> pending) {
      
      List<pending__c> pendings = new List<pending__c>();
      
          for(pending__c p : pending){
            pending__c pndg= new pending__c();
            pndg.id=p.id;
            pndg.account__c = p.service_request__r.customer__c;
            pendings.add(pndg);
        }
        if(pendings.size()>0){
           update pendings;
        }
      
      }

    global void finish(Database.BatchableContext BC) {}




}