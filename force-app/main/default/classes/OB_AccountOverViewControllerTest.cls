@isTest
public class OB_AccountOverViewControllerTest {
    
    
    public static testmethod void test1(){
    
     // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
    
    
        //crreate contact
        Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
                insert con; 
    
    test.startTest();
        	
        	
            Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;      
                 
                          
                User user = new User(alias = 'test123', email='test123@noemail.com',
                        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                        localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                        ContactId = con.Id,
                        timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
               
                insert user;
            system.runAs(user){
            
            
            
            OB_AccountOverViewController.RespondWrap restWrapTest = new OB_AccountOverViewController.RespondWrap();
            
            restWrapTest = OB_AccountOverViewController.getConDetails();
        }
        
        test.stopTest();
    }

}