/**********************************************************
Class Name: AppointmentSchedulerBatch to execute the AppointmentCreationUtilityCls
Description: Use this class to get the create the appointment every 5 mins
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By       Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    18-09-2019  Selva    #7365  
********************************************************/
global class AppointmentSchedulerBatchScheduler implements Schedulable { 
    
    global void execute(SchedulableContext SC) {
        AppointmentSchedulerBatch obj = new AppointmentSchedulerBatch();
        database.executebatch(obj,1);
        //String schTime = '0 0 5 * * * ?';
        //Id jobId = System.schedule('GS Appointment Creation',schTime,new AppointmentSchedulerBatchScheduler());
    }
}