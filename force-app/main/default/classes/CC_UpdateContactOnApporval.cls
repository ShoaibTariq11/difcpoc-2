/*
    Author      : Merul Shah
    Date        : 10-March-2020
    Description : This updates the contact OB_Block_Homepage__c
    --------------------------------------------------------------------------------------
*/

global without sharing class CC_UpdateContactOnApporval implements HexaBPM.iCustomCodeExecutable{
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp)
    {
        system.debug('@@@@@@@@@ entered into CC_UpdateContactOnApporval');
        string strResult = 'Success';
        if(stp!=null && stp.HexaBPM__SR__c!=null){
            try{
                if(stp.HexaBPM__SR__r.HexaBPM__Contact__c!=null){
                    Contact con = new Contact( id = stp.HexaBPM__SR__r.HexaBPM__Contact__c);
                    con.OB_Block_Homepage__c = false;
                    update con;
                }

            }catch(DMLException e){
                /*
                    string DMLError = e.getdmlMessage(0)+'';
                    if(DMLError==null){
                        DMLError = e.getMessage()+'';
                    }
                    strResult = DMLError;
             	*/
				strResult =  e.getdmlMessage(0)+'';   
            }
        }
        return strResult;
    }   
}