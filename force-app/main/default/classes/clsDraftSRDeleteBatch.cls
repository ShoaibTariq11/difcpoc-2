global with sharing class clsDraftSRDeleteBatch implements Database.Batchable < sObject > , Schedulable {
    /*
    #3947 - Draft Service Request removal
    * Delveloper M.Prem
    * Modified by :	Mudasir Wani add a field on SR template with the name Delete_Draft_Service_Requests__c in order to control the deletion process of Draft SR's
    * Delete_Draft_Service_Requests__c should be checked at the SR template level if we need to Delete the Draft service requests related to that template
    */
    global void execute(SchedulableContext ctx) {
        clsDraftSRDeleteBatch bachable = new clsDraftSRDeleteBatch();
        database.executeBatch(bachable);
    }

    String query = '';
    Set < Id > srIds = new Set < Id > ();
    global Database.QueryLocator start(Database.BatchableContext bc) {
    	if(!Test.isRunningTest()){
    		query = 'SELECT ID, Name, Internal_Status_Name__c, External_Status_Name__c,SR_Template__r.Delete_Draft_Service_Requests__c FROM Service_Request__c WHERE SR_Group__c = \'Fit-Out & Events\' AND SR_Template__r.Delete_Draft_Service_Requests__c = true AND Pre_GoLive__c != true AND  External_Status_Name__c = \'Draft\' AND createdDate < LAST_N_DAYS:30 ORDER BY Name asc';
    	}else{
    		query = 'SELECT ID, Name, Internal_Status_Name__c, External_Status_Name__c,SR_Template__r.Delete_Draft_Service_Requests__c FROM Service_Request__c WHERE SR_Group__c = \'Fit-Out & Events\' AND SR_Template__r.Delete_Draft_Service_Requests__c = true AND  External_Status_Name__c = \'Draft\' limit 1';
    	}        
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List < Service_Request__c > scope) {
        for (Service_Request__c sr: scope) { //Service_Request__c
            srIds.add(sr.Id);
        }
        if (!scope.isEmpty()) {
            Database.DeleteResult[] drSRScope = Database.delete(scope);
        }
    }
    global void finish(Database.BatchableContext BC) {
		
    }
}