/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_StepTrigger {
	
	@testSetup static void setTestData() {
    	
    	Test_FitoutServiceRequestUtils.setFitoutBusinessHours();
	}

    static testMethod void myUnitTest() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        
        string conRT;
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
                 conRT = objRT.Id;
           
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.Phone = '1234567890';
        objContact.RecordTypeId = conRT;
        insert objContact;
      
      
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator' limit 1];  //Customer Community Login User Custom
        User u = new User(Alias = 'standt', Email='tesclasst@difc.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='tesclasstuat1@difc.com');
        u.Community_User_Role__c = 'Admin;Company Services Approver';    
        u.IsActive = true;
        insert u;
          
        Product2 objProduct = new Product2();
        objProduct.Name = 'DIFC Product';
        objProduct.IsActive = true;
        insert objProduct;
          
        Pricebook2 objPricebook = new Pricebook2();
        objPricebook.Name = 'Test Price Book';
        objPricebook.IsActive = true;
        objPricebook.Description = 'Test Description';
        insert objPricebook;
          
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Application_of_Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Menutext__c = 'Application_of_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        objTemplate.Refund_Item__c = objProduct.Id;
        insert objTemplate;
            
        Pricing_Line__c objPricingLine = new Pricing_Line__c();
        objPricingLine.Product__c = objProduct.Id;
        objPricingLine.Active__c = true;
        objPricingLine.Priority__c = 12;
        objPricingLine.Price_Line_Condition__c = '(1 OR 2)';
        insert objPricingLine;
        
        Dated_Pricing__c objDP = new Dated_Pricing__c();
        objDP.Pricing_Line__c = objPricingLine.Id;
        objDP.Unit_Price__c = 1200;
        objDP.Appllication_Type__c = 'Add';
        objDP.Date_From__c = system.today().addYears(-2);
        objDP.Date_To__c = system.today().addYears(2);
        objDP.Use_List_Price__c = true;
        objDP.Cost__c=123.0;
        insert objDP;
            
        Pricing_Range__c objPriceRange = new Pricing_Range__c();
        objPriceRange.Dated_Pricing__c = objDP.Id;
        objPriceRange.Range_Start__c = 0;
        objPriceRange.Range_End__c = 2000;
        objPriceRange.Unit_Price__c = 1200;        
        insert objPriceRange;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'AR Third Party NOC';
        insert objDocMaster;
        
        list<Status__c> lstStatus = new list<Status__c>();
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Awaiting Approval';
        objStatus.Type__c = 'Start';
        objStatus.Code__c = 'AWAITING_APPROVAL';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Approved';
        objStatus.Type__c = 'Intermediate';
        objStatus.Code__c = 'APPROVED';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Rejected';
        objStatus.Type__c = 'End';
        objStatus.Code__c = 'PENDING_CUSTOMER_INPUTS';
        lstStatus.add(objStatus);
        
        insert lstStatus;
            
        SR_Steps__c objSRStep = new SR_Steps__c();
        objSRStep.SR_Template__c = objTemplate.Id;
        objSRStep.Step_RecordType_API_Name__c = 'General';
        //objSRStep.Step_Template__c = objStepType.Id; // Step Type
        objSRStep.Summary__c = 'Approve Request';
        objSRStep.Step_No__c = 1.0;
        objSRStep.Start_Status__c = lstStatus[0].Id; // Default Step Status
        objSRStep.Active__c = true;
        objSRStep.Sys_Create_Condition__c = true;
        objSRStep.Do_not_use_owner__c = false;
            
        for(QueueSobject obj : [select Id,QueueId from QueueSobject where SobjectType = 'Step__c' AND Queue.Name like '%Client%' limit 1]){
            objSRStep.OwnerId = obj.QueueId;
        }
            
        insert objSRStep;
        
        list<SR_Template_Docs__c> lstSRTempDocs = new list<SR_Template_Docs__c>();
        
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.On_Submit__c = false;
        objTempDocs.Validated_at__c = objSRStep.Id;
        objTempDocs.Evaluated_at__c = objSRStep.Id;
        objTempDocs.Generate_Document__c = true;
        objTempDocs.Original_Sighted_at__c = objSRStep.Id;
        objTempDocs.Added_through_Code__c = false;
        lstSRTempDocs.add(objTempDocs);
            
        objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.On_Submit__c = false;
        objTempDocs.Generate_Document__c = true;
        objTempDocs.Original_Sighted_at__c = objSRStep.Id;
        objTempDocs.Added_through_Code__c = true;
        lstSRTempDocs.add(objTempDocs);
        
        insert lstSRTempDocs;
        
        SR_Template_Item__c objSRTempItem = new SR_Template_Item__c();
        objSRTempItem.Product__c = objProduct.Id;
        objSRTempItem.SR_Template__c = objTemplate.Id;
        objSRTempItem.On_Submit__c = false;
        objSRTempItem.Evaluated_at__c = objSRStep.Id;
        objSRTempItem.Consumed_at__c = objSRStep.Id;
        insert objSRTempItem;
        
        map<string,SR_Status__c> mapSRStatus = new map<string,SR_Status__c>();
        
        for(SR_Status__c obj : [select Id,Name,Code__c,Type__c from SR_Status__c where Code__c='CLOSED' OR Code__c='REJECTED' OR Code__c='AWAITING_APPROVAL' OR Code__c='DRAFT']){
          mapSRStatus.put(obj.Code__c,obj);
        }
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        SR_Status__c objSRStatus;
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Closed';
        objSRStatus.Code__c = 'CLOSED';
        objSRStatus.Type__c = 'End';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Rejected';
        objSRStatus.Code__c = 'REJECTED';
        objSRStatus.Type__c = 'Rejected';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Awaiting Approval';
        objSRStatus.Code__c = 'AWAITING_APPROVAL';
        objSRStatus.Type__c = 'Start';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Approved';
        objSRStatus.Code__c = 'APPROVED';
        objSRStatus.Type__c = 'Intermediate';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Submitted';
        objSRStatus.Code__c = 'SUBMITTED';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Draft';
        objSRStatus.Code__c = 'DRAFT';
        lstSRStatus.add(objSRStatus);
        
        list<Transition__c> lstTransitions = new list<Transition__c>();
        Transition__c objTransition = new Transition__c();
        objTransition.From__c = lstStatus[0].Id; // awaiting approval
        objTransition.To__c = lstStatus[1].Id; // approved
        lstTransitions.add(objTransition);
        
        objTransition = new Transition__c();
        objTransition.From__c = lstStatus[1].Id; // approved
        objTransition.To__c = lstStatus[2].Id; // Rejected
        lstTransitions.add(objTransition);
        
        insert lstTransitions;
        
        list<Step_Transition__c> lstStepTransitions = new list<Step_Transition__c>();
        Step_Transition__c objStepTran;
        objStepTran = new Step_Transition__c();
        objStepTran.SR_Step__c = objSRStep.Id;
        objStepTran.SR_Status_Internal__c = lstSRStatus[0].Id;
        objStepTran.SR_Status_External__c = lstSRStatus[0].Id;
        objStepTran.Transition__c = lstTransitions[0].Id;
        lstStepTransitions.add(objStepTran);
        
        objStepTran = new Step_Transition__c();
        objStepTran.SR_Step__c = objSRStep.Id;
        objStepTran.SR_Status_Internal__c = lstSRStatus[1].Id;
        objStepTran.SR_Status_External__c = lstSRStatus[1].Id;
        objStepTran.Transition__c = lstTransitions[1].Id;
        lstStepTransitions.add(objStepTran);
        
        //Step__c objStepParent = new Step__c();
        //insert objStepParent;
        
        test.startTest();
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Email__c = 'testsr@difc.com';
        objSR.Internal_SR_Status__c = lstSRStatus[5].Id; // changing the status to Draft
        objSR.External_SR_Status__c = lstSRStatus[5].Id; // changing the status to Draft
        objSR.Contact__c = objContact.Id;
        objSR.SR_Template__c = objTemplate.Id;
        insert objSR;
          
        list<Business_Rule__c> lstBRs = new list<Business_Rule__c>();
               
        Business_Rule__c objBR = new Business_Rule__c();
        objBR.Action_Type__c = 'Create Conditions';
        objBR.Execute_on_Insert__c = true;
        objBR.Execute_on_Update__c = true;
        objBR.SR_Steps__c = objSRStep.Id;
        objBR.Text_Condition__c = 'Step Status->1.0#=#Courier Collected & Verified#AND#Service_Request__c->Name#!=#NULL';
        lstBRs.add(objBR);
        
        objBR = new Business_Rule__c();
        objBR.Action_Type__c = 'Create Conditions';
        objBR.Execute_on_Insert__c = true;
        objBR.Execute_on_Update__c = true;
        objBR.SR_Steps__c = objSRStep.Id;
        objBR.Text_Condition__c = 'Step Status->1.0#=#Courier Collected & Verified#AND#Service_Request__c->Name#!=#NULL';
        lstBRs.add(objBR);
        
        objBR = new Business_Rule__c();
        objBR.Action_Type__c = 'Custom Conditions Actions';
        objBR.Execute_on_Insert__c = true;
        objBR.Execute_on_Update__c = true;
        objBR.SR_Steps__c = objSRStep.Id;
        objBR.Text_Condition__c = 'Step Status->10.0#=#Courier Collected & Verified#AND#Service_Request__c->Customer__c#!=#NULL';
        lstBRs.add(objBR);
          
        insert lstBRs;
        
        string QueueId;
    	
        for(QueueSobject obj : [select Id,QueueId from QueueSobject where SobjectType = 'Step__c' limit 1]){
            QueueId = obj.QueueId;
        }
          
        testStepUnitTest1(QueueId,objSR.Id,objSRStep.Id,lstStatus[0].Id,lstStatus[2].Id,lstSRTempDocs[0].Id);
        
        test.stopTest();
    }
    
    static testMethod void myUnitTest2() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        
        string conRT;
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
                 conRT = objRT.Id;
           
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.Phone = '1234567890';
        objContact.RecordTypeId = conRT;
        insert objContact;
      
      
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator' limit 1];  //Customer Community Login User Custom
        User u = new User(Alias = 'standt', Email='tesclasst@difc.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='tesclasstuat1@difc.com');
        u.Community_User_Role__c = 'Admin;Company Services Approver';    
        u.IsActive = true;
        insert u;
          
        Product2 objProduct = new Product2();
        objProduct.Name = 'DIFC Product';
        objProduct.IsActive = true;
        insert objProduct;
          
        Pricebook2 objPricebook = new Pricebook2();
        objPricebook.Name = 'Test Price Book';
        objPricebook.IsActive = true;
        objPricebook.Description = 'Test Description';
        insert objPricebook;
          
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Application_of_Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Menutext__c = 'Application_of_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Do_not_use_owner__c = false;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        objTemplate.Refund_Item__c = objProduct.Id;
        
        for(QueueSobject obj : [select Id,QueueId from QueueSobject where SobjectType = 'Step__c' AND Queue.Name like '%Client%' limit 1]){
            objTemplate.OwnerId = obj.QueueId;
        }
        
        insert objTemplate;
            
        Pricing_Line__c objPricingLine = new Pricing_Line__c();
        objPricingLine.Product__c = objProduct.Id;
        objPricingLine.Active__c = true;
        objPricingLine.Priority__c = 12;
        objPricingLine.Price_Line_Condition__c = '(1 OR 2)';
        insert objPricingLine;
        
        Dated_Pricing__c objDP = new Dated_Pricing__c();
        objDP.Pricing_Line__c = objPricingLine.Id;
        objDP.Unit_Price__c = 1200;
        objDP.Appllication_Type__c = 'Add';
        objDP.Date_From__c = system.today().addYears(-2);
        objDP.Date_To__c = system.today().addYears(2);
        objDP.Use_List_Price__c = true;
        objDP.Cost__c=123.0;
        insert objDP;
            
        Pricing_Range__c objPriceRange = new Pricing_Range__c();
        objPriceRange.Dated_Pricing__c = objDP.Id;
        objPriceRange.Range_Start__c = 0;
        objPriceRange.Range_End__c = 2000;
        objPriceRange.Unit_Price__c = 1200;        
        insert objPriceRange;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'AR Third Party NOC';
        insert objDocMaster;
        
        list<Status__c> lstStatus = new list<Status__c>();
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Awaiting Approval';
        objStatus.Type__c = 'Start';
        objStatus.Code__c = 'AWAITING_APPROVAL';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Approved';
        objStatus.Type__c = 'Intermediate';
        objStatus.Code__c = 'APPROVED';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Rejected';
        objStatus.Type__c = 'End';
        objStatus.Code__c = 'PENDING_CUSTOMER_INPUTS';
        lstStatus.add(objStatus);
        
        insert lstStatus;
            
        SR_Steps__c objSRStep = new SR_Steps__c();
        objSRStep.SR_Template__c = objTemplate.Id;
        objSRStep.Step_RecordType_API_Name__c = 'General';
        //objSRStep.Step_Template__c = objStepType.Id; // Step Type
        objSRStep.Summary__c = 'Approve Request';
        objSRStep.Step_No__c = 1.0;
        objSRStep.Start_Status__c = lstStatus[0].Id; // Default Step Status
        objSRStep.Active__c = true;
        objSRStep.Sys_Create_Condition__c = true;
        objSRStep.Do_not_use_owner__c = true;
            
        insert objSRStep;
        
        list<SR_Template_Docs__c> lstSRTempDocs = new list<SR_Template_Docs__c>();
        
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.On_Submit__c = false;
        objTempDocs.Validated_at__c = objSRStep.Id;
        objTempDocs.Evaluated_at__c = objSRStep.Id;
        objTempDocs.Generate_Document__c = true;
        objTempDocs.Original_Sighted_at__c = objSRStep.Id;
        objTempDocs.Added_through_Code__c = false;
        lstSRTempDocs.add(objTempDocs);
            
        objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.On_Submit__c = false;
        objTempDocs.Generate_Document__c = true;
        objTempDocs.Original_Sighted_at__c = objSRStep.Id;
        objTempDocs.Added_through_Code__c = true;
        lstSRTempDocs.add(objTempDocs);
        
        insert lstSRTempDocs;
        
        SR_Template_Item__c objSRTempItem = new SR_Template_Item__c();
        objSRTempItem.Product__c = objProduct.Id;
        objSRTempItem.SR_Template__c = objTemplate.Id;
        objSRTempItem.On_Submit__c = false;
        objSRTempItem.Evaluated_at__c = objSRStep.Id;
        objSRTempItem.Consumed_at__c = objSRStep.Id;
        insert objSRTempItem;
        
        map<string,SR_Status__c> mapSRStatus = new map<string,SR_Status__c>();
        
        for(SR_Status__c obj : [select Id,Name,Code__c,Type__c from SR_Status__c where Code__c='CLOSED' OR Code__c='REJECTED' OR Code__c='AWAITING_APPROVAL' OR Code__c='DRAFT']){
          mapSRStatus.put(obj.Code__c,obj);
        }
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        SR_Status__c objSRStatus;
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Closed';
        objSRStatus.Code__c = 'CLOSED';
        objSRStatus.Type__c = 'End';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Rejected';
        objSRStatus.Code__c = 'REJECTED';
        objSRStatus.Type__c = 'Rejected';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Awaiting Approval';
        objSRStatus.Code__c = 'AWAITING_APPROVAL';
        objSRStatus.Type__c = 'Start';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Approved';
        objSRStatus.Code__c = 'APPROVED';
        objSRStatus.Type__c = 'Intermediate';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Submitted';
        objSRStatus.Code__c = 'SUBMITTED';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Draft';
        objSRStatus.Code__c = 'DRAFT';
        lstSRStatus.add(objSRStatus);
        
        list<Transition__c> lstTransitions = new list<Transition__c>();
        Transition__c objTransition = new Transition__c();
        objTransition.From__c = lstStatus[0].Id; // awaiting approval
        objTransition.To__c = lstStatus[1].Id; // approved
        lstTransitions.add(objTransition);
        
        objTransition = new Transition__c();
        objTransition.From__c = lstStatus[1].Id; // approved
        objTransition.To__c = lstStatus[2].Id; // Rejected
        lstTransitions.add(objTransition);
        
        insert lstTransitions;
        
        list<Step_Transition__c> lstStepTransitions = new list<Step_Transition__c>();
        Step_Transition__c objStepTran;
        objStepTran = new Step_Transition__c();
        objStepTran.SR_Step__c = objSRStep.Id;
        objStepTran.SR_Status_Internal__c = lstSRStatus[0].Id;
        objStepTran.SR_Status_External__c = lstSRStatus[0].Id;
        objStepTran.Transition__c = lstTransitions[0].Id;
        lstStepTransitions.add(objStepTran);
        
        objStepTran = new Step_Transition__c();
        objStepTran.SR_Step__c = objSRStep.Id;
        objStepTran.SR_Status_Internal__c = lstSRStatus[1].Id;
        objStepTran.SR_Status_External__c = lstSRStatus[1].Id;
        objStepTran.Transition__c = lstTransitions[1].Id;
        lstStepTransitions.add(objStepTran);
        
        //Step__c objStepParent = new Step__c();
        //insert objStepParent;
        
        test.startTest();
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Email__c = 'testsr@difc.com';
        objSR.Internal_SR_Status__c = lstSRStatus[5].Id; // changing the status to Draft
        objSR.External_SR_Status__c = lstSRStatus[5].Id; // changing the status to Draft
        objSR.Contact__c = objContact.Id;
        objSR.SR_Template__c = objTemplate.Id;
        insert objSR;
          
        list<Business_Rule__c> lstBRs = new list<Business_Rule__c>();
               
        Business_Rule__c objBR = new Business_Rule__c();
        objBR.Action_Type__c = 'Create Conditions';
        objBR.Execute_on_Insert__c = true;
        objBR.Execute_on_Update__c = true;
        objBR.SR_Steps__c = objSRStep.Id;
        objBR.Text_Condition__c = 'Step Status->1.0#=#Courier Collected & Verified#AND#Service_Request__c->Name#!=#NULL';
        lstBRs.add(objBR);
        
        objBR = new Business_Rule__c();
        objBR.Action_Type__c = 'Create Conditions';
        objBR.Execute_on_Insert__c = true;
        objBR.Execute_on_Update__c = true;
        objBR.SR_Steps__c = objSRStep.Id;
        objBR.Text_Condition__c = 'Step Status->1.0#=#Courier Collected & Verified#AND#Service_Request__c->Name#!=#NULL';
        lstBRs.add(objBR);
        
        objBR = new Business_Rule__c();
        objBR.Action_Type__c = 'Custom Conditions Actions';
        objBR.Execute_on_Insert__c = true;
        objBR.Execute_on_Update__c = true;
        objBR.SR_Steps__c = objSRStep.Id;
        objBR.Text_Condition__c = 'Step Status->10.0#=#Courier Collected & Verified#AND#Service_Request__c->Customer__c#!=#NULL';
        lstBRs.add(objBR);
          
        insert lstBRs;
          
        string QueueId;
        for(QueueSobject obj : [select Id,QueueId from QueueSobject where SobjectType = 'Step__c' limit 1]){
            QueueId = obj.QueueId;
        }
        
        testStepUnitTest1(QueueId,objSR.Id,objSRStep.Id,lstStatus[0].Id,lstStatus[2].Id,lstSRTempDocs[0].Id);
        
        test.stopTest();
    }
    
    static testMethod void myUnitTest3() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        
        string conRT;
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
                 conRT = objRT.Id;
           
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.Phone = '1234567890';
        objContact.RecordTypeId = conRT;
        insert objContact;
      
      
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator' limit 1];  //Customer Community Login User Custom
        User u = new User(Alias = 'standt', Email='tesclasst@difc.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='tesclasstuat1@difc.com');
        u.Community_User_Role__c = 'Admin;Company Services Approver';    
        u.IsActive = true;
        insert u;
          
        Product2 objProduct = new Product2();
        objProduct.Name = 'DIFC Product';
        objProduct.IsActive = true;
        insert objProduct;
          
        Pricebook2 objPricebook = new Pricebook2();
        objPricebook.Name = 'Test Price Book';
        objPricebook.IsActive = true;
        objPricebook.Description = 'Test Description';
        insert objPricebook;
          
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Application_of_Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Menutext__c = 'Application_of_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Do_not_use_owner__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        objTemplate.Refund_Item__c = objProduct.Id;
        insert objTemplate;
            
        Pricing_Line__c objPricingLine = new Pricing_Line__c();
        objPricingLine.Product__c = objProduct.Id;
        objPricingLine.Active__c = true;
        objPricingLine.Priority__c = 12;
        objPricingLine.Price_Line_Condition__c = '(1 OR 2)';
        insert objPricingLine;
        
        Dated_Pricing__c objDP = new Dated_Pricing__c();
        objDP.Pricing_Line__c = objPricingLine.Id;
        objDP.Unit_Price__c = 1200;
        objDP.Appllication_Type__c = 'Add';
        objDP.Date_From__c = system.today().addYears(-2);
        objDP.Date_To__c = system.today().addYears(2);
        objDP.Use_List_Price__c = true;
        objDP.Cost__c=123.0;
        insert objDP;
            
        Pricing_Range__c objPriceRange = new Pricing_Range__c();
        objPriceRange.Dated_Pricing__c = objDP.Id;
        objPriceRange.Range_Start__c = 0;
        objPriceRange.Range_End__c = 2000;
        objPriceRange.Unit_Price__c = 1200;        
        insert objPriceRange;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'AR Third Party NOC';
        insert objDocMaster;
        
        list<Status__c> lstStatus = new list<Status__c>();
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Awaiting Approval';
        objStatus.Type__c = 'Start';
        objStatus.Code__c = 'AWAITING_APPROVAL';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Approved';
        objStatus.Type__c = 'Intermediate';
        objStatus.Code__c = 'APPROVED';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Rejected';
        objStatus.Type__c = 'End';
        objStatus.Code__c = 'PENDING_CUSTOMER_INPUTS';
        lstStatus.add(objStatus);
        
        insert lstStatus;
        
        Step_Template__c companyClosureTemplate = new Step_Template__c(Code__c = 'COMPANY_CLOSURE_INFORMATION',Name='Company Closure Information',Step_RecordType_API_Name__c = 'Company_Closure_Information');
        
        for(QueueSobject obj : [select Id,QueueId from QueueSobject where SobjectType = 'Step__c' AND Queue.Name like '%Client%' limit 1]){
            companyClosureTemplate.OwnerId = obj.QueueId;
        }
        
        insert companyClosureTemplate;
            
        SR_Steps__c objSRStep = new SR_Steps__c();
        objSRStep.SR_Template__c = objTemplate.Id;
        objSRStep.Step_RecordType_API_Name__c = 'General';
        objSRStep.Step_Template__c = companyClosureTemplate.Id; // Step Type
        objSRStep.Summary__c = 'Approve Request';
        objSRStep.Step_No__c = 1.0;
        objSRStep.Start_Status__c = lstStatus[0].Id; // Default Step Status
        objSRStep.Active__c = true;
        objSRStep.Sys_Create_Condition__c = true;
        objSRStep.Do_not_use_owner__c = true;
            
        insert objSRStep;
        
        list<SR_Template_Docs__c> lstSRTempDocs = new list<SR_Template_Docs__c>();
        
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.On_Submit__c = false;
        objTempDocs.Validated_at__c = objSRStep.Id;
        objTempDocs.Evaluated_at__c = objSRStep.Id;
        objTempDocs.Generate_Document__c = true;
        objTempDocs.Original_Sighted_at__c = objSRStep.Id;
        objTempDocs.Added_through_Code__c = false;
        lstSRTempDocs.add(objTempDocs);
            
        objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.On_Submit__c = false;
        objTempDocs.Generate_Document__c = true;
        objTempDocs.Original_Sighted_at__c = objSRStep.Id;
        objTempDocs.Added_through_Code__c = true;
        lstSRTempDocs.add(objTempDocs);
        
        insert lstSRTempDocs;
        
        SR_Template_Item__c objSRTempItem = new SR_Template_Item__c();
        objSRTempItem.Product__c = objProduct.Id;
        objSRTempItem.SR_Template__c = objTemplate.Id;
        objSRTempItem.On_Submit__c = false;
        objSRTempItem.Evaluated_at__c = objSRStep.Id;
        objSRTempItem.Consumed_at__c = objSRStep.Id;
        insert objSRTempItem;
        
        map<string,SR_Status__c> mapSRStatus = new map<string,SR_Status__c>();
        
        for(SR_Status__c obj : [select Id,Name,Code__c,Type__c from SR_Status__c where Code__c='CLOSED' OR Code__c='REJECTED' OR Code__c='AWAITING_APPROVAL' OR Code__c='DRAFT']){
          mapSRStatus.put(obj.Code__c,obj);
        }
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        SR_Status__c objSRStatus;
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Closed';
        objSRStatus.Code__c = 'CLOSED';
        objSRStatus.Type__c = 'End';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Rejected';
        objSRStatus.Code__c = 'REJECTED';
        objSRStatus.Type__c = 'Rejected';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Awaiting Approval';
        objSRStatus.Code__c = 'AWAITING_APPROVAL';
        objSRStatus.Type__c = 'Start';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Approved';
        objSRStatus.Code__c = 'APPROVED';
        objSRStatus.Type__c = 'Intermediate';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Submitted';
        objSRStatus.Code__c = 'SUBMITTED';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Draft';
        objSRStatus.Code__c = 'DRAFT';
        lstSRStatus.add(objSRStatus);
        
        list<Transition__c> lstTransitions = new list<Transition__c>();
        Transition__c objTransition = new Transition__c();
        objTransition.From__c = lstStatus[0].Id; // awaiting approval
        objTransition.To__c = lstStatus[1].Id; // approved
        lstTransitions.add(objTransition);
        
        objTransition = new Transition__c();
        objTransition.From__c = lstStatus[1].Id; // approved
        objTransition.To__c = lstStatus[2].Id; // Rejected
        lstTransitions.add(objTransition);
        
        insert lstTransitions;
        
        list<Step_Transition__c> lstStepTransitions = new list<Step_Transition__c>();
        Step_Transition__c objStepTran;
        objStepTran = new Step_Transition__c();
        objStepTran.SR_Step__c = objSRStep.Id;
        objStepTran.SR_Status_Internal__c = lstSRStatus[0].Id;
        objStepTran.SR_Status_External__c = lstSRStatus[0].Id;
        objStepTran.Transition__c = lstTransitions[0].Id;
        lstStepTransitions.add(objStepTran);
        
        objStepTran = new Step_Transition__c();
        objStepTran.SR_Step__c = objSRStep.Id;
        objStepTran.SR_Status_Internal__c = lstSRStatus[1].Id;
        objStepTran.SR_Status_External__c = lstSRStatus[1].Id;
        objStepTran.Transition__c = lstTransitions[1].Id;
        lstStepTransitions.add(objStepTran);
        
        //Step__c objStepParent = new Step__c();
        //insert objStepParent;
        
        test.startTest();
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Email__c = 'testsr@difc.com';
        objSR.Internal_SR_Status__c = lstSRStatus[5].Id; // changing the status to Draft
        objSR.External_SR_Status__c = lstSRStatus[5].Id; // changing the status to Draft
        objSR.Contact__c = objContact.Id;
        objSR.SR_Template__c = objTemplate.Id;
        insert objSR;
          
        list<Business_Rule__c> lstBRs = new list<Business_Rule__c>();
               
        Business_Rule__c objBR = new Business_Rule__c();
        objBR.Action_Type__c = 'Create Conditions';
        objBR.Execute_on_Insert__c = true;
        objBR.Execute_on_Update__c = true;
        objBR.SR_Steps__c = objSRStep.Id;
        objBR.Text_Condition__c = 'Step Status->1.0#=#Courier Collected & Verified#AND#Service_Request__c->Name#!=#NULL';
        lstBRs.add(objBR);
        
        objBR = new Business_Rule__c();
        objBR.Action_Type__c = 'Create Conditions';
        objBR.Execute_on_Insert__c = true;
        objBR.Execute_on_Update__c = true;
        objBR.SR_Steps__c = objSRStep.Id;
        objBR.Text_Condition__c = 'Step Status->1.0#=#Courier Collected & Verified#AND#Service_Request__c->Name#!=#NULL';
        lstBRs.add(objBR);
        
        objBR = new Business_Rule__c();
        objBR.Action_Type__c = 'Custom Conditions Actions';
        objBR.Execute_on_Insert__c = true;
        objBR.Execute_on_Update__c = true;
        objBR.SR_Steps__c = objSRStep.Id;
        objBR.Text_Condition__c = 'Step Status->10.0#=#Courier Collected & Verified#AND#Service_Request__c->Customer__c#!=#NULL';
        lstBRs.add(objBR);
          
        insert lstBRs;
          
        string QueueId;
        for(QueueSobject obj : [select Id,QueueId from QueueSobject where SobjectType = 'Step__c' limit 1]){
            QueueId = obj.QueueId;
        }
        
        testStepUnitTest1(QueueId,objSR.Id,objSRStep.Id,lstStatus[0].Id,lstStatus[2].Id,lstSRTempDocs[0].Id);
        
        test.stopTest();
    }
    
    @future 
    private static void testStepUnitTest1(String queueId, String srId, String srStepId, String statusId1, String statusId2, String srTemplteDocId){
    	
    	Step__c objStep = new Step__c();
        objStep.SR__c = srId;
        objStep.SR_Step__c = srStepId;
        objStep.Status__c = statusId1;
        objStep.Step_No__c = 1.0;
        objStep.Sys_Step_Loop_No__c = '1.0_1.0';
        
        if(QueueId != null)
          objStep.OwnerId = queueId; 
        
        insert objStep;
        
        SR_Doc__c objSRDoc = new SR_Doc__c();
        objSRDoc.Service_Request__c = srId;
        objSRDoc.Step__c = objStep.Id;
        objSRDoc.SR_Template_Doc__c =srTemplteDocId ;
        insert objSRDoc;
        
        list<SR_Doc__c> lstSRDocs = new list<SR_Doc__c>();
        
        for(SR_Doc__c obj:[select id,Original_Verified__c,SR_Template_Doc__r.Original_Sighted_at__c from SR_Doc__c where Service_Request__c =:srId and SR_Template_Doc__r.Original_Sighted_at__c =:srStepId]){
          obj.Original_Verified__c = true;
          lstSRDocs.add(obj);
        }
        
        update lstSRDocs;
        
        Service_Request__c objSR = new Service_Request__c(Id = srId);
        
        objSR.Required_Docs_not_Uploaded__c = false;
        update objSR;
        
        System.runAs(new User(Id = UserInfo.getUserID(), ProfileId = [SELECT id FROM Profile WHERE Name = 'FOSP MANAGER'].Id)){
        	
        	objStep.Status__c = statusId2;
	        objStep.Rejection_Reason__c = 'Test ';
	        objStep.Step_Note_Added__c = true;
	        objStep.SR_Group__c = 'Fit-Out & Events';
	        objStep.Step_Notes__c = 'Test Notes';
	        objStep.OwnerId = userInfo.getUserId();
	        update objStep;
        	
        }	
    	
    }
    
    static TestMethod void testSteptriggerOne(){
		Test_FitoutServiceRequestUtils.testSteptriggerOne();
	}
}