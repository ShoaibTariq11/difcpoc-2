/******************************************************************************************
 *  Author   : Saima Hidayat
 *  Company  : NSI JLT
 *  Date     : 16-Jul-2015
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             


*********************************************************************************************************************/
//User Access Form controller class

public without sharing class PropListingUserCls{
	Boolean Flag;
	Boolean displayOther;
    public Service_Request__c sUser{get;set;}
    public Attachment PassportCopy;
    public Attachment CompanyCopy;
    map<string,string> mapNationalityIds = new map<string,string>();
    map<string,string> mapSRRecType = new map<string,string>();
    public string strSelVal{get;set;}
    public Boolean getFlag(){ return Flag; }
    public void setFlag(Boolean val){ Flag = val; }  
    public Boolean getdisplayOther(){return displayOther;}
    public void setdisplayOther(Boolean val){displayOther = val;}
    
    public Attachment getPassportCopy(){
        PassportCopy = new Attachment();
        return PassportCopy;
    }
    public Attachment getCompanyCopy(){
        CompanyCopy = new Attachment();
        return CompanyCopy;
    }
    
    public PropListingUserCls(ApexPages.StandardController controller) {
        strSelVal = '';
        sUser = new Service_Request__c();
        sUser.Email__c = '';
        sUser.User_Form_Action__c='Create New Listing User';
        sUser.Name_of_the_Authority__c = '';
    }
    public PropListingUserCls(){
        strSelVal = '';
        sUser = new Service_Request__c();
        sUser.Email__c = '';
        sUser.Send_SMS_To_Mobile__c =Null;
    }

          
 //Create New User button
    
 //Validate fields and show document upload form
    
   public PageReference saveInfo(){
    Savepoint sp = Database.setSavepoint();
    try{
        
        if((sUser.First_Name__c != Null && sUser.First_Name__c !='' && sUser.Last_Name__c != Null && sUser.Last_Name__c !='' && sUser.Position__c != Null && sUser.Position__c !='' && sUser.Date_of_Birth__c != Null && sUser.Nationality_list__c != Null && sUser.Nationality_list__c !='' && sUser.Passport_Number__c != Null && sUser.Passport_Number__c !='' && sUser.Send_SMS_To_Mobile__c != Null && sUser.Send_SMS_To_Mobile__c !='' && sUser.Title__c != Null && sUser.Title__c !='' && sUser.Email__c != Null && sUser.Email__c !='' && sUser.Name_of_the_Authority__c!=null && sUser.Name_of_the_Authority__c!='') || ((sUser.Name_of_the_Authority__c =='DIFC Registered Company' || sUser.Name_of_the_Authority__c == 'Non-DIFC Registered Company') && ((sUser.License_Number__c!=null && sUser.License_Number__c!='') ||(sUser.Company_Name__c!='' && sUser.Company_Name__c!=null))) || test.isRunningtest() == true){
             try{
                /* Mapping Nationality Id to SR Nationality Lookup*/
                for(Lookup__c objNation:[select id,Name from Lookup__c where Type__c='Nationality']){
                        mapNationalityIds.put(objNation.Name,objNation.Id);
                 }
                 if(sUser.Nationality_list__c!=null && mapNationalityIds.get(sUser.Nationality_list__c)!=null){
                    sUser.Nationality__c = mapNationalityIds.get(sUser.Nationality_list__c);
                 }
                
                //Disclaimer Checkbox Validation
                if(sUser.I_agree__c==false){
                	Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please check the I Agree checkbox to confirm.'));
                    getPassportCopy();
                    getCompanyCopy();
                    Database.rollback(sp);
                	return null;
                }
                
                //Checking for existing contact and Account with RORP recordtype
                if(sUser.Name_of_the_Authority__c =='Individual User'){
                	list<Lease_Partner__c> LPCont = [Select id,contact__c,Contact__r.Nationality__c,Contact__r.Passport_No__c from Lease_Partner__c 
                										where contact__r.recordtype.DeveloperName='RORP_Contact' and Type__c='Landlord' 
                										and Contact__r.Nationality__c=:sUser.Nationality_list__c and Contact__r.Passport_No__c=:sUser.Passport_Number__c];
                	
	                if(LPCont.size() == 0 || LPCont.isEmpty()){
	                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'No individual contact found with the entered passport number and nationality.'));
	                    getPassportCopy();
	                    getCompanyCopy();
	                    Database.rollback(sp);
	                	return null;
	                }
	                //To check if the User already has Listing Services.
	                list<User> RORPUser = [Select Id,Community_User_Role__c,contactId,contact.AccountId,contact.Account.RecordType.DeveloperName,contact.Passport_No__c,contact.Nationality__c
                    							from User where contact.Individual_Contact__c=:LPCont[0].contact__c and contact.RecordType.DeveloperName='Portal_User' and 
                    							contact.Passport_No__c=:sUser.Passport_Number__c and contact.Nationality__c=:sUser.Nationality_list__c 
                    							and contact.Individual_Contact__r.BP_No__c!=null and contact.Account.RecordType.DeveloperName ='RORP_Account' and isActive=true];
		                	
		            if(RORPUser.size() > 0 && !RORPUser.isEmpty() && RORPUser[0].Community_User_Role__c.contains('Listing Services')){
		            	Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'The user already has the listing services access.'));
		                getPassportCopy();
		                getCompanyCopy();
		                Database.rollback(sp);
	                	return null;
		            }else if(RORPUser.size() > 0 && !RORPUser.isEmpty() && !RORPUser[0].Community_User_Role__c.contains('Listing Services')){
		            	sUser.Customer__c = RORPUser[0].Contact.AccountId;
		            }
	                
                }else if (sUser.Name_of_the_Authority__c =='DIFC Registered Company'){
                	if(sUser.License_Number__c==null || sUser.License_Number__c==''){
                		Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please fill all the required fields.'));
		                getPassportCopy();
		                getCompanyCopy();
		                Database.rollback(sp);
	                	return null;
                	}else{
	                	//Set<string> statAcc = new Set<string>{'Pending Dissolution','Active','Not Renewed','Pending Liquidation','Inactive - DFSA Lic. Wdn'};
	                	list<Lease_Partner__c> LPAccount =[Select id,Account__c,Account__r.Active_License__r.Name,Account__r.ROC_Status__c from Lease_Partner__c
	                									where Type__c='Landlord' and Account__r.Active_License__c!=null and Account__r.Active_License__r.Name=:sUser.License_Number__c AND Account__r.ROC_Status__c='Active'];

	                	if(LPAccount.size()>0 && !LPAccount.isEmpty() && LPAccount[0].Account__c!=null){
	                		list<User> RORPUser = [Select Id,Community_User_Role__c,contactId,contact.AccountId,contact.Account.RecordType.DeveloperName,contact.Passport_No__c,contact.Nationality__c
                    							from User where contact.accountId=:LPAccount[0].Account__c and contact.RecordType.DeveloperName='Portal_User' and 
                    							contact.Passport_No__c=:sUser.Passport_Number__c and contact.Nationality__c=:sUser.Nationality_list__c and isActive=true];
		                	
		                	if(RORPUser.size() > 0 && !RORPUser.isEmpty() && RORPUser[0].Community_User_Role__c.contains('Listing Services')){
				            	Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'The user already has the listing services access.'));
				                getPassportCopy();
				                getCompanyCopy();
				                Database.rollback(sp);
			                	return null;
				            }else{
	                			sUser.Customer__c = LPAccount[0].Account__c;
		                	}
	                	}else{
	                		Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'No DIFC registered company found with the entered license number'));
		                    getPassportCopy();
		                    getCompanyCopy();
		                    Database.rollback(sp);
	                		return null;
	                	}
                	}
                }else if(sUser.Name_of_the_Authority__c =='Non-DIFC Registered Company'){
                	if(sUser.Others_Please_Specify__c==null || sUser.Others_Please_Specify__c==''){
                		Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please fill all the required fields.'));
		                getPassportCopy();
		                getCompanyCopy();
		                Database.rollback(sp);
	                	return null;
                	}else if(sUser.License_Number__c==null || sUser.License_Number__c==''){
                		Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please fill all the required fields.'));
		                getPassportCopy();
		                getCompanyCopy();
		                Database.rollback(sp);
	                	return null;
                	}else{
                		list<Lease_Partner__c> LPAccount =[Select id,Account__c,Account__r.Issuing_Authority__c,Account__r.RORP_License_No__c from Lease_Partner__c
	                									where Type__c='Landlord' and Account__r.Issuing_Authority__c=:sUser.Others_Please_Specify__c and
	                									 Account__r.RORP_License_No__c=:sUser.License_Number__c and Account__r.Recordtype.DeveloperName='RORP_Account' and Account__c!=null];

	                	if(LPAccount.size()>0 && !LPAccount.isEmpty() && LPAccount[0].Account__c!=null){
	                		//Check for the existing portal contact
	                		list<User> RORPUser = [Select Id,Community_User_Role__c,contactId,contact.AccountId,contact.Account.RecordType.DeveloperName,contact.Passport_No__c,contact.Nationality__c
                    							from User where contact.AccountId=:LPAccount[0].Account__c and contact.RecordType.DeveloperName='Portal_User' and 
                    							contact.Passport_No__c=:sUser.Passport_Number__c and contact.Nationality__c=:sUser.Nationality_list__c and isActive=true];
		                	
		                	if(RORPUser.size() > 0 && !RORPUser.isEmpty() && RORPUser[0].Community_User_Role__c.contains('Listing Services')){
				            	Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'The user already has the listing services access.'));
				                getPassportCopy();
				                getCompanyCopy();
				                Database.rollback(sp);
			                	return null;
				            }else{
				            	sUser.Customer__c = LPAccount[0].Account__c;
				            }
	                	}else{
	                		Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'No company found with the entered license number and issuing authority'));
		                    getPassportCopy();
		                    getCompanyCopy();
		                    Database.rollback(sp);
	                		return null;
	                	}
                	}
                }
                
                //Submit finally with attachment and create an SR here.
                for(SR_Template__c template : [Select Id,SR_RecordType_API_Name__c from SR_Template__c where SR_RecordType_API_Name__c = 'Property_Listing_User_Access_Form']){
                	mapSRRecType.put(template.SR_RecordType_API_Name__c,template.id);
                }
                List<Attachment> AttachList= new List<Attachment>();
                Boolean found = false;
                Boolean found1 = false;
	            Set<String> acceptedExtensions = new Set<String> {'doc','docx','gif','pdf','png','jpeg','jpg'};
                
                if(PassportCopy.body!=null){
	                String fileName = PassportCopy.name;
	                String ext1=fileName.substring(fileName.lastIndexOf('.')+1);
	        
	                for(String s : acceptedExtensions){
	                    if(ext1 == s){
	                      found = true;
	                    }
	                }
                }
                if(sUser.Name_of_the_Authority__c =='Non-DIFC Registered Company' || sUser.Name_of_the_Authority__c =='DIFC Registered Company'){
	                if(CompanyCopy.body!=null){
		                String fileName = CompanyCopy.name;	               
		                String ext1=fileName.substring(fileName.lastIndexOf('.')+1);
		        
		                for(String s : acceptedExtensions){
		                    if(ext1 == s){
		                      found1 = true;
		                    }
		                }
	                }
	                
	                if(CompanyCopy.body==null || found1 == false){
		                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please attach authorization letter / power of attorney copy with valid format.')); 
		                getPassportCopy();
		                getCompanyCopy();
		                Database.rollback(sp);
		                return null;
                	}
                }
                if(PassportCopy.body==null || found == false){
	                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please attach passport copy with valid format.')); 
	                getPassportCopy();
	                getCompanyCopy();
	                Database.rollback(sp);
	                return null;
                }
                
                sUser.SR_Template__c = mapSRRecType.get('Property_Listing_User_Access_Form');
                sUser.Submitted_DateTime__c = DateTime.now();
                sUser.Roles__c = 'Listing Services';
                sUser.Submitted_Date__c = Date.today();
                //sUser.SR_Group__c = 'Listing';
                Date dat =Date.today();
                if(sUser.Date_of_Birth__c!=null){
                    dat = sUser.Date_of_Birth__c;
                    Integer dt = dat.daysBetween(Date.Today());
                    Integer age = Integer.valueOf(dt/365);
                    //System.debug('*****************'+age);
                    if(age < 18){
                        sUser.Date_of_Birth__c.addError('The age of the user must be more than 18 years.');
                        getPassportCopy();
                        getCompanyCopy();
                        return null;
                    }
                }
                                        
                insert sUser; 
                
                //Assigning the status to SR
                list<SR_Status__c> status = [Select Id,name from SR_Status__c where name = 'Submitted'];
                
                if(status.size()>0 && !status.isEmpty()){
	          		sUser.External_SR_Status__c = status[0].Id;
	                sUser.Internal_SR_Status__c = status[0].Id;
	                update sUser;
          		}
                list<SR_Doc__c> SRDoclst = new list<SR_Doc__c>();
                
                SR_Doc__c SRDoc = new SR_Doc__c(Name = 'Passport Copy', Service_Request__c = sUser.Id,status__c='Uploaded');
                SRDoclst.add(SRDoc);
                if(sUser.Name_of_the_Authority__c =='Non-DIFC Registered Company' || sUser.Name_of_the_Authority__c =='DIFC Registered Company'){
	                SR_Doc__c SRDoc1 = new SR_Doc__c(Name = 'Authorization Letter / Power of Attorney Copy', Service_Request__c = sUser.Id,status__c='Uploaded');
	                SRDoclst.add(SRDoc1);
                }
                insert SRDoclst;
                
                Attachment PC = new Attachment(parentId = SRDoc.id, name =PassportCopy.name, body = PassportCopy.body);
                AttachList.add(PC);
                if(sUser.Name_of_the_Authority__c =='Non-DIFC Registered Company' || sUser.Name_of_the_Authority__c =='DIFC Registered Company'){
                	Attachment CC = new Attachment(parentId = SRDoc.id, name =CompanyCopy.name, body = CompanyCopy.body);
                	AttachList.add(CC);
                }
                
                insert AttachList;
                
                System.debug('name is :'+Site.getpathprefix());
                PageReference redirectPage = new PageReference(Site.getPathPrefix()+'/apex/FormPreview?&SRNo='+sUser.Id);
                return redirectPage;
                
             }catch(Exception e){
                        //sUser.License_Number__c.addError('License number not found or incorrect!');
                        Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,e.getMessage()));
                        Database.rollback(sp);
                        getPassportCopy();
                        getCompanyCopy();
                        return null; 
             }
        }else{ 
              Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please fill all the required fields.'));
              getPassportCopy();
              getCompanyCopy();
              Database.rollback(sp);
              return null;   
        }
    }catch(Exception ex){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,ex.getMessage()));
            Database.rollback(sp);
            getPassportCopy();
            getCompanyCopy();
            return null;
    }    
                                    
  }
  
  public pagereference ChangeOption(){
        sUser.Name_of_the_Authority__c = strSelVal;
        Flag = false;
        displayOther = false;
        system.debug('strSelVal====>'+strSelVal);
        if(strSelVal=='Non-DIFC Registered Company'){
            Flag = true;
            displayOther = true;
        }else if(strSelVal=='DIFC Registered Company'){
            Flag = true;
        }
        return null;
  }
 
//********************Custom Code for the Steps********************//

    public static String createListingUser(Step__c stp){

        String statusMsg;
        //map<string,User> mapUserNames = new map<string,User>();
        //list<Contact> lstcontact = new list<Contact>();
        if(stp!=null){
            List<Service_Request__c> SRUser = [Select Id, Customer__c,Send_SMS_To_Mobile__c,Nationality__c,First_Name__c,
                                               	Last_Name__c, Email__c,Name_of_the_Authority__c,Others_Please_Specify__c,Roles__c,Passport_Number__c,
                                                Place_of_Birth__c,Title__c,Nationality_list__c,Middle_Name__c,Date_of_Birth__c,Position__c,License_Number__c
                                                from Service_Request__c where Id =:stp.SR__c]; 
            if(SRUser.size()!=0 && SRUser != null){ 
                Savepoint spdata1 = Database.setSavepoint();
                try{     
                    map<string,Id> mapRecordTypeIds = new map<string,Id>();
                    for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Portal_User','RORP_Account','RORP_Contact') AND IsActive=true]){
                        mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
                    }
                    
                    //Checking for existing contact and Account with RORP recordtype
	                if(SRUser[0].Name_of_the_Authority__c =='Individual User'){
	                	list<Lease_Partner__c> LPCont = [Select id,contact__c,Contact__r.Nationality__c,Contact__r.Passport_No__c,contact__r.RecordType.DeveloperName,Contact__r.BP_No__c 
	                										from Lease_Partner__c where contact__r.recordtype.DeveloperName='RORP_Contact' and Type__c='Landlord' and Contact__r.BP_No__c!=null 
	                										and Contact__r.Nationality__c=:SRUser[0].Nationality_list__c and Contact__r.Passport_No__c=:SRUser[0].Passport_Number__c];
	                	
		                if(LPCont.size() == 0 || LPCont.isEmpty()){
		                    statusMsg = 'No individual contact found with the entered passport number and nationality.';
		                    return statusMsg;
		                }else if(LPCont.size()>0 && LPCont[0].contact__c!=null){
		                	list<User> RORPUser = [Select Id,Community_User_Role__c,contactId,contact.AccountId,contact.Account.RecordType.DeveloperName,contact.Passport_No__c,contact.Nationality__c
                    							from User where contact.Individual_Contact__c=:LPCont[0].contact__c and contact.RecordType.DeveloperName='Portal_User' and 
                    							contact.Passport_No__c=:SRUser[0].Passport_Number__c and contact.Nationality__c=:SRUser[0].Nationality_list__c 
                    							and contact.Individual_Contact__r.BP_No__c!=null and contact.Account.RecordType.DeveloperName ='RORP_Account' and isActive=true];
		                	
		                	if(RORPUser.size() > 0 && !RORPUser.isEmpty()){
		                			statusMsg = CreatePortalCont(RORPUser,RORPUser[0].ContactId,null,RORPUser[0].contact.AccountId,SRUser);
		                	}else{
		                		Account RORPAccount = new Account();
		                		if(mapRecordTypeIds.containsKey('RORP_Account'))
			                    	RORPAccount.RecordTypeId = mapRecordTypeIds.get('RORP_Account');//accRecType;
			                    RORPAccount.Name = SRUser[0].First_Name__c+' '+SRUser[0].Last_Name__c;
			                    RORPAccount.Mobile_Number__c = SRUser[0].Send_SMS_To_Mobile__c;
			                    RORPAccount.E_mail__c = SRUser[0].Email__c;
			                    RORPAccount.Country__c = 'United Arab Emirates';
			                    RORPAccount.BP_No__c = LPCont[0].Contact__r.BP_No__c;
			                    
			                    insert RORPAccount;
                    			User Admin = [Select id,Name,profileid from User where Name='Integration User' and profile.Name='System Administrator'];
			                    system.debug('User :::: '+Admin);
			                    RORPAccount.OwnerId = Admin.id;
			                    update RORPAccount;
			                    
			                    statusMsg = CreatePortalCont(null,null,RORPAccount.id,LPCont[0].Contact__c,SRUser);
		                	}
		                }
	                }else if (SRUser[0].Name_of_the_Authority__c =='DIFC Registered Company'){
	                	
	                	//Set<string> statAcc = new Set<string>{'Pending Dissolution','Active','Not Renewed','Pending Liquidation','Inactive - DFSA Lic. Wdn'};
	                	list<Lease_Partner__c> LPAccount =[Select id,Account__c,Account__r.Active_License__r.Name,Account__r.ROC_Status__c from Lease_Partner__c
	                									where Type__c='Landlord' and Account__r.Active_License__c!=null and Account__r.Active_License__r.Name=:SRUser[0].License_Number__c AND Account__r.ROC_Status__c='Active'];
	                	
	                	if(LPAccount.size()>0 && !LPAccount.isEmpty() && LPAccount[0].Account__c!=null){
	                		//Check for existing portal contacts
	                		list<User> RORPUser = [Select Id,Community_User_Role__c,contactId,contact.AccountId,contact.Account.RecordType.DeveloperName,contact.Passport_No__c,contact.Nationality__c
                    							from User where contact.accountId=:LPAccount[0].Account__c and contact.RecordType.DeveloperName='Portal_User' and 
                    							contact.Passport_No__c=:SRUser[0].Passport_Number__c and contact.Nationality__c=:SRUser[0].Nationality_list__c and isActive=true];
		                	
		                	if(RORPUser.size() > 0 && !RORPUser.isEmpty()){
		                		statusMsg = CreatePortalCont(RORPUser,RORPUser[0].ContactId,null,RORPUser[0].Contact.AccountId,SRUser);
		                	}else{
		                		 statusMsg = CreatePortalCont(null,null,LPAccount[0].Account__c,null,SRUser);
		                	}
	                	}else{
	                		statusMsg = 'No DIFC registered company found with the entered license number';
		                    return statusMsg;
	                	}
	                	
	                }else if(SRUser[0].Name_of_the_Authority__c =='Non-DIFC Registered Company'){
	                	
	            		list<Lease_Partner__c> LPAccount =[Select id,Account__c,Account__r.Issuing_Authority__c,Account__r.RORP_License_No__c from Lease_Partner__c
	                									where Type__c='Landlord' and Account__r.Issuing_Authority__c=:SRUser[0].Others_Please_Specify__c and
	                									 Account__r.RORP_License_No__c=:SRUser[0].License_Number__c and Account__r.Recordtype.DeveloperName='RORP_Account' and Account__c!=null];

	                	if(LPAccount.size()>0 && !LPAccount.isEmpty() && LPAccount[0].Account__c!=null){
	                		
	                		//Check for the existing portal contact
	                		list<User> RORPUser = [Select Id,Community_User_Role__c,contactId,contact.AccountId,contact.Account.RecordType.DeveloperName,contact.Passport_No__c,contact.Nationality__c
                    							from User where contact.AccountId=:LPAccount[0].Account__c and contact.RecordType.DeveloperName='Portal_User' and 
                    							contact.Passport_No__c=:SRUser[0].Passport_Number__c and contact.Nationality__c=:SRUser[0].Nationality_list__c and isActive=true];
		                	
		                	if(RORPUser.size() > 0 && !RORPUser.isEmpty()){
		                		statusMsg = CreatePortalCont(RORPUser,RORPUser[0].contactId,null,RORPUser[0].Contact.AccountId,SRUser);
		                	}else{
		                		statusMsg = CreatePortalCont(null,null,LPAccount[0].Account__c,null,SRUser);
		                	}
	                		
	                	}else{
	                		statusMsg = 'No company found with the entered license number and issuing authority';
		                    return statusMsg;
	                	}
	                }
	                
                }catch(Exception e){
                     statusMsg = string.valueOf(e.getMessage()+'Line Number: '+ e.getLineNumber());
                     Database.rollback(spdata1);
                }    
            }
        }
         return statusMsg;
    }
   public static string CreatePortalCont(list<User> Userlst, Id PortConId, Id accId, Id RORPContAccId, list<Service_Request__c> SRUser){
	    String statusMsg;
	    User usr = new User();
	    Contact PortalCont = new Contact ();
	    Service_Request__c updateSR = new Service_Request__c(id=SRUser[0].id);
        map<string,User> mapUserNames = new map<string,User>();
	    map<string,Id> mapRecordTypeIds = new map<string,Id>();
	    
	    Savepoint spdata = Database.setSavepoint();
	   	try{
	   		
	   		if(Userlst!=null && PortConId!=null && Userlst[0].id!=null){//Userlst.size()>0 && !Userlst.isEmpty()
	   			system.debug('Userlst :   '+Userlst);
	   			usr = new User(Id = Userlst[0].id);
	   			PortalCont = new Contact (id = PortConId);
	   			 	   			
	   			if(Userlst[0].Community_User_Role__c!=null && Userlst[0].Community_User_Role__c!=''){
	   				usr.Community_User_Role__c = Userlst[0].Community_User_Role__c+';'+'Listing Services';
	   				PortalCont.Role__c = Userlst[0].Community_User_Role__c+';'+'Listing Services';
	   			}else{
	   				usr.Community_User_Role__c = 'Listing Services';
	   				PortalCont.Role__c = 'Listing Services';
	   			}
	   			
	   			update usr;
	   		
	   		}else{
		   		
		   		for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Portal_User','RORP_Account','RORP_Contact') AND IsActive=true]){
		        	mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
		    	}
		    	system.debug('mapRecordTypeIds:  '+mapRecordTypeIds.keyset());
		   		if(Userlst==null && (SRUser[0].Name_of_the_Authority__c =='Individual User' || SRUser[0].Name_of_the_Authority__c =='DIFC Registered Company' || SRUser[0].Name_of_the_Authority__c =='Non-DIFC Registered Company')){
			   		system.debug('mapRecordTypeIds   '+ mapRecordTypeIds.keyset());
			   		PortalCont = new Contact();
			        usr = new User();
			        
			        PortalCont.AccountId = accId;//RORPAccount.id;
			        PortalCont.FirstName = SRUser[0].First_Name__c;
			        PortalCont.LastName= SRUser[0].Last_Name__c;
			        PortalCont.MobilePhone = SRUser[0].Send_SMS_To_Mobile__c;
			        PortalCont.Email = SRUser[0].Email__c;
			        PortalCont.Nationality_Lookup__c = SRUser[0].Nationality__c;
			        PortalCont.Passport_No__c = SRUser[0].Passport_Number__c;
			        PortalCont.Middle_Names__c = SRUser[0].Middle_Name__c;
			        PortalCont.Place_of_Birth__c = SRUser[0].Place_of_Birth__c;
			        PortalCont.Birthdate = SRUser[0].Date_of_Birth__c;
			        PortalCont.Salutation = SRUser[0].Title__c;
			        PortalCont.Occupation__c = SRUser[0].Position__c;
			        PortalCont.Country__c = 'United Arab Emirates';
			        PortalCont.RecordTypeId = mapRecordTypeIds.get('Portal_User');//RecordTypeId;
			        PortalCont.Role__c = SRUser[0].Roles__c; //'Property Services';
			        PortalCont.Nationality__c = SRUser[0].Nationality_list__c;
			        if(SRUser[0].Name_of_the_Authority__c =='Individual User' && RORPContAccId!=null)
			        	PortalCont.Individual_Contact__c = RORPContAccId;//RORPContact.id;
			        
			        insert PortalCont;
			        
			        //Checking for existing Usernames
	                String emailStr = SRUser[0].Email__c.split('@')[0]+'%';
	                String UserName = '';
	                
	                for(User objUsr : [select Id,Username from User where Username like :emailStr order by Username]){
	                    mapUserNames.put(objUsr.Username,objUsr);
	                }
	                                    
	                UserName = SRUser[0].Email__c.split('@')[0]+Label.Portal_UserDomain;
	                if(mapUserNames.containsKey(UserName)){                         
	                    for(Integer i=1;i<=Integer.valueof(Label.Username_limit);i++){
	                        UserName = SRUser[0].Email__c.split('@')[0]+'_'+i+Label.Portal_UserDomain;
	                        if(mapUserNames.containsKey(UserName) == false){
	                            mapUserNames.put(UserName,usr);
	                            break;
	                        }
	                    }
	                }else{
	                    mapUserNames.put(UserName,usr); 
	                }
	                system.debug('Username is : '+UserName);
	                //V1.1 - Username limit to custom label
	                
	                usr.contactId=PortalCont.Id;
	                usr.username = UserName;
	                usr.CompanyName = accId;
	                usr.firstname=SRUser[0].First_Name__c;
	                usr.lastname=SRUser[0].Last_Name__c;
	                usr.email=SRUser[0].Email__c;
	                usr.phone = SRUser[0].Send_SMS_To_Mobile__c;
	                usr.Title = SRUser[0].Position__c;
	                usr.communityNickname = (SRUser[0].Last_Name__c +string.valueof(Math.random()).substring(4,9));
	                usr.alias = string.valueof(SRUser[0].Last_Name__c.substring(0,1) + string.valueof(Math.random()).substring(4,9));            
	                usr.profileid = Label.Profile_ID;
	                usr.emailencodingkey='UTF-8';
	                usr.languagelocalekey='en_US';
	                usr.localesidkey='en_GB';
	                usr.timezonesidkey='Asia/Dubai';
	                usr.Community_User_Role__c = SRUser[0].Roles__c;
	                
	                updateSR.Username__c = UserName;
	                updateSR.Customer__c = accId;
	                PortalCont.Portal_Username__c = UserName;
	                
	                //Mail alert Coding with temporary password
	                Database.DMLOptions dlo = new Database.DMLOptions();
	                dlo.EmailHeader.triggerUserEmail = true;
	                dlo.EmailHeader.triggerAutoResponseEmail= true;
	                usr.setOptions(dlo); 
			        
			        insert usr;
		   		}
		        
	   		}
	   		
			List<SR_Status__c> status = [Select Id,name from SR_Status__c where name = 'Approved'];
	   		if(status.size()>0 && !status.isEmpty()){
	            updateSR.External_SR_Status__c = status[0].Id;
	            updateSR.Internal_SR_Status__c = status[0].Id;
	            if(accId!=null)
	            	updateSR.Customer__c = accId;
            }
            
            update updateSR;
            update PortalCont;
        	statusMsg = 'Success';
        	
	   	}catch(Exception e){
		     statusMsg = string.valueOf(e.getMessage());
		     Database.rollback(spdata);
	    } 
	    
	    return statusMsg;
   }
    
}