/****************************************************************
* Apex Class: TRCaseStatusbatch
* Description: Use this class to get the TR case status every 5 mins
* Modified  :
*****************************************************************/
global class TRCaseStatusbatch implements Database.batchable<SObject>, database.allowscallouts {
    global Database.QueryLocator start(Database.batchableContext bc) {
        return Database.getQueryLocator('select id,TRCase_No__c,TR_Case_Status__c from Thompson_Reuters_Check__c where TRCase_No__c!=null and TR_Case_Status__c!=\'CloseCase\'');
    }
    global void execute(Database.BatchableContext bc, Thompson_Reuters_Check__c[] records) {
        for(Thompson_Reuters_Check__c itr: records){
            ThompsonReutersValidation.checkTRCaseStatus(itr.TRCase_No__c);
        }
    }
    global void finish(Database.BatchableContext bc) {
       
    }
    
}