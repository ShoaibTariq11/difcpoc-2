/*
    Author      :   Ghanshyam
    Description :   This Scheduler class will be used to send the email every quaterly
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        	Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.0	23/12/2018  	Ghanshyam       Created
    V1.1    07/01/2019      Azfer           Updated the Cron expression.
                                            From: 0 0 5 1 JAN,APR,JUL,OCT ?
                                            To: 0 0 0 1 JAN,APR,JUL,OCT ?
*/
global class quaterly_Email_Alert_Batch_Schedulable implements Schedulable{
    /*
    Runnig Every Quarter for 1st date of month starting from 1st Jan 2019
    */
    public static final String CRON_EXPR = '0 0 0 1 JAN,APR,JUL,OCT ?';
    // public static final String CRON_EXPR =  '0 5 * * * ?';

    global static String scheduleIt() {
        quaterly_Email_Alert_Batch_Schedulable job = new quaterly_Email_Alert_Batch_Schedulable();
        return System.schedule('Schedule Job',CRON_EXPR, job);
    }

    global void execute(SchedulableContext sc) {
        quaterly_Email_Alert_Batch b = new quaterly_Email_Alert_Batch();
        Database.executeBatch(b);
    }
}