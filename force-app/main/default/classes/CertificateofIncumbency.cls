/**********************************************************
*Apex Class : CertificateofIncumbency
*Description : Created to replace the standard layout
*Modified: #4979
***********************************************************/
public with sharing class CertificateofIncumbency{

public Service_Request__c SRData{get;set;}

public string RecordTypeId;
public map<string,string> mapParameters;
 
public Account ObjAccount{get;set;}
public boolean IsFEY_Provided{get;set;}
public string SRRecType {get;set;}
public Boolean isEditMode{get;set;}
public Boolean isDetail{get;set;}
 public CurrentContext ContextObj {get;set;}
   
    public CertificateofIncumbency(ApexPages.StandardController controller){
        
        IsFEY_Provided=false;
        isEditMode = true;
        ObjAccount=new account();
         ContextObj = new CurrentContext();
        if(Apexpages.currentPage().getParameters().get('isDetail') != null && Apexpages.currentPage().getParameters().get('isDetail') == 'true'){
            ContextObj.IsDetail = true;
        }
        else{
            ContextObj.IsDetail = false;
        }
        ContextObj.IsPortaluser = (Userinfo.getUserType() == 'Standard') ? false : true;
        
        List<String> fields = new List<String> {'Financial_Year_End_mm_dd__c'};
        if (!Test.isRunningTest()) controller.addFields(fields); // still covered

        SRData=(Service_Request__c )controller.getRecord();

        if(SRData.Financial_Year_End_mm_dd__c!=null){
        ObjAccount.Financial_Year_End__c=SRData.Financial_Year_End_mm_dd__c;
        }
        mapParameters = new map<string,string>();

        if(apexpages.currentPage().getParameters()!=null){
        mapParameters = apexpages.currentPage().getParameters();
        }  
        
        SRRecType  ='Certificate of Incumbency';
       if(mapParameters.get('RecordType')!=null){ 
       RecordTypeId= mapParameters.get('RecordType');
       }
        for(User objUsr:[select id,ContactId,Email,Phone,Contact.Account.Legal_Type_of_Entity__c,Contact.AccountId,Contact.Account.Company_Type__c,Contact.Account.Financial_Year_End__c,Contact.Account.Next_Renewal_Date__c,Contact.Account.Name,Contact.Account.Contact_Details_Provided__c,Contact.Account.Sector_Classification__c,Contact.Account.License_Activity__c,Contact.Account.Is_Foundation_Activity__c  from User where Id=:userinfo.getUserId()])
        {
         if(SRData.id==null)
         {
            
            SRData.Customer__c = objUsr.Contact.AccountId;
            SRData.RecordTypeId=RecordTypeId;
            SRData.Email__c = objUsr.Email;
            SRData.Legal_Structures__c=objUsr.Contact.Account.Legal_Type_of_Entity__c;
            SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
            SRData.Entity_Name__c=objUsr.Contact.Account.Name;
            SRData.Expiry_Date__c=objUsr.Contact.Account.Next_Renewal_Date__c;
            ObjAccount= objUsr.Contact.Account;
            SRData.Financial_Year_End_mm_dd__c=objUsr.Contact.Account.Financial_Year_End__c;
            SRRecType  ='Certificate of Incumbency';
            
                
         }
         if(SRData.id!=null){
    SRData =[select id,Doyouwishtoaddpassportnumbers__c,RecordTypeId,Expiry_Date__c,Legal_Structures__c,Entity_Name__c,Name,Service_Type__c,Customer__c,Submitted_Date__c,Internal_Status_Name__c,External_Status_Name__c,On_behalf_of_identical_name__c,Attestation_Required__c,Email__c,Send_SMS_To_Mobile__c from Service_Request__c where id=:SRData.id];
    }
        
        }
        }

public PageReference SaveConfirmation(){
    
    boolean isvalid=true;
    isEditMode = false;
    //Boolean hasOpenCompliances = ![SELECT Id FROM Compliance__c WHERE Status__c = 'Created' AND Start_Date__c > TODAY AND Account__c = :SRData.Customer__c AND Name = 'Confirmation Statement'].isEmpty();
    if(SRData.id==null){
        Insert SRData;
    }else{
    
        update SRData;
    }
   
    if(SRData.id!=null){
    //SRData =[select id,RecordTypeId,Expiry_Date__c,Legal_Structures__c,Entity_Name__c,Name,Service_Type__c,Customer__c,Submitted_Date__c,Internal_Status_Name__c,External_Status_Name__c,On_behalf_of_identical_name__c,Attestation_Required__c,Email__c,Send_SMS_To_Mobile__c from Service_Request__c where id=:SRData.id];
        Pagereference objRef;
        objRef = new Pagereference('/CertificateofIncumbency?id'+SRData.Id);
        objRef.setRedirect(true);
        return objRef;  
    }
    else{
        Pagereference objRef = new Pagereference('/CertificateofIncumbency?id'+SRData.Id);
        objRef.setRedirect(true);
        return objRef;  
    }
    //return null;

}
public PageReference EditSR(){

    isEditMode =true;
    if(SRData.id!=null){
        //SRData =[select id,RecordTypeId,Expiry_Date__c,Legal_Structures__c,Entity_Name__c,Name,Service_Type__c,Customer__c,Submitted_Date__c,Internal_Status_Name__c,External_Status_Name__c,On_behalf_of_identical_name__c,Attestation_Required__c,Email__c,Send_SMS_To_Mobile__c from Service_Request__c where id=:SRData.id];
    }
    //Pagereference objRef = new Pagereference('/apex/CertificateofIncumbency?id='+SRData.Id);
    //objRef.setRedirect(true);
    return null;

}
public Pagereference SubmitRequest(){
            
            
           
            string AlertMsg = '';
            Boolean isUploaded = true;
            for(SR_Doc__c objDoc : [select Id,Name,Is_Not_Required__c,Status__c from SR_Doc__c where Is_Not_Required__c = false AND Service_Request__c =: SRData.Id]){
                    if(objDoc.Status__c == 'Pending Upload' || objDoc.Status__c == 'Re-upload'){
                        isUploaded = false;
                    }
                }
                
            if(isUploaded == false){
                 AlertMsg = 'Please Upload the Required Docs before Submit.';
                   Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,AlertMsg ));
                 return null;
            }
            Pagereference objRef;
                 
           if(SRData.Id!=null){
           objRef = new Pagereference('/apex/SRSubmissionPage?id='+SRData.Id);
           objRef.setRedirect(true);
           }else{
               upsert SRData;
          objRef= new Pagereference('/apex/SRSubmissionPage?id='+SRData.Id);
           objRef.setRedirect(true);
           }
           return objRef;
        
    }
     public Pagereference CancelRequest(){
        Pagereference objRef;
        try{
            if(ContextObj.IsDetail == true){ //Detail Page Button
                if(SRData.Submitted_Date__c != null){
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Request was already submitted.'));
                    return null;
                }else{
                    delete SRData;
                    if(ContextObj.IsPortaluser)
                        objRef = new Pagereference('/apex/home');
                    else
                        objRef = new Pagereference('/home/home.jsp');
                }
            }else if(ContextObj.IsDetail == false){ //Edit Page Button
                if(SRData.Id != null)
                    objRef = new Pagereference('/'+SRData.Id);
                else{
                    if(ContextObj.IsPortaluser)
                        objRef = new Pagereference('/apex/home');
                    else
                        objRef = new Pagereference('/home/home.jsp');
                }
            }
            if(objRef != null)
                objRef.setRedirect(true);
            return objRef;
        }catch(Exception ex){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please contact support.'));
        }
        return null;    
    }
     public class CurrentContext{
       
        public Boolean IsDetail {get;set;}
        public Boolean IsPortaluser {get;set;}
        
    }
    
}