global without sharing class CRM_cls_EmailUtils_lightning {
@AuraEnabled
public static String getClientContacts(String accountId){
        
        return [SELECT Id FROM Contact WHERE AccountId = :accountId].Id;
    }

 @AuraEnabled
 Public static Opportunity getopportunity(string relatedToId){
  Opportunity opp =  [select id,Name,StageName,Stage_Steps__c,OwnerId,accountId from opportunity where id =:relatedToId limit 1];
  return opp;
 }
    
    
 @AuraEnabled
public static String createEmailLink(String relatedToId, String accountId, String stageName, String stageStep, String pointOfOrigin, String emailTemplateCode,string RMEmail){
   //  Opportunity opps = [select id,Name,StageName,Stage_Steps__c,OwnerId from Opportunity where id =:relatedToId];
boolean isGate = false;
    List<Account> lstAccount = [Select ID FROM Account where ID =: accountId
               AND Property_solutions__c IN('Retail - Gate Avenue','Retail - Gate Avenue M2')];
            
    if(lstAccount.isEmpty()){
    List<Lead> lstLead = [Select ID from Lead where ConvertedAccountId =: accountId 
                              AND Property_solutions__c IN('Retail - Gate Avenue','Retail - Gate Avenue M2')];
                
          if(lstLead.isEmpty())
                    isGate = false;
                else  
                    isGate = true;
            }
            else
                isGate = true;
        
        if(pointOfOrigin == 'RRC Others' && isGate){ return 'You cannot select RRC Others for Retail Gate Avenue or Retail Gate Avenue M2';
            
        }
        
        if(pointOfOrigin == 'RRC Gate' && !isGate){ return '"Property Solution” field is blank, please check';
            
        }
        List<CRM_Email_Template_Code__mdt> referencedTemplates = String.isBlank(emailTemplateCode) ? getEmailTemplateByStage(stageName, stageStep, pointOfOrigin) : getEmailTemplateByTemplateCode(stageName, stageStep, emailTemplateCode);
        system.debug('referencedTemplates'+referencedTemplates);
        if(referencedTemplates.isEmpty() && !Test.isRunningTest()){ return 'You are not allowed to send to '+pointOfOrigin+' at this particular stage.';
            
        } 
        
        //contactId = String.isBlank(contactId) ? referencedTemplate[0].Approver_ID__c : contactId;
        
        String contactId = pointOfOrigin.equals('Client') ? getClientContacts(accountId) : referencedTemplates[0].Approver_ID__c;
        
        String templateId = referencedTemplates[0].Template_ID__c;
        
        String copyToEmails = referencedTemplates[0].CC__c;
        
        user u = [select id,email from USER where id =:RMEmail]; string RME = u.email;

             
        return URL.getSalesforceBaseUrl().toExternalForm() + '/_ui/core/email/author/EmailAuthor?p2_lkid=' 
            + contactId + '&&rtype=003&p3_lkid=' + relatedToId + '&retURL=%2F' + relatedToId + '&p26=portal@difc.ae&template_id=' + templateId + '&p24='+ RME +
            (String.isNotBlank(copyToEmails) ? '&p4='+copyToEmails : ''); // V1.4 - Claude - Added CC field
    }
    
    @AuraEnabled
    public static List<CRM_Email_Template_Code__mdt> getEmailTemplateByStage(String stageName, String stageStep, String pointOfOrigin){
        
        return [SELECT Id, Approver_ID__c, CC__c, Template_ID__c FROM CRM_Email_Template_Code__mdt WHERE Opportunity_Stage__c = :stageName AND Stage_Steps__c = :stageStep AND Point_of_Origin__c = :pointOfOrigin];
        
    }    
   @AuraEnabled
    public static List<CRM_Email_Template_Code__mdt> getEmailTemplateByTemplateCode(String stageName, String stageStep, String emailTemplateCode){
        
        return [SELECT Id, Approver_ID__c, CC__c, Template_ID__c FROM CRM_Email_Template_Code__mdt WHERE Opportunity_Stage__c = :stageName AND Stage_Steps__c = :stageStep AND Email_Template_Code__c = :emailTemplateCode];        
    }
}