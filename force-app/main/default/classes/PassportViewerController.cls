public with sharing class PassportViewerController {

  List<string> SRDodIds = new List<String>();
  
  public List<id> AttachmentIds {get;set;}

    public PassportViewerController(ApexPages.StandardController controller) {
    
        service_request__c SR = (service_request__c)controller.getRecord();

         for( SR_Doc__c  srDoc : [select id,name from SR_Doc__c where Service_Request__c = : SR.id and status__c = 'Uploaded'  and ( name ='Passport' OR  name = 'passport-1 (additional Upload)')]){
         
            SRDodIds.add(srDoc.id); 
         
         }  

    }
    
    public List<id> getAttachments(){
      if(AttachmentIds  == null ){       
            AttachmentIds  = new List<id>();        
            for(Attachment att: [select id from Attachment where parentId in: SRDodIds]){         
            AttachmentIds.add(att.id);      
           }  
    
        }
         return AttachmentIds;
    }
   
  
}