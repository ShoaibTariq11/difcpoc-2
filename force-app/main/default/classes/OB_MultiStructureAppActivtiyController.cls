/******************************************************************************************
 *  Name        : OB_MultiStructureAppActivtiyController 
 *  Author      : Maruf Bagwan
 *  Company     : PwC
 *  Date        : 24-March-2020
 *  Description : Controller for Multi Structure Activity Controller 
 ----------------------------------------------------------------------------------------                               
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No      Date          Updated By          Description
 ----------------------------------------------------------------------------------------              
 V1.0   24-March-2020   Maruf Bagwan         Initial Version
*******************************************************************************************/
public without sharing class OB_MultiStructureAppActivtiyController {
    
    public class RequestWrap {
        @AuraEnabled
        public string flowID { get; set; }
        
        @AuraEnabled
        public string pageId { get; set; }
        
        @AuraEnabled
        public string srId { get; set; }
        
        @AuraEnabled
        public string commandButtonId { get; set; }
        
        @AuraEnabled
        public string LicenseId { get; set; }
        
        public RequestWrap() {
        }
    }
    
    public class RespondWrap {
        @AuraEnabled
        public list<License_Activity__c> listLiscAct { get; set; }
        @AuraEnabled
        public HexaBPM__Section_Detail__c ButtonSection { get; set; }
        @AuraEnabled
        public string licenseApplicationId { get; set; }
        @AuraEnabled
        public boolean hasError { get; set; }
        @AuraEnabled
        public string ErrorMessage { get; set; }
        @AuraEnabled
        public string uploadedFileName { get; set; }
        
        @AuraEnabled
        public string SearchBarLable { get; set; }
        @AuraEnabled
        public HexaBPM__Service_Request__c serviceObj { get; set; }
        public RespondWrap() {
            hasError = false;
            serviceObj = new HexaBPM__Service_Request__c();
            ErrorMessage = '';
            listLiscAct = new list<License_Activity__c>();
        }
    }
    
    
    /*Contains logged in user Id*/
    public static string userId = UserInfo.getUserId();
    
    /*Contains logged in user type*/
    String userType = UserInfo.getUserType();
    
    /*Contains logged in user contactID*/
    public static ID userContactId;
    // current logged in user account detail
    public static string userAccountID;
    
    public static License__c objLicense;
    
    @AuraEnabled
    public static RespondWrap getLicenseActivityData(string requestWrapParam) {
        
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap = new RespondWrap();
        try{
            if(requestWrapParam != null && requestWrapParam != '') {
                //deseriliaze.
                reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
                List<HexaBPM__Section_Detail__c> sectionList = new List<HexaBPM__Section_Detail__c>( [SELECT id, HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection'
                                          AND HexaBPM__Section__r.HexaBPM__Page__c = :reqWrap.pageId LIMIT 1]);
                if(sectionList!= null && sectionList.size() > 0){
                     respWrap.ButtonSection = sectionList[0];
                }
                
                if(reqWrap.srId != null && reqWrap.srId != '') {
                    string AccountId;
                    string ServiceRequestId = reqWrap.srId;
                    
                    //1.0                                     
                    for(User usrObj : OB_AgentEntityUtils.getUserById(userInfo.getUserId())) {
                        //1.1
                        if(usrObj.ContactId!=null && usrObj.contact.AccountId!=null) {
                            AccountId = usrObj.contact.AccountId;
                        }
                    }
                    
                    
                    string licenseApplicationId;
                    if(AccountId != null && ServiceRequestId != null) {
                        //objLicense = new License__c();
                        
                        for(HexaBPM__Service_Request__c SR :[Select Id, License_Application__c, Buying_Selling_Real_Estate_Property__c, Dealing_in_Precious_Metals__c, DNFBP__c, Setting_Up__c, 
                                                             Category_of_Service__c, Category_of_Service_Explanation__c, legal_structures__c, 
                                                             Nature_of_Business__c, Parent_or_Entity_Business_Activities__c, 
                                                             Business_Sector__c, Entity_Type__c, RecordType.DeveloperName
                                                             from HexaBPM__Service_Request__c
                                                             where Id = :ServiceRequestId
                                                             /* and HexaBPM__Customer__c = :AccountId */
                                                            ]) {
                                                                
                                                                
                                                                respWrap.serviceObj = SR;
                                                                
                                                                if(SR.legal_structures__c == 'Company' || SR.legal_structures__c == 'Partnership') {
                                                                    respWrap.SearchBarLable = 'Select business activities';
                                                                } else if(SR.legal_structures__c == 'Foundation') {
                                                                    respWrap.SearchBarLable = 'Select foundation objects';
                                                                } else if(SR.legal_structures__c == 'Non Profit Incorporated Organisation (NPIO)') {
                                                                    respWrap.SearchBarLable = 'Select Activities';
                                                                }
                                                                
                                                            }
                        
                        
                    }
                    system.debug('=======AccountId======' + AccountId);
                    
                    if(ServiceRequestId != null) {
                        respWrap.listLiscAct = getSRLicenseActivity(ServiceRequestId);
                    } else {
                        respWrap.listLiscAct = new list<License_Activity__c>();
                    }
                    respWrap.licenseApplicationId = licenseApplicationId;
                }
            } else {
                respWrap.hasError = true;
                respWrap.ErrorMessage = 'Application Id is missing.';
            }
        } catch(DMLException e) {
            respWrap.hasError = true;
            respWrap.ErrorMessage = e.getMessage() + '';
            //return respWrap;
        }
        
        for(HexaBPM__SR_Doc__c srDoc :[SELECT id, File_Name__c, HexaBPM__Document_Master__r.HexaBPM__Code__c
                                       FROM HexaBPM__SR_Doc__c
                                       WHERE HexaBPM__Service_Request__c = :reqWrap.srId
                                       AND HexaBPM_Amendment__c = null
                                       AND HexaBPM__Document_Master__r.HexaBPM__Code__c in('Federal_or_local_government_approval')
                                       LIMIT 1
                                      ]) {
                                          
                                          respWrap.uploadedFileName = srDoc.File_Name__c;
                                      }
        return  respWrap;
        
    }
    public static list<License_Activity__c> getSRLicenseActivity(string applicationId) {
        return [select Id, Name, Activity__c, Activity__r.Name, Account__c, Activity__r.Activity_Description_english__c, Activity__r.Activity_Code__c, 
                Account__r.Name, Sector_Classification__c, Activity__r.OB_DNFBP__c, Activity__r.Sys_Code__c, Activity__r.OB_Sell_or_Buy_Real_Estate__c, Activity__r.OB_Trade_Precious_Metal__c
                from License_Activity__c where Application__c = :applicationId ORDER BY Activity__r.Name ASC];
    }
    @AuraEnabled
    public static list<License_Activity_Master__c> getLicenseActivityMaster(String srId, String searchKeyWord,HexaBPM__Service_Request__c childSRObj ) {
        String searchKey = searchKeyWord + '%';
        system.debug('=======searchKey======' + searchKey);
        system.debug('=======srID======' + srID);
         system.debug('=======srID======' + childSRObj);
        string userAccountID;
        string EntityType;
        boolean LeaseAKiosk = false;
        boolean sectorCheck = false;
        boolean nonFinancialFilter = false;
        List<String> listOfSectorToBeChecked = new List<String>{ 'Non Profit Incorporated Organisation (NPIO)', 'Foundation', 'Single Family Office', 'Prescribed Companies', 'General Partner for a Limited Partnership Fund', 'Investment Fund' };
            string businessSector;
        
        if(childSRObj != null) {
                EntityType = childSRObj.Entity_Type__c;
                businessSector = childSRObj.Business_Sector__c;
        }
        
        if(businessSector != null){
            sectorCheck = listOfSectorToBeChecked.contains(businessSector);
        }
        System.debug(sectorCheck);
        
        
        //1.0
        for(User usrObj : OB_AgentEntityUtils.getUserById(userInfo.getUserId())) {
            //1.1
            if(usrObj.ContactId!=null && usrObj.contact.AccountId!=null) {
                userAccountID = usrObj.contact.AccountId;
                userContactId = usrObj.contactid;
            }
        }
        
        string LicenseId;

        set<Id> setExistingActivities = new set<Id>();
        set<Id> childSRId = new set<Id>();
        
      /*  if(srId != null) {
            for(HexaBPM__Service_Request__c sr:[SELECT Id FROM HexaBPM__Service_Request__c WHERE HexaBPM__Parent_SR__c =:srId]){
            childSRId.add(sr.Id);
        }
            
            for(License_Activity__c LA :[Select Activity__c from License_Activity__c where Application__c IN :childSRId]) {
                setExistingActivities.add(LA.Activity__c);
            }
        }*/
        
        string strSearchText = '%' + searchKey + '%';
        list<License_Activity_Master__c> licenseActivityMasterList = new list<License_Activity_Master__c>();
        list<License_Activity_Master__c> licenseActivityMasterListFiltered = new list<License_Activity_Master__c>();
        
        list<License_Activity_Master__c> licenseActivityMasterListFilteredNonFinancial = new list<License_Activity_Master__c>();
        
        for(License_Activity_Master__c LAM :[Select Id, Name, Activity_Description_english__c, 
                                             OB_DNFBP__c, OB_Sector_Classification__c, Sector_Classification__c, 
                                             OB_Trade_Precious_Metal__c , OB_Government_Entity_Approval_Required__c, 
                                             OB_Sell_or_Buy_Real_Estate__c, Sys_Code__c, OB_Lease_a_kiosk__c, Activity_Code__c
                                             from  License_Activity_Master__c where Name LIKE :strSearchText
                                             AND Sys_Activity_Type__c = :EntityType
                                             //AND Sector_Classification__c = :businessSector
                                             AND Enabled__c = true
                                             AND ID NOT IN :setExistingActivities ORDER BY Name ASC]) {
                                                 //
                                                
                                                     licenseActivityMasterList.add(LAM);
                                     
                                             }
        
        if(sectorCheck) {
            for (License_Activity_Master__c LAM :licenseActivityMasterList) {
                
                if(LAM.OB_Sector_Classification__c == businessSector) {
                    licenseActivityMasterListFiltered.add(LAM);
                }
                
            }
        }
        
        
        // NON financial foundation activity hiding for sr with bussiness sector other than  foundation
        if(EntityType != null && EntityType == 'Non - financial' && (businessSector == null || (businessSector != null && businessSector != 'Foundation'))) {
            nonFinancialFilter = true;
        }
        
        if(sectorCheck) {
            if(nonFinancialFilter) {
                for (License_Activity_Master__c LAM :licenseActivityMasterListFiltered) {
                    
                    if(LAM.OB_Sector_Classification__c != 'Foundation') {
                        licenseActivityMasterListFilteredNonFinancial.add(LAM);
                    }
                    
                }
                return licenseActivityMasterListFilteredNonFinancial;
            } else {
                return licenseActivityMasterListFiltered;
            }
            
        } else {
            if(nonFinancialFilter) {
                for (License_Activity_Master__c LAM :licenseActivityMasterList) {
                    
                    if(LAM.OB_Sector_Classification__c != 'Foundation') {
                        licenseActivityMasterListFilteredNonFinancial.add(LAM);
                    }
                    
                }
                return licenseActivityMasterListFilteredNonFinancial;
                
            } else {
                return licenseActivityMasterList;
            }
            
        } 
        
    }
    
    @AuraEnabled
    public static License_Activity__c selectLicenseActivity(string ActivityId) {
        License_Activity__c licenseObj = new License_Activity__c();
    	 for(License_Activity_Master__c LAM :[Select Id, Name, Activity_Description_english__c, 
                                             OB_DNFBP__c, OB_Sector_Classification__c, Sector_Classification__c, 
                                             OB_Trade_Precious_Metal__c , OB_Government_Entity_Approval_Required__c, 
                                             OB_Sell_or_Buy_Real_Estate__c, Sys_Code__c, OB_Lease_a_kiosk__c, Activity_Code__c
                                              from  License_Activity_Master__c where Id=:ActivityId]) {
                                                  licenseObj.Activity__r = LAM;
                                              }
         return licenseObj;                                  
    }
    @AuraEnabled
    public static RespondWrap saveLicenseActivity(List<String> activityList, HexaBPM__Service_Request__c srRecord,String srId) {
        RespondWrap respWrap = new RespondWrap();
        list<License_Activity__c> licenseActivityList = new list<License_Activity__c>();
        list<String> actIdList = new list<String>();
        string userAccountID = '';
        string contactId = '';
        system.debug('activityList==>'+activityList);

        boolean isNew = srRecord.Id == null ? true:false;
        try{
            //1.0
            for(User usrObj : OB_AgentEntityUtils.getUserById(userInfo.getUserId())) {
                //1.1
                if(usrObj.ContactId!=null && usrObj.contact.AccountId!=null) {
                    userAccountID = usrObj.contact.AccountId;
                    contactId = usrObj.ContactId;
                }
            }
            if(srRecord != null){

                HexaBPM__Service_Request__c parenrSr = new HexaBPM__Service_Request__c();
                for(HexaBPM__Service_Request__c srObj : [select id,Entity_Type__c,Business_Sector__c,HexaBPM__Send_SMS_to_Mobile__c,HexaBPM__Email__c FROM 
                HexaBPM__Service_Request__c WHERE id = :srId ]){
                    parenrSr = srObj; 
                }
                if(parenrSr.Id != null){
                    //create account
                Account acc = new Account();
                acc.Name = srRecord.Entity_Name__c;
                acc.Description = srRecord.Id;
                acc.Company_Type__c = srRecord.Entity_Type__c;
                acc.OB_Sector_Classification__c = srRecord.Business_Sector__c;
                if(!Test.isRunningTest())
                    acc.OwnerId = label.Portal_User_Default_Owner;
                acc.ROC_Status__c = 'Under Formation';
                acc.Priority_to_DIFC__c = 'Standard';
                acc.Mobile_Number__c = parenrSr.HexaBPM__Send_SMS_to_Mobile__c;
                acc.RecordTypeId =  OB_QueryUtilityClass.getRecordtypeID ('Account','Business_Development');
                acc.OB_Is_Multi_Structure_Application__c = 'Yes';
                
                if(parenrSr.Entity_Type__c!='Financial - related' && parenrSr.Business_Sector__c!='Investment Fund'){
                    if(parenrSr.Entity_Type__c =='Non - financial'){
                        acc.BD_Sector__c = 'Corporates and Service Provider';
                    }else if(parenrSr.Entity_Type__c == 'Retail'){
                        acc.BD_Sector__c = 'Retail (RET)';
                    }
                }else{
                    acc.BD_Sector__c = 'Banks';
                }

                upsert acc;


                srRecord.HexaBPM__Parent_SR__c = srId;
                srRecord.HexaBPM__Customer__c =acc.Id;
                srRecord.HexaBPM__Contact__c = contactId;
                srRecord.HexaBPM__Email__c = parenrSr.HexaBPM__Email__c;
                srRecord.RecordTypeId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('Multi Structure').getRecordTypeId();
               	upsert srRecord; 
                respWrap.serviceObj = srRecord;

                
                for(License_Activity__c actObj : [Select id,Activity__c from License_Activity__c WHERE 	Application__c =:srRecord.Id ]){
                    actIdList.add(actObj.Activity__c);
                }

                for(string act :activityList){
                    if(!actIdList.contains(act)){
                        License_Activity__c la = new License_Activity__c();
                        la.Activity__c = act;
                        la.Account__c = /* userAccountID */acc.Id;
                        la.Application__c = srRecord.Id;
                        licenseActivityList.add(la); 
                    }
	               
            	}
                if(licenseActivityList.size() > 0) {
                    insert licenseActivityList;
                    respWrap.listLiscAct = licenseActivityList;
                }
                if(isNew == true){
                    //create company name record
                Company_Name__c compNameObj = new Company_Name__c ();
                compNameObj.Entity_Name__c = srRecord.Entity_Name__c;
                compNameObj.Application__c = srRecord.Id;
                insert compNameObj;
                }
                
            
            
                }else{
                    respWrap.hasError = true;
            respWrap.ErrorMessage = 'No parent sr found';
                }
                
                
            }
            
            	
        } catch(Exception ex) {
            respWrap.hasError = true;
            respWrap.ErrorMessage = ex.getMessage();
        }
        return respWrap;
    }
    
    @AuraEnabled
    public static RespondWrap deleteLicenseActivity(string LicenseId, string srID, string licenseAppId) {
        RespondWrap respWrap = new RespondWrap();
        if(LicenseId != null && srID != null) {
            try{
                list<License_Activity__c> delLicenseActivity = [Select Id, Activity__c, Account__c, Account__r.Name, Sector_Classification__c, Activity__r.Name, License__c, Activity_Description_english__c from License_Activity__c where Id = :LicenseId];
                string strLicenseId;
                if(delLicenseActivity != null && delLicenseActivity.size() > 0) {
                    strLicenseId = delLicenseActivity [0].License__c;
                    delete delLicenseActivity;
                }
                if(strLicenseId != null && srID != null) {
                    set<string> setGovernmentApproval = new set<string>();
                    string AccountId;
                    string GovtApprovalText = '';
                    string BDFinancialServicesActivities = '';
                    
                    HexaBPM__Service_Request__c objSRTBU = new HexaBPM__Service_Request__c(Id = srID, Has_Exempt_Activity__c = false);
                    for(License_Activity__c LA :[Select Id, Activity_Name_Eng__c, Application__c, Application__r.HexaBPM__Customer__c, Activity__r.OB_DNFBP__c, DED_Activity_Code__c, Activity__r.Activity_Code__c, Activity_Description_english__c, Activity__r.OB_Government_Entity_Approval_Required__c from License_Activity__c where Application__c = :srID]) {
                        
                        if(BDFinancialServicesActivities != '')
                            BDFinancialServicesActivities = LA.Activity_Name_Eng__c;
                        else 
                            BDFinancialServicesActivities = BDFinancialServicesActivities + ';' + LA.Activity_Name_Eng__c;
                        
                        if(LA.Application__c != null)
                            AccountId = LA.Application__r.HexaBPM__Customer__c;
                        
                        if(LA.Activity__r.Activity_Code__c == 'EXE_COM')
                            objSRTBU.Has_Exempt_Activity__c = true;
                        if(LA.Activity__r.OB_Government_Entity_Approval_Required__c != null)
                            setGovernmentApproval.add(LA.Activity__r.OB_Government_Entity_Approval_Required__c);
                        if(LA.DED_Activity_Code__c == 'RB')
                            setGovernmentApproval.add(LA.DED_Activity_Code__c);
                    }
                    for(string strVal :setGovernmentApproval) {
                        if(GovtApprovalText == '')
                            GovtApprovalText = strVal;
                        else 
                            GovtApprovalText = GovtApprovalText + ',' + strVal;
                    }
                    
                    if(AccountId != null) {
                        Account acc = new Account(Id = AccountId);
                        acc.BD_Financial_Services_Activities__c = BDFinancialServicesActivities;
                        update acc;
                    }
                    
                    objSRTBU.Government_Entity_Approval_Required__c = GovtApprovalText;
                    update objSRTBU;
                    OB_RiskMatrixHelper.CalculateRisk(objSRTBU.Id);
                }
            } catch(DMLException e) {
                respWrap.hasError = true;
                string DMLError = e.getdmlMessage(0) + '';
                if(DMLError == null)
                    DMLError = e.getMessage() + '';
                
                respWrap.ErrorMessage = DMLError;
            }
        }
        respWrap.listLiscAct = getSRLicenseActivity(srID);
        return respWrap;
    }
    
    public static void saveSRDoc(string srId, map<string, string> docMap) {
        
        //delete the previous SR Docs for the same parent and documentMasterCode
        if(docMap != null && docMap.size() > 0)
            delete [select id from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c = :srId and HexaBPM__Document_Master__r.HexaBPM__Code__c in :docMap.keySet()];
        
        OB_QueryUtilityClass.createCompanySRDoc(srId, null, docMap);
    }
    
    @AuraEnabled
    public static ButtonResponseWrapper getButtonAction(string SRID, HexaBPM__Service_Request__c serviceRequest, string ButtonId, string pageId, map<string, string> docMap) {
        
        ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
        string DMLError;
        HexaBPM__Service_Request__c objRequest;
        try{
            if(serviceRequest != null) {
                serviceRequest = OB_QueryUtilityClass.setPageTracker(serviceRequest, SRID, pageId);
                update serviceRequest;
                System.debug(docMap);
                
                if(docMap != null) {
                    saveSRDoc(SRID, docMap);
                }
                
            }
            OB_RiskMatrixHelper.CalculateRisk(SRID);
            objRequest = OB_QueryUtilityClass.QueryFullSR(SRID);
            PageFlowControllerHelper objPB = new PageFlowControllerHelper();
            PageFlowControllerHelper.objSR = objRequest;
            
            
            PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
            system.debug('@@@@@@@@2 responseNextPage ' + responseNextPage);
            respWrap.pageActionName = responseNextPage.pg;
            respWrap.communityName = responseNextPage.communityName;
            respWrap.CommunityPageName = responseNextPage.CommunityPageName;
            respWrap.sitePageName = responseNextPage.sitePageName;
            respWrap.strBaseUrl = responseNextPage.strBaseUrl;
            respWrap.srId = objRequest.Id;
            respWrap.flowId = responseNextPage.flowId;
            respWrap.pageId = responseNextPage.pageId;
            respWrap.isPublicSite = responseNextPage.isPublicSite;
            
        } catch(DMLException e) {
            DMLError = e.getdmlMessage(0) + '';
            if(DMLError == null) {
                DMLError = e.getMessage() + '';
            }
        }
        if(DMLError != null && DMLError != '')
            respWrap.errorMessage = DMLError;
        system.debug('@@@@@@@@2 respWrap.pageActionName ' + respWrap.pageActionName);
        
        return respWrap;
    }
    public class ButtonResponseWrapper {
        @AuraEnabled public String pageActionName { get; set; }
        @AuraEnabled public string communityName { get; set; }
        @AuraEnabled public String errorMessage { get; set; }
        @AuraEnabled public string CommunityPageName { get; set; }
        @AuraEnabled public string sitePageName { get; set; }
        @AuraEnabled public string strBaseUrl { get; set; }
        @AuraEnabled public string srId { get; set; }
        @AuraEnabled public string flowId { get; set; }
        @AuraEnabled public string pageId { get; set; }
        @AuraEnabled public HexaBPM__Service_Request__c updatedSr { get; set; }
        @AuraEnabled public boolean isPublicSite { get; set; }
        
    }
}