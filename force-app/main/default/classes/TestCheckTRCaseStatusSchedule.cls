/******************************************************************************************
 *  Test Class Name : Test class for CheckTRCaseStatusSchedule class
 *  Date     : 24/SEP/2019
 *  Description : 
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By          Description
 ----------------------------------------------------------------------------------------              
 V1.0   24/SEP/2019      Sai              
*******************************************************************************************/


@isTest
private class TestCheckTRCaseStatusSchedule {
	
    static testMethod void myUnitTest() {
		
		Thompson_Reuters_Check__c trCheck = new Thompson_Reuters_Check__c();
		trCheck.TRCase_No__c = '9988';
		trCheck.TR_Case_Status__c = 'Open';
		INSERT trCheck;
		
		Test.startTest();
			CheckTRCaseStatusSchedule sh1 = new CheckTRCaseStatusSchedule();
			String sch = '0 0 23 * * ?'; 
			system.schedule('Test TR Check', sch, sh1); 
		Test.stopTest();         
    }
}