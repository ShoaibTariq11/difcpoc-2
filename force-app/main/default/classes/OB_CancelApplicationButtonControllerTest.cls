/*
    Author      : Leeba
    Date        : 30-March-2020
    Description : Test class for OB_CancelApplicationButtonController
    --------------------------------------------------------------------------------------
*/
@isTest
public class OB_CancelApplicationButtonControllerTest {
    
      public static testmethod void testMethod1(){
      
        Account acc  = new Account();
        acc.name = 'test';      
        insert acc;
       
       contact con = new Contact();
       con.LastName = 'test';
       con.FirstName = 'test';
       con.Email = 'test@test.com';
       insert con;   

        Profile p = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community Plus User Custom']; 
         
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com',contactid = con.Id);
       
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
       insert objsrTemp;
       
       
       Opportunity opp = new Opportunity();
       opp.Name = 'test';
       opp.Stagename = 'Created';
       opp.closedate = system.today();
       opp.AccountId = acc.id;
       insert opp;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        objHexaSR.Opportunity__c = opp.id;
        insert objHexaSR;
        
        HexaBPM__SR_Status__c objstatus = new HexaBPM__SR_Status__c();
        objstatus.HexaBPM__Code__c = 'NOT_INTERESTED';       
        insert objstatus;
        
        HexaBPM__Status__c objhexastatus = new HexaBPM__Status__c();
        objhexastatus.HexaBPM__Type__c = 'Intermediate';
        objhexastatus.HexaBPM__Code__c = 'abcd';
        insert objhexastatus;
        
        HexaBPM__Step__c objstep = new HexaBPM__Step__c();
        objstep.HexaBPM__Status__c = objhexastatus.id;
        objstep.HexaBPM__SR__c = objHexaSR.id;
        insert objstep;
        
        HexaBPM__SR_Status__c objstatus1 = new HexaBPM__SR_Status__c();
        objstatus1.HexaBPM__Code__c = 'CANCELLED';
        insert objstatus1;
        test.startTest();
        
        OB_CancelApplicationButtonController.RequestWrap reqWrapper = new OB_CancelApplicationButtonController.RequestWrap();
        reqWrapper.srId = objHexaSR.id;
        reqWrapper.reasonForCancellation = 'test';
        reqWrapper.cancelationType = 'NOT_INTERESTED';
        
        OB_CancelApplicationButtonController.RespondWrap restWrapTest = new OB_CancelApplicationButtonController.RespondWrap();
        String requestString = JSON.serialize(reqWrapper);
        restWrapTest = OB_CancelApplicationButtonController.applicationStatusCheck(requestString);
        restWrapTest = OB_CancelApplicationButtonController.cancelApplication(requestString);
      }
}