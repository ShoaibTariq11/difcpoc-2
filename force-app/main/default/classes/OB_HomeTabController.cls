/**
 * Description : used to controll the vat rgistration component
 *
 * ****************************************************************************************
 * History :
 * [31.OCT.2019] Prateek Kadkol - Code Creation
 */
public without sharing class OB_HomeTabController {

@AuraEnabled
public static RespondWrap getConDetails() {

    RespondWrap respWrap = new RespondWrap();
    respWrap.ShowVATComponent = false;
    
    for(user loggedinUser :OB_AgentEntityUtils.getUserById(userInfo.getUserId())) {
        if(loggedinUser.ContactId!=null && loggedinUser.contact.AccountId!=null) {
            if(loggedinUser.contact.Account.Tax_Registration_Number_TRN__c == null) {
                if(loggedinUser.contact.Account.VAT_Info_Updated_DateTime__c == null || loggedinUser.contact.Account.VAT_Info_Updated_DateTime__c.date().daysBetween(system.now().date()) > 0) {
                    if(loggedinUser.contact.Account.Is_Commercial_Permission__c != 'Yes'){
                        respWrap.ShowVATComponent = true;
                    }
                    
                }
            }

            respWrap.loggedInUser = loggedinUser;
        }
    }
    return respWrap;
}
@AuraEnabled
public static RespondWrap updateAccountVatRegistrationNumber(string regestrationNumber) {

    RespondWrap respWrap = new RespondWrap();
    try {
        string accountId = '';
        for(user loggedinUser :OB_AgentEntityUtils.getUserById(userInfo.getUserId())) {
            if(loggedinUser.ContactId!=null && loggedinUser.contact.AccountId!=null) {
                accountId = loggedinUser.contact.AccountId;
            }


        }

        if(accountId != null || accountId != '') {
            account accObj = new account(id = accountId);
            accObj.Tax_Registration_Number_TRN__c = regestrationNumber;

            update accObj;
            respWrap.loggedInContactAccount = accObj;

        } else {
            respWrap.errorMessage = 'account id is null';
        }
    } catch(exception e) {
        respWrap.errorMessage = e.getMessage();
    }

    return respWrap;
}



@AuraEnabled
public static RespondWrap updateLoggedinContact(user loggedInUser) {

    RespondWrap respWrap = new RespondWrap();

    respWrap.errorMessage = loggedInUser.contact.AccountId;

    /* if(loggedInUser.Contact.AccountId != null) {
       account accObj = new account(id = loggedInUser.contact.AccountId);
       accObj.VAT_Info_Updated_DateTime__c = system.now();

       try {
       update accObj;
       respWrap.loggedInContactAccount = accObj;
       }

       catch(exception e) {
       respWrap.errorMessage = e.getMessage();
       }

       } else {
       respWrap.errorMessage = 'account id is null';
       }


       if(respWrap.errorMessage == null) {
       respWrap = getConDetails();
       } */



    return respWrap;
}

// ------------ Wrapper List ----------- //



public class RespondWrap {

@AuraEnabled public Boolean ShowVATComponent { get; set; }
@AuraEnabled public user loggedInUser { get; set; }
@AuraEnabled public account loggedInContactAccount { get; set; }
@AuraEnabled public string errorMessage { get; set; }

public RespondWrap() {
}
}
}