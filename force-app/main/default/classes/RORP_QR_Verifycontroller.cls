public class RORP_QR_Verifycontroller {

   // public Lease__c ObjLease{get;set;}
    
     public string ObjLeaseStatus{get;set;}
      public string ObjLeaseName{get;set;}
     public date Start_datec{get;set;}
     public date End_Date{get;set;}
       
       public void PageLoad()
    {
        loadDocument(apexpages.currentPage().getparameters().get('ref'));
         
    }
         
         
    public void loadDocument(String ReferenceNumber)
    {
        System.debug(ReferenceNumber);
        if(ReferenceNumber!=null && ReferenceNumber!='')
        {
            if(ReferenceNumber.length()<30)
            {
            List<Lease__c> listLease=[select id,name, Status__c,Lease_Types__c, Start_date__c, End_Date__c from Lease__c where Name =:ReferenceNumber limit 1];
            if(!listLease.isEmpty())
            {
                ObjLeaseStatus=listLease[0].Status__c;
                   Start_datec=listLease[0].Start_date__c;
                      End_Date=listLease[0].End_Date__c ;
                      ObjLeaseName=listLease[0].name;
             }
                
            }
            
        }
        
    }
         
   
}