/******************************************************************************************
 *  Name        : CRM_cls_OpportunityUtils 
 *  Author      : Claude Manahan
 *  Company     : PWC Digital Services
 *  Date        : 2017-2-3
 *  Description : Utility class for Opportunity transactions for CRM
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   02-03-2017   Claude        Created
 V1.1   15-03-2017   Claude        Added new mapping for DFSA license issued
 V1.2   30-07-2017   Claude        Added logic to check the email messages in an opportunity for retail accounts
 V1.3   08-08-2017   Claude        Added check for email messages to prevent blank errors due to blank RelatedToIds
 V1.4   10-08-2017   Claude        Added logic for approval step in opportunity
 V1.5   15-03-2018   Claude        Added logic for to copy opportunity's attachment to apporpriate steps
 V1.6   04-04-2018   Danish        Added logic for NFS security to change the security status in to in progress stage tkt # 4473
 V1.7   17-05-2018   Arun          Added logic to send Email when Freehold Transfer is approved ticket #4919
*******************************************************************************************/
global class CRM_cls_OpportunityUtils {



   /**
     * V1.7
     * Send genearetd 
     * email message to the approriated step
     * @params      ste         The step record
     * @return      strResult   The result of the transaction
     
     */
    public static String SendRORPGeneratedDoc(Step__c stp)
    {
        string strResult = 'Success';
    
        return strResult ;
    }
    
    
    /**
     * Controls the execution of methods, 
     * especially in triggers
     */
    static boolean hasExecuted = false;
    
   /**
     * V1.4
     * Copies the attachments from the most recent
     * email message to the approriated step
     * @params      ste         The step record
     * @return      strResult   The result of the transaction
     
     */
    public static String copySecurityEmailAttachments(Step__c stp)
    {
        
    
        
        string strResult = 'Success';
        
        try{
        
            String accountId = stp.SR__r.Customer__c;
            
            List<Opportunity> retailOpportunities = [SELECT Id FROM Opportunity WHERE AccountId = :accountId AND Account.Company_Type__c = 'Retail'];
            
                System.debug('===retailOpportunities>'+retailOpportunities);
                
            if(!retailOpportunities.isEmpty()){
                
                String contactId = '';
                
                for(CRM_Email_Template_Code__mdt c : CRM_cls_EmailUtils.getAllTemplateConfigurations()){
                
                    if(String.isNotBlank(c.Point_of_Origin__c) && c.Point_of_Origin__c.equals('Security') && String.isNotBlank(c.Approver_Id__c)){
                    
                        contactId = c.Approver_ID__c;
                         System.debug('===contactId>'+contactId);
                        break;
                    }   
                    
                }
                
                if(string.isNotBlank(contactId)){
                    
                    List<Attachment> emailMessageAttachments = CRM_cls_EmailUtils.getEmailAttachmentsByRecipient(retailOpportunities[0].Id,contactId);
                    System.debug('===emailMessageAttachments>'+emailMessageAttachments);
                    
                    if(!emailMessageAttachments.isEmpty()){
                        
                        for(Attachment a : emailMessageAttachments){
                                
                            a.ParentId = stp.Id;
                            
                        }
                        
                         insert emailMessageAttachments;
                    }
                
                }
 
            }
        
        } catch (Exception e){
            
            string DMLError = e.getdmlMessage(0)+'';
            
            if(DMLError==null) DMLError = e.getMessage()+'';
            
            strResult = DMLError;
            
        }
        
        return strResult;
        
    }
    
    /**
     * V1.4
     * Copies the attachment from opportunity to appropriate step
     * @params      ste         The step record
     * @return      strResult   The result of the transaction
     */
    
     public static String copyEmailAttachmentsFromOpportunity(Step__c stp)
    {
        
    
        
        string strResult = 'Success';
        
        try{
        
            String accountId = stp.SR__r.Customer__c;
            
            List<Opportunity> retailOpportunities = [SELECT Id FROM Opportunity WHERE AccountId = :accountId AND Account.Company_Type__c = 'Retail'];
            
                System.debug('===retailOpportunities>'+retailOpportunities);
                
            if(!retailOpportunities.isEmpty()){
                   
                List<Attachment> opportunityAttachments = CRM_cls_Utils.getAttachmentsByRecord(retailOpportunities[0].Id);
                System.debug('===opportunityAttachments>'+opportunityAttachments);
                
                if(!opportunityAttachments.isEmpty()){
                    
                    
                     for(Attachment a : opportunityAttachments){
                                
                            a.ParentId = stp.Id;
                            
                        }
                        
                    insert opportunityAttachments;
                }
 
            }
        
        } catch (Exception e){
            
            string DMLError = e.getdmlMessage(0)+'';
            
            if(DMLError==null) DMLError = e.getMessage()+'';
            
            strResult = DMLError;
            
        }
        
        return strResult;
        
    }
  
    /**
     * V1.2
     * Checks the format of the email being sent for opportunities
     * @params      emailMessageList        The list of email messages to be sent
     */
    public static void validateOpportunityEmailMessages(List<EmailMessage> emailMessageList){
        
        Set<Id> oppIds = new Set<ID>();
    
        for(EmailMessage e : emailMessageList){
            
            if(String.isNotBlank(e.RelatedToId) &&                  // V1.3 - Claude - Added null check 
                String.valueOf(e.relatedtoId).startsWith('006')){
                
                oppIds.add(e.RelatedToId);
                
            }
            
        }
        
        if(!oppIds.isEmpty()){
            
            Map<String,CRM_Email_Template_Code__mdt> emailTemplateCodeSet = new Map<String,CRM_Email_Template_Code__mdt>();
            
            for(CRM_Email_Template_Code__mdt emailTemplateCode : CRM_cls_EmailUtils.getAllTemplateConfigurations()){
                
                emailTemplateCodeSet.put(emailTemplateCode.DeveloperName,emailTemplateCode);
                
            }
            
            Set<String> oppChecklistIds = new Set<String>();
            
            List<Opportunity> oppsToUpdate = [SELECT Id, Lead_email__c, Company_Name_Approval__c, Record_Type_Name__c, LAF_Approval__c, Legal_Approval__c, RRC_Approval__c, Security_Check__c, Thompson_Reuters__c, StageName, Stage_Steps__c, Account.Company_Type__c FROM Opportunity WHERE id IN : oppIds AND Check_Validation_on_Email__c = true];
            
            for(Opportunity o : oppsToUpdate){
        
                for(EmailMessage e : emailMessageList){
                    
                    String selectedEmailTemplateCode = '';
                    
                    List<CRM_Email_Template_Code__mdt> selectedTemplates = new List<CRM_Email_Template_Code__mdt>();
                    system.debug('$$$$' + e.htmlBody);
                    
                    for(CRM_Email_Template_Code__mdt k : emailTemplateCodeSet.values()){
                        
                        //String decryptedEmailTemplateCode = PasswordCryptoGraphy.DecryptPassword(k.Email_Template_Code__c);
                       system.debug('^^^^^^^' + k.Email_Template_Code__c);
                        if(e.htmlBody.contains(k.Email_Template_Code__c)){
                            
                            selectedTemplates.add(k);
                        }
                    }
                    
                    if(!selectedTemplates.isEmpty()){
                        
                        Boolean usedCorrectTemplate = false;
                        
                        String stageStep = String.isNotBlank(o.Stage_Steps__c) ? o.Stage_Steps__c : '';
                        
                        for(CRM_Email_Template_Code__mdt selectedTemplate : selectedTemplates){
                            
                            usedCorrectTemplate = ((selectedTemplate.Client__c && String.isNotBlank(o.Lead_Email__c) && e.ToAddress.contains(o.Lead_Email__c)) || 
                                (!selectedTemplate.Client__c && String.isNotBlank(selectedTemplate.Approver_Email__c) && e.ToAddress.contains(selectedTemplate.Approver_Email__c))) &&
                                    o.StageName.equals(selectedTemplate.Opportunity_Stage__c) && ((String.isBlank(stageStep) && String.isBlank(selectedTemplate.Stage_Steps__c)) || stageStep.equals(selectedTemplate.Stage_Steps__c));

                            if(usedCorrectTemplate){
                                
                                selectedEmailTemplateCode = selectedTemplate.Email_Template_Code__c;
                                
                                break;  
                            } 
                        }
                        
                        Boolean hasError = false;
                        
                        if(!usedCorrectTemplate){
                            
                            for(CRM_Email_Template_Code__mdt selectedTemplate : selectedTemplates){
                            
                                List<CRM_Email_Template_Code__mdt> correctEmailTemplates = new List<CRM_Email_Template_Code__mdt>();
                                    
                                for(CRM_Email_Template_Code__mdt c : emailTemplateCodeSet.values()){
                                    
                                    if(String.isNotBlank(c.Opportunity_Stage__c) && o.StageName.equals(c.Opportunity_Stage__c) 
                                        && stageStep.equals(c.Stage_Steps__c)){
                                        
                                        correctEmailTemplates.add(c);
                                        
                                    }
                                    
                                }
                                
                                if(!correctEmailTemplates.isEmpty()){
                                    
                                    String errorMessage = '';
                                    
                                    for(CRM_Email_Template_Code__mdt c : correctEmailTemplates){
                                        
                                        errorMessage += 'Please select the correct contact ('+ (c.Client__c ? 'Client' : c.Approver_Name__c ) +') and email template (' + c.MasterLabel + ').' + '\n';
                                    
                                    }
                                    
                                    errorMessage = errorMessage.removeEnd('\n');
                                    
                                    e.addError(errorMessage);   
                                    
                                    hasError = true;
                                    
                                }
                            }
                            
                            if(!hasError) e.addError('Please select the correct template.');
                            
                        } else {
                            
                            //V1.4 - Claude - Start
                            if(String.isNotBlank(o.Account.Company_Type__c) && o.Account.Company_Type__c.equals('Retail') && o.Record_Type_Name__c.contains('Checklist')){
                
                                o.Company_Name_Approval__c = getStepApprovalStatus('Retail_Additional_Information_Company_Name_Approval',selectedEmailTemplateCode,o.Company_Name_Approval__c);
                                o.LAF_Approval__c = getStepApprovalStatus('Retail_Additional_Information_LAF_Approval',selectedEmailTemplateCode,o.LAF_Approval__c);
                                o.Legal_Approval__c = getStepApprovalStatus('Retail_Additional_Information_Legal_Approval',selectedEmailTemplateCode,o.Legal_Approval__c);
                                o.RRC_Approval__c = getStepApprovalStatus('Retail_Additional_Information_RRC_Approval',selectedEmailTemplateCode,o.RRC_Approval__c);
                                o.Security_Check__c = getStepApprovalStatus('Retail_Additional_Information_Security_Check',selectedEmailTemplateCode,o.Security_Check__c);
                                o.Thompson_Reuters__c = getStepApprovalStatus('Retail_Additional_Information_Thompson_Reuters',selectedEmailTemplateCode,o.Thompson_Reuters__c);
                                
                            }
                            //V1.4 - Claude - end
                            //V1.6 - Danish - start
                            else if(String.isNotBlank(o.Account.Company_Type__c) && o.Account.Company_Type__c.equals('Non - financial') &&  o.Record_Type_Name__c.equals('NFS')){
                            
                              
                            
                               if(emailTemplateCodeSet.containsKey('NFS_Security_Approval') && o.Security_Check__c  == NULL){
                                   
                                    o.Security_Check__c = emailTemplateCodeSet.get('NFS_Security_Approval').Security_Check__c;
                               }
                                
                            }
                             else if(String.isNotBlank(o.Account.Company_Type__c) && o.Account.Company_Type__c.equals('Financial - related') &&  o.Record_Type_Name__c.equals('BD_Financial_Fund_with_Security')){
                            
                              
                            
                               if(emailTemplateCodeSet.containsKey('Financial_Security_Approval') && o.Security_Check__c  == NULL){
                                   
                                    o.Security_Check__c = emailTemplateCodeSet.get('Financial_Security_Approval').Security_Check__c;
                               }
                                
                            }
                             //V1.6 - Danish - end
                            
                            e.htmlBody = e.htmlBody.remove(selectedEmailTemplateCode);
                            
                        }
                        
                    } else {
                        
                        e.addError('Please select the correct template.');
                        
                    }
                
                }
            
            }
            
            //V1.4 - Claude - Start
            update oppsToUpdate;
            //V1.4 - Claude - end
        }
        
    }
    
    private static String getStepApprovalStatus(String templateCode, String selectedTemplate, String originalVal){
        
        return  selectedTemplate.contains(templateCode) ? 'Additional Information Required' : originalVal;
        
    }
    
    /**
     * Updates the previous stage history record's
     * duration based on the days that have passed
     * to the most recent stage
     * @params      newStageHistoryRecords      List of new stage history records
     * @params      existingIds                 Set of most recently updated record IDs
     */
    public static void updatePreviousStageHistory(List<Opportunity_Stage_History__c> newStageHistoryRecords, Set<Id> existingIds){
    
        if(!hasExecuted){
        
            /* This will prevent this method from executing again when another DML is firing for this object */
            disableExecution();
            
            /* This will store the opportunity IDs */
            Set<String> relatedOppIds = new Set<String>();
        
            for(Opportunity_Stage_History__c o : newStageHistoryRecords){
                
                if(String.isNotBlank(o.Related_Opportunity__c)){
                    
                    relatedOppIds.add(o.Related_opportunity__c);
                    
                }
                
            }
            
            /* Query the Stage History, mapped per Opportunity for easy retrieval */
            Map<Id,Opportunity> relatedOpps = new Map<Id,Opportunity>([SELECT Id, 
                                                                              CloseDate,
                                                                              RecordType.DeveloperName, 
                                                                              (SELECT Id, 
                                                                              Name,
                                                                                      Stage__c, 
                                                                                      Stage_Duration__c, 
                                                                                      Related_Opportunity__c,
                                                                                      Stage_History_Last_Modified_Date__c
                                                                                      FROM Opportunity_Stage_History__r 
                                                                                      ORDER BY Stage_History_Last_Modified_Date__c DESC,
                                                                                      Name DESC) 
                                                                                      FROM Opportunity WHERE Id in :relatedOppIds]);
            
            /* This will store the stage history records we need to update */
            Set<Opportunity_Stage_History__c> relatedStageHistoryToUpdate = new Set<Opportunity_Stage_History__c>();
            
            Map<String,Set<String>> mappedOppStageHistoryPairs = new Map<String,Set<String>>();
            
            Map<String,Opportunity_Stage_History__c> mappedOppStageHistoryRecords = new Map<String,Opportunity_Stage_History__c>();
            
            /* If this is an INSERT operation, or the stage history record is not the one
             * that fired the operation, add the record to the list
             */
             
            List<Opportunity_Stage_History__c> listOfAllStageHistoryRecords = new List<Opportunity_Stage_History__c>();
            
            for(Opportunity o : relatedOpps.values()){ 
                if(!o.Opportunity_Stage_History__r.isEmpty()) listOfAllStageHistoryRecords.addAll(o.Opportunity_Stage_History__r);
            }
            
            /* This will store the stages where the duration will be specifically calculated based on the stage they are dependent to */
            Map<String,String> stagesToCheck = new Map<String,String>();
            
            stagesToCheck.put('Regulatory Business Plan (RBP) Received','Letter of Intent (LOI) Received');
            stagesToCheck.put('In principle issued by DFSA','Application Submitted to DFSA');
            stagesToCheck.put('Application Submitted to DFSA','Regulatory Business Plan (RBP) Received');
            stagesToCheck.put('DFSA License Issued','In principle issued by DFSA');     
            
            /* Pair each opportunity stage history record to their corresponding pair */
            for(Opportunity_Stage_History__c o : listOfAllStageHistoryRecords) mappedOppStageHistoryRecords.put(o.Name,o);
            
            if(existingIds.isEmpty()){
                
                for(Opportunity_Stage_History__c o : newStageHistoryRecords){
                
                    mappedOppStageHistoryRecords.put(o.Stage__c+o.Related_Opportunity__c,o);
                    
                    listOfAllStageHistoryRecords.add(o);
                    
                }   
                
            }
            
            Set<String> idSet;
            
            for(Opportunity_Stage_History__c o : listOfAllStageHistoryRecords){
                
                /* If the stage it represents is part of the specific stage list, process it according to state business rules */
                for(Opportunity_Stage_History__c o2 : listOfAllStageHistoryRecords){
                    
                    String key = existingIds.isEmpty() && String.isBlank(o.Name) ? o.Stage__c+o.Related_Opportunity__c : o.Name;
                    
                    idSet = mappedOppStageHistoryPairs.containsKey(key) ? mappedOppStageHistoryPairs.get(key) : new Set<String>();
                    
                    if(!key.equals(o2.Name) &&
                        o.Stage_History_Last_Modified_Date__c != null && 
                        o2.Stage_History_Last_Modified_Date__c != null &&
                        o.Related_Opportunity__c.equals(o2.Related_Opportunity__c) &&
                        o.Stage_History_Last_Modified_Date__c.date() > o2.Stage_History_Last_Modified_Date__c.date() &&
                        ( (stagesToCheck.containsKey(o.Stage__c) &&
                            o2.Stage__c.equals(stagesToCheck.get(o.Stage__c))) || !mappedOppStageHistoryPairs.containsKey(key))){
                                
                        idSet.add(o2.Name);
                        
                        if(( (stagesToCheck.containsKey(o.Stage__c) &&
                            o2.Stage__c.equals(stagesToCheck.get(o.Stage__c)) && !o.Stage__c.equals('DFSA License Issued'))) || 
                            (!mappedOppStageHistoryPairs.containsKey(o.Stage__c+o.Related_Opportunity__c) && existingIds.isEmpty()) ||
                            !mappedOppStageHistoryPairs.containsKey(o.Name) && !o.Stage__c.equals('DFSA License Issued') ){
                            
                            if(mappedOppStageHistoryPairs.containsKey(key)){
                                
                                for(String i : mappedOppStageHistoryPairs.get(key)){
                                
                                    Opportunity_Stage_History__c o3 = mappedOppStageHistoryRecords.get(i);
                                
                                    if(o3.Stage_History_Last_Modified_Date__c > o2.Stage_History_Last_Modified_Date__c){
                                        mappedOppStageHistoryPairs.put(key,new Set<String>{o3.Name});    
                                    }
                                    
                                }
                                
                            } else {
                                
                                mappedOppStageHistoryPairs.put(key,new Set<String>{o2.Name});        
                            }
                            
                        } else if(o.Stage__c.equals('DFSA License Issued')){ 
                            
                            mappedOppStageHistoryPairs.put(key,idSet);   
                        }
                    }
                    
                }
            
            }
            
            for(String i2 : mappedOppStageHistoryPairs.keySet()){
                    
                for(String i : mappedOppStageHistoryPairs.get(i2)){
                    
                    Opportunity_Stage_History__c o2 = mappedOppStageHistoryRecords.get(i2);
                    
                    if(mappedOppStageHistoryRecords.containsKey(i)){
                        
                        Opportunity_Stage_History__c o3 = mappedOppStageHistoryRecords.get(i); // earlier date
                        
                        o3.stage_duration__c = o3.Stage_History_Last_Modified_Date__c.date().daysBetween(o2.Stage_History_Last_Modified_Date__c.date()) < 0 ||
                                                o3.Stage_History_Last_Modified_Date__c.date().daysBetween(o2.Stage_History_Last_Modified_Date__c.date()) < o3.stage_duration__c ? 
                                                    o3.Stage_Duration__c : o3.Stage_History_Last_Modified_Date__c.date().daysBetween(o2.Stage_History_Last_Modified_Date__c.date());    
                        
                        
                    }   
                    
                }
            
            }   
            
            Set<String> idsToCheck;
            
            List<Opportunity_Stage_History__c> stagesToUpdate = new List<Opportunity_Stage_History__c>();
            
            for(Opportunity_Stage_History__c o : mappedOppStageHistoryRecords.values()){
                
                idsToCheck = new Set<String>();
                
                for(String i : mappedOppStageHistoryPairs.keySet()){
                
                    idsToCheck.addAll(mappedOppStageHistoryPairs.get(i));
                    
                }
                
                if(!idsToCheck.contains(o.Name)) o.Stage_Duration__c = 0;
                
                if(String.isBlank(o.Id) && existingIds.isEmpty()) mappedOppStageHistoryRecords.remove(o.Stage__c+o.Related_Opportunity__c);
                
            }
            
            stagesToUpdate.addAll(mappedOppStageHistoryRecords.values());
            
            update stagesToUpdate;
        }
        
    }
    
    /**
     * Enables the execution of the method
     */
    public static void enableExecution(){
        hasExecuted = false;
    }
    
    /**
     * Disables the execution of the method
     */
    public static void disableExecution(){
        hasExecuted = true;
    }
    
}