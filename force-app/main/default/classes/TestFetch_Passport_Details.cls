/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestFetch_Passport_Details {

    static testMethod void myUnitTest() {
        
        // Create SR Templates and Service Request
        
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Application_of_registration';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Menutext__c = 'DIFC_Sponsorship_Visa_New';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company Servfices';
        objTemplate.Active__c = true;
        insert objTemplate;
        
         Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        License_Activity_Master__c lam = new License_Activity_Master__c();
        lam.Name = 'Public Opinion Polling Services';
        lam.Activity_Code__c = '320';
        lam.Enabled__c = true;
        lam.Group__c=
        lam.Sector_Classification__c = 'Consultancy -D- management, information and marketing';
        lam.Sys_Activity_Type__c = 'Non - financial';        
        lam.Sys_Code__c = '320';
        lam.Type__c = ' License Activity';
        
        insert lam;
        
        
        License_Activity__c la = new License_Activity__c();
        la.Account__c = objAccount.id;
        la.start_date__c= system.Today();
        la.End_Date__c = system.Today().addYears(1);
        la.Sector_Classification__c = 'Consultancy -D- management, information and marketing';
        la.Activity__c = lam.id;
        insert la;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Application_of_Registration')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
         list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new Status__c(Name='Cancelled',Code__c='CANCELLED'));
        insert lstStatus;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        lstSRStatus.add(new SR_Status__c(Name='Submitted',Code__c='Submitted'));
        lstSRStatus.add(new SR_Status__c(Name='CANCELLATION PENDING',Code__c='CANCELLATION_PENDING'));
        lstSRStatus.add(new SR_Status__c(Name='Cancelled',Code__c='CANCELLED'));
        insert lstSRStatus;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;   
        
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = mapRecordTypeIds.get('Application_of_Registration');
        objSR.Send_SMS_To_Mobile__c = '+97152123456';
        objSR.Email__c = 'testclass@difc.ae.test';
        objSR.Type_of_Request__c = 'Applicant Outside UAE';
        objSR.Port_of_Entry__c = 'Dubai International Airport';
        objSR.Title__c = 'Mr.';
        objSR.First_Name__c = 'India';
        objSR.Last_Name__c = 'Hyderabad';
        objSR.Middle_Name__c = 'Andhra';
        objSR.Nationality_list__c = 'India';
        objSR.Previous_Nationality__c = 'India';
        objSR.Qualification__c = 'B.A. LAW';
        objSR.Gender__c = 'Male';
        objSR.Date_of_Birth__c = Date.newInstance(1989,1,24);
        objSR.Place_of_Birth__c = 'Hyderabad';
        objSR.Country_of_Birth__c = 'India';
        objSR.Passport_Number__c = 'ABC12345';
        objSR.Passport_Type__c = 'Normal';
        objSR.Passport_Place_of_Issue__c = 'Hyderabad';
        objSR.Passport_Country_of_Issue__c = 'India';
        objSR.Passport_Date_of_Issue__c = system.today().addYears(-1);
        objSR.Passport_Date_of_Expiry__c = system.today().addYears(2);
        objSR.Religion__c = 'Hindus';
        objSR.Marital_Status__c = 'Single';
        objSR.First_Language__c = 'English';
        objSR.Email_Address__c = 'applicant@newdifc.test';
        objSR.Mother_Full_Name__c = 'Parvathi';
        objSR.Monthly_Basic_Salary__c = 10000;
        objSR.Monthly_Accommodation__c = 4000;
        objSR.Other_Monthly_Allowances__c = 2000;
        objSR.Emirate__c = 'Dubai';
        objSR.City__c = 'Deira';
        objSR.Area__c = 'Air Port';
        objSR.Street_Address__c = 'Dubai';
        objSR.Building_Name__c = 'The Gate';
        objSR.PO_BOX__c = '123456';
        objSR.Residence_Phone_No__c = '+97152123456';
        objSR.Work_Phone__c = '+97152123456';
        objSR.Domicile_list__c = 'India';
        objSR.Phone_No_Outside_UAE__c = '+97152123456';
        objSR.City_Town__c = 'Hyderabad';
        objSR.Address_Details__c = 'Banjara Hills, Hyderabad';
        objSR.Statement_of_Undertaking__c = true;
        objSR.Internal_SR_Status__c = lstSRStatus[0].Id;
        objSR.External_SR_Status__c = lstSRStatus[0].Id;
        objSR.SAP_Unique_No__c = '123456789012345';
        objSR.Legal_Structures__c = 'LTD';
        insert objSR;
        
        Amendment__c amnd = new Amendment__c();
        amnd.ServiceRequest__c = objSR.id;
        amnd.Relationship_Type__c = 'Shareholder';
        amnd.Passport_No__c = '1234455';
        amnd.Amendment_Type__c ='Individual';
         insert amnd;
         
        Amendment__c Director = new Amendment__c();
        Director.ServiceRequest__c = objSR.id;
        Director.Relationship_Type__c = 'Director';
        Director.Passport_No__c = '1234455';
        Director.Amendment_Type__c ='Individual';
        insert Director;
        
        Amendment__c UBO = new Amendment__c();
        UBO.ServiceRequest__c = objSR.id;
        UBO.Relationship_Type__c = 'UBO';
        UBO.Passport_No__c = '1234455';
        UBO.Amendment_Type__c ='Body Corporate';
        insert UBO;
        
        
        
       
        test.startTest();
        apexpages.currentPage().getParameters().put('SRID',objSR.id);
        Fetch_Passport_Details fpd = new Fetch_Passport_Details();
        fpd.generateExcel();
        
        objSR.Legal_Structures__c = 'LLC';
        Update objSR;
        
        amnd.Relationship_Type__c = 'Member';
        update amnd;          
        fpd.fetchAmmendments(objSR);  
        
         amnd.Amendment_Type__c ='Body Corporate';
         update amnd; 
         fpd.fetchAmmendments(objSR);
        
         objSR.Legal_Structures__c = 'LP';
         Update objSR;
        
         amnd.Relationship_Type__c = 'Limited Partner';
         update amnd;       
        fpd.fetchAmmendments(objSR);  
        
        amnd.Amendment_Type__c ='Body Corporate';
        update amnd; 
        fpd.fetchAmmendments(objSR);
        
        
        
        
        objSR.Legal_Structures__c = 'GP';
        Update objSR;
        
        amnd.Relationship_Type__c = 'General Partner';
        update amnd;       
        fpd.fetchAmmendments(objSR);
        
        amnd.Amendment_Type__c ='Body Corporate';
        update amnd; 
        fpd.fetchAmmendments(objSR);    
        
        
        
        objSR.Legal_Structures__c = 'NPIO';
        Update objSR;
        
        amnd.Relationship_Type__c = 'Founding Member';
        update amnd;  
        fpd.fetchAmmendments(objSR);     
        
        
        amnd.Amendment_Type__c ='Body Corporate';
         update amnd; 
         fpd.fetchAmmendments(objSR);
         
         objSR.Legal_Structures__c = 'FRC';
         Update objSR;
         
         amnd.Amendment_Type__c ='Individual';
         amnd.Relationship_Type__c ='UBO';
         amnd.Passport_No__c ='12344';
         amnd.Nationality_list__c = 'India';
         update amnd; 
         
         apexpages.currentPage().getParameters().put('SRID',objSR.id);
         fpd.fetchAmmendments(objSR);
         
         
        test.stopTest();
        
        
        objSR.Legal_Structures__c = 'LLP';
        Update objSR;
        
        amnd.Relationship_Type__c = 'Designated Member';
        update amnd;       
        fpd.fetchAmmendments(objSR);
        
        objSR.Legal_Structures__c = 'LP';
        Update objSR;
        
        amnd.Relationship_Type__c = 'Limited Partner';
        update amnd;       
        fpd.fetchAmmendments(objSR);
           
        
        
    }
}