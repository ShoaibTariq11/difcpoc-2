/******************************************************************************************
 *  Author   : Shoaib Tariq
 *  Company  : 
 *  Date     : 15/12/2019
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    15/12/2019  shoaib    Created
**********************************************************************************************************************/

public with sharing  class InspectorMoreInformationController {
    
   public Inspection__c newInspection {get; set;}
   public string  comments     {get; set;} 
   public InspectorMoreInformationController(ApexPages.StandardController stdController) {
       this.newInspection = (Inspection__c)stdController.getRecord();
    }
    
    /**
    * Description: This method will used to save the comments to the record if the user click save.
    * Add functionality desc
    * Add error handling 
   */
    public PageReference save(){
        if(String.isNotBlank(comments)){
            Inspection__c thisInspection = [SELECT Id,NonRegulated_Q24__c, status__c FROM Inspection__c WHERE Id =:newInspection.Id  ];
            if(thisInspection.status__c ==  'Need More Information' 
               					|| thisInspection.status__c ==  'Approved' 
                                || thisInspection.status__c == 'Draft'
                                || thisInspection.NonRegulated_Q24__c == 'Yes'){
              
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Record does not match the creteria to be edited'));
              return null;  
            }
        
            Approval.UnlockResult unlockedRersult = Approval.unlock(newInspection);
            if (unlockedRersult.isSuccess()) {  newInspection.status__c = 'Need More Information';
            	newInspection.Need_More_Information_Comments__c = comments;
            	update newInspection;
            }
          
            PageReference pg = new PageReference('/'+newInspection.Id); pg.setRedirect(true);
            return pg;  
        }
        else{
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter value'));
             return null;
        }
       
    }
   /**
    * Description: This method will used to return to the record if the user click cancel.
    * same as above
   */
   public PageReference cancel(){
         PageReference pg = new PageReference('/'+newInspection.Id);
         pg.setRedirect(true);
         return pg;  
    }
    public PageReference submit(){
        
         Inspection__c thisInspection = [SELECT Id,NonRegulated_Q24__c, status__c FROM Inspection__c WHERE Id =:newInspection.Id  ];
         if(thisInspection.status__c ==  'draft' 
                                && thisInspection.NonRegulated_Q24__c != 'Yes'){
              
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please check the status of the record'));
              return null;  
            }
          if(thisInspection.status__c != 'draft' && (thisInspection.NonRegulated_Q24__c =='Yes' || thisInspection.NonRegulated_Q24__c =='No')){
              
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please check the status of the record'));
              return null;  
            }
         
         thisInspection.status__c ='Completed';
         update thisInspection;
         PageReference pg = new PageReference('/'+newInspection.Id);
         pg.setRedirect(true);
         return pg;  
    }
    
}