/*
Author		:	Ravindra Babu Nagaboina
Description :	This class will have all the Custom Code which is related to Property Listing

Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    28-07-2016	Claude		  Added User_Action__c field the list of fields
*/
public without sharing class ListingPropertyDetails {
	
	public static string CreateUpdateListing(Step__c Step){
		string res = 'Success';
		Listing__c objL;
		Service_Request__c objSR = Step.SR__r;
		Unit__c Unit = new Unit__c();
		try{
			if(objSR != null && objSR.Id != null){
				Service_Request__c objSRExt = new Service_Request__c();
				
				/* V1.0 - Claude - Added User_Action__c && User_Form_Action__c in the list of fields  */
				for(Service_Request__c obj : [select Id,User_Form_Action__c,CreatedBy.ContactId,Address_Details__c,Agent_Details__c,Amenities__c,Company_Profile__c,Listing_Description__c,Location__c from Service_Request__c where Id=:objSR.Id]){
					objSRExt = obj;
				}
				for(Unit__c objU : [select Id,Name,Building__c,Unit_Square_Feet__c from Unit__c where Id=:objSR.Listing_Unit__c]){
					Unit = objU;
				}
				if(objSR.Record_Type_Name__c == 'Update_Existing_Listing'){
					for(Listing__c obj :[select Id from Listing__c where Unit__c =: objSR.Listing_Unit__c AND Status__c = 'Active']){
						objL = new Listing__c(Id=obj.Id);
					}
					if(objL == null)
						return 'No active listing found for selected unit.';
				}else{
					objL = new Listing__c();
					objL.Unit__c = objSR.Listing_Unit__c;
					objL.Start_Date__c = system.today();
					objL.End_Date__c = system.today().addDays(90);
				}
				
				/* V1.0 - Claude - Added copy logic for user action field */
				objL.User_Action__c = objSr.User_Form_Action__c;
				
				objL.Listing_Type__c = objSR.Listing_Type__c;
				objL.Price__c = objSR.Price__c;
				objL.Sq_Feet__c = objSr.Area_sqft__c; //objSR.Sq_Feet__c; Area_sqft__c Unit.Unit_Square_Feet__c; // V1.0 - Copying Area field from SR
				objL.No_of_Bedrooms__c = ( objSR.Bedroom_Studio__c != null) ? objSR.Bedroom_Studio__c+'' : '';
				objL.No_of_Pax__c = objSR.No_of_Pax__c;
				objL.Property_Type__c = objSR.Property_Type__c;
				objL.Status__c = 'Active';
				objL.Featured__c = objSR.Feature__c == 'Yes';
				objL.Address_Details__c = objSRExt.Address_Details__c; // Long Text
				objL.Agent_Details_if_any__c = objSRExt.Agent_Details__c; // Long Text
				objL.Amenities__c = objSRExt.Amenities__c; //Long Text
				objL.Company_Name__c = objSR.Company_Name__c;
				objL.Company_Profile__c = objSRExt.Company_Profile__c; // Long Text
				objL.Food_and_Beverage__c = objSR.Food_and_Beverage__c == 'Yes';
				objL.Listing_Description__c = objSRExt.Listing_Description__c; // Long Text
				objL.Listing_Title__c = objSR.Listing_Title__c;
				objL.Location__c = objSRExt.Location__c; // Long Text
				if(objSR.No_of_Months__c != null){
					objL.No_of_Days__c = (objSR.No_of_Months__c == '1 Month') ? 30 : (objSR.No_of_Months__c == '2 Months') ? 60 : (objSR.No_of_Months__c == '3 Months') ? 90 : null ;
				}
				objL.Website__c = objSR.Website__c;
				objL.Work_Phone__c = objSR.Work_Phone__c;
				objL.Contact_Email__c = objSR.Email_Address__c;
				objL.Contact_Phone__c = objSR.Mobile_Number__c;
				objL.Contact_Time_Zone__c = objSR.Timezone__c;
				objL.Published_Date__c = system.today();
				objL.Leasing_Type__c = objSR.Leasing_Type__c;
				objL.Unit_Delivery_Condition__c = objSR.Unit_Delivery_Condition__c;
				objL.First_Name__c = objSR.First_Name__c;
				objL.Last_Name__c = objSR.Last_Name__c;
				objL.Street_Address__c = objSR.Street_Address__c;
				objL.Created_by_DIFC_Backend__c = objSRExt.CreatedBy.ContactId == null;
				upsert objL;
				
				for(Listing__c obj : [select id,Name,Building_Name__c,Unit__c,Unit__r.Building__r.Building_on_Website__c,Start_Date__c,End_Date__c,Listing_Type__c,Price__c,Sq_Feet__c,No_of_Bedrooms__c,No_of_Pax__c,Property_Type__c,
						Featured__c,Address_Details__c,Agent_Details_if_any__c,Amenities__c,Company_Name__c,Company_Profile__c,Food_and_Beverage__c,Listing_Description__c,Listing_Title__c,
						Location__c,No_of_Days__c,Website__c,Work_Phone__c,Leasing_Type__c from Listing__c where Id=:objL.Id]){
					objL = obj;
				}
				//Update the SR DOcs
				map<string,SR_Doc__c> mapDocs = new map<string,SR_Doc__c>();
				list<string> lstS = new list<string>{'Photograph 1','Photograph 2','Photograph 3','Photograph 4','Photograph 5','Photograph 6','Photograph 7','Photograph 8','Photograph 9','Photograph 10','Fit-Out Manual','Tenant Manual','Floor Plan'};
				for(SR_Doc__c obj : [select Id,Name,Listing__c,Document_Master__c from SR_Doc__c where Service_Request__c =:objSR.Id AND Document_Master__c IN (select Id from Document_Master__c where Code__c IN : lstS) AND Status__c = 'Uploaded' ]){
					mapDocs.put(obj.Document_Master__c, new SR_Doc__c(Id=obj.Id,Listing__c = objL.Id,Listing_Doc_Status__c='Active'));
				}
				if(objSR.Record_Type_Name__c == 'Update_Existing_Listing'){
					for(SR_Doc__c obj : [select Id,Name,Listing__c,Document_Master__c from SR_Doc__c where Listing__c =:objL.Id AND Document_Master__c IN : mapDocs.keySet() AND Listing_Doc_Status__c = 'Active' ]){
						mapDocs.put(obj.Document_Master__c+'_Old', new SR_Doc__c(Id=obj.Id,Listing_Doc_Status__c='Inactive'));
					}
				}
				if(!mapDocs.isEmpty()){
					update mapDocs.values();
				}
				//Need to search for Inquiries which are raised in last 90 days
				CheckForInquiries(objL);
			}
		}catch(Exception ex){
			res = ex.getMessage();
		}
		return res;
	}
	
	public static string RenewListing(Step__c Step){
		string res = 'Success';
		Listing__c objL;
		Service_Request__c objSR = Step.SR__r;
		try{
			if(objSR != null && objSR.Id != null){
				for(Listing__c obj :[select Id,Start_Date__c,End_Date__c from Listing__c where Unit__c =: objSR.Listing_Unit__c AND Status__c = 'Active']){
					objL = new Listing__c(Id=obj.Id);
				}
				if(objL == null)
					return 'No active listing found for selected unit.';
				objL.Start_Date__c = objL.Start_Date__c != null ? objL.Start_Date__c : system.today();
				objL.End_Date__c = objL.End_Date__c != null ? objL.End_Date__c.addDays(30) : system.today().addDays(30);
				objL.Status__c = 'Active';
				update objL;
			}
		}catch(Exception ex){
			res = ex.getMessage();
		}
		return res;
	}
	
	public static string DeListing(Step__c Step){
		string res = 'Success';
		Listing__c objL;
		Service_Request__c objSR = Step.SR__r;
		try{
			if(objSR != null && objSR.Id != null){
				for(Listing__c obj :[select Id,Start_Date__c,End_Date__c from Listing__c where Unit__c =: objSR.Listing_Unit__c AND Status__c = 'Active']){
					objL = new Listing__c(Id=obj.Id);
				}
				if(objL == null)
					return 'No active listing found for selected unit.';
				objL.Status__c = 'Delisted';
				update objL;
			}
		}catch(Exception ex){
			res = ex.getMessage();
		}
		return res;
	}
	
	public static void CheckForInquiries(Listing__c objL){
		map<Id,Inquiry__c> mapInquiries = new map<Id,Inquiry__c>();
		string sQry = 'select Id,Name,Building_Name__c,Email__c,Listing_Type__c,Max_Price__c,Max_Sq_Feet__c,Min_Price__c,Min_Sq_Feet__c,No_of_Bedrooms__c,No_of_Pax__c,Property_Type__c,Sys_Send_Email__c';
		sQry += ' From Inquiry__c where CreatedDate = LAST_90_DAYS AND RecordType.DeveloperName=\'Inquiry\' ';
		//sQry += objL.Listing_Type__c != null ? ' AND Listing_Type__c=:objL.Listing_Type__c ' : '';
		//sQry += objL.No_of_Bedrooms__c != null ? ' AND No_of_Bedrooms__c=:objL.No_of_Bedrooms__c ' : '';
		//sQry += objL.No_of_Pax__c != null ? ' AND No_of_Pax__c=:objL.No_of_Pax__c ' : '';
		//sQry += objL.Property_Type__c != null ? ' AND Property_Type__c=:objL.Property_Type__c ' : '';
		//sQry += ' AND Max_Price__c != null AND Max_Sq_Feet__c != null AND Min_Price__c != null AND Min_Sq_Feet__c != null';
		system.debug('sQry is : '+sQry);
		if(objL != null){
			for(Inquiry__c objI : database.query(sQry)){
				objI.Min_Price__c = objI.Min_Price__c != null ? objI.Min_Price__c : 0;
				objI.Max_Price__c = objI.Max_Price__c != null ? objI.Max_Price__c : 999999999999999.0;
				objI.Min_Sq_Feet__c = objI.Min_Sq_Feet__c != null ? objI.Min_Sq_Feet__c : 0;
				objI.Max_Sq_Feet__c = objI.Max_Sq_Feet__c != null ? objI.Max_Price__c : 9999999999.0;
				
				if(
					(objL.Price__c >= objI.Min_Price__c && objL.Price__c <= objI.Max_Price__c) &&
					(objL.Sq_Feet__c >= objI.Min_Sq_Feet__c && objL.Sq_Feet__c <= objI.Max_Sq_Feet__c) &&
					((objL.Property_Type__c == 'Residential' && objI.No_of_Bedrooms__c == objL.No_of_Bedrooms__c) || objI.No_of_Bedrooms__c == null || objI.No_of_Bedrooms__c == '' ) &&
					((objL.Leasing_Type__c == 'Business Centre' && objI.No_of_Pax__c != null && objI.No_of_Pax__c == objL.No_of_Pax__c) || objI.No_of_Pax__c == null) &&
					(objL.Unit__r.Building__r.Building_on_Website__c == objI.Building_Name__c || objI.Building_Name__c == null ||objI.Building_Name__c == '' ) &&
					(objL.Listing_Type__c == objI.Listing_Type__c || objI.Listing_Type__c == null || objI.Listing_Type__c == '' ) && 
					(objL.Property_Type__c == objI.Property_Type__c || objI.Property_Type__c == null || objI.Property_Type__c == '' )
				){
					mapInquiries.put(objI.Id,new Inquiry__c(Id=objI.Id,Sys_Send_Email__c = true));
				}
				/*if((objL.Price__c >= objI.Min_Price__c && objL.Price__c <= objI.Max_Price__c) &&
					(objL.Sq_Feet__c >= objI.Min_Sq_Feet__c && objL.Sq_Feet__c <= objI.Max_Sq_Feet__c) &&
					((objL.Property_Type__c != 'Residential' && objL.Leasing_Type__c != 'Business Centre') || (objL.Property_Type__c == 'Residential' && objI.No_of_Bedrooms__c == objL.No_of_Bedrooms__c) || (objL.Leasing_Type__c == 'Business Centre' && objI.No_of_Pax__c == objL.No_of_Pax__c) )
				){
					objI.Sys_Send_Email__c = true;
					mapInquiries.put(objI.Id,objI);
				}*/
			}
		}
		if(!mapInquiries.isEmpty())
			update mapInquiries.values();
	}
	
}