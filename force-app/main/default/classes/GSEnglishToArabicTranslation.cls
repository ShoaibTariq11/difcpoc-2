/******************************************************************************************
 *  Class Name  : GSEnglishToArabicTranslation
 *  Author      : Shoaib Tariq
 *  Company     : DIFC
 *  Date        : 28 jan 2020      
 *  Description :                
 ----------------------------------------------------------------------
   Version     Date              Author                Remarks                                                 
  =======   ==========        =============    ==================================
    v1.1    18 NOV 2019         Shoaib                Initial Version  
*******************************************************************************************/
public class GSEnglishToArabicTranslation {
    
     private  Service_Request__c thisSR;
    
     public GSEnglishToArabicTranslation(ApexPages.StandardController stdController) {
            this.thisSR = (Service_Request__c)stdController.getRecord();
         
     }
    /*
     * method to translate Name to Arabic
     */
     public PageReference redirectToRecord() {
        try{
           Service_Request__c thisServiceRequest = [SELECT Id,
                                                            First_Name__c,
                                                            Middle_Name__c,
                                                            Last_Name__c,
                                                            Place_of_Birth__c,
                                                            Mother_Full_Name__c,
                                                            First_Name_Arabic__c,
                                                            Middle_Name_Arabic__c,
                                                            Last_Name_Arabic__c,
                                                            Place_of_Birth_Arabic__c,
                                                            Mother_s_full_name_Arabic__c 
                                                    FROM Service_Request__c
                                                    WHERE Id =:thisSR.Id ];
           
           
            if(String.isBlank(thisServiceRequest.First_Name_Arabic__c)) {
                thisServiceRequest.First_Name_Arabic__c  = TranslatiorClass.translatorMethod(thisServiceRequest.First_Name__c);
            }  
            if(String.isBlank(thisServiceRequest.Middle_Name_Arabic__c)){
                thisServiceRequest.Middle_Name_Arabic__c      = TranslatiorClass.translatorMethod(thisServiceRequest.Middle_Name__c);
            }
            if(String.isBlank(thisServiceRequest.Last_Name_Arabic__c)){
                 thisServiceRequest.Last_Name_Arabic__c      = TranslatiorClass.translatorMethod(thisServiceRequest.Last_Name__c);
            }
           if(String.isBlank(thisServiceRequest.Place_of_Birth_Arabic__c)){
               thisServiceRequest.Place_of_Birth_Arabic__c   = TranslatiorClass.translatorMethod(thisServiceRequest.Place_of_Birth__c);
           }
           if(String.isBlank(thisServiceRequest.Mother_s_full_name_Arabic__c)){
            thisServiceRequest.Mother_s_full_name_Arabic__c   = TranslatiorClass.translatorMethod(thisServiceRequest.Mother_Full_Name__c);
           }
           update thisServiceRequest; 
            
           PageReference pageRef = new PageReference('/'+thisServiceRequest.Id);
           pageRef.setRedirect(false);
           return pageRef;
        }
         catch(Exception ex){
              Log__c objLog = new Log__c();
              objLog.Type__c = 'Translation error';
              objLog.Description__c = ex.getMessage() + ' at line ' + ex.getLineNumber() + ' of class PassportTrackingController';
              insert objLog; 
         }
        return null;
    }
}