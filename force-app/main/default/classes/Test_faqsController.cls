@isTest
private class Test_faqsController {
    static testMethod void Test_Faqs(){
        
        FAQ__c testFaq = new FAQ__c(Question__c = 'Test Question', Answer__c = 'Test Answer', Name='faq-1',Type__c = 'Client',Heading__c = 'FAQS');
        
        insert testFaq;
        
        Faq_controller faq = new Faq_controller();
        faq.load_faqs();
    }
}