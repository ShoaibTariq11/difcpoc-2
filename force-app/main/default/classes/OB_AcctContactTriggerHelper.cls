/******************************************************************************************
 *  Author      : Durga Prasad
 *  Company     : PwC
 *  Date        : 26-Mar-2020     
 *  Description : helper class for OB_AcctContactTriggerHandler
 *  Modification History :
 ----------------------------------------------------------------------------------------
    V.No    Date          Updated By    Description
    =====   ===========   ==========    ============
    
*******************************************************************************************/
public without sharing class OB_AcctContactTriggerHelper {
     /*
        Method Name :   CreateHistoryRecords
        Description :   Method to create the Field History Tracking object
    */
    public static void CreateHistoryRecords(list<AccountContactRelation> TriggerNew,map<Id,AccountContactRelation> TriggerOldMap,boolean IsInsert){
        map<string,string> mapHistoryFields = new map<string,string>();
        map<string,History_Tracking__c> mapHistoryCS = new map<string,History_Tracking__c>();
        if(History_Tracking__c.getAll()!=null){
            mapHistoryCS = History_Tracking__c.getAll();//Getting the Objects from Custom Setting which has to be displayed in the screen for the users
            for(History_Tracking__c objHisCS:mapHistoryCS.values()){
                if(objHisCS.Object_Name__c!=null && objHisCS.Object_Name__c.tolowercase()=='accountcontactrelation' && objHisCS.Field_Name__c!=null){
                    mapHistoryFields.put(objHisCS.Field_Name__c,objHisCS.Field_Label__c);
                }
            }
        }
        list<sobject> lstApplicationHistory = new list<sobject>();
        if(IsInsert){
            for(AccountContactRelation app:TriggerNew){
                if(mapHistoryFields!=null && mapHistoryFields.size()>0){
                    for(string objFld:mapHistoryFields.keyset()){
                        string NewValue = '';
                        if(app.get(objFld)!=null)
                            NewValue = app.get(objFld)+'';
                    	if(NewValue!=null && NewValue!=''){
	                        sobject objHistory = OB_ObjectHistoryTrackingUtil.CreateObjectHistory('Account_Contact_Relation_History__c',NewValue,'',app.Id,mapHistoryFields.get(objFld),objFld);
	                        objHistory.put('Account__c',app.AccountId);
	                        objHistory.put('Contact__c',app.ContactId);
	                        lstApplicationHistory.add(objHistory);
                    	}
                    }
                }
            }
        }else{
            for(AccountContactRelation app:TriggerNew){
                if(mapHistoryFields!=null && mapHistoryFields.size()>0 && TriggerOldMap.get(app.Id)!=app){
                    system.debug('@@@@@@@@@ mapHistoryFields '+mapHistoryFields);
                    system.debug('@@@@@@@@@ app '+app);
                    system.debug('@@@@@@@@@ trigger.oldMap '+trigger.oldMap);
                    
                    for(string objFld:mapHistoryFields.keyset())
                    {
                        if(app.get(objFld) != trigger.oldMap.get(app.Id).get(objFld)){
                        //if(app.get(objFld) != TriggerOldMap.get(app.Id).get(objFld)){
                            string OldValue = '';
                            string NewValue = '';
                            if(app.get(objFld)!=null)
                                NewValue = app.get(objFld)+'';
                            if(trigger.oldMap.get(app.Id).get(objFld)!=null)
                                OldValue = trigger.oldMap.get(app.Id).get(objFld)+'';
                            sobject objHistory = OB_ObjectHistoryTrackingUtil.CreateObjectHistory('Account_Contact_Relation_History__c',NewValue,OldValue,app.Id,mapHistoryFields.get(objFld),objFld);
                            objHistory.put('Account__c',app.AccountId);
                        	objHistory.put('Contact__c',app.ContactId);
                            lstApplicationHistory.add(objHistory);
                        }
                    }
                }
            }
        }
        if(lstApplicationHistory.size()>0)
            insert lstApplicationHistory;
    }
}