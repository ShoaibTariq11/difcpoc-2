/*
    Author      : Leeba
    Date        : 30-March-2020
    Description : Test class for OB_manageUsersController
    --------------------------------------------------------------------------------------
*/
@isTest
public with sharing class OB_manageUsersControllerTest {

    
    
    public static testmethod void testMethod1(){
            
            /* create test data here */ 
    
            // create account
            List<Account> insertNewAccounts = new List<Account>();
            insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
            insert insertNewAccounts;
    
            //create contact
            Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
            insert con; 
    
            test.startTest();
            
            
            Id profileToTestWith = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;      
            
            
            User user = new User(alias = 'test123', email='test123@noemail.com',
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = profileToTestWith, country='United States',IsActive =true,
                                 ContactId = con.Id,
                                 timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
            
            insert user;
            Account acc = new Account();
            acc.Name = 'test';
            insert acc;
            
            
            HexaBPM__SR_Template__c objSrTemp = new HexaBPM__SR_Template__c();
            objSrTemp.Name = 'Super User Authorization';
            objSrTemp.HexaBPM__Active__c = true;
            objSrTemp.HexaBPM__SR_RecordType_API_Name__c = 'Superuser_Authorization';
            insert objSrTemp;
            
            
           
            
            system.runAs(user){
                
                OB_manageUsersController.RequestWrap reqWrapper = new OB_manageUsersController.RequestWrap();
                OB_manageUsersController.RespondWrap repWrapper = new OB_manageUsersController.RespondWrap();
                
           
                String requestString = JSON.serialize(reqWrapper);
                
                system.debug('accid==>' +acc.id);               
                repWrapper = OB_manageUsersController.fetchUserData(requestString);
                repWrapper.accId = acc.id;
                reqWrapper.accId = repWrapper.accId;
                reqWrapper.contactID = con.Id;              
                repWrapper = OB_manageUsersController.createPromoteUserSr(JSON.serialize(reqWrapper));
                
                
            }
            
            
            
        }
}