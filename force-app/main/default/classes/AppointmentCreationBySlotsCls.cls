/**********************************************************
Class Name: AppointmentCreationBySlotsCls
Description: Appointment will be linked to availble slots instead of creation
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By       Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    22/12/2019  selva   #7365
********************************************************/
public class AppointmentCreationBySlotsCls {

    public static Appointment__c Appntment = new Appointment__c();
   
    public static string Counter = system.label.Medical_Appointment_Counter_Mins;
  
    public static Integer ExpressLunchDuration = Integer.valueof(system.label.Express_Lunch_Duration);
    public static string Lunch_Time = system.label.Lunch_Time;
    public static string Lunch_Time_Express = system.label.Lunch_Time_Express;
    public static string LunchEnd_Time = system.label.LunchEnd_Time;
    public static string LunchEnd_Time_Express = system.label.LunchEnd_Time_Express;
    public static integer Mins = Integer.valueof(Counter);
   
    
    public static List<service_request__c> SR;
    public static BusinessHours BH ;

    public static Boolean isInserted;
    public static Boolean isRescheduled = false;
    public static Boolean checkTriggerUpdate = false;
    public AppointmentCreationBySlotsCls (){
        isRescheduled = false;
        checkTriggerUpdate = false;
    }
    
    /**
    public static List<Appointment_Schedule_Counter__c> oldappointmentscheulerLst;
    public static Map<Datetime,string> checkRescheduleOldList;
    public static List<Appointment_Schedule_Counter__c> lastAppBySystem;
    public static Map<string,Datetime> existingAppCalendarLst;
    ***/
    public static Datetime expressTestTime;
    public static Datetime normalTestTime;

    public static string MedicalAppointmentCreate(Step__c step) {
        SR = new List<service_request__c>();
        String rtnValue = null;
        SR = [select id, Express_Service__c, name, first_Name__c, Last_Name__c from service_request__c where id =: Step.sr__c limit 1];
        DateTime dtNormal;
        if(SR.size()>0){
            if (SR.size() > 0 && SR[0].Express_Service__c == false){
                dtNormal = step.createdDate.addHours(8);
                rtnValue = linkAppointment(dtNormal,'Normal',step,SR[0].Id);
                //dtNormal = Datetime.newInstance(2019,12,19,01,05,00);
            }else{
                //dtNormal = Datetime.newInstance(2019,12,19,01,05,00);
                dtNormal = step.createdDate.addHours(2);
                rtnValue = linkAppointment(dtNormal,'Express',step,SR[0].Id);
            }
        }else{
            rtnValue='failed';
        }
        return rtnValue;
    }
    
    public static string linkAppointment(DateTime dtNormal,string appType,Step__c step,string srID){
        
        string appStatus = 'Available';
        String rtnValue = null;
        List<Appointment__c> appList= [select id,Slot_Status__c,Appointment_StartDate_Time__c,Appointment_EndDate_Time__c,Service_Type__c,Applicantion_Type__c,Step__c,Service_Request__c from Appointment__c where Appointment_StartDate_Time__c >=: dtNormal and Appointment_Status__c!='Re-Scheduled' and Slot_Status__c=:appStatus and Applicantion_Type__c=:appType order by Appointment_StartDate_Time__c ASC limit 1];
        try{
            if(appList.size()>0){
                Appntment = new Appointment__c();
                for(Appointment__c itr:appList){
                    Appntment.Step__c = step.id;
                    Appntment.Service_Request__c = srID;
                    Appntment.Applicantion_Type__c = appType;
                    Appntment.Slot_Status__c = 'Booked';
                    Appntment.id = itr.id;
                    Appntment.Appointment_StartDate_Time__c = itr.Appointment_StartDate_Time__c;
                    Appntment.Appointment_EndDate_Time__c = itr.Appointment_EndDate_Time__c;
                }
                Database.SaveResult saveResultList;
                if(Appntment.Id !=null && Appntment.Step__c!=null){
                    saveResultList = Database.update(Appntment, true);
                    rtnValue = 'Processed';
                }
                if (saveResultList.isSuccess()) {
                    insertScheduleCounter(Appntment,appType);
                }
                if (saveResultList.isSuccess()) {
                    Step__c stp = new Step__c(id=step.id,AppointmentCreation__c='Completed');
                    update stp;
                }
            }
            else{
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'portal@difc.ae'];
                if (owea != null && owea.size() > 0) {
                    mail.setOrgWideEmailAddressId(owea.get(0).Id);
                }  
                String[] toAddresses = new String[] {'c-selva.rathinam@difc.ae'};
                mail.setToAddresses(toAddresses);
                mail.setSubject('GS Appointment Scheduler');
                mail.setPlainTextBody('GS Appointment Scheduler- Slots not available, Please execute the batch class  - AppointmentSlotCreationBatch');
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
            }
        }
        catch(Exception ex){
            system.debug('--ex--'+ex);
            
            //MedicalAppointmentCreate(step);
            Log__c objLog = new Log__c();
            objLog.Type__c = 'Appointment Creation';
            objLog.Description__c = 'Exceptio is : ' + ex.getMessage()+'--Step Id--'+step.id+ '-nLine # --' + ex.getLineNumber();
            insert objLog;
        }
        return rtnValue;
    }
     public static void insertScheduleCounter(Appointment__c Appntment,string appType){
    
        //update the scheduler counter when appointment created
        if(Appntment!=null){
            Appointment_Schedule_Counter__c appSchedule = new Appointment_Schedule_Counter__c();
            appSchedule.Appointment_Time__c = Appntment.Appointment_EndDate_Time__c;
            appSchedule.Application_Type__c = appType;
            appSchedule.Lunch_Duration__c = ExpressLunchDuration;
            appSchedule.Appointment__c = Appntment.id;
            Date LunchDate = date.newInstance(Appntment.Appointment_StartDate_Time__c.year(),Appntment.Appointment_StartDate_Time__c.month(),Appntment.Appointment_StartDate_Time__c.Day());
            DateTime LunchTime = dateTime.newInstance(LunchDate, Time.newInstance(Integer.valueOf(string.valueof(Lunch_Time).split(':')[0]),Integer.valueof(string.valueof(Lunch_Time_Express).split(':')[1]), 0, 0));
            appSchedule.Lunch_Time__c = LunchTime;
            if(appSchedule!=null){
                upsert appSchedule;
            }
        }
    }
    
    //Methos use to Reschedule the appointment
    public static void  updateRescheduleRecord(List<Appointment__c> AppnewList,map<Id, Appointment__c> AppOldList){
        String rtnValue = null;
        set<Datetime> startTime = new set<Datetime>();
        Map<DateTime,Id> appoldMapID = new Map<Datetime,Id>();
        Map<Id,Appointment__c> appoldMapList = new Map<Id,Appointment__c>();
        Map<DateTime,string> appoldMapSlotStatus = new Map<Datetime,string>();
        Map<DateTime,string> appoldMapAppType = new Map<Datetime,string>();
        List<Appointment__c> updateAppList  = new List<Appointment__c>();
        set<Id> appId = new set<Id>();
        List<Appointment__c> insertUnSlotAppointment = new List<Appointment__c>();
        
        for(Appointment__c itr:AppnewList){
            startTime.add(itr.Appointment_StartDate_Time__c);
            system.debug('---itr.Appointment_StartDate_Time__c----'+itr.Appointment_StartDate_Time__c);
        }
        List<Appointment__c> appREList= [select id,Slot_Status__c,Appointment_StartDate_Time__c,Appointment_EndDate_Time__c,Service_Type__c,Applicantion_Type__c,Step__c,Service_Request__c from Appointment__c where Appointment_StartDate_Time__c IN:startTime];
        system.debug('--appREList---'+appREList.size());
        checkTriggerUpdate = true;
        for(Appointment__c oldList:appREList){
            appoldMapID.put(oldList.Appointment_StartDate_Time__c,oldList.Id);
            appoldMapList.put(oldList.Id,oldList);
            appoldMapSlotStatus.put(oldList.Appointment_StartDate_Time__c,oldList.Slot_Status__c);
            appoldMapAppType.put(oldList.Appointment_StartDate_Time__c,oldList.Applicantion_Type__c);
        }
        if(appREList.size()>0){
            for(Appointment__c itr:AppnewList){//trigger new
                if(appoldMapSlotStatus.get(itr.Appointment_StartDate_Time__c)=='Available' && itr.Applicantion_Type__c == appoldMapSlotStatus.get(itr.Appointment_StartDate_Time__c)){
                    if(appoldMapID.containsKey(itr.Appointment_StartDate_Time__c) && itr.Id!=appoldMapID.get(itr.Appointment_StartDate_Time__c)){
                        for(Appointment__c ap:appoldMapList.values()){
                             if(itr.Id!=ap.Id && ap.Slot_Status__c=='Available'){
                                ap.Slot_Status__c ='Re-Scheduled';
                                ap.Step__c = itr.Step__c;
                                ap.Service_Request__c = itr.Service_Request__c;
                                ap.Appointment_StartDate_Time__c = itr.Appointment_StartDate_Time__c;
                                ap.Appointment_EndDate_Time__c = itr.Appointment_EndDate_Time__c.addminutes(5);
                                updateAppList.add(ap);
                                appId.add(ap.Id);
                            }
                        }
                        if(itr.Slot_Status__c=='Booked'){
                            itr.Slot_Status__c ='Available';
                            itr.Step__c = null;
                            itr.Service_Request__c = null;
                            itr.Appointment_StartDate_Time__c = AppOldList.get(itr.id).Appointment_StartDate_Time__c;
                            itr.Appointment_EndDate_Time__c = AppOldList.get(itr.id).Appointment_EndDate_Time__c;
                            deleteScheduleCounter(itr.Id);
                        }
                    }
                }else if(appoldMapSlotStatus.get(itr.Appointment_StartDate_Time__c)=='Booked' || appoldMapSlotStatus.get(itr.Appointment_StartDate_Time__c)=='Re-Scheduled'){
                    itr.addError('This slot is already booked-123'); 
                }else{
                    insertUnSlotAppointment.add(itr);
                    
                }
            }
            if(insertUnSlotAppointment.size()>0){
                insertManualAppointment(insertUnSlotAppointment,AppOldList);
            }
            List<Database.SaveResult> saveResultList;
            
            if(updateAppList.size()>0){
                isRescheduled = true;
                saveResultList = Database.update(updateAppList, true);
                List<Appointment__c> insertReAppList = new List<Appointment__c>();
                for(Database.SaveResult sr : saveResultList){
                    if (sr.isSuccess()){
                        for(Appointment__c itr: updateAppList){
                            if(itr.Appointment_StartDate_Time__c!=null){
                                system.debug('---------'+itr.Appointment_StartDate_Time__c+'---'+itr.Id);
                                insertReAppList.add(itr);
                            }
                        }
                        if(insertReAppList.size()>0){
                            insertReScheduleCounter(insertReAppList,'Re-Scheduled');
                        }
                    }
                }
            }
        }
    }
     //update the scheduler counter when appointment Rescheduled
    public static void insertReScheduleCounter(List<Appointment__c> Appntment,string slotType){
        if(Appntment.size()>0){
            List<Appointment_Schedule_Counter__c> insertAppSch = new List<Appointment_Schedule_Counter__c>();
            for(Appointment__c itr:Appntment){
                Appointment_Schedule_Counter__c appSchedule = new Appointment_Schedule_Counter__c();
                appSchedule.Appointment_Time__c = itr.Appointment_StartDate_Time__c.addminutes(5);
                appSchedule.Application_Type__c = itr.Applicantion_Type__c;
                appSchedule.Lunch_Duration__c = ExpressLunchDuration;
                appSchedule.Appointment__c = itr.id;
                appSchedule.Type__c = slotType;
                //Date LunchDate = date.newInstance(Appntment.Appointment_StartDate_Time__c.year(),Appntment.Appointment_StartDate_Time__c.month(),Appntment.Appointment_StartDate_Time__c.Day());
                //DateTime LunchTime = dateTime.newInstance(LunchDate, Time.newInstance(Integer.valueOf(string.valueof(Lunch_Time).split(':')[0]),Integer.valueof(string.valueof(Lunch_Time_Express).split(':')[1]), 0, 0));
                //appSchedule.Lunch_Time__c = 40;
                insertAppSch.add(appSchedule);
            }
            if(insertAppSch.size()>0){
                upsert insertAppSch;
            }
        }
    }
    //Remove appointment from scheduler counter when appointment Re booked and marked as available
    public static void deleteScheduleCounter(string appId){
        List<Appointment_Schedule_Counter__c> appSchContrl = [select id,Appointment__c,Appointment_Time__c from Appointment_Schedule_Counter__c where Appointment__c =:appId];
        Delete appSchContrl;
    }
    public static void insertManualAppointment(List<Appointment__c> manualAppList,map<Id, Appointment__c> AppOldList){
        List<Appointment__c> insertappList = new List<Appointment__c>();
        for(Appointment__c itr:manualAppList){
            Appointment__c app = new Appointment__c();
            app.Appointment_StartDate_Time__c = itr.Appointment_StartDate_Time__c;
            app.Appointment_EndDate_Time__c = itr.Appointment_StartDate_Time__c.addminutes(5);
            app.Applicantion_Type__c=itr.Applicantion_Type__c;
            app.Slot_Status__c='Booked';
            app.Step__c = itr.Step__c;
            app.Service_Request__c = itr.Service_Request__c;
            insertappList.add(app);
            
            itr.Slot_Status__c ='Available';
            itr.Step__c = null;
            itr.Service_Request__c = null;
            system.debug('--old trigger map---'+AppOldList.get(itr.id).Appointment_StartDate_Time__c);
            itr.Appointment_StartDate_Time__c = AppOldList.get(itr.id).Appointment_StartDate_Time__c;
            itr.Appointment_EndDate_Time__c = AppOldList.get(itr.id).Appointment_EndDate_Time__c;
            
        }
        if(insertappList.size()>0){
            insert insertappList;
            insertReScheduleCounter(insertappList,'MANUAL');
        }
    }
}