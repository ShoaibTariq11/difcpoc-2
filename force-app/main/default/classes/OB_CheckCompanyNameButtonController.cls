/**-------------------------------------------------------------------
 * Description : Controller for OB_CheckCompanyNameButton component
 * -------------------------------------------------------------------
 * History :
 * [15.03.2020] Prateek Kadkol - Code Creation
 * -------------------------------------------------------------------
*/
public with sharing class OB_CheckCompanyNameButtonController {

    @AuraEnabled
    public static RespondWrap checkName(String requestWrapParam)  {
    
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
        
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);

        string ApplicationID = '';

        for(Company_Name__c compObj : [SELECT id,Application__c FROM Company_Name__c where id=:reqWrap.companyNameID]){
            ApplicationID = compObj.Application__c;
        }
        
        if(ApplicationID != ''){
            try{
        
                OB_DEDCallout.isTradeNameAllowed(ApplicationID,reqWrap.companyNameID);
             
            }catch(DMLException e) {
            
               respWrap.errorMessage = e.getMessage();
            }
        }else{
            respWrap.errorMessage = 'No Application Id found';
        }
        
        return respWrap;
    }
        
        
    // ------------ Wrapper List ----------- //
        
    public class RequestWrap{
    
       
        @AuraEnabled public String companyNameID;                                                                                                                                                                                                                                                 }
        
    public class RespondWrap{
    
        @AuraEnabled public string userId;
        @AuraEnabled public string errorMessage;                                                                                                                                                                                                      
    }
}