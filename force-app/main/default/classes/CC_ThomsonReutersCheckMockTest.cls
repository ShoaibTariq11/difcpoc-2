@isTest
global class CC_ThomsonReutersCheckMockTest implements HttpCalloutMock {
	
	public static string clientValID ='';
	public CC_ThomsonReutersCheckMockTest(String clientID){
		clientValID = clientID;
	}
	global HTTPResponse respond(HTTPRequest req) {
		
		HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('<sdResultSet><indexDate>2019-12-24T17:08:33.970+04:00</indexDate><sdQuery><clientID>"'+clientValID+'"</clientID><clientType>CUSTOMER</clientType><createCaseOnMatches>CREATECASE</createCaseOnMatches><types><type>INDIVIDUAL</type></types><caseNum>200</caseNum><sdResult>200</sdResult><entityId>200</entityId><category>200</category><originalScript>200</originalScript><score>200</score><secondaryGenderInfo>Male</secondaryGenderInfo><secondaryCountryInfo>200</secondaryCountryInfo><secondaryCityInfo>200</secondaryCityInfo><secondaryAddrInfo>200</secondaryAddrInfo><name>fsdf fdsf</name><nationalityName></nationalityName><ruleId>RULE151</ruleId></sdQuery><sdResults/></sdResultSet>');
        res.setStatusCode(200);
        //{"DIFC":"<?xml version="1.0" encoding="UTF-8" standalone="yes"?> 
        return res;
	}
	
	
	
}