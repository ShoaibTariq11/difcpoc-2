/******************************************************************************************
----------------------------------------------------------------------------------------
Modification History
----------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
----------------------------------------------------------------------------------------
V1.0   02-11-2016   Arun         7860 Change Step status to on hold when one linked SR status put on hold.And Change Main SR status to Under Verification when team accpt the step .
*******************************************************************************************/

public with sharing class cls_Submitted_Multiple_Visa_step {

 public Step__c StepData{get;set;}
  map<string,id> mapOfStatus = new map<string,id>();

    public cls_Submitted_Multiple_Visa_step(ApexPages.StandardController controller) 
    {
    
        List<String> fields = new List<String> {'SR__c','Step_Name__c','Put_on_Hold__c','Department__c','SR_Record_Type__c','SR_Status__c','On_Hold_reason__c','OwnerId','SR__r.Linked_SR__c','Closed_Date_Time__c'};
        if (!Test.isRunningTest()) controller.addFields(fields); // still covered
         StepData=(Step__c)controller.getRecord();
         
          /* Get all available SR Statuses */
        for(SR_Status__c obj : [select id,name from SR_Status__c where code__c!=null and name='Under Verification']) mapOfStatus.put(obj.name, obj.id);
        

    }
     public void LinkedDraftSrs()
     {
     System.debug('==============LinkedDraftSrs======================');
      System.debug('StepData==>'+StepData);
           string OwnerId =StepData.OwnerId;
           
         if(StepData.SR__r.Linked_SR__c!=null && StepData.Step_Name__c=='Front Desk Review' && OwnerId.startsWith('005') && StepData.Closed_Date_Time__c==null && (StepData.SR_Record_Type__c=='Non_DIFC_Sponsorship_Visa_New' || StepData.SR_Record_Type__c=='Non_DIFC_Sponsorship_Visa_Renewal' || StepData.SR_Record_Type__c=='Non_DIFC_Sponsorship_Visa_Cancellation'))
         {
             Service_Request__c ObSr=[select id,Name,First_Name__c,Last_Name__c,Relation__c,External_Status_Name__c,External_SR_Status__c,Internal_SR_Status__c from Service_Request__c where id=:StepData.SR__r.Linked_SR__c];
             
                 if(ObSr.External_Status_Name__c=='Submitted')
                 {
                       ObSr.Internal_SR_Status__c = mapOfStatus.get('Under Verification');
                        ObSr.External_SR_Status__c = mapOfStatus.get('Under Verification');
                        update ObSr;
                 }
             
             
             
               System.debug('StepData.SR__r.Linked_SR__c==>'+StepData.SR__r.Linked_SR__c);
          
             List<Step__c> ListSteps=new List<Step__c>();
             
             for(Step__c ObjStep:[select Id,OwnerId,Put_on_Hold__c,Department__c,On_Hold_reason__c from Step__c where SR__r.Linked_SR__c=:StepData.SR__r.Linked_SR__c and Step_Name__c='Front Desk Review' and Closed_Date_Time__c=null and id!=:StepData.id])
             {
                 System.debug('ObjStep==>'+ObjStep);
                 string ROwnerId =ObjStep.OwnerId;
                if(ObjStep.OwnerId!=StepData.OwnerId)//if Steps owners are diffrents 
                 {
                     ObjStep.OwnerId=StepData.OwnerId;ListSteps.add(ObjStep);
                 }
                 else if(StepData.Put_on_Hold__c!=ObjStep.Put_on_Hold__c)
                 {
                     ObjStep.Put_on_Hold__c=StepData.Put_on_Hold__c; ObjStep.Department__c=StepData.Department__c; ObjStep.On_Hold_reason__c=StepData.On_Hold_reason__c;
                     ListSteps.add(ObjStep);
                 }
                 
                 
                 
             }
             
             if(!ListSteps.isEmpty()) { update ListSteps; }
              
         }
     }

}