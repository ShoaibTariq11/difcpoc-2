@isTest
public class cls_PSA_Termination_ProcessTest {
 static testmethod void newDependentMultipleVisa1() {
        test.startTest();
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.PSA_Actual__c    = 1000;
        objAccount.PSA_Guarantee__c = 500;
        objAccount.ROC_Status__c  = 'Active';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        insert objContact;
          
        Relationship__c relBus = new Relationship__c();
        relBus.Object_Contact__c = objContact.id;
        relBus.Subject_Account__c =  objAccount.Id;
        //relBus.Relationship_Type__c = 'Beneficiary Owner';
        relBus.Relationship_Type__c ='Has DIFC Sponsored Employee';
        relBus.Active__c= true;
        insert relBus;
          
        Id GSContactId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        Contact objContact1 = new Contact();
        objContact1.firstname = 'Test Contact2';
        objContact1.lastname = 'Test Contact2';
        objContact1.accountId = objAccount.id;
        objContact1.recordTypeId = GSContactId;
        objContact1.Email = 'test2@difcportal.com';
        insert objContact1;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Consulate_or_Embassy_Letter';
        objTemplate.SR_RecordType_API_Name__c = 'Consulate_or_Embassy_Letter';
        objTemplate.Menutext__c = 'Consulate_or_Embassy_Letter';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Employee Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Required Docs to Upload';
        insert objDocMaster;
        
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.On_Submit__c = true;
        objTempDocs.Generate_Document__c = true;
        objTempDocs.SR_Template_Docs_Condition__c = 'Service_Request__c->Name#!=#DRIVER';
        insert objTempDocs;
        
        Lookup__c nat1 = new Lookup__c(Type__c='Nationality',Name='Afghanistan');
        insert nat1;
        Lookup__c nat2 = new Lookup__c(Type__c='Nationality',Name='India');
        insert nat2;
        Lookup__c nat3 = new Lookup__c(Type__c='Nationality',Name='Pakistan');
        insert nat3;
        
        string issueFineId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='PSA_Termination' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        
        SR_Status__c objSRStatus1 = new SR_Status__c();
        objSRStatus1.Name = 'Submitted';
        objSRStatus1.Code__c = 'Submitted';
        insert objSRStatus1;
         
        Service_Request__c objSR = new Service_Request__c();
        objSR.Express_Service__c = true;
        objSR.Customer__r = objAccount;
        objSR.Customer__c = objAccount.id;
        objSR.RecordTypeId = issueFineId;
        objSR.submitted_date__c = date.today();
        objSR.contact__c = objContact1.id;
        objSR.Sponsor__c = objContact1.Id;
        objSR.Express_Service__c = false;
        objSR.External_SR_Status__c = objSRStatus1.Id;
        objSR.Internal_SR_Status__c = objSRStatus1.Id;
        objSR.No_of_documents_lost__c = '3';
        insert objSR;
         
        system.assertEquals(objSRStatus1.Id, objSR.External_SR_Status__c);
        SR_Status__c objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Under Verification';
        objSRStatus.Code__c = 'Under Verification';
        insert objSRStatus;
        
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Express_Service__c = false;
        objSR1.Linked_SR__c = objSR.Id;
        objSR1.External_SR_Status__c = objSRStatus.Id;
        objSR1.Internal_SR_Status__c = objSRStatus.Id;
        objSR1.RecordTypeId = issueFineId;
        insert objSR1;
        
        Step_Template__c objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Front Desk Review';
        objStepType1.Code__c = 'Front Desk Review';
        objStepType1.Step_RecordType_API_Name__c = 'General';
        objStepType1.Summary__c = 'Front Desk Review';
        insert objStepType1;
        
        Status__c thisStep = new Status__c();
        thisStep.Name ='Pending';
        thisStep.Code__c = 'Pending';
        insert thisStep;
        
        step__c objStep = new Step__c();
        objStep.SR__c = objSR1.Id;
        objStep.Step_Template__c = objStepType1.id ;
        objStep.No_PR__c = false;
        objStep.Closed_Date_Time__c = null;
        objStep.status__c = thisStep.id;
        insert objStep;
        
        step__c objStep2 = new Step__c();
        objStep2.SR__c = objSR1.Id;
        objStep2.Put_on_Hold__c = true;
        objStep2.Step_Template__c = objStepType1.id ;
        objStep2.No_PR__c = false;
        objStep2.Closed_Date_Time__c = null;
        objStep2.status__c = thisStep.id;
        insert objStep2;
        
        step__c newStep = [SELECT Id,SR_Status__c,On_Hold_reason__c,Department__c,Put_on_Hold__c,SR__r.Linked_SR__c,SR_Record_Type__c,OwnerId,Closed_Date_Time__c,Step_Name__c From step__c where Id =:objStep.id  LIMIT 1];
        
        PSA_Deduction_Detail__c thisDetail = new PSA_Deduction_Detail__c();
        thisDetail.Applicant__c = relBus.Id;
        thisDetail.Service_Request__c = objSR1.Id;
        thisDetail.Service_Fee__c =100;
        thisDetail.Fines__c = 200; 
        Insert thisDetail;
        
        PageReference pageRef = Page.Submitted_Multiple_Visa_step;
        Test.setCurrentPage(pageRef);
        
         ApexPages.StandardController sc                       = new ApexPages.StandardController(objSR1);
         cls_PSA_Termination_Process thisInstance               = new cls_PSA_Termination_Process(sc);
         thisInstance.SaveRecord();
      
         test.stopTest(); 
      }
    
    static testmethod void newDependentMultipleVisa2() {
        test.startTest();
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.PSA_Actual__c    = 1000;
        objAccount.PSA_Guarantee__c = 500;
        objAccount.ROC_Status__c  = 'Active';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        insert objContact;
          
        Relationship__c relBus = new Relationship__c();
        relBus.Object_Contact__c = objContact.id;
        relBus.Subject_Account__c =  objAccount.Id;
        //relBus.Relationship_Type__c = 'Beneficiary Owner';
        relBus.Relationship_Type__c ='Has DIFC Sponsored Employee';
        relBus.Active__c= true;
        insert relBus;
          
        Id GSContactId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        Contact objContact1 = new Contact();
        objContact1.firstname = 'Test Contact2';
        objContact1.lastname = 'Test Contact2';
        objContact1.accountId = objAccount.id;
        objContact1.recordTypeId = GSContactId;
        objContact1.Email = 'test2@difcportal.com';
        insert objContact1;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Consulate_or_Embassy_Letter';
        objTemplate.SR_RecordType_API_Name__c = 'Consulate_or_Embassy_Letter';
        objTemplate.Menutext__c = 'Consulate_or_Embassy_Letter';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Employee Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Required Docs to Upload';
        insert objDocMaster;
        
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.On_Submit__c = true;
        objTempDocs.Generate_Document__c = true;
        objTempDocs.SR_Template_Docs_Condition__c = 'Service_Request__c->Name#!=#DRIVER';
        insert objTempDocs;
        
        Lookup__c nat1 = new Lookup__c(Type__c='Nationality',Name='Afghanistan');
        insert nat1;
        Lookup__c nat2 = new Lookup__c(Type__c='Nationality',Name='India');
        insert nat2;
        Lookup__c nat3 = new Lookup__c(Type__c='Nationality',Name='Pakistan');
        insert nat3;
        
        string issueFineId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='PSA_Termination' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        
        SR_Status__c objSRStatus1 = new SR_Status__c();
        objSRStatus1.Name = 'Submitted';
        objSRStatus1.Code__c = 'Submitted';
        insert objSRStatus1;
         
        Service_Request__c objSR = new Service_Request__c();
        objSR.Express_Service__c = true;
        objSR.Customer__r = objAccount;
        objSR.Customer__c = objAccount.id;
        objSR.RecordTypeId = issueFineId;
        objSR.submitted_date__c = date.today();
        objSR.contact__c = objContact1.id;
        objSR.Sponsor__c = objContact1.Id;
        objSR.Express_Service__c = false;
        objSR.External_SR_Status__c = objSRStatus1.Id;
        objSR.Internal_SR_Status__c = objSRStatus1.Id;
        objSR.No_of_documents_lost__c = '3';
        insert objSR;
         
        system.assertEquals(objSRStatus1.Id, objSR.External_SR_Status__c);
        SR_Status__c objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Under Verification';
        objSRStatus.Code__c = 'Under Verification';
        insert objSRStatus;
        
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Express_Service__c = false;
        objSR1.Linked_SR__c = objSR.Id;
        objSR1.External_SR_Status__c = objSRStatus.Id;
        objSR1.Internal_SR_Status__c = objSRStatus.Id;
        objSR1.RecordTypeId = issueFineId;
        insert objSR1;
        
        Step_Template__c objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Front Desk Review';
        objStepType1.Code__c = 'Front Desk Review';
        objStepType1.Step_RecordType_API_Name__c = 'General';
        objStepType1.Summary__c = 'Front Desk Review';
        insert objStepType1;
        
        Status__c thisStep = new Status__c();
        thisStep.Name ='Pending';
        thisStep.Code__c = 'Pending';
        insert thisStep;
        
        step__c objStep = new Step__c();
        objStep.SR__c = objSR1.Id;
        objStep.Step_Template__c = objStepType1.id ;
        objStep.No_PR__c = false;
        objStep.Closed_Date_Time__c = null;
        objStep.status__c = thisStep.id;
        insert objStep;
        
        step__c objStep2 = new Step__c();
        objStep2.SR__c = objSR1.Id;
        objStep2.Put_on_Hold__c = true;
        objStep2.Step_Template__c = objStepType1.id ;
        objStep2.No_PR__c = false;
        objStep2.Closed_Date_Time__c = null;
        objStep2.status__c = thisStep.id;
        insert objStep2;
        
        step__c newStep = [SELECT Id,SR_Status__c,On_Hold_reason__c,Department__c,Put_on_Hold__c,SR__r.Linked_SR__c,SR_Record_Type__c,OwnerId,Closed_Date_Time__c,Step_Name__c From step__c where Id =:objStep.id  LIMIT 1];
        
        PSA_Deduction_Detail__c thisDetail = new PSA_Deduction_Detail__c();
        thisDetail.Applicant__c = relBus.Id;
        thisDetail.Service_Request__c = objSR1.Id;
        thisDetail.Service_Fee__c =100;
        thisDetail.Fines__c = 200; 
        Insert thisDetail;
        
        PageReference pageRef = Page.Submitted_Multiple_Visa_step;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('field_name','Customer__c');
          pageRef.getParameters().put('field_value',objAccount.id);
        
         ApexPages.StandardController sc                       = new ApexPages.StandardController(objSR1);
         cls_PSA_Termination_Process thisInstance               = new cls_PSA_Termination_Process(sc);
         thisInstance.RecordvalueChange();
      
         test.stopTest(); 
      }
}