/*********************************************************************************************************************
*  Name     : OB_SendPriceItemsToSAPScheduler
*  Author   : Durga Kandula
*  Purpose  : Push price item to SAP system 
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
 V1.0         Durga
**/
global class OB_SendPriceItemsToSAPScheduler implements database.batchable<sobject>,schedulable,Database.AllowsCallouts{
    
    global Iterable<sObject> start(database.batchablecontext bc){
        List<HexaBPM__Service_Request__c> serviceRequestList = new List<HexaBPM__Service_Request__c>();//SAP integration
        set<string> setRecordTypeNames = new set<string>{'Name_Check','In_Principle','AOR_NF_R','AOR_F'};
        List<String> productCodeList = Label.OB_ProductCODE.split(',');
        for(HexaBPM__Service_Request__c srObj: [Select Id,(Select Id From HexaBPM__SR_Price_Items1__r 
        											where HexaBPM__Status__c != 'Invoiced' 
                                                    AND HexaBPM__Status__c != 'Added' 
                                                    AND HexaBPM__ServiceRequest__c != null 
                                                    AND HexaBPM__Price__c> 0 
                                                    AND Transaction_Id__c = null 
                                                    AND Company_Code__c!='1100' 
                                                    AND HexaBPM__ServiceRequest__r.RecordType.DeveloperName IN:setRecordTypeNames
                                                    AND HexaBPM__Product__r.ProductCode Not IN:  productCodeList
                                                    Limit 1) 
        										From HexaBPM__Service_Request__c
        										Where RecordType.DeveloperName IN:setRecordTypeNames
        										AND  HexaBPM__External_Status_Name__c  != 'Draft'
        										Limit : (Limits.getLimitQueryRows()-Limits.getQueryRows())]){
        											
            if(srObj.HexaBPM__SR_Price_Items1__r != null && srObj.HexaBPM__SR_Price_Items1__r.size() != 0)
            {
        		serviceRequestList.add(srObj);
        	}
        }
        /*for(HexaBPM__SR_Price_Item__c objSRItem : [select HexaBPM__ServiceRequest__c 
                                                    From HexaBPM__SR_Price_Item__c 
                                                    Where HexaBPM__Status__c != 'Invoiced' 
                                                    AND HexaBPM__Status__c != 'Added' 
                                                    AND HexaBPM__ServiceRequest__c != null 
                                                    AND HexaBPM__Price__c> 0 
                                                    AND Transaction_Id__c = null 
                                                    AND Company_Code__c!='1100' 
                                                    AND HexaBPM__ServiceRequest__r.RecordType.DeveloperName IN:setRecordTypeNames
                                                    AND HexaBPM__Product__r.ProductCode Not IN:  productCodeList
                                                    Limit : (Limits.getLimitQueryRows()-Limits.getQueryRows()) ]){
            serviceRequestSet.add(new HexaBPM__Service_Request__c (id = objSRItem.HexaBPM__ServiceRequest__c));
        }*/
        system.debug('==init==serviceRequestList===='+serviceRequestList);
        system.debug('==init==serviceRequestList===='+serviceRequestList.size());
        return serviceRequestList;
        
    }
    global void execute(database.batchablecontext bc, List<HexaBPM__Service_Request__c> serviceRequestList){
        set<string> serviceRequestIdSet = new set<string>();//SAP integration
        system.debug('=execute===serviceRequestList====='+serviceRequestList);
        for(HexaBPM__Service_Request__c sr :serviceRequestList){
         serviceRequestIdSet.add(sr.Id);   
        }
        
        system.debug('====serviceRequestIdSet====='+serviceRequestIdSet);
        if(serviceRequestIdSet != null && serviceRequestIdSet.size() != 0){
        	system.debug('==inside==serviceRequestIdSet====='+serviceRequestIdSet);
            OB_IntegrationHelper.SendPriceItemsToSAPSchedule(new List<String>(serviceRequestIdSet),true); 
        }
    }
    global void finish(database.batchablecontext bc){
    
    }
    global void execute(schedulablecontext sc){
        OB_SendPriceItemsToSAPScheduler sc1 = new OB_SendPriceItemsToSAPScheduler(); 
        database.executebatch(sc1,50);
    }
}