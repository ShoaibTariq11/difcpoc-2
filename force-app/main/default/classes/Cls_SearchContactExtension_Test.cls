@isTest
public class Cls_SearchContactExtension_Test{
    
    static testMethod void SearchContact(){
        
        
      Account acc = new Account();
      acc.Name = 'Test Account';
      insert acc;
      
      Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        objContact.AccountId = acc.ID;
        objContact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objContact;
        
       Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = acc.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
        
        
        Cls_SearchContactExtension searchExt = new Cls_SearchContactExtension();
        searchExt.ClientName = 'Test';
        searchExt.currentAccountId = acc.ID;
        searchExt.SearchContact();
        searchExt.clearList();
    
    }


}