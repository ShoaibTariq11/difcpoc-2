/*----------------------------------------------------------------------------------------------------------------------
Modification History     
-----------------------------------------------------------------------------------------------------------------------
 V.No    Date        Updated By      Description
 V1.1    05-10-2016  Sravan          Added code to decrypt Password Tkt # 3392
 V1.2    19-12-2018  Azfer           Updated the Http to HTTPS in the call. Comment Style: AP-001
----------------------------------------------------------------------------------------------------------------------*/
global class CLS_SendSMS{
    @future (callout=true)
    
    webservice static void sendsms(String SRMob,string strMessage){
        system.debug('SRMob==>'+SRMob);
        system.debug('strMessage==>'+strMessage);
        
        //Make the mobile number in required format
        String mobileNo = '971'+SRMob.right(9);
        system.debug('MMMMM'+mobileNo);
        
        String SMSMessage='';
        SMSMessage = strMessage;
        
        //Encode the message in UTF-8
        SMSMessage= EncodingUtil.urlEncode(SMSMessage, 'UTF-8');
        system.debug('SSSMSSS'+SMSMessage);  
        
        //Creating a HTTP request to SMSCountry with the sms parameters       
        Http http = new Http();
        HttpRequest req = new HttpRequest(); 
        HttpResponse res = new HttpResponse();
        req.setTimeout(100000); //milliseconds   
       
        
        string decryptedSMSPassWrd = test.isRunningTest() ==false ? PasswordCryptoGraphy.DecryptPassword(Label.SMS_Password) : '123456' ;  // V1.1 
        
        
        //Setting the SMS endpoint with required parameters
        //AP-001 Starts
        //Ticket #6117  Replaced http with https
        req.setEndpoint(System.Label.SMS_EndPoint+'?user='+Label.SMS_Username+'&passwd='+decryptedSMSPassWrd+'&mobilenumber='+mobileNo+'&message='+SMSMessage+'&sid=DIFC&mtype=N&DR=Y'); 
        //AP-001 End
        req.setMethod('GET');
        
        try{
            if(!System.Test.isRunningTest()){  //to prevent callouts while running test class
                //Sending the HTTP request
                res = http.send(req);
                system.debug('***Body was:' + res.getBody());
            }
            
        } catch(System.CalloutException ex) {
            System.debug('Callout error: '+ ex);
            //AP-001 Starts
        	//Ticket #6117, updated the insert of the log record to get better code coverage.
            Log__c objLog = new Log__c(Description__c ='Line No===>'+ex.getLineNumber()+'---Message==>'+ex.getMessage(), Type__c = 'HTTP Callout to SMS' );
            //AP-001 End
            insert objLog; 
           
        }
    }
}