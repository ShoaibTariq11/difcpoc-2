/**********************************************************
Class Name: AppointmentSlotCreationBatch to create the slots in appointment object
Scheduler Class : AppointmentSchedulerBatchScheduler 
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By       Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    22-12-2019  Selva    #7365  
********************************************************/
// Batch Job for Processing the Records
global class AppointmentSlotCreationBatch implements Database.Batchable<sobject> {
       public static BusinessHours BH ;
   global Database.Querylocator start(Database.BatchableContext BC) {
      return Database.getQueryLocator('select id,name,Appointment_StartDate_Time__c,Appointment_EndDate_Time__c,Step__c,Service_Request__c from Appointment__c where Appointment_StartDate_Time__c!=null and Service_Type__c= \'Normal\' order by Appointment_StartDate_Time__c DESC limit 1'); 
      
      //return Database.getQueryLocator('select id,Step_Code__c, sr__c,createdDate,Biometrics_Required__c,AppointmentCreation__c from step__c where AppointmentCreation__c=\'Create1\' order by createdDate ASC limit 1');
   }
   global void execute (Database.BatchableContext BC, List<Appointment__c> scope) {
        system.debug('-scope--'+scope.size());
        bh = [SELECT Id, FridayStartTime, MondayEndTime, MondayStartTime, SaturdayEndTime, SaturdayStartTime, SundayEndTime, SundayStartTime, ThursdayEndTime, ThursdayStartTime, TuesdayEndTime, TuesdayStartTime, WednesdayStartTime, WednesdayEndTime FROM BusinessHours where name = 'GS- Medical Appointment Express' limit 1 ];
        
        List<Appointment__c> expqueryAppList = Database.query('select id,name,Appointment_StartDate_Time__c,Appointment_EndDate_Time__c,Step__c,Service_Request__c from Appointment__c where Appointment_StartDate_Time__c!=null and Service_Type__c= \'Express\' order by Appointment_StartDate_Time__c DESC limit 1');
        
        Datetime NormalslotStartTime = null;
        Datetime ExpslotStartTime = null;
        for(Appointment__c itr:scope){
          NormalslotStartTime = itr.Appointment_EndDate_Time__c;
        }
        for(Appointment__c exp : expqueryAppList){
            ExpslotStartTime = exp.Appointment_EndDate_Time__c;
        }
        system.debug('--slot start time--'+NormalslotStartTime);
        List<Appointment__c> normalAppList = new List<Appointment__c>();
        List<Appointment__c> ExpAppList = new List<Appointment__c>();
        
        for(Integer i=0;i<75;i++){
            if(NormalslotStartTime!=null){
                Boolean isWithin= BusinessHours.isWithin(bh.id,NormalslotStartTime);
                Appointment__c app = new Appointment__c();
                if(isWithin){
                    NormalslotStartTime = AppointmentCreationUtilityCls.checklunchtime(NormalslotStartTime,'Normal');
                    app.Appointment_StartDate_Time__c = NormalslotStartTime;
                    app.Appointment_EndDate_Time__c = NormalslotStartTime.addminutes(5);
                    NormalslotStartTime = app.Appointment_EndDate_Time__c;
                }else if(!isWithin){
                    Datetime  StartDatetime = BusinessHours.nextStartDate(BH.Id,NormalslotStartTime);
                    Date nextNormalBusinessStartDate = Date.newinstance(StartDatetime.year(), StartDatetime.month(), StartDatetime.day());
                    app.Appointment_StartDate_Time__c = StartDatetime;
                    app.Appointment_EndDate_Time__c = StartDatetime.addminutes(5);
                    NormalslotStartTime = app.Appointment_EndDate_Time__c;
                }
                app.Applicantion_Type__c='Normal';
                app.Slot_Status__c='Available';
                normalAppList.add(app);
            }   
        }
        system.debug('--ExpslotStartTime-starts-'+ExpslotStartTime);
        for(Integer i=0;i<75;i++){
            if(ExpslotStartTime!=null){
                Boolean isWithin= BusinessHours.isWithin(bh.id,ExpslotStartTime);
                Appointment__c app = new Appointment__c();
                if(isWithin){
                    ExpslotStartTime = AppointmentCreationUtilityCls.checklunchtime(ExpslotStartTime,'Express');
                    app.Appointment_StartDate_Time__c = ExpslotStartTime;
                    app.Appointment_EndDate_Time__c =  app.Appointment_StartDate_Time__c.addminutes(5);
                    ExpslotStartTime = app.Appointment_EndDate_Time__c;
                }else if(!isWithin){
                    Datetime  StartDatetime = BusinessHours.nextStartDate(BH.Id,ExpslotStartTime);
                    Date nextNormalBusinessStartDate = Date.newinstance(StartDatetime.year(), StartDatetime.month(), StartDatetime.day());
                    app.Appointment_StartDate_Time__c = StartDatetime.addminutes(2);
                    app.Appointment_EndDate_Time__c = app.Appointment_StartDate_Time__c.addminutes(5);
                    ExpslotStartTime = app.Appointment_EndDate_Time__c;
                }
                system.debug('--ExpslotStartTime--'+ExpslotStartTime);
                app.Applicantion_Type__c='Express';
                app.Slot_Status__c='Available';
                ExpAppList.add(app);
            }   
        }
        if(normalAppList.size()>0){
            insert normalAppList;
        }
        if(ExpAppList.size()>0){
            insert ExpAppList;
        }
   }
   global void finish(Database.BatchableContext BC) {
       
   }
}