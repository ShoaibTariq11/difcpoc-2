public class SRExemptedUBOViewcontroller {

    Public List<Amendment__c> getObjAmendment() {

        //return ListAmdDDetails()[0];
        return ListAmdDDetails();

    }


    Public List < Amendment__c > ListAmdDDetails() {

        return [select id, Gender__c, Job_Title__c, Customer__c, Title_new__c, Date_of_Birth__c, Family_Name__c, Place_of_Birth__c, Middle_Name__c, Nationality_list__c,
            Given_Name__c, Passport_No__c, Person_Email__c, Passport_Expiry_Date__c, Mobile__c,
            Phone__c, Apt_or_Villa_No__c, Building_Name__c, PO_Box__c, Street__c, Post_Code__c, Emirate_State__c, Permanent_Native_Country__c,
            Permanent_Native_City__c, Status__c, ServiceRequest__c from Amendment__c where ServiceRequest__c =: Sr_Id
        ];
    }

    public string Sr_Id {
        get;
        set;
    }

    Public UBO_Data__c getObjUBO() {

        return ListUBOList()[0];

    }

    Public List < UBO_Data__c > ListUBOList() {

        return [select id, Apartment_or_Villa_Number__c, BC_UBO__c, Building_Name__c, City_Town__c,
            Country__c, Date_Of_Becoming_a_UBO__c, Date_of_Birth__c, Date_Of_Registration__c, Email__c, Emirate_State_Province__c,
            Entity_Name__c, Entity_Type__c, First_Name__c, Gender__c, I_am_Exempt_entities__c,
            Individual_UBO__c, Is_Existing__c, Last_Name__c, Name_of_Regulator_Exchange_Govt__c, Nationality__c,
            Passport_Data_of_Issuance__c, Passport_Expiry_Date__c, Passport_Number__c,
            Phone_Number__c, Place_of_Birth__c, Place_of_Registration__c, PO_Box__c, Post_Code__c,
            Reason_for_exemption__c, Relationship__c, Select_Regulator_Exchange_Govt__c, Service_Request__c,
            Status__c, Street_Address__c, Title__c from UBO_Data__c
            where Service_Request__c =: Sr_Id
        ];

    }


}