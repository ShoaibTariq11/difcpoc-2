/**
 * Description : Controller for OB_docuSignEnvelopeStatustrigger 
 *
 * ****************************************************************************************
 * History :
 * [06.FEB.2020] Prateek Kadkol - Code Creation
 */
public without Sharing class  OB_docuSignEnvelopeStatusTriggerHandler {
    
    public static void execute_BI(list<dfsle__EnvelopeStatus__c > TriggerNew) {
        
        map<string,dfsle__Envelope__c> envelopeLinkingMap = new map<string,dfsle__Envelope__c>();
        set<string> docuSignEnvIdList = new set<string>();
        
        
        for(dfsle__EnvelopeStatus__c statusObj : TriggerNew){
            docuSignEnvIdList.add(statusObj.dfsle__DocuSignId__c);
        }
        system.debug(docuSignEnvIdList);
        
        if(docuSignEnvIdList.size() > 0){
            for(dfsle__Envelope__c envObj : [SELECT id , dfsle__DocuSignId__c,step__c FROM dfsle__Envelope__c WHERE dfsle__DocuSignId__c IN: docuSignEnvIdList]){
                envelopeLinkingMap.put(envObj.dfsle__DocuSignId__c, envObj );
            }
        }
        system.debug(envelopeLinkingMap);
        for(dfsle__EnvelopeStatus__c statusObj : TriggerNew){
            if(envelopeLinkingMap.containsKey(statusObj.dfsle__DocuSignId__c)){
                statusObj.DocuSign_Envelope__c = envelopeLinkingMap.get(statusObj.dfsle__DocuSignId__c).Id;
                statusObj.Step__c = envelopeLinkingMap.get(statusObj.dfsle__DocuSignId__c).step__c;
            }
        }
        
        
    }
    
    public static void execute_AI(list<dfsle__EnvelopeStatus__c > TriggerNew) {
        
    }
    
    public static void execute_BU(list<dfsle__EnvelopeStatus__c > TriggerNew,map<Id,dfsle__EnvelopeStatus__c > TriggerOldMap) {
        
        
    }
    
    public static void execute_AU(list<dfsle__EnvelopeStatus__c > TriggerNew,map<Id,dfsle__EnvelopeStatus__c > TriggerOldMap) {
        
        set<id> actionItemToCheck = new set<id>();
        set<id> actionItemsToClose = new set<id>();
        set<id> IdsInTrigger = new set<id>();
        
        for(dfsle__EnvelopeStatus__c statusObj : TriggerNew){
            if(statusObj.dfsle__Status__c != TriggerOldMap.get(statusObj.Id).dfsle__Status__c && statusObj.dfsle__Status__c == 'completed'){
                actionItemToCheck.add(statusObj.step__c);
                IdsInTrigger.add(statusObj.id);
            }
        }
        
        try{
            for(HexaBPM__Step__c stepObj : [SELECT id,HexaBPM__SR__c,HexaBPM__SR_Step__c,HexaBPM__Status__c, (select id from DocuSign_Status__r WHERE  dfsle__Status__c != 'completed' AND dfsle__Status__c != 'Voided' AND id NOT IN : IdsInTrigger) from HexaBPM__Step__c WHERE id IN: actionItemToCheck]){
                system.debug('related envelope size ' +stepObj.DocuSign_Status__r.size());
                if(stepObj.DocuSign_Status__r.size() == 0 ){
                    actionItemsToClose.add(stepObj.Id);
                    CC_AutoCloseStep objAutoClose = new CC_AutoCloseStep();
                    string result = objAutoClose.EvaluateCustomCode(null,stepObj);
                    system.debug('Auto close result ' +result);
                }
                
            }     
        }catch(Exception e){
              
            Log__c objLog = new Log__c();
                objLog.Description__c = 'Exception on OB_docuSignEnvelopeStatusTriggerHandler  :'+e.getMessage();
                objLog.Type__c = 'Docusign Envelope Status Error';
                insert objLog;
          }
                 
        
        
    }
}