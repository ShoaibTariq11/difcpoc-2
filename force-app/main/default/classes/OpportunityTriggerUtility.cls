/********************************************
* Class Name : OpportunityTriggerUtility 
* Description : if the lead converted with Offer Process ,all the related offer process will be linked with opportunity
* Author : Selva
*  Date : 16-jul-2018 , Lead and Offer Process flow
*********************************************/
public class OpportunityTriggerUtility {
    
    public static Boolean isOfferProcessupdated = false;
    
    public static void updateOfferProcessonOpportunity(Map<Id,string> leadId){
        try{
        system.debug('-----leadId.keyset()----'+leadId.keyset());
            List<Offer_Process__c> updateOppId = [select id,Opportunity__c,Lead__c from Offer_Process__c where Lead__c IN: leadId.keyset()];
            if(updateOppId.size()>0){
                for(Offer_Process__c itr:updateOppId ){
                    
                    itr.Opportunity__c = leadId.get(itr.Lead__c);
                }
                isOfferProcessupdated = true;
                system.debug('-----updateOppId---'+updateOppId);
                update updateOppId;
        }
        
        }catch(Exception e){
            system.debug('---'+e);
        }
        
        
    }
    
    //check the attachment in each stage for offer process record type.
    public static Boolean isAttched(List<Opportunity> opplist){
        Boolean isattached = false;
        set<string> oppIdset = new set<string>();
        for(opportunity itr:opplist){
            if(itr.Oppty_Rec_Type__c=='Leasing OfferProcess'){
                oppIdset.add(itr.Id);
            }
        }
        if(oppIdset.size()>0){
            List<Attachment> att = [select id from attachment where parentId IN:oppIdset];
            Integer attSize = att.size();
            for(opportunity itr:opplist){
                if(attSize <1 && itr.stageName =='Confirmed Unit'){
                    //itr.addError('Please be informed,attach the required document before changing the stage to-'+itr.stageName);
                }
                if(attSize <2 && itr.stageName =='LAF Process'){
                    itr.addError('Please be informed,attach the required document before changing the stage to-'+itr.stageName);
                }
                if(attSize <3 && itr.stageName =='Reservation Letter Sent to Client'){
                    itr.addError('Please be informed,attach the required document before changing the stage to-'+itr.stageName);
                }
                if(attSize <4 && itr.stageName =='Signed Reservation LetterReceived'){
                    itr.addError('Please be informed,attach the required document before changing the stage to-'+itr.stageName);
                }
                if(attSize <5 && itr.stageName =='Send Reservation Letter Received'){
                    itr.addError('Please be informed,attach the required document before changing the stage to-'+itr.stageName);
                }
                if(attSize <6 && itr.stageName =='Reservation Payment Received and Confirmed'){
                    itr.addError('Please be informed,attach the required document before changing the stage to-'+itr.stageName);
                }
                if(attSize <7 && itr.stageName =='Lease Agreement Sent to Client'){
                    itr.addError('Please be informed,attach the required document before changing the stage to-'+itr.stageName);
                }
                if(attSize <8 && itr.stageName =='Signed Lease Agreement Received'){
                    itr.addError('Please be informed,attach the required document before changing the stage to-'+itr.stageName);
                }
                if(attSize <9 && itr.stageName =='Send Signed Lease Agreement to Client'){
                    itr.addError('Please be informed,attach the required document before changing the stage to-'+itr.stageName);
                }
            }
        }
       return isattached; 
    }
    
}