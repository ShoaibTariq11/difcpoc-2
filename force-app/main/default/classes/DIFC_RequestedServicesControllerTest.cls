@isTest
public class DIFC_RequestedServicesControllerTest {
    public static testmethod void test1(){
         // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        Test.startTest();
           Id p = [select id from profile where name='DIFC Customer Community User Custom'].id;      
            Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
            insert con;  
                      
            User user = new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                    ContactId = con.Id,
                    timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
           
            insert user;
        system.runAs(user){
			           
            DIFC_RequestedServicesController.RequestWrapper reqWrap = new DIFC_RequestedServicesController.RequestWrapper();
            reqWrap.requestType = 'test';
            reqWrap.serviceNumber = '123';
            reqWrap.serviceStatus = 'Test';
            reqWrap.taxRegistrationNumber = '111';
            
            String reqString = JSON.serialize(reqWrap);
            
            DIFC_RequestedServicesController.getAccountDetail();
            DIFC_RequestedServicesController.getLoggedInUserAccountName();
            DIFC_RequestedServicesController.getLoggedInUserAccount();
            DIFC_RequestedServicesController.getRequestedServices(reqString);
            DIFC_RequestedServicesController.updateAccountVatRegistrationNumber(reqString);
            DIFC_RequestedServicesController.checkVATNumber();
            DIFC_RequestedServicesController.updateAccountField();
        }
       Test.stopTest();
    }
}