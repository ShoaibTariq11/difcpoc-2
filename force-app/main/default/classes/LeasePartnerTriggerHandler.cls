/******************************************************************************************
*  Author   : Ravindra Babu Nagaboina
*  Company  : NSI DMCC
*  Date     : 09th Nov 2016   
*  Description : This class will be used as Lease Trigger Handler.                       
--------------------------------------------------------------------------------------------------------------------------
Modification History 
----------------------------------------------------------------------------------------
V.No    Date                Updated By          Description
----------------------------------------------------------------------------------------              
V1.0    09/Nov/2016         Ravi                Created
v1.1    22/Nov/2017         Arun                #4353 Added filters  
v1.2    05/Mar/2019         Arun                #6461 Updated the condition used to creating opp location
*******************************************************************************************/
public with sharing class LeasePartnerTriggerHandler implements TriggerFactoryInterface {
    public void executeBeforeInsertTrigger(list < sObject > lstNewRecords) {
    }

    public void executeBeforeUpdateTrigger(list < sObject > lstNewRecords, map < Id, sObject > mapOldRecords) {
    }

    public void executeBeforeInsertUpdateTrigger(list < sObject > lstNewRecords, map < Id, sObject > mapOldRecords) {
    }

    public void executeAfterInsertTrigger(list < sObject > lstNewRecords) {//CreateOperatingLocaitonsFromLease(lstNewRecords);
    }

    public void executeAfterUpdateTrigger(list < sObject > lstNewRecords, map < Id, sObject > mapOldRecords) {
    }

    public void executeAfterInsertUpdateTrigger(list < sObject > lstNewRecords, map < Id, sObject > mapOldRecords) {
        map < Id, Lease_Partner__c > mapOldLeasePartnerRecords = new map < Id, Lease_Partner__c > ();
        if (mapOldRecords != null)
            mapOldLeasePartnerRecords = (map < Id, Lease_Partner__c > ) mapOldRecords;
        CreateOperatingLocaitonsFromLease(lstNewRecords, mapOldLeasePartnerRecords);
    }

    public void CreateOperatingLocaitonsFromLease(list < Lease_Partner__c > lstLPs, map < Id, Lease_Partner__c > mapOldLeasePartnerRecords) {
        list < Operating_Location__c > lstOperatingLocations = new list < Operating_Location__c > ();
        map < Id, list < Lease_Partner__c >> mapLeasePartners = new map < Id, list < Lease_Partner__c >> ();

        if (lstLPs != null) {
            for (Lease_Partner__c objLP: lstLPs) {
                if (mapOldLeasePartnerRecords != null && !mapOldLeasePartnerRecords.isEmpty()) {
                    Lease_Partner__c objOldLP = mapOldLeasePartnerRecords.get(objLP.Id);
                    // v1.1 Is_Valid_Operating_Location__c=true
                    //v1.2 updated the following if and else if
                    // Added a new field on operating location for upsert purposes.
                    if (objOldLP != null && objOldLP.Status__c != 'Active' && objLP.Status__c == 'Active' &&  objLP.Is_Valid_Operating_Location__c ) {
                        list < Lease_Partner__c > lst = mapLeasePartners.containsKey(objLP.Lease__c) ? mapLeasePartners.get(objLP.Lease__c) : new list < Lease_Partner__c > ();
                        lst.add(objLP);
                        mapLeasePartners.put(objLP.Lease__c, lst);
                    }
                } else if ( objLP.Is_Valid_Operating_Location__c ) {
                    list < Lease_Partner__c > lst = mapLeasePartners.containsKey(objLP.Lease__c) ? mapLeasePartners.get(objLP.Lease__c) : new list < Lease_Partner__c > ();
                    lst.add(objLP);
                    mapLeasePartners.put(objLP.Lease__c, lst);
                }
            }
            if (!mapLeasePartners.isEmpty()) { //Create Operating Locations
                Operating_Location__c objOL;
                for (Id LeaseId: mapLeasePartners.keySet()) {
                    for (Lease_Partner__c objLP: mapLeasePartners.get(LeaseId)) {
                        objOL = new Operating_Location__c();
                        objOL.Unique_id__C = objLP.Account__c + '-' + objLP.Unit__c; 
                        objOL.Account__c = objLP.Account__c;
                        objOL.Unit__c = objLP.Unit__c;
                        objOL.Lease__c = objLP.Lease__c;
                        objOL.Lease_Partner__c = objLP.Id;
                        objOL.Status__c = 'Active';
                        objOL.Type__c = objLP.Is_License_to_Occupy__c ? 'Purchased' : 'Leased';
                        lstOperatingLocations.add(objOL);
                    }
                }
            }
            //added a label to insert/upsert the operating location based on the value 
            // false will insert it 
            // anyting other than false will upsert it.
            if (!lstOperatingLocations.isEmpty()) {
                if( label.Upsert_Operating_Location == 'false' ){
                    insert lstOperatingLocations;
                }else{
                    upsert lstOperatingLocations Unique_id__C;
                }
            }
        }
    }
}