@isTest
public class AcceptCaseControllerTest {
    @isTest
    private static void init(){

        Case ca = new Case(
        GN_Type_of_Feedback__c = null,
        Type = 'Parking',
        Status = 'Awaiting Response - Client',
        Origin = 'Email',
        Subject = 'Testing',
        Full_Name__c='Raghav',
        GN_Sys_Case_Queue__c = 'IT',
        GN_Registration_No__c = '123456');
        
        insert ca;
        
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Case');
            insert testQueue;
            GroupMember gm = new GroupMember();
            gm.GroupId = testGroup.id;
            gm.UserOrGroupId = UserInfo.getUserId();
            insert gm;
        }
        
        //Case aCase = new Case();
        //insert aCase;
        ca.OwnerId = testGroup.Id;
        update ca; 
  
        AcceptCaseController.getCase(ca.id);
        AcceptCaseController.invalidCase(ca.id);
        AcceptCaseController.getUserId();
        AcceptCaseController.saveCaseWithOwner(ca.id);
        System.debug('userinfo.getuserid()'+userinfo.getuserid());
        System.debug('ca.ownerid'+ca);

    }
}