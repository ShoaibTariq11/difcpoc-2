global class BatchCRSGenerateXml implements Database.Batchable<sObject>{

   global final String Query;
   global final String formYearId;
   global final String formYearName;


   global BatchCRSGenerateXml(String q, String formId, String YearName)
   {

      Query=q;
       formYearId=formId;
        this.formYearName=YearName;
   }

 global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }
   
global void execute(Database.BatchableContext BC, List<CRS_Form__c> scope)
   {
       List<id> ListIds=new List<id>();
    for(CRS_Form__c Objform:scope)
    {
        ListIds.add(Objform.id);
    
    }
    
     Attachment a = new Attachment();
    a.Body =Blob.valueOf(CRS_generate_XML.generateMassXml(formYearId,ListIds)) ;
    a.Name='CRS '+formYearName+' XML Mass -'+DateTime.now().second()+'-'+DateTime.now().millisecond()+'.xml';
    a.parentid=formYearId;
    a.ContentType='.xml';
    insert a;
    
   

    }

  
   global void finish(Database.BatchableContext BC){
   // Get the ID of the AsyncApexJob representing this batch job
   // from Database.BatchableContext.
   // Query the AsyncApexJob object to retrieve the current job's information.
   AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
      TotalJobItems, CreatedBy.Email
      FROM AsyncApexJob WHERE Id =
      :BC.getJobId()];
   // Send an email to the Apex job's submitter notifying of job completion.
   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
   String[] toAddresses = new String[] {a.CreatedBy.Email};
   mail.setToAddresses(toAddresses);
   mail.setSubject('CRS files are ready -' + System.today().year());
   mail.setPlainTextBody ('CRS files are Ready to export . Number of Errors '+ a.NumberOfErrors + '.');
   Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
}

   
   
}