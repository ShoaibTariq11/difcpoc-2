/*
    Author      : Rica Ramos -- PWC
    Date        : 23-April-2019
    Description : Controller class for CaseReOpen page
    --------------------------------------------------------------------------------------
*/
public without sharing class CaseReopenController{

    public string caseId{get;set;}
    public Case objCase{get;set;}
    public string currentTheme{get;set;}
    public CaseReopenController(ApexPages.StandardController controller){

        if(apexpages.currentpage().getParameters().get('Id')!=null)
            caseId = String.escapeSingleQuotes(apexpages.currentpage().getParameters().get('Id'));
        objCase = new Case();
        objCase = [SELECT Id,Status, GN_Department__c,GN_Type_of_Feedback__c,GN_Proposed_Due_Date__c,Origin,GN_Sys_Case_Queue__c, Type,GN_Other_Reopen_Reason__c, GN_Are_you_sure_you_want_to_reopen__c, GN_Reopen_Reason__c,RecordtypeId,AccountId,GN_Account_Name_Non_DIFC__c,GN_Client_Type__c FROM Case WHERE Id = :caseId];
        objCase.GN_Are_you_sure_you_want_to_reopen__c = 'Yes';
        System.debug('##UI: '+UserInfo.getUiThemeDisplayed());
        currentTheme = UserInfo.getUiThemeDisplayed();
    }

    public pagereference CancelAction(){
        Pagereference pg = new Pagereference('/'+caseId);
        pg.setredirect(true);
        return pg;
    }

    public pagereference ReopenAction(){  
        //Savepoint sp = Database.setSavepoint();
        try{
            List<Recordtype> recordtypeId = [Select Id FROM Recordtype WHERE DeveloperName = 'Default' AND SObjectType = 'Case' LIMIT 1];
            if(!recordtypeId.isEmpty()){
                objCase.RecordtypeId = recordtypeId[0].Id;
            }
            objCase.Status = 'In Progress';
            /*
            string[] str_ProposedDueDate_ForType_Arr = new string[]{'complaint','client survey','focus group','mystery shopping'};
            if(objCase.GN_Proposed_Due_Date__c == null && String.IsNotBlank(objCase.GN_Type_of_Feedback__c)
                && str_ProposedDueDate_ForType_Arr .indexOf(objCase.GN_Type_of_Feedback__c.toLowerCase()) > -1 ){
                objCase.GN_Proposed_Due_Date__c = Date.Today().addDays(4); //Adds 4 days from current date for the existing cases in prod[since val.rule was added during later stage. and error will not throw for existing cases]
            }
            */
            map<string,SLA_Configurations__mdt> MapSLAMaster = new map<string,SLA_Configurations__mdt>();
            for(SLA_Configurations__mdt SLA:[select Id,Business_Hour_Id__c,Queue_Name__c,SLA_Hours__c,SLA_Minutes__c,Type__c from SLA_Configurations__mdt limit 10000])
                MapSLAMaster.put(SLA.Queue_Name__c.tolowercase()+'_'+SLA.Type__c,SLA);
            string type_Origin;
            if(objCase.GN_Type_of_Feedback__c != null && objCase.GN_Type_of_Feedback__c != 'Invalid'){
                type_Origin = objCase.GN_Type_of_Feedback__c;
            } else{
                type_Origin = objCase.Origin;
            }  
            Long sla=0;
            if(String.isNotBlank(objCase.GN_Sys_Case_Queue__c) && String.isNotBlank(type_Origin) && MapSLAMaster.get(objCase.GN_Sys_Case_Queue__c.toLowerCase()+'_'+type_Origin) != null ){
                sla = MapSLAMaster.get(objCase.GN_Sys_Case_Queue__c.toLowerCase()+'_'+type_Origin).SLA_Minutes__c.longvalue();
                sla = sla*60*1000L;  
            }  
            if(objCase != null){
                List<Case_SLA__c> slaToBeInsertedlist = new List<Case_SLA__c>();
                List<Case> updateParentCase = new List<Case>();
                //for(String keyStr : mapcaseSLA.keySet()){
                Case_SLA__c newObjSLA = new Case_SLA__c(); //mapcaseSLA.get(keyStr).clone();
                newObjSLA.From__c = System.Now();
                newObjSLA.Until__c = null;
                newObjSLA.Change_Type__c = 'Owner';
                newObjSLA.Status__c = 'In Progress';
                newObjSLA.Parent__c = objCase.Id;
                newObjSLA.Owner__c = objCase.GN_Sys_Case_Queue__c;
                objCase.GN_Due_Date_Time__c = newObjSLA.Due_Date_Time__c;
                if(String.isNotBlank(objCase.GN_Sys_Case_Queue__c) && String.isNotBlank(type_Origin) && MapSLAMaster.get(objCase.GN_Sys_Case_Queue__c.toLowerCase()+'_'+type_Origin) != null ){
                    newObjSLA.Business_Hours_Id__c = MapSLAMaster.get(objCase.GN_Sys_Case_Queue__c.toLowerCase()+'_'+type_Origin).Business_Hour_Id__c;
                    //Long busHoursRemaining = BusinessHours.diff(mapcaseSLA.get(keyStr).Business_Hours_Id__c,mapcaseSLA.get(keyStr).Until__c,mapcaseSLA.get(keyStr).Due_Date_Time__c);
                    //DateTime newDueDate = BusinessHours.add(mapcaseSLA.get(keyStr).Business_Hours_Id__c,System.Now(),busHoursRemaining);
                    newObjSLA.Due_Date_Time__c = BusinessHours.add(newObjSLA.Business_Hours_Id__c,system.now(),sla); //newDueDate;
                    slaToBeInsertedlist.add(newObjSLA);
                }
                if(!slaToBeInsertedlist.isEmpty()){
                    insert slaToBeInsertedlist;
                    update objCase;
                }
            }
            
            Pagereference pg = new Pagereference('/'+caseId);
            pg.setredirect(true);
            return pg;
        } catch(Exception ex){ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage());ApexPages.addMessage(msg);
            return null;
        }
    }
}