@isTest(seeAllData=false)
public class TestPublicRegistry {
    static testMethod void myUnitTest() {
        
        //Creating test record for PR
        Account acc1= new Account();
        acc1.name='test account1';
        acc1.Home_Country__c='United Kingdom';
        acc1.Has_Permit_1__c=True;
        acc1.Has_Permit_2__c=False;
        acc1.Has_Permit_3__c=False;
        acc1.Nature_of_business__c='Testing';
        acc1.Phone='+9712223444444';
        acc1.Authorized_Share_Capital__c=2000000;
        acc1.Company_Type__c='Retail';
        acc1.Trade_Name__c='Test Trade';
        acc1.Registration_License_No__c='Test101';
        acc1.ROC_Status__c='Dissolved';
        acc1.Legal_Type_of_Entity__c='LTD';
        acc1.DFSA_License_Status__c='Yes';
        acc1.Place_of_Registration__c='United Arab Emirates';
        acc1.Registered_Address__c='Office-101, Floor-1,DIFC';
        acc1.Remarks__c='test';
        acc1.ROC_reg_incorp_Date__c=date.newinstance(2010,10,28);
        acc1.Financial_Year_End__c='December 31st';
        acc1.Sector_Classification__c='test';
        acc1.Liquidated_Date__c=date.newinstance(2014,10,28);
        acc1.DNFBP__c='No';
        acc1.DFSA_License_Status__c='Active';
        acc1.Push_to_PR__c=true;
        insert acc1;
        
        Account acc= new Account();
        acc.name='test account';
        acc.Home_Country__c='United Kingdom';
        acc.Has_Permit_1__c=True;
        acc.Has_Permit_2__c=False;
        acc.Has_Permit_3__c=False;
        acc.Nature_of_business__c='Testing';
        acc.Phone='+9712223444444';
        acc.Authorized_Share_Capital__c=2000000;
        acc.Company_Type__c='Financial - related';
        acc.Trade_Name__c='Test Trade';
        acc.Registration_License_No__c='Test102';
        acc.ROC_Status__c='Active';
        acc.Legal_Type_of_Entity__c='RLLP';
        acc.DFSA_License_Status__c='Yes';
        acc.Place_of_Registration__c='United Arab Emirates';
        acc.Registered_Address__c='Office-101, Floor-1,DIFC';
        acc.Remarks__c='test';
        acc.ROC_reg_incorp_Date__c=date.newinstance(2010,10,28);
        acc.Financial_Year_End__c='December 31st';
        acc.Sector_Classification__c='test';
        acc.Push_to_PR__c=true;
        insert acc;
        
        License_Activity_Master__c LAM= new License_Activity_Master__c();
        LAM.name='Test Activity';
        LAM.Type__c='License Activity';
        LAM.Sector_Classification__c='test';
        LAM.Enabled__c=true;
        insert LAM;
        
        License_Activity__c LA= new License_Activity__c();
        LA.Account__c=acc.id;
        LA.Activity__c=LAM.id;  
        LA.Sector_Classification__c='test';      
        insert LA;
        
        Account_Share_Detail__c ASD= new Account_Share_Detail__c();
        ASD.name='test';
        ASD.Account__c=acc.id;
        ASD.Start_Date__c=date.newinstance(2010,10,28);
        ASD.End_Date__c=date.newinstance(2015,10,27);
        insert ASD;
        
        //List<Permit__c> PList= new List<Permit__c>();
        Permit__c P1= new Permit__c();
        P1.Permit_Type__c='Notification to process personal data';
        P1.Account__c=acc.id;
        P1.Purpose_of_Processing_Data__c='Accounting & Auditing; Administration of Justice; Administration of Membership Records; Advt, Mrkt & PR for Others; Advt, Mrkt & PR for the DC Itself; Benifits, Grants & Loans Administration; Consultancy & Advisory Services; Credit Referencing; Debt Administration and Factoring; Education; Information & Data Bank Administration; Insurance Administration; Legal Services; Licensing & Registration; Pastoral Care; Pensions Administration; Policing; Private Investigation; Property Management; Provision of Financial Services; Research; Staff Administration; Other, Please Specify';
        P1.Other_Purpose_of_Processing_Data__c='test';
        P1.Data_Subjects_Personal_Data__c='Staff (Agents, Workers); Clients & Customer; Suppliers; Members; Complainants, Correspondents, Enquirers; Relatives and Associates; Advisors, Consultants, Prof. Experts; Other';
        P1.Other_Data_Subjects_Personal_Data__c='test';
        P1.Date_From__c= date.newinstance(2010,10,28);
        P1.Date_To__c= date.newinstance(2015,10,27);
        P1.Status__c='Active';
        insert P1;
        
        Data_Protection__c DP= new Data_Protection__c();
        DP.Permit__c=P1.id;
        DP.Name_of_Jurisdiction__c='United Kingdom';
        insert DP;
        
        
        Entity_History__c EH1 = new Entity_History__c();
        EH1.Account__c=acc.id;
        EH1.Field_Name__c='name';
        EH1.Old_Value__c='test';
        EH1.Date_of_Change__c=system.today();
        insert EH1;
        Entity_History__c EH2 = new Entity_History__c();
        EH2.Account__c=acc.id;
        EH2.Field_Name__c='Trade_Name__c';
        EH2.Old_Value__c='test';
        EH2.Date_of_Change__c=system.today();
        insert EH2;
        
        Account bc= new Account();
        bc.name='test bc';
        insert bc;
        
        Contact ind= new Contact();
        ind.FirstName='test';
        ind.LastName='test ind';
        insert ind;
        
        List<Relationship__c> Rlist = new  List<Relationship__c>();
        Relationship__c R1= new Relationship__c();
        R1.Subject_Account__c=acc.id;
        R1.Object_Account__c=bc.id;
        R1.Relationship_Type__c='Director';
        R1.Start_Date__c=date.newinstance(2010,10,28);                
        Rlist.add(R1);
        Relationship__c R2= new Relationship__c();
        R2.Subject_Account__c=acc.id;
        R2.Object_Contact__c=ind.id;
        R2.Relationship_Type__c='Has Manager';
        R2.Start_Date__c=date.newinstance(2010,10,28);
        Rlist.add(R2);
        Relationship__c R3= new Relationship__c();
        R3.Subject_Account__c=acc.id;
        R3.Object_Account__c=bc.id;
        R3.Relationship_Type__c='Has Member';
        R3.Start_Date__c=date.newinstance(2010,10,28);
        Rlist.add(R3);
        Relationship__c R4= new Relationship__c();
        R4.Subject_Account__c=acc.id;
        R4.Object_Account__c=bc.id;
        R4.Relationship_Type__c='Limited Partner';
        R4.Start_Date__c=date.newinstance(2010,10,28);
        Rlist.add(R4);
        Relationship__c R5= new Relationship__c();
        R5.Subject_Account__c=acc.id;
        R5.Object_Account__c=bc.id;
        R5.Relationship_Type__c='Shareholder';
        R5.Start_Date__c=date.newinstance(2010,10,28);
        Rlist.add(R5);
        Relationship__c R6= new Relationship__c();
        R6.Subject_Account__c=acc.id;
        R6.Object_Account__c=bc.id;
        R6.Relationship_Type__c='Secretary';
        R6.Start_Date__c=date.newinstance(2010,10,28);
        Rlist.add(R6);
        Relationship__c R7= new Relationship__c();
        R7.Subject_Account__c=acc.id;
        R7.Object_Account__c=bc.id;
        R7.Relationship_Type__c='Authorized Representative';
        R7.Start_Date__c=date.newinstance(2010,10,28);
        Rlist.add(R7);
        Relationship__c R8= new Relationship__c();
        R8.Subject_Account__c=acc.id;
        R8.Object_Account__c=bc.id;
        R8.Relationship_Type__c='General Partner';
        R8.Start_Date__c=date.newinstance(2010,10,28);
        Rlist.add(R8);
        Relationship__c R9= new Relationship__c();
        R9.Subject_Account__c=acc.id;
        R9.Object_Account__c=bc.id;
        R9.Relationship_Type__c='Auditor';
        R9.Start_Date__c=date.newinstance(2010,10,28);
        Rlist.add(R9);
        Relationship__c R10= new Relationship__c();
        R10.Subject_Account__c=acc.id;
        R10.Object_Account__c=bc.id;
        R10.Relationship_Type__c='Founding Member';
        R10.Start_Date__c=date.newinstance(2010,10,28);
        Rlist.add(R10);
        Relationship__c R11= new Relationship__c();
        R11.Subject_Account__c=acc.id;
        R11.Object_Account__c=bc.id;
        R11.Relationship_Type__c='Designated Member';
        R11.Start_Date__c=date.newinstance(2010,10,28);
        Rlist.add(R11);
        insert Rlist;
        Relationship__c R= new Relationship__c();
        R.Subject_Account__c=acc.id;
        R.Object_Account__c=bc.id;
        R.Relationship_Type__c='Member LLC';
        R.Start_Date__c=date.newinstance(2010,10,28);
        insert R;
        
        Shareholder_Detail__c SH= new Shareholder_Detail__c();
        SH.Relationship__c=R.id;
        SH.Account_Share__c=ASD.id;
        SH.No_of_Shares__c=100;
        SH.Account__c=acc.id;
        insert SH;
        
        License__c L= new License__c();
        L.Account__c= acc.id;
        L.License_Issue_Date__c=date.newinstance(2010,10,28);
        L.License_Expiry_Date__c=date.newinstance(2015,10,27);
        insert L;
        acc.Active_License__c=L.id;
        update acc;
        
        CLS_CreatePRXML cls = new CLS_CreatePRXML();
        string s=cls.getxmlRes();
        Test.setMock(WebServiceMock.class, new TestPRWebservice());
        TestPRWebservice.req.name=s;
        CLS_BatchPublicRegistry.callPR();
        
        
    }
}