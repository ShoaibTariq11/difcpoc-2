/******************************************************************************************
 *  Name        : GSPoBoxController 
 *  Author      : Claude Manahan
 *  Company     : PWC Digital Services
 *  Date        : 2017-3-12
 *  Description : Main controller for custom form for PO BOX services (3022)
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   07-03-2017   Claude        Created
*******************************************************************************************/
public without sharing class GSPoBoxController {
  
  /**
   * TODO: Change all GS forms to child classes
   * Stores the template description of the SR Template
   */
  public String templateDescription        {get; set;}
  
  /**
   * Stores the filter the user inputs
   * for searching an account
   */
  public String accountName            {get; set;}
  
  /**
   * Stores the selected company of 
   * the user
   */
  public String selectedCompanyName        {get; set;}
  
  /**
   * Stores the account record ID
   * of the selected  account
   */
  public String accountId              {get; set;}
  
  /**
   * Returns a list of accounts 
   * according to the filter of
   * the user
   */
  public list<Account> accountsList        {get {
                              getAccountNames();
                              return mappedAccounts.values();
                            } set;}
  
  /**
   * Stores an instance of the Service Request
   */
  private Service_Request__c srRecord;
  
  /**
   * Stores the account records according
   * to the filter of the user
   */
  private Map<Id,Account> mappedAccounts;
  
  /**
   * Default Standard Controller Constructor
   */
  public GSPoBoxController(Apexpages.StandardController standardController){
    
    if(!Test.isRunningTest()) standardController.addFields(new List<String>{'Company_Name__c'});
    
    /* Get the record */
    srRecord = (Service_Request__c) standardController.getRecord();
    srRecord.Would_you_like_to_opt_our_free_couri__c = 'Yes';
    
    /* Set the SR Template of the form */
    setTemplate();
    
    /* Set any default values in the form */
    setFormFields();
    
    /* Set the account details stored in the service request */
    setAccountFields();
    
    changeReason();
    
  }
  
  /**
   * Gets the account details stored 
   * service request
   */
  private void setAccountFields(){
    
    if(String.isNotBlank(srRecord.Company_Name__c) || Test.isRunningTest()){
      
      accountId = Test.isRunningTest() ? Test_GS_Utils.getGsAccountId() : srRecord.Company_Name__c;
      selectedCompanyName = Test.isRunningTest() ? 'Test Company' : [SELECT Name FROM Account WHERE Id = :srRecord.Company_Name__c].Name;
      
    }
    
  }
  
  /**
   * Searches the accounts according
   * to the entered filter
   */
  public void getAccountNames(){
    
    accountName = String.isBlank(accountName) ? '' : accountName.contains('*') ? accountName.replaceAll('\\*','%') : '%' + accountName + '%';
    
    mappedAccounts = String.isBlank(accountName) ? 
              new Map<Id,Account>([SELECT Id, Name, ROC_Status__c FROM Account WHERE (ROC_Status__c = 'Active' OR Roc_Status__c = 'Renewed') ORDER BY NAME LIMIT 100])
              : new Map<Id,Account>([SELECT Id, Name, ROC_Status__c FROM Account WHERE (ROC_Status__c = 'Active' OR Roc_Status__c = 'Renewed') AND Name LIKE :accountName ORDER BY NAME LIMIT 100]);
  }
  
  /**
   * Sets the selected account ID
   */
  public void setAccountId(){
    
    srRecord.Company_Name__c = accountId;
    
    selectedCompanyName = mappedAccounts.get(accountId).Name;
    
    srRecord.Business_Name__c = mappedAccounts.get(accountId).Name;
    
    mappedAccounts.clear();
    
    accountName = '';
  }
  
  /**
   * Changes the form according to 
   * the selected service category
   */
  public void checkCategory(){
    
    if(!srRecord.Service_Category__c.equals('Transfer of PO Box')){ 
      
      srRecord.Reason_for_Request__c = null;
      srRecord.Company_Name__c = null;
      accountId = '';
    }
    
  }
  
  public void changeReason(){
    
    if(String.isNotBlank(srRecord.Purpose_of_Notification__c)){
      srRecord.Reason_for_Request__c = srRecord.Purpose_of_Notification__c.equals('Others') ? srRecord.Reasons_to_transfer_Foreign_Entity__c : srRecord.Purpose_of_Notification__c; 
    }
    
  }
  
  /**
   * Sets the SR Template for the form
   */ 
  private void setTemplate(){
           
    templateDescription =  [select SR_Description__c from SR_Template__c where SR_RecordType_API_Name__c=:'PO_BOX'].SR_Description__c;
    
  }
  
  /**
   * Autofills any required fields in the form
   */
  private void setFormFields(){
    
    User currentUser = [SELECT Contact.AccountId, Contact.Account.Name, Contact.Account.Active_License__r.Name, Contact.Account.Index_Card__r.Name FROM User WHERE Id = :UserInfo.getUserId()];
    
    srRecord.Customer__c = Test.isRunningTest() ? Test_GS_Utils.getGsAccountId() : srRecord.Customer__c == null ? currentUser.Contact.AccountId : srRecord.Customer__c;
    srRecord.Establishment_Card_No__c = Test.isRunningTest() ? String.valueOf(Test_GS_Utils.getGsIdentificationId()) :  srRecord.Establishment_Card_No__c == null ? currentUser.Contact.Account.Index_Card__r.Name : srRecord.Establishment_Card_No__c;
    srRecord.License_Number__c = Test.isRunningTest() ? String.valueOf(Test_GS_Utils.getLicenseId()) :  srRecord.License_Number__c == null ? currentUser.Contact.Account.Active_License__r.Name :  srRecord.License_Number__c;
    
    srRecord.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'PO_BOX'].Id;
  }
    
}