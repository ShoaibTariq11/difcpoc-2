public class FormReviewSuperAdminController {
    
    
        public static void sendUserAccessFormEmail(Set<Id> setDocID){
            
            if(!system.isFuture()){
                sendUserAccessFormEmailfuture(setDocID);
            }
            else {
                Emailwithoutfuture(setDocID);
            }
        }
        @future(Callout=true)
        public static void sendUserAccessFormEmailfuture(Set<Id> setDocID){
            try{
                List<HexaBPM__Service_Request__c> lstServiceRequest = new List<HexaBPM__Service_Request__c>();
                List<messaging.SingleEmailMessage> messages = new List<messaging.SingleEmailMessage>();
                Set<Id> setDocumentID = new Set<Id>();
                List<HexaBPM__SR_Doc__c> lstSrDocDetails = new List<HexaBPM__SR_Doc__c>();
                lstSrDocDetails = [select id,Name,HexaBPM__Service_Request__r.HexaBPM__Email__c,HexaBPM__Service_Request__r.HexaBPM__SR_Template__r.Name,HexaBPM__Service_Request__c,HexaBPM__Service_Request__r.Name,HexaBPM__Service_Request__r.HexaBPM__Customer__r.Name from HexaBPM__SR_Doc__c where Id IN:setDocID AND Name= 'Super User Authorisation - Generated'];
                if(lstSrDocDetails.size() >0){
                    id owEmailID;
                    Id srId = lstSrDocDetails[0].HexaBPM__Service_Request__c;
                    OrgWideEmailAddress owa =[select id, Address from OrgWideEmailAddress where Address='noreply.portal@difc.ae' limit 1];
                    owEmailID = owa.ID;
                    
                    List<ContentVersion> lstVersion = new List<ContentVersion>();
                    lstVersion = [select VersionData,FileExtension,Application_Document__c,Application_Document__r.Name from ContentVersion where Application_Document__c =:lstSrDocDetails[0].ID];
                    messaging.SingleEmailMessage message = new messaging.SingleEmailMessage();
                    pagereference pg = new pageReference('/apex/SuperUserAccessFormEmailTemplate?id='+SRId);
                    if(!test.isRunningTest()){
                        message.SetHtmlBody(pg.getContent().toString());
                    }
                     message.SetToAddresses(new list<string>{lstSrDocDetails[0].HexaBPM__Service_Request__r.HexaBPM__Email__c});                 
                     message.setSubject(lstSrDocDetails[0].HexaBPM__Service_Request__r.HexaBPM__Customer__r.Name+'- Your Request for '+lstSrDocDetails[0].HexaBPM__Service_Request__r.HexaBPM__SR_Template__r.Name+'-'+lstSrDocDetails[0].HexaBPM__Service_Request__r.Name+ ' has been received. Request Number : '+lstSrDocDetails[0].HexaBPM__Service_Request__r.Name);
                     Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();            
                     if(lstVersion.size()>0){
                        efa.setFileName(lstVersion[0].Application_Document__r.Name+'.pdf');
                        efa.setBody(lstVersion[0].VersionData);
                        message.setFileAttachments(new List<Messaging.EmailFileAttachment>{efa});
                     }
                     
                     message.SetOrgWideEmailAddressID(owEmailID);
                     messages.add(message);   
                }   
                if(messages !=null && messages.size()>0){
                    messaging.SendEmail(messages); 
                }
            }catch(Exception e){
            
            }       
        }
        
        public static void Emailwithoutfuture(Set<Id> setDocID){
            try{
                List<HexaBPM__Service_Request__c> lstServiceRequest = new List<HexaBPM__Service_Request__c>();
                List<messaging.SingleEmailMessage> messages = new List<messaging.SingleEmailMessage>();
                Set<Id> setDocumentID = new Set<Id>();
                List<HexaBPM__SR_Doc__c> lstSrDocDetails = new List<HexaBPM__SR_Doc__c>();
                lstSrDocDetails = [select id,Name,HexaBPM__Service_Request__r.HexaBPM__Email__c,HexaBPM__Service_Request__r.HexaBPM__SR_Template__r.Name,HexaBPM__Service_Request__c,HexaBPM__Service_Request__r.Name,HexaBPM__Service_Request__r.HexaBPM__Customer__r.Name from HexaBPM__SR_Doc__c where Id IN:setDocID AND Name= 'Super User Authorisation - Generated'];
                if(lstSrDocDetails.size() >0){
                    id owEmailID;
                    Id srId = lstSrDocDetails[0].HexaBPM__Service_Request__c;
                    OrgWideEmailAddress owa =[select id, Address from OrgWideEmailAddress where Address='noreply.portal@difc.ae' limit 1];
                    owEmailID = owa.ID;
                    
                    List<ContentVersion> lstVersion = new List<ContentVersion>();
                    lstVersion = [select VersionData,FileExtension,Application_Document__c,Application_Document__r.Name from ContentVersion where Application_Document__c =:lstSrDocDetails[0].ID];
                    messaging.SingleEmailMessage message = new messaging.SingleEmailMessage();
                    pagereference pg = new pageReference('/apex/SuperUserAccessFormEmailTemplate?id='+SRId);
                    if(!test.isRunningTest()){
                        message.SetHtmlBody(pg.getContent().toString());
                    }
                     message.SetToAddresses(new list<string>{lstSrDocDetails[0].HexaBPM__Service_Request__r.HexaBPM__Email__c});                 
                     message.setSubject(lstSrDocDetails[0].HexaBPM__Service_Request__r.HexaBPM__Customer__r.Name+'- Your Request for '+lstSrDocDetails[0].HexaBPM__Service_Request__r.HexaBPM__SR_Template__r.Name+'-'+lstSrDocDetails[0].HexaBPM__Service_Request__r.Name+ ' has been received. Request Number : '+lstSrDocDetails[0].HexaBPM__Service_Request__r.Name);
                     Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();            
                     if(lstVersion.size()>0){
                        efa.setFileName(lstVersion[0].Application_Document__r.Name+lstVersion[0].FileExtension);
                        efa.setBody(lstVersion[0].VersionData);
                        efa.setContentType(lstVersion[0].FileExtension);
                        message.setFileAttachments(new List<Messaging.EmailFileAttachment>{efa});
                     }
                     
                     message.SetOrgWideEmailAddressID(owEmailID);
                     messages.add(message);   
                }   
                if(messages !=null && messages.size()>0){
                    messaging.SendEmail(messages); 
                }
            }catch(Exception e){
            
            }       
        }
}