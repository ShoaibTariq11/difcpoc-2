@isTest
public class CarParking_CtrlTest
{
  static testMethod void testCarParkingTest_Draft()
  {   
      
      Account acc = new Account();
      acc.Name = 'Test Account';
      insert acc;
      
      Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        objContact.AccountId = acc.ID;
        objContact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objContact;
        
       Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = acc.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
      
      Service_Request__c sr = new Service_Request__c();
      sr.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Car Parking').getRecordTypeId();
      sr.Customer__c = acc.ID;
      sr.Contact__c = objContact.ID;
      sr.Client_s_Representative_in_Charge__c = 'New Vehicle Registration';
      insert sr;
      test.startTest();
      
      Test.setCurrentPageReference(new PageReference('Page.CarParking')); 
      System.currentPageReference().getParameters().put('Id', sr.ID);
      System.currentPageReference().getParameters().put('isDetail', 'false');
      System.currentPageReference().getParameters().put('RecordType', sr.RecordTypeID);
      
      
      ApexPages.StandardController SRStandardController = new ApexPages.StandardController(sr);
      CarParking_Ctrl caCtrl = new CarParking_Ctrl(SRStandardController);
      caCtrl.AddRowNew();
      caCtrl.AddRowOld();
      caCtrl.GetCar();
      caCtrl.changeEvent();
      Test.stopTest();
      
      //ca.createAccount(‘TestclassAcc1’);
  
  }
  
  static testMethod void testCarParkingTest_DraftWithNoContact()
  {   
      
      Account acc = new Account();
      acc.Name = 'Test Account';
      insert acc;
      
      
      Service_Request__c sr = new Service_Request__c();
      sr.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Car Parking').getRecordTypeId();
      sr.Customer__c = acc.ID;
     
      sr.Client_s_Representative_in_Charge__c = 'New Vehicle Registration';
      insert sr;
      test.startTest();
      
      Test.setCurrentPageReference(new PageReference('Page.CarParking')); 
      System.currentPageReference().getParameters().put('Id', sr.ID);
      System.currentPageReference().getParameters().put('isDetail', 'false');
      System.currentPageReference().getParameters().put('RecordType', sr.RecordTypeID);
      
      
      ApexPages.StandardController SRStandardController = new ApexPages.StandardController(sr);
      CarParking_Ctrl caCtrl = new CarParking_Ctrl(SRStandardController);
     
      caCtrl.GetCar();
     
      Test.stopTest();
      
      //ca.createAccount(‘TestclassAcc1’);
  
  }
  
  static testMethod void testCarParkingTest_DraftWithCar()
  {   
      
      Account acc = new Account();
      acc.Name = 'Test Account';
      insert acc;
      
      Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        objContact.AccountId = acc.ID;
        objContact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objContact;
        
       Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = acc.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
      
      Service_Request__c sr = new Service_Request__c();
      sr.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Car Parking').getRecordTypeId();
      sr.Customer__c = acc.ID;
      sr.Contact__c = objContact.ID;
      sr.Client_s_Representative_in_Charge__c = 'New Vehicle Registration';
      insert sr;
      
      Car_Parking__c carPark = new Car_Parking__c();
      carPark.Status__c = 'Active';
      carPark.Vehicle_Manufacturer__c = 'Honda';
      carPark.Applicant__c = objContact.ID;
      insert carPark;
      
      carPark = new Car_Parking__c();
      carPark.Status__c = 'Draft';
      carPark.Vehicle_Manufacturer__c = 'Honda';
      carPark.Applicant__c = objContact.ID;
      insert carPark;
      
      Document_Details__c docType = new Document_Details__c();
      docType.Document_Type__c = 'Access Card';
      docType.EXPIRY_DATE__c = Date.Today().AddDays(5);
      docType.Contact__c = objContact.ID;
      insert docType;
      
      test.startTest();
      
      Test.setCurrentPageReference(new PageReference('Page.CarParking')); 
      System.currentPageReference().getParameters().put('Id', sr.ID);
      System.currentPageReference().getParameters().put('isDetail', 'false');
      System.currentPageReference().getParameters().put('RecordType', sr.RecordTypeID);
      
      
      ApexPages.StandardController SRStandardController = new ApexPages.StandardController(sr);
      CarParking_Ctrl caCtrl = new CarParking_Ctrl(SRStandardController);
      caCtrl.AddRowNew();
      caCtrl.AddRowOld();
      caCtrl.GetCar();
      caCtrl.changeEvent();
      caCtrl.RowID = 0;
      caCtrl.DeleteRow();
      caCtrl.undoRequest();
      
      Test.stopTest();
      
      //ca.createAccount(‘TestclassAcc1’);
  
  }
  
  static testMethod void testCarParkingTest_DraftSaveSR()
  {   
      
      Account acc = new Account();
      acc.Name = 'Test Account';
      insert acc;
      
      Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        objContact.AccountId = acc.ID;
        objContact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objContact;
        
       Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = acc.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
      
      Service_Request__c sr = new Service_Request__c();
      sr.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Car Parking').getRecordTypeId();
      sr.Customer__c = acc.ID;
      sr.Contact__c = objContact.ID;
      sr.Client_s_Representative_in_Charge__c = 'New Vehicle Registration';
      insert sr;
      
      Car_Parking__c carPark = new Car_Parking__c();
      carPark.Status__c = 'Active';
      carPark.Vehicle_Manufacturer__c = 'Honda';
      carPark.Applicant__c = objContact.ID;
      insert carPark;
      
      carPark = new Car_Parking__c();
      carPark.Status__c = 'Draft';
      carPark.Vehicle_Manufacturer__c = 'Honda';
      carPark.Applicant__c = objContact.ID;
      insert carPark;
      
      Document_Details__c docType = new Document_Details__c();
      docType.Document_Type__c = 'Access Card';
      docType.EXPIRY_DATE__c = Date.Today().AddDays(5);
      docType.Contact__c = objContact.ID;
      insert docType;
      
      test.startTest();
      
      Test.setCurrentPageReference(new PageReference('Page.CarParking')); 
      System.currentPageReference().getParameters().put('Id', sr.ID);
      System.currentPageReference().getParameters().put('isDetail', 'false');
      System.currentPageReference().getParameters().put('RecordType', sr.RecordTypeID);
      
      
      ApexPages.StandardController SRStandardController = new ApexPages.StandardController(sr);
      CarParking_Ctrl caCtrl = new CarParking_Ctrl(SRStandardController);
      caCtrl.lstofCars.add(new SR_Detail__c(status__c = 'Draft',Is_Card_Required__c = true));
      caCtrl.SaveSR();
      
      caCtrl.objSRFlow.Client_s_Representative_in_Charge__c = 'Lost Card';
      caCtrl.SaveSR();
      
      caCtrl.objSRFlow.Client_s_Representative_in_Charge__c = 'Lost Card';
      caCtrl.SaveSR();
      
      caCtrl.objSRFlow.Client_s_Representative_in_Charge__c = 'Remove Vehicle';
      caCtrl.SaveSR();
      
      caCtrl.objSRFlow.Client_s_Representative_in_Charge__c = 'Replace Vehicle Details';
      caCtrl.objSRFlow.Sys_Requires_Security_Check_Amendments__c = true;
      caCtrl.SaveSR();
      
      caCtrl.EditRequest();
      caCtrl.SubmitRequest();
      caCtrl.CancelRequest();
      caCtrl.OpenDraftSR();
      
      caCtrl.ShowMessage('New Vehicle Registration');
      caCtrl.ShowMessage('Remove Employee Access');
      caCtrl.ShowMessage('Remove Vehicle');
      caCtrl.ShowMessage('Replace Vehicle Details');
      caCtrl.ShowMessage('Lost Card');
      caCtrl.ShowMessage('Replace Card');
      Test.stopTest();
      
      //ca.createAccount(‘TestclassAcc1’);
  
  }
  
  static testMethod void testCarParkingTest_OpenDraftSR()
  {   
      
      Account acc = new Account();
      acc.Name = 'Test Account';
      insert acc;
      
      Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        objContact.AccountId = acc.ID;
        objContact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objContact;
        
       Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = acc.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
      
      Service_Request__c sr = new Service_Request__c();
      sr.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Car Parking').getRecordTypeId();
      sr.Customer__c = acc.ID;
      sr.Contact__c = objContact.ID;
      sr.Client_s_Representative_in_Charge__c = 'New Vehicle Registration';
      insert sr;
      
      Car_Parking__c carPark = new Car_Parking__c();
      carPark.Status__c = 'Active';
      carPark.Vehicle_Manufacturer__c = 'Honda';
      carPark.Applicant__c = objContact.ID;
      insert carPark;
      
      carPark = new Car_Parking__c();
      carPark.Status__c = 'Draft';
      carPark.Vehicle_Manufacturer__c = 'Honda';
      carPark.Applicant__c = objContact.ID;
      insert carPark;
      
      Document_Details__c docType = new Document_Details__c();
      docType.Document_Type__c = 'Access Card';
      docType.EXPIRY_DATE__c = Date.Today().AddDays(5);
      docType.Contact__c = objContact.ID;
      insert docType;
      
      test.startTest();
      
      Test.setCurrentPageReference(new PageReference('Page.CarParking')); 
     
      System.currentPageReference().getParameters().put('isDetail', 'false');
      System.currentPageReference().getParameters().put('RecordType', sr.RecordTypeID);
      
      
      ApexPages.StandardController SRStandardController = new ApexPages.StandardController(sr);
        CarParking_Ctrl caCtrl = new CarParking_Ctrl(SRStandardController);
      caCtrl.OpenDraftSR();
      caCtrl.MarkCardRequired();
      caCtrl.objSRFlow.Is_AOR__c = true;
      caCtrl.MarkCardRequired();
     
      Test.stopTest();
      
      //ca.createAccount(‘TestclassAcc1’);
  
  }
  
  static testMethod void testCarParkingTest_SubmitSR()
  {   
      
      Account acc = new Account();
      acc.Name = 'Test Account';
      insert acc;
      
      Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        objContact.AccountId = acc.ID;
        objContact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objContact;
        
       Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = acc.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
      
      Service_Request__c sr = new Service_Request__c();
      sr.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Car Parking').getRecordTypeId();
      sr.Customer__c = acc.ID;
      sr.Contact__c = objContact.ID;
      sr.Client_s_Representative_in_Charge__c = 'Remove Employee Access';
      insert sr;
      
      SR_Status__c srStatus = new SR_Status__c ();
      srStatus.name  = 'Submitted';
      srStatus.Code__c = 'Submitted';
      insert srStatus;
      
      sr.Submitted_Date__c = system.today();
      sr.Submitted_DateTime__c = system.now();
      sr.Internal_SR_Status__c = srStatus.ID;
      sr.External_SR_Status__c = srStatus.ID;
      sr.finalizeAmendmentFlg__c  = true;
      update sr;
      
     
      
      Document_Details__c docType = new Document_Details__c();
      docType.Document_Type__c = 'Access Card';
      docType.EXPIRY_DATE__c = Date.Today().AddDays(5);
      docType.Contact__c = objContact.ID;
      insert docType;
      
      test.startTest();
      
      Test.setCurrentPageReference(new PageReference('Page.CarParking')); 
      System.currentPageReference().getParameters().put('Id', sr.ID);
      System.currentPageReference().getParameters().put('isDetail', 'false');
      System.currentPageReference().getParameters().put('RecordType', sr.RecordTypeID);
      
       Car_Parking__c carPark = new Car_Parking__c();
      carPark.Status__c = 'Active';
      carPark.Vehicle_Manufacturer__c = 'Honda';
      carPark.Applicant__c = objContact.ID;
      insert carPark;
      
      ApexPages.StandardController SRStandardController = new ApexPages.StandardController(sr);
      CarParking_Ctrl caCtrl = new CarParking_Ctrl(SRStandardController);
     
     
      Test.stopTest();
      
      //ca.createAccount(‘TestclassAcc1’);
  
  }
  
   static testMethod void testCarParkingTest_CloseStep()
  {   
      
      Account acc = new Account();
      acc.Name = 'Test Account';
      insert acc;
      
      Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        objContact.AccountId = acc.ID;
        objContact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objContact;
        
       Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = acc.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
      
      Service_Request__c sr = new Service_Request__c();
      sr.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Car Parking').getRecordTypeId();
      sr.Customer__c = acc.ID;
      sr.Contact__c = objContact.ID;
      sr.Client_s_Representative_in_Charge__c = 'Replace Vehicle Details';
      insert sr;
      
      SR_Status__c srStatus = new SR_Status__c ();
      srStatus.name  = 'Submitted';
      srStatus.Code__c = 'Submitted';
      insert srStatus;
      
      sr.Submitted_Date__c = system.today();
      sr.Submitted_DateTime__c = system.now();
      sr.Internal_SR_Status__c = srStatus.ID;
      sr.External_SR_Status__c = srStatus.ID;
      sr.finalizeAmendmentFlg__c  = true;
      update sr;
      
      test.startTest();
      
     
      
       SR_Detail__c srDetail = new SR_Detail__c();
      srDetail.Status__c = 'Draft';
      srDetail.Vehicle_Manufacturer__c = 'Honda';
      srDetail.Service_Request__c = sr.ID;
      insert srDetail;
      
      Step_Template__c  stpTemp = new Step_Template__c ();
      stpTemp.CODE__C  ='VERIFICATION_OF_APPLICATION';
      stpTemp.Step_RecordType_API_Name__c ='VERIFICATION_OF_APPLICATION';
      insert stpTemp;
      
      SR_Steps__c   stpTemplateCode = new SR_Steps__c   ();
      stpTemplateCode.Step_Template__c= stpTemp.ID;
      insert stpTemplateCode;
      
      Step__c step = new Step__c();
      step.SR__c = sr.ID;
      step.SR_Step__c = stpTemplateCode.ID;
      step.Step_Template__c  = stpTemp.ID;
      insert step;
      
      CarParking_Ctrl.UpdateCarParking(step);
      
       sr.Client_s_Representative_in_Charge__c = 'New Vehicle Registration';
      update sr;
      
      CarParking_Ctrl.UpdateCarParking(step);
      
      srDetail = new SR_Detail__c();
      srDetail.Status__c = 'Draft';
      srDetail.Vehicle_Manufacturer__c = 'Honda';
      srDetail.Service_Request__c = sr.ID;
      insert srDetail;
      
      
      sr.Client_s_Representative_in_Charge__c = 'Renewal';
      update sr;
      
      CarParking_Ctrl.UpdateCarParking(step);
      
      
      sr.Client_s_Representative_in_Charge__c = 'Remove Employee Access';
      update sr;
      
      CarParking_Ctrl.UpdateCarParking(step);
      
      
      
      sr.Client_s_Representative_in_Charge__c = 'Remove Vehicle';
      update sr;
      
      
      CarParking_Ctrl.UpdateCarParking(step);
      
      
      
     
      
      Test.stopTest();
      
      //ca.createAccount(‘TestclassAcc1’);
  
  }
  
   static testMethod void testCarParkingTest_CloseStepRemove()
  { 
  
  
         Account acc = new Account();
      acc.Name = 'Test Account';
      insert acc;
      
      Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        objContact.AccountId = acc.ID;
        objContact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objContact;
        
       Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = acc.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
      
      Service_Request__c sr = new Service_Request__c();
      sr.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Car Parking').getRecordTypeId();
      sr.Customer__c = acc.ID;
      sr.Contact__c = objContact.ID;
      sr.Client_s_Representative_in_Charge__c = 'Remove Vehicle';
      insert sr;
      
      SR_Status__c srStatus = new SR_Status__c ();
      srStatus.name  = 'Submitted';
      srStatus.Code__c = 'Submitted';
      insert srStatus;
      
      sr.Submitted_Date__c = system.today();
      sr.Submitted_DateTime__c = system.now();
      sr.Internal_SR_Status__c = srStatus.ID;
      sr.External_SR_Status__c = srStatus.ID;
      sr.finalizeAmendmentFlg__c  = true;
      update sr;
      
      test.startTest();
      
     
      
       SR_Detail__c srDetail = new SR_Detail__c();
      srDetail.Status__c = 'Draft';
      srDetail.Vehicle_Manufacturer__c = 'Honda';
      srDetail.Service_Request__c = sr.ID;
      insert srDetail;
      
      Step_Template__c  stpTemp = new Step_Template__c ();
      stpTemp.CODE__C  ='VERIFICATION_OF_APPLICATION';
      stpTemp.Step_RecordType_API_Name__c ='VERIFICATION_OF_APPLICATION';
      insert stpTemp;
      
      SR_Steps__c   stpTemplateCode = new SR_Steps__c   ();
      stpTemplateCode.Step_Template__c= stpTemp.ID;
      insert stpTemplateCode;
      
      Step__c step = new Step__c();
      step.SR__c = sr.ID;
      step.SR_Step__c = stpTemplateCode.ID;
      step.Step_Template__c  = stpTemp.ID;
      insert step;
      
      CarParking_Ctrl.UpdateCarParking(step);
      CarParking_Ctrl.AddQuota(sr.ID,acc.ID);
      CarParking_Ctrl.RemoveQuota(sr.ID,acc.ID);
     
      
      sr.Client_s_Representative_in_Charge__c = 'New Vehicle Registration';
      sr.Activity_NOC_Required__c  = true;
      update sr;
      
      CarParking_Ctrl.HoldQuota(step);
      CarParking_Ctrl.ReleaseQuota(step);

  
  }
}