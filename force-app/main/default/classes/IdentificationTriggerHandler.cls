/******************************************************************************************
 *  Identification Trigger Handler
 
 *  Author   : Mudasdir Wani
 *  Date     : 2-March-2020                          
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        	Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0    2-March-2020 	Mudasir       Created                            
*******************************************************************************************/

public with sharing class IdentificationTriggerHandler {
    
    public static void afterInsertOperation(Map<id,Identification__c> newIdentificationMap){
    	List<Account> accountList = new List<Account>();
    	for(Identification__c identficationRec : newIdentificationMap.values()){
    		if(identficationRec.Valid_To__c != Null && identficationRec.Account__c != NULL && identficationRec.Identification_Type__c != NULL && identficationRec.Identification_Type__c =='Index Card'){
    			accountList.add(IdentificationTriggerHandlerHelper.prepareAccountFromIdentifications(identficationRec));
    		}
    	}  
    	IdentificationTriggerHandlerHelper.updateIdentficationsOnAccounts(accountList);
    }
    
    public static void afterUpdateOperation(Map<id,Identification__c> newIdentificationMap,Map<id,Identification__c> oldIdentificationMap){
    	List<Account> accountList = new List<Account>();
    	for(Identification__c identficationRec : newIdentificationMap.values()){
    		if((identficationRec.Valid_To__c != oldIdentificationMap.get(identficationRec.id).Valid_To__c) && identficationRec.Account__c != NULL && identficationRec.Identification_Type__c != NULL && identficationRec.Identification_Type__c =='Index Card'){
    			accountList.add(IdentificationTriggerHandlerHelper.prepareAccountFromIdentifications(identficationRec));
    		}
    	}  
    	IdentificationTriggerHandlerHelper.updateIdentficationsOnAccounts(accountList);
    }
}