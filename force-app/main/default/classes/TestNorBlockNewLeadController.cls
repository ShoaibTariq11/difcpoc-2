@isTest
private class TestNorBlockNewLeadController {

    static testMethod void myUnitTest() {
		
		List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insertNewAccounts[0].Working_from_Co_Working_Space__c = true;
        insertNewAccounts[0].Tax_Registration_Number_TRN__c = '11111111';
        insertNewAccounts[0].Registration_License_No__c = '6054';
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'LTD';
        insertNewAccounts[0].Office__c = 'Gate District';
        insertNewAccounts[0].Emirate__c = 'Dubai';
        insertNewAccounts[0].City__c = 'Dubai'; 
        insertNewAccounts[0].Country__c = 'UAE';
        insertNewAccounts[0].Place_of_Registration__c = 'India';
        insertNewAccounts[0].Building_Name__c ='DIFC';
        insertNewAccounts[0].Reason_for_exemption__c = 'Wholly owned by government';
        insertNewAccounts[0].Street__c = 'DIFC';
        insert insertNewAccounts;
        
		NorBlockNewLeadController.finalJSON(insertNewAccounts[0].Id);        
    }
}