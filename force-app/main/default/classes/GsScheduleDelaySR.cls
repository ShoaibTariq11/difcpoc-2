/*
    Author      :   Ravi
    Description :   This class is used to update the SR Status to Closed
    --------------------------------------------------------------------------------------------------------------------------
	Modification History
 	--------------------------------------------------------------------------------------------------------------------------
	V.No	Date		Updated By    	Description
	--------------------------------------------------------------------------------------------------------------------------             
 	V1.0    25-04-2015	Ravi			Created   
*/
global without sharing class GsScheduleDelaySR implements Schedulable {
	global void execute(SchedulableContext ctx) {
		GsBatchDelaySR obj = new GsBatchDelaySR();
		database.executeBatch(obj, 1);
	}
}