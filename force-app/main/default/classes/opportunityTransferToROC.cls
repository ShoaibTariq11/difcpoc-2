public class opportunityTransferToROC {
    public static List < Account > accountQuery;
    @auraEnabled
    public static String updateOpportunity(id recordId) {
        try {
            Opportunity Opportunity = [select id, AccountId, Name, Lead_Email__c, ROC_Transferable__c, Record_Type_Name__c, Company_Type__c, Transferred_to_ROC__c, Activities__c, Sector_Classification__c, ROC_Transfarrable_for_NFS_Security__c from Opportunity where id =: recordId];
            if (Opportunity.AccountId != Null) {                
            accountQuery = [SELECT OwnerId, Sector_Lead_user__c, Transferred_to_ROC__c, Authorization_Group__c, Company_Type__c, Priority_to_DIFC__c, BD_Sector__c, Activities__c, Sector_Classification__c, ROC_Status__c, Financial_Sector_Activities__c, BD_Financial_Services_Activities__c FROM Account WHERE Id =: Opportunity.AccountId];                
                Account accountRecord = accountQuery[0];
                if ((accountRecord.Authorization_Group__c != null && accountRecord.Company_Type__c != null && accountRecord.Priority_to_DIFC__c != null && accountRecord.BD_Sector__c != null &&
                        accountRecord.ROC_Status__c != null && accountRecord.OwnerId != null && accountRecord.Sector_Lead_user__c != null) || Opportunity.Record_Type_Name__c == 'BD_Financial_Fund') {
                    if (accountRecord.ROC_Status__c == 'Under Formation' || accountRecord.Transferred_to_ROC__c == True) {
                        return 'The account has already been transferred to ROC.';
                    } else if (!(Opportunity.ROC_Transferable__c) && (Opportunity.Record_Type_Name__c != 'BD_Financial_Fund')) {
                        return 'You cannot transfer this opportunity at this particular stage.';
                    } else if (Opportunity.Company_Type__c == 'Financial - related' && (accountRecord.Financial_Sector_Activities__c == null || accountRecord.BD_Financial_Services_Activities__c == null)) {
                        return 'Please provide the Client Type and BD Financial Services Activities on account before transferring it to ROC.';
                    } else if ((Opportunity.Activities__c == Null || Opportunity.Sector_Classification__c == Null) && (Opportunity.Company_Type__c == 'Retail' || Opportunity.Company_Type__c == 'Non - financial')) {
                        return 'Please provide activities transferred to ROC.';
                    } else if (Opportunity.ROC_Transfarrable_for_NFS_Security__c) {  return 'Please approve security check before transfering to ROC.';
                    } else {
                        Boolean isError = false;
                        try {
                            if (Opportunity.Company_Type__c == 'Retail' || Opportunity.Company_Type__c == 'Non - financial') {
                                Account acctObj = new Account();
                                acctObj.Id = Opportunity.AccountId;
                                acctObj.Sector_Classification__c = Opportunity.Sector_Classification__c;
                                update acctObj;
                                //callback = { onSuccess: function(){Console.log ='success'; isError = false}, onFailure: function(){Console.log ='error'; isError = true;} }; 
                                //sforce.apex.execute("CLS_CreateBP","createLicenseActivitiesFromOppty",{opptyID:"{!Opportunity.Id}" ,activities:"{!Opportunity.Activities__c}"},callback); 

                                CLS_CreateBP.createLicenseActivitiesFromOppty(Opportunity.Id, Opportunity.Activities__c);

                            }
                        } catch (exception err) { return err.getMessage();
                        }
                        if (!isError) {
                            try {
                                Opportunity.Transferred_to_ROC__c = true;
                                update Opportunity;
                                 
                                //Transfer to ROC Changes
                                system.debug('!!@##$$#@'+accountRecord.Company_Type__c);
                                if(accountRecord.Company_Type__c != 'Financial - related'){ 
	                                accountRecord.ROC_Status__c = 'Under Formation';
                                }
	                                accountRecord.Transferred_to_ROC_Date__c = system.today();
	                                accountRecord.Transferred_to_ROC__c = true;
                                
                                if (Opportunity.Record_Type_Name__c == 'Retail_Onboarding_Security_Checklist_Gate_Building' || Opportunity.Record_Type_Name__c == 'Retail_Onboarding_Security_Checklist' || Opportunity.Company_Type__c == 'Non - financial') {
                                    accountRecord.Send_Portal_Email__c = true;
                                    accountRecord.Initial_Contact_Email__c = Opportunity.Lead_Email__c;
                                }
                                update accountRecord;
                                return 'The account has been successfully transferred.';

                            } catch (exception e) { e.getMessage();
                            }
                        }
                        //location.reload(true); 
                    }
                } else {
                    return 'Please fill-in the ff. fields before proceeding: Transfer to Roc, Authorization Group, Relationship Manager, Sector Lead, Company Type, Priority to DIFC, BD Sector, and ROC Status.';
                }

            } else {
                return 'Please fill-up the account for this opportunity.';
            }
        } catch (exception e) {
            return 'An error occurred while processing your request. Please contact your administrator, and then try again.';
        }
        return 'The account has been successfully transferred.';
    }
}