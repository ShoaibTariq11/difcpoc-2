@istest
private class srdocTriggerTest{

    static testmethod void myunittest1(){
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@dmcc.com';
        insert objContact;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'New Lease';
        objTemplate.SR_RecordType_API_Name__c = 'Client_Information';
        objTemplate.Active__c = true;
        objTemplate.Available_for_menu__c = true;
        insert objTemplate;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Test Doc Master';
        insert objDocMaster;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.SR_Template__c = objTemplate.Id;
        insert objSR;
      
      
        
      
        SR_Doc__c objSRDoc = new SR_Doc__c();
        objSRDoc.Service_Request__c = objSR.Id;
        objSRDoc.Is_Not_Required__c = false;
        objSRDoc.Group_No__c = 1;
        
        insert objSRDoc;
        
        Test.startTest();
    
    }


}