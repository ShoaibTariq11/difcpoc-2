/*
    Author      :   Shoaib Tariq
    Description :   This class is update SAP Unique Id for Miscellaneous_Services
    --------------------------------------------------------------------------------------------------------------------------
  Modification History
   --------------------------------------------------------------------------------------------------------------------------
  V.No  Date    Updated By      Description
  --------------------------------------------------------------------------------------------------------------------------             
   V1.0    17-02-2020  shoaib      Created
*/
public class PriceItemTriggerHandler {
    
    public static void UpdateSAPUniqueField(List<SR_Price_Item__c> priceItemList){
        
        Set<Id> srIds = new Set<Id>();
        Map<String,SR_Price_Item__c> srItemMap = new Map<String,SR_Price_Item__c>();
        
        for(SR_Price_Item__c thisItem :priceItemList){
            if(String.isNotBlank(thisItem.Transaction_Number__c)){
                  
                   srIds.add(thisItem.ServiceRequest__c);
                   srItemMap.put(thisItem.ServiceRequest__c, thisItem);
            }
        }
        if(!srIds.isEmpty()){
            List<Service_Request__c> thisRequestList = new List<Service_Request__c>(); 
            for(Service_Request__c thisRequest : [SELECT Id,
                                                            RecordTypeId,
                                                            SAP_Unique_No__c,
                                                            SAP_SO_No__c
                                                            FROM Service_Request__c 
                                                            WHERE Id IN:srIds]){
                                                                
                 if(thisRequest.RecordTypeId == Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('Miscellaneous_Services').getRecordTypeId()){
                   
                     thisRequest.SAP_Unique_No__c = String.isBlank(thisRequest.SAP_Unique_No__c)
                                                    && srItemMap.containsKey(thisRequest.Id) 
                                                    ?  srItemMap.get(thisRequest.Id).Transaction_Number__c :thisRequest.SAP_Unique_No__c;
                 }  
                thisRequestList.add(thisRequest);
            }  
            update thisRequestList;
        }  
     }
}