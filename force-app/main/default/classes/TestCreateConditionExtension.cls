/******************************************************************************************
 *  Test Class Name : Test class for CreateConditionExtension class
 *  Date     : 24/SEP/2019
 *  Description : 
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By          Description
 ----------------------------------------------------------------------------------------              
 V1.0   24/SEP/2019      Sai              
*******************************************************************************************/


@isTest
private class TestCreateConditionExtension {
    
    public static Account objAccount;
    public static SR_Template__c objTemplate;
    public static Contact objContact;
    public static Service_Request__c objSR;
    public static Amendment__c objAmd;
    public static TR_Sanctioned_Nationality__c trNationality;
    
    public static void testData(){
        
        
        objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Application_of_Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Menutext__c = 'Application_of_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        insert objContact;
        
        objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Email__c = 'testsr@difc.com';
       // objSR.Contact__c = objContact.Id;
        objSR.SR_Template__c = objTemplate.Id;
        insert objSR;
        
        Service_Request__c objSR2 = new Service_Request__c();
        objSR2.Customer__c = objAccount.Id;
        objSR2.Email__c = 'testsr@difc.com';
       // objSR.Contact__c = objContact.Id;
        objSR2.SR_Template__c = objTemplate.Id;
        insert objSR2;
        
        Lookup__c objlkp = new Lookup__c();
        objlkp.Name = 'United States of America';
        objlkp.Type__c = 'Nationality';
        insert objlkp;
        
        objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = objSR.Id;
        objAmd.Amendment_Type__c = 'Individual';
        objAmd.PO_Box__c = '12345';
        objAmd.First_Name__c = 'Test';
        objAmd.Last_Name__c = 'Test';
        objAmd.sys_create__c = false;
        objAmd.Status__c = 'New';
        objAmd.Nationality__c = objlkp.Id;
        Insert objAmd;
        
        trNationality = new TR_Sanctioned_Nationality__c();
        trNationality.Name = 'India';
        INSERT trNationality;
        
        List<UBO_Data__c> uboList = new List<UBO_Data__c>();
        map<string,string> mapRecordType1 = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where SObjectType = 'UBO_Data__c' and DeveloperName IN ('Individual')]){
            mapRecordType1.put(objRT.DeveloperName,objRT.Id);
        }        
        UBO_Data__c uboData = new UBO_Data__c(
        Nationality_New__c='India',
        Date_of_Cessation__c= system.today(),
        Status__c='Draft',
        Passport_Number_New__c='+971543343234',
        Email_New__c='ss@gmail.com',
        Last_Name_New__c='MOhammed',
        First_Name_New__c = 'Ashik',
        RecordTypeid= mapRecordType1.get('Individual'),
        Type__c='Direct',
        Date_of_Birth__c=system.today().addyears(-20),
        Gender__c='Male',
        Passport_Expiry_Date__c=system.today().addyears(3),
        Passport_Number__c='FGHFJ3242',
        Email__c='ss@gmail.com',
        Place_of_Birth__c='Chennai',
        Title__c='Mr.',
        Date_Of_Becoming_a_UBO__c=system.today(),
        First_Name__c='Poonkuzhali',
        Last_Name__c='Senthamil Selvi',
        Passport_Data_of_Issuance__c=system.today().addyears(-1),
        Entity_Name__c='ABC Corporation',
        Date_Of_Registration__c=system.today().addyears(-1),
        Name_of_Regulator_Exchange_Govt__c='UAE',
        Select_Regulator_Exchange_Govt__c='Dubai',
        Reason_for_exemption__c='Regulated by a recognised financial service provider other than DFSA',
        Entity_Type__c='The individual has the right to appoint or remove the majority of the Directors of the Company',
        Phone_Number__c='+971543343234',
        Apartment_or_Villa_Number__c='3223',
        Emirate_State_Province__c='Dubai',
        Nationality__c='India',
        Building_Name__c='Raga',
        Street_Address__c='business bay',
        City_Town__c='Dubai',
        Country__c='India',
        PO_Box__c='23432432',
        Post_Code__c='234243',
        Service_Request__c = objSR.id
        );

        insert uboData;
        
        status__c stepStatus = new Status__c();//Code__c='VERIFIED',Type__c='End',Name='Verified'
        stepStatus.Type__c='End';
        stepStatus.Name='VERIFIED';
        stepStatus.Code__c='VERIFIED';
        upsert stepStatus;
        
        status__c stepStatus1 = new Status__c();//Code__c='VERIFIED',Type__c='End',Name='Verified'
        stepStatus1.Type__c='Start';
        stepStatus1.Name='Pending Review';
        stepStatus1.Code__c='Pending_Review';
        upsert stepStatus1;
        
        List<step_Template__c> stpTemp = new List<step_Template__c>();
        step_Template__c stpTemp1 = new Step_template__c(Name='Verification of Application',Step_RecordType_API_Name__c='Verification_of_Application',Code__c='VERIFICATION_OF_APPLICATION');
        step_Template__c stpTemp2 = new Step_template__c(Name='Quick Review by Employee',Step_RecordType_API_Name__c='Quick_Review_by_Employee',Code__c='Quick_Review_by_Employee');
        step_Template__c stpTemp3 = new Step_template__c(Name='Review and Approve',Step_RecordType_API_Name__c='Review_and_Approve',Code__c='REVIEW_AND_APPROVE');
        stpTemp.add(stpTemp1);
        stpTemp.add(stpTemp2);
        stpTemp.add(stpTemp3);
        insert stpTemp;
        
        List<step__c> stpList = new List<step__c>();
        step__c verfStep = new step__c();
        verfStep.sr__c = objSR.id;
        verfStep.Step_Template__c = stpTemp[0].id;
        verfStep.status__c = stepStatus1.id;
        stpList.add(verfStep);
        
        step__c verfStep1 = new step__c();
        verfStep1.sr__c = objSR.id;
        verfStep1.Step_Template__c = stpTemp[1].id;
        verfStep1.status__c = stepStatus1.id;
        stpList.add(verfStep1);
        
        step__c verfStep2 = new step__c();
        verfStep2.sr__c = objSR2.id;
        verfStep2.Step_Template__c = stpTemp[1].id;
        verfStep2.status__c = stepStatus.id;
        stpList.add(verfStep2);
        
        step__c verfStep3 = new step__c();
        verfStep3.sr__c = objSR2.id;
        verfStep3.Step_Template__c = stpTemp[2].id;
        verfStep3.status__c = stepStatus.id;
        stpList.add(verfStep3);
        
        insert stpList;
    }
    static testMethod void myUnitTest() {
        
        testData();
        
        Test.startTest();
            CreateConditionExtension.checkCreateMethod('checkTRRecords',objSR.Id,true);
        Test.stopTest();
    }
    
     static testMethod void myUnitTest1() {
        
        testData();
        objAmd = new Amendment__c();
        
        Test.startTest();
            CreateConditionExtension.checkCreateMethod('checkCreateFinancialdocs',objSR.Id,true);
        Test.stopTest();
    }
    
    static testMethod void myUnitTest4() {
        
        testData();
        objAmd.Place_of_Registration__c = 'USA';
        objAmd.Amendment_Type__c = 'Body Corporate';
        UPDATE objAmd;
        Test.startTest();
            CreateConditionExtension.checkCreateMethod('checkCreateFinancialdocs',objSR.Id,true);
        Test.stopTest();
    }
    
     static testMethod void myUnitTest2() {
        
        testData();
        objAmd.Nationality_list__c = 'India';
        UPDATE objAmd;
        Test.startTest();
            CreateConditionExtension.checkCreateMethod('checkCreateBankdocs',objSR.Id,true);
        Test.stopTest();
    }
    
    static testMethod void myUnitTest5() {
        
        testData();
        objAmd = new Amendment__c();
        Test.startTest();
            CreateConditionExtension.checkCreateMethod('checkCreateBankdocs',objSR.Id,true);
        Test.stopTest();
    }
    
    static testMethod void myUnitTest3() {
        
        testData();
        trNationality = new TR_Sanctioned_Nationality__c();
        objAmd = new Amendment__c();
        Test.startTest();
            CreateConditionExtension.checkCreateMethod('checkTRRecords',objSR.Id,false);
        Test.stopTest();
    }
    static testMethod void myUnitTest6() {
        
        testData();
        trNationality = new TR_Sanctioned_Nationality__c();
        objAmd = new Amendment__c();
        Test.startTest();
            CreateConditionExtension.checkCreateMethod('checkAMLNatioality',objSR.Id,false);
            CreateConditionExtension.checkCreateMethod('checkIsAMLorSecRejected',objSR.Id,false);
            CreateConditionExtension.checkCreateMethod('isAMLCreated',objSR.Id,false);
            CreateConditionExtension.checkCreateMethod('checkReviewandApprove',objSR.Id,false);
            CreateConditionExtension.checkCreateMethod('checkUBONationality',objSR.Id,false);
            CreateConditionExtension.checkCreateMethod('UBOReviewandApprove',objSR.Id,false);
            CreateConditionExtension.checkCreateMethod('checkAMLCreate',objSR.Id,false);
            CreateConditionExtension.SRAutoApprove(objSR.Id);
            CreateConditionExtension.dummytest();
        Test.stopTest();
    }
    
}