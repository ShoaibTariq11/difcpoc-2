/*************************************************************************************************
 *  Name        : CLS_BatchPaymentReconcile
 *  Author      : Kaavya Raghuram
 *  Company     : NSI JLT
 *  Date        : 2014-10-01     
 *  Purpose     : This class is to call the Payment Query API for the pending receipt records   
 ---------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.1    02-Jun-2015 Kaavya        Updated for new query API         
 V1.2    06-10-2016  Sravan        Added code to decrypt MerchantID Tkt # 3392 
**************************************************************************************************/

global class CLS_BatchPaymentReconcile implements Database.Batchable<sObject>,Database.AllowsCallouts{

   global Database.QueryLocator start(Database.BatchableContext info){ 
   
        //Get all receipt records which do not have a success or failure status updated from Payment Gateway 
        string cpgStatus = ''; 
        string pgStatus = 'Pending'; 
        String rectype='Card';    
        string query = 'select id,name,Amount__c,Bank_Ref_No__c,Customer__c,Payment_Gateway_Ref_No__c,Payment_Status__c,Processing_Charge__c,Payment_Inquiry_Done__c from Receipt__c where Receipt_Type__c=:rectype AND ((Payment_Inquiry_Done__c=false AND Payment_Status__c=:cpgStatus) OR Payment_Status__c=:pgStatus)';
        return Database.getQueryLocator(query);
   }     
   global void execute(Database.BatchableContext info, List<Receipt__c> scope){
        system.debug('IIInside EEExecute');
        system.debug('SSSScope===>'+scope);
        try{
        for(Receipt__c R: scope){
            system.debug('R===>'+R.Id);  
            
            /**********Start of V1.1 ***************************************/
            WS_PaymentQuery.InvokeEcomWebServicesPort Query= new WS_PaymentQuery.InvokeEcomWebServicesPort();
            Query.timeout_x = 100000;
            
            //Encrypting each parameter to be passed in the webservice
            String txntype=EncryptStr('01'); // “01" indicates the Sale transaction type
            String orderNumber=EncryptStr(R.name);
            String atrn;
            if(R.Payment_Gateway_Ref_No__c!=null)
                atrn=EncryptStr(R.Payment_Gateway_Ref_No__c);
            else
                atrn=EncryptStr('');
            
            // V1.2
            
            string decryptedMerchantID =  test.isrunningTest()==false ? PasswordCryptoGraphy.DecryptPassword(Label.Merchant_ID) :'123456'; 
            
            //Invoking the Query API of Payment Gateway    
          //  String EncResp= Query.InvokeQueryWS(Label.Merchant_ID,orderNumber,atrn,txntype); //Commented for V1.2
            String EncResp= Query.InvokeQueryWS(decryptedMerchantID,orderNumber,atrn,txntype); // Added for V1.2
          
            system.debug('Encrypted Response==>'+EncResp);
            
            //Decrypting the response from the Query API        
            String DecResp=DecryptStr(EncResp);  
             
            if(DecResp!=null && DecResp!=''){
                DecResp= DecResp.replace('|','--SEPARATOR--');        
                system.debug('DSSSSS==>'+DecResp);
                
                /****Get response parameters from the decrypted message***/
                List<String> RespParams=DecResp.split('--SEPARATOR--');
                system.debug('Response Params=====>'+ RespParams); 
                
                //Update the values from the response parameter onto the receipt record
                R.Payment_Gateway_Ref_No__c=RespParams[2];        
                R.Payment_Status__c=RespParams[5];            
                R.Card_Type__c=RespParams[7];
                R.PG_Error_Code__c=RespParams[8];
                R.PG_Error_Message__c=RespParams[9];
            }
            R.Payment_Inquiry_Done__c=true;
            
            /****** Commented for V1.1 ******************
            *****************************************************************************************************
                // Create a pipe separated string of all fields to be sent in the request        
                String message = R.name + '||'+Label.ForceSite_Domain+'/VF_PaymentStatusUpdate?mode=query|01|';
                system.debug('RRReqqMessage===>'+message);
                
                //****Encrypt the request message****
                
                //Get the encryption key from custom label
                String keyval= Label.Encryption_Key;
                Blob cryptoKey = EncodingUtil.base64Decode(keyval);
                
                //Get the initialization vector from custom setting
                Blob iv = Blob.valueof('0123456789abcdef');
                
                //Convert the request string to Blob
                Blob data = Blob.valueOf(message);  
                
                //Encrypt the data using Salesforce Crypto class          
                Blob encryptedData = Crypto.encrypt('AES256', cryptoKey,iv, data);            
                system.debug('EEEE=>'+encryptedData) ;
                
                // Convert the encrypted data to string
                String encryptedMessage  = EncodingUtil.base64Encode(encryptedData); 
                System.debug('EEEESSSS=>'+encryptedMessage);
                
                //Append the encrypted string to the MerchantID
                String reqstr= Label.Merchant_ID + '|' + encryptedMessage + '|';
                system.debug('RRRRR===>'+reqstr);
                  
                //Creating a HTTP request to Query API with the request parameters       
                Http http = new Http();
                HttpRequest req = new HttpRequest(); 
                HttpResponse res = new HttpResponse();            
                
                //Setting the URL for the request
                req.setEndpoint(Label.Reconciliation_URL);
                req.setBody('requestParameter='+reqstr);
                req.setTimeout(20000); //milliseconds
                req.setMethod('POST');
                
                    try{
                        if(!System.Test.isRunningTest()){ //to prevent callouts while running test class
                            //Sending the HTTP request
                            res = http.send(req);
                            system.debug('***Body was:' + res.getBody());
                        }
                         
                        
                    } 
                    catch(System.CalloutException e) {
                        System.debug('Callout error: '+ e);
                       
                    }
                **************************************************************************************************    
                ****** End of Commented for V1.1 ******************/

            }
            update scope;
       }
        catch(Exception ex){
            system.debug('Exception is : '+ex.getMessage());
            Log__c objLog = new Log__c();
            objLog.Description__c ='Line No===>'+ex.getLineNumber()+'---Message==>'+ex.getMessage();
            objLog.Type__c = 'Webservice Callout for Payment Query API';
            insert objLog;        
        }
   }    
   
   global void finish(Database.BatchableContext info){     
   } 
   
   global String EncryptStr(String str){
           //****Encrypt the request message****
            
            //Get the encryption key from custom label
            String keyval= Label.Encryption_Key;
            Blob cryptoKey = EncodingUtil.base64Decode(keyval);
            
            //Get the initialization vector from custom setting
            Blob iv = Blob.valueof('0123456789abcdef');
            
            //Convert the request string to Blob
            Blob data = Blob.valueOf(str);  
            
            //Encrypt the data using Salesforce Crypto class          
            Blob encryptedData = Crypto.encrypt('AES256', cryptoKey,iv, data);            
            system.debug('EEEE=>'+encryptedData) ;
            
            // Convert the encrypted data to string
            String encryptedMessage  = EncodingUtil.base64Encode(encryptedData); 
            System.debug('EEEESSSS=>'+encryptedMessage);  
            
            return encryptedMessage;  
   }
    global String DecryptStr(String EncStr){
        /*****Decrypt the encrypted response message****/
        //Convert the encrypted string to Blob
        Blob encryptedData= EncodingUtil.base64Decode(EncStr); 
        
        //Get the encryption key from custom setting
        String keyval= Label.Encryption_Key;
        Blob cryptoKey = EncodingUtil.base64Decode(keyval);
        
        //Get the initialization vector from custom setting
        Blob iv = Blob.valueof('0123456789abcdef');
        
        //Decrypt the data using Salesforce Crypto class
        Blob decryptedData = Crypto.decrypt('AES256', cryptoKey,iv,encryptedData);
        system.debug('DDDDD==>'+decryptedData);
        
        // Convert the decrypted data to string
        String decryptedDataString = decryptedData.toString();
        system.debug('DSSSSS==>'+decryptedDataString);
        
        return decryptedDataString;
    }
   
}