@isTest
private class Test_AmendmentTrg{

    static TestMethod void testBC(){
        
        map<string,string> mapRecordType = new map<string,string>();
        
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Lease_Registration','Lease_Renewal_Request','Permit_to_Access_Permanent','Permit_to_Access','Permit_to_Work','Permit_to_add_or_remove_equipment',
                                'Body_Corporate_Tenant','Business_Centre_Office','Company_Buyer','Company_Seller','DIFCI_Tenant','Data_Center_Requestor','Equipment','Individual_Tenant','Landlord','Tool','Lease_Application_Request')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        insert objAccount;
        
        Account objAccount2 = new Account();
        objAccount2.Name = 'Test Customer 1';
        objAccount2.E_mail__c = 'test@test.com';
        objAccount2.BP_No__c = '001289';
        objAccount2.BP_No__c = '12389';
        objAccount2.RORP_License_No__c = 'RORP-'+'17856';
        objAccount2.ROC_Status__c = 'Under Formation';
        objAccount2.Company_Type__c = 'Financial - related';
        objAccount2.Sector_Classification__c = 'Authorised Market Institution';
        objAccount2.Issuing_Authority__c = 'ABCD';
        
        insert objAccount2;
        
        Account objAccount3 = new Account();
        objAccount3.Name = 'Test Customer 3';
        objAccount3.E_mail__c = 'test@test.com';
        objAccount3.BP_No__c = '001589';
        objAccount3.BP_No__c = '12589';
        objAccount3.RORP_License_No__c = '13456';
        objAccount3.ROC_Status__c = 'Under Formation';
        objAccount3.Company_Type__c = 'Financial - related';
        objAccount3.Sector_Classification__c = 'Authorised Market Institution';
        objAccount3.Issuing_Authority__c = 'DIFC - Registered';
        
        insert objAccount3;
        
        License__c testLicense = Test_CC_FitOutCustomCode_DataFactory.getTestLicense(objAccount2.Id);

		testLicense.name = 'RORP-'+'17856';
			
		insert testLicense;
			
		objAccount2.Active_License__c = testLicense.Id;
		
		update objAccount2;
		
		License__c testLicense2 = Test_CC_FitOutCustomCode_DataFactory.getTestLicense(objAccount2.Id);

		testLicense2.name = '13456';
			
		insert testLicense2;
			
		objAccount3.Active_License__c = testLicense2.Id;
		
		update objAccount3;
        
        Building__c build = new Building__c();
        build.Name = 'Gate Building';
        build.Company_Code__c = '5000';
        insert build;
        
        list<Unit__c> lstUnit = new list<Unit__c>();
        
        Unit__c units = new Unit__c();
        units.Name='101';
        units.Unit_Usage_Type__c = 'DIFC Educational Lease Space';
        units.Building__c = build.id;
        units.Building_Name__c = 'Gate Building';
        units.SAP_No_of_Desks__c = 45;
        lstUnit.add(units);
        
        Unit__c unit2 = new Unit__c();
        unit2.Name='102';
        unit2.Unit_Usage_Type__c = 'DIFC Educational Lease Space';
        unit2.Building__c = build.id;
        unit2.Building_Name__c = 'Gate Building';
        unit2.SAP_No_of_Desks__c = 45;
        lstUnit.add(unit2);
        
        Unit__c unit3 = new Unit__c();
        unit3.Name='102';
        unit3.Unit_Usage_Type__c = 'Commercial';
        unit3.Building__c = build.id;
        unit3.Building_Name__c = 'Gate Building';
        unit3.SAP_No_of_Desks__c = 45;
                
        lstUnit.add(unit3);
        
        insert lstUnit;
        
        Lease__c lease = new Lease__c(Account__c=objAccount.id,Type__c='Leased',Status__c='Active',Lease_Types__c='Data Centre Lease');
        insert lease;
        
        Lease__c lease2 = new Lease__c(Account__c=objAccount2.id,Type__c='Leased',Status__c='Active',Lease_Types__c='Data Centre Lease');
        insert lease2;
        
        Lease__c lease3 = new Lease__c(Account__c=objAccount3.id,Type__c='Leased',Status__c='Active',Lease_Types__c='Data Centre Lease');
        insert lease3;
        
        Tenancy__c ten = new Tenancy__c(Unit__c=units.id,Lease__c=lease.id);
        
        SR_Status__c srStatus = new SR_Status__c(Name = 'Draft',Code__c = 'DRAFT');
        
        insert srStatus;
        
        list<Service_Request__c> lstSR = new list<Service_Request__c>();
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = mapRecordType.get('Lease_Application_Request');
        lstSR.add(objSR);
        
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.RecordTypeId = mapRecordType.get('Permit_to_add_or_remove_equipment');
        lstSR.add(objSR1);
        
        Service_Request__c objSR2 = new Service_Request__c();
        objSR2.Customer__c = objAccount.Id;
        objSR2.RecordTypeId = mapRecordType.get('Lease_Registration');
        lstSR.add(objSR2);
        
        Service_Request__c objSR3 = new Service_Request__c();
        objSR3.Customer__c = objAccount2.Id;
        objSR3.RecordTypeId = mapRecordType.get('Lease_Application_Request');
        lstSR.add(objSR3);
        
        Service_Request__c objSR4 = new Service_Request__c();
        objSR4.Customer__c = objAccount3.Id;
        objSR4.RecordTypeId = mapRecordType.get('Lease_Application_Request');
        lstSR.add(objSR4);
        
        Service_Request__c objSR6 = new Service_Request__c();
        objSR6.Customer__c = objAccount3.Id;
        objSR6.RecordTypeId = mapRecordType.get('Lease_Application_Request');
        objSR6.Type_of_Lease__c = 'Sub-lease';
        objSR6.Unit__c = unit3.Id;
        objSR6.Building__c = build.id;
        lstSR.add(objSR6);
        
        Service_Request__c objSR5 = new Service_Request__c();
        objSR5.Customer__c = objAccount3.Id;
        objSR5.RecordTypeId = mapRecordType.get('Lease_Application_Request');
        objSR5.Internal_SR_Status__c = srStatus.Id;
        lstSR.add(objSR5);
        
        insert lstSR;
        
        Test.startTest();
        
        testAmendmentBusinessCentre(mapRecordType.get('Business_Centre_Office'),units.id,objSR.id);
        
        testAmendmentBusinessCentre2(mapRecordType.get('Business_Centre_Office'),unit2.id,objSR.id);
        
        testAmendmentDataCentre(mapRecordType.get('Data_Center_Requestor'),objSR1.id);
        
        testAmendmentIndividualTenant(mapRecordType.get('Individual_Tenant'),unit2.id,objSR2.id);
        
        testAmendmentLandlord(mapRecordType.get('Landlord'),unit2.id,objSR3.id);

        testAmendmentBodyCorp(mapRecordType.get('Body_Corporate_Tenant'),unit2.id,objSR4.id);
        
        testAmendmentBodyCorp2(mapRecordType.get('Body_Corporate_Tenant'),unit2.id,objSR2.id);
        testAmendmentBodyCorp2(mapRecordType.get('Body_Corporate_Tenant'),unit2.id,objSR5.id);
        testAmendmentBodyCorp2(mapRecordType.get('Body_Corporate_Tenant'),unit2.id,objSR5.id);
        testAmendmentBodyCorp2(mapRecordType.get('Body_Corporate_Tenant'),unit3.id,objSR6.id);
        
        testAmendmentBodyCorp2(mapRecordType.get('Company_Seller'),unit2.id,objSR2.id);
		
        Test.stopTest();    
        
    }
    
    @future
    private static void testAmendmentBusinessCentre(String recordTypeId, String unitId, String srId){
    	
    	Amendment__c testAmendment = new Amendment__c (RecordTypeId=recordTypeId,Lease_Unit__c=unitId,ServiceRequest__c=srId);
    	
    	insert testAmendment;
    	delete testAmendment;
    }
    
    @future
    private static void testAmendmentDataCentre(String recordTypeId, String srId){
    	insert new Amendment__c (RecordTypeId=recordTypeId,ServiceRequest__c=srId);
    }
    
    @future
    private static void testAmendmentBusinessCentre2(String recordTypeId, String unitId, String srId){
    	insert new Amendment__c (RecordTypeId=recordTypeId,ServiceRequest__c=srId,Lease_Unit__c=unitId);
    }
    
    @future
    private static void testAmendmentBodyCorp(String recordTypeId, String unitId, String srId){
    	insert new Amendment__c (RecordTypeId=recordTypeId,Company_Name__c='Test',Name_of_Declaration_Signatory__c='Test',CL_Certificate_No__c='13456',IssuingAuthority__c='DIFC - Registered',Issuing_Authority_Other__c='ABCD',ServiceRequest__c=srId,Unit__c=unitId);
    }
    
    @future
    private static void testAmendmentBodyCorp2(String recordTypeId, String unitId, String srId){
    	
    	try {
    		
    		Amendment__c testAmendment = new Amendment__c (RecordTypeId=recordTypeId,Company_Name__c='Test',Name_of_Declaration_Signatory__c='Test',CL_Certificate_No__c='RORP-17856',IssuingAuthority__c='Others',Issuing_Authority_Other__c='ABCD',ServiceRequest__c=srId,Unit__c=unitId);
    		
    		insert testAmendment;
    		
    		testAmendment.Company_Name__c = 'Test TWo';
    		testAmendment.IssuingAuthority__c = 'DIFC - Under Formation';
    		
    		update testAmendment;
    		
    		testAmendment.IssuingAuthority__c = 'Others';
    		
    		update testAmendment;
    	
    	} catch (Exception e){
        	
        }
    }
    
    @future
    private static void testAmendmentIndividualTenant(String recordTypeId, String unitId, String srId){
    	
    	Amendment__c testAmendment = new Amendment__c (RecordTypeId=recordTypeId,Given_Name__c='Test',Family_Name__c='Test',First_Name__c='Test',Passport_No__c='test1233',Nationality_list__c='Pakistan',ServiceRequest__c=srId,Unit__c=unitId);
    	
    	testAmendment.First_Name__c = 'Testing FName';
    	testAmendment.Passport_No_Occupant__c = 'Test';
    	testAmendment.Nationality_Occupant__c = 'United Arab Emirates';
    	
    	insert testAmendment;
    	
    	testAmendment.Given_Name__c = 'Testing Two';
    	
    	update testAmendment;
    	
    	testAmendment.First_Name__c = 'Testing FName Two';
    	
    	update testAmendment;
    }
    
    @future
    private static void testAmendmentLandlord(String recordTypeId, String unitId, String srId){
    	insert new Amendment__c (RecordTypeId=recordTypeId,Type_of_Landlord__c='Individual',Given_Name__c='Test',Family_Name__c='Test',First_Name__c='Test',Passport_No__c='test123300',Nationality_list__c='Pakistan',ServiceRequest__c=srId,Unit__c=unitId);
    }

}