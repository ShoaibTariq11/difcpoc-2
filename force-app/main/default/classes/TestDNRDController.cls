@isTest(seeAllData=false)
public class TestDNRDController {
    
    static testMethod void myUnitTest()
    {
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        RecordType rt = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Service_Request__c' AND Name = 'Index Card Other Services'];

        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        lstSRStatus.add(new SR_Status__c(Name='Submitted',Code__c='SUBMITTED'));
        insert lstSRStatus;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = rt.id;
        
        //objSR.RecordType.DeveloperName = 'DIFC_Sponsorship_Visa_New';
        //objSR.Send_SMS_To_Mobile__c = '+97152123456';
        objSR.Email__c = 'testclass@difc.ae.test';
        objSR.Type_of_Request__c = 'Applicant Outside UAE';
        objSR.Port_of_Entry__c = 'Dubai International Airport';
        objSR.Title__c = 'Mr.';
        objSR.First_Name__c = 'India';
        objSR.Last_Name__c = 'Hyderabad';
        objSR.Middle_Name__c = 'Andhra';
        objSR.Nationality_list__c = 'India';
        objSR.Previous_Nationality__c = 'India';
        objSR.Qualification__c = 'B.A. LAW';
        objSR.Gender__c = 'Male';
        objSR.Date_of_Birth__c = Date.newInstance(1989,1,24);
        objSR.Place_of_Birth__c = 'Hyderabad';
        objSR.Country_of_Birth__c = 'India';
        objSR.Passport_Number__c = 'ABC12345';
        objSR.Passport_Type__c = 'Normal';
        objSR.Passport_Place_of_Issue__c = 'Hyderabad';
        objSR.Passport_Country_of_Issue__c = 'India';
        objSR.Passport_Date_of_Issue__c = system.today().addYears(-1);
        objSR.Passport_Date_of_Expiry__c = system.today().addYears(2);
    
        objSR.Email_Address__c = 'applicant@newdifc.test';
    
        objSR.Statement_of_Undertaking__c = true;
        objSR.Internal_SR_Status__c = lstSRStatus[0].Id;
        objSR.External_SR_Status__c = lstSRStatus[0].Id;
        insert objSR;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        objStep.DNRD_Receipt_No__c = '009988776655';
        //objStep.SR_Record_Type__c = ;
        //objStep.Step_Name__c = 'Front Desk Review';
        insert objStep;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objStep);
        DNRDController dnrd1 = new DNRDController(sc);
        
        PageReference pageRef = Page.DNRDFieldUpdate; // Add your VF page Name here
        pageRef.getParameters().put('id', String.valueOf(objStep.Id));
        Test.setCurrentPage(pageRef);        
        DNRDController.dnrd();
    }

}