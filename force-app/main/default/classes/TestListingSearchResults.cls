/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestListingSearchResults {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'Test Building';
        objBuilding.Building_No__c = '00000001';
        insert objBuilding;
        
        Unit__c objUnit = new Unit__c();
        objUnit.Building__c = objBuilding.Id;
        objUnit.SAP_Unit_No__c = '0000000000001';
        insert objUnit;
        
        list<Listing__c> lstListings = new list<Listing__c>();
        
        Listing__c objList = new Listing__c();
        objList.Unit__c = objUnit.Id;
        objList.Status__c = 'Active';
        objList.Start_Date__c = system.today();
        objList.End_Date__c = system.today().addDays(30);
        objList.Featured__c = true;
        objList.Price__c = 1200;
        objList.Sq_Feet__c = 1200;
        objList.Listing_Type__c = 'Rent';
        lstListings.add(objList);
        
        objList = new Listing__c();
        objList.Unit__c = objUnit.Id;
        objList.Status__c = 'Active';
        objList.Start_Date__c = system.today();
        objList.End_Date__c = system.today().addDays(30);
        objList.Featured__c = false;
        objList.Price__c = 1200;
        objList.Sq_Feet__c = 1200;
        objList.Listing_Type__c = 'Sale';
        objList.Leasing_Type__c = 'Long Term';
        objList.No_of_Bedrooms__c = '2';
        objList.Listing_Title__c = 'Test Listing Title';
        lstListings.add(objList);
        
        insert lstListings;
        
        Service_Request__c objSR = new Service_Request__c();
        insert objSR;
        
        list<SR_Doc__c> lstSRDocs = new list<SR_Doc__c>();
        
        SR_Doc__c objSRDoc = new SR_Doc__c();
        objSRDoc.Service_Request__c = objSR.Id;
        objSRDoc.Name = 'Photograph 1';
        objSRDoc.Doc_ID__c = objSR.Id;
        objSRDoc.Listing__c = lstListings[0].Id;
        objSRDoc.Listing_Doc_Status__c = 'Active';
        lstSRDocs.add(objSRDoc);
        
        objSRDoc = new SR_Doc__c();
        objSRDoc.Service_Request__c = objSR.Id;
        objSRDoc.Name = 'Photograph 1';
        objSRDoc.Doc_ID__c = objSR.Id;
        objSRDoc.Listing__c = lstListings[1].Id;
        objSRDoc.Listing_Doc_Status__c = 'Active';
        lstSRDocs.add(objSRDoc);
        
        Apexpages.currentPage().getParameters().put('minPrice','1000');
        Apexpages.currentPage().getParameters().put('maxPrice','1234567');
        Apexpages.currentPage().getParameters().put('minSize','0');
        Apexpages.currentPage().getParameters().put('maxSize','100000');
        Apexpages.currentPage().getParameters().put('rentOrSale','');
        Apexpages.currentPage().getParameters().put('buildingName','Test Building');
        Apexpages.currentPage().getParameters().put('propertyType','Sale');
        Apexpages.currentPage().getParameters().put('leasingType','Long Term');
        Apexpages.currentPage().getParameters().put('bedroomsNum','2');
        Apexpages.currentPage().getParameters().put('fnbNonFnb','Fnb');
        Apexpages.currentPage().getParameters().put('keyword','Test');
        Apexpages.currentPage().getParameters().put('filter','price-low');
        Apexpages.currentPage().getParameters().put('cageNo','123');
        Apexpages.currentPage().getParameters().put('buildingNames','Test Building,Test Building 2,Test Building 3');
        
        
        ListingSearchResults objListingSearchResults = new ListingSearchResults();
        
        objListingSearchResults.name = 'Test Class';
        objListingSearchResults.email = 'Test';
        objListingSearchResults.doVerify();
        
        objListingSearchResults.name = 'Test Class';
        objListingSearchResults.email = 'test123@gmail.com';
        objListingSearchResults.doVerify();
        
    }
}