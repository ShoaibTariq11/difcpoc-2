/*
    Author      : Leeba
    Date        : 29-March-2020
    Description : Test class for CC_CreateCommPermission
    --------------------------------------------------------------------------------------
*/

@isTest
public class CC_CreateCommPermissionTest{

    public static testMethod void CC_CreateCommPermission() {
    
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
      
        
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
       insert objsrTemp;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        objHexaSR.HexaBPM__Email__c = 'test@test.com';
        objHexaSR.Date_of_Issue__c = system.today();
        objHexaSR.Date_of_Expiry__c = system.today()+100;
        objHexaSR.Duration_days__c = 5;
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
        
       
        Test.startTest();
         HexaBPM__Step__c step = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Date_of_Issue__c, HexaBPM__SR__r.Duration_days__c,
                                  HexaBPM__SR__r.Date_of_Expiry__c                           
                                  from HexaBPM__Step__c where Id=:objHexastep.Id];
        CC_CreateCommPermission CC_CreateCommPermissionObj = new CC_CreateCommPermission();
        CC_CreateCommPermissionObj.EvaluateCustomCode(objHexaSR,step); 
        Test.stopTest();
       
    }
    
     public static testMethod void CC_CreateCommPermission1() {
    
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
      
        
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
       insert objsrTemp;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        objHexaSR.HexaBPM__Email__c = 'test@test.com';
        objHexaSR.Date_of_Issue__c = system.today();
        objHexaSR.Date_of_Expiry__c = system.today()+100;
        objHexaSR.Duration_days__c = null;
        objHexaSR.Duration__c = 'Event - Up to 3 days';
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
        
       
        Test.startTest();
         HexaBPM__Step__c step = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Date_of_Issue__c, HexaBPM__SR__r.Duration__c,HexaBPM__SR__r.Duration_days__c,
                                  HexaBPM__SR__r.Date_of_Expiry__c                           
                                  from HexaBPM__Step__c where Id=:objHexastep.Id];
        CC_CreateCommPermission CC_CreateCommPermissionObj = new CC_CreateCommPermission();
        CC_CreateCommPermissionObj.EvaluateCustomCode(objHexaSR,step); 
        Test.stopTest();
       
    }
    
     public static testMethod void CC_CreateCommPermission2() {
    
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
      
        
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
       insert objsrTemp;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        objHexaSR.HexaBPM__Email__c = 'test@test.com';
        objHexaSR.Date_of_Issue__c = system.today();
        objHexaSR.Date_of_Expiry__c = system.today()+100;
        objHexaSR.Duration_days__c = null;
        objHexaSR.Duration__c = 'Event - Up to 1 week';
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
        
       
        Test.startTest();
         HexaBPM__Step__c step = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Date_of_Issue__c, HexaBPM__SR__r.Duration__c, HexaBPM__SR__r.Duration_days__c,
                                  HexaBPM__SR__r.Date_of_Expiry__c                           
                                  from HexaBPM__Step__c where Id=:objHexastep.Id];
        CC_CreateCommPermission CC_CreateCommPermissionObj = new CC_CreateCommPermission();
        CC_CreateCommPermissionObj.EvaluateCustomCode(objHexaSR,step); 
        Test.stopTest();
       
    }
    
     public static testMethod void CC_CreateCommPermission3() {
    
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
      
        
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
       insert objsrTemp;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        objHexaSR.HexaBPM__Email__c = 'test@test.com';
        objHexaSR.Date_of_Issue__c = system.today();
        objHexaSR.Date_of_Expiry__c = system.today()+100;
        objHexaSR.Duration_days__c = null;
        objHexaSR.Duration__c = 'Event - Up to 1 month';
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
        
       
        Test.startTest();
         HexaBPM__Step__c step = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Date_of_Issue__c, HexaBPM__SR__r.Duration__c,HexaBPM__SR__r.Duration_days__c,
                                  HexaBPM__SR__r.Date_of_Expiry__c                           
                                  from HexaBPM__Step__c where Id=:objHexastep.Id];
        CC_CreateCommPermission CC_CreateCommPermissionObj = new CC_CreateCommPermission();
        CC_CreateCommPermissionObj.EvaluateCustomCode(objHexaSR,step); 
        Test.stopTest();
       
    }
    
     public static testMethod void CC_CreateCommPermission4() {
    
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
      
        
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
       insert objsrTemp;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        objHexaSR.HexaBPM__Email__c = 'test@test.com';
        objHexaSR.Date_of_Issue__c = system.today();
        objHexaSR.Date_of_Expiry__c = system.today()+100;
        objHexaSR.Duration_days__c = null;
        objHexaSR.Duration__c = 'Event - Up to 3 months';
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
        
       
        Test.startTest();
         HexaBPM__Step__c step = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Date_of_Issue__c, HexaBPM__SR__r.Duration__c,HexaBPM__SR__r.Duration_days__c,
                                  HexaBPM__SR__r.Date_of_Expiry__c                           
                                  from HexaBPM__Step__c where Id=:objHexastep.Id];
        CC_CreateCommPermission CC_CreateCommPermissionObj = new CC_CreateCommPermission();
        CC_CreateCommPermissionObj.EvaluateCustomCode(objHexaSR,step); 
        Test.stopTest();
       
    }
 }