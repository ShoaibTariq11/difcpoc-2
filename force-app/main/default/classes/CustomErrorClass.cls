/*
CreatedBy : Danish Farooq
CreatedOn : 12-Feb-2018
Purpose : to show custom error message
TestClass : GetCustomMessage_Test
*/
public class CustomErrorClass{

    
    public static string GetCustomMessage(string aErrorMessage){
        
        if(aErrorMessage.Contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
            
            string changeMsg = aErrorMessage.split('FIELD_CUSTOM_VALIDATION_EXCEPTION,')[1];
            integer index = changeMSG.indexOf(':'); //: [Custom_Field__c] to avoid this statement from end
            if(index != -1){
                return changeMsg.subString(0,index);
            }
            return changeMsg;
        }
           
        
        return aErrorMessage;
    }

}