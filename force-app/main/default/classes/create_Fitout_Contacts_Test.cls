@isTest
private class create_Fitout_Contacts_Test {
	 static testMethod void myUnitTest() {
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Request_Contractor_Access','Fit_Out_Service_Request','Fit_Out_Induction_Request','Permit_to_Work','Permit_to_add_or_remove_equipment',
                                'Fit_Out_Contact','Fit_Out_Units','Revision_of_Fit_Out_Request','Update_Contact_Details','Data_Center_Requestor','NOC_Bloomberg','Individual_Tenant','Landlord','Tool','Lease_Application_Request')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        Status__c objStatusApproved = new Status__c();
        objStatusApproved.Name = 'Approved';
        objStatusApproved.Code__c = 'APPROVED';
        insert objStatusApproved;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        SR_Status__c objSRStatus;
           
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Submitted';
        objSRStatus.Code__c = 'Submitted';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Draft';
        objSRStatus.Code__c = 'DRAFT';
        lstSRStatus.add(objSRStatus);  
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Approved';
        objSRStatus.Code__c = 'Approved';
		lstSRStatus.add(objSRStatus);
        
		objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Fit to Occupy Certificate Issued';
        objSRStatus.Code__c = 'Fit_to_Occupy_Certificate_Issued';        
        
        lstSRStatus.add(objSRStatus);
             
        insert lstSRStatus;
        
        Business_Hours__c bh= new Business_Hours__c();
        BH.name='Fit-Out';
        BH.Business_Hours_Id__c='01m20000000BOf6';
        insert BH; 
        
        SR_Template__c testSrTemplate = new SR_Template__c();
    
        testSrTemplate.Name = 'Fit - Out Service Request';
        testSrTemplate.Menutext__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_RecordType_API_Name__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_Group__c = 'Fit-Out & Events';
        testSrTemplate.Active__c = true;
        
        insert testSrTemplate;
                
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
         

        Service_Request__c SR111= new Service_Request__c();
        SR111.RecordTypeId = mapRecordType.get('Request_Contractor_Access');
        SR111.Customer__c = objAccount.Id;
        SR111.external_SR_Status__c = lstSRStatus[2].Id;
        //SR.Service_Category__c = 'New'; 
        SR111.email__c = 'mudasir.w@difc.ae';      
        insert SR111;
        
        Service_Request__c SR1= new Service_Request__c();
        SR1.RecordTypeId = mapRecordType.get('Fit_Out_Induction_Request');
        SR1.Customer__c = objAccount.Id;
        SR1.email__c = 'mudasir.w@difc.ae';
        sr1.Linked_SR__C = SR111.id;
        Amendment__c amend = new Amendment__c(ServiceRequest__c = SR111.id,amendment_Type__c ='Tenant Authorized Representative');
         insert amend;
         Test.startTest();
         ApexPages.StandardController sc = new ApexPages.StandardController(SR111);
         	create_Fitout_Contacts cont = New create_Fitout_Contacts(sc);
         	cont.LoadData();
         	cont.save();
         Test.stopTest();
	}
}