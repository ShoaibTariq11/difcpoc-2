@isTest

public class TestTrainingRequestViewController {
    
    static testmethod void unitTest1(){
    	
    	Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='test2123@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com',Community_User_Role__c = 'Company Services, Employee Services; Property Services');
          
        Training_Request__c training = new Training_Request__c();
        training.First_Name__c = 'Test';
        training.Last_Name__c = 'Test ing';
        training.Mobile__c = '+97188776654';
        training.Email__c = 'Test@test.com';
        INSERT training;
         
        Training_Master__c tr = new Training_Master__c();
        tr.Name = 'Test';
        tr.Start_DateTime__c = system.today()+1;
        tr.To_DateTime__c = system.today()+8;
        tr.Category__c = 'Company Services';   
        tr.Available_Seats__c   = 10;
        tr.Seats_Occupied__c = 1;
        tr.Status__c = 'Approved by HOD';
        INSERT tr;     
        Apexpages.standardcontroller controller = new Apexpages.standardcontroller(training);
            
    	TrainingRequestViewController training1 = new TrainingRequestViewController(controller);
    	training1.SelectedValue = 'Company Services';
    	training1.getCommunityRoles();
    	training1.trainingRequestPage();
    }
}