@isTest
public class Person_Auth_DEWS_Registration_Test {
    
    static testmethod void testAuthDEWSRegistrationPage() {
        
        Map<string,string> mapRecordTypeIds = new Map<string,string>();
        
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('License_Renewal','Notification_of_Personal_Data_Operations','Annual_Return','Annual_Reporting','Add_or_Remove_Auditor','Allotment_of_Shares_Membership_Interest','Add_Audited_Accounts','Notice_of_Amemdment_of_AOA')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount 		= new Account();
        objAccount.Name 		= 'Test Custoer 1';
        objAccount.E_mail__c 	= 'test@test.com';
        objAccount.BP_No__c 	= '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c  = 'LTD';
        objAccount.ROC_Status__c 			= 'Active';
        objAccount.Financial_Year_End__c    = 'Yes';
        insert objAccount;
        
        ID BDrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Business Development').getRecordTypeId(); //Added as per V1.2
        Contact objContact      	= new Contact();
        objContact.FirstName 		= 'Test';
        objContact.LastName 		= 'Name';
        objContact.AccountId 		= objAccount.Id; 
        objContact.Passport_No__c	= 'ABC12345';
        objContact.RecordTypeId     = BDrectypeid ;
        objContact.important__C	    = true;
        objContact.Is_Active__c     = true;
        insert objContact;
        
        Profile thisProfile = [Select Id from profile where Name='DIFC Customer Community User Custom']; 

		String orgId      = UserInfo.getOrganizationId(); 
		String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        String uniqueName = orgId+dateString+'0000'; 

        User newUser       		= new User();
        newUser.ContactId  		= objContact.Id;
        newUser.email      		= uniqueName + '@test' + orgId + '.org';
        //newUser.Phone      		= '+971 00000000';
        newUser.Username   		= uniqueName + '@test' + orgId + '.org';
        newUser.firstname  		= 'Alan';
        newUser.lastName   		= 'McCarthy';
        newUser.EmailEncodingKey  = 'ISO-8859-1';
        newUser.Alias 			= uniqueName.substring(18, 23);
        newUser.TimeZoneSidKey  = 'America/Los_Angeles';
        newUser.LocaleSidKey    = 'en_US';
        newUser.LanguageLocaleKey = 'en_US';
        newUser.ProfileId         = thisProfile.Id;
        
        system.runAs(newUser){
              
            Service_Request__c objSR = new Service_Request__c();
        	
        	Relationship__c thisRelations = new Relationship__c();
            thisRelations.Relationship_Type__c = 'Has Temporary Employee';
            thisRelations.Object_Contact__c = objContact.Id;
            thisRelations.Relationship_Type__c = 'Is Authorized Signatory for';
            thisRelations.Active__c    = true;
            thisRelations.Subject_Account__c = objAccount.Id;
            insert thisRelations;
            
            objSR.Commercial_Activity__c = objContact.Id;
            objSR.Customer__c            = objAccount.Id;
            
            
              PageReference pageRef = Page.Person_Authorised_DEWS_Registration;
              Test.setCurrentPage(pageRef);

            pageRef.getParameters().put('RecordType', mapRecordTypeIds.get('License_Renewal'));
            ApexPages.StandardController sc                       = new ApexPages.StandardController(objSR);
            Person_Authorised_DEWS_Registration_edit thisInstance = new Person_Authorised_DEWS_Registration_edit(sc);
            thisInstance.getItems();
           }
     }
    
     static testmethod void testAuthDEWSRegistrationPageUpdate() {
        
        Map<string,string> mapRecordTypeIds = new Map<string,string>();
        
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('License_Renewal','Notification_of_Personal_Data_Operations','Annual_Return','Annual_Reporting','Add_or_Remove_Auditor','Allotment_of_Shares_Membership_Interest','Add_Audited_Accounts','Notice_of_Amemdment_of_AOA')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount 		= new Account();
        objAccount.Name 		= 'Test Custoer 1';
        objAccount.E_mail__c 	= 'test@test.com';
        objAccount.BP_No__c 	= '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c  = 'LTD';
        objAccount.ROC_Status__c 			= 'Active';
        objAccount.Financial_Year_End__c    = 'Yes';
        insert objAccount;
        
        ID BDrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Business Development').getRecordTypeId(); //Added as per V1.2
        Contact objContact      	= new Contact();
        objContact.FirstName 		= 'Test';
        objContact.LastName 		= 'Name';
        objContact.AccountId 		= objAccount.Id; 
        objContact.Passport_No__c	= 'ABC12345';
        objContact.RecordTypeId     = BDrectypeid ;
        objContact.important__C	    = true;
        objContact.Is_Active__c     = true;
        insert objContact;
        
    

		Profile thisProfile = [Select Id from profile where Name='DIFC Customer Community User Custom']; 

		String orgId      = UserInfo.getOrganizationId(); 
		String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        String uniqueName = orgId+dateString+'0000'; 

        User newUser       		= new User();
        newUser.ContactId  		= objContact.Id;
        newUser.email      		= uniqueName + '@test' + orgId + '.org';
        //newUser.Phone      		= '+971 00000000';
        newUser.Username   		= uniqueName + '@test' + orgId + '.org';
        newUser.firstname  		= 'Alan';
        newUser.lastName   		= 'McCarthy';
        newUser.EmailEncodingKey  = 'ISO-8859-1';
        newUser.Alias 			= uniqueName.substring(18, 23);
        newUser.TimeZoneSidKey  = 'America/Los_Angeles';
        newUser.LocaleSidKey    = 'en_US';
        newUser.LanguageLocaleKey = 'en_US';
        newUser.ProfileId         = thisProfile.Id;
        
        system.runAs(newUser){
              
            Service_Request__c objSR = new Service_Request__c();
        	
        	Relationship__c thisRelations = new Relationship__c();
            thisRelations.Relationship_Type__c = 'Has Temporary Employee';
            thisRelations.Object_Contact__c = objContact.Id;
            thisRelations.Relationship_Type__c = 'Is Authorized Signatory for';
            thisRelations.Active__c    = true;
            thisRelations.Subject_Account__c = objAccount.Id;
            insert thisRelations;
            
            objSR.Commercial_Activity__c = objContact.Id;
            objSR.Customer__c            = objAccount.Id;
            insert objSR;
            
            
              PageReference pageRef = Page.Person_Authorised_DEWS_Registration;
              Test.setCurrentPage(pageRef);

            pageRef.getParameters().put('RecordType', mapRecordTypeIds.get('License_Renewal'));
            ApexPages.StandardController sc                       = new ApexPages.StandardController(objSR);
            Person_Authorised_DEWS_Registration_edit thisInstance = new Person_Authorised_DEWS_Registration_edit(sc);
            thisInstance.getItems();
            thisInstance.SaveConfirmation();

        }
     }
   }