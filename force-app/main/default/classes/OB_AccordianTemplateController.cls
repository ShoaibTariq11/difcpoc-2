/**
 * Description : Controller for OB_AccordianTemplate component
 *
 * ****************************************************************************************
 * History :
 * [31.OCT.2019] Prateek Kadkol - Code Creation
 * [6.MAY.2020] Prateek Kadkol - Edited to user the agent helper class
 */

public without sharing class OB_AccordianTemplateController {
    public class RequestWrap {
        @AuraEnabled public String flowId { get; set; }
        @AuraEnabled public string pageId { get; set; }
        @AuraEnabled public String srId { get; set; }
        @AuraEnabled public String commandButtonId { get; set; }
        public RequestWrap() {
        }
    }
    public class RespondWrap {
        @AuraEnabled public boolean isSRLocked { get; set; }
        @AuraEnabled public string communityReviewPageURL { get; set; }
        @AuraEnabled public string flowNAme  { get; set; }
        @AuraEnabled public list<pageWrap> pageWrapList { get; set; }
        @AuraEnabled public HexaBPM__Section_Detail__c ButtonSection { get; set; }
        @AuraEnabled public map<string, boolean> lockingMap { get; set; }
        @AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }
        public RespondWrap() {
            lockingMap = new map<string, boolean>();
            pageWrapList = new list<pageWrap>();
        }
    }
    public class pageWrap {
        @AuraEnabled public HexaBPM__Page__c pageObj { get; set; }
        @AuraEnabled public boolean isComplete { get; set; }
        @AuraEnabled public boolean isRendered { get; set; }
        @AuraEnabled public string isLocked { get; set; }
        public pageWrap() {
            isRendered = false;
            isComplete = false;
        }
    }

    @AuraEnabled
    public static RespondWrap fetchPageFlowDeatils(string requestWrapParam) {
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap = new RespondWrap();

        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
        string pageTracker = '';
        string srId = reqWrap.srId;
        string pageId = reqWrap.pageId;
        string flowId = reqWrap.flowId;

        //Locking and Unlocking logic
        if(!String.isBlank(srId)) {
            OB_LockAndUnlockSRHelper.RequestWrapper reqWarpLocUnLoc = new OB_LockAndUnlockSRHelper.RequestWrapper();
            reqWarpLocUnLoc.srId = srId;
            reqWarpLocUnLoc.currentCommunityPageId = pageId;
            reqWarpLocUnLoc.flowId = flowId;
            reqWarpLocUnLoc.communityReviewPageName = 'ob-reviewandconfirm';
            system.debug('$$$$$$$$$ ' + OB_LockAndUnlockSRHelper.lockAndUnlockSRById(reqWarpLocUnLoc));

            OB_LockAndUnlockSRHelper.ResponseWrapper respWarpLocUnLoc = new OB_LockAndUnlockSRHelper.ResponseWrapper();
            respWarpLocUnLoc = OB_LockAndUnlockSRHelper.lockAndUnlockSRById(reqWarpLocUnLoc);
            respWrap.isSRLocked = respWarpLocUnLoc.isSRLocked;
            respWrap.communityReviewPageURL = respWarpLocUnLoc.communityReviewPageURL;
        }

        string AccountId;
        string contactID;
        /* for(User usr :[Select Id, contact.AccountId from User where Id = :userinfo.getuserid()]) {
            AccountId = usr.contact.AccountId;
        } */
        for(User usrObj : OB_AgentEntityUtils.getUserById(userInfo.getUserId())) {
        if(usrObj.ContactId!=null && usrObj.contact.AccountId!=null) {
            AccountId = usrObj.contact.AccountId;
            contactID = usrObj.contactID;
        }
    }



        HexaBPM__Service_Request__c objSR = OB_QueryUtilityClass.QueryFullSR(SRID);
        respWrap.srObj = objSR;
        boolean IsValidSR = true;
        if(AccountId != null) {
            if(AccountId != objSR.HexaBPM__Customer__c){
                if(objSR.HexaBPM__Contact__c != null ){
                    if(objSR.HexaBPM__Contact__c != contactID){
                        IsValidSR = false;
                    }
                }else{
                    IsValidSR = false;
                }                
            }            
        } else if(AccountId == null && objSR.HexaBPM__Record_Type_Name__c != 'New_User_Registration'){
            IsValidSR = false;
        }
        system.debug('IsValidSR===>' + IsValidSR);
        if(IsValidSR) {

            pageTracker = objSR.Page_Tracker__c;
            map<string, string> MapHiddenPageIds = PageFlowControllerHelper.getHiddenPageIdsMap(reqWrap.flowID, objSR);

            system.debug('pageTracker==>' + pageTracker);
            system.debug('MapHiddenPageIds===>' + MapHiddenPageIds);

            respWrap.lockingMap = OB_LockAndUnlockSRHelper.GetPageLockDetails(reqWrap.flowID, SRID);


            for(HexaBPM__Page__c pagesObj :[SELECT id, Name, SLA_Display__c,HexaBPM__Page_Flow__r.HexaBPM__Record_Type_API_Name__c, Community_Page__c, Pathway_Guide__c, HexaBPM__Page_Description__c, HexaBPM__Render_by_Default__c FROM HexaBPM__Page__c
                                            WHERE HexaBPM__Page_Flow__c = :reqWrap.flowID ORDER BY HexaBPM__Page_Order__c ASC]) {
                if(pagesObj.HexaBPM__Render_by_Default__c || (!pagesObj.HexaBPM__Render_by_Default__c && MapHiddenPageIds.get(pagesObj.Id) == null)) {
                    system.debug('pagesObj.Name===>' + pagesObj.Name);
                    pageWrap pageWrapClass = new pageWrap();
                    pageWrapClass.pageObj = pagesObj;
                    if(pageTracker != null) {
                        pageWrapClass.isComplete = pageTracker.containsIgnoreCase(pagesObj.id) ? true :false;
                    }
                    if(!respWrap.lockingMap.isEmpty()) {
                        if(respWrap.lockingMap.containsKey(pagesObj.id)) {
                            pageWrapClass.isLocked = String.valueOf(respWrap.lockingMap.get(pagesObj.id));
                        }
                    }
                    respWrap.pageWrapList.add(pageWrapClass);
                }
            }

            for(HexaBPM__Page_Flow__c fl : [select name,HexaBPM__Record_Type_API_Name__c from HexaBPM__Page_Flow__c where id =:reqWrap.flowID]){
                respWrap.flowNAme = fl.HexaBPM__Record_Type_API_Name__c;
            }
           
        }
        return respWrap;
    }

}