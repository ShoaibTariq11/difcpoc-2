//Catch Gateway response.
public  class CLS_ADCB_PayementConfirm {


  public  Map<String,String> Parameter{get;set;}
  string req_reference_number{get;set;}
  string transaction_id{get;set;}
   public boolean IsAdded{get;set;}
    public CLS_ADCB_PayementConfirm (){  
      
    
     Parameter= ApexPages.currentPage().getParameters();
  IsAdded=false;
  
    
     
    } 
 public void updateServiceRequest(string SId,Map<String,String> tempParameter) 
    {
       
    try
    {        
        Service_Request__c ObjSR=[select Bank_Name__c,Sys_Estimated_Share_Capital__c,Customer__c,Name,Id from Service_Request__c where id=:SId];
        List<Amendment__c> ListAmdUnit=[select Nominal_Value__c,id from Amendment__c where Amendment_Type__c='Unit' and ServiceRequest__c=:ObjSR.id];
        List<Amendment__c> ListAmd=[select id from Amendment__c where Amendment_Type__c='Tenant' and ServiceRequest__c=:ObjSR.id];
        List<Receipt__c> ListRec=new List<Receipt__c>();
     
     System.debug('ObjSR===>'+ObjSR);
     System.debug('ListAmdUnit===>'+ListAmdUnit);
     System.debug('ListAmd===>'+ListAmd);
     
    
     
            
        List<Receipt__c> ListRecAdded=[select id from Receipt__c where Service_Request__c=:SId];//to avoid duplicate inserts 
        
         System.debug('ListRecAdded===>'+ListRecAdded);
         
        
       if(ListRecAdded.isEmpty() && IsAdded==false)
        {
             for(Amendment__c ObjAmd:ListAmdUnit)
            {
                Receipt__c ObjRec=new Receipt__c();
                ObjRec.Receivable_Type__c='Lease Security Deposit';
                ObjRec.Amount__c=ObjAmd.Nominal_Value__c;
                
                ObjRec.Recordtypeid=Schema.SObjectType.Receipt__c.getRecordTypeInfosByDeveloperName().get('Card').getRecordTypeId();
                
                  if(tempParameter.get('decision')=='ACCEPT')         
                ObjRec.Payment_Status__c='Success';
                else
                ObjRec.Payment_Status__c='Failure';
            
                if(tempParameter.get('score_card_scheme')!=null)
                ObjRec.Card_Type__c=tempParameter.get('score_card_scheme');
                
                //updR.PG_Error_Code__c=Parameter.get('decision');
                ObjRec.PG_Error_Message__c=tempParameter.get('message');
               
                
                ObjRec.Customer__c=ObjSR.Customer__c;
                ObjRec.Receipt_Type__c='Card';
                ObjRec.Amendment__c=ObjAmd.Id;
                ObjRec.Tenant__c=ListAmd[0].id;
                 ObjRec.Service_Request__c=SId;
                
             if(tempParameter.get('transaction_id')!=null)
                {
                    ObjRec.Payment_Gateway_Ref_No__c=tempParameter.get('transaction_id'); 
                     ObjRec.Bank_Ref_No__c=tempParameter.get('transaction_id');
                }
            
            ObjRec.Transaction_Date__c=Datetime.now();
              
            ListRec.add(ObjRec);
            
            }
                insert ListRec;
            //Update only if accepted 
            if(tempParameter.get('decision')=='ACCEPT')
            {
                ObjSR.Bank_Name__c='Yes';
                update ObjSR;
            }
                
            //if(!ListRec.isEmpty())
           // {
                
                /*
                insert ListRec;
               List<Step__c> ObjStep=[select Id from Step__c where SR__c=:SId and Step_Code__c='SECURITY_DEPOSIT_PAYMENT' and Closed_Date_Time__c=null limit 1];
               if(!ObjStep.isEmpty())
                Cls_DocumentReupload_StatusChange.UpdateStatus(ObjStep[0].id, 'a1M20000001iL9S');
            */
           // }
            IsAdded=true;
        }
       
    }catch(Exception e)
    {
                Log__c ObjLog=new Log__c(); ObjLog.Description__c=e.getMessage();    ObjLog.Type__c='Security Deposit Payment'; insert ObjLog;
       
    }
        
   
}
    
    public PageReference updateReceipt() 
    {
         profile p = [Select Name from Profile where Id =: userinfo.getProfileid()];
         
        string userLic=p.Name;   //avoid duplicate calls   
        if(Parameter.get('req_reference_number')!=null && userLic.contains('Plus'))//=='DIFC Customer Community Plus User Custom')
        {
                  //Get the transation receipt record for the payment done on the portal sidebar
           
            
            if(Parameter.get('decision')=='ACCEPT') 
            {

               updateServiceRequest(Parameter.get('req_reference_number'),Parameter);
            }
            else
            {
                Log__c ObjLog=new Log__c();
                ObjLog.Description__c=string.valueof(Parameter);
                ObjLog.Type__c='Security Deposit Payment';
                insert ObjLog;
                
                
            }
            
          
          
           
        }
        
   //Return to the Portal Home Page     
            PageReference pageRef = new PageReference(Label.Community_URL+'/'+Parameter.get('req_reference_number'));
           return pageRef ; 
    
            
       
    }
    
    /*
      public PageReference updateReceipt() 
    {
        if(Parameter.get('req_reference_number')!=null)
        {
                  //Get the transation receipt record for the payment done on the portal sidebar
           List<Receipt__c> ListRec =[select id,name,Amount__c,Card_Type__c,Payment_Status__c,Payment_Gateway_Ref_No__c,PG_Error_Code__c,PG_Error_Message__c 
                from Receipt__c where name=:Parameter.get('req_reference_number')];
        
        if(!ListRec.IsEmpty())
        {
          Receipt__c updR =ListRec[0];
      
            if(Parameter.get('transaction_id')!=null)
            {
                updR.Payment_Gateway_Ref_No__c=Parameter.get('transaction_id'); 
                 updR.Bank_Ref_No__c=Parameter.get('transaction_id');
            }
           
            
            if(Parameter.get('decision')=='ACCEPT')         
            updR.Payment_Status__c='Success';
            else
            updR.Payment_Status__c='Failure';
            
        
           updR.Transaction_Date__c=Datetime.now();
        
           if(Parameter.get('score_card_scheme')!=null)
            updR.Card_Type__c=Parameter.get('score_card_scheme');
            
            //updR.PG_Error_Code__c=Parameter.get('decision');
            updR.PG_Error_Message__c=Parameter.get('message');
            
            update updR;
            //Return to the Portal Home Page     
            //PageReference pageRef = new PageReference(Label.Community_URL+'/'+updR.id);
           // return pageRef; 
           }
        }
        
    return null;
    
            
       
    }
    
    */
    
    

}