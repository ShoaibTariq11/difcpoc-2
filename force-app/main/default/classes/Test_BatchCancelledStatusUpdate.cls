@isTest(seealldata=false)
private class Test_BatchCancelledStatusUpdate {

    static testMethod void myUnitTest() {
    
    
            List<Account> accList = new List<Account>();
            Account objAccount = new Account();
            objAccount.Name = 'Test Custoer 354545';
            objAccount.E_mail__c = 'test@test.com';
            objAccount.BP_No__c = '1234';
            accList.add(objAccount);
            
             objAccount = new Account();
            objAccount.Name = 'Test Custoer 354544';
            objAccount.E_mail__c = 'test@tests.com';
            objAccount.BP_No__c = '5678';
            accList.add(objAccount);
            
            insert accList;
            
            SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'New Employment Visa package';
        objTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_New';
        objTemplate.Menutext__c = 'DIFC_Sponsorship_Visa_New';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Open a Business';
        objTemplate.Active__c = true;        
        insert objTemplate;
            
            
            list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
            SR_Status__c objSRStatus;
            objSRStatus = new SR_Status__c();
            objSRStatus.Name = 'Submitted';
            objSRStatus.Code__c = 'Submitted';
            objSRStatus.Type__c = 'End';
            lstSRStatus.add(objSRStatus);
            insert lstSRStatus;
            
            
            Service_Request__c objSR = new Service_Request__c();
            objSR.Customer__c = accList[0].Id;
            objSR.Email__c = 'testsr@difc.com';
            objSR.Internal_SR_Status__c = lstSRStatus[0].Id; // changing the status to Draft
            objSR.External_SR_Status__c = lstSRStatus[0].Id; // changing the status to Draft
            //objSR.Contact__c = objContact.Id;
            objSR.SR_Template__c = objTemplate.Id;
            objSR.SR_Group__c = 'GS';           
            insert objSR;
            
            service_request__c sr1 =[select id,External_SR_Status__r.name  from service_request__c where id =: objSR.id];
            
            system.assertEquals(sr1.External_SR_Status__r.name,'Submitted');
            
            
            Step_Template__c stpTemp = new Step_Template__c();
            stpTemp.Name = 'Entry Permit is Issued';
            stpTemp.Code__c = 'Entry Permit is Issued';
            //stpTemp.SR_Template__c =  srTemplate.id;
            stpTemp.Step_RecordType_API_Name__c = 'General';
            insert stpTemp;
            
            SR_Steps__c srStp = new SR_Steps__c();
            srStp.SR_Template__c = objTemplate.id;
            srStp.Step_Template__c = stpTemp.id;
            insert srStp;
            
            status__c st = new status__c();
            st.Name ='Cancelled';
            st.Code__c = 'Cancelled';
            insert st;
            
           
             
            step__c stp = new step__c();
            stp.sr__c = sr1.id;
          //stp.step_Name__c = 'HOD Review';
            stp.Step_Template__c = stpTemp.id;
            stp.Applicant_Email__c = 'c-sravan.Booragadda@difc.ae';
            stp.status__c = st.id;
            stp.Closed_date_time__c = system.now();
            insert stp;
            
            
        test.startTest();
        step__c stp1 =[select id,step_status__c,Sys_SR_Group__c,sr__r.External_SR_Status__r.Name,sr__c, sr__r.External_SR_Status__c,createdDate from step__c where id =: stp.id];
        
        system.assertEquals(stp1.Sys_SR_Group__c,'GS');
        system.assertNotEquals(stp1.sr__r.id, '1234343746384683643');
        //system.assertEquals(stp1.sr__r.External_SR_Status__c,'123456');
        
       // system.assertEquals(stp1.sr__r.External_SR_Status__r.Name,'submitted');
       
      
       
        BatchCancelledStatusUpdate obj = new BatchCancelledStatusUpdate();
        database.executeBatch(obj);  
        
        
        test.stopTest();
            
            
    
    }
    
    
     static testMethod void myUnitTest1() {
    
    
            List<Account> accList = new List<Account>();
            Account objAccount = new Account();
            objAccount.Name = 'Test Custoer 354545';
            objAccount.E_mail__c = 'test@test.com';
            objAccount.BP_No__c = '1234';
            accList.add(objAccount);
            
             objAccount = new Account();
            objAccount.Name = 'Test Custoer 354544';
            objAccount.E_mail__c = 'test@tests.com';
            objAccount.BP_No__c = '5678';
            accList.add(objAccount);
            
            insert accList;
            
            SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'New Employment Visa package';
        objTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_New';
        objTemplate.Menutext__c = 'DIFC_Sponsorship_Visa_New';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Open a Business';
        objTemplate.Active__c = true;        
        insert objTemplate;
            
            
            list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
            SR_Status__c objSRStatus;
            objSRStatus = new SR_Status__c();
            objSRStatus.Name = 'Submitted';
            objSRStatus.Code__c = 'Submitted';
            objSRStatus.Type__c = 'End';
            lstSRStatus.add(objSRStatus);
            insert lstSRStatus;
            
            
            Service_Request__c objSR = new Service_Request__c();
            objSR.Customer__c = accList[0].Id;
            objSR.Email__c = 'testsr@difc.com';
            objSR.Internal_SR_Status__c = lstSRStatus[0].Id; // changing the status to Draft
            objSR.External_SR_Status__c = lstSRStatus[0].Id; // changing the status to Draft
            //objSR.Contact__c = objContact.Id;
            objSR.SR_Template__c = objTemplate.Id;
            objSR.SR_Group__c = 'GS';           
            insert objSR;
            
            service_request__c sr1 =[select id,External_SR_Status__r.name  from service_request__c where id =: objSR.id];
            
            system.assertEquals(sr1.External_SR_Status__r.name,'Submitted');
            
            
            Step_Template__c stpTemp = new Step_Template__c();
            stpTemp.Name = 'Entry Permit is Issued';
            stpTemp.Code__c = 'Entry Permit is Issued';
            //stpTemp.SR_Template__c =  srTemplate.id;
            stpTemp.Step_RecordType_API_Name__c = 'General';
            insert stpTemp;
            
            SR_Steps__c srStp = new SR_Steps__c();
            srStp.SR_Template__c = objTemplate.id;
            srStp.Step_Template__c = stpTemp.id;
            insert srStp;
            
            status__c st = new status__c();
            st.Name ='Cancelled';
            st.Code__c = 'Cancelled';
            insert st;
            
           
             
            step__c stp = new step__c();
            stp.sr__c = sr1.id;
          //stp.step_Name__c = 'HOD Review';
            stp.Step_Template__c = stpTemp.id;
            stp.Applicant_Email__c = 'c-sravan.Booragadda@difc.ae';
            stp.status__c = st.id;
            stp.Closed_date_time__c = system.now();
            insert stp;
            
            
       // test.startTest();
        step__c stp1 =[select id,step_status__c,Sys_SR_Group__c,sr__r.External_SR_Status__r.Name,sr__c, sr__r.External_SR_Status__c,createdDate from step__c where id =: stp.id];
        
        system.assertEquals(stp1.Sys_SR_Group__c,'GS');
        system.assertNotEquals(stp1.sr__r.id, '1234343746384683643');
        //system.assertEquals(stp1.sr__r.External_SR_Status__c,'123456');
      
        
        Datetime dt = system.now().addMinutes(1);
        string str = dt.second()+' '+(dt.minute() )+' '+dt.hour()+' '+dt.day()+' '+dt.month()+' '+' ? '+ dt.year();
        system.schedule('ScheduleBatchCancelledStatusUpdate',str,new ScheduleBatchCancelledStatusUpdate());
      //  Database.executeBatch(new ScheduleBatchCancelledStatusUpdate ());
              
       // test.stopTest();
            
            
    
    }
    
}