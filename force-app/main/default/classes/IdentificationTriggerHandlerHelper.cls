/******************************************************************************************
 *  Identification Trigger Handler Helper
 
 *  Author   : Mudasdir Wani
 *  Date     : 2-March-2020                          
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        	Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0    2-March-2020 	Mudasir       Created - 
 										                          
*******************************************************************************************/

public with sharing class IdentificationTriggerHandlerHelper {
    /***************************************************
    *	Description	:	Prepare account from the identification on account after insert and update of the identificaion provide the valid to date is changed
    *	Created By	:	Mudasir
    *	Parameters	:	List of identifications
    *	Return 		:	List of Accounts 
    ***************************************************/
    public static Account prepareAccountFromIdentifications(Identification__c identficationRec){
    	return new Account(id =identficationRec.Account__c,Index_Card__c =identficationRec.id);
    }
    
    public static void updateIdentficationsOnAccounts(List<Account> accountList){
    	if(accountList != NUll && accountList.size() > 0){
    		try{
    			update accountList;
    		}catch(Exception e){ insert LogDetails.CreateLog(null, 'IdentificationTriggerHandlerHelper', 'Line # '+e.getLineNumber()+'\n'+e.getMessage());}
    	}
    }
}