public with sharing class create_Fitout_Contacts {
    private Service_Request__c serviceRequest;
    public string serviceRequestStatus{get;set;}
    public List<Amendment__c> amendmentList{get;set;}
    public boolean refreshParentPage{get;set;}
    public create_Fitout_Contacts(ApexPages.StandardController controller){
        //Get the service request record 
        initializeData();
        if(!Test.isRunningTest())controller.addFields(new List<String> {'Name','Customer__c','contractor__c','Company_Name__c','External_status_name__c'});
        serviceRequest = (Service_Request__c) controller.getRecord();
        system.debug('serviceRequest-----------------'+serviceRequest);
        LoadData();
    }
    
    @testVisible
    private void initializeData(){
        amendmentList = new List<amendment__c>(); 
        refreshParentPage = false;
    }
    @testVisible
    private void LoadData(){
        //We can not use describe call as we have no method to filter it using the record Type 
        Set<String> existingAmendmentTypes = new Set<String>();
        Id fitoutContactRecordTypeId = SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Fit_Out_Contact').getRecordTypeId();
        serviceRequestStatus = serviceRequest != Null ? serviceRequest.External_status_name__c  : '';
        List<String> amendmentTypes = new List<String>{'Tenant Authorized Representative','Tenant On - Site Fit - Out Representative','Emergency Contacts'};
        amendmentList = new List<amendment__c>();
        for(amendment__c amend : [SELECT Id, Name_DDP__c,Address1__c,Amendment_Type__c, Name__c,Person_Email__c,Email_Address__c, Title__c,Contact_Number_During_Office_Hours__c,Contact_Number_After_Office_Hours__c,Passport_No__c,Nationality_list__c,Company_Name__c,Address__c,Fax__c,ServiceRequest__c FROM Amendment__c where ServiceRequest__c =:serviceRequest.id and Amendment_Type__c IN : amendmentTypes]){
            amendmentList.add(amend);
            existingAmendmentTypes.add(amend.Amendment_Type__c);
        }
        if(amendmentList.size() <3){
            for(String amendType :amendmentTypes){
            	if(!existingAmendmentTypes.contains(amendType)){
                	amendmentList.add(new Amendment__c(Amendment_Type__c = amendType,ServiceRequest__c = serviceRequest.id,RecordTypeId=fitoutContactRecordTypeId));
            	}
            }
        }
    }
    
    public pagereference save(){
        try{
            upsert amendmentList;
            refreshParentPage = true; 
        }
        catch(VisualforceException ex){if(!ex.getMessage().contains('Upsert failed'))ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR ,ex.getMessage()) );}
        catch(TypeException ex){ if(!ex.getMessage().contains('Upsert failed'))ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR ,ex.getMessage()) );}
        catch(DMLException ex){if(!ex.getMessage().contains('Upsert failed'))ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR ,ex.getMessage()) );}
        catch(Exception ex){if(!ex.getMessage().contains('Upsert failed'))ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR ,ex.getMessage()) ); }
        return null;
    }
}