/*----------------------------------------------------------------------------------------------------------------------
Modification History     
-----------------------------------------------------------------------------------------------------------------------
 V.No    Date        	Updated By      	Description
 V0.0    26-12-2018  	Azfer          		This is a the test class for the class CLS_SendSMS, this class will be the callout mockup
----------------------------------------------------------------------------------------------------------------------*/
@isTest
public class CLS_SendSMSTest {

    @isTest static void Test_SendSMS(){
        HttpCalloutMockTest HttpCalloutMockTestObj = new HttpCalloutMockTest(200, 'Pass', 'OK:123456789', new Map<String, String>());
            
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, HttpCalloutMockTestObj);
        
        Test.startTest();
        CLS_SendSMS.sendsms('+971507541798','Test');
        Test.stopTest();
    }
}