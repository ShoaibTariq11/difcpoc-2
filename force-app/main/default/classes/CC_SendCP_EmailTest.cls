/*
    Author      : Leeba
    Date        : 29-March-2020
    Description : Test class for CC_SendCP_Email
    --------------------------------------------------------------------------------------
*/

@isTest
public class CC_SendCP_EmailTest{

    public static testMethod void CC_SendCP_Email() {
    
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
      
        
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
       insert objsrTemp;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        objHexaSR.HexaBPM__Email__c = 'test@test.com';
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
        
        HexaBPM__Document_Master__c docmaster = new HexaBPM__Document_Master__c ();
        docmaster.HexaBPM__Code__c ='Commercial_Permission';
        insert docmaster;
        
         HexaBPM__Document_Master__c docmaster2 = new HexaBPM__Document_Master__c ();
        docmaster2.HexaBPM__Code__c ='Commercial_Permission_Dual_License';
        insert docmaster2;
        
        HexaBPM__SR_Template_Docs__c objSRTempdoc = new HexaBPM__SR_Template_Docs__c();
        objSRTempdoc.Visible_to_GS__c = true;
        objSRTempdoc.HexaBPM__Document_Master__c = docmaster.id;
        insert objSRTempdoc;
        
        HexaBPM__SR_Doc__c objSrDoc = new HexaBPM__SR_Doc__c();
        objSrDoc.HexaBPM__Service_Request__c = objHexaSR.Id;
        objsrDoc.HexaBPM__Document_Master__c = docmaster.id;
        objSRDoc.HexaBPM__SR_Template_Doc__c = objSRTempdoc.Id;
         objSrDoc.HexaBPM__Doc_ID__c = '123';
        insert objSRDoc;
        
         HexaBPM__SR_Doc__c objSrDoc2 = new HexaBPM__SR_Doc__c();
        objSrDoc2.HexaBPM__Service_Request__c = objHexaSR.Id;
        objsrDoc2.HexaBPM__Document_Master__c = docmaster2.id;
        objSRDoc2.HexaBPM__SR_Template_Doc__c = objSRTempdoc.Id;
         objSrDoc.HexaBPM__Doc_ID__c = '123';
        insert objSRDoc2;
        
        ContentVersion contentVersionInsert = new ContentVersion(
                Title = 'Test',
                PathOnClient = 'Test.jpg',
                VersionData = Blob.valueOf('Test Content Data'),
                IsMajorVersion = true
            );
            insert contentVersionInsert;
            List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
            
            //create ContentDocumentLink  record 
            ContentDocumentLink cdl = New ContentDocumentLink();
            cdl.LinkedEntityId = objSrDoc.id;
            cdl.ContentDocumentId = documents[0].Id;
            cdl.shareType = 'V';
            insert cdl;
        
        objSrDoc.HexaBPM__Doc_ID__c = cdl.id;
        update objSrDoc;
       
       Blob b = Blob.valueOf('Test Data');
        
        Attachment attachment = new Attachment();
        attachment.ParentId = objSRDoc.Id;
        attachment.Name = 'Commercial Permission Letter';
        attachment.Body = b;
        insert(attachment);
        
        Test.startTest();
         HexaBPM__Step__c step = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR__r.Account_Owner_Email__c   
                                  ,HexaBPM__SR__r.Type_of_commercial_permission__c  ,HexaBPM__SR__r.HexaBPM__Record_Type_Name__c                           
                                  from HexaBPM__Step__c where Id=:objHexastep.Id];
        CC_SendCP_Email CC_SendCP_EmailObj = new CC_SendCP_Email();
        CC_SendCP_EmailObj.EvaluateCustomCode(objHexaSR,step); 
        
       
        objHexaSR.Type_of_commercial_permission__c = 'Dual license of DED licensed firms with an affiliate in DIFC';
        update objHexaSR;
         HexaBPM__Step__c step2 = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR__r.Account_Owner_Email__c   
                                  ,HexaBPM__SR__r.Type_of_commercial_permission__c     ,HexaBPM__SR__r.HexaBPM__Record_Type_Name__c                       
                                  from HexaBPM__Step__c where Id=:objHexastep.Id];
          CC_SendCP_EmailObj.EvaluateCustomCode(objHexaSR,step2); 
        Test.stopTest();
       
    }
 }