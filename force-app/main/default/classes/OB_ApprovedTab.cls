/**
 * Description : used to fech data to be displayed in the approved tab
 *
 * ****************************************************************************************
 * History :
 * [31.OCT.2019] Prateek Kadkol - Code Creation
 */
public without Sharing class OB_ApprovedTab {

@AuraEnabled
public static RespondWrap getStepData() {
    //declaration of wrapper
    RespondWrap respWrap = new RespondWrap();
    list<srStepRecWrap> srObjRecWrapList = new list<srStepRecWrap>();
    //string UserContactID = [select ContactId FROM User Where Id=:userInfo.getUserId() and ContactId!=null].ContactId;
    string UserAccID;
    /* for(User usr :[select Contact.accountID FROM User Where Id = :userInfo.getUserId() and ContactId != null]) {
            UserAccID = usr.contact.accountID;
       } */
    for(User usrObj : OB_AgentEntityUtils.getUserById(userInfo.getUserId())) {
        if(usrObj.ContactId!=null && usrObj.contact.AccountId!=null) {
            UserAccID = usrObj.contact.AccountId;
        }
    }
    if(UserAccID != null) {
        for(HexaBPM__Service_Request__c srObj :[SELECT id, HexaBPM__External_Status_Name__c, HexaBPM__SR_Template__r.name, RecordType.DeveloperName, RecordType.Name, (SELECT id, name, HexaBPM__SR_Step__r.Step_Name_on_Community__c, HexaBPM__SR__r.Name, HexaBPM__SR__r.SR_Number__c, HexaBPM__Step_Status__c, HexaBPM__Start_Date__c, HexaBPM__Step_Template__r.Name, Owner.name FROM HexaBPM__Steps_SR__r where Is_Client_Step__c = false and HexaBPM__SR_Step__r.Visible_In_Community__c = true)
                                                FROM HexaBPM__Service_Request__c WHERE HexaBPM__Customer__c = :UserAccID and RecordType.DeveloperName != 'New_User_Registration' and RecordType.DeveloperName != 'Converted_User_Registration' and RecordType.DeveloperName != 'Name_Check' and RecordType.DeveloperName != 'Superuser_Authorization']) {
            srStepRecWrap srObjWrapItem = new srStepRecWrap();
            srObjWrapItem.srObj = srObj;
            //srObjWrapItem.srStepList = srObj.HexaBPM__Steps_SR__r;
            map<string, HexaBPM__Step__c> DuplicateFilterMap = new map<string, HexaBPM__Step__c>();
            for(HexaBPM__Step__c stepOBj :srObj.HexaBPM__Steps_SR__r) {

            if(!DuplicateFilterMap.containsKey(stepOBj.HexaBPM__Step_Template__r.Name)) {
            DuplicateFilterMap.put(stepOBj.HexaBPM__Step_Template__r.Name, stepOBj);
            }

            }
            srObjWrapItem.srStepList = DuplicateFilterMap.values();

            srObjRecWrapList.add(srObjWrapItem);
        }
    }
    respWrap.srObjRecWrapList = srObjRecWrapList;
    return respWrap;
}
public class RequestWrap {
public RequestWrap() {
}
}
public class RespondWrap {
@AuraEnabled public list<srStepRecWrap> srObjRecWrapList { get; set; }
public RespondWrap() {

}
}
public class srStepRecWrap {
@AuraEnabled public list<HexaBPM__Step__c> srStepList { get; set; }
@AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }
public srStepRecWrap() {
}
}
}