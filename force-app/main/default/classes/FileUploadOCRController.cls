/**************************************************************************************************
* Name               : FileUploadController                                                       *
* Description        : Controller class for File Upload Lightning component                       *
* Created Date       : 20/04/2019                                                                 *
* Created By         : PWC                                                                        *
* ------------------------------------------------------------------------------------------------*
* VERSION  AUTHOR    DATE            COMMENTS                                                     *
* 1.0      Merul     21/10/2019      Initial Draft.                                               *        
**************************************************************************************************/
global without sharing class FileUploadOCRController 
{
    
    /*********************************************************************************************
    * @Description : Function to save file in chunks if greater than 4MB else normally           *  
    * @Params      : Id, String, String, String, String                                          *
    * @Return      : Id                                                                          *
    *********************************************************************************************/
    
    public static Schema.DescribeSObjectResult descObj;
    public static Map<String,Schema.SObjectField> fldDecsMap;
   
    
    //param: HexaBPM__Service_Request__c
    public static Schema.DescribeSObjectResult getDescObj(String objName)
    {
        descObj = (descObj == NULL ? Schema.getGlobalDescribe().get(objName).getDescribe() : descObj); 
        return descObj;
        
        
    }
    
    public static Map<String,Schema.SObjectField> getFldDecsMap(Schema.DescribeSObjectResult descObj)
    {
        
        if(descObj != NULL)
        {
          fldDecsMap  =  (fldDecsMap == NULL ?  descObj.fields.getMap() : fldDecsMap); 
          return fldDecsMap;
        }
         else
        return NULL;
    }
    
    
    
    @AuraEnabled
    public static String getFileName(Id parentId,String documentMasterCode) 
    {
    	String fileName = '';
    	// this is used if we pass the doument master then bind the content version with SR DOC else with service request
        	String srDocID = ''; 
        	String attachmentID = ''; 
        	srDocID = parentId;
        	if(String.IsNotBlank(parentId) && String.IsNotBlank(documentMasterCode)){
        		
        		for(HexaBPM__SR_Doc__c srDoc : [Select Id,HexaBPM__Document_Master__c,HexaBPM__Doc_ID__c 
        										From HexaBPM__SR_Doc__c 
        										Where HexaBPM__Document_Master__c != null 
        										 AND HexaBPM__Document_Master__r.HexaBPM__Code__c =: documentMasterCode
        										 AND HexaBPM__Service_Request__c =: parentId
        										 Order by lastmodifiedDate
        										 Limit 1]){
        			srDocID = srDoc.Id;
        			attachmentID = srDoc.HexaBPM__Doc_ID__c;
        			system.debug('==srDoc===='+srDoc);
        		}
        	}
        	
        	
        	
        	
        	system.debug('=srDocID==='+srDocID);
        	//
        	
        if(String.IsNotBlank(srDocID)){
        	for(ContentDocumentLink cDe : [Select ID,ContentDocumentId,ContentDocument.title 
        									From ContentDocumentLink 
        									Where LinkedEntityId =: srDocID]){
        		fileName = cDe.ContentDocument.title;
        		system.debug('==cDe===='+cDe);
        	}
        }
        	/*for(Attachment att:[Select ID,Name From Attachment Where ID =: attachmentID]){
        		fileName = att.Name;
        	}*/
        	
            system.debug('===PathOnClient=='+fileName);
            return fileName;
    }
    @AuraEnabled
    public static Id saveChunk(Id parentId, String fileName, String base64Data, String contentType, String fileId,String documentMasterCode) 
    {
        // check if fileId id ''(Always blank in first chunk), then call the saveTheFile method,
        //  which is save the check data and return the attachemnt Id after insert, 
        //  next time (in else) we are call the appentTOFile() method
        //   for update the attachment with reamins chunks   
        if (String.isBlank(fileId)) {
            fileId = saveTheFile(parentId, fileName, base64Data, contentType,documentMasterCode);
        } else {
            appendToFile(fileId, base64Data);
        }
        
        system.debug(fileId);
        return Id.valueOf(fileId);
    }
    
    /*********************************************************************************************
    * @Description : Function to save file                                                       *  
    * @Params      : Id, String, String, String                                                  *
    * @Return      : Id                                                                          *
    *********************************************************************************************/
    public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType,String documentMasterCode) 
    {
        system.debug(documentMasterCode+'############ saveTheFile-- '+fileName);
        system.debug('############ parentId-- '+parentId);
        try{
        	// this is used if we pass the doument master then bind the content version with SR DOC else with service request
        	String srDocID = ''; 
        	if(String.IsNotBlank(parentId) && String.IsNotBlank(documentMasterCode)){
        		
        		for(HexaBPM__SR_Doc__c srDoc : [Select Id,HexaBPM__Document_Master__c 
        										From HexaBPM__SR_Doc__c 
        										Where HexaBPM__Document_Master__c != null 
        										 AND HexaBPM__Document_Master__r.HexaBPM__Code__c =: documentMasterCode
        										 AND HexaBPM__Service_Request__c =: parentId
        										 Limit 1]){
        			srDocID = srDoc.Id;
        			system.debug('=parentId=srDoc===='+srDoc);
        		}
        	}
            ContentVersion conVer = new ContentVersion();
            conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
            conVer.PathOnClient = fileName; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = fileName; // Display name of the files
            conver.IsMajorVersion = false;
            base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
            conVer.VersionData =EncodingUtil.base64Decode(base64Data); // converting your binary string to Blob
            insert conVer;
            
            // First get the content document Id from ContentVersion
            Id conDocID = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
            
            //Create ContentDocumentLink
            ContentDocumentLink cDe = new ContentDocumentLink();
            cDe.ContentDocumentId = conDocID;
            cDe.LinkedEntityId = String.IsnotBlank(srDocID)?srDocID : parentId; // you can use objectId,GroupId etc
            cDe.ShareType = 'I'; // Inferred permission, checkout description of ContentDocumentLink object for more details
            cDe.Visibility = 'AllUsers';
            insert cDe;
            
            if(srDocID!=null && srDocID!=''){
            	HexaBPM__SR_Doc__c objDoc = new HexaBPM__SR_Doc__c(Id=srDocID,HexaBPM__Status__c='Uploaded',HexaBPM__Doc_ID__c=conVer.Id);
            	update objDoc;
            }
            system.debug('=== cDe.LinkedEntityId==='+ cDe.LinkedEntityId);
            return conVer.ID;
        }catch(Exception ex){
            system.debug(ex);
           // LoggerUtils.createErrorLog('ContentVersion', 'FileUploadController', 'saveTheFile', ex.getMessage() + ' - ' + ex.getStackTraceString());
        }
        return null;
    }
    
    /*********************************************************************************************
    * @Description : Function to append the rest of the chunk to already uploaded part of file   *  
    * @Params      : Id, String                                                                  *
    * @Return      : void                                                                        *
    *********************************************************************************************/
    private static void appendToFile(Id fileId, String base64Data) {
        system.debug(fileId);
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        try{
            ContentVersion a = [
                SELECT Id, VersionData 
                FROM ContentVersion
                WHERE Id =: fileId
            ];
            
            String existingBody = EncodingUtil.base64Encode(a.VersionData);
            a.VersionData = EncodingUtil.base64Decode(existingBody + base64Data);
            
            update a;
        }catch(Exception ex){
            system.debug(ex);
           // LoggerUtils.createErrorLog('ContentVersion', 'FileUploadController', 'appendToFile', ex.getMessage() + ' - ' + ex.getStackTraceString());
        }
    }
    
    @AuraEnabled
    public static FileUploadOCRController.ResponseWrapper processOCR(String fileId) 
    {
       system.debug('@@@@@@@@ Entered processOCR ');
       FileUploadOCRController.ResponseWrapper respWrap = new FileUploadOCRController.ResponseWrapper();
        
        try{
            // FileUploadOCRController.ResponseWrapper respWrap = new  FileUploadOCRController.ResponseWrapper();
            
            //To DO : logic to build the rest structure and send to OCR services.
            System.debug(fileId+' This is the file Id');
            ContentVersion cv = [SELECT VersionData 
                                   FROM ContentVersion 
                                  WHERE  Id=:fileId]; 
                                  
            String param = EncodingUtil.Base64Encode(cv.VersionData);
            system.debug('param@@@'+param);
            
            // Set Timeout limit
            Integer timeoutLimit = integer.ValueOf(system.label.APICallTimeoutLimit);
            Long endTime ,startTime ,totalTime ;
            startTime = datetime.now().getTime();
            
            // Instantiate a new http object
            Http h = new Http();
            
            // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
            HttpRequest req = new HttpRequest();
            // Pass in the endpoint to be used using the string url
            req.setEndpoint(system.label.OCR_url);
            req.setTimeout(timeoutLimit);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Username',system.label.OCR_username );
            req.setHeader('Password',system.label.OCR_password);
            req.setBody('{"content":"'+param+'","filename":"test.jpg"}');
            System.debug('REQUEST BODY: '+req.getBody());
                        
            // Send the request, and return a response
            HttpResponse res = h.send(req);
            endTime = datetime.now().getTime();
            totalTime = endTime - startTime;
            System.debug('====totalTime===='+totalTime);
            System.debug('RESPONSE STATUS CODE: '+res.getStatusCode());
            System.debug('RESPONSE BODY: '+res.getBody());
            String jsonToString = String.valueOf(JSON.deserializeUntyped(res.getBody()) );
            
            
            /*
                DIFC_ParseJson obj= new DIFC_ParseJson();
                List<String> passportData = obj.extractData(jsonToString);
                System.debug(passportData);
                return passportData;
            */
            system.debug('==jsonToString=='+jsonToString);
            if(string.IsNotBlank(jsonToString)){
            	respWrap.mapFieldApiObject = processOCRResponse(jsonToString);
            }
            system.debug('====respWrap.mapFieldApiObject=='+respWrap.mapFieldApiObject);
            return respWrap;
            
        }
                        
        catch(exception e){
          System.debug('Exception Caught at line : '+e.getLineNumber()+' in DIFC_OCRServices class. Message: '+e.getMessage());
            return null;}
        
    }
    
    public static Map<String,Object> processOCRResponse(String jsonString) 
    {
        //jsonString = '{\"Name\":\"OSULLIVAN,LAUREN\",\"Gender\":\"F\",\"Country\":\"IRL\",\"DOB\":\"1988-05-04\",
        //\"PassportNo\":\"XN5004216\",\"PassportExpiry\":\"2023-09-15\",\"PersonalNo\":\"              \"}';
        jsonString = '['+jsonString+']';
         system.debug('======jsonString=====');
        List<Object> result = (List<Object>)JSON.deserializeUntyped(jsonString);
        Map<String,Object> mapResp;
        for(Object obj : result) 
        { 
            mapResp = (Map<String,Object>)obj; 
            
        }
        
        system.debug('@@@@@@@@@@ mapResp  '+mapResp );
        Map<String,Object> mapFieldApiObject = new Map<String,Object>();
        
        
        //change it to custom metadatatype.
        Map<String,String> mapOcrSFfld = new Map<String,String>{
                                                                 'Gender'=>'gender__c',
                                                                 'DOB'=>'date_of_birth__c',
                                                                 'PassportNo'=>'passport_no__c',
                                                                 'PassportExpiry'=>'date_of_expiry__c',
                                                                 'PersonalNo'=>'',
                                                                 'Country'=>'nationality__c'
                                                               };
        
        
        
        //resolving name to first name and last name.
        if(mapResp.size() > 0 && mapResp.containsKey('Name') )
        {
            String[] name = ( (String) mapResp.get('Name')).split(',');
            mapFieldApiObject.put('last_name__c',name[0] );
            mapFieldApiObject.put('first_name__c',name[1] );
        }
        // get nationality
        if(mapResp.size() > 0 && mapResp.containsKey('Country') )
        {
            String nationality = getNationlaity((String) mapResp.get('Country'),'');
            mapFieldApiObject.put('nationality__c',nationality );
        }
        
        
        for(String ORCFldapi : mapResp.keySet() )
        {
        	system.debug('=====ORCFldapi===');
            if( ORCFldapi != 'Country' ){
	            String fldApiSF = (mapOcrSFfld.containsKey(ORCFldapi) ? mapOcrSFfld.get(ORCFldapi).toLowerCase() : NULL);
	            Object objValue =  mapResp.get(ORCFldapi);
	            if(fldApiSF != NULL && fldApiSF != '' && objValue != NULL && objValue != '')
	            {
	              // mapFieldApiObject.put(fldApiSF,objValue);
	            }
	            typeCastFlds(fldApiSF,objValue,mapFieldApiObject);
            }
            
        }
        
        system.debug('$$$$$$$$$$$ mapFieldApiObject '+mapFieldApiObject);
        
        return mapFieldApiObject;
        
    }
    
    /*
    * get nationality based on country code from custom setting
    */
    public static string getNationlaity(string countrycode,string nationality){
    	string nationalityName = '';
    	if(String.IsNotBlank(countrycode)){
    		for(Country_Codes__c cc:[Select Name from Country_Codes__c where Alpha_3_Code__c=: countrycode]){
    			nationalityName = cc.Name;
    		}
    	}
    	if(String.IsNotBlank(nationality)){
    		for(Country_Codes__c cc:[Select ID,Alpha_3_Code__c from Country_Codes__c where Name=: nationality]){
    			nationalityName = cc.Nationality__c;
    		}
    	}
    	return nationalityName;
    }
    
    //Merul: This type cast the field based on data type.
    public static void typeCastFlds(String fname,
                                    Object fval,
                                    Map<String,Object> mapFieldApiObject)
    {
        set<Schema.DisplayType> fldtyp = new set<Schema.DisplayType>();
        Schema.DescribeSObjectResult dsr = getDescObj('HexaBPM__Service_Request__c');
        Map<String,Schema.SObjectField> mapDesc = getFldDecsMap(dsr);
        system.debug('######### mapDesc  '+mapDesc );
        
        if(fname != null  && fname  !='')
        {
            if(fval != null && fval!='' )
            {
                //objrequest.put(fname,Boolean.valueof(fval));
                system.debug('######### fname '+fname);
                Schema.DisplayType fldType = mapDesc.get(fname.toLowerCase()).getDescribe().getType();
                system.debug('########### fldType '+fldType);

                switch on fldType
                {

                    when BOOLEAN 
                    {       
                        mapFieldApiObject.put(fname,Boolean.valueof(fval));
                    }   
                    when DOUBLE
                    {       
                        mapFieldApiObject.put(fname,Double.valueof(fval));
                    }
                    when DATE 
                    {       
                        mapFieldApiObject.put(fname,Date.valueof(fval+''));
                    }
                    /* Not required as of now for passport.
                    when DATETIME 
                    {       
                        
                        Datetime dt = Datetime.valueOf(fval.replace('T',' ')).addHours(4);
                        mapFieldApiObject.put(fname,dt);
                        
                    }*/
                    when else 
                    {         
                        mapFieldApiObject.put(fname,String.valueof(fval));
                    }

                }

            }
            else
            {
                mapFieldApiObject.put(fname,null);
            }
        }
    }
    
    
    
    
    public class RequestWrapper 
    {
        @AuraEnabled public Id flowId { get; set; }
        @AuraEnabled public Id pageId {get;set;}
        @AuraEnabled public Id srId { get; set; }
        @AuraEnabled public String fieldApi { get; set; }
        @AuraEnabled public String fieldVal { get; set; }
        @AuraEnabled public String cmdActionId { get; set; }
        @AuraEnabled public HexaBPM__Service_Request__c objRequest{get;set;}
        @AuraEnabled public Map<String,String> objRequestMap { get; set; }

        

       
        public RequestWrapper()
        {}
    }

    public class ResponseWrapper 
    {
       
        @AuraEnabled public String pageActionName{get;set;}
        @AuraEnabled public List<String> passportData {get;set;}
        @AuraEnabled public  Map<String,Object> mapFieldApiObject {get;set;}
        
       
        public ResponseWrapper()
        {}
    }
    
    
    
}