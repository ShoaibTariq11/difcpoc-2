global class SearchAndReplaceContact implements Database.Batchable < sObject > {

    global final String Query;


    global SearchAndReplaceContact(String q) {

        Query = q;
        system.debug('Query---'+Query);
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List < Contact > scope) {
        for (Contact ObjContact: scope) {
            ObjContact.email = ObjContact.email != NULl ?ObjContact.email+'.invalid' : ObjContact.email;
            ObjContact.AssistantPhone = '+971569803616';
            ObjContact.HomePhone = '+971569803616';
            ObjContact.MobilePhone = '+971569803616';
            ObjContact.OtherPhone = '+971569803616';
            ObjContact.Phone = '+971569803616';
            ObjContact.CONTACT_PHONE__c = '+971569803616';
            ObjContact.Residence_Phone__c = '+971569803616';
            ObjContact.Work_Phone__c = '+971569803616';
            
            ObjContact.email = ObjContact.email != NULl ?ObjContact.email+'.invalid' : ObjContact.email;
            ObjContact.Account_Principle_User_Email__c = ObjContact.Account_Principle_User_Email__c != NULl ?ObjContact.Account_Principle_User_Email__c+'.invalid' : ObjContact.Account_Principle_User_Email__c;
            ObjContact.Additional_Email__c = ObjContact.Additional_Email__c != NULl ?ObjContact.Additional_Email__c+'.invalid' : ObjContact.Additional_Email__c;
        }
        system.debug('scope---'+scope);
        database.update(scope,false) ;
    }

    global void finish(Database.BatchableContext BC) {}
}