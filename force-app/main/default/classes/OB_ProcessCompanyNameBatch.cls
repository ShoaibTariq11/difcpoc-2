/*
Author: Merul
*/
global class  OB_ProcessCompanyNameBatch implements Database.Batchable<sObject>  
{
    
    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
        

        String query = 'SELECT Id,Name,Date_of_Expiry__c,Application__c,Application__r.HexaBPM__Customer__c FROM Company_Name__c WHERE Application__c != NULL AND Date_of_Expiry__c != NULL';
        return Database.getQueryLocator(query);
        
    }

    global void execute(Database.BatchableContext BC, List<Company_Name__c> scope) 
    {
        
        createNameReservRenewalSR(scope);
        markRenewalSRAndStepExpired(scope);
    }  

   // Finish Method
   global void finish(Database.BatchableContext BC) 
   {
   }
   
   // After expiry on if SR is still open, mark SR and its Step as Expired.
   public void markRenewalSRAndStepExpired( List<Company_Name__c> scope )
   {
        
        try 
        {
            Map<Id,Company_Name__c> mapSRIdCmpyName = new Map<Id,Company_Name__c>();
            Map<Id,HexaBPM__Service_Request__c> mapParentSRIdSR = new Map<Id,HexaBPM__Service_Request__c>();
            List<HexaBPM__Service_Request__c> lstSRToUpdate = new List<HexaBPM__Service_Request__c>();
            List<HexaBPM__Step__c> lstStepToUpdate = new List<HexaBPM__Step__c>();
            List<Company_Name__c> lstCompNameToUpdate = new List<Company_Name__c>();
            List<Company_Name__c> cmpyNameToProcess = new List<Company_Name__c>();
            
            HexaBPM__Status__c stepExpiredStatus;
            HexaBPM__SR_Status__c srExpiredStatus;

            //Getting Name_Reservation_Renewal SR Template.
            List<HexaBPM__SR_Template__c> nameRenewalSRTemplateLst = [
                                                                            SELECT id,
                                                                                HexaBPM__SR_RecordType_API_Name__c 
                                                                            FROM HexaBPM__SR_Template__c
                                                                            WHERE HexaBPM__SR_RecordType_API_Name__c = 'Name_Reservation_Renewal'
                                                                            LIMIT 1
                                                                        ];

            for(Company_Name__c cmpName : scope)
            {
                // if the company name is expired.
                if(cmpName.Date_of_Expiry__c != NULL && system.today() > cmpName.Date_of_Expiry__c )
                {
                    // get all the Related Principal/AOR SR
                    mapSRIdCmpyName.put(cmpName.Application__c, cmpName ); 
                    cmpyNameToProcess.add(cmpName); 
                }
            }

            system.debug('@@@@@@@@@ mapSRIdCmpyName '+mapSRIdCmpyName);

            // getting all related Name_Reservation_Renewal which are OPEN....means payment is not done after Expiry.
            for(HexaBPM__Service_Request__c nameResSR : [
                                                                SELECT id,
                                                                    HexaBPM__Customer__c,
                                                                    HexaBPM__SR_Template__c,
                                                                    recordTypeId,
                                                                    HexaBPM__Parent_SR__c,
                                                                    HexaBPM__Auto_Submit__c,
                                                                    HexaBPM__External_SR_Status__c,
                                                                    (
                                                                        SELECT id,
                                                                                HexaBPM__Current_Status__c,
                                                                                HexaBPM__Status__c,
                                                                                HexaBPM__Status__r.Name,
                                                                                Step_Template_Code__c 
                                                                        FROM HexaBPM__Steps_SR__r 
                                                                        WHERE HexaBPM__Status__r.Name = 'Open'
                                                                            AND  Step_Template_Code__c = 'NAME_RESERVATION_RENEWAL'
                                                                    )
                                                                FROM HexaBPM__Service_Request__c
                                                                WHERE HexaBPM__Parent_SR__c IN: mapSRIdCmpyName.keySet() 
                                                                AND HexaBPM__IsClosedStatus__c = false
                                                                AND HexaBPM__Is_Rejected__c = false
                                                                AND HexaBPM__IsCancelled__c = false
                                                                AND HexaBPM__SR_Template__c = :nameRenewalSRTemplateLst[0].Id
                                                        ] 
                )
                {
                        mapParentSRIdSR.put(nameResSR.HexaBPM__Parent_SR__c,nameResSR);
                }

                system.debug('@@@@@@@@@ mapParentSRIdSR '+mapParentSRIdSR);

                //if there is open SR is there,then query Expiry status of HexaBPM__SR_Status__c, HexaBPM__Status__c
                if(mapParentSRIdSR.size() > 0)
                {
                    // get expired HexaBPM__Status__c
                    for(HexaBPM__Status__c stepStatusTemp : [
                                                                SELECT id,
                                                                    HexaBPM__Code__c 
                                                                FROM HexaBPM__Status__c
                                                                WHERE HexaBPM__Code__c = 'EXPIRED'
                                                                LIMIT 1
                                                            ]
                    )
                    {
                        stepExpiredStatus = stepStatusTemp;
                    }
                    // get expired HexaBPM__SR_Status__c
                    for(HexaBPM__SR_Status__c srStatusTemp : [
                                                                SELECT id,
                                                                    HexaBPM__Code__c 
                                                                FROM HexaBPM__SR_Status__c
                                                                WHERE HexaBPM__Code__c = 'EXPIRED'

                                                                LIMIT 1
                                                            ]
                    )
                    {
                        srExpiredStatus = srStatusTemp;
                    }

                }  

                system.debug('@@@@@@@@@ stepExpiredStatus '+stepExpiredStatus);
                system.debug('@@@@@@@@@ srExpiredStatus '+srExpiredStatus);

                // if there exist any open SR then mark SR and related Step as expired.
                for(Company_Name__c cmpName : cmpyNameToProcess )
                {
                    // if contains open Name_Reservation_Renewal SR....then make SR ad Step as expired.
                    if( mapParentSRIdSR.containsKey(cmpName.Application__c) )
                    {
                        system.debug('@@@@@@@@@ mapParentSRIdSR value '+mapParentSRIdSR.get(cmpName.Application__c));
                        // get open name rewneal SR
                        HexaBPM__Service_Request__c nameResSR = mapParentSRIdSR.get(cmpName.Application__c);
                        //mark it as expired.
                        nameResSR.HexaBPM__External_SR_Status__c = srExpiredStatus.Id;
                        nameResSR.HexaBPM__Internal_SR_Status__c = srExpiredStatus.Id;
                        lstSRToUpdate.add(nameResSR);

                        system.debug('@@@@@@@@@ nameResSR.HexaBPM__Steps_SR__r '+nameResSR.HexaBPM__Steps_SR__r);

                        if( nameResSR.HexaBPM__Steps_SR__r != NULL 
                                && nameResSR.HexaBPM__Steps_SR__r.size() > 0)
                        {
                            
                                for( HexaBPM__Step__c step : nameResSR.HexaBPM__Steps_SR__r)
                                {
                                    step.HexaBPM__Status__c = stepExpiredStatus.Id;    
                                    step.HexaBPM__Closed_Date__c  = system.today(); 
                                    step.HexaBPM__Closed_Date_Time__c = system.now();
                                    lstStepToUpdate.add(step);
                                }
                            
                        }

                        //marking company Name as unreserved.
                        cmpName.Reserved_Name__c = false;
                        lstCompNameToUpdate.add(cmpName);
                    }
                }

                if(lstSRToUpdate.size() > 0)
                {
                    update lstSRToUpdate;
                }

                if(lstStepToUpdate.size() > 0)
                {
                    update lstStepToUpdate;
                }

                if(lstCompNameToUpdate.size() > 0)
                {
                    update lstCompNameToUpdate;
                }



        } 
        catch (Exception e) 
        {
            
            Log__c objLog = new Log__c();
            objLog.Description__c = 'Exception OB_ProcessCompanyNameBatch.markRenewalSRAndStepExpired()'+e.getLineNumber()+' '+e.getMessage();
            objLog.Type__c = 'OB_ProcessCompanyNameBatch';
            insert objLog;
        }    


   }


   // create NameReservRenewalSR based on company name.
   public void createNameReservRenewalSR( List<Company_Name__c> scope )
   {
       
    try 
    {
            system.debug('$$$$$$$$$ scope '+scope);
            List<Company_Name__c> cmpyNameToProcess = new List<Company_Name__c>();
            List<HexaBPM__Service_Request__c> lstSR = new List<HexaBPM__Service_Request__c>();
            Map<Id,Company_Name__c> mapSRIdCmpyName = new Map<Id,Company_Name__c>();
            Map<Id,HexaBPM__Service_Request__c> mapParentSRIdSR = new Map<Id,HexaBPM__Service_Request__c>();

            //Getting Name_Reservation_Renewal SR Template.
            List<HexaBPM__SR_Template__c> nameRenewalSRTemplateLst = [
                                                                            SELECT id,
                                                                                HexaBPM__SR_RecordType_API_Name__c 
                                                                            FROM HexaBPM__SR_Template__c
                                                                            WHERE HexaBPM__SR_RecordType_API_Name__c = 'Name_Reservation_Renewal'
                                                                            LIMIT 1
                                                                        ];

            string recordTypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','Name_Reservation_Renewal');
            if( nameRenewalSRTemplateLst.size() > 0 
                            && String.isNotBlank(recordTypeId))
            {
                    // map of relted In princlpal / AOR 
                    for(Company_Name__c cmpName : scope )
                    {
                        // if 10 days before expiry.
                        if( cmpName.Date_of_Expiry__c != NULL )
                        {
                            Date tenDayBefore = cmpName.Date_of_Expiry__c - 10;
                            Date today = system.today();
                            if( today <= cmpName.Date_of_Expiry__c 
                                                    && tenDayBefore >= today)
                            {
                                mapSRIdCmpyName.put( cmpName.Application__c , cmpName );  
                                cmpyNameToProcess.add(cmpName);
                            }
                        }
                       
                    }
        
                    // getting all related Name_Reservation_Renewal which are OPEN.
                    for(HexaBPM__Service_Request__c nameResSR : [
                                                                        SELECT id,
                                                                                HexaBPM__Customer__c,
                                                                                HexaBPM__SR_Template__c,
                                                                                recordTypeId,
                                                                                HexaBPM__Parent_SR__c,
                                                                                HexaBPM__Auto_Submit__c
                                                                        FROM HexaBPM__Service_Request__c
                                                                    WHERE HexaBPM__Parent_SR__c IN: mapSRIdCmpyName.keySet() 
                                                                        AND HexaBPM__IsClosedStatus__c = false
                                                                        AND HexaBPM__Is_Rejected__c = false
                                                                        AND HexaBPM__IsCancelled__c = false
                                                                        AND HexaBPM__SR_Template__c = :nameRenewalSRTemplateLst[0].Id
                                                                ] 
                        )
                    {
                        mapParentSRIdSR.put(nameResSR.HexaBPM__Parent_SR__c,nameResSR);
                    }

                    // if related name remewal SR does not exist then create new one
                    for(Company_Name__c cmpName : cmpyNameToProcess )
                    {
                                // if no related name  name remewal SR found    
                                if( !mapParentSRIdSR.containsKey(cmpName.Application__c) )
                                {
                                        //create Name renewal SR.
                                        HexaBPM__Service_Request__c sr = new HexaBPM__Service_Request__c();
                                        sr.HexaBPM__Customer__c = cmpName.Application__r.HexaBPM__Customer__c;
                                        sr.HexaBPM__SR_Template__c = nameRenewalSRTemplateLst[0].Id;
                                        sr.recordTypeId = recordTypeId;
                                        sr.HexaBPM__Parent_SR__c = cmpName.Application__c;
                                        sr.HexaBPM__Auto_Submit__c = true;
                                        lstSR.add(sr);
                                }
                        //}

                        
                    }

                    if( lstSR.size() > 0 )
                    {
                                insert lstSR;
                    }
            } 
    }
    catch (Exception e) 
    {
        Log__c objLog = new Log__c();
        objLog.Description__c = 'Exception OB_ProcessCompanyNameBatch.createNameReservRenewalSR'+e.getLineNumber()+' '+e.getMessage();
        objLog.Type__c = 'OB_ProcessCompanyNameBatch';
        insert objLog;
    }
    
   }

}