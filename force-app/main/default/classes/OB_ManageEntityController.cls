/**
 * Description : Controller for OB_ManageEntity Component 
 *
 * ****************************************************************************************
 * History :
 * [06.FEB.2020] Prateek Kadkol - Code Creation
 */
public without sharing class OB_ManageEntityController {

    @AuraEnabled
    public static RespondWrap fetchEntityData()  {
    
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
    
    
    
        User loggedInUser = new User();
    
        for(User UserObj : [Select contactid,Contact.AccountId from User where Id = :UserInfo.getUserId() LIMIT 1]) {
            loggedInUser =  UserObj;
        }

        //get relatedsr
        string recordId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('New User Registration').getRecordTypeId();
        for(HexaBPM__Service_Request__c relSr : [SELECT id,HexaBPM__Contact__c from HexaBPM__Service_Request__c WHERE recordTypeId =:recordId AND HexaBPM__Contact__c  =: loggedInUser.contactid 
        AND HexaBPM__IsClosedStatus__c = false AND HexaBPM__IsCancelled__c = false AND 
        HexaBPM__Is_Rejected__c = false LIMIT 1]){
            respWrap.relEntitySr = relSr;
        }
        
        
              //get navigationUrl
	for(HexaBPM__Page_Flow__c pfFlow : [SELECT id , (select id from HexaBPM__Pages__r WHERE HexaBPM__Page_Order__c = 1 LIMIT 1) from HexaBPM__Page_Flow__c WHERE HexaBPM__Record_Type_API_Name__c = 'New_User_Registration' LIMIT 1]){
		respWrap.pageFlowURl = '?flowId='+pfFlow.Id+'&pageId='+pfFlow.HexaBPM__Pages__r[0].Id;
    }
    
        OB_AgentEntityUtils.RespondWrap utilsWrapper = new OB_AgentEntityUtils.RespondWrap();
        utilsWrapper = OB_AgentEntityUtils.getRelatedAccounts(loggedInUser.ContactID);
    
        respWrap.relatedAccounts = utilsWrapper.relatedAccounts;
        respWrap.loggedInUser = loggedInUser;
    
        return respWrap;
    }
    
    @AuraEnabled
    public static RespondWrap setContactMainAccount(String requestWrapParam)  {
    
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
    
    
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
        system.debug(reqWrap);
    
    
    
    
    
        try {
    
            contact conToUpdate = new Contact(id=reqWrap.contactId);
            conToUpdate.AccountId = reqWrap.mainAccIdToSet;
            update conToUpdate;
    
        }catch(DMLException e) {
    
            string DMLError = e.getdmlMessage(0) + '';
            if(DMLError == null) {
                DMLError = e.getMessage() + '';
            }
            respWrap.errorMessage = DMLError;
        }
    
    
    
        return respWrap;
    }

    @AuraEnabled
    public static RespondWrap createNewEntitySr(String requestWrapParam)  {
    
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
    
    
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
                        
    
        string recordId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('New User Registration').getRecordTypeId();
		string srTemplateId = '';
        contact conObj = new contact();
        
        for(contact relCont : [SELECT id,firstname,lastname , email, phone  from contact where id =: reqWrap.contactID]){
            conObj = relCont;
        }
       
    
		for(HexaBPM__SR_Template__c srTempObj : [SELECT id from HexaBPM__SR_Template__c WHERE Name = 'New User Registration'
		AND HexaBPM__SR_RecordType_API_Name__c = 'New_User_Registration' AND HexaBPM__Active__c = true]){
			srTemplateId = srTempObj.Id;
		}

		if( srTemplateId!= null && recordId !=null){

			HexaBPM__Service_Request__c srObj = new HexaBPM__Service_Request__c();
			srObj.recordTypeId = recordId;
			srObj.HexaBPM__SR_Template__c = srTemplateId;
			//srObj.HexaBPM__Customer__c = reqWrap.accId;
            srObj.HexaBPM__Contact__c = reqWrap.contactID;
            srObj.Created_By_Agent__c = true;
            srObj.first_name__c = conObj.firstname; 
            srObj.last_name__c = conObj.lastname; 
            srObj.hexabpm__email__c = conObj.email;
            srObj.hexabpm__send_sms_to_mobile__c = conObj.phone;  


			try{
			
			 insert srObj;
             respWrap.insertedSr =srObj;
			 
			 
			}catch(DMLException e) {
			
			 string DMLError = e.getdmlMessage(0) + '';
			 if(DMLError == null) {
			 DMLError = e.getMessage() + '';
			 }
			 respWrap.errorMessage = DMLError; 
			}

		}else{
			respWrap.errorMessage = 'SR Record Type Missing';
		}		     
		return respWrap;
    }
    
    // ------------ Wrapper List ----------- //
    
    public class RequestWrap {
    
    @AuraEnabled public String contactId;
    @AuraEnabled public String mainAccIdToSet;      
    
    }
    
    public class RespondWrap {
     
	@AuraEnabled public object insertedSr;
    @AuraEnabled public list<Account> relatedAccounts = new list<Account>();
    @AuraEnabled public string pageFlowURl;
    @AuraEnabled public User loggedInUser;
    @AuraEnabled public string errorMessage;
    @AuraEnabled public HexaBPM__Service_Request__c relEntitySr ;                                                                                                                                                                                                                                    
    }
}