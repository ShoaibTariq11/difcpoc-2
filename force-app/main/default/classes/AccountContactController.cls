public class AccountContactController{
      
    public ID recID{get;set;}
    public AccountContactController(ApexPages.standardController con)
    {
       recID = ApexPages.currentPage().getParameters().get('id');
    }
    
    @AuraEnabled
    public static AccountContactListWrapper getAccountWithContacts(String accountId)
    {
       
        AccountContactListWrapper accWrapper = new AccountContactListWrapper();
       
        //Account Information
        List<Account> accListnew = [SELECT Id,Building_Name__c,Office__c,Street__c,Name,Tax_Registration_Number_TRN__c,Lease_Types__c,BillingState, Website, 
                                    Phone,Legal_Type_of_Entity__c,License_Issue_Date__c,Next_Renewal_Date__c,Exempted_from_UBO_Regulations__c,City__c,
                                    Registration_License_No__c FROM Account where ID=:accountId];
        
       //Relationship Details
        Relationship__c relationship = new Relationship__c();
        List<Relationship__c> lstRelationship = new List<Relationship__c>();
        for(Relationship__c eachRelationship:[Select Id,Relationship_Type_formula__c,Active__c,Object_Account__r.Registration_No__c,Object_Account__r.Postal_Code__c,Object_Account__r.Emirate_State__c,Object_Account__r.City__c,Object_Account__r.Country__c,Object_Account__r.PO_Box__c,Object_Account__r.Registration_License_No__c,Subject_Account__c,Relationship_Type__c,Object_Account__c,Object_Account__r.Name,Object_Contact__r.Name,Object_Contact__r.Are_you_a_resident_in_the_U_A_E__c,Object_Contact__r.FirstName,Object_Contact__r.LastName,
                                              Object_Contact__r.Birthdate,Object_Contact__r.Nationality__c,Object_Contact__r.Details_on_source_of_income__c,
                                              Object_Contact__r.Passport_No__c,Object_Contact__r.Passport_Expiry_Date__c,Object_Contact__r.Is_this_individual_a_PEP__c,
                                              Object_Contact__r.Address_Line_1_HC__c,Object_Contact__r.Address_Line_2_HC__c,Object_Contact__r.City__c,Object_Contact__r.Emirate_State_Province__c,
                                              Object_Contact__r.Postal_Code__c,Object_Contact__r.Country__c,Object_Contact__r.Phone,Object_Contact__r.Email,Subject_Account__r.Legal_Type_of_Entity__c from Relationship__c where Subject_Account__c=:accountId and Active__c=true])
        {
            //if(eachRelationship.Relationship_Type__c!=NULL )
            //{
                
                //Public and Private Limited Company Information
                if(eachRelationship.Subject_Account__r.Legal_Type_of_Entity__c =='LTD' || eachRelationship.Subject_Account__r.Legal_Type_of_Entity__c =='Public Company')
                {
                    if(eachRelationship.Object_Account__c!=NULL && eachRelationship.Relationship_Type_formula__c =='Shareholder')
                    {
                        //Shareholder body corporate
                        accWrapper.dirsectionName = 'Shareholder Body Corporate';
                        relationship = new Relationship__c();
                        relationship = eachRelationship;
                        lstRelationship.add(relationship);
                        accWrapper.bodyCorporateList = lstRelationship;
                    }
                    
                    else if(eachRelationship.Object_Contact__c!=NULL && eachRelationship.Relationship_Type_formula__c =='Shareholder')
                    {
                        //Shareholder individual
                        accWrapper.sectionName = 'Individual Shareholder';
                        relationship = new Relationship__c();
                        relationship = eachRelationship;
                        lstRelationship.add(relationship);
                        accWrapper.rshipList1 = lstRelationship;
                    }
                    
                    else if(eachRelationship.Object_Account__c!=NULL && eachRelationship.Relationship_Type_formula__c =='Director')
                    {
                        //Director
                        accWrapper.dirsectionName = 'Director Body Corporate';
                        relationship = new Relationship__c();
                        relationship = eachRelationship;
                        lstRelationship.add(relationship);
                        accWrapper.bodyCorporateList = lstRelationship;
                    }
                    else if(eachRelationship.Object_Contact__c!=NULL && eachRelationship.Relationship_Type_formula__c =='Director')
                    {
                        //Director
                        accWrapper.sectionName = 'Individual Body Corporate';
                        relationship = new Relationship__c();
                        relationship = eachRelationship;
                        lstRelationship.add(relationship);
                        accWrapper.bodyCorporateList = lstRelationship;
                        
                    }
                }
                //end of Public and Private Limited Company Information
                
                //Start General Partner
                else if(eachRelationship.Subject_Account__r.Legal_Type_of_Entity__c =='GP' && eachRelationship.Relationship_Type_formula__c == 'General Partner')
                {
                    //General Partner
                    if(eachRelationship.Object_Account__c!=NULL && eachRelationship.Object_Contact__c==NULL)
                    {
                        accWrapper.dirsectionName = 'General Partner body corporate';
                        relationship = new Relationship__c();
                        relationship = eachRelationship;
                        lstRelationship.add(relationship);
                        accWrapper.bodyCorporateList = lstRelationship;
                    }
                    else if(eachRelationship.Object_Account__c==NULL && eachRelationship.Object_Contact__c!=NULL)
                    {
                        accWrapper.sectionName = 'General Partner Individual';
                        relationship = new Relationship__c();
                        relationship = eachRelationship;
                        lstRelationship.add(relationship);
                        accWrapper.rshipList1 = lstRelationship;
                    }
                    
                }
                
                //End General Partner
                
                //Start Limited Partnership
                else if(eachRelationship.Subject_Account__r.Legal_Type_of_Entity__c =='LP' && eachRelationship.Relationship_Type_formula__c == 'Partner')
                {
                    //Only Partner details
                    if(eachRelationship.Object_Account__c!=NULL && eachRelationship.Object_Contact__c==NULL)
                    {
                        accWrapper.dirsectionName = 'Partner body corporate';
                        relationship = new Relationship__c();
                        relationship = eachRelationship;
                        lstRelationship.add(relationship);
                        accWrapper.bodyCorporateList = lstRelationship;
                    }
                    else if(eachRelationship.Object_Account__c==NULL && eachRelationship.Object_Contact__c!=NULL)
                    {
                        accWrapper.sectionName = 'Partner Individual';
                        relationship = new Relationship__c();
                        relationship = eachRelationship;
                        lstRelationship.add(relationship);
                        accWrapper.rshipList1 = lstRelationship;
                    }
                }
                //End Limited partnership
               
                //Start LLP
                else if(eachRelationship.Subject_Account__r.Legal_Type_of_Entity__c =='LLP' && eachRelationship.Relationship_Type_formula__c == 'Member')
                {
                    //Member details
                    if(eachRelationship.Object_Account__c!=NULL && eachRelationship.Object_Contact__c==NULL)
                    {
                        accWrapper.dirsectionName = 'Member body corporate';
                        relationship = new Relationship__c();
                        relationship = eachRelationship;
                        lstRelationship.add(relationship);
                        accWrapper.bodyCorporateList = lstRelationship;
                    }
                    else if(eachRelationship.Object_Account__c==NULL && eachRelationship.Object_Contact__c!=NULL)
                    {
                        accWrapper.sectionName = 'Member Individual';
                        relationship = new Relationship__c();
                        relationship = eachRelationship;
                        lstRelationship.add(relationship);
                        accWrapper.rshipList1 = lstRelationship;
                    }
                }
                // End LLP
                
                // Start Foundation
                else if(eachRelationship.Subject_Account__r.Legal_Type_of_Entity__c =='Foundation' )
                {
                    //Founder and Council Member details
                    if(eachRelationship.Relationship_Type__c == 'Founder')
                    {
                        if(eachRelationship.Object_Account__c!=NULL && eachRelationship.Object_Contact__c==NULL)
                        {
                            accWrapper.dirsectionName = 'Founder body corporate';
                            relationship = new Relationship__c();
                            relationship = eachRelationship;
                            lstRelationship.add(relationship);
                            accWrapper.bodyCorporateList = lstRelationship;
                        }
                        else if(eachRelationship.Object_Account__c==NULL && eachRelationship.Object_Contact__c!=NULL)
                        {
                            accWrapper.sectionName = 'Founder Individual';
                            relationship = new Relationship__c();
                            relationship = eachRelationship;
                            lstRelationship.add(relationship);
                            accWrapper.rshipList1 = lstRelationship;
                        }
                    }
                    else if(eachRelationship.Relationship_Type__c == 'Council Member')
                    {
                        if(eachRelationship.Object_Account__c!=NULL && eachRelationship.Object_Contact__c==NULL)
                        {
                            accWrapper.dirsectionName = 'Council Member body corporate';
                            relationship = new Relationship__c();
                            relationship = eachRelationship;
                            lstRelationship.add(relationship);
                            accWrapper.bodyCorporateList = lstRelationship;
                        }
                        else if(eachRelationship.Object_Account__c==NULL && eachRelationship.Object_Contact__c!=NULL)
                        {
                            accWrapper.sectionName = 'Council Member Individual';
                            relationship = new Relationship__c();
                            relationship = eachRelationship;
                            lstRelationship.add(relationship);
                            accWrapper.rshipList1 = lstRelationship;
                        }
                    }
                }
                
                //End Foundation
                
                //Start NPIO
                else if(eachRelationship.Subject_Account__r.Legal_Type_of_Entity__c =='NPIO' && eachRelationship.Relationship_Type_formula__c == 'Founding Member')
                {
                    //Founding Member
                    if(eachRelationship.Object_Account__c!=NULL && eachRelationship.Object_Contact__c==NULL)
                    {
                        accWrapper.dirsectionName = 'Founding Member body corporate';
                        relationship = new Relationship__c();
                        relationship = eachRelationship;
                        lstRelationship.add(relationship);
                        accWrapper.bodyCorporateList = lstRelationship;
                    }
                    else if(eachRelationship.Object_Account__c==NULL && eachRelationship.Object_Contact__c!=NULL)
                    {
                        accWrapper.sectionName = 'Founding Member Individual';
                        relationship = new Relationship__c();
                        relationship = eachRelationship;
                        lstRelationship.add(relationship);
                        accWrapper.rshipList1 = lstRelationship;
                    }
                }
                //End NPIO
                
                //Start Recognized Company
                else if(eachRelationship.Subject_Account__r.Legal_Type_of_Entity__c =='Recognized Company')
                {
                    //Foreign Company 
                    if(eachRelationship.Relationship_Type__c == 'Founder')
                    {
                        if(eachRelationship.Object_Account__c!=NULL && eachRelationship.Object_Contact__c==NULL)
                        {
                            accWrapper.dirsectionName = 'Foreign Company corporate';
                            relationship = new Relationship__c();
                            relationship = eachRelationship;
                            lstRelationship.add(relationship);
                            accWrapper.bodyCorporateList = lstRelationship;
                        }
                        else if(eachRelationship.Object_Account__c==NULL && eachRelationship.Object_Contact__c!=NULL)
                        {
                            accWrapper.sectionName = 'Foreign Company individual';
                            relationship = new Relationship__c();
                            relationship = eachRelationship;
                            lstRelationship.add(relationship);
                            accWrapper.rshipList1 = lstRelationship;
                        }
                    }
                    
                    //Director
                    else if(eachRelationship.Relationship_Type__c == 'Director')
                    {
                        if(eachRelationship.Object_Account__c!=NULL && eachRelationship.Object_Contact__c==NULL)
                        {
                            accWrapper.dirsectionName = 'Foreign Company body corporate';
                            relationship = new Relationship__c();
                            relationship = eachRelationship;
                            lstRelationship.add(relationship);
                            accWrapper.bodyCorporateList = lstRelationship;
                        }
                        else if(eachRelationship.Object_Account__c==NULL && eachRelationship.Object_Contact__c!=NULL)
                        {
                            accWrapper.sectionName = 'Foreign Company Individual';
                            relationship = new Relationship__c();
                            relationship = eachRelationship;
                            lstRelationship.add(relationship);
                            accWrapper.rshipList1 = lstRelationship;
                        }
                    }
 
                }
                //End Recognized Company
                
                //Start RP
                else if(eachRelationship.Subject_Account__r.Legal_Type_of_Entity__c =='RP' && eachRelationship.Relationship_Type_formula__c == 'Foreign Parnership')
                {
                    //Foreign Partnership
                    if(eachRelationship.Object_Account__c!=NULL && eachRelationship.Object_Contact__c==NULL)
                    {
                        accWrapper.dirsectionName = 'Foreign Partnership body corporate';
                        relationship = new Relationship__c();
                        relationship = eachRelationship;
                        lstRelationship.add(relationship);
                        accWrapper.bodyCorporateList = lstRelationship;
                    }
                    else if(eachRelationship.Object_Account__c==NULL && eachRelationship.Object_Contact__c!=NULL)
                    {
                        accWrapper.sectionName = 'Foreign Partnership Individual';
                        relationship = new Relationship__c();
                        relationship = eachRelationship;
                        lstRelationship.add(relationship);
                        accWrapper.rshipList1 = lstRelationship;
                    }

                }
                //End RP
                
                //Start RLP
                else if(eachRelationship.Subject_Account__r.Legal_Type_of_Entity__c =='RLP' && eachRelationship.Relationship_Type_formula__c == 'Foreign Limited Partnership')
                {
                    //Foreign Limited Partnership
                    if(eachRelationship.Object_Account__c!=NULL && eachRelationship.Object_Contact__c==NULL)
                    {
                        accWrapper.dirsectionName = 'Foreign Limited Partnership body corporate';
                        relationship = new Relationship__c();
                        relationship = eachRelationship;
                        lstRelationship.add(relationship);
                        accWrapper.bodyCorporateList = lstRelationship;
                    }
                    else if(eachRelationship.Object_Account__c==NULL && eachRelationship.Object_Contact__c!=NULL)
                    {
                        accWrapper.sectionName = 'Foreign Limited Partnership Individual';
                        relationship = new Relationship__c();
                        relationship = eachRelationship;
                        lstRelationship.add(relationship);
                        accWrapper.rshipList1 = lstRelationship;
                    }
                }
                //End RLP
               
               //Start RLLP 
                else if(eachRelationship.Subject_Account__r.Legal_Type_of_Entity__c =='RLLP' && eachRelationship.Relationship_Type_formula__c == 'Foreign Limited Liability Partnership')
                {
                    //Foreign Limited Liability Partnership 
                    if(eachRelationship.Object_Account__c!=NULL && eachRelationship.Object_Contact__c==NULL)
                    {
                        accWrapper.dirsectionName = 'Foreign Limited Liability Partnership body corporate';
                        relationship = new Relationship__c();
                        relationship = eachRelationship;
                        lstRelationship.add(relationship);
                        accWrapper.bodyCorporateList = lstRelationship;
                    }
                    else if(eachRelationship.Object_Account__c==NULL && eachRelationship.Object_Contact__c!=NULL)
                    {
                        accWrapper.sectionName = 'Foreign Limited Liability Partnership Individual';
                        relationship = new Relationship__c();
                        relationship = eachRelationship;
                        lstRelationship.add(relationship);
                        accWrapper.rshipList1 = lstRelationship;
                    }
                }
          
                //End RLLP
               
           /** else 
            {
                accWrapper.sectionName = 'No Data Found';
                accWrapper.dirsectionName ='test dats';
                relationship = new Relationship__c();
                relationship = eachRelationship;
                lstRelationship.add(relationship);
                accWrapper.rshipList1 = lstRelationship;
            }**/
                
            //}
        }
        

        //Portal user Infomration
        User usr = new User();
        List<User> lstUser = new List<User>();
        for(User eachUser: [Select Id,Name,Contact.FirstName,Contact.LastName,Contact.Email,Contact.Phone,IsActive,ContactID,Contact.Name from User where IsActive=True and Contact.AccountID=:accountId and ContactID!=Null and Contact.Recordtype.DeveloperName ='Portal_User'])
       {
           if(eachUser.ID!=NULL)
           {
               usr = new User();
               accWrapper.portalUserSectionName = 'Portal User Details';
               usr = eachUser;
               lstUser.add(usr);
               accWrapper.userList = lstUser;
           }
        
       }
        
        // SR Documents
        SR_Doc__c  docs = new SR_Doc__c ();
        List <SR_Doc__c> docList = new List <SR_Doc__c>();
        for(SR_Doc__c  eachSRDoc : [Select ID,Document_Master__r.Doc_ID__c,Preview_Document__c,Doc_ID__c,Name,Document_type__c,Customer__c from SR_Doc__c  where Customer__c =:accountId LIMIT 10])
        {
                docs = new SR_Doc__c ();
              docs = eachSRDoc;
            system.debug('@@@@@@'+docs);
                docList.add(docs);
                accWrapper.srDocuList = docList;
            system.debug('@@@@@@'+ accWrapper.srDocuList);
            
            
        }
        
        
        Attachment attRec = new Attachment();
        List<Attachment> attlist = new List<Attachment>();
        for(Attachment eachAtt : [SELECT Id, Name, ParentId, Parent.Type FROM Attachment where Parent.Type = 'SR_Doc__c' and ParentId in:accWrapper.srDocuList])
        {
            attRec = new Attachment();
            attRec= eachAtt;
            attlist.add(attRec);
            accWrapper.listAttach = attlist;
             system.debug('***!!!!@@@@@@'+ accWrapper.listAttach);
        }
        
        
        
          
        // Shareholder Information
        Shareholder_Detail__c shds = new Shareholder_Detail__c();
        List<Shareholder_Detail__c> lstShd = new List<Shareholder_Detail__c>();
        List<Shareholder_Detail__c> lstDirShd = new List<Shareholder_Detail__c>();
        Map<Id,String> mapShareName = new Map<Id,String>();
        Map<Id,String> mapSharePer = new Map<Id,String>();
        for(Shareholder_Detail__c shareHolderInfo : [Select Id, Name,Shareholder_Contact__r.FirstName,Shareholder_Contact__r.LastName,Account__c,ownership__c,Account_Share__r.Name,Shareholder_Contact__c,Relationship_Type__c,Relationship__c,Shareholder_Account__c from Shareholder_Detail__c where Account__c =:accountID and Relationship_Type__c='Shareholder'])
        {
            shds = new Shareholder_Detail__c();
            
            if(shareHolderInfo.Shareholder_Contact__c!=Null)
            {
                accWrapper.shdSectionName ='DSSSirDetails';
                shds = new Shareholder_Detail__c();
                shds = shareHolderInfo;
                lstShd.add(shds);
                //accWrapper.shdList = lstShd;
                accWrapper.shdIndList = lstShd;
                
                system.debug('#####' +accWrapper.shdList);
            }
            
            else if(shareHolderInfo.Shareholder_Account__c!=Null)
            {
                
                accWrapper.shdSectionName ='DirDetails';
                shds = new Shareholder_Detail__c();
                shds = shareHolderInfo;
                lstDirShd.add(shds);
                //accWrapper.shdList = lstDirShd;
                accWrapper.shdDirList = lstDirShd;
                system.debug('#####' +accWrapper.shdDirList);
            }
            
            
           
        }
        
        //Shareholder_Detail__c shds = new Shareholder_Detail__c();
        //List<Shareholder_Detail__c> lstShd = new List<Shareholder_Detail__c>();
        //Map<Id,String> mapShareName = new Map<Id,String>();
        //Map<Id,String> mapSharePer = new Map<Id,String>();
        //for(Shareholder_Detail__c shareHolderInfo : [Select Id, Name,Account__c,ownership__c,Account_Share__r.Name,Shareholder_Contact__c,Shareholder_Contact__r.Name,Relationship_Type__c,Relationship__c,Shareholder_Account__c,Shareholder_Account__r.Name from Shareholder_Detail__c where Account__c =:accountId])
        
        List<Account> accList = [SELECT Id,Building_Name__c,Office__c,Street__c,Name,Tax_Registration_Number_TRN__c,Lease_Types__c,BillingState, Website, 
                                 Phone,Legal_Type_of_Entity__c,License_Issue_Date__c,Next_Renewal_Date__c,Exempted_from_UBO_Regulations__c,City__c,
                                 Registration_License_No__c,Emirate__c,Initial_Contact_Email__c,PO_Box__c,
                                    (SELECT Id, FirstName, LastName, Email,Phone From Contacts where Recordtype.DeveloperName ='Portal_User'),(Select ID,Subject_Account__r.Name,Name,Object_Contact__r.Name,Object_Contact__r.Birthdate,Object_Contact__r.Nationality__c,Object_Contact__r.Passport_No__c,Object_Contact__r.Passport_Expiry_Date__c,Object_Contact__r.Address_Line_1_HC__c,Object_Contact__r.Address_Line_2_HC__c,Relationship_Type__c from Primary_Account__r),(Select id, name,Service_Type__c from Service_Requests__r where Service_Type__c = 'Application for Incorporation / Registration' )
                                    FROM Account WHERE Id =: accountId];
     
        if(!accList.isEmpty())
        {
            accWrapper.accRecord = accList[0];
            accWrapper.contactList = accList[0].Contacts;
            accWrapper.SRList = accList[0].Service_Requests__r;
            //accWrapper.rshipList = accList[0].Primary_Account__r;
            accWrapper.contactCount = accList[0].Contacts.size();
        }
        return accWrapper;
    }
     
    // wrapper class with @AuraEnabled and {get;set;} properties 
    public class AccountContactListWrapper{
        @AuraEnabled
        public Account accRecord{get;set;}
        @AuraEnabled
        public String sectionName{get;set;}
        @AuraEnabled
        public String dirsectionName{get;set;}
        @AuraEnabled
        public String shdSectionName{get;set;}
        @AuraEnabled
        public String portalUserSectionName{get;set;}
        @AuraEnabled
        public List<User> userList{get;set;}//Portal Users
        @AuraEnabled
        public List<Contact> contactList{get;set;}
        @AuraEnabled
        public List<Account> accList1{get;set;}
        @AuraEnabled
        public List<Service_request__c> SRList{get;set;}
        @AuraEnabled
        public List<Relationship__c> rshipList{get;set;}
        @AuraEnabled
        public List<Relationship__c> rshipList1{get;set;}
        @AuraEnabled
        public List<SR_Doc__c> srDocuList{get;set;}
        @AuraEnabled
        public List<Attachment> listAttach{get;set;}
        @AuraEnabled
        public List<Shareholder_Detail__c> shdList{get;set;}
        @AuraEnabled
        public List<Shareholder_Detail__c> shdIndList{get;set;}
        @AuraEnabled
        public List<Shareholder_Detail__c> shdDirList{get;set;}
        @AuraEnabled
        public List<Relationship__c> bodyCorporateList{get;set;}
        @AuraEnabled
        public Integer contactCount{get;set;}
    }
}