global class RescheduleAppointmentBatch implements Database.Batchable<sObject> {

global Database.QueryLocator start(Database.BatchableContext BC) {
String query = 'SELECT Id, Appointment_StartDate_Time__c, Appointment_EndDate_Time__c,'+
                    'Step__r.createdDate, Step__r.sr__c '+
                    'FROM Appointment__C '+
                    'WHERE Appointment_StartDate_Time__c = Today '+
                    'AND Medical_Process_Completed__c = false '+
                    'AND Step_Status__c != \'Closed\''+
                    'limit 1';
        return Database.getQueryLocator(query);
    }
global void execute(Database.BatchableContext BC, List<Appointment__C> paramListAppointment) {
}
global void finish(Database.BatchableContext BC) {
        
}
    /**************************************comment out **************************
    String query = 'SELECT Id, Appointment_StartDate_Time__c, Appointment_EndDate_Time__c, '+
                    'Step__r.createdDate, Step__r.sr__c '+
                    'FROM Appointment__C '+
                    'WHERE Appointment_StartDate_Time__c = Today '+
                    'AND Medical_Process_Completed__c = false '+
                    'AND Step_Status__c != \'Closed\' ';

    public RescheduleAppointmentBatch( String paramQuery ) {
        
        if( paramQuery != '' ){
            query = paramQuery;
        }
        
        if( Test.isRunningTest() ){
            query = query + 'LIMIT 1';
        }   
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<Appointment__C> paramListAppointment) {

        for(Appointment__C AppointmentObj : paramListAppointment ){
            Appointment__c Appntment = new Appointment__c();
            List<service_request__c> SR = [select id,Express_Service__c,name,first_Name__c,Last_Name__c from service_request__c where id =: AppointmentObj.Step__r.sr__c limit 1];

            DateTime Cdt = AppointmentObj.step__r.createdDate.addDays(1);

            Date createdDate = Date.newinstance(cdt.year(),cdt.month(), cdt.day());

            Date ExpressDate = Date.newinstance(AppointmentObj.step__r.createdDate.year(),AppointmentObj.step__r.createdDate.month(),AppointmentObj.step__r.createdDate.Day());
            system.debug('SR '+ SR);
            system.debug('SR[0].Express_Service__c  '+ SR[0].Express_Service__c );
            if(SR.size()>0 && SR[0].Express_Service__c == false){
                BusinessHours BH =[SELECT Id,FridayStartTime,MondayEndTime,MondayStartTime,SaturdayEndTime,SaturdayStartTime,SundayEndTime,SundayStartTime,ThursdayEndTime,ThursdayStartTime,TuesdayEndTime,TuesdayStartTime,WednesdayStartTime,WednesdayEndTime FROM BusinessHours where name  ='GS- Medical Appointment Normal' limit 1];  
                Datetime nextStartDate_time = BusinessHours.nextStartDate(BH.Id,createdDate);    

                Date nxtBusinessDate = Date.newinstance(nextStartDate_time.year(),nextStartDate_time.month(), nextStartDate_time.day());

                // order by createddate DESC limit 1
                List<Appointment_Schedule_Counter__c> ListAppointmentCounter=[select id,Appointment_Time__c,Lunch_Time__c,Lunch_Duration__c,Application_Type__c from Appointment_Schedule_Counter__c where DAY_ONLY(Appointment_Time__c) = : nxtBusinessDate and Application_Type__c = 'Normal'];

                if(ListAppointmentCounter.size() < = 0 ){     
                    AppointmentCreationHandler.getStartTimeofDay(BH ,nxtBusinessDate);

                    List<Appointment__c> appointment = [select id,Appointment_StartDate_Time__c,Appointment_EndDate_Time__c from Appointment__c where Appointment_StartDate_Time__c =:  Appntment.Appointment_StartDate_Time__c and Service_Type__c = 'Normal'];

                        if(appointment.size()>0){

                            Appntment.Appointment_StartDate_Time__c = appointment[0].Appointment_EndDate_Time__c ;   
                            Appntment.Appointment_EndDate_Time__c  = appointment[0].Appointment_EndDate_Time__c.addMinutes(AppointmentCreationHandler.mins) ;           
                    }
                }

                if(ListAppointmentCounter.size()> 0){ 
                    AppointmentCreationHandler.appointmentBookingMethod(BH,ListAppointmentCounter);
                }

                Appntment.Step__c = AppointmentObj.step__r.id;       
                Appntment.Service_Request__c=SR[0].id;
                Appntment.Subject__c = SR[0].name +'-'+SR[0].first_Name__c + ''+ SR[0].Last_Name__c +'-'+'Normal';
                List<step__c> stp = [select id,Step_Code__c,Biometrics_Required__c from step__c where step_code__c = 'Emirates ID Registration is completed' and sr__r.id = : SR[0].id limit 1];
                if(stp.size()>0){
                    Appntment.Bio_Metrics__c = stp[0].Biometrics_Required__c;
                }

                Database.SaveResult saveResultList;
                Appntment = AppointmentCreationHandler.Appntment;
                Appntment.Id = AppointmentObj.Id;
                if(Appntment.Appointment_StartDate_Time__c != null){
                    saveResultList = Database.update(Appntment, false);
                }
                
                if ( saveResultList != null && saveResultList.isSuccess()) {            
                    AppointmentCreationHandler.updateScheduleCounter(Appntment,AppointmentCreationHandler.isnextday,ListAppointmentCounter);   
                }        
            }else if(SR.size()>0 && SR[0].Express_Service__c == true){

                BusinessHours BH =[SELECT Id,FridayStartTime,MondayEndTime,MondayStartTime,SaturdayEndTime,SaturdayStartTime,SundayEndTime,SundayStartTime,ThursdayEndTime,ThursdayStartTime,TuesdayEndTime,TuesdayStartTime,WednesdayStartTime,WednesdayEndTime FROM BusinessHours where name  ='GS- Medical Appointment Express' limit 1];
                Datetime nextStartDate_time = BusinessHours.nextStartDate(BH.Id,ExpressDate);    

                Date nxtBusinessDate = Date.newinstance(nextStartDate_time.year(),nextStartDate_time.month(), nextStartDate_time.day());
                List<Appointment_Schedule_Counter__c> ListAppointmentCounter = new List<Appointment_Schedule_Counter__c>();

                datetime dt = AppointmentObj.step__r.createdDate.addMinutes(AppointmentCreationHandler.ExpressMins);

                string lunchHoursNormal = system.label.Normal_Lunch_time;
                string lunchHoursExpress = system.label.Express_Lunch_Hours;

                Integer LunchMinsExpress = Integer.valueof(system.label.Express_Lunch_time);
                Integer LunchMinsExpress1 = Integer.valueof(system.label.Express_lunch_Time1);

                Boolean isWithin= BusinessHours.isWithin(bh.id, dt);
                if(isWithin){   

                    DateTime dt1 ;
                    string sHours = string.valueOf(dt.hour());

                    integer mins1 = dt.addMinutes(AppointmentCreationHandler.ExpressMins).minute();
                    if(AppointmentCreationHandler.ExpressLunchHour == sHours){                     
                        if(mins1 > LunchMinsExpress ){   
                            dt1 = dt.addMinutes(AppointmentCreationHandler.ExpressLunchDuration); 
                        }                     
                    }else if(lunchHoursNormal == sHours ){

                        if(mins1 < LunchMinsExpress1) {
                            integer minutes1 = LunchMinsExpress1-mins1;                        
                            dt1 = dt.addMinutes(minutes1); 
                        }    
                    }else{

                        dt1 = dt;
                    }

                    List<Appointment__c> ListAppointment =[select id,Applicantion_Type__c,Appointment_StartDate_Time__c,Appointment_EndDate_Time__c,Appointment_StartTime_Mins__c,Appointment_Hours1__c, Step__c from Appointment__c where Appointment_StartDate_Time__c = : dt1 and Service_Type__c ='Express'];

                    if(ListAppointment.size()>0){

                    Appntment.Appointment_StartDate_Time__c = ListAppointment[0].Appointment_EndDate_Time__c;            
                    Appntment.Appointment_EndDate_Time__c  = ListAppointment[0].Appointment_EndDate_Time__c.addMinutes(AppointmentCreationHandler.Mins) ;

                    }else {

                    Appntment.Appointment_StartDate_Time__c = dt1 ;            
                    Appntment.Appointment_EndDate_Time__c  = dt1.addMinutes(AppointmentCreationHandler.Mins) ;

                    }
                }else{

                    Date nxtBusinessDateExpress =  ExpressDate.addDays(1);
                    Datetime nextStartDate_time1 = BusinessHours.nextStartDate(BH.Id,nxtBusinessDateExpress);

                    Date nxtBusinessDate1 = Date.newinstance(nextStartDate_time1.year(),nextStartDate_time1.month(), nextStartDate_time1.day());

                    ListAppointmentCounter=[select id,Appointment_Time__c,Lunch_Time__c,Lunch_Duration__c,Application_Type__c from Appointment_Schedule_Counter__c where DAY_ONLY(Appointment_Time__c) = : nxtBusinessDate1 and Application_Type__c = 'Express'];

                    if(ListAppointmentCounter.size() < =0 ){                    
                        AppointmentCreationHandler.getStartTimeofDay(BH ,nxtBusinessDate1);
                        List<Appointment__c> appointment = [select id,Appointment_StartDate_Time__c,Appointment_EndDate_Time__c from Appointment__c where Appointment_StartDate_Time__c =:  Appntment.Appointment_StartDate_Time__c and Service_Type__c = 'Express'];
                        if(appointment.size()>0){
                            Appntment.Appointment_StartDate_Time__c = appointment[0].Appointment_EndDate_Time__c ;   
                            Appntment.Appointment_EndDate_Time__c  = appointment[0].Appointment_EndDate_Time__c.addMinutes(AppointmentCreationHandler.mins) ;           
                        }
                    }else if(ListAppointmentCounter.size() > 0){
                        AppointmentCreationHandler.appointmentBookingMethod(BH,ListAppointmentCounter);
                    }
                }

                Appntment.Step__c = AppointmentObj.step__r.id;       
                Appntment.Service_Request__c=SR[0].id;
                Appntment.Subject__c = SR[0].name +'-'+SR[0].first_Name__c + ''+ SR[0].Last_Name__c + '-'+'Express';
                List<step__c> stp = [select id,Step_Code__c,Biometrics_Required__c from step__c where step_code__c = 'Emirates ID Registration is completed' and sr__r.id = : SR[0].id limit 1];
                
                if(stp.size()>0){
                    Appntment.Bio_Metrics__c = stp[0].Biometrics_Required__c;
                }
                Database.SaveResult saveResultList;
                Appntment = AppointmentCreationHandler.Appntment;
                Appntment.Id = AppointmentObj.Id;
                if(Appntment.Appointment_StartDate_Time__c!=null){
                    saveResultList = Database.update(Appntment, false);
                }

                if ( saveResultList != null && saveResultList.isSuccess()) {            
                    AppointmentCreationHandler.updateScheduleCounterExpress(Appntment,AppointmentCreationHandler.isnextday,ListAppointmentCounter);   
                }        
            }

        }
    }
    
    public void finish(Database.BatchableContext BC) {
        
    }
    *******************************/
}