/*****************************************************************
Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date           Updated By      Description
 ----------------------------------------------------------------------------------------              
 V1.1    06-Jan-2016    Rajil           Added the communityUserRoles to enable/disable the services for DIFC mobile portal.
 V1.2    29-03-2016     Swati           Added the logic to seggregate company current and pipeline issues
 V1.3    19/06/2016     Sravan          Commented the lines created in V1.2 and added below as the method signatures had been changed in GSHelper class as per ticket # 2570
 V1.4    21-08-2016     Claude          Added logic for Contractors per Phase 2 of DIFC FIT-OUT
 V1.5    01-12-2016     Claude          Revised logic for DIFC Registered companies
 V1.6    14-03-2017     Claude          Added logic to immediately deduct 2500 for establishment card in the display (3806)
 v1.7    29-05-2017     Claude          Added flag for PSA Calculation
 V1.9    05-06-2017     Sravan          Added flag to skip pricing consideration for Account balance when price is for establishment card and for GSO
 V1.10   24-09-2014     Sravan          Added code to deduct refund amount from portal balance tkt # 4164
 V1.11   24-12-2017     Danish          Condition to access VAT
 V1.12   25th Feb-2019  Mudasir         Updated logic to remove the consideration of Fit-Out service request price Items for the client portal balance
 V1.13   02-07-2019     Azfer           Updated the SOQL to ignore the refund price line item from the Available Portal Balance. 
 *****************************************************************/
public without sharing class CompanyOverviewCls {
    
    public String CompanyName                           {get;set;}
    public decimal PortalBalance                        {get;set;}
    public string PortalBalanceFormatted                {set;get;}
    public string PSADepositFormatted                   {get;set;}
    public string PSAAvailableFormatted                 {get;set;}
    
    public String companyType                           {get;set;}
    
    public decimal PSAAvailable                         {get;set;}    
    public decimal PSADeposit                           {get;set;}
    public Integer VisaQuota                            {get;set;}
    public Integer VisaIssued                           {get;set;}
    public Integer PipelineQuota                        {get;set;}
    public Date LastUpdatedDate                         {get;set;}
    public DateTime LastUpdatedDateTime                 {get;set;}
    public String CallType                              {get;set;}
    public Integer Hours                                {get;set;}
    public Integer Minutes                              {get;set;}
    public Integer Seconds                              {get;set;}
    
    //V1.2
    public integer sponsoredCurrent                     {get;set;}
    public integer GCCCurrent                           {get;set;}
    public integer secondedCurrent                      {get;set;}
    public integer temporaryCurrent                     {get;set;}
    
    public integer sponsoredPipeline                    {get;set;}
    public integer GCCPipeline                          {get;set;}
    public integer secondedPipeline                     {get;set;}
    public integer temporaryPipeline                    {get;set;}
    //End
    
    public String CustomerId                            {get;set;}
    public String ReceiptPrefix                         {get;set;}
    public String BPNumber                              {get;set;}
    public string PaymentChecqueAmount_ElemId           {set;get;}            
    
    public String BalanceUpdateFailedErrorMessage       {set;get;}
    public boolean BalanceFetchSuccessfull              {set;get;}
    public boolean renderGIINSection                    {get;set;}     // Boolean field added by Shabbir Ahmed to display GIIN hyperlink or not. 12-Feb-15
    public boolean FATCAEligibilitytoNonRegComp         {get;set;}     // Boolean field added by Shabbir Ahmed to display FATCA links for Non-Regulated companies. 16-Aug-2015
    
    public Boolean IsCompanyServices                    {get;set;}
    
    //added by Ravi on 11th Oct,2015 - RoRP
    public Boolean IsRorpClient                         {get;set;}
    
    //Added by: Rajil
    //Purpose: Enable/Disable the service based on user community role for Client portal mobile app.
    public string communityUserRoles                    {get;set;}
    
    //V1.4 - Claude - Added new variable to store company name of contractor
    public String contractorName                        {get;set;}
    
    //V1.4 - Claude - Added flag to check if the user is a contractor
    public Boolean isFitOutContractor                   {get;set;}
    
    //V1.4 - Claude - Added if the Contractor is visible
    public Boolean isPortalBalanceVisible               {get;set;}
    public Boolean isEventOrganizer                     {get;set;}
    
    public Boolean isVATOnly {get;set;}
    
    public CompanyOverviewCls(){
        
        // SOQL updated by Shabbir Ahmed DIFC to add GIIN field as well. 12-Feb-15
        // SOQL updated by Shabbir Ahmed DIFC to add FATCA Eligibility field. 16-Aug-15
        
        //V1.5 - Claude - Start
        // TODO: Will be replaced with Custom setting for DIFC internal projects
        String difcAccountBPNo = '0000631828';
        //V1.5 - Claude - End
        
        /* NOTE: Since we're using Userinfo.getUserId(), there is no need to bulkify
                 the query for the user details since the UserInfo class will always 
                 refer to the current running user. 
         */
        User objUser = [select Id,
                               AccountId,
                               Account_ID__c,
                               Contact.AccountId,
                               Contact.Account.Is_Registered__c,            // V1.4 - Claude - new field for contractors 
                               Contact.Account.RecordType.DeveloperName,    // V1.4 - Claude - new field for contractors
                               Contact.Contractor__r.RecordType.DeveloperName,
                               Contact.Contractor__r.Is_Registered__c,      // V1.4 - Claude - new field for contractors
                               Contact.Contractor__c,                       // V1.4 - Claude - new field for contractors
                               Contact.Contractor__r.Name,                  // V1.4 - Claude - new field for contractors
                               Profile.Name,                                // V1.4 - Claude - added profile to distinguish contractors from client ysers
                               ProfileId,
                               Contact.Role__c,
                               Contact.Account.BP_No__c,                    // V1.5 - Claude
                               Contact.Account.Company_Type__c,
                               Contact.Account.GIIN__c,
                               Contact.Account.FATCA_Eligibility__c,
                               Contact.Account.Name,
                               Contact.Account.Roc_Status__c,
                               Can_Access_VAT_Info__c, // V1.11
                               Community_User_Role__c from User where Id=:Userinfo.getUserId()];   
        
        /* Determins if the user is a fit-out contractor */
        isFitOutContractor = (String.isNotBlank(objUser.Contact.Contractor__c) && !objUser.Contact.Contractor__c.equals(objUser.Contact.AccountId) ) || 
                                objUser.Contact.Account.RecordType.DeveloperName.equals('Contractor_Account');
        
        /* Get the client name */                               
        companyName = objUser.Contact.Account.Name;
        
        customerId = isFitOutContractor && String.isNotBlank(objUser.Contact.Contractor__c) ? objUser.Contact.Contractor__c : objUser.AccountId;
        
        /* Get the current contractor name */
        contractorName = isFitOutContractor && String.isNotBlank(objUser.Contact.Contractor__r.Name) ? objUser.Contact.Contractor__r.Name : objUser.Contact.Account.Name;
        
        /* V1.5 - Claude - controls the visibility of PSA  */
        isEventOrganizer = objUser.Contact.Account.RecordType.DeveloperName.equals('Event_Account') || 
                            (String.isNotBlank(objUser.Contact.Account.Roc_Status__c) && 
                                objUser.ProfileId.equals(Label.Contractor_Profile_Id)); 

        /* Exclusively for Contractors under the organizer/client (DFIC - Registered Companies) */
        Boolean isContractorUnderOrganizer = (String.isNotBlank(objUser.contact.contractor__c) && 
                                                objUser.Contact.Contractor__r.Is_Registered__c && 
                                                ( String.isNotBlank(objUser.Contact.Account.Roc_Status__c) ||  
                                                    ( String.isNotBlank(objUser.Contact.Account.BP_No__c) && 
                                                    objUser.Contact.Account.BP_No__c.equals(difcAccountBPNo) ) ) );
        
        /* Exclusively for Contractors under the event organizer (NON-DIFC) */
        Boolean isEventContractor =  (String.isNotBlank(objUser.contact.contractor__c) && 
                                        objUser.Contact.Contractor__r.Is_Registered__c &&               
                                        objUser.Contact.Account.RecordType.DeveloperName.equals('Event_Account'));
                                        
        /* Exclusively for Event organizers and/or contractors who login as themselves */
        Boolean isActualEventOrganizer = ( (objUser.Contact.Account.RecordType.DeveloperName.equals('Contractor_Account')  ||
                                                objUser.Contact.Account.RecordType.DeveloperName.equals('Event_Account') ) && 
                                                objUser.contact.Account.Is_Registered__c && (String.isBlank(objUser.Contact.Contractor__c) || 
                                                objUser.Contact.AccountId.equals(objUser.Contact.Contractor__c)) ); 
        
        /* Exclusively for all NON-FIT-OUT users //V1.5 - Claude - for DIFC Registered 'ROC Status'  */
        Boolean isDifcRegisteredAccount = (!objUser.Contact.Account.RecordType.DeveloperName.equals('Contractor_Account') && 
                                            !objUser.Contact.Account.RecordType.DeveloperName.equals('Event_Account') && 
                                            String.isBlank(objUser.contact.Contractor__c) && 
                                            !objUser.ProfileId.equals(Label.Contractor_Profile_Id));  

        /* Exclusively for Event Oganizers under the client  */
        Boolean isClientEventOrganizer =  ( ( String.isNotBlank(objUser.Contact.Account.Roc_Status__c) || 
                                                ( String.isNotBlank(objUser.Contact.Account.BP_No__c) && 
                                                objUser.Contact.Account.BP_No__c.equals(difcAccountBPNo) ) ) && 
                                                    objUser.ProfileId.equals(Label.Contractor_Profile_Id) && 
                                                    !objUser.Contact.Contractor__r.RecordType.DeveloperName.equals('Contractor_Account') && 
                                                    !objUser.Contact.Contractor__r.RecordType.DeveloperName.equals('Event_Account'));

        isPortalBalanceVisible = isContractorUnderOrganizer || isEventContractor || isActualEventOrganizer || isDifcRegisteredAccount || isClientEventOrganizer;

        companyType = objUser.Contact.Account.Company_Type__c;
        
        FATCAEligibilitytoNonRegComp = objUser.Contact.Account.FATCA_Eligibility__c;
            
        renderGIINSection = string.isBlank(objUser.Contact.Account.GIIN__c);
        
        //added by Ravi on 24th May
        IsCompanyServices = objUser.Community_User_Role__c != null && objUser.Community_User_Role__c.contains('Company Services');
            
        //Added by: Rajil
        //Purpose: Enable/Disable the service based on user community role for Client portal mobile app.
        if(String.isNotBlank(objUser.Community_User_Role__c)) communityUserRoles = objUser.Community_User_Role__c; 
        
        //Danish
        //24-12-2017
        //to check VAT
        boolean isFitOutOnly = String.isNotBlank(objUser.Contact.Account.RecordType.DeveloperName) && objUser.Contact.Account.RecordType.DeveloperName.equals('Contractor_Account');
        isVATOnly = objUser.Can_Access_VAT_Info__c;
            
        for(Account objAccount : [select Id,Name,BP_No__c,RecordType.DeveloperName from Account where Id=:CustomerId]){
            BPNumber = objAccount.BP_No__c;
            IsRorpClient = objAccount.RecordType.DeveloperName.equals('RORP_Account');
        }
        
        system.debug('CustomerId is : '+CustomerId);
        
        if(test.isRunningTest() == false){
            ReceiptPrefix = Cls_FieldElementId_Util.getObjectPrefix('Receipt__c');
            PaymentChecqueAmount_ElemId = Cls_FieldElementId_Util.getFieldElementId('Receipt__c','Amount'); 
        }
        
        BalanceUpdateFailedErrorMessage = 'An error has occured updating the balance. Please inform DIFC support team.';
        CallType = 'Class';
    }   
    
    public Pagereference AccountBalance(){
        
        try{
            
            system.debug('CallType is : '+CallType);
            
            PortalBalance = 0;
            PSADeposit = 0;
            PSAAvailable = 0;
            
            decimal dSRPriceItemNotSentAmt = 0;
            decimal dRecpNotSentAmount = 0;
            
            Boolean hasEstablishmentCard = false; // V1.6 - Claude - Added exemption for new establishment card
            Boolean isUpdate = false;
            
            if((LastUpdatedDate == null || (LastUpdatedDate.day() != system.today().day() || LastUpdatedDate.month() != system.today().month())) || CallType == 'Page'){
                
                AccountBalanceInfoCls.getAccountBalance(new list<string>{CustomerId}, '5000');
                
                System.debug('Balance is Updated.');
                
                isUpdate = true;
            }
            
            for(Account objAccount : [select Id,Index_Card_Status__c,Name,Portal_Balance__c,PSA_Deposit__c,Balance_Updated_On__c from Account where Id=:CustomerId]){
                    
                LastUpdatedDate = (objAccount.Balance_Updated_On__c != null) ? Date.valueOf(objAccount.Balance_Updated_On__c) : system.today();
                
                PortalBalance = (objAccount.Portal_Balance__c != null) ? objAccount.Portal_Balance__c : 0;
                
                PSADeposit = (objAccount.PSA_Deposit__c != null) ? objAccount.PSA_Deposit__c : 0;
                
                LastUpdatedDateTime = (objAccount.Balance_Updated_On__c != null) ? Date.valueOf(objAccount.Balance_Updated_On__c) : system.now();
                
                //V1.6 - Claude - Start
                hasEstablishmentCard = String.isNotBlank(objAccount.Index_Card_Status__c) && !objAccount.Index_Card_Status__c.equals('Not Available') && !objAccount.Index_Card_Status__c.equals('Cancelled');
                //V1.6 - Claude - End
            }
            
            if(LastUpdatedDateTime != null){
                Minutes = LastUpdatedDateTime.Minute();
                Hours = LastUpdatedDateTime.hour();
                Seconds = LastUpdatedDateTime.second();
            }
            
            /* V1.4 - Claude - Start - New Query to distinguish fit-out from the rest */
            /* V1.12 - Mudasir updated the logic of the filter for the client portal balance */
            //V1.13 - Azfer  - Start - Updated the SOQL added a new and clause in the SOQL added Excluded_Price_Item__c = false
            String accountFilter = isFitOutContractor ? 'ServiceRequest__r.SR_Group__c =\'Fit-Out & Events\' AND ServiceRequest__r.Contractor__c = ' : 'ServiceRequest__r.SR_Group__c !=\'Fit-Out & Events\' AND ServiceRequest__r.Customer__c = ' ;
            String priceItemQuery = 'select Price__c,Total_Service_Amount__c from SR_Price_Item__c where ServiceRequest__c!=null and Price__c!=null and '+accountFilter+' \''+CustomerId+'\' and (Status__c=\'Blocked\' OR Status__c=\'Consumed\') and Add_in_Account_Balance__c=true AND Excluded_Price_Item__c = false';// Add_in_Account_Balance__c is a field that validates whether or not the price item should be counted for Account balance check V1.9
            /* V1.4 - Claude - End */
            //V1.13 - Azfer  - END

            for(SR_Price_Item__c objSRPriceItem : Database.query(priceItemQuery)){
                //dSRPriceItemNotSentAmt += objSRPriceItem.Price__c;
                dSRPriceItemNotSentAmt += objSRPriceItem.Total_Service_Amount__c; // added as part of vat
            }
            
            system.debug('PortalBalance is : '+PortalBalance);
            system.debug('dSRPriceItemNotSentAmt is : '+dSRPriceItemNotSentAmt);
            
            PortalBalance = PortalBalance - dSRPriceItemNotSentAmt;
            
            for(Receipt__c objReceipt : [select Amount__c from Receipt__c where Customer__c=:CustomerId and Amount__c!=null and Payment_Status__c='Success' and Pushed_to_SAP__c=false]){
                PortalBalance += objReceipt.Amount__c;
            }
            
            
            
            
             PortalBalance -=    RefundRequestUtils.getRefundAmount(CustomerId);  // V1.10            
        
        
            
            
            
            decimal[] VisaInfo = isUpdate == false ? GsHelperCls.CalculateVisa(CustomerId) : GsHelperCls.UpdateVisaQuota(CustomerId);
           
            // V1.3
            decimal[] currentInfoCount =  GsHelperCls.getCurrentEmployeeCount(new set<string>{CustomerId});
            decimal[] pipeLineInfoCount =  GsHelperCls.getPipelineEmployeeCount(new set<string>{CustomerId});
                      
            VisaQuota = Integer.valueOf(Math.round((VisaInfo[0]+VisaInfo[2])));
            VisaIssued = Integer.valueOf(VisaInfo[1]);
            PipelineQuota = Integer.valueOf(VisaInfo[3]);
            
            sponsoredCurrent = Integer.valueOf(currentInfoCount[0]);
            GCCCurrent = Integer.valueOf(currentInfoCount[1]);
            secondedCurrent = Integer.valueOf(currentInfoCount[2]);
            temporaryCurrent = Integer.valueOf(currentInfoCount[3]);
            
            sponsoredPipeline = Integer.valueOf(pipeLineInfoCount[0]);
            GCCPipeline = Integer.valueOf(pipeLineInfoCount[1]);
            secondedPipeline = Integer.valueOf(pipeLineInfoCount[2]);
            temporaryPipeline = Integer.valueOf(pipeLineInfoCount[3]);
            
            PSADeposit -=  RefundRequestUtils.getPSAAmount(CustomerId);   // V1.10
       
            
            
           
            
            PSAAvailable = (PSADeposit>= 125000) ? 125000 :  PSACalculation(hasEstablishmentCard);//( PSADeposit - (dAvaPSA + (VisaIssued*2500)) );
                
            //V1.6 - Claude - Start
            if(PSAAvailable < 0 && !hasEstablishmentCard) PSAAvailable = 0; // V1.8 - Claude - Added exemption for new establishment cards
            //V1.6 - Claude - End  
            
                          
                
            system.debug('PSAAvailable is : '+PSAAvailable);
            formatPortalBalance();
            BalanceFetchSuccessfull = true;
            
        } catch(Exception ex){
            
            system.debug('Exception is : '+ex.getMessage());
            BalanceFetchSuccessfull = false;
        }
        
        return null;
    }
    
    public decimal PSACalculation(Boolean hasEstablishmentCard){
        
        list<string> lstPSARecTypes = new list<string>{'DIFC_Sponsorship_Visa_New','Employment_Visa_from_DIFC_to_DIFC','Employment_Visa_Govt_to_DIFC','Visa_from_Individual_sponsor_to_DIFC'};
        
        map<Id,Service_Request__c> mapOpenSRs = new map<Id,Service_Request__c>([select Id,External_Status_Name__c from Service_Request__c where (NOT Occupation_GS__r.Name like '%STUDENT%') AND Customer__c=:CustomerId AND Record_Type_Name__c IN : lstPSARecTypes AND isClosedStatus__c != true AND Is_Rejected__c != true AND External_Status_Name__c != 'Cancelled' AND External_Status_Name__c != 'Rejected' AND External_Status_Name__c != 'Draft']);
        
        map<Id,Contact> mapActiveCons = new map<Id,Contact>([select Id from Contact where (NOT Job_Title__r.Name like '%STUDENT%') AND Id IN (select Object_Contact__c from Relationship__c where Subject_Account__c =:CustomerId AND Active__c = true AND Relationship_Type__c='Has DIFC Sponsored Employee')]);
        
        Integer iEmpCount = (mapActiveCons != null && !mapActiveCons.isEmpty()) ? mapActiveCons.size() : 0 ;
        
        system.debug('iEmpCount is : '+iEmpCount);
        
        if(!mapOpenSRs.isEmpty()) iEmpCount += mapOpenSRs.size();
        
        system.debug('iEmpCount is : '+iEmpCount);
        system.debug('PSADeposit is : '+PSADeposit);
        
        //V1.6 - Claude - Start
        if(hasEstablishmentCard) iEmpCount++;
        //V1.6 - Claude - End
        
        for(SR_Price_Item__c objSRItem : [select Id,Price__c from SR_Price_Item__c where 
                                                                                   Count_in_PSA__c = TRUE AND //V1.7 - Claude - Added PSA flag for SR Groups
                                                                                   ServiceRequest__r.Customer__c =: CustomerId AND (Status__c='Blocked' OR Status__c='Consumed') AND Pricing_Line__r.Product__r.Name = 'PSA']){ //AND ServiceRequest__r.External_Status_Name__c = 'Submitted'
            //dAvaPSA -= objSRItem.Price__c;
            PSADeposit += objSRItem.Price__c;
        }
        
        system.debug('PSADeposit is : '+PSADeposit);
        
          
        
        decimal dAvaPSA = (PSADeposit-(iEmpCount*2500)); // V1.6 - Claude - Added fixed 2500 as 'Establishment Card' // V1.10
        
        system.debug('dAvaPSA is : '+dAvaPSA);
        
        return dAvaPSA;
    }
    
    private void formatPortalBalance(){

        List<String> args = new String[]{'0','number','###,###,###,##0.00'};
        
        PortalBalanceFormatted = String.format(PortalBalance.format(), args); 
        PSADepositFormatted = String.format(PSADeposit.format(), args);
        PSAAvailableFormatted = String.format(PSAAvailable.format(), args);
        
        //Format function removes trailing zeros
        if(!PortalBalanceFormatted.contains('.'))
            PortalBalanceFormatted +='.00';
        if(!PSADepositFormatted.contains('.'))
            PSADepositFormatted +='.00';
        if(!PSAAvailableFormatted.contains('.'))
            PSAAvailableFormatted +='.00';
    }
}