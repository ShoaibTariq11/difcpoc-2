/*
    Author      : Leeba
    Date        : 30-March-2020
    Description : Test class for OB_EntityDropdownComponentController
    --------------------------------------------------------------------------------------
*/
@isTest
public class OB_EntityDropdownComponentControllerTest {
    
    
    public static testmethod void test1(){
    
     // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
    
    
        //crreate contact
        Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id,Firstname = 'testfirst', Email = 'test123@noemail.com', Passport_No__c ='1234',
                        Nationality__c = 'India');
                insert con; 
           
        
        
        HexaBPM__SR_Template__c objSrTemp = new HexaBPM__SR_Template__c();
        objSrTemp.Name = 'New User Registration';
        objSrTemp.HexaBPM__Active__c = true;
        objSrTemp.HexaBPM__SR_RecordType_API_Name__c = 'New_User_Registration';
        insert objSrTemp;
            
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('New_User_Registration', 1);
        insert listPageFlow;
        
        //create page
        HexaBPM__Page__c objPage = new HexaBPM__Page__c();
        objPage.HexaBPM__Page_Flow__c = listPageFlow[0].id;
        objPage.HexaBPM__Page_Order__c = 2;
        insert objPage;
    
    test.startTest();
          
          
            Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;      
                 
                          
                User user = new User(alias = 'test123', email='test123@noemail.com',
                        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                        localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                        ContactId = con.Id,
                        timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
               
                insert user;
            system.runAs(user){
            
            
            OB_EntityDropdownComponentController.RequestWrap reqWrapper = new OB_EntityDropdownComponentController.RequestWrap();
            reqWrapper.contactId = con.id;
            OB_EntityDropdownComponentController.RespondWrap restWrapTest = new OB_EntityDropdownComponentController.RespondWrap();
            OB_ManageAssosiatedEntityController.RequestWrap reqWrapper1 = new OB_ManageAssosiatedEntityController.RequestWrap();
            reqWrapper1.contactId = con.id;
            OB_ManageAssosiatedEntityController.RespondWrap restWrapTest1 = new OB_ManageAssosiatedEntityController.RespondWrap();
            
            String requestString = JSON.serialize(reqWrapper);
            String requestString1 = JSON.serialize(reqWrapper1);
            restWrapTest = OB_EntityDropdownComponentController.fetchRelatedEntities(requestString);
            //restWrapTest1 = OB_ManageAssosiatedEntityController.createNewEntitySr(requestString1);
            restWrapTest1 = OB_ManageAssosiatedEntityController.createNewEntitySr(requestString);
            restWrapTest1 = OB_EntityDropdownComponentController.createNewEntitySr(requestString);
            
             
            
            
        }
        
        test.stopTest();
    }

}