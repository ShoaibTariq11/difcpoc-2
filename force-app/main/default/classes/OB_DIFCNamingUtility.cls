/**
 * Description : used to maintain the naming convention accross the components
 *
 * ****************************************************************************************
 * History :
 * [31.OCT.2019] Prateek Kadkol - Code Creation
 * V1.1 14 May 2020   Sai             --Ticket 323
 */
public without sharing class OB_DIFCNamingUtility {

	public static string manageDIFCLable(HexaBPM__Service_Request__c srObj, string lableToCheck) {

		string leagalStructure = srOBj.legal_structures__c;
		string entitytype = srOBj.Type_of_Entity__c;

		if(entitytype != null) {

			if(lableToCheck.toLowerCase().contains('shareholder')) {

				return setShareholderLable(lableToCheck, entitytype);
			} 
			else if(lableToCheck.toLowerCase().contains('director')) {
				return setDirectorLable(lableToCheck, entitytype);
			} else {
				return lableToCheck;
			}

		} else {
			return lableToCheck;
		}



	}

	public static string setShareholderLable(string lableToCheck, string entitytype) {
	
		//V1.1  -- Updated the 	Recognized Company label to Shareholder
		Map<String, String> shareholderReplacementMap = new Map<String, String>{ 'General Partnership (GP)' => 'General Partner', 'Limited Partnership (LP)' => 'Partner', 'Limited Liability Partnership (LLP)' => 'Member', 'Foundation' => 'Founder', 'NPIO' => 'Founding Member', 'Recognized Company' => 'shareholder', 'Recognized Partnership (RP)' => 'Foreign Partnership', 'Recognized Limited Partnership (RLP)' => 'Foreign Limited Partnership', 'Recognized Limited Liability Partnership (RLLP)' => 'Foreign Limited Liability Partnership' };

		if(shareholderReplacementMap.containsKey(entitytype)) {
			return lableToCheck.replace('Shareholder', shareholderReplacementMap.get(entitytype));
		} else {
			return lableToCheck;
		}



	}

	public static string setDirectorLable(string lableToCheck, string entitytype) {

		Map<String, String> directorReplacementMap = new Map<String, String>{ 'Foundation' => 'Council Member' };

		if(directorReplacementMap.containsKey(entitytype)) {
			return lableToCheck.replace('Director', directorReplacementMap.get(entitytype));
		} else {
			return lableToCheck;
		}


	}

	public static string getActivityLable(string legalStructre) {

		Map<String, String> ActivityLableMap = new Map<String, String>{ 'Foundation' => 'Foundation Objects', 
			'Company' => 'Business Activities', 'Partnership' => 'Business Activities', 'Non Profit Incorporated Organisation (NPIO)' => 'Activities' };

		if(ActivityLableMap.containsKey(legalStructre)) {
			return  ActivityLableMap.get(legalStructre);
		} else {
			return 'Activities';
		}

	}

}