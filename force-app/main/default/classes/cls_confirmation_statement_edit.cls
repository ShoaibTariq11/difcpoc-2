/*********************************************************************************************************************
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.1    26-Nov-2019 Arun        Prescribed Company change 
 V1.2    15-dec-2019 Sai         #7917
 
**********************************************************************************************************************/


public with sharing class cls_confirmation_statement_edit {

public Service_Request__c SRData{get;set;}

    public string RecordTypeId;
    public map<string,string> mapParameters;
    public boolean isISPV{get;set;}
    public boolean isUBOExempted {get;set;}
    public Account ObjAccount{get;set;}
    public boolean IsFEY_Provided{get;set;}
    public boolean structuredFinancing{get;set;}
    public Company_Business_Hours__c businessHours{get;set;} 
  //  public string accQualifyingEntityType {get;set;} 
   // public string accQualifyingPurposeType {get;set;} 
   // public String prescribedCompany{get;set;}   //This Variable added for PrescribedCompany
   // public Boolean isPrescribedCompany{get;set;} //This Variable added for PrescribedCompany
    
  public String CustomerId;
   
   public   PageReference AnnualTurnover()
   {
      return null;
   } 
    public cls_confirmation_statement_edit(ApexPages.StandardController controller)
    {
        IsFEY_Provided=false;
        isISPV = FALSE; 
       // isPrescribedCompany = FALSE;
       // prescribedCompany = '';
        ObjAccount=new account();
        structuredFinancing = false;
        List<String> fields = new List<String> {'Financial_Year_End_mm_dd__c'};
        if (!Test.isRunningTest()) controller.addFields(fields); // still covered
        SRData=(Service_Request__c )controller.getRecord();
        Service_Request__c eachRequest = new Service_Request__c();
        if(SRData.id !=null){
            
            eachRequest = [SELECT Id,Customer__r.Name,Customer__r.License_Activity__c ,Customer__c,Customer__r.Exempted_from_UBO_Regulations__c,Customer__r.Qualifying_Purpose_Type__c,Customer__r.Qualifying_Type__c FROM Service_Request__c where Id=:SRData.id];
            ObjAccount.Exempted_from_UBO_Regulations__c=eachRequest.Customer__r.Exempted_from_UBO_Regulations__c;
            ObjAccount.Qualifying_Purpose_Type__c=eachRequest.Customer__r.Qualifying_Purpose_Type__c;
            ObjAccount.Qualifying_Type__c=eachRequest.Customer__r.Qualifying_Type__c;
            ObjAccount.License_Activity__c = eachRequest.Customer__r.License_Activity__c;   //V1.2
            ObjAccount.Name = eachRequest.Customer__r.Name;
        }
        if(SRData.Financial_Year_End_mm_dd__c!=null)
        ObjAccount.Financial_Year_End__c=SRData.Financial_Year_End_mm_dd__c;
        
        
    
        mapParameters = new map<string,string>();        
        if(apexpages.currentPage().getParameters()!=null)
        mapParameters = apexpages.currentPage().getParameters();              
       if(mapParameters.get('RecordType')!=null) 
       RecordTypeId= mapParameters.get('RecordType');      
            for(User objUsr:[select id,ContactId,Email,Phone,Contact.Account.Exempted_from_UBO_Regulations__c,Contact.Account.Legal_Type_of_Entity__c,Contact.AccountId,Contact.Account.Company_Type__c,
                             Contact.Account.Financial_Year_End__c,Contact.Account.Next_Renewal_Date__c,Contact.Account.Name,
                             Contact.Account.Qualifying_Type__c,Contact.Account.Qualifying_Purpose_Type__c,Contact.Account.Contact_Details_Provided__c,
                             Contact.Account.Sector_Classification__c,Contact.Account.License_Activity__c,Contact.Account.Is_Foundation_Activity__c  
                             from User where Id=:userinfo.getUserId()])
            {
              CustomerId = objUsr.Contact.AccountId;
             if(SRData.id==null){
                 
                
                SRData.Customer__c = objUsr.Contact.AccountId;
                SRData.RecordTypeId=RecordTypeId;
                SRData.Email__c = objUsr.Email;
                SRData.Legal_Structures__c=objUsr.Contact.Account.Legal_Type_of_Entity__c;
                SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                SRData.Entity_Name__c=objUsr.Contact.Account.Name;
                SRData.Expiry_Date__c=objUsr.Contact.Account.Next_Renewal_Date__c;
                SRData.CreatedDate = system.today();
                ObjAccount= objUsr.Contact.Account;
                SRData.Financial_Year_End_mm_dd__c=objUsr.Contact.Account.Financial_Year_End__c;
              }
              
             
          
                    
              //Below Logic added by Sai, For 6368  
             if(objUsr.Contact.Account.Sector_Classification__c !=null && objUsr.Contact.Account.Sector_Classification__c ==Label.Intermediate_special_purpose_vehicle){
                isISPV = TRUE;           }
             if(objUsr.Contact.Account.Financial_Year_End__c!=null)
                IsFEY_Provided=true;
            //SRData.Customer__r = objUsr.Contact.Account;
            
                    //system.debug('!!@@@@'+eachAccount.Qualifying_Type__c);
                    
            }
            
            for(License_Activity__c eachActivity:[SELECT Activity__r.Name FROM License_Activity__c where Account__c=:CustomerId]){
	        	if(eachActivity.Activity__r.Name.equalsIgnoreCase('Qualifying Purpose of Structured Financing')){
	        		structuredFinancing = true;
	        	}
        	}
            /*
            Account eachAccount = new Account();
            system.debug('!!@@@@'+eachRequest.Customer__c);
                if(eachRequest.Customer__c !=null){
                    eachAccount = [SELECT Exempted_from_UBO_Regulations__c,Sub_Legal_Type_of_Entity__c,Qualifying_Type__c
                                     FROM Account where ID=:eachRequest.Customer__c];
                    if(eachAccount.Exempted_from_UBO_Regulations__c !=null){
                        isUBOExempted = eachAccount.Exempted_from_UBO_Regulations__c;
                    }
                    
                }
                */
}


public  PageReference SaveConfirmation()
{

   
boolean isvalid=true;

 Boolean hasOpenCompliances = ![SELECT Id FROM Compliance__c WHERE Status__c = 'Created' AND Start_Date__c > TODAY AND Account__c = :SRData.Customer__c AND Name = 'Confirmation Statement'].isEmpty();
 
        if(hasOpenCompliances) {ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'You can only submit the Confirmation Statement one month before expiry.'));isvalid=false;                }       
//Confirm_Change_of_Trading_Name__c
//Financial_Year_End_mm_dd__c

if(SRData.Entity_Name_checkbox__c==false)//I hereby confirm that the company profile and officer details available on the DIFC Client Portal are accurate and up to date as of
{
    isvalid=false;
    SRData.addError('Please confirm all checkboxes in the Confirmation and Undertaking section');
    
}


// Start Below Logic added for PrescribedCompany /v 1.1
if(ObjAccount.Qualifying_Type__c!=null && ObjAccount.Qualifying_Type__c !='Qualifying Purpose' && SRData.Processing_Personal_Data__c ==false) 
{
    isvalid=false;
    SRData.addError('Please confirm all checkboxes in the Confirmation and Undertaking section');
    
}

 //V1.2
 if(ObjAccount.License_Activity__c !=null && ObjAccount.License_Activity__c.contains('316') && SRData.Use_Registered_Address__c ==false) 
{
    isvalid=false;
    SRData.addError('Please confirm all checkboxes in the Confirmation and Undertaking section');
    
}
 
 
/*
if(ObjAccount.Qualifying_Purpose_Type__c!=null && SRData.On_behalf_of_identical_name__c ==false && ObjAccount.Qualifying_Purpose_Type__c=='Structured Financing') 
{
    isvalid=false;
    SRData.addError('Please confirm all checkboxes in the Confirmation and Undertaking section');
    
}*/

// End


if(ObjAccount.Contact_Details_Provided__c!=true && SRData.id==null)//I hereby confirm that the company profile and officer details available on the DIFC Client Portal are accurate and up to date as of
{ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'You must submit Update details, add or remove management details before proceeding with this request.'));          isvalid=false;}

//Below Logic updated by Sai for #6333
if(ObjAccount.Exempted_from_UBO_Regulations__c != true && SRData.Registered_Office_Address__c==false)// I hereby confirm that there has been no changes to the Ultimate Beneficial Owners register since the last filing with the DIFC Registrar of Companies.
{
    isvalid=false;
    SRData.addError('Please confirm all checkboxes in the Confirmation and Undertaking section');
   // ApexPages.addMessage(myMsg);
} 

if(isISPV && SRData.Change_Activity__c == FALSE){
    isvalid=false;
    SRData.addError('Required information- ISPV Checkbox');
}

if(SRData.Registered_Phone_Number__c==false && SRData.Confirm_Change_of_Trading_Name__c=='Yes' && SRData.Legal_Structures__c=='LTD')// has an annual turnover of US$5 million or less, calculated on a consolidated basis including all subsidiaries. 
{
	isvalid=false;
	SRData.addError('Please confirm all checkboxes in the Confirmation and Undertaking section');
}
if(String.isBlank(SRData.Confirm_Change_of_Trading_Name__c) && SRData.Legal_Structures__c=='LTD')// has an annual turnover of US$5 million or less, calculated on a consolidated basis including all subsidiaries. 
{
	isvalid=false;
	SRData.addError('Required information- Annual Turnover');
}

if(SRData.Business_Activity__c==false)//I hereby confirm that all information required to be filed by the entity pursuant to DIFC Companies Law, Law No. 5 of 2018 and Companies Regulations in 
{
        isvalid=false;
        SRData.addError('Please confirm all checkboxes in the Confirmation and Undertaking section');
}

    /*
    if(!isvalid)
    {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Review all error messages below to correct your data.');
        ApexPages.addMessage(myMsg);
    
    }
    */
    
    
    //5.    If BC shareholder/founding member etc. exist without UBO, they cannot submit confirmation statement. Yes

    List<Relationship__c> ListBCshareholders=[select Subject_Account__c,id from Relationship__c where Subject_Account__c=:SRData.Customer__c and Relationship_Type__c='Is Shareholder Of' and Object_Account__c!=null and Active__c=true];
    
    if(!ListBCshareholders.isempty())
    {
        List<Relationship__c> ListBCOnwer=[select Subject_Account__c,id from Relationship__c where Subject_Account__c=:SRData.Customer__c and Relationship_Type__c='Beneficiary Owner' and Active__c=true];
        if(ListBCOnwer.isempty()) {ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please Update your Shareholder details and provide Beneficiary Owner information.'); ApexPages.addMessage(myMsg); }
        
    }
    if(isvalid) {       
        if(!IsFEY_Provided)        SRData.Financial_Year_End_mm_dd__c=ObjAccount.Financial_Year_End__c;     
        try     
        {  
            upsert SRData;
            PageReference acctPage = new ApexPages.StandardController(SRData).view();        
            acctPage.setRedirect(true);        
            return acctPage;     
        }catch (Exception e)     
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());         
            ApexPages.addMessage(myMsg);    
        }  
     }
    return null;
    }
}