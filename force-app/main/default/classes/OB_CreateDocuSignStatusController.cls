public with sharing class OB_CreateDocuSignStatusController {
    public OB_CreateDocuSignStatusController() {

    }
    @AuraEnabled
    public static RespondWrap createDocuSignStatus(String requestWrapParam)  {
    
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
        
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
        
        respWrap.userId = UserInfo.getUserId();
        return respWrap;
    }
        
    
    // ------------ Wrapper List ----------- //
        
    public class RequestWrap{
    
        @AuraEnabled public String userId; 
    }
        
    public class RespondWrap{
    
        @AuraEnabled public string userId;
        @AuraEnabled public string errorMessage;
    }
}