/**
*Author : Merul Shah
*Description : Utility for OCR.
**/
public without sharing  class OB_OCRHelper
{


     public static Schema.DescribeSObjectResult descObj;
     public static Map<String,Schema.SObjectField> fldDecsMap; 
        
     public static Schema.DescribeSObjectResult getDescObj(String objName)
     {
        descObj = (descObj == NULL ? Schema.getGlobalDescribe().get(objName).getDescribe() : descObj); 
        return descObj;
     }
       
     public static Map<String,Schema.SObjectField> getFldDecsMap(Schema.DescribeSObjectResult descObj)
     {
        if(descObj != NULL){
          fldDecsMap  =  (fldDecsMap == NULL ?  descObj.fields.getMap() : fldDecsMap); 
          return fldDecsMap;
        }
        else
        return NULL;
     }


    @AuraEnabled
    public static ResponseWrapper processOCR(RequestWrapper reqWrap ) 
    {
       system.debug('@@@@@@@@ Entered processOCR ');
       
       //RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);
       ResponseWrapper respWrap = new ResponseWrapper();
        
        String fileId =  reqWrap.fileId; //param
        try{
            // FileUploadOCRController.ResponseWrapper respWrap = new  FileUploadOCRController.ResponseWrapper();
            //To DO : logic to build the rest structure and send to OCR services.
            System.debug(fileId+' This is the file Id');
            ContentVersion cv = [SELECT VersionData 
                                   FROM ContentVersion 
                                  WHERE  ContentDocumentId =:fileId
                               ORDER BY CreatedDate DESC
                                  LIMIT 1
                                ]; 
                                  
                                  
                                  
            String param = EncodingUtil.Base64Encode(cv.VersionData);
            system.debug('param@@@'+param);
            
            // Set Timeout limit
            Integer timeoutLimit = integer.ValueOf(system.label.APICallTimeoutLimit);
            Long endTime ,startTime ,totalTime ;
            startTime = datetime.now().getTime();
            
            // Instantiate a new http object
            Http h = new Http();
            
            // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
            HttpRequest req = new HttpRequest();
            // Pass in the endpoint to be used using the string url
            req.setEndpoint(system.label.OCR_url);
            req.setTimeout(timeoutLimit);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Username',system.label.OCR_username );
            req.setHeader('Password',system.label.OCR_password);
            req.setBody('{"content":"'+param+'","filename":"test.jpg"}');
            req.setTimeout(20000);
            System.debug('REQUEST BODY: '+req.getBody());
                        
            // Send the request, and return a response
            HttpResponse res = h.send(req);
            endTime = datetime.now().getTime();
            totalTime = endTime - startTime;
            System.debug('====totalTime===='+totalTime);
            System.debug('RESPONSE STATUS CODE: '+res.getStatusCode());
            System.debug('RESPONSE BODY: '+res.getBody());
            String jsonToString = String.valueOf(JSON.deserializeUntyped(res.getBody()) );
            
            
            /*
                DIFC_ParseJson obj= new DIFC_ParseJson();
                List<String> passportData = obj.extractData(jsonToString);
                System.debug(passportData);
                return passportData;
            */
            system.debug('==jsonToString=='+jsonToString);
            if(string.IsNotBlank(jsonToString))
            {
                reqWrap.jsonToString = jsonToString;
                respWrap.mapFieldApiObject = processOCRResponse(reqWrap);
            }
            system.debug('====respWrap.mapFieldApiObject=='+respWrap.mapFieldApiObject);
            return respWrap;
            
        }
        catch(CalloutException e)
        {
            System.debug('Exception Caught at line : '+e.getLineNumber()+' in DIFC_OCRServices class. Message: '+e.getMessage());
            respWrap.errorMessage = e.getMessage();
            return respWrap;
        }
        
    }
    
    public static Map<String,Object> processOCRResponse( RequestWrapper reqWrap ) 
    {
        
        String jsonString = reqWrap.jsonToString;
        String sObjectToProcess =  reqWrap.sObjectToProcess;
        system.debug('@@@@@@@@@ processOCRResponse ');
        system.debug('@@@@@@@@@ sObjectToProcess  '+sObjectToProcess);
        
        if(test.isRunningTest())
        {
          jsonString = '{\"Name\":\"OSULLIVAN,LAUREN\",\"Gender\":\"F\",\"Country\":\"IRL\",\"DOB\":\"1988-05-04\",\"PassportNo\":\"XN5004216\",\"PassportExpiry\":\"2023-09-15\",\"PersonalNo\":\"\"}';
          
        }
        
        jsonString = '['+jsonString+']';
         system.debug('======jsonString=====');
        List<Object> result = (List<Object>)JSON.deserializeUntyped(jsonString);
        Map<String,Object> mapResp;
        for(Object obj : result) 
        { 
            mapResp = (Map<String,Object>)obj; 
            
        }
        
        system.debug('@@@@@@@@@@ mapResp  '+mapResp );
        Map<String,Object> mapFieldApiObject = new Map<String,Object>();
        
        Map<String,String> mapOcrSFfld;
        if( String.isNotBlank(sObjectToProcess) 
                    && sObjectToProcess.toLowerCase()  == 'hexabpm__service_request__c')
        {
            
            mapOcrSFfld = new Map<String,String>{
                                                 'Gender'=>'gender__c',
                                                 'DOB'=>'date_of_birth__c',
                                                 'PassportNo'=>'passport_no__c',
                                                 'PassportExpiry'=>'date_of_expiry__c',
                                                 'PersonalNo'=>'',
                                                 'Country'=>'nationality__c' 
                                            };
        }
        else   // this is genric but mainly for hexaBPM_amendment__c
        {
            
            mapOcrSFfld = new Map<String,String>{
                                                 'Gender'=>'gender__c',
                                                 'DOB'=>'date_of_birth__c',
                                                 'PassportNo'=>'passport_no__c',
                                                 'PassportExpiry'=>'date_of_expiry__c',
                                                 'PersonalNo'=>'',
                                                 'Country'=>'nationality_list__c'
                                                };
                                                
        }
        
                                            
        
        
        
        //resolving name to first name and last name.
        if(mapResp.size() > 0 && mapResp.containsKey('Name') )
        {
            String[] name = ( (String) mapResp.get('Name')).split(',');
            mapFieldApiObject.put('last_name__c',name[0] );
            mapFieldApiObject.put('first_name__c',name[1] );
        }
        // get nationality
        if(mapResp.size() > 0 && mapResp.containsKey('Country') )
        {
            String nationality = getNationlaity((String) mapResp.get('Country'),'');
            String nationFld = mapOcrSFfld.get('Country');
            if( String.isNotBlank(nationality) 
               			&& String.isNotBlank(nationFld) )
            {
                mapFieldApiObject.put(nationFld,nationality );
            }
            
        }
        
        // resolving Gender..... Gender
        if(mapResp.size() > 0 && mapResp.containsKey('Gender') )
        {
            String gender = (String) mapResp.get('Gender');
            if(String.isNotBlank(gender))
            {
                if( gender.equalsIgnoreCase('m') )
                {
                    gender = 'Male';
                }
                else if( gender.equalsIgnoreCase('f') )
                {
                    gender = 'Female';
                }
                String genderFld = mapOcrSFfld.get('Gender');
                if( String.isNotBlank(gender) 
                            && String.isNotBlank(genderFld) )
                {
                    mapFieldApiObject.put(genderFld,gender );
                } 
            }
        }
        
        
        
        
        for(String ORCFldapi : mapResp.keySet() )
        {
            system.debug('=====ORCFldapi===');
            if( ORCFldapi != 'Country' 
               			&& ORCFldapi != 'Gender')
            {
                String fldApiSF = (mapOcrSFfld.containsKey(ORCFldapi) ? mapOcrSFfld.get(ORCFldapi).toLowerCase() : NULL);
                Object objValue =  mapResp.get(ORCFldapi);
                if(fldApiSF != NULL && fldApiSF != '' && objValue != NULL && objValue != '')
                {
                  // mapFieldApiObject.put(fldApiSF,objValue);
                }
                typeCastFlds(fldApiSF,objValue,mapFieldApiObject);
            }
            
        }
        
        system.debug('$$$$$$$$$$$ mapFieldApiObject '+mapFieldApiObject);
        
        return mapFieldApiObject;
        
    }
    
    //Merul: This type cast the field based on data type.
    public static void typeCastFlds(String fname,
                                    Object fval,
                                    Map<String,Object> mapFieldApiObject)
    {
        set<Schema.DisplayType> fldtyp = new set<Schema.DisplayType>();
        Schema.DescribeSObjectResult dsr = getDescObj('HexaBPM__Service_Request__c');
        Map<String,Schema.SObjectField> mapDesc = getFldDecsMap(dsr);
        system.debug('######### mapDesc  '+mapDesc );
        
        if(fname != null  && fname  !='')
        {
            if(fval != null && fval!='' )
            {
                //objrequest.put(fname,Boolean.valueof(fval));
                system.debug('######### fname '+fname);
                Schema.DisplayType fldType = mapDesc.get(fname.toLowerCase()).getDescribe().getType();
                system.debug('########### fldType '+fldType);

                switch on fldType
                {

                    when BOOLEAN 
                    {       
                        mapFieldApiObject.put(fname,Boolean.valueof(fval));
                    }   
                    when DOUBLE
                    {       
                        mapFieldApiObject.put(fname,Double.valueof(fval));
                    }
                    when DATE 
                    {       
                        mapFieldApiObject.put(fname,Date.valueof(fval+''));
                    }
                    /*Not required as of now for passport.
                        when DATETIME 
                        {       
                            
                            Datetime dt = Datetime.valueOf(fval.replace('T',' ')).addHours(4);
                            mapFieldApiObject.put(fname,dt);
                            
                        }
                    */
                    when else 
                    {         
                        mapFieldApiObject.put(fname,String.valueof(fval));
                    }

                }

            }
            else
            {
                mapFieldApiObject.put(fname,null);
            }
        }
    }   


    public static string getNationlaity(string countrycode,string nationality)
    {
        string nationalityName = '';
        if(String.IsNotBlank(countrycode)){
            for(Country_Codes__c cc:[Select Name from Country_Codes__c where Alpha_3_Code__c=: countrycode]){
                nationalityName = cc.Name;
            }
        }
        if(String.IsNotBlank(nationality)){
            for(Country_Codes__c cc:[Select ID,Alpha_3_Code__c from Country_Codes__c where Name=: nationality]){
                nationalityName = cc.Nationality__c;
            }
        }
        return nationalityName;
    }
        
        
       
 
 public class RequestWrapper
 {
        
        // For OCR
        @AuraEnabled public string fileId {get;set;}
        @AuraEnabled public String jsonToString {get;set;}
        @AuraEnabled public String sObjectToProcess{get;set;}
        
        
        public RequestWrapper()
        {}
  }
 
 public class ResponseWrapper
 {
        
        // For OCR
        @AuraEnabled public  Map<String,Object> mapFieldApiObject {get;set;}
        @AuraEnabled public  String errorMessage {get;set;}
        
        
        public ResponseWrapper()
        {}
  }
   
}