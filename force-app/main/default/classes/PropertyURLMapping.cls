global with sharing class PropertyURLMapping implements Site.UrlRewriter {
            String forSale = '/for-sale/';
        String forRent = '/for-rent/';
    global PageReference mapRequestUrl(PageReference myFriendlyUrl) {
        String url = myFriendlyUrl.getUrl();

        if(url.contains('advanced-search') ) {
            return new PageReference('/AdvancedSearch');
        } if(url.contains('map')) {
            return new PageReference('/MapView');
        } if(url.contains('search-results')) {
            return new PageReference('/SearchResults');
        } if(url.contains(forSale) || url.contains(forRent)) {
            String lId = url.substring(forSale.length()+3, url.length());
            //Listing__c objL = [select id,Listing_Title__c from Listing__c where Id=:lId];
            return new PageReference('/PropertyDetail'+'?id=' + lId);
        }
        return null;
    }
    global PageReference[] generateUrlFor(PageReference[] mySalesforceUrls){
        List<PageReference> myFriendlyUrls = new List<PageReference>();
        for(PageReference mySalesforceUrl : mySalesforceUrls) {
           String url = mySalesforceUrl.getUrl();
           if(url.contains('AdvancedSearch')){
             myFriendlyUrls.add(new PageReference('/advanced-search'));
           } else if(url.contains('MapView')){
             myFriendlyUrls.add(new PageReference('/map'));
           } else if(url.contains('SearchResults')){
             myFriendlyUrls.add(new PageReference('/search-results'));
           } else if(url.contains('PropertyDetail')){
               String lId = Apexpages.currentPage().getParameters().get('id');
               Listing__c objL = [select id,Listing_Title__c,Listing_Type__c from Listing__c where Id=:lId];
                String newUrl = '';
               if(objL.Listing_Type__c == 'Sale')
                   newUrl = forSale + 'fs-' + lId;
               else
                   newUrl = forRent + 'fr-' + lId;
             myFriendlyUrls.add(new PageReference(newUrl));
           } else {
             myFriendlyUrls.add(mySalesforceUrl);
           }
        }
        return myFriendlyUrls;
    }
}