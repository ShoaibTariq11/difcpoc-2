@isTest(seeAllData=false)
public class TestClone_ROC{
      
      
     static Step__c CreateData_Clone(){
          
        Account objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c ='Pakistan';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        
        Account objAccount2 = new Account();
        objAccount2.Name = 'Test Account';
        objAccount2.Place_of_Registration__c ='Pakistan';
        insert objAccount2;
        string ContacRecType = '';
          for(RecordType recType:[select id from RecordType where sObjectType='Contact' and DeveloperName='Individual']){
            ContacRecType = recType.Id;
          }
      
    
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Passport_No__c = 'AS4324SDF';
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = ContacRecType;
        objContact.Nationality__c = 'Pakistan';
        insert objContact;
        
        Contact objContact2 = new Contact();
        objContact2.LastName = 'Test Contact2';
        objContact2.FirstName = 'Test Contact2';
        objContact2.Passport_No__c = 'AS132';
        objContact2.AccountId = objAccount.Id;
        objContact2.Nationality__c = 'Pakistan';
        objContact2.Email = 'test@difc.com';
        objContact2.RecordTypeId = ContacRecType;
        insert objContact2;
        
      
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Consolidate_Subdivide_Share';
        objTemplate.SR_RecordType_API_Name__c = 'Consolidate_Subdivide_Share';
        objTemplate.Menutext__c = 'Consolidate_Subdivide_Share';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        objTemplate.Allow_Multiple_SR_Submission__c = true;
        insert objTemplate;
        
          map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName ='Consolidate_Subdivide_Share']){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        Currency__c curr =new Currency__c(Name='US Dollar',Exchange_Rate__c=1.00,Active__c=true);
        insert curr;
        
         Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount2.Id;
       // objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR1.legal_structures__c ='LTD';
        objSR1.currency_list__c = 'US Dollar';
        objSR1.Currency_Rate__c = curr.id;
        objSR1.Share_Capital_Membership_Interest__c =90;
        objSR1.No_of_Authorized_Share_Membership_Int__c = 5;
        objSR1.Proposed_Trading_Name_1__c = 'ABC tRADING NAME';
        objSR1.RecordTypeId = mapRecordType.get('Consolidate_Subdivide_Share');
        objSR1.Commercial_Activity__c = 'test';
        insert objSR1;
        
        
        
        Account_Share_Detail__c accshrlist = new Account_Share_Detail__c();
        accshrlist.Account__c =  objAccount2.Id;
        accshrlist.Name = 'A';
        accshrlist.Status__c = 'Active';
        accshrlist.No_of_shares_per_class__c = 50;
        accshrlist.Nominal_Value__c = 1000;
        accshrlist.Sys_Proposed_Nominal_Value__c = 1000;
        accshrlist.Sys_Proposed_No_of_Shares_per_Class__c=50;
        insert accshrlist;
        
        Shareholder_Detail__c shDetail = new Shareholder_Detail__c();
        shDetail.Account_Share__c = accshrlist.ID;
        shDetail.Account__c = objAccount2.ID;
        shDetail.No_of_Shares__c = 5;
        shDetail.Sys_Proposed_No_of_Shares__c = 5;
        shDetail.Shareholder_Account__c = objAccount2.ID;
        
        insert shDetail;
        
        shDetail = new Shareholder_Detail__c();
        shDetail.Account_Share__c = accshrlist.ID;
        shDetail.Account__c = objAccount2.ID;
        shDetail.No_of_Shares__c = 5;
        shDetail.Sys_Proposed_No_of_Shares__c = 5;
        shDetail.Status__c = 'Active';
        shDetail.Shareholder_Account__c = objAccount2.ID;
        insert shDetail;
        
        shDetail = new Shareholder_Detail__c();
        shDetail.Account_Share__c = accshrlist.ID;
        shDetail.Account__c = objAccount2.ID;
        shDetail.No_of_Shares__c = 5;
        shDetail.Sys_Proposed_No_of_Shares__c = 5;
        shDetail.Shareholder_Contact__c = objContact2.ID;
        
        insert shDetail;
        
        shDetail = new Shareholder_Detail__c();
        shDetail.Account_Share__c = accshrlist.ID;
        shDetail.Account__c = objAccount2.ID;
        shDetail.No_of_Shares__c = 5;
        shDetail.Sys_Proposed_No_of_Shares__c = 5;
        shDetail.Status__c = 'Active';
        shDetail.Shareholder_Contact__c = objContact2.ID;
        insert shDetail;
        
        Amendment__c iAmm = new Amendment__c();
        iAmm.Status__c = 'Active';
        iAmm.No_of_shares_per_class__c = 1;
        iAmm.Sys_Proposed_Nominal_Value__c = 1;
        iAmm.Nominal_Value__c = 1;
        iAmm.Sys_Proposed_No_of_Shares_per_Class__c = 1;
        iAmm.Shareholder_Company__c = objAccount2.ID;
        iAmm.ServiceRequest__c = objSR1.ID;
        iAmm.Class_Name__c ='A';
        iAmm.Total_Issued__c = 50;
        iAmm.Shareholder_Company_val__c = objAccount2.ID;
        
        insert iAmm;
        
        iAmm = new Amendment__c();
        iAmm.Status__c = 'Active';
        iAmm.No_of_shares_per_class__c = 1;
        iAmm.Sys_Proposed_Nominal_Value__c = 1;
        iAmm.Nominal_Value__c = 1;
        iAmm.Sys_Proposed_No_of_Shares_per_Class__c = 1;
        iAmm.Shareholder_Company__c = objAccount2.ID;
        iAmm.ServiceRequest__c = objSR1.ID;
        iAmm.Class_Name__c ='A';
        iAmm.Total_Issued__c = 50;
        iAmm.Shareholder_Company_val__c = objContact2.ID;
        
        insert iAmm;
        
        Step__c objStp1 = new Step__c();
        objStp1.SR__c = objSR1.Id;
        insert objStp1;
      
       
        SR_Status__c  srStatus = new SR_Status__c ();
        srStatus.name = 'Submitted';
        srStatus.Code__C   = 'Submitted';
        insert srStatus;
        
        Status__c srStatusP = new Status__c ();
        srStatusP.name = 'Pending Review';
        srStatusP.Code__C   = 'PENDING_REVIEW';
        insert srStatusP;
        
        Step_Template__c  stpTemplate = new  Step_Template__c();
        stpTemplate.CODE__c = 'VERIFICATION_OF_APPLICATION';
        stpTemplate.Name = 'Verification of Application';
        stpTemplate.Step_RecordType_API_Name__c = 'VERIFICATION_OF_APPLICATION';
        insert stpTemplate;
        
        SYSTEM.DEBUG('$$$$$$' + [select id,name from Status__c where Code__c  = 'PENDING_REVIEW']);
        
        SR_Steps__c srStep = new SR_Steps__c();
        srStep.SR_Template__c = objTemplate.ID;
        srStep.Step_Template__c = stpTemplate.ID;
        
        insert srStep;
        
        
        
        Step__c stp =[Select ID, SR__c, SR__r.Customer__c,SR__r.Commercial_Activity__c from step__C 
                      WHERE ID =: objStp1.ID];
        
        return stp;
          
      }
      
    static testMethod void myUnitTest4() {
              
        test.startTest();
        
        Step__c cloneStep = CreateData_Clone();
        CC_AoR_Util.CloneAORAsync(Json.serialize(cloneStep));
        
        test.stopTest();
    }
      
}