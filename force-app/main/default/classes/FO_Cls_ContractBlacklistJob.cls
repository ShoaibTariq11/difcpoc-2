/**
 * Schedulable class for updating the contractor blacklist points
 * @author	Claude Manahan. NSI-DMCC
 * @date	19-04-2016
 */
global class FO_Cls_ContractBlacklistJob implements Schedulable {
	
	global void execute(SchedulableContext SC) {
		
		Date dateYearBefore = System.today().addYears(-1);
		
		Integer days = dateYearBefore.daysBetween(System.Today());
		
		String query = 'SELECT Id, Blacklist_Point_3_months__c, Blacklist_Point_6_months__c, Blacklist_Point_1_year__c, (SELECT Id, Violation_Date__c, Blacklist_Point__c FROM Violations__r WHERE Violation_Date__c = LAST_N_DAYS:'+days+' ORDER BY Violation_Date__c DESC) FROM Account WHERE Recordtype.DeveloperName = \'Contractor_Account\'';
  		
  		FO_Cls_ContractBlacklist calculateBlackListJob = new FO_Cls_ContractBlacklist(query);
  		database.executeBatch(calculateBlackListJob);
  		 
	}
}