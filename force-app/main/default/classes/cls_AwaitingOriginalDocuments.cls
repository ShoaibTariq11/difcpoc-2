/**************************************************************************************************************
Author      : Swati Sehrawat
Date        : 07/Sep/2014
ClassName   : cls_AwaitingOriginalDocuments
Description :
    -This class is used to create generic email template for Awaiting Original steps
****************************************************************************************************************/

global without sharing class cls_AwaitingOriginalDocuments{
    public string stepId {get; set;}
    public string srId;
    public step__c objStep{
        get{
            if (objStep == null){
                objStep = getRelatedStep();
            }
            return objStep;
        }
        set;
    }
    
    public string documentsRequired{
        get{
            if ((documentsRequired == '' || documentsRequired == null) && objStep!=null && objStep.SR__c!=null){
                documentsRequired = getRelatedCustomSetting(objStep.SR__c);
            }
            return documentsRequired;
        }
        
        set;
    }
    
    public cls_AwaitingOriginalDocuments(){
    }
    
    public step__c getRelatedStep(){
        if(string.isNotBlank(stepId)){
            list<step__c> tempStepList = [select id, SR__c from step__c where id=:stepId and SR__c!=null limit 1]; 
            if(tempStepList!=null && !tempStepList.isEmpty()){
                step__c tempObj = tempStepList[0];
                srId = tempObj.SR__c;
                return tempObj; 
            }
            else{
                return null;
            }
        }    
        return null;
    }
    
    public string getRelatedCustomSetting(string relatedSRID){
        if(string.isNotBlank(relatedSRID)){
            String tempDocumentsRequired = '';
			Map<string,Awaiting_Original_Documents__c> customSettings = Awaiting_Original_Documents__c.getAll();
    		list<SR_Price_Item__c> priceItemList = [select id, Material_Code__c from SR_Price_Item__c where ServiceRequest__c =: srId and  Material_Code__c IN : customSettings.keyset()];
            
            for(SR_Price_Item__c tempObj : priceItemList){
            	if(customSettings.get(tempObj.Material_Code__c)!=null){
            		customSettings.get(tempObj.Material_Code__c).Document_Description__c = customSettings.get(tempObj.Material_Code__c).Document_Description__c.replaceAll('\n','<br/>');
            		tempDocumentsRequired = customSettings.get(tempObj.Material_Code__c).Document_Description__c;
            	}	
            }
            
            return tempDocumentsRequired;
        }
        else{
            return null;
        }
    }
}