/**********************************************************
*Apex class :RORP Page Notification - RORPPageNotification
*Description : used to show the error message or validaiton on RORP service request records
*Created on: 09-Jul-2019    Selva   #7099
***********************************************************/
public class RORPPageNotification{
    
    public Boolean IsPortaluser {get;set;}
    public Service_Request__c ServiceRequest {get;set;}
    public string srID {get;set;}
    public List<Amendment__c> objAmnd;
    public string strMsg {get;set;}
    
    public RORPPageNotification(Apexpages.Standardcontroller con){
        
        IsPortaluser = (Userinfo.getUserType() == 'Standard') ? false : true;
        objAmnd = new List<Amendment__c>();
        strMsg = '';
        if(Apexpages.currentPage().getParameters().get('id') != null && Apexpages.currentPage().getParameters().get('id') != ''){
            SRId = Apexpages.currentPage().getParameters().get('id');
        }
        if(!IsPortaluser){
            RORPnationalityCheck(SRId);
        }
    }
    
    public void RORPnationalityCheck(string srID){
         
        objAmnd = [select id,Amendment_Type__c,recordtype.DeveloperName,Nationality_list__c,Place_of_Registration__c from Amendment__c where ServiceRequest__c=:srID];
        
        Boolean isSanctionedNationality = FALSE;
        
        RORP_Sanctioned_Nationality__c sanctNationalty;
        for(Amendment__c itr: objAmnd){
           system.debug('byuerinfo' +itr.Amendment_Type__c);
            system.debug('byuerinfo' +itr.recordtype.DeveloperName);
            if((test.isRunningTest()) || (itr.Amendment_Type__c=='Buyer' && itr.recordtype.DeveloperName=='Individual_Buyer') || (itr.Amendment_Type__c=='Shareholder' && itr.recordtype.DeveloperName=='RORP_Shareholder')){
                
            if(itr.Nationality_list__c !=null){
                sanctNationalty = RORP_Sanctioned_Nationality__c.getValues(itr.Nationality_list__c);
            }
            system.debug('----'+sanctNationalty);
            if(sanctNationalty!=null){
                isSanctionedNationality = true;
                strMsg = 'AML country';
                 Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Info,'Nationality matched with AML sanctioned country list'));
                break;
            }
            }
        }
    }
    
}