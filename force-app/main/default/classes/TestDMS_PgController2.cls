@isTest(seealldata=false)
public class TestDMS_PgController2{

    
      public static testmethod void testPGDMSFoundation() {
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Application_of_Registration','Change_Shareholder_Details','Allotment_of_Shares_Membership_Interest')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        Page_Flow__c objPageFlow = new Page_Flow__c();
        objPageFlow.Name = 'Application_for_Registration';
        objPageFlow.Master_Object__c = 'Service_Request__c';
        objPageFlow.Record_Type_API_Name__c = 'Allotment_of_Shares_Membership_Interest';
        insert objPageFlow;

        UBO_Configuration__c uboConfig = new UBO_Configuration__c();
        uboConfig.name = 'shareholder';
        uboConfig.List_of_Legal_structures__c = 'LTD,LTD IC,LTD PCC,LTD SPC';
        insert uboConfig;

        uboConfig = new UBO_Configuration__c();
        uboConfig.name = 'partner';
        uboConfig.List_of_Legal_structures__c = 'GP,RLP,LP,RP';
        insert uboConfig;

        uboConfig = new UBO_Configuration__c();
        uboConfig.name = 'member';
        uboConfig.List_of_Legal_structures__c = 'LLC';
        insert uboConfig;

        uboConfig = new UBO_Configuration__c();
        uboConfig.name = 'limited partner';
        uboConfig.List_of_Legal_structures__c = 'RLP';
        insert uboConfig;

        uboConfig = new UBO_Configuration__c();
        uboConfig.name = 'general partner';
        uboConfig.List_of_Legal_structures__c = 'RP,GP';
        insert uboConfig;

        uboConfig = new UBO_Configuration__c();
        uboConfig.name = 'founding member';
        uboConfig.List_of_Legal_structures__c = 'NPIO';
        insert uboConfig;

        uboConfig = new UBO_Configuration__c();
        uboConfig.name = 'founder';
        uboConfig.List_of_Legal_structures__c = 'Foundation';
        insert uboConfig;

        uboConfig = new UBO_Configuration__c();
        uboConfig.name = 'designated member';
        uboConfig.List_of_Legal_structures__c = 'LLP,RLLP';
        insert uboConfig;

        uboConfig = new UBO_Configuration__c();
        uboConfig.name = 'allotshares';
        uboConfig.List_of_Legal_structures__c = 'LTD,LTD IC,LTD PCC,LTD SPC,LLC';
        insert uboConfig;


        Currency__c curr =new Currency__c(Name='US Dollar',Exchange_Rate__c=1.00,Active__c=true);
        insert curr;
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.Authorized_Share_Capital__c = 500000;
        objAccount.Currency__c = curr.id;
        objAccount.BP_No__c = '001234';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Non - financial';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        insert objAccount;
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.Currency_Rate__c = curr.id;
       // objSR1.Send_SMS_To_Mobile__c = '+971123456789';
        objSR1.legal_structures__c ='Foundation';
        objSR1.Entity_Name__c = 'test Foundation';
        objSR1.currency_list__c = 'US Dollar';
        objSR1.Share_Capital_Membership_Interest__c =50000;
        objSR1.No_of_Authorized_Share_Membership_Int__c = 560;
        objSR1.RecordTypeId = mapRecordTypeIds.get('Application_of_Registration');
        insert objSR1;
        Apexpages.currentPage().getParameters().put('id',objSR1.Id);
        Apexpages.currentPage().getParameters().put('FlowId',objPageFlow.Id);
        Apexpages.currentPage().getParameters().put('Type','Shareholder');
        Cls_DMS_PgController dms1 = new Cls_DMS_PgController();
        dms1.Add_Amendment_BodyCorporate();
        dms1.Add_Amendment_Individual();
        
        list<Amendment__c> amendlist = new list<Amendment__c> ();
        Amendment__c amnd1 = new Amendment__c (Passport_No__c='AS132',ServiceRequest__c=objSR1.id,Relationship_Type__c='Council Member',Place_of_Registration__c = 'Pakistan',Company_Name__c = 'Danish Test' ,Amendment_Type__c = 'Body Corporate');
        amendlist.add(amnd1);
         Amendment__c amnd2 = new Amendment__c (Passport_No__c='AS132',ServiceRequest__c=objSR1.id,Relationship_Type__c='Guardian',Place_of_Registration__c = 'Pakistan',Company_Name__c = 'Danish Test',Amendment_Type__c = 'Body Corporate');
        amendlist.add(amnd2);
       
        insert amendlist;
        Apexpages.currentPage().getParameters().put('Type','Guardian');
        Cls_DMS_PgController dms12 = new Cls_DMS_PgController();
        dms12.objAmnd = amendlist[1];
        //dms12.objAmnd.Passport_No__c='ASD132';
        
        dms12.Save_Amendment();
        
        
            
    }
    
    public static testmethod void testPGDMSFoundation2() {
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Application_of_Registration','Change_Shareholder_Details','Allotment_of_Shares_Membership_Interest')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        Page_Flow__c objPageFlow = new Page_Flow__c();
        objPageFlow.Name = 'Application_for_Registration';
        objPageFlow.Master_Object__c = 'Service_Request__c';
        objPageFlow.Record_Type_API_Name__c = 'Allotment_of_Shares_Membership_Interest';
        insert objPageFlow;

         UBO_Configuration__c uboConfig = new UBO_Configuration__c();
        uboConfig.name = 'shareholder';
        uboConfig.List_of_Legal_structures__c = 'LTD,LTD IC,LTD PCC,LTD SPC';
        insert uboConfig;

        uboConfig = new UBO_Configuration__c();
        uboConfig.name = 'partner';
        uboConfig.List_of_Legal_structures__c = 'GP,RLP,LP,RP';
        insert uboConfig;

        uboConfig = new UBO_Configuration__c();
        uboConfig.name = 'member';
        uboConfig.List_of_Legal_structures__c = 'LLC';
        insert uboConfig;

        uboConfig = new UBO_Configuration__c();
        uboConfig.name = 'limited partner';
        uboConfig.List_of_Legal_structures__c = 'RLP';
        insert uboConfig;

        uboConfig = new UBO_Configuration__c();
        uboConfig.name = 'general partner';
        uboConfig.List_of_Legal_structures__c = 'RP,GP';
        insert uboConfig;

        uboConfig = new UBO_Configuration__c();
        uboConfig.name = 'founding member';
        uboConfig.List_of_Legal_structures__c = 'NPIO';
        insert uboConfig;

        uboConfig = new UBO_Configuration__c();
        uboConfig.name = 'founder';
        uboConfig.List_of_Legal_structures__c = 'Foundation';
        insert uboConfig;

        uboConfig = new UBO_Configuration__c();
        uboConfig.name = 'designated member';
        uboConfig.List_of_Legal_structures__c = 'LLP,RLLP';
        insert uboConfig;

        uboConfig = new UBO_Configuration__c();
        uboConfig.name = 'allotshares';
        uboConfig.List_of_Legal_structures__c = 'LTD,LTD IC,LTD PCC,LTD SPC,LLC';
        insert uboConfig;

        Currency__c curr =new Currency__c(Name='US Dollar',Exchange_Rate__c=1.00,Active__c=true);
        insert curr;
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.Authorized_Share_Capital__c = 500000;
        objAccount.Currency__c = curr.id;
        objAccount.BP_No__c = '001234';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Non - financial';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        insert objAccount;
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.Currency_Rate__c = curr.id;
       // objSR1.Send_SMS_To_Mobile__c = '+971123456789';
        objSR1.legal_structures__c ='Foundation';
        objSR1.Entity_Name__c = 'test Foundation';
        objSR1.currency_list__c = 'US Dollar';
        objSR1.Share_Capital_Membership_Interest__c =50000;
        objSR1.No_of_Authorized_Share_Membership_Int__c = 560;
        objSR1.RecordTypeId = mapRecordTypeIds.get('Application_of_Registration');
        insert objSR1;
        Apexpages.currentPage().getParameters().put('id',objSR1.Id);
        Apexpages.currentPage().getParameters().put('FlowId',objPageFlow.Id);
        Apexpages.currentPage().getParameters().put('Type','Shareholder');
        Cls_DMS_PgController dms1 = new Cls_DMS_PgController();
        dms1.Add_Amendment_BodyCorporate();
        dms1.Add_Amendment_Individual();
        
        list<Amendment__c> amendlist = new list<Amendment__c> ();
        Amendment__c amnd1 = new Amendment__c (Passport_No__c='AS132',ServiceRequest__c=objSR1.id,Relationship_Type__c='Guardian',Place_of_Registration__c = 'Pakistan',Company_Name__c = 'Danish Test' ,Amendment_Type__c = 'Body Corporate');
        amendlist.add(amnd1);
         Amendment__c amnd2 = new Amendment__c (Passport_No__c='AS132',ServiceRequest__c=objSR1.id,Relationship_Type__c='Council Member',Place_of_Registration__c = 'Pakistan',Company_Name__c = 'Danish Test',Amendment_Type__c = 'Body Corporate');
        amendlist.add(amnd2);
       
        insert amendlist;
        Apexpages.currentPage().getParameters().put('Type','Council Member');
        Cls_DMS_PgController dms12 = new Cls_DMS_PgController();
        dms12.objAmnd = amendlist[1];
        //dms12.objAmnd.Passport_No__c='ASD132';
        
        dms12.Save_Amendment();
        
        
            
    }
    
    public static testmethod void testPGDMSFoundation3() {
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Application_of_Registration','Change_Shareholder_Details','Allotment_of_Shares_Membership_Interest')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        Page_Flow__c objPageFlow = new Page_Flow__c();
        objPageFlow.Name = 'Application_for_Registration';
        objPageFlow.Master_Object__c = 'Service_Request__c';
        objPageFlow.Record_Type_API_Name__c = 'Allotment_of_Shares_Membership_Interest';
        insert objPageFlow;

        Currency__c curr =new Currency__c(Name='US Dollar',Exchange_Rate__c=1.00,Active__c=true);
        insert curr;
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.Authorized_Share_Capital__c = 500000;
        objAccount.Currency__c = curr.id;
        objAccount.BP_No__c = '001234';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Non - financial';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        insert objAccount;
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.Currency_Rate__c = curr.id;
       // objSR1.Send_SMS_To_Mobile__c = '+971123456789';
        objSR1.legal_structures__c ='Foundation';
        objSR1.Entity_Name__c = 'test Foundation';
        objSR1.currency_list__c = 'US Dollar';
        objSR1.Share_Capital_Membership_Interest__c =50000;
        objSR1.No_of_Authorized_Share_Membership_Int__c = 560;
        objSR1.RecordTypeId = mapRecordTypeIds.get('Application_of_Registration');
        insert objSR1;
        Apexpages.currentPage().getParameters().put('id',objSR1.Id);
        Apexpages.currentPage().getParameters().put('FlowId',objPageFlow.Id);
        Apexpages.currentPage().getParameters().put('Type','Shareholder');
        Cls_DMS_PgController dms1 = new Cls_DMS_PgController();
        dms1.Add_Amendment_BodyCorporate();
        dms1.Add_Amendment_Individual();
        
        list<Amendment__c> amendlist = new list<Amendment__c> ();
        Amendment__c amnd1 = new Amendment__c (Passport_No__c='AS132',ServiceRequest__c=objSR1.id,Relationship_Type__c='Guardian',Place_of_Registration__c = 'Pakistan',Company_Name__c = 'Danish Test' ,Amendment_Type__c = 'Individual');
        amendlist.add(amnd1);
         Amendment__c amnd2 = new Amendment__c (Passport_No__c='AS132',ServiceRequest__c=objSR1.id,Relationship_Type__c='Council Member',Place_of_Registration__c = 'Pakistan',Company_Name__c = 'Danish Test',Amendment_Type__c = 'Individual');
        amendlist.add(amnd2);
       
        insert amendlist;
        Apexpages.currentPage().getParameters().put('Type','Guardian');
        Cls_DMS_PgController dms12 = new Cls_DMS_PgController();
        dms12.objAmnd = amendlist[1];
        //dms12.objAmnd.Passport_No__c='ASD132';
        
        dms12.Save_Amendment();
        
        
            
    }
    
    public static testmethod void testPGDMSFoundation4() {
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Application_of_Registration','Change_Shareholder_Details','Allotment_of_Shares_Membership_Interest')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        Page_Flow__c objPageFlow = new Page_Flow__c();
        objPageFlow.Name = 'Application_for_Registration';
        objPageFlow.Master_Object__c = 'Service_Request__c';
        objPageFlow.Record_Type_API_Name__c = 'Allotment_of_Shares_Membership_Interest';
        insert objPageFlow;

        Currency__c curr =new Currency__c(Name='US Dollar',Exchange_Rate__c=1.00,Active__c=true);
        insert curr;
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.Authorized_Share_Capital__c = 500000;
        objAccount.Currency__c = curr.id;
        objAccount.BP_No__c = '001234';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Non - financial';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        insert objAccount;
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.Currency_Rate__c = curr.id;
       // objSR1.Send_SMS_To_Mobile__c = '+971123456789';
        objSR1.legal_structures__c ='Foundation';
        objSR1.Entity_Name__c = 'test Foundation';
        objSR1.currency_list__c = 'US Dollar';
        objSR1.Share_Capital_Membership_Interest__c =50000;
        objSR1.No_of_Authorized_Share_Membership_Int__c = 560;
        objSR1.RecordTypeId = mapRecordTypeIds.get('Application_of_Registration');
        insert objSR1;
        Apexpages.currentPage().getParameters().put('id',objSR1.Id);
        Apexpages.currentPage().getParameters().put('FlowId',objPageFlow.Id);
        Apexpages.currentPage().getParameters().put('Type','Shareholder');
        Cls_DMS_PgController dms1 = new Cls_DMS_PgController();
        dms1.Add_Amendment_BodyCorporate();
        dms1.Add_Amendment_Individual();
        
        list<Amendment__c> amendlist = new list<Amendment__c> ();
        Amendment__c amnd1 = new Amendment__c (Passport_No__c='AS132',ServiceRequest__c=objSR1.id,Relationship_Type__c='Council Member',Place_of_Registration__c = 'Pakistan',Company_Name__c = 'Danish Test' ,Amendment_Type__c = 'Individual');
        amendlist.add(amnd1);
         Amendment__c amnd2 = new Amendment__c (Passport_No__c='AS132',ServiceRequest__c=objSR1.id,Relationship_Type__c='Guardian',Place_of_Registration__c = 'Pakistan',Company_Name__c = 'Danish Test',Amendment_Type__c = 'Individual');
        amendlist.add(amnd2);
       
        insert amendlist;
        Apexpages.currentPage().getParameters().put('Type','Council Member');
        Cls_DMS_PgController dms12 = new Cls_DMS_PgController();
        dms12.objAmnd = amendlist[1];
        //dms12.objAmnd.Passport_No__c='ASD132';
        
        dms12.Save_Amendment();
        
        
            
    }

}