/**************************************************************************************************
* Name               : OB_DEDCallout                                                               
* Description        : Apex Class to Invoke the DED Process in Informatica.                                       
* Created Date       : 10 March 2020                                                              
* Created By         : Leeba (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
Version Author              Date            Comment                                                              

**************************************************************************************************/
global without sharing class OB_DEDCallout{

    // This method will reserve the Trade Name in DED
    @future (callout=true)
    public static void reserveTradeName(string ApplicationId,string CompanyNameId){
        list<Company_Name__c > lstCompany = new list<Company_Name__c >();
        string strEnglishName = '';//Leeba
        string strArabicName = '';//لايم كي جي س
        integer strTradeNameFormatID = 563;
        string strActivityGroupCode = '1';
        string strAddedBy = 'Mahmoud';
        string strOwnerPhone = '01026665552';
        string strOwner = 'MAHMOUDB';
        Boolean IsBranch;
        Boolean IsStartup;
        Boolean IsFranchise;
        
        for(HexaBPM__Service_Request__c SR:[Select Id,Setting_Up__c,Business_Activity_Include__c,Franchise__c from HexaBPM__Service_Request__c where Id=:ApplicationId]){
            if(SR.Setting_Up__c == 'Branch')
                IsBranch = true;
            if(SR.Business_Activity_Include__c == 'A start-up entity')
                IsStartup = true;
            if(SR.Setting_Up__c != 'Branch' && SR.Franchise__c == 'Yes')
                IsFranchise = true;
            
        }
        for(Company_Name__c CN:[Select Id,Entity_Name__c,Arabic_Entity_Name__c,Trading_Name__c,Arabic_Trading_Name__c from Company_Name__c where Id=:CompanyNameId]){
            If(IsBranch == true){
                strEnglishName = CN.Trading_Name__c+' (Branch of '+CN.Entity_Name__c+')';
                strArabicName = CN.Arabic_Trading_Name__c;
            } else if (IsStartup == true || IsFranchise == true){
                strEnglishName = CN.Trading_Name__c;
                strArabicName = CN.Arabic_Trading_Name__c;
            }
            else
            {
                strEnglishName = CN.Entity_Name__c;
                strArabicName  = CN.Arabic_Entity_Name__c;
            }
        }
        
        string InputPayload = '{"CoreNameEnglish":"'+strEnglishName+'","CoreNameArabic":"'+strArabicName+'","Remarks":"remarks","Charachteristcs":{"Abbreviation":false,"Arabized":false,"Gulf":false,"Number":false,"Region":false,"TradeMark":false,"ForeignWords":false},"TradeNameFormatID":'+strTradeNameFormatID+',"ActivityGroupCode":"'+strActivityGroupCode+'","MainLicense":null,"Owner":"'+strOwner+'","OwnerPhone":"'+strOwnerPhone+'","AddedBy":"'+strAddedBy+'"}';

        OB_INF_ReserveTradeName_WS.ReserveTradeNameResponse_element objResponse = new OB_INF_ReserveTradeName_WS.ReserveTradeNameResponse_element();
        OB_INF_ReserveTradeName_WS.ReserveTradeNameRequest_element request = new OB_INF_ReserveTradeName_WS.ReserveTradeNameRequest_element();
        request.In_Payload = InputPayload;
        OB_INF_ReserveTradeName_WS.ReserveTradeName_Service objReserveTrade = new OB_INF_ReserveTradeName_WS.ReserveTradeName_Service();
        
            objResponse = objReserveTrade.ReserveTradeName(InputPayload);
            
            String TransactionNumber = objResponse.OP_TransactionNumber;
            String TradeNameNumber = objResponse.OP_TradeNameNumber;
            String ArabicTradeName = objResponse.OP_ArabicTradeName;
            String EnglishTradeName = objResponse.OP_EnglishTradeName;
            string ErrorMessageArabic = objResponse.OP_ArabicMessage;
            string ErrorMessageEnglish = objResponse.OP_EnglishMessage;
            
            Company_Name__c objcomp = new Company_Name__c(Id=CompanyNameId);
            objcomp.OB_TradeNumber__c = decimal.valueOf(TradeNameNumber);
            objcomp.OB_Trade_Name_in_DED_Arabic__c = ArabicTradeName;
            objcomp.OB_Trade_Name_in_DED_English__c = EnglishTradeName;
            objcomp.OB_Reserve_TradeName_Message_Arabic__c = ErrorMessageArabic;
            objcomp.OB_Reserve_TradeName_Message_English__c = ErrorMessageEnglish;                         
            lstCompany.add(objcomp);
            
            if(lstCompany.size()>0)
                    update lstCompany;
            
                                                                     
    }
    
    //This Method will cancel the Reserved Trade Name in DED
    @future (callout=true)
    public static void cancelTradeName(string CompanyNameId){
        list<Company_Name__c > lstCompany = new list<Company_Name__c >();
        Decimal strTradeNumber;
        String strCancledBy = 'MAHMOUDB';
        String strremarks = 'pls cancel';
    
        for(Company_Name__c CN:[Select Id,Entity_Name__c,Arabic_Entity_Name__c,Trading_Name__c,Arabic_Trading_Name__c,OB_TradeNumber__c from Company_Name__c where Id=:CompanyNameId]){
            strTradeNumber = CN.OB_TradeNumber__c;
        }
        string InputPayload = '{"tradeNameNumber":"'+strTradeNumber+'","canceledBy":"'+strCancledBy+'","remark":"'+strremarks+'"}';
            
        OB_INF_CancelTradeName_WS.CancelTradeNameResponse_element objResponse = new OB_INF_CancelTradeName_WS.CancelTradeNameResponse_element();
        OB_INF_CancelTradeName_WS.CancelTradeNameRequest_element request = new OB_INF_CancelTradeName_WS.CancelTradeNameRequest_element();
        request.IN_Input = InputPayload;
        OB_INF_CancelTradeName_WS.CancelTradeName_Service objCancelTrade = new OB_INF_CancelTradeName_WS.CancelTradeName_Service ();
        
            objResponse = objCancelTrade.CancelTradeName(InputPayload);
            String strSuccess = objResponse.P_OP_Success;
            string ErrorMessageArabic = objResponse.P_OP_ArabicMessage;
            string ErrorMessageEnglish = objResponse.P_OP_EnglishMessage;
            
            Company_Name__c objcomp = new Company_Name__c(Id=CompanyNameId);
            if(strSuccess == 'true')
            {
                objcomp.OB_Canceled_Trade_Name__c = true;
                objcomp.Status__c = 'Cancelled';
            }
            objcomp.OB_Cancel_TradeName_Message_Arabic__c = ErrorMessageArabic;
            objcomp.OB_Cancel_TradeName_Message_English__c = ErrorMessageEnglish;
            lstCompany.add(objcomp);
            
            if (lstCompany.size()>0)
                update lstCompany;
                 
    }
    
    // This method will check if the Trade Name is available for use in DED
    webService static void isTradeNameAllowed(string ApplicationId,string CompanyNameId){
        list<Company_Name__c > lstCompany = new list<Company_Name__c >();
        string strEnglishName = '';//Leeba
        string strArabicName = '';//لايم كي جي س
        integer strTradeNameFormatID = 563;
        string strActivityGroupCode = '1';
        string strAddedBy = 'Mahmoud';
        string strOwnerPhone = '01026665552';
        string strOwner = 'MAHMOUDB';
        Boolean IsBranch;
        Boolean IsStartup;
        Boolean IsFranchise;
        
        system.debug('ApplicationId==>' +ApplicationId);
        
        for(HexaBPM__Service_Request__c SR:[Select Id,Setting_Up__c,Business_Activity_Include__c,Franchise__c from HexaBPM__Service_Request__c where Id=:ApplicationId]){
            if(SR.Setting_Up__c == 'Branch')
                IsBranch = true;
            if(SR.Business_Activity_Include__c == 'A start-up entity')
                IsStartup = true;
            if(SR.Setting_Up__c != 'Branch' && SR.Franchise__c == 'Yes')
                IsFranchise = true;
            
        }
        
        system.debug('IsBranch===>' +IsBranch);
        system.debug('IsStartup===>' +IsStartup);
        system.debug('IsFranchise===>' +IsFranchise);
        
        for(Company_Name__c CN:[Select Id,Entity_Name__c,Arabic_Entity_Name__c,Trading_Name__c,Arabic_Trading_Name__c from Company_Name__c where Id=:CompanyNameId]){
            If(IsBranch == true){
                strEnglishName = CN.Trading_Name__c+' (Branch of '+CN.Entity_Name__c+')';
                strArabicName = CN.Arabic_Trading_Name__c;
            } else if (IsStartup == true || IsFranchise == true){
                strEnglishName = CN.Trading_Name__c;
                strArabicName = CN.Arabic_Trading_Name__c;
            }
            else
            {
                strEnglishName = CN.Entity_Name__c;
                strArabicName  = CN.Arabic_Entity_Name__c;
            }
        }
        
        string InputPayload = '{"CoreNameEnglish":"'+strEnglishName+'","CoreNameArabic":"'+strArabicName+'","Remarks":"remarks","Charachteristcs":{"Abbreviation":false,"Arabized":false,"Gulf":false,"Number":false,"Region":false,"TradeMark":false,"ForeignWords":false},"TradeNameFormatID":'+strTradeNameFormatID+',"ActivityGroupCode":"'+strActivityGroupCode+'","MainLicense":null,"Owner":"'+strOwner+'","OwnerPhone":"'+strOwnerPhone+'","AddedBy":"'+strAddedBy+'"}';
        //string strcontentype = 'application/json;charset=UTF-8';
        
        OB_INF_IsTradeNameAllowed_WS.ISTradeNameAllowedResponse_element objResponse = new OB_INF_IsTradeNameAllowed_WS.ISTradeNameAllowedResponse_element();
        OB_INF_IsTradeNameAllowed_WS.ISTradeNameAllowedRequest_element objRequest = new OB_INF_IsTradeNameAllowed_WS.ISTradeNameAllowedRequest_element();
        objRequest.P_IN_Payload = InputPayload;
        //objRequest.IN_Contenttype = strcontentype;
        OB_INF_IsTradeNameAllowed_WS.ISTradeNameAllowed_Service objisTradeAllowed = new OB_INF_IsTradeNameAllowed_WS.ISTradeNameAllowed_Service();
        
            objResponse = objisTradeAllowed.ISTradeNameAllowed(InputPayload);
            
            String strSuccess = objResponse.P_OP_Message;
            string ErrorMessageArabic = objResponse.P_OP_ArabicMessage;
            string ErrorMessageEnglish = objResponse.P_OP_EnglishMessage;
            
            Company_Name__c objcomp = new Company_Name__c(Id=CompanyNameId);
            if(strSuccess == 'true')
            {
                objcomp.OB_Is_Trade_Name_Allowed__c = 'Yes';
                objcomp.OB_Name_Checked_Arabic__c = strArabicName;
                objcomp.OB_Name_Checked_English__c = strEnglishName;
            }
            else if (strSuccess == 'false')
            {
                objcomp.OB_Is_Trade_Name_Allowed__c = 'No';
                objcomp.OB_Name_Checked_Arabic__c = strArabicName;
                objcomp.OB_Name_Checked_English__c = strEnglishName;
            }
            objcomp.OB_TradeName_Allowed_Message_Arabic__c = ErrorMessageArabic;
            objcomp.OB_TradeName_Allowed_Message_English__c = ErrorMessageEnglish;
            lstCompany.add(objcomp);
            
            if (lstCompany.size()>0)
                update lstCompany;
            
            
        
    }
    
}