/******************************************************************************************
 *  Class Name  : ManagmentDetailsPopup
 *  Author      : Sai kalyan Sanisetty 
 *  Company     : DIFC
 *  Date        : 21 NOV 2019        
 *  Description :                   
 ----------------------------------------------------------------------
   Version     Date              Author                Remarks                                                 
  =======   ==========        =============    ==================================
    v1.1    21 NOV 2019           Sai                Initial Version  
*******************************************************************************************/


public with sharing class ManagmentDetailsPopup {
   
   public Service_Request__c SRData{get;set;}
   public boolean isError{get;set;}
   public boolean isConfirmation{get;set;}
   
   public ManagmentDetailsPopup(ApexPages.StandardController controller){
   		
   		isError = false;
   		isConfirmation = false;
   		
   		List<Compliance__c> lstCompliance = new List<Compliance__c>();
   		SRData=(Service_Request__c)controller.getRecord();
   		Service_Request__c eachRequest = new Service_Request__c();
   		eachRequest = [SELECT Customer__c,Customer__r.Contact_Details_Provided__c FROM Service_Request__c where ID=:SRData.ID];
   		
   		if(String.isNotBlank(eachRequest.Customer__c)){
   			
   			lstCompliance = [SELECT Name FROM Compliance__c where Account__c =:eachRequest.Customer__c 
   						     AND Name =:Label.Confirmation_Statement AND Status__c =:Label.Defaulted];
   		}
   		if(eachRequest.Customer__r.Contact_Details_Provided__c ==false && lstCompliance.size() > 0){
   			isError = true;
   		}
   		else if(lstCompliance.size() > 0){
   			isConfirmation = true;
   		}
   }
   
}