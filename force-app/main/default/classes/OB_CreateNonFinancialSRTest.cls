@isTest
public class OB_CreateNonFinancialSRTest {
    @testSetup
    static  void createTestData() {
        // create account
        List<Account> lstNewAccount = new List<Account>();
        lstNewAccount =  OB_TestDataFactory.createAccounts(1);
        insert lstNewAccount;

        // create contact
        List<Contact> lstContacts = new List<Contact>();
        lstContacts = OB_TestDataFactory.createContacts(1,lstNewAccount);
        insert lstContacts;
        
         Opportunity opp = new Opportunity();
		opp.AccountId = lstNewAccount[0].id;
        opp.Name = 'test';
        opp.StageName='Prospecting';
        opp.CloseDate=System.today().addMonths(1);
        insert opp;

        //Create Custom Setting Records
        OB_User_Registration_SR_Map__c customSetMap = new OB_User_Registration_SR_Map__c();
        customSetMap.Name = 'AccountId';
        customSetMap.SourceObjectField__c = 'AccountId';
        customSetMap.TargetObjectField__c = 'HexaBPM__Customer__c';
        insert customSetMap;

        customSetMap = new OB_User_Registration_SR_Map__c();
        customSetMap.Name = 'Mobile';
        customSetMap.SourceObjectField__c = 'Account.Phone';
        customSetMap.TargetObjectField__c = 'HexaBPM__Send_SMS_to_Mobile__c';
        insert customSetMap;

        OB_InPrinciple_SR_Map__c customInPrincipleSetMap = new OB_InPrinciple_SR_Map__c();
        customInPrincipleSetMap.Name = 'AccountId';
        customInPrincipleSetMap.SourceObjectField__c = 'AccountId';
        customInPrincipleSetMap.TargetObjectField__c = 'HexaBPM__Customer__c';
        insert customInPrincipleSetMap;

        customInPrincipleSetMap = new OB_InPrinciple_SR_Map__c();
        customInPrincipleSetMap.Name = 'Mobile';
        customInPrincipleSetMap.SourceObjectField__c = 'Account.Phone';
        customInPrincipleSetMap.TargetObjectField__c = 'HexaBPM__Send_SMS_to_Mobile__c';
        insert customInPrincipleSetMap;
    }
    static testMethod void CreateNonFinancialSRTest() {
        List<Contact> lstContacts = [select id,AccountId,Account.Phone from Contact];
        List<account> lstNewAccount = [select id,Is_Commercial_Permission__c from account];
        test.startTest();
            OB_CreateNonFinancialSR.createNonFinancialSR(lstContacts);
             OB_CreateNonFinancialSR.createNonFinancialSRForExstCon(new map<id,account>{lstContacts[0].id => lstNewAccount[0]});
         OB_CreateNonFinancialSR.createNonFinancialSR(lstContacts);
        lstNewAccount[0].is_commercial_permission__c = 'yes';
        update lstNewAccount[0];
         OB_CreateNonFinancialSR.createNonFinancialSR(lstContacts);
         OB_CreateNonFinancialSR.createNonFinancialSRForExstCon(new map<id,account>{lstContacts[0].id => lstNewAccount[0]});
         OB_CreateNonFinancialSR.createNonFinancialSRForExstConHelper(new map<id,account>{lstContacts[0].id => lstNewAccount[0]});
        test.stopTest();
    }

}