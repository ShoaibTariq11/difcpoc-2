/*
	Author      : Durga Prasad.Kandula
	Date        : 06/April/2014
	ClassName   : Cls_DocumentReupload_StatusChange
	Description : When User Reuploads all the Documents we will change the Status on behalf of the Customer.
	When OTC user creates a document, then reupload optiono will be available to the customer, after he reuploads and click "Return Back to SR" , 
	then this cLass will be invoked and change the step status of "Reupload Step / UPload Step " from awaing reupload to document uploaded	     

*/
public without sharing class Cls_DocumentReupload_StatusChange {
	
	public static string UpdateStatus(String StepId,string StatusId){
		// this method is called in documentviewer.Back_To_SR .
		// This class can be used in task closure program 
		// batch size should be small ex : 3
		// it should be scheduled
		system.debug('StepId==>'+StepId);
		system.debug('StatusId==>'+StatusId);
		
		//From__r.Name = 'Awaiting Re-upload',From__r.Code__c = 'AWAITING_RE_UPLOAD'
		//To__r.Name = 'Document Re-uploaded',To__r.Code__c = 'DOCUMENT_RE_UPLOADED'
		
		string strDMLError;
		string ParentSR_StepId;
		
		string strStatusID;
		string ExtSRStatus;//holds the External SR Status
		string IntSRStatus;//holds the Internal SR Status
		string ParSRStatus;//holds the Parent SR Status
		string ParStepStatus;//holds the Parent Step Status
		string statusType;//holds the Status type
		string statusCode;
  		Id checkStepId;
  		
      	boolean isReuploadRequired = false;
      	boolean isReuploadStat = false;
  		
  		
		Step__c step;
		Service_Request__c GrandParent_SR = new Service_Request__c();
		Step__c GrandParent_SR_Step = new Step__c();
		map<string,Step_Transition__c> mapGrandParentStepTransitions = new map<string,Step_Transition__c>();
  		
		if(StepId!=null && StepId!='' && StatusId!=null && StatusId!=''){
			
			list<Step__c> lstStep = [select id,Parent_Step__c,SR__c,SR__r.Parent_SR__c,SR__r.Parent_SR__r.Parent_SR__c,SR__r.Parent_Step__c,SR__r.Parent_Step__r.SR_Step__c,SR__r.Parent_SR__r.Parent_Step__c,SR__r.Parent_SR__r.Parent_Step__r.SR_Step__c,SR__r.SR_Template__c,SR__r.SR_Template__r.Refund_Item__c,Status__c,SR__r.Summary__c,SR_Step__c,Rejection_Reason__c,Status__r.Name from Step__c where Id=:StepId and SR__c!=null and SR__r.SR_Template__c!=null]; 
    		step = new Step__c();
    		
		    if(lstStep!=null && lstStep.size()>0){
		    	ParentSR_StepId = lstStep[0].SR__r.Parent_Step__c;
		        step = lstStep[0];
		        strStatusID = StatusId;
		        
		        if(step.SR_Step__c!=null){
		        	for(Step_Transition__c CurrentStepTrans:[select Name,Evaluate_Refund__c,Parent_SR_Status__c,Parent_Step_Status__c,SR_Status_External__c,SR_Status_Internal__c,SR_Step__c,Transition__c,Transition__r.From__c,Transition__r.To__c,Transition__r.To__r.Type__c,Transition__r.To__r.Code__c from Step_Transition__c where SR_Step__c=:step.SR_Step__c and Transition__c!=null and Transition__r.To__c=:StatusId]){
        				ExtSRStatus = CurrentStepTrans.SR_Status_External__c;
        				IntSRStatus = CurrentStepTrans.SR_Status_Internal__c;
        				ParSRStatus = CurrentStepTrans.Parent_SR_Status__c;
        				ParStepStatus = CurrentStepTrans.Parent_Step_Status__c;
        				statusType = CurrentStepTrans.Transition__r.To__r.Type__c; // this the pick list value
        				statusCode = CurrentStepTrans.Transition__r.To__r.Code__c; // all caps case sensitive field ex: END, CLOSED
        			}
		        }
		        
		        system.debug('ExtSRStatus==>'+ExtSRStatus);
		        system.debug('IntSRStatus==>'+IntSRStatus);
		        system.debug('ParSRStatus==>'+ParSRStatus);
		        system.debug('ParStepStatus==>'+ParStepStatus);
		        system.debug('statusType==>'+statusType);
		        system.debug('statusCode==>'+statusCode);
		        system.debug('StatusId==>'+StatusId);
		        
		        if(step.SR__c!=null){
		        	system.debug('Parent SR==>'+step.SR__r.Parent_SR__c);
		        	system.debug('Parent Step==>'+step.SR__r.Parent_Step__c);
		        	if(step.SR__r.Parent_SR__c!=null && step.SR__r.Parent_SR__r.Parent_SR__c!=null && step.SR__r.Parent_SR__r.Parent_Step__c!=null){
		        		system.debug('Grand Parent SR==>'+step.SR__r.Parent_SR__r.Parent_SR__c);
		        		system.debug('Grand Parent Step==>'+step.SR__r.Parent_SR__r.Parent_Step__c);
		        		GrandParent_SR.Id = step.SR__r.Parent_SR__r.Parent_SR__c;
		        		GrandParent_SR_Step.Id = step.SR__r.Parent_SR__r.Parent_Step__c;
		        		if(step.SR__r.Parent_Step__c!=null && step.SR__r.Parent_Step__r.SR_Step__c!=null){
		        			for(Step_Transition__c StpTrans:[select Name,Evaluate_Refund__c,Parent_SR_Status__c,Parent_Step_Status__c,SR_Status_External__c,SR_Status_Internal__c,SR_Step__c,Transition__c,Transition__r.From__c,Transition__r.To__c from Step_Transition__c where SR_Step__c=:step.SR__r.Parent_Step__r.SR_Step__c and Transition__c!=null]){
		        				mapGrandParentStepTransitions.put(StpTrans.Transition__r.To__c,StpTrans);
		        			}
		        		}
		        	}
		        	system.debug('mapGrandParentStepTransitions==>'+mapGrandParentStepTransitions);
		        }
		        
		    }
		    
             list<Service_Request__c> checkSRLst = new list<Service_Request__c>();
		      /* Start of Preparing the Save Point */
		        Savepoint Stat_svpoint = Database.setSavepoint();
		      /* End of Preparing the Save Point */
		      try{
		      	if(isReuploadRequired!=true && isReuploadStat==true){
		      		throw new CommonCustomException('Please change at least one  document status to “re-upload” status in SR Documents  before changing the step status for Re-upload Document.');
		      	}
		        system.debug('update2=>'+step);
		        system.debug('strStatusID=>'+strStatusID);
		        
		        system.debug('statusType outside');
		        if(strStatusID!=null && strStatusID!='' && step.Status__c!=strStatusID){
		            //test for open SRs
		            system.debug('statusType=>'+statusType);
		            step.Status__c = strStatusID;
		            system.debug('step==>'+step);
		            list<Service_Request__c> lstSRUpdatable = new list<Service_Request__c>();
		            list<Step__c> lstUpdatableSteps = new list<Step__c>();
		            
		            if(step.Id!=null && step.SR__c!=null){
		                
		                Service_Request__c sr = new Service_Request__c(id=step.SR__c);
		                system.debug('step.Rejection_Reason__c==>'+step.Rejection_Reason__c);
		                if(ExtSRStatus!=null && ExtSRStatus!='')
		                    sr.External_SR_Status__c = ExtSRStatus;
		                if(IntSRStatus!=null && IntSRStatus!='')
		                    sr.Internal_SR_Status__c = IntSRStatus;
		                    
		                if(step.SR__r.Parent_SR__c!=null && ParSRStatus!=null && ParSRStatus!='' && step.SR__r.Parent_SR__c!=step.SR__c){
		                    Service_Request__c ParentSR = new Service_Request__c(id=step.SR__r.Parent_SR__c);
		                    ParentSR.Summary__c = step.Rejection_Reason__c;
		                    ParentSR.Rejection_Reason__c = step.Rejection_Reason__c;
		                    ParentSR.Internal_SR_Status__c = ParSRStatus;
		                    ParentSR.External_SR_Status__c = ParSRStatus;
		                    lstSRUpdatable.add(ParentSR);
		                    system.debug('ParentSR==>'+ParentSR);
		                }   
		                sr.Summary__c = step.Rejection_Reason__c;
		                sr.Rejection_Reason__c = step.Rejection_Reason__c;
		                lstSRUpdatable.add(sr);
		                 system.debug('sr==>'+sr);
		            }
		            if(ParentSR_StepId!=null && step.Id!=ParentSR_StepId && ParStepStatus!=null && ParStepStatus!=''){
		            	if(GrandParent_SR!=null && GrandParent_SR.Id!=null && GrandParent_SR_Step!=null && GrandParent_SR_Step.Id!=null){
		                	if(mapGrandParentStepTransitions!=null && mapGrandParentStepTransitions.get(ParStepStatus)!=null){
		                		if(mapGrandParentStepTransitions.get(ParStepStatus).Parent_Step_Status__c!=null){
		                			GrandParent_SR_Step.Status__c = mapGrandParentStepTransitions.get(ParStepStatus).Parent_Step_Status__c;
		                			lstUpdatableSteps.add(GrandParent_SR_Step);
		                		}
		                		if(mapGrandParentStepTransitions.get(ParStepStatus).Parent_SR_Status__c!=null){
		                			GrandParent_SR.External_SR_Status__c = mapGrandParentStepTransitions.get(ParStepStatus).Parent_SR_Status__c;
		                			GrandParent_SR.Internal_SR_Status__c = mapGrandParentStepTransitions.get(ParStepStatus).Parent_SR_Status__c;
		                			lstSRUpdatable.add(GrandParent_SR);
		                		}
		                	}
		                }
		                
		                Step__c parentStp = new Step__c(id=ParentSR_StepId);
		                parentStp.Status__c = ParStepStatus;
		                lstUpdatableSteps.add(parentStp);
		            }
		            system.debug('GrandParent_SR==>'+GrandParent_SR);
		            system.debug('step==>'+step);
		          
		            lstUpdatableSteps.add(step);
		            system.debug('step==>'+step);
		            
		            if(lstSRUpdatable!=null && lstSRUpdatable.size()>0){
		            	for(Service_Request__c ser:lstSRUpdatable){
		            		system.debug('ser==>'+ser.Id);
		            	}
		                update lstSRUpdatable;
		            }
		            
		             if(lstUpdatableSteps!=null && lstUpdatableSteps.size()>0){
		                update lstUpdatableSteps;
		            }
		            
		            if(statusType!=null){
		            	statusType = statusType.TolowerCase();
		            	checkStepId = step.Id;
		            	if(statusType=='end'){
		            		checkSRLst = [SELECT id,Parent_Step__c,isClosedStatus__c FROM Service_Request__c WHERE Parent_Step__c = :checkStepId AND isClosedStatus__c=FALSE AND Parent_SR__c=:step.SR__c];
		            	}
		            }
		            system.debug('checkSRLst=>'+checkSRLst);
		            if(checkSRLst!=null && checkSRLst.size()>0){
		            	  system.debug('in here');
		            	for(Service_Request__c checkSR : checkSRLst){
		            		if(checkSR.Parent_Step__c ==  step.Id){
		            			throw new CommonCustomException('Step cannot be closed as there are open subSRs linked to it');
		            		}
		            	}
		            }
		            
		        }
		        strDMLError = 'Success';
		      }catch(Exception e){
		            system.debug('Exception==>'+e.getMessage() + ' at Line: ' + e.getLineNumber());
		            strDMLError = string.valueOf(e.getMessage()) + ' at Line: ' + e.getLineNumber();
		            system.debug('strDMLError==>'+strDMLError);
		            /* Reverting the Executed Code by invoking the Save Point */
		            Database.rollback(Stat_svpoint);
		      }
		}
		return strDMLError;
	}
	
}