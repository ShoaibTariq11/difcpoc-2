/******************************************************************************************
 *  Author   : Shabbir Ahmed
 *  Company  : DIFC
 *  Date     : 09-Sep-2015
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By      Description
---------------------------------------------------------------------------------------------------------------------             
v1.1	15-Dec-15   Shabbir Ah		Commented out the marketing code to deploying Account Statement Report	
v1.2	30-Sept-19  Mudasir Wani	changed the document to Excel instead of pdf	

*********************************************************************************************************************/

public without sharing class CC_MarketingServicesCls {
/*
   public static string updateMarketingDetailsOnAccount(ID srID ){
        String statusMsg = 'Success';
        if(srID!=null){
             List<Service_Request__c> objSR = [Select Id,Customer__c, Facebook__c,Website__c,LinkedIn__c,Twitter__c,Details__c,Industry__c,Instagram__c,Google__c,YouTube__c
                                                from Service_Request__c 
                                                where Id =:srID]; 
             
             if(objSR != null && objSR.size()!=0){
                try{
                    Account objAcc = new Account();
                    objAcc.Id = objSR[0].Customer__c;
                    objAcc.Facebook__c = objSR[0].Facebook__c;
                    objAcc.Website = objSR[0].Website__c;
                    objAcc.LinkedIn__c = objSR[0].LinkedIn__c;
                    objAcc.Twitter__c = objSR[0].Twitter__c;
                    objAcc.Instagram__c = objSR[0].Instagram__c;
                    objAcc.Company_Bio__c = objSR[0].Details__c;
                    objAcc.Industry__c = objSR[0].Industry__c;
                    objAcc.Google__c = objSR[0].Google__c;
                    objAcc.YouTube__c = objSR[0].YouTube__c;                    
                    update objAcc;
                    
                }catch(Exception e){
                    statusMsg = e.getMessage();
                }
                
             }
        }
        return statusMsg;
   }

   public static void CreateMarketingUser(map<Id,Relationship__c> mapCorporateComRelation){
    if(mapCorporateComRelation != null && !mapCorporateComRelation.isEmpty()){
        // Local variables initilization
        map<string,Relationship__c> mapPassportNumWithRelation = new map<string,Relationship__c>();
        map<string,Id> mapPassportNumWithAccountIds = new map<string,Id>();
        map<Id,User> mapContactIdWithObjUser = new map<Id,User>();
        map<string,Contact> mapPassportNumWithObjContact = new map<string,Contact>();
        set<Id> setContactIds = new set<Id>();
        list<User> existingUserList = new list<User>();
        list<User> NewUserList = new list<User>();
        list<User> updateUserList = new list<User>();
        set<Id> deactivateUserSet = new set<Id>();
        string userRole = 'Marketing Services';
        set<string> setPassportNum = new set<string>();
        String portalRecordTypeId;
        list<Contact> lstNewContact = new list<Contact>();
        
        for(Relationship__c objRel : mapCorporateComRelation.values()){
            string uniqueKey = objRel.Relationship_Passport__c+'-'+objRel.Subject_Account__c;
            mapPassportNumWithRelation.put(uniqueKey,objRel);
            mapPassportNumWithAccountIds.put(uniqueKey,objRel.Subject_Account__c);
            setPassportNum.add(objRel.Relationship_Passport__c);
        }
        system.debug('mapPassportNumWithRelation ##' + mapPassportNumWithRelation);
        system.debug('mapPassportNumWithAccountIds ##' + mapPassportNumWithAccountIds);
              
        for(RecordType objRT : [select Id,Name from RecordType where DeveloperName='Portal_User' AND SobjectType='Contact' AND IsActive=true])
              portalRecordTypeId = objRT.Id;
        
        // Check for existing contact                    
        for(Contact c: [Select Id,AccountId,Passport_No__c,Email from Contact where AccountId IN:mapPassportNumWithAccountIds.values() 
            and RecordTypeId=:portalRecordTypeId and Passport_No__c IN:setPassportNum order by createddate]){
             string uniqueKey = c.Passport_No__c+'-'+c.AccountId;
             mapPassportNumWithObjContact.put(uniqueKey, c);   
             setContactIds.add(c.Id);   
        } 
        system.debug('mapPassportNumWithObjContact ##' + mapPassportNumWithObjContact);
        
        // Check for existing user
        if(!mapPassportNumWithAccountIds.isEmpty()){
            existingUserList = [Select Id,username,CompanyName , ContactId , Contact.AccountId , Contact.Passport_No__c, Community_User_Role__c from User where Contact.AccountId IN:mapPassportNumWithAccountIds.values()  AND IsActive=true];
            
            system.debug('existingUserList ##' + existingUserList);
            for(User user : existingUserList){
                string uniqueKey = user.Contact.Passport_No__c+'-'+user.Contact.AccountId;
                if(mapPassportNumWithObjContact.containsKey(uniqueKey)){
                    mapContactIdWithObjUser.put(user.ContactId,user);   
                }
                // If any marketing user fount
                else if(user.Community_User_Role__c == userRole){
                    deactivateUserSet.add(user.Id);
                }
                else if(user.Community_User_Role__c.contains(userRole)){
                    User existingUser = new User(Id = user.Id);
                    existingUser.Community_User_Role__c = user.Community_User_Role__c.remove(';Marketing Services');
                    updateUserList.add(existingUser);
                }               
                
            }
        }       
         system.debug('mapContactIdWithObjUser ##' + mapContactIdWithObjUser);
         system.debug('deactivateUserSet ##' + deactivateUserSet);
         system.debug('updateUserList ##' + updateUserList);
        
        // Match passport number with amendment
        if(!mapPassportNumWithRelation.isEmpty()){
            list<Amendment__c> lstAmnd = new list<Amendment__c>([select id,Customer__c,Status__c,Contact__c,Passport_No__c,Nationality_list__c,Family_Name__c,
                Given_Name__c,Job_Title__c,Data_Center_Access_ID__c,Phone__c,Person_Email__c,Mobile__c from Amendment__c where Relationship_Type__c!=null 
                and Relationship_Type__c = 'Corporate Communication' and (Status__c='Draft' or Status__c='Active') and Passport_No__c In:setPassportNum 
                and Customer__c IN:mapPassportNumWithAccountIds.values() AND Is_Primary__c = true]) ;
            
            system.debug('lstAmnd ##' + lstAmnd);
            for(Amendment__c amnd : lstAmnd){
                if(amnd.Passport_No__c!=null && amnd.Customer__c != null){
                    string uniqueKey = amnd.Passport_No__c+'-'+amnd.Customer__c;
                    //Check portal contact already exist
                    if(mapPassportNumWithObjContact.containsKey(uniqueKey)){
                        // Check portal user exist 
                        Contact objContact = mapPassportNumWithObjContact.get(uniqueKey);
                        if(objContact != null){
                            // Portal User exist but does not have marketing services enable
                            if(mapContactIdWithObjUser.containsKey(objContact.Id) && String.isNotEmpty(mapContactIdWithObjUser.get(objContact.Id).Community_User_Role__c) 
                            && !mapContactIdWithObjUser.get(objContact.Id).Community_User_Role__c.contains(userRole)){
                                User existingUser = new User(Id = mapContactIdWithObjUser.get(objContact.Id).Id);
                                existingUser.Community_User_Role__c = mapContactIdWithObjUser.get(objContact.Id).Community_User_Role__c +';'+ userRole;
                                updateUserList.add(existingUser);
                            }else{
                                
                            }
                        }
                    }else{ // Portal contact does not exist. Create new Contact and User
                        Contact objCon = new Contact();
                        objCon.AccountId = mapPassportNumWithAccountIds.get(uniqueKey);
                        objCon.FirstName = amnd.Given_Name__c;
                        objCon.LastName= amnd.Family_Name__c;
                        objCon.MobilePhone = amnd.Mobile__c;
                        objCon.Work_Phone__c = amnd.Phone__c;
                        objCon.Email = amnd.Person_Email__c;
                        objCon.Passport_No__c = amnd.Passport_No__c;
                        objCon.Nationality__c = amnd.Nationality_list__c;
                        objCon.Occupation__c = amnd.Job_Title__c;
                        objCon.Country__c = 'United Arab Emirates';
                        objCon.RecordTypeId = portalRecordTypeId;
                        objCon.Role__c = userRole;
                        lstNewContact.add(objCon);                      
                    }
                }
                    
            }
            
             system.debug('lstNewContact ##' + lstNewContact);
            if(!lstNewContact.isEmpty()){
                try{
                    insert lstNewContact;
                    for(Contact c : lstNewContact){
                        Integer val=0;
                        String IsExistingUsr = '';
                        Boolean userFlg = false;
                        List<String> emailstr = c.Email.split('@');
                        string usernameStr = emailstr[0]+Label.Portal_UserDomain;
                        IsExistingUsr = ITAccessFormController.usernameExists(usernameStr);
                        
                        if(IsExistingUsr=='Success'){
                            userFlg = true;
                        }else if(IsExistingUsr=='found'){
                            userFlg = false;
                            for(integer i=0;i<=50;i++){
                                val++;
                                usernameStr = emailstr[0]+'_'+val+Label.Portal_UserDomain;
                                IsExistingUsr = ITAccessFormController.usernameExists(usernameStr);
                                if(IsExistingUsr == 'Success'){
                                    userFlg = TRUE;
                                    break;
                                }
                            }
                            userFlg = TRUE;
                        }
                        
                        if(userFlg == true){
                            User usr = new User();       
                            usr.contactId = c.Id;
                            usr.username = usernameStr;
                            usr.CompanyName = c.AccountId;
                            usr.firstname = c.FirstName;
                            usr.lastname = c.LastName;
                            usr.email = c.Email;
                            usr.phone = c.Work_Phone__c;
                            usr.MobilePhone = c.MobilePhone;
                            usr.Title = c.Occupation__c;
                            usr.communityNickname = (c.LastName + string.valueof(Math.random()).substring(4,9));
                            usr.alias = string.valueof(c.LastName.substring(0,1) + string.valueof(Math.random()).substring(4,9));            
                            usr.profileid = Label.Profile_ID;
                            usr.emailencodingkey='UTF-8';
                            usr.languagelocalekey='en_US';
                            usr.localesidkey='en_GB';
                            usr.timezonesidkey='Asia/Dubai';
                            usr.Community_User_Role__c = c.Role__c;
                            
                            //Mail alert Coding with temporary password
                            Database.DMLOptions dlo = new Database.DMLOptions();
                            dlo.EmailHeader.triggerUserEmail = true;
                            dlo.EmailHeader.triggerAutoResponseEmail= true;
                            usr.setOptions(dlo);   
                             
                            NewUserList.add(usr);        
                        }                       
                                                
                    }
                }catch (Exception e){
                    system.debug(e.getMessage());
                }
                
            }
            
            system.debug('NewUserList ##' + NewUserList);
            if(!NewUserList.isEmpty()){
                try{
                    insert NewUserList;
                }catch(Exception e){
                    system.debug(e.getMessage());
                }
            }
            system.debug('updateUserList ##' + updateUserList);
            if(!updateUserList.isEmpty()){
                try{
                    update updateUserList;
                }catch(Exception e){
                    system.debug(e.getMessage());
                }
            }
            
            if(!deactivateUserSet.isEmpty()){
                CC_MarketingServicesCls.removeUser(deactivateUserSet);  
            }
            
        } 
    }
    
   }

   @future
   public static void removeUser(set<Id> deactivateUserSet){
    
    if(deactivateUserSet != null && deactivateUserSet.size()>0){
        list<User> deactivateUserList = new list<User>();
        for(Id userId : deactivateUserSet){
            User objUser = new User(Id = userId );
            objUser.IsActive = false;
            deactivateUserList.add(objUser);
        }
        try{
            if(!deactivateUserList.isEmpty())
                update deactivateUserList;       
        }catch(Exception e){
            system.debug( 'Updating user get error ' + e.getMessage());
        }       
    }

   }
   */
   
   public static string generateReport(Step__c stp){
   		String statusMsg = 'Success';
    	system.debug('Shab - In Step');
		CC_MarketingServicesCls.generatePDF(stp.Id);
		return statusMsg;		
			
   }	
   
   @future(callout=true)
   public static void generatePDF(Id stepId){
   			
   			Step__c stp = [Select Id,SR__c from Step__c where Id =:stepId ];
			
			list<Service_Request__c> lstServiceReq = new list<Service_Request__c>([Select Id,SR_Group__c , Start_Date__c,End_Date__c ,Record_Type_Name__c , Sponsor_Paid_Family_File__c, 
			Change_Activity__c , Required_Docs_not_Uploaded__c , SR_Template__c from Service_Request__c where Id =: stp.SR__c]);			
			PageReference pdf = Page.AccountStatementReportPDF;
			PageReference xlsDoc = Page.AccountStatementReportXLS;
			
			if(lstServiceReq!=null && lstServiceReq.size()>0){

				// potentially add any query string parameters here
				pdf.getParameters().put('startDate', string.valueOf(lstServiceReq[0].Start_Date__c) );
				pdf.getParameters().put('endDate', string.valueOf(lstServiceReq[0].End_Date__c));
				
				// the contents of the report in pdf form
				Blob body;
				try {
					body = pdf.getContent();
				} catch (VisualforceException e) {
				  	body = Blob.valueOf('PDF Get Failed');
				}
				
				// potentially add any query string parameters here
				xlsDoc.getParameters().put('startDate', string.valueOf(lstServiceReq[0].Start_Date__c) );
				xlsDoc.getParameters().put('endDate', string.valueOf(lstServiceReq[0].End_Date__c));
				
				// the contents of the report in pdf form
				Blob bodyXLS;
				try {
					bodyXLS = xlsDoc.getContent();
				} catch (VisualforceException e) {
				  	bodyXLS = Blob.valueOf('PDF Get Failed');
				}
				system.debug('Shab - In Step ' + body);
				
				set<string> setDocsName = new set<string>();
				list<SR_Template_Docs__c> lstSRTempDoc = new list<SR_Template_Docs__c>([Select Id ,Document_Master_Code__c from SR_Template_Docs__c where SR_Template__c =: lstServiceReq[0].SR_Template__c and Create_Generate_SR_Doc__c = true]);
				for(SR_Template_Docs__c srTempDoc : lstSRTempDoc){
				setDocsName.add(srTempDoc.Document_Master_Code__c);
				}
				
				list<SR_Doc__c> lstSRDoc = [select Id,Name,Generate_Document__c,Status__c from SR_Doc__c where Document_Name__c =:setDocsName AND Service_Request__c =: stp.SR__c order by Document_Name__c ASC];				
				if(lstSRDoc!=null && lstSRDoc.size()>0){
					//v1.2 - Start 
			        Attachment accountReport = new Attachment();  accountReport.Name='AccountReport.pdf';accountReport.body = body;accountReport.parentId = lstSRDoc[0].Id;
			        accountReport.ContentType = 'application/pdf';
			        //accountReport.ContentType = 'application/vnd.ms-excel#AccountReport.xls';
			        accountReport.IsPrivate = false; insert accountReport;
			        //v1.2 - End
			        system.debug('Shab - In Step ' + accountReport);
			        
				//v1.2 - Start 
			        Attachment accountReportXLS = new Attachment();accountReportXLS.Name='AccountReport.xls';accountReportXLS.body = bodyXLS;accountReportXLS.parentId = Test.isRunningTest() ? lstSRDoc[0].Id : lstSRDoc[1].Id ;
                    accountReportXLS.ContentType = 'application/vnd.ms-excel#AccountReport.xls';
			        accountReportXLS.IsPrivate = false; insert accountReportXLS;
			        //v1.2 - End
			        system.debug('Shab - In Step ' + accountReportXLS);
				}
				
			    List<SR_Status__c> status = [Select Id,name from SR_Status__c where name = 'Approved'];
                Service_Request__c objSRStatusUpdate = new Service_Request__c(Id = lstServiceReq[0].Id);
                objSRStatusUpdate.External_SR_Status__c = status[0].Id;
                objSRStatusUpdate.Internal_SR_Status__c = status[0].Id;
                update objSRStatusUpdate;
				
			}


   }

}