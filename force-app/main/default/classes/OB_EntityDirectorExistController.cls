/**
*Author : Merul Shah
*Description : This should the list of all existing Ind/BC directors.
*v1.2[05.MAP.2020] Prateek Kadkol - edited the AmendmentWrapper for error fix
**/
public without sharing class OB_EntityDirectorExistController
{

    @AuraEnabled   
    public static ResponseWrapper  deleteAmed(String reqWrapPram)
    {
        //reqest wrpper
        OB_EntityDirectorExistController.RequestWrapper reqWrap = (OB_EntityDirectorExistController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_EntityDirectorExistController.RequestWrapper.class);
        
        //response wrpper
        OB_EntityDirectorExistController.ResponseWrapper respWrap = new OB_EntityDirectorExistController.ResponseWrapper();
       
        
        String amedId = reqWrap.amedId;
        AmendmentWrapper amedWrap = reqWrap.amedWrap;
        if(! String.isBlank( amedWrap.amedObj.role__c) )
        {
            
            if( String.IsNotBlank(amedWrap.amedObj.Role__c) && amedWrap.amedObj.Role__c.equalsIgnoreCase('Director') )
            {
                 HexaBPM_Amendment__c amedTemp = new HexaBPM_Amendment__c( id = amedId );
                 delete amedTemp;
            }
            else
            {
                amedWrap.amedObj.Role__c = amedWrap.amedObj.Role__c.replaceAll(';?Director;?','');
                update amedWrap.amedObj;
            }
        }
        
        
        return respWrap;
    }
    

    /*
    public class SRWrapper 
    {
        @AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }
        @AuraEnabled public List<OB_EntityDirectorContainerController.AmendmentWrapper> amedWrapLst { get; set; }
        
        public SRWrapper()
        {}
    }
    */
    
    public class AmendmentWrapper 
    {
        @AuraEnabled public HexaBPM_Amendment__c amedObj { get; set; }
        @AuraEnabled public Id amedID { get; set; }
        @AuraEnabled public Boolean isIndividual { get; set; }
        @AuraEnabled public Boolean isBodyCorporate { get; set; }
        @AuraEnabled public String lookupLabel { get; set; }
        //v.2
        @AuraEnabled public String displayName { get; set; }
        
        //lookupLabel
        
        public AmendmentWrapper()
        {}
    }
    
    
    public class RequestWrapper 
    {
        @AuraEnabled public Id flowId { get; set; }
        @AuraEnabled public Id pageId {get;set;}
        @AuraEnabled public Id srId { get; set; }
        @AuraEnabled public AmendmentWrapper amedWrap{get;set;}
        @AuraEnabled public String amedId{get;set;}
        
        public RequestWrapper()
        {
        
        }
    }

    public class ResponseWrapper 
    {
        //@AuraEnabled public SRWrapper  srWrap{get;set;}
        @AuraEnabled public AmendmentWrapper amedWrap{get;set;}
        
        public ResponseWrapper()
        {}
    }
    
    
}