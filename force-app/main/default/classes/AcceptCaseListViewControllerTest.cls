@isTest
private class AcceptCaseListViewControllerTest{
    static testmethod void test1(){
        Case objCase = new Case();
        objCase.Subject = 'test subject';
        insert objCase;       
        
        List<Case> caseList = new List<Case>();
        caseList.add(objCase);
        
        test.startTest();
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(caseList);
            sc.setSelected(caseList);   
            AcceptCaseListViewController ctrlr = new AcceptCaseListViewController(sc);  
            ctrlr.acceptCases();
            ctrlr.updateCases();
        test.stopTest();
    }
    
    static testmethod void test2(){
    
        Group testGroup = new Group(Name='QUEUE NAME', Type='Queue');
        insert testGroup;
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            //Associating queue with group AND to the Case object
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Case');
            insert testQueue;
        }
    
        Case objCase = new Case();
        objCase.Subject = 'test subject';
        objCase.Status = 'Invalid';
        objCase.OwnerId = testGroup.id;
        insert objCase;       
        
        List<Case> caseList = new List<Case>();
        caseList.add(objCase);
        
        test.startTest();
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(caseList);
            sc.setSelected(caseList);   
            AcceptCaseListViewController ctrlr = new AcceptCaseListViewController(sc);  
            ctrlr.acceptCases();
            ctrlr.updateCases();
        test.stopTest();
    }
    
    static testmethod void test3(){
    
        Group testGroup = new Group(Name='QUEUE NAME', Type='Queue');
        insert testGroup;
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            //Associating queue with group AND to the Case object
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Case');
            insert testQueue;
        }
    
        Case objCase = new Case();
        objCase.Subject = 'test subject';
        objCase.Status = 'New';
        objCase.OwnerId = testGroup.id;
        insert objCase;       
        
        List<Case> caseList = new List<Case>();
        caseList.add(objCase);
        
        test.startTest();
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(caseList);
            sc.setSelected(caseList);   
            AcceptCaseListViewController ctrlr = new AcceptCaseListViewController(sc);  
            ctrlr.acceptCases();
            ctrlr.updateCases();
        test.stopTest();
    }
}