/******************************************************************************************
 *  Name        : Cls_SearchContactExtension 
 *  Author      : Danish Farooq
 *  Date        : 2018-06-04
 *  Description : Extension Class for searching Active Clients
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description

*******************************************************************************************/
public virtual class Cls_SearchContactExtension {
  
  public String currentAccountId            {get;set;}
  public String ClientName {get;set;}
  
  public String accountStatus              {get;set;}
  
  public Boolean searchWithLicenseNo          {get;set;}
  public Boolean isSameClient              {get;set;}
  
  public list<HostingWrapper> HostingCompanies     {get;set;}
  
  public Cls_SearchContactExtension (){
    
    searchWithLicenseNo = false;
    isSameClient = false;
    
  }
  
  public void clearList(){
    
    HostingCompanies.clear();
    
  }
  
  public virtual void SearchContact(){
    
        HostingCompanies = new list<HostingWrapper>();
        
        ClientName  = String.isNotBlank(ClientName) ? ClientName .trim() : '';
        system.debug('%%%%%%%' +ClientName  );
          system.debug('%%%%%%%' +currentAccountId );
        HostingWrapper objHostingWrapper;
      
                                
         List<Relationship__c> lstRel = [Select Id,Object_Contact__c,Object_Contact__r.FirstName,
                                         Object_Contact__r.LastName from Relationship__c where
                                         Subject_Account__c =: currentAccountId AND
                                         Object_Contact__r.RecordType.DeveloperName = 'GS_Contact' AND
                                         Active__c = true AND (
                                         Object_Contact__r.LastName like :'%'+ClientName  +'%' OR
                                         Object_Contact__r.FirstName like :'%'+ClientName  +'%' )];
                                         
      
        
        for(Relationship__c objCon : lstRel ){ // V1.9 - Claude - new query
        
            objHostingWrapper = new HostingWrapper();
            objHostingWrapper.ClientName = (objCon.Object_Contact__r.FirstName != NULL) ?  objCon.Object_Contact__r.FirstName+' '+objCon.Object_Contact__r.LastName : objCon.Object_Contact__r.LastName;
            objHostingWrapper.ClientID = objCon.Object_Contact__C;
            HostingCompanies.add(objHostingWrapper);
        }
    }
    
    public class HostingWrapper{
        
        public string ClientID {get;set;}
        public string ClientName {get;set;}
        public String RegistrationNumber   {get;set;}
    }
    
}