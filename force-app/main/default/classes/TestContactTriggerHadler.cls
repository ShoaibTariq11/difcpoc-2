@isTest(seeAllData=false)
private class TestContactTriggerHadler {

  static testMethod void myUnitTest() {
         
      
         
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        List<contact> ListobjContact = new List<contact>();
        
       ID BDrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Business Development').getRecordTypeId(); //Added as per V1.2
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Name';
        objContact.AccountId = objAccount.Id; 
        objContact.Passport_No__c = 'ABC12345';
        objContact.RecordTypeId = BDrectypeid ;
        objContact.important__C= true;
        objContact.Is_Active__c= true;
        ListobjContact.add (objContact );
        insert ListobjContact;
        
        ContactTriggerHadler.IsImportantCheck(ListobjContact, false);
        
  
  }
  
  
  static testMethod void testAccountTrigger() {
  
           //Getting the record type ids of Contact
        ID ROCrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Individual Contact').getRecordTypeId();
        ID GSrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        ID RORPrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('RORP Contact').getRecordTypeId();        
        ID BDrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Business Development').getRecordTypeId(); //Added as per V1.2
        
        
        List<contact> ListobjContact = new List<contact>();
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Name';
        objContact.RecordTypeId = GSrectypeid ;
        objContact.important__C= false;
        ListobjContact.add (objContact );
        
        objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Name';
        objContact.RecordTypeId = ROCrectypeid ;
        objContact.important__C= false;
        ListobjContact.add (objContact );
        
        objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Name';
        objContact.RecordTypeId = BDrectypeid ;
        objContact.important__C= false;
        ListobjContact.add (objContact );
        
        objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Name';
        objContact.RecordTypeId = RORPrectypeid ;
        objContact.important__C= false;
        ListobjContact.add (objContact );
        insert ListobjContact;
         
  
  }


}