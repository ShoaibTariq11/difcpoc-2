global class AccountConversionBatch implements Database.Batchable<sObject>
{

   global final String Query;
   global final String Datatype;

   global AccountConversionBatch(String q, String e)
   {
    Query=q; 
    Datatype=e;

   
   }

   global Database.QueryLocator start(Database.BatchableContext BC)
   {
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope)
   {
       if(Datatype=='Sr')
       {
           for(Account ObjAccount:(List<Account>)scope)
           {
             
        //string Accountid='0012000001H3yaVAAR';
        //Account ObjAccount=[select name,Old_Company_Name__c,Legal_Type_of_Entity__c,Active_License__r.Name,Active_License__r.License_Expiry_Date__c from account where id=:Accountid];


                SR_Template__c SR_Template=[select id,SR_RecordType_API_Name__c from  SR_Template__c where SR_RecordType_API_Name__c='Company_Conversion_Certificates'];

                Map<string,string> srStatusMap = new map<string,string>(); // Map with status code as Key and Id as Value ** SR Statuses **

                for(SR_Status__c srStatus: [select id,code__c from SR_Status__c where Code__c IN  ('SUBMITTED','DRAFT')])
                {
                srStatusMap.put(srStatus.code__c,srStatus.id);
                }

                List<Contact> ListContact=[select id,Email,MobilePhone,phone from contact where Accountid=:ObjAccount.id and Is_User_Active__c=true and Role__c like '%Company Services%'];

                System.debug('ListContact==>'+ListContact);     

                if(!ListContact.isEmpty())
                {
                Contact ObjContact=ListContact[0];

                service_request__c serviceRequest = new Service_Request__c(SR_group__c='ROC'); // 
                serviceRequest.SR_Template__c =  SR_Template.id;
                serviceRequest.Customer__c = ObjAccount.id;
                serviceRequest.Legal_Structures__c=ObjAccount.Legal_Type_of_Entity__c;
                serviceRequest.License_Number__c = ObjAccount.Active_License__r.Name ;
                serviceRequest.Current_License_Expiry_Date__c = ObjAccount.Active_License__r.License_Expiry_Date__c;

                serviceRequest.Email__c = ObjContact.email;

                serviceRequest.Send_SMS_To_Mobile__c =  ObjContact.phone ;
                serviceRequest.Use_Registered_Address__c = true;

                serviceRequest.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Company_Conversion_Certificates').getRecordTypeId();
                serviceRequest.Quantity__c = 0;
                serviceRequest.internal_SR_Status__c = srStatusMap.get('DRAFT');
                serviceRequest.External_sr_status__c = srStatusMap.get('DRAFT');
                serviceRequest.Submitted_Date__c = Date.Today();
                serviceRequest.Submitted_DateTime__c = dateTime.Now();
                insert serviceRequest;
                System.debug('===serviceRequest============>'+serviceRequest);

                serviceRequest.internal_SR_Status__c = srStatusMap.get('SUBMITTED');
                serviceRequest.External_sr_status__c = srStatusMap.get('SUBMITTED');
                update serviceRequest;

                }

           }
              
                         
       }
       if(Datatype=='steps')
       {
           List<Step__c> ListSteps=scope;
             for(Step__c  stp:ListSteps)
           {
           
           //select id,SR__c,Customer_Name__c,SR_Step__c,Applicant_Email__c from Step__c where 
           
                CC_ROCCodeCls.EmailToCSUsersWithAtt(string.valueof(stp.id),string.valueof(stp.SR__c),string.valueof(stp.Customer_Name__c),
                string.valueof(stp.SR_Step__c) ,stp.Applicant_Email__c );
                stp.Status__c='a1M20000001iL9O';
           }
           update ListSteps;
           
       }
     
     
    }

   global void finish(Database.BatchableContext BC){
   }
}