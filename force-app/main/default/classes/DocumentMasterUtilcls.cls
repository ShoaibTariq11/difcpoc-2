/*
  Author    :  Durga Prasad
  Class Name  :  DocumentMasterUtilcls
  Description  :  This is class will handles the Document Master content.
  
*/
public without sharing class DocumentMasterUtilcls {
  public Document_Master__c Fetch_DocumentMaster(string DocMasterCode){
    Document_Master__c DocumentMaster = new Document_Master__c();
    for(Document_Master__c objDM:[select id,Name,Description__c,LetterTemplate__c,Code__c from Document_Master__c where Code__c=:DocMasterCode]){
      DocumentMaster = objDM;
    }
    return DocumentMaster;
  }
  public map<string,Document_Master__c> Fetch_Specific_DocumentMasters(set<string> DocMasterCodes){
    map<string,Document_Master__c> mapDocumentMasters = new map<string,Document_Master__c>();
    for(Document_Master__c objDM:[select id,Name,Description__c,LetterTemplate__c,Code__c from Document_Master__c where Code__c IN:DocMasterCodes]){
      mapDocumentMasters.put(objDM.Code__c,objDM);
    }
    return mapDocumentMasters;
  }
  public map<string,Document_Master__c> Fetch_All_DocumentMasters(){
    map<string,Document_Master__c> mapDocumentMasters = new map<string,Document_Master__c>();
    for(Document_Master__c objDM:[select id,Name,Description__c,LetterTemplate__c,Code__c from Document_Master__c where Code__c!=null]){
      mapDocumentMasters.put(objDM.Code__c,objDM);
    }
    return mapDocumentMasters;
  }
}