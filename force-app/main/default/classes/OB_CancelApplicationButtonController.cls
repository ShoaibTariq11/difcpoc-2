public with sharing class OB_CancelApplicationButtonController {
@AuraEnabled
public static RespondWrap applicationStatusCheck(String requestWrapParam)  {

	//declaration of wrapper
	RequestWrap reqWrap = new RequestWrap();
	RespondWrap respWrap =  new RespondWrap();

	//deseriliaze.
	reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);

	for(HexaBPM__Service_Request__c srOBj : [SELECT id,HexaBPM__IsClosedStatus__c,HexaBPM__Is_Rejected__c,  HexaBPM__IsCancelled__c from HexaBPM__Service_Request__c WHERE id=: reqWrap.srId LIMIT 1 ]) {
		if(srOBj.HexaBPM__IsClosedStatus__c == true ||srOBj.HexaBPM__IsCancelled__c == true ||srOBj.HexaBPM__Is_Rejected__c == true  ) {
			respWrap.errorMessage = 'Application is already canceled.';
		}
	}


	return respWrap;
}
@AuraEnabled
public static RespondWrap cancelApplication(String requestWrapParam)  {

	//declaration of wrapper
	RequestWrap reqWrap = new RequestWrap();
	RespondWrap respWrap =  new RespondWrap();

	//deseriliaze.
	reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);

	HexaBPM__Service_Request__c srObjToCancle = new HexaBPM__Service_Request__c();
	for(HexaBPM__Service_Request__c srOBj : [SELECT id,HexaBPM__Customer__c,Opportunity__c from HexaBPM__Service_Request__c WHERE id=: reqWrap.srId LIMIT 1 ]) {
		srObjToCancle = srOBj;
	}

	if(srObjToCancle != null) {
		try{

			//udate account status
			string AccountId = srObjToCancle.HexaBPM__Customer__c;
			if(AccountId != null) {
				account accObj = new account(id=AccountId);
				accObj.ROC_Status__c = 'Withdrawn';
				update accObj;
			}

			//update opportunity stage
			string OpportunityId = srObjToCancle.Opportunity__c;
			if(OpportunityId != null) {
				Opportunity oppObj = new Opportunity (id=OpportunityId);
				oppObj.StageName = 'Withdrawn';
				update oppObj;
			}

			//update sr status

			for(HexaBPM__SR_Status__c srStatusTobeSet : [SELECT id FROM HexaBPM__SR_Status__c where HexaBPM__Code__c = :reqWrap.cancelationType LIMIT 1]) {
				srObjToCancle.HexaBPM__External_SR_Status__c = srStatusTobeSet.id;
                srObjToCancle.HexaBPM__Internal_SR_Status__c = srStatusTobeSet.id;
                srObjToCancle.Cancellation_Reason__c = reqWrap.reasonForCancellation;
            }
            update srObjToCancle;



			//updating open action items

			list<HexaBPM__Step__c> actionItemsToBeUpdated = new list<HexaBPM__Step__c>();
			HexaBPM__Status__c statusToSet = new HexaBPM__Status__c();

			for(HexaBPM__Status__c actionItemStatusObj : [SELECT id FROM HexaBPM__Status__c where HexaBPM__Code__c = 'CANCELLED' LIMIT 1]) {
				statusToSet = actionItemStatusObj;
			}

			for(HexaBPM__Step__c actionItemObj : [SELECT id,HexaBPM__Status__c FROM HexaBPM__Step__c where HexaBPM__SR__c =: srObjToCancle.Id AND HexaBPM__Status_Type__c != 'End' ]) {

				actionItemObj.HexaBPM__Status__c = statusToSet.id;
				actionItemsToBeUpdated.add(actionItemObj);

			}

			if(actionItemsToBeUpdated.size() > 0) {
				update actionItemsToBeUpdated;
			}


		}catch(Exception e) {
			respWrap.errorMessage = e.getMessage()+'';
		}
	}




	return respWrap;
}

// ------------ Wrapper List ----------- //

public class RequestWrap {

@AuraEnabled public String srId;
@AuraEnabled public String reasonForCancellation;  
@AuraEnabled public String cancelationType;                                                                                                                                                                                                                                             public RequestWrap(){
}
}

public class RespondWrap {

@AuraEnabled public string userId;
@AuraEnabled public string errorMessage = '';                                                                                                                                                                                                                                             public RespondWrap(){
}
}
}