/**
Description : This apex class will be used to get the updated amount for user and to get the BP no of account.          

**/
public without sharing class DIFC_TopUpBalance {
    	public static decimal PSADeposit = 0;
        public static decimal PSAAvailable = 0;
        public static decimal dRecpNotSentAmount = 0;
        public static Boolean isFitOutContractor = false;
        public static Date LastUpdatedDate;
    	public static Boolean BalanceFetchSuccessfull = false;
        public static Boolean hasEstablishmentCard = false; // V1.6 - Claude - Added exemption for new establishment card
        public static Boolean isUpdate = false;
    
    @AuraEnabled
    public static Account getBpNumberFromAccount(){
        try{
        	system.debug('====DIFC_TopUpBalance=====');
            DateTime LastUpdatedDateTime;
            String loggedInUser = UserInfo.getUserId();
            list<User> userList = [select contactid,accountid from user where id =:loggedInUser];
            Account accObj;
            system.debug('====userList====='+userList);
            if(userList[0].accountId != null) {
                accObj = [SELECT Name, BP_No__c,Balance_Updated_On__c FROM Account where id =: userList[0].accountid];
            }else{
                return null;
            } 
            system.debug('===accObj=='+accObj);
            return accObj;
        }catch(Exception e){
            return null;
        }
    }
     @AuraEnabled
    public static string getRecordTypeId(){
        String recordTypeID = Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get('Cheque').getRecordTypeId();
        return recordTypeID;
    }
    @AuraEnabled
    public static PriceItem loadPriceItem(string srId, string category){
        DIFC_TopUpBalance.DisplayRecords portalBalanceRecord = new DIFC_TopUpBalance.DisplayRecords();
        portalBalanceRecord = DIFC_TopUpBalance.getTopUpBalanceAmount();
        PriceItem pI = new PriceItem();
        decimal price = 0;
        if(String.isNotBlank(category) && category == 'namereserve'){
            for(HexaBPM__SR_Price_Item__c priceItem :[select HexaBPM__Product__r.Name,HexaBPM__Price__c from HexaBPM__SR_Price_Item__c 
                                                where HexaBPM__Product__r.ProductCode = 'OB Name Reservation'
                                                and HexaBPM__Status__c = 'Added'
                                                 and HexaBPM__ServiceRequest__c = :srId limit 1]){
                price = priceItem.HexaBPM__Price__c;
                ProductInfo prodObj = new ProductInfo();
                prodObj.name = priceItem.HexaBPM__Product__r.Name;
                prodObj.amount = priceItem.HexaBPM__Price__c;
                pI.lstProductInfo.add(prodObj);
            }
        }
        else{
            for(HexaBPM__SR_Price_Item__c srPriceItem : [select HexaBPM__Product__r.Name, HexaBPM__Price__c from HexaBPM__SR_Price_Item__c 
                                                            where HexaBPM__ServiceRequest__c = :srId
                                                            and HexaBPM__Status__c = 'Added'
                                                            and HexaBPM__Product__r.ProductCode != 'OB Name Reservation']){
                price = price + srPriceItem.HexaBPM__Price__c;
                ProductInfo prodObj = new ProductInfo();
                prodObj.name = srPriceItem.HexaBPM__Product__r.Name;
                prodObj.amount = srPriceItem.HexaBPM__Price__c;
                pI.lstProductInfo.add(prodObj);
            }
                     
        }
        pI.amount = price;
        pI.receiptRecordTypeId = DIFC_TopUpBalance.getRecordTypeId();
        if(portalBalanceRecord != null && portalBalanceRecord.portalBalanceValue != null){
            pI.totalBalance = portalBalanceRecord.portalBalanceValue;
        }
        return pI;
    }
    //This method is getting invoked from payWallet method in DIFC_Topupbalance Component
    @AuraEnabled
    public static DIFC_Payment.SRPayment updatePriceItem(string srId, string category){
        
        DIFC_Payment.SRPayment paymentObj = new DIFC_Payment.SRPayment();
        try{
            list<HexaBPM__SR_Price_Item__c> lstSRPriceItem = new list<HexaBPM__SR_Price_Item__c>();
            if(String.isNotBlank(category) && category == 'namereserve'){
                for(HexaBPM__SR_Price_Item__c srPriceItem : [select Id,HexaBPM__Status__c from HexaBPM__SR_Price_Item__c
                                                            where HexaBPM__Product__r.ProductCode = 'OB Name Reservation'
                                                    and HexaBPM__Status__c = 'Added'
                                                    and HexaBPM__ServiceRequest__c = :srId limit 1]){
                    srPriceItem.HexaBPM__Status__c = 'Blocked';
                    lstSRPriceItem.add(srPriceItem);
                }
                if(lstSRPriceItem != null && lstSRPriceItem.size() > 0)
                    update lstSRPriceItem;
                //Create Sub SR
                OB_NamingConventionHelper.CreateNameApprovalSR(srId);

                //Submit CompanyNames for Approval
                DIFC_TopUpBalance.submitCompanyNameForApproval(srId);

                //Setting the Page Tracker once the name reservation is completed.
                OB_NamingConventionHelper.setPageTrackerForNameReservation(srId);

            }
            else{
                paymentObj = DIFC_Payment.updateSRLines(srId);
            }
        } catch (DMLException e){
            paymentObj.isError = true;
            string DMLError = e.getdmlMessage(0) + '';
			if(DMLError == null) {
				DMLError = e.getMessage() + '';
			}
            paymentObj.message = DMLError;
            System.debug('=====>'+ e.getMessage());
        }
        
        return paymentObj;
        /*
        if(lstSRPriceItem != null && lstSRPriceItem.size() > 0)
            update lstSRPriceItem;
            */
        
    }
    public static void submitCompanyNameForApproval(string srId){
        List<Company_Name__c> lstCompNames = new List<Company_Name__c>();
        
        for(Company_Name__c cn: [select id,Entity_Name__c,OwnerId,Ends_With__c,Name,Stage__c,Status__c,Reserved_Name__c from Company_Name__c where Application__c =:srId]){
            cn.Stage__c='Submitted';
            cn.Status__c='In Progress'; 
            cn.Reserved_Name__c=true;
            lstCompNames.add(cn);
        }
        if(lstCompNames != null && lstCompNames.size() > 0)
            update lstCompNames;
    }
    /**
		Description : This apex method is used to get the cheque record type from backend Receipt object  
		*Created By : Brajesh Tiwary
		*Created Date : 16.2.2019
		*Modified By : Brajesh Tiwary
		*Modified Date : 16.4.2019
		*Modified by Brajesh to make some adjustment to the data.
	**/   
    @AuraEnabled
    public static Id getRecTypeId(){
        Id recid = Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get('Cheque').getRecordTypeId();        
        return recid;
    }
    /**
		Description : This apex method is used to get the updated amount for the user.       
		*Created By : Brajesh Tiwary
		*Created Date : 16.2.2019
		*Modified By : Brajesh Tiwary
		*Modified Date : 16.4.2019
		*Modified by Brajesh to make some adjustment to the data.
	**/
    @AuraEnabled
    public static DisplayRecords getTopUpBalanceAmount(){
        DisplayRecords wrapObj = new DisplayRecords();
        decimal PortalBalance = 0;
        decimal dSRPriceItemNotSentAmt = 0;
        list<string> accountList = new list<string>();
        DateTime LastUpdatedDateTime;
        string loggedInUser = UserInfo.getUserId();
        list<User> userList = [select ContactId,Contact.AccountId from user where Id =:loggedInUser AND ContactId!=null];
        for(User userObj : userList){
            accountList.add(userObj.Contact.AccountId);
        }
        
        if((LastUpdatedDate == null || (LastUpdatedDate.day() != system.today().day() || LastUpdatedDate.month() != system.today().month()))){
            
            AccountBalanceInfoCls.getAccountBalance(accountList, '5000');
            
            System.debug('Balance is Updated.');
            
            isUpdate = true;
        }
        for(Account objAccount : [select Id,Index_Card_Status__c,Name,Portal_Balance__c,PSA_Deposit__c,Balance_Updated_On__c from Account where Id=:accountList]){
            
            LastUpdatedDate = (objAccount.Balance_Updated_On__c != null) ? Date.valueOf(objAccount.Balance_Updated_On__c) : system.today();
            
            PortalBalance = (objAccount.Portal_Balance__c != null) ? objAccount.Portal_Balance__c : 0;
            
            PSADeposit = (objAccount.PSA_Deposit__c != null) ? objAccount.PSA_Deposit__c : 0;
            
            LastUpdatedDateTime = (objAccount.Balance_Updated_On__c != null) ? Date.valueOf(objAccount.Balance_Updated_On__c) : system.now();
            
            //V1.6 - Claude - Start
            hasEstablishmentCard = String.isNotBlank(objAccount.Index_Card_Status__c) && !objAccount.Index_Card_Status__c.equals('Not Available') && !objAccount.Index_Card_Status__c.equals('Cancelled');
            //V1.6 - Claude - End
        }
        
        if(LastUpdatedDateTime != null){
            wrapObj.Minutes = LastUpdatedDateTime.Minute();
            wrapObj.Hours = LastUpdatedDateTime.hour();
            wrapObj.Seconds = LastUpdatedDateTime.second();
        }
        /* V1.4 - Claude - Start - New Query to distinguish fit-out from the rest */
        string accountFilter = isFitOutContractor ? 'ServiceRequest__r.Contractor__c = ' : 'ServiceRequest__r.Customer__c = ' ;
        string priceItemQuery = 'select Price__c,Total_Service_Amount__c from SR_Price_Item__c where ServiceRequest__c!=null and Price__c!=null and '+accountFilter+' \''+accountList+'\' and (Status__c=\'Blocked\' OR Status__c=\'Consumed\') and Add_in_Account_Balance__c=true';// Add_in_Account_Balance__c is a field that validates whether or not the price item should be counted for Account balance check V1.9
        /* V1.4 - Claude - End */
        system.debug('====priceItemQuery======'+priceItemQuery);
        if(accountList.size()>0){
        	/* //Commented because of existing code issue
            for(SR_Price_Item__c objSRPriceItem : Database.query(priceItemQuery)){
                dSRPriceItemNotSentAmt += objSRPriceItem.Total_Service_Amount__c; // added as part of vat
            }
            */
            for(HexaBPM__SR_Price_Item__c SRP:[Select Id,HexaBPM__Price__c,Total_Service_Amount__c from HexaBPM__SR_Price_Item__c where HexaBPM__ServiceRequest__r.HexaBPM__Customer__c IN:accountList and (HexaBPM__Status__c='Blocked' OR HexaBPM__Status__c='Consumed')]){
        		if(SRP.Total_Service_Amount__c > 0)
        			dSRPriceItemNotSentAmt += SRP.Total_Service_Amount__c;
        	}
        }
        
        
        system.debug('PortalBalance is : '+PortalBalance);
        system.debug('dSRPriceItemNotSentAmt is : '+dSRPriceItemNotSentAmt);
        
        PortalBalance = PortalBalance - dSRPriceItemNotSentAmt;
        
        for(Receipt__c objReceipt : [select Amount__c from Receipt__c where Customer__c=:accountList and Amount__c!=null and Payment_Status__c='Success' and Pushed_to_SAP__c=false]){
            PortalBalance += objReceipt.Amount__c;
        }
        try{
	        if(userList!=null && userList.size()>0)
	        	PortalBalance -= RefundRequestUtils.getRefundAmount(userList[0].Contact.AccountId);  // V1.10 
        }catch(Exception e){
        	
        }
        PSAAvailable = (PSADeposit>= 125000) ? 125000 :  PSACalculation(hasEstablishmentCard, accountList);//( PSADeposit - (dAvaPSA + (VisaIssued*2500)) );
        system.debug('PSAAvailable is : '+PSAAvailable);
        wrapObj.portalBalance = formatPortalBalance(PortalBalance);
        wrapObj.portalBalanceValue = PortalBalance;
        BalanceFetchSuccessfull = true;
        return wrapObj;
    }
    @Auraenabled
    public static decimal PSACalculation(Boolean hasEstablishmentCard,list<string> accountList){
        
        list<string> lstPSARecTypes = new list<string>{'DIFC_Sponsorship_Visa_New','Employment_Visa_from_DIFC_to_DIFC','Employment_Visa_Govt_to_DIFC','Visa_from_Individual_sponsor_to_DIFC'};
        
        map<Id,Service_Request__c> mapOpenSRs = new map<Id,Service_Request__c>([select Id,External_Status_Name__c from Service_Request__c where (NOT Occupation_GS__r.Name like '%STUDENT%') AND Customer__c=:accountList AND Record_Type_Name__c IN : lstPSARecTypes AND isClosedStatus__c != true AND Is_Rejected__c != true AND External_Status_Name__c != 'Cancelled' AND External_Status_Name__c != 'Rejected' AND External_Status_Name__c != 'Draft']);
        
        map<Id,Contact> mapActiveCons = new map<Id,Contact>([select Id from Contact where (NOT Job_Title__r.Name like '%STUDENT%') AND Id IN (select Object_Contact__c from Relationship__c where Subject_Account__c =:accountList AND Active__c = true AND Relationship_Type__c='Has DIFC Sponsored Employee')]);
        
        Integer iEmpCount = (mapActiveCons != null && !mapActiveCons.isEmpty()) ? mapActiveCons.size() : 0 ;
        
        system.debug('iEmpCount is : '+iEmpCount);
        
        if(!mapOpenSRs.isEmpty()) iEmpCount += mapOpenSRs.size();
        
        system.debug('iEmpCount is : '+iEmpCount);
        system.debug('PSADeposit is : '+PSADeposit);
        
        //V1.6 - Claude - Start
        if(hasEstablishmentCard) iEmpCount++;
        //V1.6 - Claude - End
        
        for(SR_Price_Item__c objSRItem : [select Id,Price__c from SR_Price_Item__c where 
                                                                                   Count_in_PSA__c = TRUE AND //V1.7 - Claude - Added PSA flag for SR Groups
                                                                                   ServiceRequest__r.Customer__c =: accountList AND (Status__c='Blocked' OR Status__c='Consumed') AND Pricing_Line__r.Product__r.Name = 'PSA']){ //AND ServiceRequest__r.External_Status_Name__c = 'Submitted'
            //dAvaPSA -= objSRItem.Price__c;
            PSADeposit += objSRItem.Price__c;
        }
        
        system.debug('PSADeposit is : '+PSADeposit);
        
          
        
        decimal dAvaPSA = (PSADeposit-(iEmpCount*2500)); // V1.6 - Claude - Added fixed 2500 as 'Establishment Card' // V1.10
        
        system.debug('dAvaPSA is : '+dAvaPSA);
        
        return dAvaPSA;
    }
    
    /* ---------------------------------------------------------------------
     * Method name : getButtonAction
     * description : used to perform button action and return a wrapper
     * --------------------------------------------------------------------*/
    @AuraEnabled
    public static String getSRLinesAmount(string SRID){ 
        List<SRLine> lines = new List<SRLine>();
        List<HexaBPM__SR_Price_Item__c> srPriceItems = [SELECT id,HexaBPM__Price__c,HexaBPM__Product__r.Name FROM HexaBPM__SR_Price_Item__c WHERE HexaBPM__ServiceRequest__c =: SRID AND HexaBPM__Status__c = 'Added'];
        for(HexaBPM__SR_Price_Item__c sri : srPriceItems){
            SRLine line = new SRLine();
            line.product = sri.HexaBPM__Product__r.Name;
            line.price = sri.HexaBPM__Price__c;
            lines.add(line);
           // totalPrice += sri.HexaBPM__Price__c;
        }
        return json.serialize(lines);
    }
    
    private static string formatPortalBalance(decimal PortalBalance){

        List<String> args = new String[]{'0','number','###,###,###,##0.00'};
        
        string PortalBalanceFormatted = String.format(PortalBalance.format(), args); 
        string PSADepositFormatted = String.format(PSADeposit.format(), args);
        string PSAAvailableFormatted = String.format(PSAAvailable.format(), args);
        
        //Format function removes trailing zeros
        //if(!PortalBalanceFormatted.contains('.'))
            //PortalBalanceFormatted +='.00';
        if(!PSADepositFormatted.contains('.'))
            PSADepositFormatted +='.00';
        if(!PSAAvailableFormatted.contains('.'))
            PSAAvailableFormatted +='.00';
        return PortalBalanceFormatted;
    }
    public class DisplayRecords {
        
        @AuraEnabled public string portalBalance;
        @AuraEnabled public Integer Minutes;
        @AuraEnabled public Integer Hours;
        @AuraEnabled public Integer Seconds;
        @AuraEnabled public decimal portalBalanceValue;
    }
    public class PriceItem{
        @AuraEnabled
        public decimal amount {get;set;}
        @AuraEnabled
        public decimal totalBalance {get;set;}
        @AuraEnabled
        public string name {get;set;}
        @AuraEnabled
        public string receiptRecordTypeId {get;set;}
        @AuraEnabled
        public list<ProductInfo> lstProductInfo {get;set;}
        public PriceItem(){
            amount = 0;
            totalBalance = 0;
            lstProductInfo = new list<ProductInfo>();
        }
    }
    public class ProductInfo{
        @AuraEnabled
        public string name {get;set;}
        @AuraEnabled
        public decimal amount {get;set;}
    }
    public class SRLine {     
        @AuraEnabled public string product;
        @AuraEnabled public Decimal price;
    }
}