global with sharing class DIFC_APIFieldSelectionsController
{
    public List<DIFC_Field_Selection__c> objFields2 {get;set;}
    public List<DIFC_Field_Selection__c> objFields3 {get;set;}
    
    /*method to fetch selected and Unselected fields on UI*/
    public void fetchSelectedFields()
    {
        Set<String> fields = new Set<String>();
        Set<String> s2 = new Set<String>();
        List<String> so1 = new List<String>();
        List<String> so2 = new List<String>();
        objFields2 = [Select Name,Selected__c from DIFC_Field_Selection__c where Selected__c= false];
        objFields2.sort();
        objFields3 = [Select Name,Selected__c from DIFC_Field_Selection__c where Selected__c= true];
        objFields3.sort();
        //CLS_CreatePRJSON_SelectedFieldController.jsonRes();
    }
    
    /*to update selected__c field status*/
    @RemoteAction
    global static String updateField(String fieldName,String flag) 
    {
      DIFC_Field_Selection__c updateList = [Select Id,Name,Selected__c from DIFC_Field_Selection__c where Name =:fieldName Limit 1];
      if(flag == '0')
          updateList.Selected__c = false;
      else
          updateList.Selected__c = true;
      update updateList;
      //CLS_CreatePRJSON_NanoController.jsonRes();
      return fieldName;
    
    }
  }