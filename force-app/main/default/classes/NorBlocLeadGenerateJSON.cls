/******************************************************************************************
 *  Class Name  : NorBlocLeadGenerateJSON
 *  Author      : 
 *  Company     : DIFC
 *  Date        : 25 NOV 2019        
 *  Description :                
 ----------------------------------------------------------------------
   Version     Date              Author                Remarks                                                 
  =======   ==========        =============    ==================================
    v1.1   25 NOV 2019                              Initial Version  
*******************************************************************************************/

public class NorBlocLeadGenerateJSON {
    
    public static String norBlockLeadJSONMethod(String accountID) {
        
        Account eachAccount = new Account();
        eachAccount = [SELECT Registration_License_No__c FROM Account where ID=:accountID];
        
        CustomerInfo customerInformation = new CustomerInfo();
        customerInformation.categoryId = 'corporateCustomer';
        customerInformation.category = 'DIFC'+eachAccount.Registration_License_No__c;
        customerInformation.countryIso2 = 'AE';
        
        List<TargetOrgs> lstTargetOrgs = new List<TargetOrgs>();
        
        TargetOrgs targetORG = new TargetOrgs();
        targetORG.orgId = 2004;
        lstTargetOrgs.add(targetORG);
        
        finalJSON fjson = new finalJSON();
        fjson.customerInfo = customerInformation;
        fjson.targetOrgs = lstTargetOrgs;
        
        String finalJson = JSON.serialize(fJSON);  
        return finalJson;
    }
    public class TargetOrgs {
        public Integer orgId;
    }
    
    public class finalJSON{
        public CustomerInfo customerInfo;
        public List<TargetOrgs> targetOrgs;
    }

    public class CustomerInfo {
        public String categoryId;
        public String category;
        public String countryIso2;
    }

    
}