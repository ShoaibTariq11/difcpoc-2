@isTest(seeAllData=false)
    private class TestGsEntryPermitUploadHandler {
    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        list<Endpoint_URLs__c> lstCS = new list<Endpoint_URLs__c>();
    Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS ECC';
        objEP.URL__c = 'http://GSECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://ECC.com/sampledata';
        lstCS.add(objEP);
        insert lstCS;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        list<Country_Codes__c> lstCC = new list<Country_Codes__c>();
        Country_Codes__c objCode = new Country_Codes__c();
        objCode.Name = 'India';
        objCode.Code__c = 'IN';
        objCode.Nationality__c = 'India';
        lstCC.add(objCode);
        insert lstCC;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new Status__c(Name='Cancelled',Code__c='CANCELLED'));
        insert lstStatus;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        lstSRStatus.add(new SR_Status__c(Name='Submitted',Code__c='Submitted'));
        lstSRStatus.add(new SR_Status__c(Name='CANCELLATION PENDING',Code__c='CANCELLATION_PENDING'));
        lstSRStatus.add(new SR_Status__c(Name='Cancelled',Code__c='CANCELLED'));
        insert lstSRStatus;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'DIFC_Sponsorship_Visa_New';
        objTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_New';
        objTemplate.Menutext__c = 'DIFC_Sponsorship_Visa_New';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Employee Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        Step_Template__c objStepTemplate = new Step_Template__c();
        objStepTemplate.Name = 'Entry permit issue is typed';
        objStepTemplate.Code__c = 'Entry permit issue is typed';
        objStepTemplate.Step_RecordType_API_Name__c = 'General';
        objStepTemplate.Summary__c = 'Entry permit issue is typed';
        insert objStepTemplate;
        
        SR_Steps__c objSRStep = new SR_Steps__c();
        objSRStep.SR_Template__c = objTemplate.Id;
        objSRStep.Step_RecordType_API_Name__c = 'General';
        objSRStep.Step_Template__c = objStepTemplate.Id; // Step Type
        objSRStep.Summary__c = 'Review for Cancellation';
        objSRStep.Step_No__c = 1.0;
        objSRStep.Start_Status__c = lstStatus[0].Id; // Default Step Status
        objSRStep.Estimated_Hours__c = 10;
        insert objSRStep;
      
      list<Product2> lstProducts = new list<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'DIFC New Visa';
        lstProducts.add(objProd);
        
        objProd = new Product2();
        objProd.Name = 'Refund';
        lstProducts.add(objProd);
        
        insert lstProducts;
        
     list<Pricing_Line__c> lstPLs = new list<Pricing_Line__c>();
        Pricing_Line__c objPL;
        objPL = new Pricing_Line__c();
        objPL.Name = 'DIFC New Sponsorship Visa New';
        objPL.DEV_Id__c = 'GO-00001';
        objPL.Product__c = lstProducts[0].Id;
        objPL.Material_Code__c = 'GO-00001';
        objPL.Priority__c = 1;
        objPL.Additional_Item__c = false;
        lstPLs.add(objPL);
        
        objPL = new Pricing_Line__c();
        objPL.Name = 'DIFC New Sponsorship Visa New';
        objPL.DEV_Id__c = 'GO-Refund';
        objPL.Product__c = lstProducts[1].Id;
        objPL.Material_Code__c = 'GO-Refund';
        objPL.Priority__c = 1;
        objPL.Additional_Item__c = false;
        lstPLs.add(objPL);
        
        insert lstPLs;
     
     list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
        Dated_Pricing__c objDP;
        for(Pricing_Line__c objP : lstPLs){
          objDP = new Dated_Pricing__c();
          objDP.Pricing_Line__c = objP.Id;
          objDP.Unit_Price__c = 1234;
          objDP.Date_From__c = system.today().addMonths(-1);
          objDP.Date_To__c = system.today().addYears(2);
          lstDP.add(objDP);
        }
        insert lstDP;
        
     map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DIFC_Sponsorship_Visa_New')]){
          mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }       
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = mapRecordTypeIds.get('DIFC_Sponsorship_Visa_New');
        objSR.Send_SMS_To_Mobile__c = '+97152123456';
        objSR.Email__c = 'testclass@difc.ae.test';
        objSR.Type_of_Request__c = 'Applicant Outside UAE';
        objSR.Port_of_Entry__c = 'Dubai International Airport';
        objSR.Title__c = 'Mr.';
        objSR.First_Name__c = 'India';
        objSR.Last_Name__c = 'Hyderabad';
        objSR.Middle_Name__c = 'Andhra';
        objSR.Nationality_list__c = 'India';
        objSR.Previous_Nationality__c = 'India';
        objSR.Qualification__c = 'B.A. LAW';
        objSR.Gender__c = 'Male';
        objSR.Date_of_Birth__c = Date.newInstance(1989,1,24);
        objSR.Place_of_Birth__c = 'Hyderabad';
        objSR.Country_of_Birth__c = 'India';
        objSR.Passport_Number__c = 'ABC12345';
        objSR.Passport_Type__c = 'Normal';
        objSR.Passport_Place_of_Issue__c = 'Hyderabad';
        objSR.Passport_Country_of_Issue__c = 'India';
        objSR.Passport_Date_of_Issue__c = system.today().addYears(-1);
        objSR.Passport_Date_of_Expiry__c = system.today().addYears(2);
        objSR.Religion__c = 'Hindus';
        objSR.Marital_Status__c = 'Single';
        objSR.First_Language__c = 'English';
        objSR.Email_Address__c = 'applicant@newdifc.test';
        objSR.Mother_Full_Name__c = 'Parvathi';
        objSR.Monthly_Basic_Salary__c = 10000;
        objSR.Monthly_Accommodation__c = 4000;
        objSR.Other_Monthly_Allowances__c = 2000;
        objSR.Emirate__c = 'Dubai';
        objSR.City__c = 'Deira';
        objSR.Area__c = 'Air Port';
        objSR.Street_Address__c = 'Dubai';
        objSR.Building_Name__c = 'The Gate';
        objSR.PO_BOX__c = '123456';
        objSR.Residence_Phone_No__c = '+97152123456';
        objSR.Work_Phone__c = '+97152123456';
        objSR.Domicile_list__c = 'India';
        objSR.Phone_No_Outside_UAE__c = '+97152123456';
        objSR.City_Town__c = 'Hyderabad';
        objSR.Address_Details__c = 'Banjara Hills, Hyderabad';
        objSR.Statement_of_Undertaking__c = true;
        objSR.Internal_SR_Status__c = lstSRStatus[0].Id;
        objSR.External_SR_Status__c = lstSRStatus[0].Id;
        objSR.SAP_Unique_No__c = '123456789012345';
        insert objSR;
      
      list<SR_Price_Item__c> lstPriceItems = new list<SR_Price_Item__c>();
        SR_Price_Item__c objItem;
        
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = objSR.Id;
        objItem.Price__c = 3210;
        objItem.Pricing_Line__c = lstPLs[0].Id;
        objItem.Product__c = lstProducts[0].Id;
        lstPriceItems.add(objItem);
        
        SR_Price_Item__c objItemRef = new SR_Price_Item__c();
        objItemRef.ServiceRequest__c = objSR.Id;
        objItemRef.Price__c = 3210;
        objItemRef.Pricing_Line__c = lstPLs[1].Id;
        objItemRef.Product__c = lstProducts[1].Id;
        lstPriceItems.add(objItemRef);
      insert lstPriceItems;
      
        Step__c objStep = new Step__c();
        objStep.SR__r = objSR;
        objStep.SR__c = objSR.Id;
        objStep.SAP_Seq__c = '0010';
        objStep.Step_Template__c = objStepTemplate.id;
      
        insert objStep;
        
        //objSR.SAP_Unique_No__c = '12345678901234';
        //update objSR;
        //GsServiceCancellation.CancelService(objStep);
            
        list<SAPGSWebServices.ZSF_S_GS_SERV_OP> lstGSItems = new list<SAPGSWebServices.ZSF_S_GS_SERV_OP>();
        SAPGSWebServices.ZSF_S_GS_SERV_OP objGSItem = new SAPGSWebServices.ZSF_S_GS_SERV_OP();
        objGSItem.ACCTP = 'N';
        objGSItem.SOPRP = 'N';
        objGSItem.BANFN = '50001234';
        objGSItem.VBELN = '05001234';
        objGSItem.BELNR = '1234567';
        objGSItem.WSTYP = 'SERV';
        objGSItem.MATNR = 'GO-00001';
        objGSItem.UNQNO = '12345675000215';
        lstGSItems.add(objGSItem);
        objGSItem = new SAPGSWebServices.ZSF_S_GS_SERV_OP();
        objGSItem.ACCTP = 'P';
        objGSItem.SOPRP = 'N';
        objGSItem.BANFN = '50001234';
        objGSItem.VBELN = '05001234';
        objGSItem.BELNR = '1234567';
        objGSItem.WSTYP = 'SERV';
        objGSItem.MATNR = 'GO-00001';
        objGSItem.UNQNO = '12345675000215';
        lstGSItems.add(objGSItem);
        
        list<SAPECCWebService.ZSF_S_RECE_SERV_OP> lstPSA = new list<SAPECCWebService.ZSF_S_RECE_SERV_OP>();
        SAPECCWebService.ZSF_S_RECE_SERV_OP objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
        objTemp.ACCTP = 'P';
        objTemp.SGUID = lstPriceItems[0].Id;
        objTemp.SFMSG = 'Test Class';
        objTemp.BELNR = '1234567';
        objTemp.UNQNO = '123456750002015';
        objTemp.SFMSG = 'Records are Inserted Successfully in the custom table';
        lstPSA.add(objTemp);
        objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
        objTemp.ACCTP = 'N';
        objTemp.SGUID = lstPriceItems[0].Id;
        objTemp.SFMSG = 'Test Class';
        objTemp.BELNR = '1234567';
        objTemp.UNQNO = '123456750002015';
        lstPSA.add(objTemp);
    
    //objSR.SAP_Unique_No__c = '12345678901234';
        //update objSR;
            
        TestGsCRMServiceMock.lstGSPriceItems = lstGSItems;
     TestGsCRMServiceMock.lstPSAItems = lstPSA;
         
     //objStep = new Step__c();
        //objStep.SR__r = objSR;
        //objStep.SAP_Seq__c = '0010';
        
        Service_request__c sr =[select id,lastModifiedDate from service_request__c where id =:objSR.id ];
        DateTime dt = sr.lastModifiedDate; 
     
       Test.setMock(WebServiceMock.class, new TestGsCRMServiceMock());
     
        GsEntryPermitUploadHandler.EntryPermitStatusInSAP(objSR.id,objStep.SAP_Seq__c,dt);
        GsMedicalTestScheduledHandler.SendMedicalServiceToSAP(objStep);
       
    }
}