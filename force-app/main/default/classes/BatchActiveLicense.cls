/***********************************************************************************
 *  Author   : Arun 
 *  Purpose  : Batch to update companies Active License
  --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
V.No           Date            Updated By       Description
 ----------------------------------------------------------------------------------------              
 V 1.0                          Arun              Initial Version
 v 1.1       08 May2019         Sai              Added Line 56 to Populate Operating location Building Name in Account (Ticket Num:6754)        

**********************************************************************************************/

global class BatchActiveLicense implements Database.Batchable<sObject>
{

   global final String Query;

   global BatchActiveLicense (String q){

      Query=q; 
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<account> ListAccounts)
   {
       /*
     for(sobject s : scope){
     s.put(Field,Value); 
     }
     update scope;
     List<account> ListAccounts=[select id,Active_Leases__c,(select id from Lease__r where Status__c='Active') from Account where ROC_Status__c='Active' and Active_Leases__c=null limit 200];
     

        for(Account ObjAccount:ListAccounts)
        {
            ObjAccount.Active_Leases__c=ObjAccount.Lease__r.Size();
        }
        update ListAccounts;
        */
        //[select id,Active_Leases__c,Lease_Expiry_Date__c,(select id from Lease__r where Status__c='Active') from Account where ROC_Status__c='Active' and Lease_Expiry_Date__c=null limit 200]
       Map<Id,Account> MapAccount=new Map<Id,Account>();
       // List<id> ListExpiredId=new List<Id>();
        for(Account ObjAccount:ListAccounts)
        {
            ObjAccount.Active_Leases__c=ObjAccount.Lease__r.Size();
            
            ObjAccount.Registered_Active_Lease__c=null;
            
            for(Operating_Location__c opp:ObjAccount.Operating_Locations__r)
            {
                
                ObjAccount.Registered_Active_Lease__c=opp.Lease__c;
                ObjAccount.Operating_Location__c =opp.ID;      // This Logic added by Sai to Populate Operating location Building Name in Account
                
            }
            
            //if(ObjAccount.Lease__r.Size()==0)
           // {
              //  ListExpiredId.add(ObjAccount.id);
            //}
            
            MapAccount.put(ObjAccount.id,ObjAccount);
        }
        
        for(Account tempObjAccount:[select id,Lease_Expiry_Date__c,(select id,Name,End_Date__c from Lease__r where Status__c!='Active' order by End_Date__c DESC limit 1) from Account  where id in : MapAccount.keyset()])
        {
             tempObjAccount.Lease_Expiry_Date__c =null;
            Account ObjAccount=MapAccount.get(tempObjAccount.id);
            if(tempObjAccount.Lease__r.Size()==1)
            {
                ObjAccount.Lease_Expiry_Date__c =   tempObjAccount.Lease__r[0].End_Date__c;
            }
            
            
        }
        Update MapAccount.values();


    
    }

   global void finish(Database.BatchableContext BC)
   {
   }
}