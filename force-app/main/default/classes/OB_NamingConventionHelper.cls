/******************************************************************************************
 *  Name        : OB_NamingConventionHelper 
 *  Author      : Durga Prasad
 *  Description : helper class for Name Reservation check
*******************************************************************************************/
public without sharing class OB_NamingConventionHelper {
    public static void CreateNameApprovalSR(string SRID){
    	if(SRID!=null){
    		boolean NameCheckSRExists = false;
    		for(HexaBPM__Service_Request__c NameCheckSR:[Select Id,HexaBPM__Customer__c from HexaBPM__Service_Request__c where HexaBPM__Parent_SR__c=:SRID and RecordType.DeveloperName='Name_Check' and HexaBPM__IsClosedStatus__c=false and HexaBPM__Is_Rejected__c=false and HexaBPM__IsCancelled__c=false]){
    			NameCheckSRExists = true;
    		}
    		if(!NameCheckSRExists){
	    		HexaBPM__Service_Request__c objNameCheckSR = new HexaBPM__Service_Request__c();
	    		for(HexaBPM__Service_Request__c objSR:[Select Id,HexaBPM__Customer__c,first_name__c,last_name__c,HexaBPM__Email__c,HexaBPM__Send_SMS_to_Mobile__c from HexaBPM__Service_Request__c where Id=:SRID]){
	    			objNameCheckSR.HexaBPM__Customer__c = objSR.HexaBPM__Customer__c;
	    			objNameCheckSR.HexaBPM__Parent_SR__c = objSR.Id;
					objNameCheckSR.first_name__c = objSR.first_name__c;
					objNameCheckSR.last_name__c = objSR.last_name__c;
					objNameCheckSR.HexaBPM__Email__c = objSR.HexaBPM__Email__c;
					objNameCheckSR.HexaBPM__Send_SMS_to_Mobile__c = objSR.HexaBPM__Send_SMS_to_Mobile__c;
	    		}
	    		if(Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('Name_Check')!=null)
	    			objNameCheckSR.RecordTypeId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('Name_Check').getRecordTypeId();
	    		objNameCheckSR.HexaBPM__Auto_Submit__c = true;
	    		insert objNameCheckSR;
    		}
    	}
    }
	
	//Setting the Page Tracker once the name reservation is completed.
	public static void setPageTrackerAfterPayment(string SRID, string pageName){
		string recordTypeName;
        string pageId;
		for(HexaBPM__Service_Request__c serviceRequest :[select recordtype.DeveloperName from HexaBPM__Service_Request__c
													     where id = :SRID]) {
			recordTypeName = serviceRequest.recordtype.DeveloperName;
		}
		for(HexaBPM__Page__c page :[select id, HexaBPM__Page_Flow__c from HexaBPM__Page__c where Community_Page__c = :pageName
								    and HexaBPM__Page_Flow__r.HexaBPM__Record_Type_API_Name__c = :recordTypeName]) {
            pageId = page.id;
		}
        HexaBPM__Service_Request__c objRequest = OB_QueryUtilityClass.QueryFullSR(SRID);
        objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, SRID, pageId);
		upsert objRequest;
	}

	//Submit the Company Names
	public static void submitCompanyNameForApproval(string srId){
        List<Company_Name__c> lstCompNames = new List<Company_Name__c>();
        
        for(Company_Name__c cn: [select id,Stage__c,Status__c,Reserved_Name__c from Company_Name__c where Application__c =:srId]){
            cn.Stage__c='Submitted';
            cn.Status__c='In Progress'; 
            cn.Reserved_Name__c=true;
            lstCompNames.add(cn);
        }
        if(lstCompNames != null && lstCompNames.size() > 0)
            update lstCompNames;
    }
}