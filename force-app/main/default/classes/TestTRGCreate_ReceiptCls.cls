/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class TestTRGCreate_ReceiptCls {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        User objLoggedinUser = new User(id=Userinfo.getUserId());
        objLoggedinUser.SAP_User_Id__c = 'SFIUSER';
        update objLoggedinUser;
        
        WebService_Details__c objWSD = new WebService_Details__c();
        objWSD.Name = 'Credentials';
        objWSD.SAP_User_Id__c = 'testuser';
        insert objWSD;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        
        map<string,string> mapReceiptRecordTypes = new map<string,string>();
        for(Schema.RecordTypeInfo obj :  Receipt__c.SObjectType.getDescribe().getRecordTypeInfos()){
        	mapReceiptRecordTypes.put(obj.getName(),obj.getRecordTypeId());
        }
        system.debug('mapReceiptRecordTypes are : '+mapReceiptRecordTypes);
        Test.setMock(WebServiceMock.class, new TestReceiptCreationCls());
        //Endpoint_URLs__c.getAll().get('ECC').URL__c
        Endpoint_URLs__c objEU = new Endpoint_URLs__c();
        objEU.Name = 'ECC';
        objEU.URL__c = 'http://difc.ae/ecc';
        insert objEU;
        
        list<Receipt__c> lstReceipts = new list<Receipt__c>();
        Receipt__c objReceipt = new Receipt__c();
        objReceipt.Customer__c = objAccount.Id;
        //objReceipt.Payment_Status__c = 'Success';
        objReceipt.Amount__c = 10000;
        objReceipt.Receipt_Type__c = 'Card';
        lstReceipts.add(objReceipt);
        objReceipt = new Receipt__c();
        objReceipt.Customer__c = objAccount.Id;
        //objReceipt.Payment_Status__c = 'Success';
        objReceipt.Amount__c = 10000;
        objReceipt.Receipt_Type__c = 'Cash';
        lstReceipts.add(objReceipt);
        objReceipt = new Receipt__c();
        objReceipt.Customer__c = objAccount.Id;
        //objReceipt.Payment_Status__c = 'Success';
        objReceipt.Amount__c = 10000;
        objReceipt.Receipt_Type__c = 'Cheque';
        lstReceipts.add(objReceipt);
        insert lstReceipts;
        
        list<SAPECCWebService.ZSF_S_RECE_SERV_OP> resItems = new list<SAPECCWebService.ZSF_S_RECE_SERV_OP>();
        SAPECCWebService.ZSF_S_RECE_SERV_OP objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
        objTemp.ACCTP = 'P';
        objTemp.SGUID = lstReceipts[0].Id;
        objTemp.SFMSG = 'Test Class';
        objTemp.BELNR = '1234567';
        resItems.add(objTemp);
        objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
        objTemp.ACCTP = 'E';
        objTemp.SGUID = lstReceipts[1].Id;
        objTemp.SFMSG = 'Test Class';
        objTemp.BELNR = '1234567';
        resItems.add(objTemp);
        
        list<SAPECCWebService.ZSF_S_ACC_BAL> resActBals = new list<SAPECCWebService.ZSF_S_ACC_BAL>();
        SAPECCWebService.ZSF_S_ACC_BAL objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'D';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'H';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = '9';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        
        TestReceiptCreationCls.resItems = resItems;
        TestReceiptCreationCls.resActBals = resActBals;
        
        lstReceipts[0].Payment_Status__c = 'Success';
        lstReceipts[0].Moved_to_SAP__c = false;
        lstReceipts[1].Payment_Status__c = 'Success';
        lstReceipts[1].Moved_to_SAP__c = false;
        lstReceipts[2].Payment_Status__c = 'Success';
        lstReceipts[2].Moved_to_SAP__c = false;
        update lstReceipts;
        
    }
}