public class CommonWrapperClass {

    public class MasterDebtorParty {
        @AuraEnabled
        public DebtorSecured_Party_Master__c dbtPrtyMstr;
        @AuraEnabled
        public Boolean isSelected;
   		@AuraEnabled
        public string recId;
        
        public MasterDebtorParty() {
            isSelected = false;
            dbtPrtyMstr = new DebtorSecured_Party_Master__c();
        }
    }
    
    public class SRProtalPricingWrapper {
        @AuraEnabled
        public Decimal AvailablePortalBalance;
        @AuraEnabled
        public Decimal subTotalPriceItemVal;
        @AuraEnabled
        public Decimal TotalTaxPriceItemVal;
        @AuraEnabled
        public Decimal TotalPriceItemVal;
        @AuraEnabled
        public String AmountColor;
        @AuraEnabled
        public Boolean insufficientBalance;
        @AuraEnabled
        public List<SR_Price_Item__c> lstSRPriceItems;
        @AuraEnabled
        public Service_Request__c sr;
        
        public SRProtalPricingWrapper() {
            AvailablePortalBalance = 0;
            subTotalPriceItemVal = 0;
            TotalTaxPriceItemVal = 0;
            TotalPriceItemVal = 0;
            insufficientBalance = false;
            lstSRPriceItems = new List<SR_Price_Item__c>();
            sr = new Service_Request__c();
        }
    }
    
    public class PendingActionsWrapper {
        @AuraEnabled
        public Boolean ROCaccess;
        @AuraEnabled
        public List<Step__c> steps;
        @AuraEnabled
        public Boolean GSaccess;
        @AuraEnabled
        public List<Step__c> stepsGS;
        @AuraEnabled
        public Boolean ITaccess;
        @AuraEnabled
        public List<Step__c> stepsIT;
        @AuraEnabled
        public Boolean PROPaccess;
        @AuraEnabled
        public List<Step__c> stepsPROP;
        @AuraEnabled
        public Boolean ListingAccess;
        @AuraEnabled
        public List<Step__c> ListingSteps;
        @AuraEnabled
        public Boolean FitOutAccess;
        @AuraEnabled
        public List<Step__c> FitOutSteps;
        @AuraEnabled
        public Boolean isEvent;
        @AuraEnabled
        public List<Step__c> eventFitOutSteps;
        @AuraEnabled
        public Boolean isMortgage;
        @AuraEnabled
        public List<Step__c> mortgageSteps;
        @AuraEnabled
        public Boolean isROS;
        @AuraEnabled
        public List<Step__c> rosSteps;
        
        public PendingActionsWrapper() {
            ROCaccess = false;
        	GSaccess = false;
            ITaccess = false;
            PROPaccess = false;
            ListingAccess = false;
            FitOutAccess = false;
            isEvent = false;
            isMortgage = false;
            isROS = false;
            steps = new List<Step__c>();
            stepsGS = new List<Step__c>();
            stepsIT = new List<Step__c>();
            stepsPROP = new list<Step__c>();
            ListingSteps = new list<Step__c>();
            FitOutSteps = new list<Step__c>();
            eventFitOutSteps = new list<Step__c>();
            mortgageSteps = new List<Step__c>();
            rosSteps = new List<Step__c>();
        }
    }
}