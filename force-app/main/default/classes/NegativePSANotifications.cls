/********************************************************************************************************************************************************
 *  Author   	: Mudasir Wani
 *  Date     	: 31-March-2020
 *  Description	: This batch class is used to create the notification log records in order to send the notifications 	
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date       			Updated By    		Description
---------------------------------------------------------------------------------------------------------------------  
V1.0	02-April-2020		Mudasir 			Created this class for the assembla ticket #6176
*********************************************************************************************************************************************************/

global class NegativePSANotifications implements Database.Batchable<sObject>, Database.Stateful {
    
    // instance member to retain state across transactions
    global Integer recordsProcessed = 0;
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
            'Select id,name,BP_NO__c,Excess_PSA_Deposit__c,PSA_Deposit__c,Fintech_Four_Years_Completed__c,Sponsored_Employee__c  from Account where ROC_Status__c '+
            ' in  (\'Active\',\'Not Renewed\') AND RecordTypeId NOT IN (\'012200000003Ezl\',\'012200000003Ezm\') '+
            ' and Excess_PSA_Deposit__c < 0'
        );
    }
    global void execute(Database.BatchableContext bc, List<Account> scope){
        List<Email_Notification__c> emailNortifications = new List<Email_Notification__c>();
        // process each batch of records
        for (Account account : scope) {
        	//Name of the company , Total PSA deposit , Required PSA deposit , Amount to be Paid
        	//emailNortifications.add(createEmailNotification()); 
        	DIFCEmailUtils.createEmailNotificationRecord('notificationSubject', 'notificationType' , 'emailBody' , 
        												'smsBody', 'To_addresses', 'CC_addresses','BCC_addresses' );
        }
    }    
    global void finish(Database.BatchableContext bc){
        System.debug(recordsProcessed + ' records processed. Shazam!');
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, 
            JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob
            WHERE Id = :bc.getJobId()];
        // call some utility to send email
        //EmailUtils.sendMessage(job, recordsProcessed);
    }    
}