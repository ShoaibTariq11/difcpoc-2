@isTest(SeeAllData =true)
public class Test_CRS_Submission_Form_masscontroller {
     static testMethod void insertCaseTest1() {
        CRS_Master__c ObjYear = new CRS_Master__c();
        ObjYear.Name = '2019';
        insert ObjYear;
        user u = [select id, Accountid from user where profileid = '00e200000012hZD'
            and isActive = true limit 1
        ];
        System.runAs(u) {
            CRS_Form__c ObjFormd = new CRS_Form__c();

            ObjFormd.CRS_Year__c = ObjYear.id;
            ObjFormd.FI_ID__c = 'Deom';
            objFormd.Is_Null_return__c = 'NO';
            ObjFormd.Official_FI_Legal_Name__c = 'Deom';
            ObjFormd.Holder_Type__c = 'CRS101';
            ObjFormd.Account_Balance__c = 11000;
            ObjFormd.Tax_Residence_Country_new__c = 'Tax_Residence';
            ObjFormd.Account_Number__c = 'DEEEE';

            ObjFormd.Status__c = 'Submitted';
            insert ObjFormd;

            CRS_Client__c ClintP = new CRS_Client__c();
            ClintP.Address__c = '';
            ClintP.City__c = '';
            ClintP.Client_Type__c = 'Person'; //Company//Controlling=Controlling_Person_Type__c
            ClintP.Controlling_Person_Type__c = 'CRS801';
            ClintP.Country__c = 'United Kingdom';
            ClintP.CRS_Form__c = ObjFormd.id;
            ClintP.Date_of_Birthday__c = Date.today();
            ClintP.First_Name__c = 'United Kingdom';
            ClintP.IN__c = '2222';
            ClintP.Last_Name__c = 'United Kingdom';
            ClintP.TIN__c = 'sdasdasd';
            //ObjClintCP.Master_Client__c=ObjClint.id;
            insert ClintP;

            List < CRS_Payment__c > ListPayment = new List < CRS_Payment__c > ();
            CRS_Payment__c payment = new CRS_Payment__c();
            payment.Payment_Amount__c = 1;
            payment.Client__c = ClintP.id;
            payment.Payment_Type__c = 'CRS501';
            ListPayment.add(payment);

            CRS_Payment__c payment1 = new CRS_Payment__c();
            payment1.Payment_Amount__c = 236;
            payment1.Client__c = ClintP.id;
            payment1.Payment_Type__c = 'CRS501';

            ListPayment.add(payment1);


            CRS_Client__c ClintCP = new CRS_Client__c();
            ClintCP.Address__c = '';
            ClintCP.City__c = '';
            ClintCP.Client_Type__c = 'Controlling'; //Company//Controlling=Controlling_Person_Type__c
            ClintCP.Controlling_Person_Type__c = 'CRS801';
            ClintCP.Country__c = 'United Kingdom';
            ClintCP.CRS_Form__c = ObjFormd.id;
            ClintCP.Date_of_Birthday__c = Date.today();
            ClintCP.First_Name__c = 'United Kingdom';
            ClintCP.IN__c = '2222';
            ClintCP.Last_Name__c = 'United Kingdom';
            ClintCP.TIN__c = 'sdasdasd';
            ClintCP.Master_Client__c = ClintP.id;
            insert ClintCP;

            CRS_Payment__c payment2 = new CRS_Payment__c();
            payment2.Payment_Amount__c = 236;
            payment2.Client__c = ClintCP.id;
            payment2.Payment_Type__c = 'CRS501';

            ListPayment.add(payment2);

            //Company 


            CRS_Client__c ClintC = new CRS_Client__c();
            ClintC.Address__c = '';
            ClintC.City__c = '';
            ClintC.Client_Type__c = 'Company'; //Company//Controlling=Controlling_Person_Type__c
            ClintC.Controlling_Person_Type__c = 'CRS801';
            ClintC.Country__c = 'United Kingdom';
            ClintC.CRS_Form__c = ObjFormd.id;
            ClintC.Date_of_Birthday__c = Date.today();
            ClintC.First_Name__c = 'United Kingdom';
            ClintC.IN__c = '2222';
            ClintC.Last_Name__c = 'United Kingdom';
            ClintC.TIN__c = 'sdasdasd';
            //ObjClintCP.Master_Client__c=ObjClint.id;
            insert ClintC;

            CRS_Payment__c paymentC = new CRS_Payment__c();
            paymentC.Payment_Amount__c = 1;
            paymentC.Client__c = ClintC.id;
            paymentC.Payment_Type__c = 'CRS501';
            ListPayment.add(paymentC);
            CRS_Payment__c paymentC1 = new CRS_Payment__c();
            paymentC1.Payment_Amount__c = 236;
            paymentC1.Client__c = ClintC.id;
            paymentC1.Payment_Type__c = 'CRS501';
            ListPayment.add(paymentC1);
            CRS_Client__c ClintCCP = new CRS_Client__c();
            ClintCCP.Address__c = '';
            ClintCCP.City__c = '';
            ClintCCP.Client_Type__c = 'Controlling'; //Company//Controlling=Controlling_Person_Type__c
            ClintCCP.Controlling_Person_Type__c = 'CRS801';
            ClintCCP.Country__c = 'United Kingdom';
            ClintCCP.CRS_Form__c = ObjFormd.id;
            ClintCCP.Date_of_Birthday__c = Date.today();
            ClintCCP.First_Name__c = 'United Kingdom';
            ClintCCP.IN__c = '2222';
            ClintCCP.Last_Name__c = 'United Kingdom';
            ClintCCP.TIN__c = 'sdasdasd';
            ClintCCP.Master_Client__c = ClintC.id;
            insert ClintCCP;
            CRS_Payment__c paymentCCp2 = new CRS_Payment__c();
            paymentCCp2.Payment_Amount__c = 236;
            paymentCCp2.Client__c = ClintCCP.id;
            paymentCCp2.Payment_Type__c = 'CRS501';

            ListPayment.add(paymentCCp2);
            insert ListPayment;
            //Company EndOfInputException
            CRS_generate_XML.generateXml('Demo');

            /*ApexPages.StandardController scd = new ApexPages.StandardController(ObjFormd);
            CRS_Submission_Formcontroller CRSSubD = new CRS_Submission_Formcontroller(scd);
            CRSSubD.existingRecords();
            List < CRS_Submission_Formcontroller.CRSMainWrapper > CompaniesRecordsD = CRSSubD.CompaniesRecords;
            List < CRS_Submission_Formcontroller.CRSMainWrapper > PersonRecordsD = CRSSubD.PersonRecords;

            //Company 
            CRSSubD.SeletedId = paymentCCp2.id;
            CRSSubD.DeleteControllingPersonPayments();

            CRSSubD.SeletedId = ClintC.id;
            CRSSubD.AddControllingPersonPayment();

            //CRSSubD.SeletedId=ClintCCP.id;
            //CRSSubD.DeleteControllingPersonMain();

            //person
            CRSSubD.SeletedId = payment2.id;
            CRSSubD.DeleteControllingPersonPayments();

            CRSSubD.SeletedId = ClintP.id;
            CRSSubD.AddControllingPersonPayment();

            CRSSubD.SeletedId = ClintCP.id;
            CRSSubD.DeleteControllingPersonMain();


            CRSSubD.SaveMain();*/
            //CRSSubD.SaveAllMain();
            //********************************************************************************************************************************************************************
            CRS_Form__c ObjForm = new CRS_Form__c();
            ObjForm.Tax_Residence_Country_new__c = 'Tax_Residence';
            ObjForm.CRS_Year__c = ObjYear.id;
            //insert ObjForm;

            ApexPages.StandardController sc = new ApexPages.StandardController(ObjForm);
            CRS_Submission_Form_masscontroller ObjCRSCreate = new CRS_Submission_Form_masscontroller(sc);
            ObjCRSCreate.SeletedId = ClintCP.id;
            ObjCRSCreate.AddPerson();
            ObjCRSCreate.AddCompany();
            //ObjCRSCreateCRSMain.ListCRSPayment testList = new List<CRS_Payment__c>();
            //testList.addAll(ListPayment); 
            Blob blb = Blob.valueof('THis is new blob');
            //ObjCRSCreate.InPersonFile = blb;
            //ObjCRSCreate.UploadCompanyFile = blb;
            //ObjCRSCreate.ContraolllingPersonFile = blb;
            String[] bytes = new String[]{'bytesCount\n','test'};
            //ObjCRSCreate.filelines = bytes;
            // ObjCRSCreate.crsform=new CRS_Form__c ();
            //crsform.Tax_Residence_Country_new__c='United Kingdom';
            system.assertEquals(ClintP.id,ClintCP.Master_Client__c);
            //system.assertEquals(null,ClintP);
            CRS_Client__c crsClnt = [select id,name,Master_Client__c,(select id,Master_Client__c from clients__r)  from CRS_Client__c where id=:ClintP.id];
            CRS_Client__c crsClnt2 = [select id,name,Master_Client__c from CRS_Client__c where id=:ClintCP.id];
            //system.assertEquals(null,crsClnt2.Master_Client__c);
            ClintP.clients__r.add(ClintCP);
            ClintP.CRS_Payments__r.addAll(ListPayment);
            //system.assertEquals(null,crsClnt.clients__r);
           // system.assertNotEquals(null,ClintP.clients__r);
            //List<CRS_Client__c> crsForms = [select id,Name,Address__c,City__c, Tax_Residence_Country__c,Account_Balance__c,Holder_Type__c,Type__c,Account_Number__c,Client_Type__c,Country__c,CRS_Form__c,Date_of_Birthday__c,First_Name__c,IN__c,Last_Name__c,Master_Client__c,TIN__c,(select id,Name,Address__c,City__c,Tax_Residence_Country__c,Account_Balance__c,Holder_Type__c,Type__c,Account_Number__c,Controlling_Person_Type__c ,Client_Type__c,Country__c,CRS_Form__c,Date_of_Birthday__c,First_Name__c,IN__c,Last_Name__c,Master_Client__c,TIN__c from Clients__r),(select id,Client__c,Payment_Amount__c,Payment_Type__c from CRS_Payments__r) from CRS_Client__c where id=:ClintP.id];
            //system.assertEquals(null,crsForms[0].Clients__r);
            //ObjCRSCreate.addClint(crsForms[0],ObjFormd);
            
            ObjCRSCreate.existingRecords();
            ObjCRSCreate.addClint(ClintCP,ObjFormd);
            ObjCRSCreate.AddControllingPerson();
            ObjCRSCreate.AddMainPayment();          
            ObjCRSCreate.AddControllingPersonPayment();
            ObjCRSCreate.SaveAllMain();
            ObjCRSCreate.DeleteControllingPersonPayments();
            ObjCRSCreate.DeleteControllingPersonMain();
            ObjCRSCreate.DeletePaymentMain();
            ObjCRSCreate.DeleteMain();
            ObjCRSCreate.AddPerson();
            ObjCRSCreate.AddCompany();
            ObjCRSCreate.CancelMain();
            ObjCRSCreate.SaveControllingperson();
            ObjCRSCreate.CancelControllingperson();
            ObjCRSCreate.SaveMain();
            CRS_Submission_Formcontroller.CRSMainWrapper ObjCRSCreateCRSMain = new CRS_Submission_Formcontroller.CRSMainWrapper();
            CRS_Submission_Formcontroller.CRSControllingPersonWrapper test = new CRS_Submission_Formcontroller.CRSControllingPersonWrapper();
            ObjCRSCreateCRSMain.MainRecord = ClintC;
            ObjCRSCreateCRSMain.Forms = ObjFormd;
        }
    }
    static testMethod void IntermmediateCRS_CtrlTest() {
        CRS_Master__c ObjYear = new CRS_Master__c();
        ObjYear.Name = '2019';
        insert ObjYear;
        CRS_Form__c ObjFormd = new CRS_Form__c();

            ObjFormd.CRS_Year__c = ObjYear.id;
            ObjFormd.FI_ID__c = 'Deom';
            objFormd.Is_Null_return__c = 'NO';
            ObjFormd.Official_FI_Legal_Name__c = 'Deom';
            ObjFormd.Holder_Type__c = 'CRS101';
            ObjFormd.Account_Balance__c = 11000;
            ObjFormd.Tax_Residence_Country_new__c = 'Tax_Residence';
            ObjFormd.Account_Number__c = 'DEEEE';

            ObjFormd.Status__c = 'Submitted';
            insert ObjFormd;
            ApexPages.StandardController sc = new ApexPages.StandardController(ObjFormd);
            IntermmediateCRS_Ctrl con = new IntermmediateCRS_Ctrl(sc);
            con.redirect();
    }   
}