/******************************************************************************************
 *  Author   : Shabbir Ahmed
 *  Company  : DIFC
 *  Date     : 24-Apr-2015   
                
*******************************************************************************************/
@isTest(seealldata=false)
private class TestCC_GSCodeCls {

    static testMethod void NewPOBoxTest() {
        Id POBoxRecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('PO BOX').getRecordTypeId();
        
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Cancellation Approved';
        objStatus.Code__c = 'Cancellation Approved';
        insert objStatus;
        
        PO_Box_Master__c poBoxMaster = new PO_Box_Master__c();
        poBoxMaster.Name = '482008';
        poBoxMaster.Location__c = 'GV';
        poBoxMaster.Status__c = 'Available';
        insert poBoxMaster;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Service_Request__c poBoxSR = new Service_Request__c();
        poBoxSR.recordTypeId = POBoxRecordTypeId;
        poBoxSR.Customer__c = objAccount.Id;
        poBoxSR.Service_Category__c = 'New';
        poBoxSR.PO_Box_Master__c = poBoxMaster.Id;
        insert poBoxSR;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = poBoxSR.Id;
        objStep.Step_No__c = 1.0;
        objStep.Status__c = objStatus.Id;
        insert objStep;  
        
        Medical_Fitness_Step_Name__c objMFS = new Medical_Fitness_Step_Name__c();
        objMFS.Name = 'Medical Fitness Test Completed';
        objMFS.Step_Name__c = 'Medical Fitness Test Completed';
        objMFS.Step_Status__c = '';
        insert objMFS;
        
        CustomCode_UtilCls.Execute_Custom_Code(objStep,'CC_GSCodeCls','Assign_POBox');
        CustomCode_UtilCls.Execute_Custom_Code(objStep, 'CC_GSCodeCls', 'UpdatePOBoxStatus');
        CustomCode_UtilCls.Execute_Custom_Code(objStep, 'CC_GSCodeCls', 'allowExpressMedicalTest');
        CustomCode_UtilCls.Execute_Custom_Code(objStep, 'CC_GSCodeCls', 'generateEmbassyLetter');
        
        Medical_Fitness_Step_Name__c.getValues('Medical Fitness Test Completed');
    }
    
    static testMethod void RenewPOBoxTest() {
        Id POBoxRecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('PO BOX').getRecordTypeId();
                        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        PO_Box_Master__c poBoxMaster = new PO_Box_Master__c();
        poBoxMaster.Name = '482008';
        poBoxMaster.Location__c = 'GV';
        poBoxMaster.Status__c = 'Available';
        insert poBoxMaster;
        
        Customer_PO_Box__c customerPOBox = new Customer_PO_Box__c();
        customerPOBox.Client__c = objAccount.Id;
        customerPOBox.Status__c = 'Booked';
        customerPOBox.PO_Box_Master__c = poBoxMaster.Id;
        customerPOBox.End_Date__c=date.newInstance(system.today().year()-1,12,31);
        insert customerPOBox;
        
        Service_Request__c poBoxSR = new Service_Request__c();
        poBoxSR.recordTypeId = POBoxRecordTypeId;
        poBoxSR.Customer__c = objAccount.Id;
        poBoxSR.Service_Category__c = 'Renewal';
        poBoxSR.PO_Box_Master__c = poBoxMaster.Id;
        poBoxSR.Pre_GoLive__c = true;
        poBoxSR.duration__c = '1 year';
        
        insert poBoxSR;
        SRValidations.GSValidations(poBoxSR.id);
        
        Step__c objStep = new Step__c();
        objStep.SR__c = poBoxSR.Id;
        objStep.Step_No__c = 1.0;
        insert objStep;  
        
        CustomCode_UtilCls.Execute_Custom_Code(objStep,'CC_GSCodeCls','Renew_POBox');
        CustomCode_UtilCls.Execute_Custom_Code(objStep,'CC_GSCodeCls','transferPOBox');
        CustomCode_UtilCls.Execute_Custom_Code(objStep,'CC_GSCodeCls','Cancel_POBox');
    }
    
   
    static testMethod void negativePOBoxTest() {
        Id POBoxRecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('PO BOX').getRecordTypeId();
                 
        PO_Box_Master__c poBoxMaster = new PO_Box_Master__c();
        poBoxMaster.Name = '482008';
        poBoxMaster.Location__c = 'GV';
        poBoxMaster.Status__c = 'Available';
        //insert poBoxMaster;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Service_Request__c poBoxSR = new Service_Request__c();
        poBoxSR.recordTypeId = POBoxRecordTypeId;
        poBoxSR.Customer__c = objAccount.Id;
        poBoxSR.Service_Category__c = 'New';
        poBoxSR.PO_Box_Master__c = poBoxMaster.Id;
        poBoxSR.duration__c = '1 year';
        insert poBoxSR;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = poBoxSR.Id;
        objStep.Step_No__c = 1.0;
        insert objStep;  
        
        CustomCode_UtilCls.Execute_Custom_Code(objStep,'CC_GSCodeCls','Assign_POBox');
    }    
    
    
    static testMethod void sponsorDNRDFileUnitTest() {
                
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        insert objContact;
        
        Service_Request__c poBoxSR = new Service_Request__c();
        poBoxSR.Customer__c = objAccount.Id;
        poBoxSR.Service_Category__c = 'New';
        poBoxSR.Sponsor__c = objContact.Id;
        insert poBoxSR;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = poBoxSR.Id;
        objStep.Step_No__c = 1.0;
        insert objStep;  
        
        CustomCode_UtilCls.Execute_Custom_Code(objStep,'CC_GSCodeCls','Sponsor_OpenFamilyFile');
    }    
    
    static testMethod void GSembassyLetterUnitTest() {
                
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        insert objContact;
        
        Id GSContactId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        Contact objContact1 = new Contact();
        objContact1.firstname = 'Test Contact2';
        objContact1.lastname = 'Test Contact2';
        objContact1.accountId = objAccount.id;
        objContact1.recordTypeId = GSContactId;
        objContact1.Email = 'test2@difcportal.com';
        insert objContact1;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Consulate_or_Embassy_Letter';
        objTemplate.SR_RecordType_API_Name__c = 'Consulate_or_Embassy_Letter';
        objTemplate.Menutext__c = 'Consulate_or_Embassy_Letter';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Employee Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Required Docs to Upload';
        insert objDocMaster;
        
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.On_Submit__c = true;
        objTempDocs.Generate_Document__c = true;
        objTempDocs.SR_Template_Docs_Condition__c = 'Service_Request__c->Name#!=#DRIVER';
        insert objTempDocs;
        
        Lookup__c nat1 = new Lookup__c(Type__c='Nationality',Name='Afghanistan');
        insert nat1;
        Lookup__c nat2 = new Lookup__c(Type__c='Nationality',Name='India');
        insert nat2;
        Lookup__c nat3 = new Lookup__c(Type__c='Nationality',Name='Pakistan');
        insert nat3;
        
        string issueFineId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Consulate_or_Embassy_Letter' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        
        SR_Status__c objStatus = new SR_Status__c();
        objStatus.name = 'Submitted';
        objStatus.Code__c = 'Submitted';
        insert objStatus;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Express_Service__c = true;
        objSR.Customer__r = objAccount;
        objSR.Customer__c = objAccount.id;
        objSR.RecordTypeId = issueFineId;
        objSR.submitted_date__c = date.today();
        objSR.Internal_SR_Status__c = objStatus.id;
        objSR.External_SR_Status__c = objStatus.id;
        objSR.Country__c = 'India; ';
        objSR.Country__c = objSR.Country__c + 'Afghanistan; ';
        objSR.Country__c = objSR.Country__c + 'Pakistan; ';
        objSR.contact__c = objContact1.id;
        insert objSR;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        objStep.Step_No__c = 1.0;
        insert objStep;  
        
        Id recordTypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByName().get('Consulate or Embassy Letter').getRecordTypeId();
        amendment__c amendObj = new amendment__c();
        amendObj.Letter_Format__c = 'Arabic';
        amendObj.Letter_To_Consulate_Embassy__c = 'Embassy of';
        amendObj.Purpose_of_Visit__c = 'Business';
        amendObj.Type_of_Visit_Visa__c = 'Single Entry';
        amendObj.emirate__c = 'Dubai';
        amendObj.ServiceRequest__c = objSR.id;
        amendObj.Permanent_Native_Country__c = 'India'; 
        amendObj.ServiceRequest__c = objSR.id;
        amendObj.recordTypeId = recordTypeId;
        amendObj.customer__c = objSR.customer__c;
        amendObj.Contact__c = objSR.contact__c;
        insert amendObj;
        
        CustomCode_UtilCls.Execute_Custom_Code(objStep, 'CC_GSCodeCls', 'generateEmbassyLetter');
    } 
    
    static testMethod void TestCreateGSOAuthorizedSignatory(){
        
        Id ContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = ContactRecordTypeId;
        objContact.Passport_No__c = 'IND1234';
        objContact.Nationality__c ='India';
        insert objContact;
        
        Relationship__c objRel = new Relationship__c();
        objRel.Subject_Account__c = objAccount.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Group__c = 'GS';
        objRel.Relationship_Type__c = 'Is Authorized Signatory for';
        objRel.Active__c = true;
        insert objRel;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Service_Category__c = 'New';
        insert objSR;
        
        list<Amendment__c> lstAmds = new list<Amendment__c>();
        
        Amendment__c objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = objSR.Id;
        objAmd.Family_Name__c = 'Test';
        objAmd.Given_Name__c = 'GS';
        objAmd.Middle_Name__c = 'Contact';
        objAmd.Passport_No__c = 'IND1234';
        objAmd.Nationality_list__c = 'India';
        objAmd.Phone__c = '+9714567345678';
        objAmd.Mobile__c = objAmd.Phone__c;
        objAmd.Person_Email__c = 'test@123.com';
        lstAmds.add(objAmd);
        objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = objSR.Id;
        objAmd.Family_Name__c = 'Test';
        objAmd.Given_Name__c = 'GS';
        objAmd.Middle_Name__c = 'Contact';
        objAmd.Passport_No__c = 'IND1235';
        objAmd.Nationality_list__c = 'India';
        objAmd.Phone__c = '+9714567345678';
        objAmd.Mobile__c = objAmd.Phone__c;
        objAmd.Person_Email__c = 'test@123.com';
        lstAmds.add(objAmd);
        
        insert lstAmds;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        objStep.Step_No__c = 1.0;
        insert objStep;  
        
         CustomCode_UtilCls.Execute_Custom_Code(objStep,'CC_GSCodeCls','CreateGSOAuthorizedSignatory');
    }
    
    static testmethod void updateArabfieldsonGSContactTest(){
          Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Last RB';
        objContact.Email = 'test@difcportal.com';
        objContact.AccountId = objAccount.Id;
        objContact.FirstName = 'Nagaboina';
        objContact.RecordTypeId = [SELECT ID FROM RecordType WHERE DeveloperName = 'GS_Contact' AND SobjectType = 'Contact'].ID;
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Employee Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
            
        
        String objRectype;
        string difcSponsorReciD;
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Company_Temporary_Work_Permit_Request_Letter','Non_DIFC_Sponsorship_Visa_New') AND SobjectType='Service_Request__c']){
            if(objRT.developerName=='Non_DIFC_Sponsorship_Visa_New')objRectype = objRT.id; else
            difcSponsorReciD = objRT.id;
        }
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = objRectype;
        objSR.SR_Group__c = 'GS';
        objSR.contact__c = objContact.id;
        
        insert objSR; 
        
        Step_Template__c stepTemplate = new Step_Template__c();
        stepTemplate.name = 'Temporary Work Permit Form is Typed';
        stepTemplate.Code__c = 'Temporary Work Permit Form is Typed';
        stepTemplate.Step_RecordType_API_Name__c = 'General';
        insert stepTemplate;
        
        Status__c stepStatus = new Status__c();
        stepStatus.name = 'Awaiting Review';
        stepStatus.Code__c = 'Awaiting_Review';
        insert stepStatus;
        
        Status__c stepStatus1 = new Status__c();
        stepStatus1.name = 'Closed';
        stepStatus1.Code__c = 'Closed';
        insert stepStatus1;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        objStep.Step_Template__c = stepTemplate.id;
        objStep.Status__c = stepStatus.id;
        objStep.first_Name_Arabic__c ='test';
        objStep.Last_Name_Arabic__c ='test';
        
        objStep.Government_Services__c = true;
        insert objStep;
        
        
      SR_Doc__c objSRDoc = new SR_Doc__c();
      objSRDoc.Name = 'Coloured Photo';
      objSRDoc.Service_Request__c = objSR.Id;  
      objSRDoc.status__c ='Uploaded';    
      insert objSRDoc;
      
      
      Attachment objAttachment = new Attachment();
      objAttachment.Name = 'New User Access Form.pdf';
      objAttachment.Body = blob.valueOf('test');
      objAttachment.ParentId = objSRDoc.Id;
      
      
      insert objAttachment;
        
        objStep.Status__c = stepStatus1.id;
        update objStep;
        step__c stpTemp= [select id,sr__c,Update_Arabic_Names__c,closed_Date__c,First_Name_Arabic__c,Last_Name_Arabic__c from step__c where id =:objStep.id];
        
        CustomCode_UtilCls.Execute_Custom_Code(stpTemp,'CC_GSCodeCls','updateArabfieldsonGSContact');
        
       
        
        
         objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = difcSponsorReciD;
        objSR.SR_Group__c = 'GS';
        objSR.contact__c = objContact.id;
        insert objsr;
        
       objSRDoc = new SR_Doc__c();
      objSRDoc.Name = 'Test';
      objSRDoc.Service_Request__c = objSR.Id;  
      objSRDoc.status__c ='Uploaded';    
      insert objSRDoc;
      
      
       objAttachment = new Attachment();
      objAttachment.Name = 'New User Access Form.pdf';
      objAttachment.Body = blob.valueOf('test');
      objAttachment.ParentId = objSRDoc.Id;
      insert objAttachment ;
      
      CC_GSCodeCls.CopyBarcodeDetails(objSRDoc.id,objAttachment.id);
      
     
    }
    
}