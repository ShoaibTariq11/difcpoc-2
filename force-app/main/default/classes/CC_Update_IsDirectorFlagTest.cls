/*
    Author      : Leeba
    Date        : 29-March-2020
    Description : Test class for CC_Update_IsDirectorFlagTest
    --------------------------------------------------------------------------------------
*/
@isTest
public class CC_Update_IsDirectorFlagTest{

    public static testMethod void CC_Update_IsDirectorFlag() {
    
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
       insert objsrTemp;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
        
        HexaBPM_Amendment__c objamm = new HexaBPM_Amendment__c ();
        objamm.ServiceRequest__c = objHexaSR.Id;
        objamm.Roles__c = 'Director;Authorised Signatory';
        objamm.Role__c = 'Director;Authorised Signatory';
        objamm.Is_Authorized_Signatory_Flag__c = false;
        objamm.Is_Director_Flag__c = false;
        insert objamm;
       
        
        Test.startTest();
        HexaBPM__Step__c step = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c                                
                                  from HexaBPM__Step__c where Id=:objHexastep.Id];
        CC_Update_IsDirectorFlag CC_Update_IsDirectorFlagObj = new CC_Update_IsDirectorFlag();
        CC_Update_IsDirectorFlagObj.EvaluateCustomCode(objHexaSR,step); 
        Test.stopTest();
       
    }
    
 }