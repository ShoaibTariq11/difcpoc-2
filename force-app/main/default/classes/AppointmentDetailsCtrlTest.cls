@IsTest(SeeAllData=false)
public class AppointmentDetailsCtrlTest {

    static testMethod void AppointmentDetailsCtrlTestOne() {
    
        Test.StartTest(); 

        PageReference pageRef = Page.AppointmentDetails;
        
        Test.setCurrentPage(pageRef);
        AppointmentDetailsCtrl AppointmentDetailsCtrlObj = new AppointmentDetailsCtrl();

        Test.StopTest();       
    }

    static testMethod void AppointmentDetailsCtrlTestTwo() {
    
        Id CurrentUserId = UserInfo.getUserId();
        Id GroupId = Id.ValueOf( Label.DHA_Team_Public_Group );

        GroupMember GM = new GroupMember();
        GM.GroupId = GroupId;
        GM.UserOrGroupId = CurrentUserId;

        insert GM;
        
        Test.StartTest(); 

        PageReference pageRef = Page.AppointmentDetails;
        
        Test.setCurrentPage(pageRef);
        AppointmentDetailsCtrl AppointmentDetailsCtrlObj = new AppointmentDetailsCtrl();

        Test.StopTest();       
    }

    static testMethod void AppointmentDetailsCtrlTestThree() {
    
        Id CurrentUserId = UserInfo.getUserId();
        Id GroupId = Id.ValueOf( Label.DHA_Team_Public_Group );
        
        // Checking membership of the user from the group.
        List<GroupMember> ListGroupMember = [SELECT Id, Group.Name, Group.DeveloperName FROM GroupMember WHERE UserOrGroupId =: CurrentUserId AND GroupId =: GroupId];
        delete ListGroupMember;
        
        Test.StartTest(); 

        PageReference pageRef = Page.AppointmentDetails;
        
        Test.setCurrentPage(pageRef);
        AppointmentDetailsCtrl AppointmentDetailsCtrlObj = new AppointmentDetailsCtrl();

        Test.StopTest();       
    }



}