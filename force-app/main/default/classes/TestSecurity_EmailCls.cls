/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class TestSecurity_EmailCls {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        list<Document_Master__c> lstDocMasters = new list<Document_Master__c>();
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Security Email';
        objDocMaster.Code__c = 'Security Email';
        lstDocMasters.add(objDocMaster);
        
        insert lstDocMasters;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Phone = '+971234567890';
        objAccount.PO_Box__c = '123456';
        insert objAccount;
        
        list<Service_Request__c> lstSRs = new list<Service_Request__c>();
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.Legal_Structures__c ='LTD';
        lstSRs.add(objSR);
        objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.Legal_Structures__c ='LLC';
        lstSRs.add(objSR);
        objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.Legal_Structures__c ='LP';
        lstSRs.add(objSR);
        objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.Legal_Structures__c ='NPIO';
        lstSRs.add(objSR);
        objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.Legal_Structures__c ='GP';
        lstSRs.add(objSR);
        objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.Legal_Structures__c ='LP';
        lstSRs.add(objSR);
        
        objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.Legal_Structures__c ='LLP';
        lstSRs.add(objSR);
        
        insert lstSRs;
        
        list<Amendment__c> lstAmds = new list<Amendment__c>();
        Amendment__c objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = lstSRs[0].Id;
        objAmd.Relationship_Type__c='Shareholder';
        objAmd.Amendment_Type__c='Individual';
        objAmd.Passport_No__c = 'NNE1212';
        lstAmds.add(objAmd);
        
        objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = lstSRs[0].Id;
        objAmd.Relationship_Type__c='Shareholder';
        objAmd.Amendment_Type__c='Body Corporate';
        objAmd.Company_Name__c = 'Test Custoer 1';
        lstAmds.add(objAmd);
        
        objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = lstSRs[0].Id;
        objAmd.Relationship_Type__c='UBO';
        objAmd.Amendment_Type__c='Individual';
        objAmd.Family_Name__c = 'Sample name1';
        objAmd.Given_Name__c = 'Sample name 2';        
        objAmd.Name_of_incorporating_Shareholder__c = 'Test Custoer 1';
        objAmd.Passport_No__c = 'NNE121df2';
        objAmd.Nationality_List__c = 'India'; 
        lstAmds.add(objAmd);
      
        objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = lstSRs[1].Id;
        objAmd.Relationship_Type__c='Member';
        objAmd.Amendment_Type__c='Individual';
        objAmd.Passport_No__c = 'NNE12';
        
     
        
        lstAmds.add(objAmd);
        objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = lstSRs[2].Id;
        objAmd.Relationship_Type__c='Limited Partner';
        objAmd.Amendment_Type__c='Individual';
        lstAmds.add(objAmd);
        
       
        
        objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = lstSRs[3].Id;
        objAmd.Relationship_Type__c='Founding Member';
        objAmd.Amendment_Type__c='Individual';
        objAmd.Passport_No__c = 'NNE12';
        lstAmds.add(objAmd);
        
     
        
        objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = lstSRs[4].Id;
        objAmd.Relationship_Type__c='General Partner';
        objAmd.Amendment_Type__c='Individual';
        objAmd.Passport_No__c = 'NNE1278';
        lstAmds.add(objAmd);
        
         objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = lstSRs[4].Id;
        objAmd.Relationship_Type__c='General Partner';
        objAmd.Amendment_Type__c='Body Corporate';
        objAmd.Company_Name__c = 'Test Custoer 1';
        lstAmds.add(objAmd);
        
        objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = lstSRs[5].Id;
        objAmd.Relationship_Type__c='General Partner';
        objAmd.Amendment_Type__c='Individual';
        objAmd.Passport_No__c = 'NNE1278';
        lstAmds.add(objAmd);
        
        objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = lstSRs[5].Id;
        objAmd.Relationship_Type__c='General Partner';
        objAmd.Amendment_Type__c='Body Corporate';
        objAmd.Company_Name__c = 'Test Custoer 1';
        lstAmds.add(objAmd);

        objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = lstSRs[6].Id;
        objAmd.Relationship_Type__c='Designated Member';
        objAmd.Amendment_Type__c='Individual';
        objAmd.Company_Name__c = 'Test Custoer 1';
        lstAmds.add(objAmd);    
       
        objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = lstSRs[6].Id;
        objAmd.Relationship_Type__c='Designated Member';
        objAmd.Amendment_Type__c='Body Corporate';
        objAmd.Company_Name__c = 'Test Custoer 1';
        lstAmds.add(objAmd);


        insert lstAmds;
        
        Attachment objAttachment = new Attachment();
        objAttachment.Body = blob.valueOf('Test Attachment');
        objAttachment.Name = 'Test.docx.pdf';
        objAttachment.ParentId = lstSRs[0].Id;
        insert objAttachment;
        
        list<SR_Doc__c> lstSRDocs = new list<SR_Doc__c>();
        SR_Doc__c objSRDoc;
        for(Amendment__c objAmdTemp : lstAmds){
             objSRDoc = new SR_Doc__c();
            if(objAmdTemp.Amendment_Type__c == 'individual')
                objSRDoc.name = 'Passport Copy';
            else if(objAmdTemp.Amendment_Type__c == 'Body Corporate')
                objSRDoc.name  = 'Certificate of Incorporation';    
            objSRDoc.Amendment__c = objAmdTemp.Id;
            objSRDoc.Service_Request__c = objAmdTemp.ServiceRequest__c;
            objSRDoc.Doc_ID__c = objAttachment.Id;
            objSRDoc.Unique_SR_Doc__c = objAmdTemp.Id+'_'+objAmdTemp.ServiceRequest__c;
            lstSRDocs.add(objSRDoc);
        }
        objSRDoc = new SR_Doc__c();
        objSRDoc.Amendment__c = lstAmds[0].Id;
        objSRDoc.Service_Request__c = lstAmds[0].ServiceRequest__c;
        objSRDoc.Doc_ID__c = objAttachment.Id;
        objSRDoc.Unique_SR_Doc__c = lstAmds[0].Id+'_'+lstAmds[0].ServiceRequest__c+'_'+lstDocMasters[0].Id;
        objSRDoc.Document_Master__c = lstDocMasters[0].Id;
        lstSRDocs.add(objSRDoc);
        insert lstSRDocs;
        
        List<step__c> steps = new list<step__c>();
        for(Service_Request__c objSRTemp : lstSRs){
            Step__c objStep = new Step__c();
            objStep.SR__c = objSRTemp.Id;
            objStep.SR__r = objSRTemp;
            steps.add(objStep);  // Added by Sravan
            //Security_EmailCls.Send_Email_for_Verification(objStep); commented by Sravan
        }  
        // Added by Sravan 
        insert steps;     
        
        for(step__c s :steps) 
        {
            Security_EmailCls.Send_Email_for_Verification(s);
        }
        // End
    }
    
    static testmethod void myTestMethod2(){
        
        
         // TO DO: implement unit test
        list<Document_Master__c> lstDocMasters = new list<Document_Master__c>();
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Security Email';
        objDocMaster.Code__c = 'Security Email';
        lstDocMasters.add(objDocMaster);
        
        insert lstDocMasters;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Phone = '+971234567890';
        objAccount.PO_Box__c = '123456';
        objAccount.Legal_Type_of_Entity__c = 'FRC';
        insert objAccount;
        
        list<Service_Request__c> lstSRs = new list<Service_Request__c>();
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.Legal_Structures__c ='FRC';
        lstSRs.add(objSR);
       /* objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.Legal_Structures__c ='RLLP';
        lstSRs.add(objSR);
        objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.Legal_Structures__c ='RLP';
        lstSRs.add(objSR);
         objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.Legal_Structures__c ='RP';
        lstSRs.add(objSR);
       */
        
        insert lstSRs;
        
        list<Amendment__c> lstAmds = new list<Amendment__c>();
        Amendment__c objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = lstSRs[0].Id;
        objAmd.Relationship_Type__c='UBO';
        objAmd.Amendment_Type__c='Individual';
        objAmd.Passport_No__c = 'NNE1212';
        objAmd.Nationality_list__c = 'India';
        lstAmds.add(objAmd);
       

        insert lstAmds;
        
        Attachment objAttachment = new Attachment();
        objAttachment.Body = blob.valueOf('Test Attachment');
        objAttachment.Name = 'Test.docx';
        objAttachment.ParentId = lstSRs[0].Id;
        insert objAttachment;
        
        list<SR_Doc__c> lstSRDocs = new list<SR_Doc__c>();
        SR_Doc__c objSRDoc;
        for(Amendment__c objAmdTemp : lstAmds){
            objSRDoc = new SR_Doc__c();
            ObjSRDoc.Name ='Passport Copy';
            objSRDoc.Amendment__c = objAmdTemp.Id;
            objSRDoc.Service_Request__c = objAmdTemp.ServiceRequest__c;
            objSRDoc.Doc_ID__c = objAttachment.Id;
            objSRDoc.Unique_SR_Doc__c = objAmdTemp.Id+'_'+objAmdTemp.ServiceRequest__c;
            lstSRDocs.add(objSRDoc);
        }
        objSRDoc = new SR_Doc__c();
       
        objSRDoc.Amendment__c = lstAmds[0].Id;
        objSRDoc.Service_Request__c = lstAmds[0].ServiceRequest__c;
        objSRDoc.Doc_ID__c = objAttachment.Id;
        objSRDoc.Unique_SR_Doc__c = lstAmds[0].Id+'_'+lstAmds[0].ServiceRequest__c+'_'+lstDocMasters[0].Id;
        objSRDoc.Document_Master__c = lstDocMasters[0].Id;
        lstSRDocs.add(objSRDoc);
        insert lstSRDocs;
        
        List<step__c> steps = new list<step__c>();
        for(Service_Request__c objSRTemp : lstSRs){
            Step__c objStep = new Step__c();
            objStep.SR__c = objSRTemp.Id;
            objStep.SR__r = objSRTemp;
            steps.add(objStep);  // Added by Sravan
            //Security_EmailCls.Send_Email_for_Verification(objStep); commented by Sravan
        }  
        // Added by Sravan 
        insert steps;
        
        Security_EmailCls.SendSecurityEmailforRecognizedComp(steps[0].id);
        
            
    }
    
}