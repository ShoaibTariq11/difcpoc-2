public without sharing class StepProcessBuilderSRUpdate {
    @InvocableMethod(label='GS Step Process Builder SR Update' description='GS Step Process Builder initaiation SR Update') 
    public static void GsStepProcessbuilderActionsSRUpdate(List<Id> stepIds){
    	String objSOQLFieldsToQuery ='SR__c,status__r.name,step_name__c,Step_Template__r.Internal_SR_Status__c,Step_Template__r.External_SR_Status__c,SR__r.Internal_SR_Status__c,SR__r.External_SR_Status__r.Do_Not_Change_SR_Status__c,SR__r.External_SR_Status__c';
    	String objSOQLFilters = 'ID IN ('+DIFCBusinessLogicUtils.prepareStringOfIdsFromListOfId(stepIds)+')';
    	step__c currentStep = DIFCDataServiceUtils.getStepList(objSOQLFieldsToQuery ,objSOQLFilters,'Name' ,1)[0];
  		if(currentStep.step_name__c=='Original Passport Received'){
	  		if(currentStep.status__r.name !='Closed'){
	  			Service_request__c servReqRec = new Service_Request__c();
		  		servReqRec = DIFCApexCodeUtility.serviceRequestStatusUpdateOnStep(currentStep);
		  		if(servReqRec != NULL && servReqRec.id !=NULL){
		  			Step__c step = DIFCApexCodeUtility.prepareStep(true,null,'Pending','Original passport submission',servReqRec.id);
		  			if(!Test.isRunningTest())insert step;
		  		}
		  		update servReqRec;
	  		}else{
	  			String objStepSOQLFieldsToQuery ='SR__c,status__c,step_name__c';
	    		List<step__c> originalPassportSubmissionSteps = new List<step__c>();
	    		originalPassportSubmissionSteps = DIFCDataServiceUtils.getStepList(objStepSOQLFieldsToQuery , 'Step_Name__c=\'Original passport submission\'' +' AND SR__c=\''+currentStep.SR__c+'\''  ,'Name' ,0);
	  			integer indexOfList=0;
	  			for(Step__c stepRec : originalPassportSubmissionSteps){
	  				originalPassportSubmissionSteps[indexOfList] = DIFCApexCodeUtility.prepareStep(false,stepRec,'Closed','','');
		  			indexOfList++;
	  			}
	  			if(originalPassportSubmissionSteps.size() > 0)update originalPassportSubmissionSteps;
	  		}
  		}
  		else if(currentStep.step_name__c=='Entry Permit is Issued'){
  			if(currentStep.status__r.name =='Closed'){
	  			Service_request__c servReqRec = new Service_Request__c();
		  		servReqRec = DIFCApexCodeUtility.serviceRequestStatusUpdateOnStep(currentStep);
		  		if(servReqRec != NULL && servReqRec.id !=NULL){
		  			Step__c step = DIFCApexCodeUtility.prepareStep(true,null,'Pending Customer Inputs','Entry Permit With Airport Entry Stamp Upload',currentStep.SR__c);
		  			insert step;
		  		}
		  		update servReqRec;
	  		}
  		}
    }
}