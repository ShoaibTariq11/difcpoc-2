public class OB_resetPassword {
    
    @AuraEnabled
    public static void resetpassword(String username)
    {
        List<User> users = new List<User>();
       // String username = 'test@test.com';
        users = Database.Query('SELECT Id, Name FROM User WHERE UserName  =: username');
        
        for(User u : users){
            //For reset User password
           // System.resetPassword(u.Id, true);
            
            //For set User password
            System.setPassword(u.Id,username);
        }
        
    }
}