/******************************************************************************************
 *  Author   : Arun Singh
 *  Date     : 24 October 2019
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        		Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    24 October 2019  	Arun          Created

*/
public with sharing class cls_Prescribed_Company_Name_edit {

    public Service_Request__c SRData;
    public string RecordTypeId;
    public Account ObjAccount {get;set;}
    public map < string, string > mapParameters;
    public String CustomerId;

    public cls_Prescribed_Company_Name_edit(ApexPages.StandardController stdController) {
        this.SRData = (Service_Request__c) stdController.getRecord();
        mapParameters = new map < string, string > ();
        if (apexpages.currentPage().getParameters() != null) mapParameters = apexpages.currentPage().getParameters();
        if (mapParameters.get('RecordType') != null) RecordTypeId = mapParameters.get('RecordType');
        for (User objUsr: [select id, ContactId, Email, Phone, Contact.Account.Legal_Type_of_Entity__c, Contact.AccountId, Contact.Account.Company_Type__c, Contact.Account.Trade_Name__c, Contact.Account.Next_Renewal_Date__c, Contact.Account.Name, Contact.Account.Arabic_Name__c, Contact.Account.Trading_Name_Arabic__c, Contact.Account.Contact_Details_Provided__c, Contact.Account.Sector_Classification__c, Contact.Account.License_Activity__c, Contact.Account.Is_Foundation_Activity__c from User where Id =: userinfo.getUserId()]) {
            CustomerId = objUsr.Contact.AccountId;
            if (SRData.id == null) {
                SRData.Customer__c = objUsr.Contact.AccountId;
                SRData.RecordTypeId = RecordTypeId;
                SRData.Email__c = objUsr.Email;
                SRData.Legal_Structures__c = objUsr.Contact.Account.Legal_Type_of_Entity__c;
                SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                //SRData.Entity_Name__c = objUsr.Contact.Account.Name;
                ObjAccount = objUsr.Contact.Account;
                SRData.Previous_Entity_Name__c = ObjAccount.Name;
                //SRData.Entity_Name__c=ObjAccount.Name;
                SRData.Pre_Entity_Name_Arabic__c = ObjAccount.Arabic_Name__c;
                //SRData.Pro_Entity_Name_Arabic__c=ObjAccount.Name;
                SRData.Previous_Trading_Name_1__c = ObjAccount.Trade_Name__c;
                //SRData.Proposed_Trading_Name_1__c=ObjAccount.Name;
                SRData.Pre_Trade_Name_1_in_Arab__c = ObjAccount.Trading_Name_Arabic__c;
                //SRData.pro_trade_name_1_in_arab__c=ObjAccount.Name;
            }
        }

    }
    public PageReference SaveRequest() {
        try {
            upsert SRData;
            PageReference acctPage = new ApexPages.StandardController(SRData).view();
            acctPage.setRedirect(true);
            return acctPage;
        } catch (Exception e) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);
        }
        return null;
    }
}