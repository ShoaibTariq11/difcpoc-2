/*************************************************
* Class : LeadOfferUnitPDFTemplateUtility
* Description : Class to generate the PDF with offer unit when user send email on lead.
* Date : 23/7/2018
* Modification: Name    Assembla     Description
v1.1            Selva    7325        Added Internal and External Area
**************************************************/    
global class LeadOfferUnitPDFTemplateUtility{
    
    global List<Offer_Process__c> offerunitList {get;set;}
    global Lead ld{get;set;}
  //  global List<Lead> leadList {get;set;}
    global string recId=null;
    global string recCompId {get;set;}
    public string leadName {get;set;}
   
    public string rentDayscls {get;set;}
    public string leasTermYearcls {get;set;}
    public string leasTermMonthcls {get;set;}
    Public String headerText {get;set;}
        
    
    global  LeadOfferUnitPDFTemplateUtility(){
        recId = ApexPages.currentPage().getParameters().get('id');
        leadName = ApexPages.currentPage().getParameters().get('Name');
        recCompId = ApexPages.CurrentPage().getParameters().get('p2_lkid');
        offerunitList = new List<Offer_Process__c>();
       
          List<string> objectfieldnames = getCreatableFields('Offer_Process__c');
            String fields = '';
            for(string s:objectfieldnames ){
                if(s.contains('__c')){
                    if(fields==null&& fields==''){
                        fields = s;
                    }
                    else{
                       fields = fields + s + ',';
                    }
                }
            }
            system.debug('-------'+fields);
            fields=fields.substring(0, fields.lastIndexOf(','));
            String queryString = '(select id,Name,'+fields+' from Offer_Process__r)';
       
        system.debug('----ld----'+ld);
        //List<Lead> leadlst = [select id,Name,Company,Offer_Header__c,CreatedBy,(select id,Retail_Sector_classification__c,Marketing_Contribution__c,Grand_Opening_Charges__c,Turnover_Rent__c,firstyearServC__c,X1stYrUtilities__c,Fit_Out_period__c,TotalExternalAreasinSqft__c,X5thYrEBR__c,X4thYrEBR__c,X3rdYrEBR__c,X2ndYrEBR__c,X1stYrEBR__c,InternalArea__c,ExternalArea__c,AvailableBy__c,Status__c,UnitStatus__c,LeasingCommencementDate__c,RentType__c,PaymentTerms__c,Premises__c,Area__c,FirstYearBaseRent__c,SecondYearBaseRent__c,ThirdyearBaseRent__c,FourthYearBaseRent__c,FifthYearBaseRent__c,TotalExternalBaseRent__c,Service_Charges_per_Sq_Ft_per_Annum__c,Utilities_per_Sq_Ft_per_Annum__c,Car_Park_Allotment__c,CarParkReservedUnReserved__c,No_ofCarParks__c,NoofReservedCarParksPaid__c,No_of_Un_Reserved_Car_Parks__c,NoofUnReservedCarParksPaid__c,ReservedParkingCost__c,UnreseverdParkingcost__c,SecurityDeposit__c,Rent_Free_Period__c,HandoverCondition__c,LeasableAreaComments__c,Lease_Commencement_Date__c,Leasing_Terms_Year__c,Leasing_Terms_Months__c,OfferValidityTill__c,Technical_Requirement__c,Important_Attachments__c,AdditionalTerms__c from Offer_Process__r) from Lead where id=:recCompId];
        
        string leadquery ='select id,Name,Company,Offer_Header__c,createdby.Name,createdby.Title,';
        leadquery= leadquery+queryString+' from Lead where id=:recCompId';
         List<Lead> leadlst = Database.query(leadquery);
        if(leadlst.size()>0){
            ld= leadlst[0];
        } 
     }
     
     public static list<String> getCreatableFields(String objAPIName){
       Map<string,string> childFieldsName = new Map<string,string>{};

       Map<String, Schema.SObjectType> gd = Schema.getglobalDescribe();
       SObjectType sot = gd.get(objAPIName);
       
       Map<String, SObjectField> fields = new Map<String, SObjectField>{};
       if(sot.getDescribe().fields.getMap().keyset().size()>0)
           fields = sot.getDescribe().fields.getMap();

       //And drop those tokens in a List
       List<SObjectField> fieldtokens = fields.values();

       List<string> objectFields = new List<String>();

       for(SObjectField fieldtoken:fieldtokens) {
           DescribeFieldResult dfr = fieldtoken.getDescribe();
               objectFields.add(dfr.getLocalName());                       
       }
       return objectFields;
    }
    
}