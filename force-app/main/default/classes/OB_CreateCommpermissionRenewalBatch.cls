/*
Author      : Leeba
Date        : 03-April-2020
Description : Batch class to create Draft Commercial Permmission Renewal SR
--------------------------------------------------------------------------------------
*/
global class  OB_CreateCommpermissionRenewalBatch implements Database.Batchable<sObject>,schedulable{

    global Database.QueryLocator start(Database.BatchableContext BC){
    
    Date dt = system.today().addDays(30);
    String commvalue = 'Yes';
    String statusValue = 'Open';
    String query = 'SELECT Id,Account_ID__c FROM Compliance__c WHERE End_Date__c!= NULL  AND Account__r.Is_Commercial_Permission__c =: commvalue AND End_Date__c <: dt AND Status__c =:statusValue AND Account_ID__c != NULL';
    return Database.getQueryLocator(query);
    
    }
    
    global void execute(Database.BatchableContext BC, List<Compliance__c> scope){
    
    markAccountAsExpired(scope);      
    
    }  
    
    // Finish Method
    global void finish(Database.BatchableContext BC){
    
    }
    
    public void markAccountAsExpired( List<Compliance__c> scope ){
    try{
            system.debug('inside method '+scope);
            set<string> setAccount = new set<string>();
            set<Id> cpId = new set<Id>();
            list<Account> accToUpdate = new list<Account>();
            list<Compliance__c> complToUpdate = new list<Compliance__c>();
            list<Commercial_Permission__c> cpToUPdate = new list<Commercial_Permission__c>();
            list<AccountContactRelation> ACRToUPdate = new list<AccountContactRelation>();
    
            //sr status query
    
            for(Compliance__c complObj : scope ){
                //complObj.Status__c = 'Defaulted';
                //complToUpdate.add(complObj);
                setAccount.add(complObj.Account_ID__c);
            }
    
            if(setAccount.size() > 0){
                set<id>  accountWithoutOpenSr = OB_AgentEntityUtils.hasOpenSr(setAccount);
                if(accountWithoutOpenSr.size() > 0){
    
                    for(Compliance__c complObj : scope ){

                       /*  for(string accId : accountWithoutOpenSr){
                            if(complObj.Account_ID__c == accId  ){
                                complObj.Status__c = 'Defaulted';
                                complToUpdate.add(complObj);                                 
                            }
                        }*/
                        
                        if(accountWithoutOpenSr.contains(complObj.Account_ID__c)){
                             complObj.Status__c = 'Defaulted';
                             complToUpdate.add(complObj);
                        }
                       
                        
                    }
    
                    system.debug('setaccounts===>' +setAccount);
    
            //set account status to cp-expire
            for(Account acc:[Select Id,(select id from AccountContactRelations where IsActive = true),(SELECT Id,HexaBPM__Customer__c FROM HexaBPM__Service_Requests__r LIMIT 1),
            OB_Active_Commercial_Permission__c  from Account where Id IN:accountWithoutOpenSr]){
                acc.ROC_Status__c = 'CP-Expired';
                accToUpdate.add(acc);   
                cpId.add(acc.OB_Active_Commercial_Permission__c);
                for(AccountContactRelation ACR : acc.AccountContactRelations){
                    ACR.IsActive = false;
                    ACRToUPdate.add(ACR);
                }
            }
    
            //set cp to inactive
            for(Commercial_Permission__c cp:[Select Id  from Commercial_Permission__c where Id IN:cpId]){
                cp.Status__c = 'Expired';
                cpToUPdate.add(cp);   
            }
    
    
            if(accToUpdate.size() > 0){
                update accToUpdate;
            }
    
            if(complToUpdate.size() > 0){
                update complToUpdate;
            }
    
            if(cpToUPdate.size() > 0){
                update cpToUPdate;
            }
    
            if(ACRToUPdate.size() > 0){
                update ACRToUPdate;
            }
    
        }
    }
            
            
            
            
                    
    }catch (Exception e){
        Log__c objLog = new Log__c();
        objLog.Description__c = 'Exception OB_CreateCommpermissionRenewalBatch.createCommpermissionRenewalSR'+e.getLineNumber()+' '+e.getMessage();
        objLog.Type__c = 'OB_CreateCommpermissionRenewalBatch';
        insert objLog;
    }
    }
    
    global void execute(schedulablecontext sc){
    OB_CreateCommpermissionRenewalBatch sc1 = new OB_CreateCommpermissionRenewalBatch(); 
    database.executebatch(sc1,200);
    }
}