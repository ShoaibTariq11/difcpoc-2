/******************************************************************************************
 *  Author      : Durga Prasad
 *  Company     : PwC
 *  Date        : 14-Dec-2019     
 *  Description : helper class for OB_SRTriggerHandler
 *  Modification History :
 ----------------------------------------------------------------------------------------
    V.No    Date          Updated By    Description
    =====   ===========   ==========    ============
    v.1.2   03/Feb/2020   Durga         Invoking the helper method to create History Track records.
        
*******************************************************************************************/
public without sharing class OB_ServiceRequestTriggerHelper {
	public static void SubmitMultiStructureApplications(list<HexaBPM__Service_Request__c> TriggerNew){
		list<HexaBPM__Service_Request__c> lstMultiStructureApps = new list<HexaBPM__Service_Request__c>();
		for(HexaBPM__Service_Request__c ObjSR:[Select Id,HexaBPM__Parent_SR__r.HexaBPM__Internal_SR_Status__c,HexaBPM__Internal_SR_Status__c,HexaBPM__External_SR_Status__c,HexaBPM__Internal_Status_Name__c from HexaBPM__Service_Request__c where HexaBPM__Parent_SR__c IN:TriggerNew and HexaBPM__Internal_Status_Name__c='Draft' and RecordType.DeveloperName='Multi_Structure']){
			ObjSR.HexaBPM__Internal_SR_Status__c = ObjSR.HexaBPM__Parent_SR__r.HexaBPM__Internal_SR_Status__c;
			ObjSR.HexaBPM__External_SR_Status__c = ObjSR.HexaBPM__Parent_SR__r.HexaBPM__Internal_SR_Status__c;
			lstMultiStructureApps.add(ObjSR);
		}
		try{
			if(lstMultiStructureApps.size()>0)
				update lstMultiStructureApps;
		}catch(Exception e){
			insert LogDetails.CreateLog(null, 'OB_ServiceRequestTriggerHelper : SubmitMultiStructureApplications', 'Line Number : '+e.getLineNumber()+'\nException is : '+e.getMessage());
		}
	}
    public static void CreateBCAmendment(list<HexaBPM__Service_Request__c> TriggerNew,map<Id,HexaBPM__Service_Request__c> TriggerOldMap){
        list<HexaBPM_Amendment__c> listOBAmendments = new list<HexaBPM_Amendment__c>();
        set<string> setRegistrationNumber = new set<string>();
        for(HexaBPM__Service_Request__c objSR:TriggerNew){
            if(TriggerOldMap==null && objSR.Foreign_entity_registered_number__c!=null){
                setRegistrationNumber.add(objSR.Foreign_entity_registered_number__c);
            }else{
                if(TriggerOldMap!=null && objSR.Foreign_entity_registered_number__c!=TriggerOldMap.get(objSR.Id).Foreign_entity_registered_number__c && TriggerOldMap.get(objSR.Id).Foreign_entity_registered_number__c!=null){
                    setRegistrationNumber.add(objSR.Foreign_entity_registered_number__c);
                }
            }
        }
        system.debug('setRegistrationNumber==>'+setRegistrationNumber);
        if(setRegistrationNumber.size()>0){
            map<string,string> MapExistingBCShareHolders = new map<string,string>();
            for(HexaBPM_Amendment__c Amnd:[Select Id,Registration_No__c,ServiceRequest__c from HexaBPM_Amendment__c where ServiceRequest__c IN:TriggerNew and Registration_No__c IN:setRegistrationNumber and Role__c INCLUDES('Shareholder')]){
                MapExistingBCShareHolders.put(Amnd.Registration_No__c+'-'+Amnd.ServiceRequest__c,Amnd.Id);
            }
            if(TriggerOldMap!=null && TriggerOldMap.size()>0){
                for(HexaBPM__Service_Request__c objSR:TriggerNew){
                    if(objSR.Setting_Up__c=='Branch' && objSR.legal_structures__c=='Company' && objSR.Foreign_entity_registered_number__c!=null){
                        HexaBPM_Amendment__c objAmnd = new HexaBPM_Amendment__c();
                        objAmnd = MapAmendmentFields(objSR);
                        if(MapExistingBCShareHolders.get(objSR.Foreign_entity_registered_number__c+'-'+objSR.Id)!=null)
                            objAmnd.Id = MapExistingBCShareHolders.get(objSR.Foreign_entity_registered_number__c+'-'+objSR.Id);
                        else if(TriggerOldMap.get(objSR.Id).Foreign_entity_registered_number__c!=null && MapExistingBCShareHolders.get(TriggerOldMap.get(objSR.Id).Foreign_entity_registered_number__c+'-'+objSR.Id)!=null)
                            objAmnd.Id = MapExistingBCShareHolders.get(TriggerOldMap.get(objSR.Id).Foreign_entity_registered_number__c+'-'+objSR.Id);
                        
                        if(objAmnd.Id==null)
                            objAmnd.Role__c = 'Shareholder';
                        listOBAmendments.add(objAmnd);
                    }
                }
            }else{
                for(HexaBPM__Service_Request__c objSR:TriggerNew){
                    if(objSR.Setting_Up__c=='Branch' && objSR.legal_structures__c=='Company' && objSR.Foreign_entity_registered_number__c!=null){
                        HexaBPM_Amendment__c objAmnd = new HexaBPM_Amendment__c();
                        objAmnd = MapAmendmentFields(objSR);
                        objAmnd.Role__c = 'Shareholder';
                        listOBAmendments.add(objAmnd);
                    }
                }
            }
        }
        system.debug('listOBAmendments==>'+listOBAmendments);
        if(listOBAmendments.size()>0){
            try{
                upsert listOBAmendments;
            }catch(DMLException e){
                    
            }
        }
    }
    /*
        Method Name :   MapAmendmentFields
        Description :   Method to map the Application fields to Application Amendment
    */
    public static HexaBPM_Amendment__c MapAmendmentFields(HexaBPM__Service_Request__c objSR){
        HexaBPM_Amendment__c objAmnd = new HexaBPM_Amendment__c();
        if(Schema.SObjectType.HexaBPM_Amendment__c.getRecordTypeInfosByDeveloperName().get('Body_Corporate')!=null)
            objAmnd.RecordTypeId = Schema.SObjectType.HexaBPM_Amendment__c.getRecordTypeInfosByDeveloperName().get('Body_Corporate').getRecordTypeId();
        
        objAmnd.ServiceRequest__c = objSR.Id;
        objAmnd.Customer__c = objSR.HexaBPM__Customer__c;
        
        objAmnd.Registration_No__c = objSR.Foreign_entity_registered_number__c;
        objAmnd.Company_Name__c = objSR.Foreign_Entity_Name__c;
        objAmnd.Former_Name__c = objSR.Foreign_entity_former_name__c;
        objAmnd.Registration_Date__c = objSR.Foreign_entity_date_of_registration__c;
        objAmnd.Date_of_Registration__c = objSR.Foreign_entity_date_of_registration__c;
        objAmnd.Place_of_Registration__c = objSR.Foreign_entity_place_of_registration__c;
        objAmnd.Country_of_Registration__c = objSR.Foreign_entity_country_of_registration__c;
        objAmnd.Telephone_No__c = objSR.Foreign_entity_telephone_number__c;
        
        objAmnd.Address__c = objSR.Foreign_entity_address_line_1__c;
        objAmnd.Apartment_or_Villa_Number_c__c = objSR.Foreign_entity_address_line_2__c;
        objAmnd.Permanent_Native_City__c = objSR.Foreign_entity_city_town__c;
        objAmnd.Permanent_Native_Country__c = objSR.Foreign_entity_country__c;
        objAmnd.Emirate_State_Province__c = objSR.Foreign_entity_state_province_region__c;
        objAmnd.PO_Box__c = objSR.Foreign_entity_Po_Box_Postal_Code__c;
        
        objAmnd.Title__c = objSR.Authorised_Individual_Title__c;
        objAmnd.First_Name__c = objSR.Authorised_individual_first_name__c;
        objAmnd.Last_Name__c = objSR.Authorized_individual_last_name__c;
        objAmnd.Email__c = objSR.Authorised_individual_email__c;
        objAmnd.Mobile__c = objSR.Authorised_individual_telephone_no__c;
        objAmnd.Is_this_Entity_registered_with_DIFC__c = 'No';
        return objAmnd;
    }
    /*
        Method Name :   CreateHistoryRecords
        Description :   Method to create the Field History Tracking object
    */
    public static void CreateHistoryRecords(list<HexaBPM__Service_Request__c> TriggerNew,map<Id,HexaBPM__Service_Request__c> TriggerOldMap,boolean IsInsert){
        map<string,string> mapHistoryFields = new map<string,string>();
        map<string,History_Tracking__c> mapHistoryCS = new map<string,History_Tracking__c>();
        if(History_Tracking__c.getAll()!=null){
            mapHistoryCS = History_Tracking__c.getAll();//Getting the Objects from Custom Setting which has to be displayed in the screen for the users
            for(History_Tracking__c objHisCS:mapHistoryCS.values()){
                system.debug('objHisCS==>'+objHisCS);
                if(objHisCS.Object_Name__c!=null && objHisCS.Object_Name__c.tolowercase()=='hexabpm__service_request__c' && objHisCS.Field_Name__c!=null){
                    mapHistoryFields.put(objHisCS.Field_Name__c,objHisCS.Field_Label__c);
                }
            }
        }
        system.debug('mapHistoryCS==>'+mapHistoryCS);
        system.debug('mapHistoryFields==>'+mapHistoryFields);
        list<sobject> lstApplicationHistory = new list<sobject>();
        if(IsInsert){
        	/*
            for(HexaBPM__Service_Request__c app:TriggerNew){
                if(mapHistoryFields!=null && mapHistoryFields.size()>0){
                    for(string objFld:mapHistoryFields.keyset()){
                        string NewValue = '';
                        if(app.get(objFld)!=null)
                            NewValue = app.get(objFld)+'';
                        sobject objHistory = OB_ObjectHistoryTrackingUtil.CreateObjectHistory('Application_History__c',NewValue,'',app.Id,mapHistoryFields.get(objFld),objFld);
                        lstApplicationHistory.add(objHistory);
                    }
                }
            }
            */
        }else{
            system.debug('***Update Block***');
            for(HexaBPM__Service_Request__c app:TriggerNew){
            	if(app.HexaBPM__Internal_Status_Name__c!=null && app.HexaBPM__Internal_Status_Name__c.tolowercase()!='draft'){
	                if(mapHistoryFields!=null && mapHistoryFields.size()>0 && TriggerOldMap.get(app.Id)!=app){
	                    for(string objFld:mapHistoryFields.keyset()){
	                        if(app.get(objFld) != trigger.oldMap.get(app.Id).get(objFld)){
	                            string OldValue = '';
	                            string NewValue = '';
	                            system.debug('OldValue==>'+OldValue);
	                            system.debug('NewValue==>'+NewValue);
	                            if(app.get(objFld)!=null)
	                                NewValue = app.get(objFld)+'';
	                            if(trigger.oldMap.get(app.Id).get(objFld)!=null)
	                                OldValue = trigger.oldMap.get(app.Id).get(objFld)+'';
	                            sobject objHistory = OB_ObjectHistoryTrackingUtil.CreateObjectHistory('Application_History__c',NewValue,OldValue,app.Id,mapHistoryFields.get(objFld),objFld);
	                            lstApplicationHistory.add(objHistory);
	                        }
	                    }
	                }
            	}
            }
        }
        system.debug('lstApplicationHistory==>'+lstApplicationHistory);
        if(lstApplicationHistory.size()>0)
            insert lstApplicationHistory;
    }
}