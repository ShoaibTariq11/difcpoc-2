/******************************************************************************************
 *  Author      : Claude Manaan
 *  Company     : NSI DMCC 
 *  Date        : 2016-18-11
 *  Description : Dedicated test class for new fit-out classes (PHASE 2)
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By        Description
 ----------------------------------------------------------------------------------------            
 V1.0   2016-18-11      Claude            Created
 *******************************************************************************************/
@isTest(seeAllData=false)
public class Test_cls_fitout_phase_two {

    @testsetup static void setupData(){
        
        /* NOTE: THESE DETAILS DO NOT EXIST, AND ARE ONLY USED FOR TESTING */
        WebService_Details__c testCredentials = new WebService_Details__c();
        
        testCredentials.Name = 'Credentials';
        testCredentials.Password__c = 'thisistest12345678';
        testCredentials.SAP_User_Id__c = 'TEST_SAP';
        testCredentials.Username__c = 'TEST_SAP';
        
        insert testCredentials;
        
        Endpoint_URLs__c testCRMUrl = new Endpoint_URLs__c();
        
        testCRMUrl.Name = 'AccountBal';
        testCRMUrl.url__c = 'TEST Url';
        
        insert testCRMUrl;
    }
    
    private static void createTestConfig(){
        
        Sr_Status__c testDraftStatus = Test_CC_FitOutCustomCode_DataFactory.getTestSRStatus('Draft','DRAFT');
        Sr_Status__c testSubmittedStatus = Test_CC_FitOutCustomCode_DataFactory.getTestSRStatus('Submitted','SUBMITTED');
        
        insert new Status__c(Name='Approved',Code__c='APPROVED');
        insert new Status__c(Name='Pending',Code__c='PENDING');
        
        insert new Step_Template__c(Code__c = 'RE_UPLOAD_DOCUMENT',Name='Re-upload Document',Step_RecordType_API_Name__c = 'General');
        insert new Step_Template__c(Code__c = 'UPLOAD_DOCUMENT',Name='Upload Detailed Design Documents',Step_RecordType_API_Name__c = 'General');
        
        insert new List<Sr_Status__c>{testDraftStatus,testSubmittedStatus};
        
        insert Test_CC_FitOutCustomCode_DataFactory.getCountryLookupList();
        
        insert Test_CC_FitOutCustomCode_DataFactory.getTestCountryCodes();
        
    }
    
    ///* TODO: Remove comment during final deployment
    private static testMethod void testRequestContractorAccessCtlr(){
        
        createTestConfig();
        
        SR_Template__c contractorAccessTemplate = Test_CC_FitOutCustomCode_DataFactory.getTestSRTemplate('Request for Contractor Access','Request_Contractor_Access','Fit-Out & Events');
        
        insert contractorAccessTemplate;
        
        Account objContractor = Test_CC_FitOutCustomCode_DataFactory.getTestContractor();
        objContractor.is_Registered__c = true;
        
        insert objContractor;
        
        Account objAccount = Test_CC_FitOutCustomCode_DataFactory.getTestAccount();
        
        insert objAccount;
        
        Building__c testBuilding = Test_CC_FitOutCustomCode_DataFactory.getTestBuilding();
        
        insert testBuilding;
        
        List<Unit__c> testUnits = Test_CC_FitOutCustomCode_DataFactory.getTestUnits(testBuilding.Id);
        Lease__c testLease = Test_CC_FitOutCustomCode_DataFactory.getTestLease(objAccount.Id);
        License__c testLicense = Test_CC_FitOutCustomCode_DataFactory.getTestLicense(objAccount.Id);
        
        //TODO: Add any dependent data declarations 
        insert testUnits;
        insert testLicense;
        insert testLease;
        
        Test.startTest();
        
        Test.setCurrentPage(Page.NewRequestContractorAccess);
        
        ApexPages.currentPage().getParameters().put('clientId',objAccount.Id);
        
        Fo_cls_ContractorAccessFormController ctlr = new Fo_cls_ContractorAccessFormController();
        
        ctlr.srObj = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
        ctlr.srObj.Building__c = testBuilding.Id;
        ctlr.srObj.Contractor__c = objContractor.Id;
        
        ctlr.contractorId = objContractor.Id;
        
        ctlr.srObj.Trade_License_No__c = '123';
        ctlr.srObj.Company_Name__c = 'Test Contractor';
        
        ctlr.srObj.Title__c = 'Mr.';
        ctlr.srObj.Passport_Number__c = '123456AB';
        ctlr.srObj.Email__c = 'test.contractoruat@difc.dev.org';
        ctlr.srObj.First_Name__c = 'TestContractor';
        ctlr.srObj.Mobile_Number__c = '+971 12345678';
        ctlr.srObj.Last_Name__c = '+971 12345678';
        ctlr.srObj.Position__c = 'Tester';
        ctlr.srObj.Place_of_Birth__c = 'Dubai';
        ctlr.srObj.Date_of_Birth__c = System.Today() - 200;
        ctlr.srObj.Expiry_Date__c = System.Today() + 200;
        ctlr.srObj.Nationality_list__c = 'United Arab Emirates';
        
        ctlr.saveContractorAccessRequest();
        
        String newRequestId = [SELECT Id from service_request__c where createddate = TODAY limit 1].Id;
        
        ApexPages.currentPage().getParameters().put('srId',newRequestId);
        
        ctlr = new Fo_cls_ContractorAccessFormController();
        ctlr.setSelectedContractorDetails();
        ctlr.setSelectedBuilding();
        Test.stopTest();
        
    }//*/
    
    private static testMethod void testAmendmentOverride(){
        
        createTestConfig();
        
        Test.startTest();
        
        Service_Request__c tempSr = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
        
        tempSr.Company_Type_list__c = 'Fitout Contractor';
        
        tempSr.RecordTypeId = [SELECT Id from RecordType WHERE DeveloperName = 'Contractor_Registration'].Id;
        tempSr.Trade_License_No__c = '123456';
        tempSr.Name_of_the_Authority__c = 'Government of Dubai';
        tempSr.Company_Name__c = 'Test Contractor';
        
        tempSr.Title__c = 'Mr.';
        tempSr.Passport_Number__c = '123456AB';
        tempSr.Email_Address__c = 'test.contractoruat@difc.dev.org';
        tempSr.First_Name__c = 'TestContractor';
        tempSr.Mobile_Number__c = '+971 12345678';
        tempSr.Send_SMS_To_Mobile__c= '+971 12345678';        
        tempSr.Last_Name__c = 'Lname'; 
        tempSr.Position__c = 'Tester';
        tempSr.Place_of_Birth__c = 'Dubai';
        tempSr.Date_of_Birth__c = System.Today() - 200;
        tempSr.Expiry_Date__c = System.Today() + 200;
        tempSr.Nationality_list__c = 'United Arab Emirates';
        tempSr.Type_Of_Request__c = 'Type D Minor';
        
        insert tempSr;
        
        Step__c testStep = Test_CC_FitOutCustomCode_DataFactory.getTestStep(tempSr.Id,[SELECT ID FROM Status__c where name = 'Pending'].Id,'Test Contractor'); 
    
        testStep.Step_Template__c = [SELECT Id FROM Step_Template__c WHERE Name='Upload Detailed Design Documents' LIMIT 1].Id;
        
        insert testStep;
            
            Test.stopTest();
            
            /* Create amendments for the SR */
        list<Amendment__c> tempAmendList = Test_CC_FitOutCustomCode_DataFactory.getAmendments(new Set<String>{'Consultant/Project Manager'},tempSr.Id);
        
        Id amendmentRecordType = [SELECT ID from RecordType WHERE DeveloperName = 'Contractor_Insurance'].Id;
        
        for(Amendment__c c : tempAmendList){
          c.RecordTypeId = amendmentRecordType;
          c.Name_DDP__c = 'Test Name';
        }
        
        insert tempAmendList;
        
        tempAmendList = [SELECT Id, Record_Type_Name__c FROM Amendment__c where createddate = TODAY];
        
        Test.setCurrentPage(Page.AmendmentOverride);
        
        Fo_cls_amendmentextension testCls = new Fo_cls_amendmentextension(new Apexpages.StandardController(tempAmendList[0]));
        
        tempSr.Type_Of_Request__c = 'Type A';
        
        update tempSr;
        
        testCls = new Fo_cls_amendmentextension(new Apexpages.StandardController(tempAmendList[0]));
        
        tempSr.Type_Of_Request__c = 'Type D Major';
        
        update tempSr;
        
        testCls = new Fo_cls_amendmentextension(new Apexpages.StandardController(tempAmendList[0]));
        
    }
    
    private static testmethod void testEmailHandler(){
        
        createTestConfig();
        
        SR_Steps__c testSrStep = new SR_Steps__c();
    
        testSrStep.Step_No__c = 1;
        
        insert testSrStep; 
        
        SR_Template__c testFitoutTemplate 
            = Test_CC_FitOutCustomCode_DataFactory.getTestSRTemplate('Test','Test','Fit-Out & Events');
            
        insert testFitoutTemplate; // Create an event site visit report template
        
        /* Create a Document master */
        Document_Master__c testDocumentMaster = new Document_Master__c();
        
        testDocumentMaster.Name = 'Test Document Master';
        
        insert testDocumentMaster;
        
        /* Create an SR Template Doc */
        SR_Template_Docs__c testSRTemplateDoc = new SR_Template_Docs__c();
        
        testSRTemplateDoc.SR_Template__c = testFitoutTemplate.Id;
        testSRTemplateDoc.Document_Master__c = testDocumentMaster.Id;
        testSrTemplateDoc.Validate_In_Code_at_Step_No__c = testSrStep.Id;
        
        insert testSRTemplateDoc;
        
        Test.startTest();
        
        Service_Request__c tempSr = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
        
        tempSr.Company_Type_list__c = 'Fitout Contractor';
        
        tempSr.RecordTypeId = [SELECT Id from RecordType WHERE DeveloperName = 'Contractor_Registration'].Id;
        tempSr.Trade_License_No__c = '123456';
        tempSr.Name_of_the_Authority__c = 'Government of Dubai';
        tempSr.Company_Name__c = 'Test Contractor';
        
        tempSr.Title__c = 'Mr.';
        tempSr.Passport_Number__c = '123456AB';
        tempSr.Email_Address__c = 'test.contractoruat@difc.dev.org';
        tempSr.First_Name__c = 'TestContractor';
        tempSr.Mobile_Number__c = '+971 12345678'; 
        tempSr.Send_SMS_To_Mobile__c= '+971 12345678';        
        tempSr.Last_Name__c = 'Lname';
        tempSr.Position__c = 'Tester';
        tempSr.Place_of_Birth__c = 'Dubai';
        tempSr.Date_of_Birth__c = System.Today() - 200;
        tempSr.Expiry_Date__c = System.Today() + 200;
        tempSr.Nationality_list__c = 'United Arab Emirates';
        
        insert tempSr;
        
        Step__c testStep = Test_CC_FitOutCustomCode_DataFactory.getTestStep(tempSr.Id,[SELECT ID FROM Status__c where name = 'Approved'].Id,'Test Contractor'); 
    
        testStep.Step_Template__c = [SELECT Id FROM Step_Template__c WHERE Name='Re-upload Document' LIMIT 1].Id;
        
        insert testStep;
        
        Attachment testAttachment = Test_CC_FitOutCustomCode_DataFactory.getTestAttachment('Test Name','Test Body');
    
        SR_Doc__c testSampleStepDoc = Test_CC_FitOutCustomCode_DataFactory.getTestSrDoc('Sample Step Document',tempSr.Id);
    
        testSampleStepDoc.Step__c = testStep.Id;
        testSampleStepDoc.Sys_IsGenerated_Doc__c = false;
        testSampleSTepDoc.SR_Template_Doc__c = testSRTemplateDoc.ID;
    
        insert testSampleSTepDoc;
        
        testAttachment.ParentId = testSampleSTepDoc.Id;
        
        insert testAttachment;
        
        testSampleSTepDoc.Doc_ID__c = testAttachment.Id;
        
        update testSampleSTepDoc;
    
        Test.stopTest();
        
        Fo_cls_EmailServiceHandler.SrDocEmailDetails docDetails = new Fo_cls_EmailServiceHandler.SrDocEmailDetails();
        
        docDetails.srDocId = testSampleStepDoc.ID;
        docDetails.documentName = 'test_doc.pdf';
        docDetails.emailTemplateName = 'Portal_User_creation_for_contractor_wallet';
        
        Fo_cls_EmailServiceHandler.sendSrDocEmail(new List<Fo_cls_EmailServiceHandler.SrDocEmailDetails>{docDetails});
        
    }
    
    private static testmethod void testContractorForm(){
        
        User objLoggedinUser = new User(id=Userinfo.getUserId());
        objLoggedinUser.SAP_User_Id__c = 'SFIUSER';
        
        update objLoggedinUser;
        
        createTestConfig();
        
        SR_Template__c contractorWalletTemplate = Test_CC_FitOutCustomCode_DataFactory.getTestSRTemplate('Contractor_Registration','Contractor_Registration','Fit-Out & Events');
        
        insert contractorWalletTemplate;
        
        Test.startTest();
        
        Test.setCurrentPage(Page.NewContractorForm);
        
        Fo_cls_ContractorFormController contractorWalletCls = new Fo_cls_ContractorFormController();
        
        contractorWalletCls.srObj = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
        contractorWalletCls.srObj.sr_template__c = contractorWalletTemplate.id;
        
        contractorWalletCls.srObj.Company_Type_list__c = 'Fitout Contractor';
        
        contractorWalletCls.changeIssuingAuthority();
        
        contractorWalletCls.srObj.Trade_License_No__c = '123456';
        contractorWalletCls.selectedAuthority = 'Government of Dubai';
        contractorWalletCls.srObj.Company_Name__c = 'Test Contractor';
        
        contractorWalletCls.srObj.Title__c = 'Mr.';
        contractorWalletCls.srObj.Passport_Number__c = '123456AB';
        contractorWalletCls.srObj.Email_Address__c = 'test.contractoruat@difc.dev.org';
        contractorWalletCls.srObj.First_Name__c = 'TestContractor';
        contractorWalletCls.srObj.Mobile_Number__c = '+971 12345678';         
        contractorWalletCls.srObj.Send_SMS_To_Mobile__c= '+971 12345678';
        contractorWalletCls.srObj.Last_Name__c = 'Lname';
        contractorWalletCls.srObj.Position__c = 'Tester';
        contractorWalletCls.srObj.Place_of_Birth__c = 'Dubai';
        contractorWalletCls.srObj.Date_of_Birth__c = System.Today() - 200;
        contractorWalletCls.srObj.Expiry_Date__c = System.Today() + 200;
        contractorWalletCls.srObj.Nationality_list__c = 'United Arab Emirates';
        contractorWalletCls.isAcceptedTermsAndConditions = true;
        Service_Request__c tempSr = contractorWalletCls.srObj.clone(false,true,false,false);
        upsert tempSr;
        contractorWalletCls.saveContractor();
        
        String newRequestId = [SELECT Id from service_request__c where createddate = TODAY limit 1].Id;
        
        FO_SRValidations.SRValidations(newRequestId);
        
        ApexPages.currentPage().getParameters().put('srId',newRequestId);
        
        contractorWalletCls = new Fo_cls_ContractorFormController();
        
        contractorWalletCls.contractorLicense = Test_CC_FitOutCustomCode_DataFactory.getTestAttachment('Test Name','Test Body');
        contractorWalletCls.contractorPassport = Test_CC_FitOutCustomCode_DataFactory.getTestAttachment('Test Name 2','Test Body 2');
        
        contractorWalletCls.submitContractor();
        
        Step__c testStep = new Step__c(SR__c = newRequestId);     // TODO: Remove comments on second deployment
        CC_cls_FitOutandEventCustomCode.createContractor(testStep);    
        
        Test.stopTest();
        
        Account newContractor = [SELECt Id, Is_Registered__c FROM Account where createddate = TODAY limit 1];
        
        newContractor.BP_No__c = '12345678';
        
        update newContractor;
        
        CC_cls_FitOutandEventCustomCode.approveContractor(testStep);    
        
        contractorWalletCls = new Fo_cls_ContractorFormController();
        
        contractorWalletCls.contractorId = newContractor.Id;
        
        contractorWalletCls.srObj = tempSr;
        
        contractorWalletCls.setSelectedContractor();
        
        /* Create another contractor with no contact */
        Service_Request__c tempSr2 = tempSr.clone(false,true,false,false);
        
        tempSr2.Company_Type_list__c = 'Fitout Contractor';
        tempSr2.Trade_License_No__c = '164456';
        tempSr2.Passport_Number__c = '122456AB';
        tempSr2.Email_Address__c = 'test.contractoruattwo@difc.dev.org';    
        tempSr2.Expiry_Date__c = System.Today() + 200;
        tempSr2.Nationality_list__c = 'United Arab Emirates';
        
        tempSr2.Mobile_Number__c = '+971 12345678';
        tempSr2.Company_Name__c = 'Test Contractor Two';
        tempSR2.Name_of_the_Authority__c = 'Government of Dubai';
        
        CC_cls_FitOutandEventCustomCode.createContractorAccount(tempSr2,false); // TODO: Uncomment on next deployment
        
        Account newContractor2 = [SELECt Id, Is_Registered__c FROM Account where RORP_License_No__c = :tempSr2.Trade_License_No__c];
        
        newContractor2.Is_Registered__c = true;
        newContractor2.Credit_Customer__c = 'Yes';
        update newContractor2;
        
        /* TODO: For actual deployment, revise test classes and new classes by removing is @testvisible annotation; test using actual class */
        
        contractorWalletCls.contractorId = newContractor2.Id;
        
        contractorWalletCls.setSelectedContractor();
        
        contractorWalletCls.contractorId = newContractor.Id;
        
        contractorWalletCls.setPortalUserDetails(false);
        
        contractorWalletCls.clearPortalUserDetails(false);
        
        contractorWalletCls.clearPortalUserDetails(true);
        
        contractorWalletCls.setErrorMessage('Test');
        
        contractorWalletCls.displayErrorMessage('testContractorForm');
        
        contractorWalletCls.getFormattedDateTime(System.Now(),'dd/MM/yyyy HH:mm a');
        
        contractorWalletCls.cancelRequest();
        
        contractorWalletCls.setRecordTypeMap(new Set<String>{'Contractor_Account'});
        
        contractorWalletCls.getRecordTypeId('Contractor_Account','Account');
        
        contractorWalletCls.getRetUrl();
        
        /* Create a refund request */
        Service_request__c testRefund = new Service_Request__c();
        
        testRefund.Customer__c = newContractor.Id;
        testRefund.Contractor__c = newContractor.Id;
        testRefund.Payment_Method__c = 'Cheque';
        testRefund.Event_Brief__c = 'test';
        testRefund.Declaration_Signatory_Name__c = 'test';
        testRefund.Address_Details__c = 'test';
        testRefund.SAP_SGUID__c = 'test';
        testRefund.SAP_OSGUID__c = 'test';
        testRefund.RecordTypeId = [SELECT Id from Recordtype where DeveloperName = 'Request_for_Contractor_Wallet_Refund'].Id;
        
        insert testRefund;
        
        Product2 testFineProduct = Test_CC_FitOutCustomCode_DataFactory.getTestProduct('Fine','ROC');
        
        Product2 testFineProduct2 = Test_CC_FitOutCustomCode_DataFactory.getTestProduct('Fine Two','ROC');
            
        insert testFineProduct; // create the test fine product
        insert testFineProduct2;
        
        Pricing_Line__c testPricingLine = Test_CC_FitOutCustomCode_DataFactory.getTestPricingLine(testFineProduct.Id,'Other');
        
        Pricing_Line__c testPricingLine2 = Test_CC_FitOutCustomCode_DataFactory.getTestPricingLine(testFineProduct2.Id,'Other Two');
        
        insert testPricingLine; // create a test pricing line 
        insert testPricingLine2;
        
        SR_Price_Item__c testSrPriceLineItem 
                = Test_CC_FitOutCustomCode_DataFactory.getTestSrPriceItem(testRefund.Id,testPricingLine.Id,testFineProduct.Id);
                
        testSrPriceLineItem.Status__c = 'Consumed';
        
        SR_Price_Item__c testSrPriceLineItem2 
                = Test_CC_FitOutCustomCode_DataFactory.getTestSrPriceItem(testRefund.Id,testPricingLine2.Id,testFineProduct2.Id);
                
        testSrPriceLineItem2.Status__c = 'Consumed';
            
        insert new List<SR_Price_Item__c>{testSrPriceLineItem,testSrPriceLineItem2}; // create the price line item
        
        Test_CC_FitOutCustomCode_DataFactory.customerId = newContractor.Id;
        
        Receipt__c testReceipt = Test_CC_FitOutCustomCode_DataFactory.getTestReceipt();
            
        testReceipt.Bank_Name__c = '123456-ADCB';
        testReceipt.Payment_Status__c = 'Success';
        testReceipt.Amount__c = 100;
        testReceipt.Transaction_Date__c = System.Now();
        testReceipt.Customer__c = newContractor.Id;
        testReceipt.Receivable_Type__c='PSA Deposit';	
        
        insert testReceipt;
        
        try {
            FO_SRValidations.SRValidations(testRefund.Id);
        } catch (Exception e){
            
        }
        
        Status__c testStatus = [SELECT id FROM Status__c where name = 'Pending'];
        
        Step__c testSTep2 = Test_CC_FitOutCustomCode_DataFactory.getTestStep(testRefund.Id,testStatus.Id,'Test Contractor'); 
        
        insert testSTep2;
        
        ApexPages.currentPage().getParameters().put('stepId',testSTep2.Id);
        ApexPages.currentPage().getParameters().put('srId',testRefund.Id);
        
        FO_Cls_PaymentConfCntlr testPayment = new FO_Cls_PaymentConfCntlr();
        testPayment.objSR = tempSr;
        testPayment.objacc = newContractor2;
        testPayment.AvailablePortalBalance = 10000;
        testPayment.executeOnLoad();
        //testPayment.confirmPayment();
        
        System.debug(testPayment.AvailablePortalBalance);
        System.debug(testPayment.AmountColor);
        System.debug(testPayment.lstSRPriceItems);
        
        testPayment.backToStep();
    }
  

}