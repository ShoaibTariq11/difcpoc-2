public with sharing class cls_Voluntary_Strike_off_edit {

    public string RecordTypeId;
    public map<string,string> mapParameters;
    public String CustomerId;
    public Account ObjAccount{get;set;}
    public Service_Request__c SRData{get;set;}
    public boolean ValidTosubmit;
 

    public cls_Voluntary_Strike_off_edit(ApexPages.StandardController controller) {
    
     SRData=(Service_Request__c )controller.getRecord();
     ValidTosubmit=true;
     
       mapParameters = new map<string,string>();        
        if(apexpages.currentPage().getParameters()!=null)
        mapParameters = apexpages.currentPage().getParameters();              
       if(mapParameters.get('RecordType')!=null) 
       RecordTypeId= mapParameters.get('RecordType');    
       else
      RecordTypeId=Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('Request_for_Voluntary_Strike_off').getRecordTypeId();
         
       
            for(User objUsr:[select id,ContactId,Email,Phone,Contact.Account.Exempted_from_UBO_Regulations__c,Contact.Account.Legal_Type_of_Entity__c,Contact.AccountId,Contact.Account.Company_Type__c,
                             Contact.Account.Financial_Year_End__c,Contact.Account.Next_Renewal_Date__c,Contact.Account.Name,
                             Contact.Account.Qualifying_Type__c,Contact.Account.Qualifying_Purpose_Type__c,Contact.Account.Contact_Details_Provided__c,
                             Contact.Account.Index_Card__c,Contact.Account.Index_Card_Status__c,Contact.Account.Is_Foundation_Activity__c  
                             from User where Id=:userinfo.getUserId()])
            {
                 CustomerId = objUsr.Contact.AccountId;
                 if(SRData.id==null)
                 {
                    SRData.Customer__c = objUsr.Contact.AccountId;
                    SRData.RecordTypeId=RecordTypeId;
                    SRData.Email__c = objUsr.Email;
                    SRData.Legal_Structures__c=objUsr.Contact.Account.Legal_Type_of_Entity__c;
                    SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                    SRData.Entity_Name__c=objUsr.Contact.Account.Name;
                    ObjAccount= objUsr.Contact.Account;
                    
    
                  }
              
              }
     

    }
    
    public string  validationRule()
    {
    
     string Errormessage='';
      
    if(SRData.In_accordance_with_Article_10_1__c!=true ||
    SRData.In_Accordance_with_Article_11__c!=true ||
    SRData.In_Accordance_with_Article_12_1a__c!=true ||
    SRData.In_Accordance_with_Article_12_1bj__c!=true)
    {
       Errormessage='Please provide all the required information.';

    }
    return Errormessage;
    }
    
    public  PageReference SaveRecord()
    {
       string Errormessage=validationRule();
    
    boolean isvalid=String.isBlank(Errormessage);
    
    
        if(isvalid) 
        {       
        
            try     
            {  
                upsert SRData;
                PageReference acctPage = new ApexPages.StandardController(SRData).view();        
                acctPage.setRedirect(true);        
                return acctPage;     
            }catch (Exception e)     
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());         
                ApexPages.addMessage(myMsg);    
            }  
         }
             else
         {
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Errormessage);         
                ApexPages.addMessage(myMsg); 
         }
   
    return null;
    
       
    }

}