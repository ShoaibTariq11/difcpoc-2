/**
 * An apex page controller that exposes the Digital Onboarding site login functionality
 */
@IsTest global with sharing class OB_CommunitiesLoginControllerTest {
    @IsTest(SeeAllData=true) 
    global static void testCommunitiesLoginController () {
     	OB_CommunitiesLoginController controller = new OB_CommunitiesLoginController();
     	System.assertEquals(null, controller.forwardToAuthPage());  
        OB_CommunitiesLoginController.checkPortal('test','test');     
    }    
}