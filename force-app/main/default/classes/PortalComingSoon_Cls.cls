public without sharing class PortalComingSoon_Cls {

    public string LoggedInUser {set; get;}
    
    public PortalComingSoon_Cls(){
       LoggedInUser = UserInfo.getName();
    }
}