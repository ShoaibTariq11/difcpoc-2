/*************************************************************************************************
 *  Name        : UpdateUserInfo
 *  Author      : Saima Hidayat
 *  Company     : NSI JLT
 *  Date        : 31-05-2015    
 *  Purpose     : This class is to update email address on the user related contacts       
**************************************************************************************************/
global without sharing class UpdateUserInfo implements Schedulable {
    global void execute(SchedulableContext contx) {
        usrUpdate();
    }
    public static void usrUpdate(){
    	map<string,Contact> mapContact = new map<string,Contact>();
        map<string,string> mapUsrEmail = new map<string,string>();
        list<Contact> lstcont = new list<Contact>();
        Set<String> contId = new Set<String>();
        for(User usr:[Select id,email,contactId,Community_User_Role__c,Username,User_Email__c,contact.Email from User where contactId!=null AND LastmodifiedDate=TODAY]){
            if(usr.Email != usr.contact.email){
                Contact Pcont = new Contact(id=usr.contactId);
                Pcont.email = usr.email;
                Pcont.Portal_Username__c = usr.Username;
                Pcont.Role__c = usr.Community_User_Role__c;
                mapUsrEmail.put(usr.contactId,usr.email);
                if(!mapContact.containsKey(usr.contactId)){
                	mapContact.put(usr.contactId,Pcont);
                	lstcont.add(Pcont);
                }
            }
            contId.add(usr.contactId);
        }
        for(Contact con :[Select id,Individual_Contact__c from Contact where id IN :contId AND Individual_Contact__c!=null]){
            if(mapUsrEmail.containsKey(con.id)){
                Contact Indcont = new Contact(id=con.Individual_Contact__c);
                Indcont.email = mapUsrEmail.get(con.id);
                if(!mapContact.containsKey(con.Individual_Contact__c)){
                	mapContact.put(con.Individual_Contact__c,Indcont);
                	lstcont.add(Indcont);
                }
                //lstcont.add(Indcont);
            }
        }
        try{
            if(lstcont!=null && lstcont.size()>0)
                update lstcont;
        }catch(Exception ex){
            system.debug('Exception is : '+ex.getMessage());
            Log__c objLog = new Log__c();
            objLog.Description__c ='Line No===>'+ex.getLineNumber()+'---Message==>'+ex.getMessage();
            objLog.Type__c = 'Schedule for User Info Update Failed';
            insert objLog;
        }
    }
    
}