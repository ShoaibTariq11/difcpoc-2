@isTest
private class TestDIFCLoginController_Cls {
    static testMethod void TestLogin() {
        DIFCLoginController loginController = new DIFCLoginController();

        loginController.forwardToCustomAuthPage();
        loginController.login();
        
        //Without cookie
        loginController.redirectToMobilePage();
        
        //with cookie
        Cookie cookie = new Cookie('mobile_cookie', 'mobile', null, -1, false);
		ApexPages.currentPage().setCookies(new Cookie[]{cookie});
        
        loginController.redirectToMobilePage();
        
        Set<String> selectionSet = new Set<String>{'DIFC Clients','IT services','Non DIFC Clients','Non DIFC Clients - Property Listing','Events'};
        
        for(String s : selectionSet){
        	loginController.selectlstVal = s;
        	loginController.RedirectToUserPage();
        }
        
    }
}