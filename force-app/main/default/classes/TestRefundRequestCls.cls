/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData = false)
private class TestRefundRequestCls {
	
	@testSetUp
	static void prepareTestData(){
		Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'AccountBal';
        objEP.URL__c = 'http://accbal.com/sampledata';
        insert objEP;
        
        Block_Trigger__c objCS = new Block_Trigger__c();
        objCS.Block_Web_Services__c = false;
        objCS.Name = 'Block';
        insert objCS;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        
        
         SR_Template__c srTemplate = new SR_Template__c();
        srTemplate.Name = 'Request a Refund';
        srTemplate.SR_RecordType_API_Name__c = 'Refund_Request';
        srTemplate.SR_Group__c = 'Other';
        srTemplate.Menu__c = 'otherServices';
        srTemplate.sub_Menu__c = 'Administrative Services';
        
        insert srTemplate;
        
       
        List<Account> accList = new List<Account>();
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 354545';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '1234';
        accList.add(objAccount);
        
         objAccount = new Account();
        objAccount.Name = 'Test Custoer 354544';
        objAccount.E_mail__c = 'test@tests.com';
        objAccount.BP_No__c = '5678';
        accList.add(objAccount);
        
        insert accList;
        
        Service_Request__c srobj = new Service_Request__c();
        srobj.Type_of_refund__c ='Portal Balance Refund';
        srobj.Mode_of_refund__c = 'Cheque in Favour of the Entity';
        srobj.Refund_Amount__c = 10000;
        srobj.I_agree__c = true;
        srobj.Customer__c = [select id from account limit 1].id;
        srObj.SR_template__c = srTemplate.id;
        srObj.RecordTypeID = [select id from RecordType where DeveloperName = 'Refund_Request' and SobjectType = 'Service_Request__c' limit 1].id;
        insert srobj;        
        // Create SR Template   
	}

    static testMethod void myTestMethod1() {    	
       
       	test.startTest();
       		Test.setMock(WebServiceMock.class, new TestAccountBalanceServiceCls());
	       	list<AccountBalenseService.ZSF_S_ACC_BAL> resActBals = new list<AccountBalenseService.ZSF_S_ACC_BAL>();
	        AccountBalenseService.ZSF_S_ACC_BAL objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
	        objActBal.KUNNR = '1234';
	        objActBal.UMSKZ = 'D';
	        objActBal.WRBTR = '1234';
	        resActBals.add(objActBal);
	        objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
	        resActBals.add(objActBal);
	        
	        TestAccountBalanceServiceCls.resActBals = resActBals;    	
       	test.StopTest();
    	
    	
        Pagereference pgRef = page.GSRefundRequest;
        Test.SetCurrentPage(pgRef);
        ApexPages.StandardController sc= new ApexPages.StandardController(new service_request__c());        
        RefundRequestCls rfndCls = new RefundRequestCls(sc);
        Service_Request__c srobjTemp = new Service_Request__c();
        srobjTemp.Type_of_refund__c ='Portal Balance Refund';
        srobjTemp.Mode_of_refund__c = 'Cheque in Favour of the Entity';
        srobjTemp.Refund_Amount__c = 1000;
        srobjTemp.I_agree__c = true;
        srobjTemp.Customer__c = [select id from account limit 1].id;
        rfndCls.objSR = srobjTemp;
        rfndCls.saveRequest(); 
        rfndCls.editRequest();
        rfndCls.CancelSR();
        rfndCls.getRefundTypes();
        rfndCls.accountName = 'Test Custoer'; 
        rfndCls.SearchAccounts();
       // rfndCls.SubmitRefundRequest();
        
    }
    
    static  testMethod void MyTestMethod2(){
    	service_request__c srObj = new Service_Request__c();
    	 for(Service_Request__c srTemp :[select id,Name,Customer__c,Establishment_Card_No__c,License_Number__c,Email__c,Send_SMS_To_Mobile__c,Type_of_Refund__c,Mode_of_Refund__c,Refund_Amount__c,Last_Name__c,First_Name__c,SAP_SGUID__c,Company_Name__c,Bank_Name__c,Bank_Address__c,Car_Registration_number__c,
                                                   Transfer_to_account__r.Name,Transfer_to_account__c,Transfer_to_account__r.Active_License__r.Name,Submitted_Date__c,Record_type_name__c,External_Status_Name__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,I_agree__c,Terms_Conditions__c,customer__r.Skip_PSA_Validation_for_Refund__c 
                                                   from service_Request__c limit 1]){                                                  
                srObj = srTemp;                  
         }
         
         test.startTest();
       		Test.setMock(WebServiceMock.class, new TestAccountBalanceServiceCls());
	       	list<AccountBalenseService.ZSF_S_ACC_BAL> resActBals = new list<AccountBalenseService.ZSF_S_ACC_BAL>();
	        AccountBalenseService.ZSF_S_ACC_BAL objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
	        objActBal.KUNNR = '1234';
	        objActBal.UMSKZ = 'D';
	        objActBal.WRBTR = '1234';
	        resActBals.add(objActBal);
	        objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
	        resActBals.add(objActBal);
	        
	        TestAccountBalanceServiceCls.resActBals = resActBals;    	
       	test.StopTest();
        
        
	     ApexPages.StandardController sc= new ApexPages.StandardController(new service_request__c());
	     apexPages.currentPage().getParameters().put('id',srObj.id);  
	     RefundRequestCls rfndCls = new RefundRequestCls(sc);
	          
	     //rfndCls.objSR = srObj;
         rfndCls.SubmitRefundRequest();
         
                     
    }
    
    static testmethod void MyTestMethod3(){
    	
    	Pagereference pgRef = page.GSRefundRequest;
        Test.SetCurrentPage(pgRef);
        ApexPages.StandardController sc= new ApexPages.StandardController(new service_request__c());        
        RefundRequestCls rfndCls = new RefundRequestCls(sc);
        Service_Request__c srobjTemp = new Service_Request__c();
        srobjTemp.Type_of_refund__c ='PSA Refund';
        srobjTemp.Mode_of_refund__c = 'Cheque in Favour of the Entity';
        //srobjTemp.Refund_Amount__c = 1000;
        srobjTemp.I_agree__c = true;
        srobjTemp.Customer__c = [select id from account limit 1].id;
        rfndCls.objSR = srobjTemp;
        rfndCls.saveRequest(); 
        
        srobjTemp.Refund_Amount__c = 1000;
        srobjTemp.Mode_of_refund__c = '';
        rfndCls.saveRequest(); 
        
        srobjTemp.Refund_Amount__c = 1000;
        srobjTemp.Mode_of_refund__c = 'PSA Refund';
        srobjTemp.Type_of_Refund__c = '';
        rfndCls.saveRequest(); 
        
       
    	
    }
}