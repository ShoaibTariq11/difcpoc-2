/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_CreateConditions {

    static testMethod void myUnitTest() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@dmcc.com';
        objContact.Phone = '1234567890';
        insert objContact;
      
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'General Letter in Arabic';
        objTemplate.SR_RecordType_API_Name__c = 'General_Letter_in_Arabic';
        objTemplate.ORA_Process_Identification__c = 'REGISTRATION';
        objTemplate.ORA_SR_Type__c = 'DMCC Registration Letters';
        objTemplate.Menutext__c = 'Arabic Letter to Third Party (New)';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        //objTemplate.Code__c = 'Tax_Exemption_Letter';
        insert objTemplate;
        
        Step_Template__c objStepType = new Step_Template__c();
        objStepType.Name = 'Approve Request';
        objStepType.Code__c = 'APPROVE_REQUEST';
        objStepType.Step_RecordType_API_Name__c = 'General';
        objStepType.Summary__c = 'Approve Request';
        insert objStepType;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'AR Third Party NOC';
        insert objDocMaster;
        
        Document_Details__c objDocDetails = new Document_Details__c();
        objDocDetails.Account__c = objAccount.Id;
        objDocDetails.EXPIRY_DATE__c = system.today().addYears(1);
        objDocDetails.DOCUMENT_STATUS__c = 'Active';
        objDocDetails.ISSUE_DATE__c = system.today();
        objDocDetails.DOCUMENT_TYPE__c = 'PIC';
        insert objDocDetails;
        
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.On_Submit__c = true;
        objTempDocs.Generate_Document__c = true;
        insert objTempDocs;
        
        list<Status__c> lstStatus = new list<Status__c>();
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Awaiting Approval';
        objStatus.Type__c = 'Start';
        objStatus.Code__c = 'AWAITING_APPROVAL';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Approved';
        objStatus.Type__c = 'Intermediate';
        objStatus.Code__c = 'APPROVED';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Rejected';
        objStatus.Type__c = 'End';
        objStatus.Code__c = 'REJECTED';
        lstStatus.add(objStatus);
        
        insert lstStatus;
        
        SR_Steps__c objSRStep = new SR_Steps__c();
        objSRStep.SR_Template__c = objTemplate.Id;
        objSRStep.Step_RecordType_API_Name__c = 'General';
        objSRStep.Step_Template__c = objStepType.Id; // Step Type
        objSRStep.Summary__c = 'Approve Request';
        objSRStep.Step_No__c = 1.0;
        objSRStep.Start_Status__c = lstStatus[0].Id; // Default Step Status
	    insert objSRStep;
	    
	    Business_Rule__c objBR = new Business_Rule__c();
	    objBR.Action_Type__c = 'Default Actions';
	    objBR.Execute_on_Insert__c = true;
	    objBR.Execute_on_Update__c = true;
	    objBR.SR_Steps__c = objSRStep.Id;
	    insert objBR;
	    
	    list<BR_Objects__c> lstCSData = new list<BR_Objects__c>();
	    lstCSData.add(new BR_Objects__c(Name='Service_Request__c',sObject_Name__c='Service_Request__c'));
	    lstCSData.add(new BR_Objects__c(Name='Cancel Employee',Class_Name__c='cls_executeAction',Method_Name__c='CancelEmployee',Class_Method__c=true));
	    insert lstCSData;
	    
	    Apexpages.currentPage().getParameters().put('id',objSRStep.Id);
	    ApexPages.currentPage().getParameters().put('ActionType','Default Actions');
	    CreateConditions objCreateConditions = new CreateConditions();
	    
	    list<Condition__c> lstConditions = new list<Condition__c>();
	
	    Condition__c objCondition = new Condition__c();
	    objCondition.Business_Rule__c = objBR.Id;
	    objCondition.Object_Name__c = 'Service_Request__c';
	    objCondition.Field_Name__c = 'Name';
	    objCondition.Value__c = '';
	    objCondition.Operator__c = '!=';
	    objCondition.S_No__c = 1;
	    lstConditions.add(objCondition);
        
        objCondition = new Condition__c();
	    objCondition.Business_Rule__c = objBR.Id;
	    objCondition.Object_Name__c = 'Service_Request__c';
	    objCondition.Field_Name__c = 'name';
	    objCondition.Value__c = '';
	    objCondition.Operator__c = '!=';
	    objCondition.S_No__c = 2;
	    lstConditions.add(objCondition);
	    insert lstConditions;
	    
	    objCreateConditions.getConditions();
	    objCreateConditions.selectedRowIndex = 0;
	    objCreateConditions.ChangesObjectField();
	    objCreateConditions.SaveBR();
	    objCreateConditions.CancelBR();
	    objCreateConditions.addRow();
	    objCreateConditions.EditBR();
	    objCreateConditions.saveConditions();
	    objCreateConditions.FilterCondition = '(1 OR 2)';
	    objCreateConditions.saveConditions();
	    objCreateConditions.cancelCondition();
	    objCreateConditions.DeleteCondition();
	    objCreateConditions.updateBRCondition();
	    objCreateConditions.iSelectedRow = 0;
	    objCreateConditions.getSelectedObjectFlds();
        // TO DO: implement unit test
    }
}