public with sharing class GSONotificationController {


    public list<GSO_Employee_Service_Notification__mdt> gsoNotifications { get; set; }

    public GSONotificationController (){
    
        gsoNotifications = new list<GSO_Employee_Service_Notification__mdt>();
        
        for(GSO_Employee_Service_Notification__mdt gse : [select MasterLabel,Notification_Details__c from GSO_Employee_Service_Notification__mdt where is_active__c = true]){
           gsoNotifications.add(gse);
        }
        
        
    }
    
     public string Department{get; set;}
          
          public List<Service_Notification__c> getListServiceNotification()
          {
          
              return [select Department__c,Notification_Details__c from Service_Notification__c where Department__c=:Department];
          
          }
           
           
}