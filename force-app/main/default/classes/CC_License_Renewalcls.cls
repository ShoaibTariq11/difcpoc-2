/*
	Author Name	:	Durga Prasad
	Class Name	:	CC_License_Renewalcls
	Description	:	Custom code which will be Renew the License.
*/
public without sharing class CC_License_Renewalcls {
	public static string Renew_License(Step__c stp){
		string strResult = 'Success';
		if(stp!=null && stp.SR__c!=null && stp.SR__r.Customer__c!=null && stp.SR__r.License_Renewal_Term__c!=null && stp.SR__r.License_Renewal_Term__c.isNumeric()){
			integer RenewalYears = integer.valueOf(stp.SR__r.License_Renewal_Term__c);
			Service_Request__c objSR = new Service_Request__c(Id=stp.SR__c);
			License__c objLicenseRenew = new License__c();
			//and Status__c='Active' 
			for(License__c objLic:[Select id,Last_Renewal_Date__c,License_Issue_Date__c,License_Expiry_Date__c from License__c where Account__c=:stp.SR__r.Customer__c limit 1]){
				objSR.License__c = objLic.Id;
				objLicenseRenew = objLic;
				if(objLic.License_Expiry_Date__c!=null)
					objLicenseRenew.Last_Renewal_Date__c = objLic.License_Expiry_Date__c.addDays(1);
				else
					objLicenseRenew.Last_Renewal_Date__c = system.today();
					
				objLicenseRenew.License_Expiry_Date__c = objLicenseRenew.License_Expiry_Date__c.addYears(RenewalYears);
				//objLicenseRenew.License_Expiry_Date__c = objLicenseRenew.License_Expiry_Date__c.addDays(-1);
			}
			if(objLicenseRenew!=null && objLicenseRenew.Id!=null){
				try{
					update objLicenseRenew;
					update objSR;
				}catch(Exception e){
					string DMLError = e.getdmlMessage(0)+'';
					if(DMLError==null){
						DMLError = e.getMessage()+'';
					}
	                strResult = DMLError;
					return strResult;
				}
			}else{
				strResult = 'There is no active license for renewal.';
				return strResult;
			}
		}
		return strResult;
	}
}