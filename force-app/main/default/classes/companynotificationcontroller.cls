public class companynotificationcontroller {
    public ID SRID {get; set;}
    public List<Amendment__c> amendlist = new List<Amendment__c>();

    public List<Amendment__c> getamendlist(){
         List<Amendment__c> amendlist = [select Given_Name__c,Family_Name__c,Date_of_Birth__c,Nationality_list__c,Passport_No__c,Passport_Expiry_Date__c,Mobile__c,Person_Email__c,ServiceRequest__c from Amendment__c where ServiceRequest__c =:SRID and status__c = 'Draft'];
        return amendlist;
    }
}