/******************************************************************************************
*  Name        : OB_ECDocumentsControllerTest
*  Author      : Leeba
*  Company     : PwC
*  Date        : 11-March-2020
*  Description : Test Class for OB_ECDocumentsController          
*******************************************************************************************/

@isTest
public class OB_ECDocumentsControllerTest {

    public static testMethod void testMethod1(){
    
       //Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In_Principle').getRecordTypeId();
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        
        insert objHexaSR;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.OB_Application__c = objHexaSR.Id;
        insert objSR;
        
        HexaBPM__Document_Master__c docmaster = new HexaBPM__Document_Master__c ();
        docmaster.HexaBPM__Code__c ='123';
        insert docmaster;
        
        HexaBPM__SR_Template_Docs__c objSRTemp = new HexaBPM__SR_Template_Docs__c();
        objSRTemp.Visible_to_GS__c = true;
        objSRTemp.HexaBPM__Document_Master__c = docmaster.id;
        insert objSRTemp;
        
        HexaBPM__SR_Doc__c objSRDoc = new HexaBPM__SR_Doc__c();
        objSRDoc.HexaBPM__Service_Request__c = objHexaSR.Id;
        objSRDoc.Name = 'test';
        objSRDoc.HexaBPM__SR_Template_Doc__c = objSRTemp.Id;
        insert objSRDoc;
        
        

        Test.StartTest();    
        ApexPages.currentPage().getParameters().put('Id', String.valueOf(objSR.Id));         
        OB_ECDocumentsController  testDoc = new OB_ECDocumentsController();
        testDoc.LoadDocuments(); 
        testDoc.ViewSR();       
        Test.StopTest();
    }


}