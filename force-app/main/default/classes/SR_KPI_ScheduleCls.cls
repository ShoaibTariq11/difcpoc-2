/*************************************************************************************************
 *  Name        : SR_KPI_ScheduleCls
 *  Author      : Saima Hidayat
 *  Company     : NSI JLT
 *  Date        : 24-02-2015    
 *  Purpose     : This class is to calculate RoC and Client Step durations on SR         
**************************************************************************************************
Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------             
 V1.1    04-04-2016     Swati       calculate RORP 
 V1.2    12-07-2016     Ravi        Added the logic to put Database.update to update the SR's
 V1.3    03-08-2016     Sravan      Replaced the normal method with the batch class 
 ----------------------------------------------------------------------------------------*/
global without sharing class SR_KPI_ScheduleCls implements Schedulable {
    global void execute(SchedulableContext ctx) {
        Database.executeBatch(new SR_KPI_BatchCls(),20);  // V1.3
        
    }
  
}