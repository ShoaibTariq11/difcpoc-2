public with sharing class CreatedBigObject
{
   
    public  Void CreatedObject(string ObjName)
    {
    
    
MetadataService.MetadataPort service = new MetadataService.MetadataPort();  
service.SessionHeader = new MetadataService.SessionHeader_element();  
service.SessionHeader.sessionId = UserInfo.getSessionId();  
  
//AllOrNoneHeader is mandatory for Big Object  
service.AllOrNoneHeader = new MetadataService.AllOrNoneHeader_element();  
service.AllOrNoneHeader.allOrNone = true; //this is required for Big Objects  
  
  
List<MetadataService.CustomField> lstCustomFields = new List<MetadataService.CustomField>();  
  
 
  
Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(ObjName).getDescribe().Fields.getMap();
    

//define Big Object  
MetadataService.CustomObject bigObject = new MetadataService.CustomObject();  
bigObject.fullName = ObjName+'__b';  
bigObject.label = ObjName;  
bigObject.pluralLabel = ObjName;  
bigObject.deploymentStatus = 'Deployed'; //Make it Deployed if you need

MetadataService.CustomField fieldObj = new MetadataService.CustomField();  
fieldObj.type_x = 'Text';  
fieldObj.label = 'Name';  
fieldObj.fullName = 'Name__c';  
fieldObj.length = 50;  
fieldObj.required = true;  
lstCustomFields.add(fieldObj);

        if (fMap != null)
        {
            for (Schema.SObjectField ft : fMap.values())
            { 
            
            Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
            
            if(fd.getName()!='Name' & (fd.isUpdateable() || fd.isCreateable()) )
            {
            // loop through all field tokens (ft)
                
                
                    
                fieldObj = new MetadataService.CustomField();  
                string fullName=fd.getName();
                
                if(!fullName.Contains('__c'))
                    fullName=fullName+'__c';
                string fieldType=string.valueof(fd.getType());
                
             if(fieldType=='DATETIME')
                {
                    fieldObj = new MetadataService.CustomField();  
                    fieldObj.type_x = 'DateTime';  
                    fieldObj.label = fd.getLabel();
                    fieldObj.fullName = fullName;  
                    lstCustomFields.add(fieldObj);

                    
                }
                else if(fd.getLength()<=255)// && fd.getName().Contains('__c')==true)
                {
                    fieldObj.type_x = 'Text';   
                    fieldObj.label = fd.getLabel();
                    fieldObj.fullName = fullName;
                    fieldObj.length = 255;  
                    fieldObj.required = false; 
                    lstCustomFields.add(fieldObj);  
                    
                    //fieldObj.type_x = 'Text';  
                    //fieldObj.label = 'Name';  
                    //fieldObj.fullName = 'Name__c';  
                    //fieldObj.length = 50;  
                    //fieldObj.required = true;  

                System.debug('fieldObj===>'+fieldObj.fullName);
                System.debug('type_x===>'+fieldObj.type_x);
                System.debug('label===>'+fieldObj.label);
                System.debug('fullName===>'+fieldObj.fullName);
                
            
                System.debug('required===>'+fieldObj.required);
                //System.debug('length===>'+fieldObj.length);
                
                
                }
                else //if(fd.getName().Contains('__c')==true)
                {
                   // fieldObj.type_x = 'LongTextArea';   
                    //fieldObj.label = fd.getLabel();
                  //  fieldObj.length = fd.getLength();
                    //fieldObj.visibleLines = 3;
                    //fieldObj.unique = false; 
                    //fieldObj.required = false;  
                    
                    
                    fieldObj.type_x = 'LongTextArea';  
                    fieldObj.label = fd.getLabel();
                    fieldObj.fullName = fullName;
                    
                    fieldObj.length = fd.getLength();  
                    fieldObj.visibleLines = 3;  
                    fieldObj.required = false;  
                    fieldObj.unique = false;   
                    System.debug('length===>'+fd.getLength());
    
                    lstCustomFields.add(fieldObj);  
                }
                
            
                
            }
            
            }
        }
        
  

        
  /*
//define Text Field  
MetadataService.CustomField fieldObj = new MetadataService.CustomField();  
fieldObj.type_x = 'Text';  
fieldObj.label = 'Name';  
fieldObj.fullName = 'Name__c';  
fieldObj.length = 50;  
fieldObj.required = true;  
lstCustomFields.add(fieldObj);  
  
//define DateTime Field  
fieldObj = new MetadataService.CustomField();  
fieldObj.type_x = 'DateTime';  
fieldObj.label = 'StartDate';  
fieldObj.fullName = 'StartDate__c';  
lstCustomFields.add(fieldObj);  
  
//define Number Field  
fieldObj = new MetadataService.CustomField();  
fieldObj.type_x = 'Number';  
fieldObj.label = 'Amount';  
fieldObj.fullName = 'Amount__c';  
fieldObj.externalId = false;  
fieldObj.precision = 18;  
fieldObj.required = false;  
fieldObj.scale = 2;  
fieldObj.unique = false;          
lstCustomFields.add(fieldObj);  
  
//define Long Text Area Field  
fieldObj = new MetadataService.CustomField();  
fieldObj.type_x = 'LongTextArea';  
fieldObj.label = 'Description';  
fieldObj.fullName = 'Description__c';  
fieldObj.length = 33000;  
fieldObj.visibleLines = 3;  
fieldObj.required = false;  
fieldObj.unique = false;          
lstCustomFields.add(fieldObj);  
  
 
//define Lookup Relationship with Account  
//fieldObj = new MetadataService.CustomField();  
//fieldObj.type_x = 'Lookup';  
//fieldObj.label = 'Account';  
//fieldObj.fullName = 'Account__c';  
//fieldObj.relationshipLabel = 'Applications';  
//fieldObj.relationshipName = 'Applications';  
//fieldObj.referenceTo = 'Account';  
//lstCustomFields.add(fieldObj);  
  */
bigObject.fields = lstCustomFields;  
  
//define index  
List<MetadataService.Index> lstIndex = new List<MetadataService.Index>() ;  
MetadataService.Index indexObj = new MetadataService.Index();  
indexObj.label = 'MyIndex';  
indexObj.fullName = 'MyIndex';  
  
bigObject.indexes = lstIndex;  
  
//define index field and add it to index  
List<MetadataService.IndexField> lstIndexFields = new List<MetadataService.IndexField>();  
MetadataService.IndexField indfl = new MetadataService.IndexField();  
indfl.name = 'Name__c';  
indfl.sortDirection = 'ASC';  
lstIndexFields.add(indfl);  
  
indexObj.fields = lstIndexFields;  
lstIndex.add(indexObj);  
  
//finally create Big Object with Index field  
List<MetadataService.SaveResult> saveResults =          
    service.createMetadata(  
        new MetadataService.Metadata[] { bigObject});         
  
MetadataService.SaveResult saveResult = saveResults[0];  
  
if(saveResult==null || saveResult.success)  
    return;  
// Construct error message   
if(saveResult.errors!=null)  
{  
    System.debug('errors=' + JSON.serialize(saveResult.errors));  
    List<String> messages = new List<String>();  
    messages.add(  
        (saveResult.errors.size()==1 ? 'Error ' : 'Errors ') +  
            'occured processing component ' + saveResult.fullName + '.');  
    for(MetadataService.Error error : saveResult.errors)  
        messages.add(  
            error.message + ' (' + error.statusCode + ').' +  
            ( error.fields!=null && error.fields.size()>0 ?  
                ' Fields ' + String.join(error.fields, ',') + '.' : '' ) );  
    if(messages.size()>0)  
        System.debug(String.join(messages, ' '));  
}  
if(!saveResult.success)  
    System.debug('Request failed with no specified error.'); 
    
    
    }


}