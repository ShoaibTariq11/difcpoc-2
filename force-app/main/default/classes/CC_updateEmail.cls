/*
    Author      : Prateek Kadkol
    Date        : 10-FEB-2020
    Description : This upadtes the email in the related applications
    --------------------------------------------------------------------------------------
*/

global without sharing class CC_updateEmail implements HexaBPM.iCustomCodeExecutable{
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp)
    {
        system.debug('@@@@@@@@@ entered into CC_UpdateContactOnApporval');
        string strResult = 'Success';
        if(stp!=null && stp.HexaBPM__SR__c!=null){
            try{
                list<HexaBPM__Service_Request__c> updateSrList =  new list<HexaBPM__Service_Request__c>();
                if(stp.HexaBPM__SR__r.HexaBPM__Record_Type_Name__c == 'Converted_User_Registration'){

                    for(account accObj : [select id,(select id from HexaBPM__Service_Requests__r WHERE id !=: stp.HexaBPM__SR__c)
                    from account WHERE id =: stp.HexaBPM__SR__r.HexaBPM__Customer__c  ]){
                        if(accObj.HexaBPM__Service_Requests__r != null && accObj.HexaBPM__Service_Requests__r.size() > 0){
                            for(HexaBPM__Service_Request__c relSr : accObj.HexaBPM__Service_Requests__r){
                                relSr.HexaBPM__Email__c = stp.HexaBPM__SR__r.HexaBPM__Email__c;
                                updateSrList.add(relSr);
                            }
                        }
                    }

                    contact conToUpdate = new contact(id = stp.HexaBPM__SR__r.HexaBPM__Contact__c);
                    conToUpdate.Email = stp.HexaBPM__SR__r.HexaBPM__Email__c;

                    update conToUpdate;

                    for(user customerUser : [select id from user where contactId =:stp.HexaBPM__SR__r.HexaBPM__Contact__c LIMIT 1]){
                        customerUser.Email = stp.HexaBPM__SR__r.HexaBPM__Email__c;
                        update customerUser;
                    }
                    if(updateSrList.size() > 0 ){
                        update updateSrList;
                    }
                   
                    
                }

            }catch(DMLException e){
                /*
                    string DMLError = e.getdmlMessage(0)+'';
                    if(DMLError==null){
                        DMLError = e.getMessage()+'';
                    }
                    strResult = DMLError;
             	*/
				strResult =  e.getdmlMessage(0)+'';   
            }
        }
        return strResult;
    }   
}