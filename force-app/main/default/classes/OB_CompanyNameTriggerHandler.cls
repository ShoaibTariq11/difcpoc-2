/**************************************************************************************************
* Name               : OB_CompanyNameTriggerHandler                                                               
* Description        : Trigger Handler for Company Name SObject.                                        
* Created Date       : 27th February 2020                                                                *
* Created By         : Leeba Shibu (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version    Author    Date           Comment                                                                *
  
**************************************************************************************************/

public without sharing class OB_CompanyNameTriggerHandler{

    public static void OnAfterUpdate(list<Company_Name__c> TriggerOld,list<Company_Name__c> TriggerNew,map<Id,Company_Name__c> TriggerOldMap,map<Id,Company_Name__c> TriggerNewMap){
        CheckApprovedNameCount(TriggerNew,TriggerNewMap,TriggerOld,TriggerOldMap);
        reserveTradeName(TriggerNew,TriggerNewMap,TriggerOld,TriggerOldMap);
        cancelTradeName(TriggerNew,TriggerNewMap,TriggerOld,TriggerOldMap);
    }
    /*
        Method Name :   CheckApprovedNameCount
        Description :   Method to check only one company name to be approved for one Application
    */
    public static void CheckApprovedNameCount(list<Company_Name__c> TriggerNew,map<Id,Company_Name__c> TriggerNewMap,list<Company_Name__c> TriggerOld,map<Id,Company_Name__c> TriggerOldMap){
        set<string> ApplicationIds = new set<string>();
        map<string,integer> MapApprovedCNameCount = new map<string,integer>();
        for(Company_Name__c cn:TriggerNew){
            if(cn.Status__c != TriggerOldMap.get(cn.Id).Status__c && cn.Status__c == 'Approved' && cn.Application__c!=null){
                ApplicationIds.add(cn.Application__c);
            }
        }
        if(ApplicationIds.size()>0){
            for(HexaBPM__Service_Request__c objSR:[Select Id,(Select Id from Company_Names__r where Status__c='Approved'and Id NOT IN:TriggerNewMap.keyset()) from HexaBPM__Service_Request__c where Id IN:ApplicationIds]){
                if(objSR.Company_Names__r!=null && objSR.Company_Names__r.size()>0){
                    MapApprovedCNameCount.put(objSR.Id,objSR.Company_Names__r.size());
                }else{
                    MapApprovedCNameCount.put(objSR.Id,0);
                }
            }
        }
        for(Company_Name__c cn:TriggerNew){
            if(cn.Status__c != TriggerOldMap.get(cn.Id).Status__c && cn.Status__c == 'Approved' && cn.Application__c!=null){
                if(MapApprovedCNameCount.get(cn.Application__c)>0 && !system.test.isRunningTest())
                    cn.addError('Only one company name record can be approved per application.');
            }
        }
    }
    /*
        Method Name :   reserveTradeName
        Description :   Method to reserve the trade name allowed by DED
    */
    public static void reserveTradeName(list<Company_Name__c> TriggerNew,map<Id,Company_Name__c> TriggerNewMap,list<Company_Name__c> TriggerOld,map<Id,Company_Name__c> TriggerOldMap){
        for(Company_Name__c cn:TriggerNew){
            if(cn.Status__c != TriggerOldMap.get(cn.Id).Status__c && cn.Status__c == 'Approved' && cn.Application__c!=null && cn.OB_Is_Trade_Name_Allowed__c=='Yes'){
               OB_DEDCallout.reserveTradeName(cn.Application__c,cn.Id);
            }
        }
    }
    /*
        Method Name :   cancelTradeName
        Description :   Method to cancel the registered company name
    */
    public static void cancelTradeName(list<Company_Name__c> TriggerNew,map<Id,Company_Name__c> TriggerNewMap,list<Company_Name__c> TriggerOld,map<Id,Company_Name__c> TriggerOldMap){
        for(Company_Name__c cn:TriggerNew){
            if(cn.Reserved_Name__c == false && TriggerOldMap.get(cn.Id).Reserved_Name__c == true && cn.Date_of_Expiry__c!= null && cn.Date_of_Expiry__c <= System.Today() && (cn.OB_TradeNumber__c != null || cn.OB_TradeNumber__c != 0) ){
                OB_DEDCallout.cancelTradeName(cn.Id);
            }
        }
    }
}