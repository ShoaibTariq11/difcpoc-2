/*
    Created By  : Shabbir - DIFC on 11 Sep,2015
    Description : Test class for TenancyTrg 
--------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By      Description
 ----------------------------------------------------------------------------------------              
 ----------------------------------------------------------------------------------------
*/
@isTest
private class Test_TenancyTrg {

    public static Account objAccount;
    public static Service_Request__c objSR;
    public static Step__c objStep;
    public static Amendment__c amndRemCon;
    public static Unit__c objUnit;
    public static Lease__c objLease;
    public static Tenancy__c objTen;
    public static Building__c objBuilding;
    public static Amendment__c objAmend;
    
    static void init(){
        objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c ='Pakistan';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;      
        
        string strRecType = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Lease_Application_Request' and sObjectType='Service_Request__c']){
            strRecType = rectyp.Id;
        }    
        
        objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        if(strRecType!='')
            objSR.RecordTypeId = strRecType;
        insert objSR;
        
        objBuilding = new Building__c();
        objBuilding.Name = 'Gate Building';
        objBuilding.Building_No__c = '1234';
        objBuilding.Company_Code__c = '5300';
        insert objBuilding;
        
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Building_Name__c = 'Gate Building';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'DIFC Short Term Business Lease';
        insert objUnit;
        
        objLease = new Lease__c();
        objLease.Account__c = objAccount.Id;
        objLease.End_Date__c = system.today().addYears(1);
        objLease.Type__c = 'Leased';
        objLease.Status__c = 'Active';
        objLease.Lease_Types__c = 'Business Centre Lease';
        insert objLease;  
                        
    }
    
    /** Test with signage and lease email **/
    static testMethod void testSignageEmailAndLeaseEmail() {
        init();
        Test.startTest();

        String AmendRecTypeId = '';
        for(RecordType objRT : [select Id,Name from RecordType where DeveloperName='Business_Centre_Office' AND SobjectType='Amendment__c' AND IsActive=true])
            AmendRecTypeId = objRT.Id;      
                
        objAmend = new Amendment__c();
        objAmend.Lease_Unit__c = objUnit.Id;
        objAmend.RecordTypeId = AmendRecTypeId;
        objAmend.Unit_Rent__c = 1000;
        objAmend.No_of_Desks__c = 20;
        objAmend.ServiceRequest__c = objSR.id;
        insert objAmend;
        
        objTen = new Tenancy__c();
        objTen.Lease__c = objLease.Id;
        objTen.End_Date__c = system.today().addYears(1);
        objTen.Unit__c = objUnit.Id;
        insert objTen;          
        
        Test.stopTest();
    }   
    
    

}