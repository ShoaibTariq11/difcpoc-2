/*
* Description: Get the list of service request of looged in user's account
*              Search by service number, status and Request Type
**/
public without sharing class DIFC_RequestedServicesController {
    
    // collection of both service request object 
    public static List<sObject> sObjectList;
    
    // current logged in user account detail
    public static string userAccountID;
    
    public static  List<Account> accupdateList;
    public static  List<Account> accountlist;
    
    
    @AuraEnabled public static Account getAccountDetail(){
        
        /*Contains logged in user Id*/ 
        String userId = UserInfo.getUserId();
        
        /*Contains logged in user type*/
        String userType = UserInfo.getUserType();
        
        /*Contains logged in user contactID*/
        ID userContactId;
        Account acc = new Account();
        
        // query user information
        for(User usr : [Select contactid,Contact.AccountId,Contact.Account.Name from User where Id =: userId]){
            if(usr.contactid != null){
                userContactId = usr.contactid;
                if(usr.Contact.AccountId != null){
                    userAccountID = usr.Contact.AccountID;
                }
            }
        }
        system.debug('-----userAccountID---'+userAccountID);
        if(String.isNotBlank(userAccountID)){
            
            accountlist =[Select ID,(select id, name,Account_Principle_User_Email__c from contacts),
                          (select id,name from Licenses__r),(Select id ,Unit__c,Building_Name__c,Floor__c,name from Operating_Locations__r),Name, 
                          Trade_Name__c, Trade_Name_AR_CL__c,Company_Type__c,RORP_License_No__c,Tax_Registration_Number_TRN__c,Registration_Number__c,
                          ROC_Status__c,Business_Activity__c,Place_of_Registration__c,Office__c,
                          Street__c,City__c,Country__c,Former_Trading_Name__c,Former_Trading_Name_Arabic__c,
                          Active_License__c,Entity_Name__c,PO_Box__c,Emirate__c,Website,Mobile_Number__c,
                          Principal_User__c,PSA_Deposit__c,PSA_Guarantee__c,Long_Term_Visit_Visa_Service__c,
                          Short_Term_Visit_Visa_Service__c
                          From Account
                          Where ID =: userAccountID] ;
            if(accountlist !=null && accountlist.size()>0)
            {
                acc=accountlist[0];
            }
            for(Account acccountObject : accountList)
            {                
                //Assiging the values just for binding purpose, NO DML is being done;
                acc.IDAMA_BlackPoints__c =acccountObject.Licenses__r.size();
            }          
        }
        System.debug('-------------accountlist-----------'+ accountlist);
        
        
        return acc;
    }
    
    @AuraEnabled public static String getLoggedInUserAccountName(){
        
        /*Contains logged in user Id*/ 
        String userId = UserInfo.getUserId();
        
        /*Contains logged in user type*/
        String userType = UserInfo.getUserType();
        
        /*Contains logged in user contactID*/
        ID userContactId;
        String accountName;
        
        // query user information
        for(User usr : [Select contactid,Contact.AccountId,Contact.Account.Name from User where Id =: userId]){
            if(usr.contactid != null){
                userContactId = usr.contactid;
                if(usr.Contact.AccountId != null){
                    accountName = usr.Contact.Account.Name;
                }
            }
        }
        return accountName;
    }
    
    public static void getLoggedInUserAccount(){
        
        /*Contains logged in user Id*/ 
        String userId = UserInfo.getUserId();
        
        /*Contains logged in user type*/
        String userType = UserInfo.getUserType();
        
        /*Contains logged in user contactID*/
        ID userContactId;
        
        // query user information
        for(User usr : [Select contactid,Contact.AccountId from User where Id =: userId]){
            if(usr.contactid != null){
                userContactId = usr.contactid;
                if(usr.Contact.AccountId != null){
                    userAccountID = usr.Contact.AccountId;
                }
            }
        }
        
    }
    /*
* Used in: DIFC_RequestedServicesList Lightning component
**/
    @AuraEnabled public static List<sObject> getRequestedServices(String reqWrapPram){
        
        getLoggedInUserAccount();
        sObjectList = new List<sObject>();
        // request detail from lightning component
        RequestWrapper reqWrap = (DIFC_RequestedServicesController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, DIFC_RequestedServicesController.RequestWrapper.class);
        system.debug('==reqWrap===='+reqWrap);
        sObjectList = getServices(userAccountID,reqWrap);
        return sObjectList;
    } 
    
    public static List<sObject> getServices(Id userAccountID,RequestWrapper reqWrap){
        
        system.debug('==userAccountID===='+userAccountID);
        /*******************query form for old service request object*************************/
        // get service request detail of that looged user account
        String strQuery = ' Select Id,Name,External_Status_Name__c,CreatedDate,SR_Template__c,SR_Template__r.Name From Service_Request__c WHERE Customer__c =: userAccountID';
        // get service request detail of that looged user account
        String newStrQuery = ' Select Id,SR_Number__c,HexaBPM__External_Status_Name__c,CreatedDate,HexaBPM__SR_Template__c,HexaBPM__SR_Template__r.Name From HexaBPM__Service_Request__c';
        newStrQuery += ' WHERE HexaBPM__Customer__c =: userAccountID';
        
        // sr number
        if(String.IsNotBlank(reqWrap.serviceNumber)){
            strQuery += ' AND Name like  \'%'+reqWrap.serviceNumber+'%\'';
            newStrQuery += ' AND Name like  \'%'+reqWrap.serviceNumber+'%\'';
        }
        
        // sr status
        if(String.IsNotBlank(reqWrap.serviceStatus) && !reqWrap.serviceStatus.equalsIgnorecase('In Progress')){
            strQuery += ' AND External_Status_Name__c =\''+ reqWrap.serviceStatus+'\'';
            newStrQuery += ' AND HexaBPM__External_Status_Name__c =\''+ reqWrap.serviceStatus+'\'';
        }else if(String.IsNotBlank(reqWrap.serviceStatus) && reqWrap.serviceStatus.equalsIgnorecase('In Progress')){
            strQuery += ' AND IsCancelled__c = false AND Is_Rejected__c = false AND isClosedStatus__c = false ';
            newStrQuery += ' AND HexaBPM__IsClosedStatus__c = false AND HexaBPM__Is_Rejected__c = false AND HexaBPM__IsCancelled__c = false ';
            
        }
        
        // sr request Type
        if(String.IsNotBlank(reqWrap.requestType) ){
            //strQuery += ' AND SR_Template__r.Menu__c =\''+ reqWrap.requestType+'\'';
            //newStrQuery += ' AND HexaBPM__SR_Template__r.HexaBPM__Menu__c =\''+ reqWrap.requestType+'\'';
        }
        strQuery += ' Limit '+(Limits.getLimitQueryRows()-Limits.getQueryRows()) ;
        system.debug('=====strQuery======'+strQuery);
        
        /*************************query form for new service request object*****************************************/
        
        newStrQuery += ' Limit '+(Limits.getLimitQueryRows()-Limits.getQueryRows()) ;
        system.debug('=====newStrQuery======'+newStrQuery);
        
        /********************************get list of object service request object******************************/    
        if(String.IsNotBlank(userAccountID)){
            sObjectList.addAll( (List<sObject>)database.query(strQuery));
            
            sObjectList.AddAll((List<sObject>)database.query(newStrQuery));
        }
        system.debug('=====sObjectList========'+sObjectList);
        
        return sObjectList;
    }
    
    
    /*
* Used in: DIFC_VATRegistrationForm Lightning component
* Description: will update account with the provided registration number, when user click on "Confirm" button
*/
    @AuraEnabled public static void updateAccountVatRegistrationNumber(String reqWrapPram){
        accupdateList = new List<Account>();
        
        getLoggedInUserAccount();
        // request detail from lightning component
        RequestWrapper reqWrap = (DIFC_RequestedServicesController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, DIFC_RequestedServicesController.RequestWrapper.class);
        system.debug('==reqWrap===='+reqWrap);
        
        system.debug('==update account===userAccountID==='+userAccountID);
        if(String.IsNotBlank(userAccountID)){
            // taxRegistrationNumber
            for(Account acc:[Select Id,Tax_Registration_Number_TRN__c,
                             VAT_Info_Updated__c,
                             VAT_Info_Updated_DateTime__c,
                             VAT_Option__c From Account Where Id =: userAccountID]){
                                 
                                 acc.Tax_Registration_Number_TRN__c = reqWrap.taxRegistrationNumber;
                                 acc.VAT_Info_Updated__c = true;
                                 acc.VAT_Info_Updated_DateTime__c = Date.today();
                                 acc.VAT_Option__c = 'Is Vat';
                                 accupdateList.add(acc);
                                 
                             }
            system.debug('=====accupdateList==='+accupdateList);
            if(!accupdateList.IsEmpty()){
                update accupdateList;
            }
        }
    }
    
    /*
* Used in: DIFC_HomeTab Lightning component
* Description: check if company has vat registration or not for displaying the registration section. This will check on load of page
**/   
    @AuraEnabled public static ResponseWrapper checkVATNumber(){
        DIFC_RequestedServicesController.ResponseWrapper  respWrap = new DIFC_RequestedServicesController.ResponseWrapper();
        getLoggedInUserAccount();
        respWrap.isVATNotRegistered = true;
        if(String.IsNotBlank(userAccountID)){
            for(Account acc:[Select Id,VAT_Option__c,Tax_Registration_Number_TRN__c,VAT_Info_Updated__c From Account Where Id =: userAccountID]){
                if((String.IsnotBlank(acc.VAT_Option__c) && acc.VAT_Option__c.equalsIgnorecase('Is Not Vat')) 
                   || (String.IsnotBlank(acc.VAT_Option__c) && acc.VAT_Option__c.equalsIgnorecase('Is Vat') 
                       && String.IsnotBlank(acc.Tax_Registration_Number_TRN__c))){
                           respWrap.isVATNotRegistered = false;
                           
                       }
            }
            
        }
        return respWrap;
    }
    
    /*
* Used in: DIFC_HomeTab Lightning component
* Description: will update the Vat option field to "IS Not VAT" if user click on "I AM NOT ELIGIBLE"
*/
    @AuraEnabled public static void updateAccountField(){
        accupdateList = new List<Account>();
        getLoggedInUserAccount();
        system.debug('==update account===userAccountID==='+userAccountID);
        if(String.IsNotBlank(userAccountID)){
            for(Account acc:[Select Id,VAT_Option__c From Account Where Id =: userAccountID]){
                acc.VAT_Option__c = 'Is Not Vat';
                accupdateList.add(acc);
            }
            system.debug('=====accupdateList==='+accupdateList);
            if(!accupdateList.IsEmpty()){
                update accupdateList;
            }
        }
    }
   
    /**********************************Wrapper classes*********************************************************/    
    public class ResponseWrapper {
        @AuraEnabled public Boolean isVATNotRegistered      {get;set;}
    }    
    
    public class RequestWrapper{
        @AuraEnabled public String serviceNumber            {get;set;}
        @AuraEnabled public String requestType              {get;set;}
        @AuraEnabled public String serviceStatus            {get;set;}
        @AuraEnabled public String taxRegistrationNumber    {get;set;}
    }
}