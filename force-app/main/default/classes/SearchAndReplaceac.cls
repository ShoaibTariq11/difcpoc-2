global class SearchAndReplaceac implements Database.Batchable < sObject > {

    global final String Query;


    global SearchAndReplaceac(String q) {

        Query = q;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List < Contact > scope) {
        for (Contact ObjContact: scope) {
            ObjContact.email.replace('@', '=@');
        }
        update scope;
    }

    global void finish(Database.BatchableContext BC) {}
}