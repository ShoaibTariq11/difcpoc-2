public class ROCDetailsOfRegisteredAgent_Cls{

    
      /* Start of Properties for Dynamic Flow */
    public string strPageId                {get;set;}
    public string strActionId              {get;set;}
    public map<string,string> mapParameters;
    public Service_Request__c objSR            {get;set;}
    public string strHiddenPageIds            {get;set;}
    public string strNavigatePageId            {get;set;}
    public Account accObj {get;set;}
    /* End of Properties for Dynamic Flow */
    
    
    string SRID;
     public string pageTitle               {get;set;}
    public string pageDescription             {get;set;}
    
    public Amendment__c objAmnd {get;set;}
    
    public string SelectedType{ get;set;}
    
     public List<SelectOption> TypeOptions {get;set;}
     
     Map<string,Account> mapOfAccount ;
    
    
    public ROCDetailsOfRegisteredAgent_Cls(){
        
         /* Start of Properties Initialization for Dynamic Flow */
        mapParameters = new map<string,string>();
        mapOfAccount = new map<string,Account>();
        if(apexpages.currentPage().getParameters()!=null)
            mapParameters = apexpages.currentPage().getParameters();
        SRId = mapParameters.get('Id');
        system.debug('mapParameters==>'+mapParameters);
        objSR = new Service_Request__c();
        objSR.Id = SRId;
        if(objSR.Id!=null){
             strPageId = mapParameters.get('PageId');
             if(strPageId!=null && strPageId!=''){
                for(Page__c page:[select id,Name,Page_Description__c from Page__c where Id=:strPageId]){
                    pageTitle = page.Name;
                    pageDescription = page.Page_Description__c;
                }
             }
             if(mapParameters!=null && mapParameters.get('FlowId')!=null){
                set<string> SetstrFields = Cls_Evaluate_Conditions.FetchObjectFields(mapParameters.get('FlowId'),'Service_Request__c');
                string strQuery = 'select Id';
                if(SetstrFields!=null && SetstrFields.size()>0){
                    for(String strFld:SetstrFields){
                        if(strFld.toLowerCase()!='id')
                            strQuery += ','+strFld.toLowerCase();
                    }
                }
                if(!strQuery.contains('customer__c'))
                    strQuery += ',customer__c';
                if(!strQuery.contains('record_type_name__c'))
                    strQuery += ',record_type_name__c';
                if(!strQuery.contains('address_changed__c'))
                    strQuery += ',address_changed__c';
                if(!strQuery.contains('legal_structures__c'))
                    strQuery += ',legal_structures__c';
                if(!strQuery.contains('external_status_name__c'))
                    strQuery += ',external_status_name__c';
                if(!strQuery.contains('change_address__c'))
                    strQuery += ',change_address__c';
                if(!strQuery.contains('finalizeamendmentflg__c'))
                    strQuery += ',finalizeamendmentflg__c';
                strQuery = strQuery+',Customer__r.Legal_Type_of_Entity__c,Customer__r.License_Activity__c,Customer__r.Registered_Address__c,Customer__r.Sys_Office__c,Customer__r.Building_Name__c,Customer__r.Street__c,Customer__r.PO_Box__c,Customer__r.City__c,Customer__r.Emirate__c,Customer__r.Country__c,Customer__r.Sector_Classification__c,customer__r.Company_Type__c from Service_Request__c where Id=:SRId'; //v1.6 added sector classifcation in soql
                system.debug('strQuery===>'+strQuery);
                for(Service_Request__c SR:database.query(strQuery)){
                    objSR = SR;
                }
             }
           
               strHiddenPageIds = PreparePageBlockUtil.getHiddenPageIds(mapParameters.get('FlowId'),objSR);
               GetAccount();
               GetAmendmend(objSR.ID);
               GetSelectedAccountsDetail();
        }
                
    }
    
      public Component.Apex.PageBlock getDyncPgMainPB(){ 
        PreparePageBlockUtil.FlowId = mapParameters.get('FlowId');
        PreparePageBlockUtil.PageId = mapParameters.get('PageId');
        PreparePageBlockUtil.objSR = objSR;
        PreparePageBlockUtil objPB = new PreparePageBlockUtil();
        return objPB.getDyncPgMainPB();
    }
    
    void GetAmendmend(ID aSRID){
        
        List<Amendment__c> lstAmnd = new List<Amendment__c>();
        
        lstAmnd = [Select ID,Title_new__c,Show_Declaration__c,Gender__c,Given_Name__c,Family_Name__c,Date_of_Birth__c,Place_of_Birth__c,Passport_No__c,
                    Passport_Expiry_Date__c,Nationality_list__c,Phone__c,Capacity__c,Apt_or_Villa_No__c,Building_Name__c,
                    Street__c,PO_Box__c,Shareholder_Company__c,Permanent_Native_City__c,Emirate_State__c,Post_Code__c,Permanent_Native_Country__c
                    from Amendment__c where ServiceRequest__c =: aSRID AND Relationship_Type__c = 'Registered Agent'];
        
        if(lstAmnd.size() == 1){
            objAmnd = lstAmnd[0];
            SelectedType = objAmnd.Shareholder_Company__c;
        }
        else{
                objAmnd = new Amendment__c();
        }
        
        
    }
    
    public void SaveAmnd(){
        
        try{
            
        
        objAmnd.ServiceRequest__c = objSR.ID;
        objAmnd.Relationship_Type__c = 'Registered Agent';
        objAmnd.Shareholder_Company__c = SelectedType;
        objAmnd.Status__c = 'Draft';
        objAmnd.Amendment_Type__c = 'Individual';
        
        if(mapOfAccount.containsKey(SelectedType)){
             objAmnd.Office_Phone_Number__c = mapOfAccount.get(SelectedType).phone;
              objAmnd.CL_Certificate_No__c = mapOfAccount.get(SelectedType).Registration_License_No__c;
               objAmnd.Address_of_Caterer__c = mapOfAccount.get(SelectedType).Registered_Address__c;
             
        }
       
        upsert objAmnd;
        }
        catch(Exception e){
                
                }
    }
    
    public void CancelAmnd(){
        
        objAmnd = new Amendment__c();
        
    }
    
     public pagereference goTopage(){
        if(strNavigatePageId!=null && strNavigatePageId!=''){
            updateSR();
            if(Apexpages.getMessages() != null && Apexpages.getMessages().size() > 0 && test.isRunningTest() == false)
                return null;
            PreparePageBlockUtil objSidebarRef = new PreparePageBlockUtil();
            PreparePageBlockUtil.strSideBarPageId = strNavigatePageId;
            PreparePageBlockUtil.objSR = objSR;
            return objSidebarRef.getSideBarReference();
        }
        return null;
    }
    public pagereference DynamicButtonAction(){
        system.debug('strActionId==>'+strActionId);
        
        //added by ravi to validate the Registered address
        //if(AddressType == 'Change Registered Address' || AddressType == 'Change any Operating Location'){
            Boolean isNext = false;
            for(Section_Detail__c objSec:[select id,Name,Component_Label__c,Navigation_Direction__c from Section_Detail__c where Id=:strActionId]){
                if(objSec.Navigation_Direction__c=='Forward')
                    isNext = true;
            }
            if(isNext){
                //added the logic to update Phone & PO Box in AOR , #185
                if(objAmnd.ID == null){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please save the record..!'));
                    return null;
                }
                updateSR();
                if(Apexpages.getMessages() != null && Apexpages.getMessages().size() > 0 && test.isRunningTest() == false)
                    return null;
            }
        //}
        
        PreparePageBlockUtil.FlowId = mapParameters.get('FlowId');
        PreparePageBlockUtil.PageId = mapParameters.get('PageId');
        PreparePageBlockUtil.objSR = objSR;
        PreparePageBlockUtil.ActionId = strActionId;
        
        PreparePageBlockUtil objPB = new PreparePageBlockUtil();
        pagereference pg = objPB.getButtonAction();
        system.debug('pg==>'+pg);
        return pg;
    }
    
    
    void updateSR(){
        
       /* objSR.Shareholder_Company__c= SelectedType;
        update objSR;*/
    }
    
      public Pagereference BackendSRUpdate(){
        updateSR(); 
        if(Apexpages.getMessages() != null && Apexpages.getMessages().size() > 0){
            return null;
        }
        Pagereference ref = new Pagereference('/'+objSR.Id);
        ref.setRedirect(true);
        return ref;
    }
    
        
    public void GetSelectedAccountsDetail(){
        
        if(mapOfAccount.containsKey(SelectedType)){
            accObj = mapOfAccount.get(SelectedType);
        }
        else{
            accObj  = new Account();
        }
        
    }
    
    void GetAccount(){
        
        
        TypeOptions = new List<SelectOption>();
        TypeOptions.add(new SelectOption('', '--none--'));
        mapOfAccount = new map<string,Account>();
        
        for(Account acc : [Select ID ,Name, Registered_Address__c,Active_License__c,Registration_License_No__c,
                            Phone,Active_License__r.Name from Account where License_Activity__c like '%318%' AND 
                             ROC_Status__c = 'Active']){
            TypeOptions.add(new SelectOption(acc.ID, acc.Name));
            mapOfAccount.put(acc.ID,acc);
         }
        
    }
    
    

}