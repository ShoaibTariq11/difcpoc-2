/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
public  with sharing class OB_CommunitiesLandingController {
    
    // Code we will invoke on page load.
    public PageReference forwardToStartPage() {
        return new PageReference('/s/ob-srindex');
    }
    
    public OB_CommunitiesLandingController() {}
}