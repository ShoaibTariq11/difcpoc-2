/*
    Author      : Durga Prasad
    Date        : 11-Dec-2019
    Description : Batch class to check whether the AOR is open for 3.months then re-trigger
    			  Security Step and In Principle approval
    ----------------------------------------------------------------------------------------
*/
public without sharing class OB_SecurityStepRetrigger_Batch  implements Database.Batchable<sObject>{
	public list<HexaBPM__Service_Request__c> start(Database.BatchableContext BC){

		date dateBefore=System.Today().addMonths(-3);

	
		list<HexaBPM__Service_Request__c> lstOpenAORSR = [Select Id,HexaBPM__Record_Type_Name__c,
		HexaBPM__Parent_SR__c from HexaBPM__Service_Request__c where 
		HexaBPM__IsClosedStatus__c=false and HexaBPM__IsCancelled__c=false 
		and HexaBPM__Is_Rejected__c=false 
		and HexaBPM__Internal_Status_Name__c!='Submitted' 
		and (HexaBPM__Record_Type_Name__c='AOR_Financial' OR HexaBPM__Record_Type_Name__c='AOR_NF_R')
		and createddate <=: dateBefore];
		return lstOpenAORSR;
	}
	public void execute(Database.BatchableContext BC, list<HexaBPM__Service_Request__c> ServiceRequests){
		if(ServiceRequests!=null && ServiceRequests.size()>0){
			/*
			for(HexaBPM__Service_Request__c){
				
			}
			*/
		}
	}
	public void finish(Database.BatchableContext BC) {
   	
   	}
}