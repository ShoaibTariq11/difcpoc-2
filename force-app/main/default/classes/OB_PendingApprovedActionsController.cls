public with sharing class OB_PendingApprovedActionsController {
	@AuraEnabled
	public static RespondWrap getStepData() {
		//declaration of wrapper
		RespondWrap respWrap = new RespondWrap();
		list<srStepRecWrap> srObjRecWrapList = new list<srStepRecWrap>();
		//string UserContactID = [select ContactId FROM User Where Id=:userInfo.getUserId() and ContactId!=null].ContactId;
		string UserAccID;
		for(User usr :[select Contact.accountID FROM User Where Id = :userInfo.getUserId() and ContactId != null]) {
			UserAccID = usr.Contact.accountID;
		}
		if(UserAccID != null) {
			for(HexaBPM__Service_Request__c srObj :[SELECT id, HexaBPM__SR_Template__r.name, RecordType.Name, (SELECT id, name, HexaBPM__Step_Status__c, HexaBPM__Start_Date__c, HexaBPM__Step_Template__r.Name, Owner.name FROM HexaBPM__Steps_SR__r where Is_Client_Step__c = true) FROM HexaBPM__Service_Request__c WHERE HexaBPM__Customer__c = :UserAccID]) {
				srStepRecWrap srObjWrapItem = new srStepRecWrap();
				srObjWrapItem.srObj = srObj;
				srObjWrapItem.srStepList = srObj.HexaBPM__Steps_SR__r;
				srObjRecWrapList.add(srObjWrapItem);
			}
		}
		respWrap.srObjRecWrapList = srObjRecWrapList;
		return respWrap;
	}
	public class RequestWrap {
		public RequestWrap() {
		}
	}
	public class RespondWrap {
		@AuraEnabled public list<srStepRecWrap> srObjRecWrapList { get; set; }
		public RespondWrap() {

		}
	}
	public class srStepRecWrap {
		@AuraEnabled public list<HexaBPM__Step__c> srStepList { get; set; }
		@AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }
		public srStepRecWrap() {
		}
	}
}