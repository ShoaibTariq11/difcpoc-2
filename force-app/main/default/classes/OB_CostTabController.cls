/**
 * Description : Controller for OB_CostTab component
 *
 * ****************************************************************************************
 * History :
 * [5.NOV.2019] Prateek Kadkol - Code Creation
 * [6.MAY.2020] Prateek Kadkol - Edited to user the agent helper class
 */

public without sharing class OB_CostTabController {
@AuraEnabled
public static RespondWrap getSrObjRec() {
    //declaration of wrapper
    RespondWrap respWrap = new RespondWrap();
    list<srObjRecWrap> srObjRecWrapList = new list<srObjRecWrap>();


    //string UserContactID = [select ContactId FROM User Where Id=:userInfo.getUserId() and ContactId!=null].ContactId;
    string UserAccID;
    
    for(User usrObj : OB_AgentEntityUtils.getUserById(userInfo.getUserId())) {
        if(usrObj.ContactId!=null && usrObj.contact.AccountId!=null) {
            UserAccID = usrObj.contact.AccountId;
        }
    }
    if(UserAccID != null) {
        for(HexaBPM__Service_Request__c srObj :[SELECT id, RecordType.Name, (SELECT id, HexaBPM__Product__r.name,
                                                                             HexaBPM__Product__r.ProductCode, HexaBPM__Status__c, Pricing_Line_Name__c, Total_Price_USD__c, name FROM HexaBPM__SR_Price_Items1__r), Name 
                                                                             FROM HexaBPM__Service_Request__c WHERE HexaBPM__Customer__c = :UserAccID
                                                                             AND HexaBPM__Is_Rejected__c= false and HexaBPM__IsCancelled__c= false
                                                                             ]) {

            list<HexaBPM__SR_Price_Item__c> priceitemList = new list<HexaBPM__SR_Price_Item__c>();
            for(HexaBPM__SR_Price_Item__c priceitemObj :srObj.HexaBPM__SR_Price_Items1__r) {

                if(priceitemObj.HexaBPM__Status__c == 'Added' && priceitemObj.HexaBPM__Product__r.ProductCode != 'OB Name Reservation') {
                    priceitemList.add(priceitemObj);
                }

            }
            if(priceitemList.size() > 0) {
                srObjRecWrap srObjWrapItem = new srObjRecWrap();
                srObjWrapItem.srObj = srObj;
                srObjWrapItem.srPriceItemList = priceitemList;
                srObjRecWrapList.add(srObjWrapItem);
            }

        }
    }
    respWrap.srObjRecWrapList = srObjRecWrapList;
    return respWrap;
}
public class RequestWrap {
public RequestWrap() {
}
}
public class RespondWrap {
@AuraEnabled public list<srObjRecWrap> srObjRecWrapList { get; set; }
public RespondWrap() {
}
}
public class srObjRecWrap {
@AuraEnabled public list<HexaBPM__SR_Price_Item__c> srPriceItemList { get; set; }
@AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }
public srObjRecWrap() {

}
}

}