/**
    *Author : Merul Shah
    *Description : Utilty for file upload on amedments.
    **/ 
    
    public without sharing  class OB_AmendmentfileUploadOCR
    {
        
        @AuraEnabled
        public static ResponseWrapper viewSRDocs(string reqWrapPram)
        {
            
             ResponseWrapper respWrap = new ResponseWrapper();
             RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);
            
             string srId = reqWrap.srId;
             string docMasterCode = reqWrap.docMasterCode;
             string amedId = reqWrap.amedId;
             
            //Preparing the SRDocs for the amendment
            map<string,HexaBPM__SR_Doc__c> mapSRDocs = new map<string,HexaBPM__SR_Doc__c>();
            if( !(String.isBlank( srId )) 
                        && !(String.isBlank(amedId)) 
                                && !(String.isBlank(docMasterCode))
               ) 
              
            {
                boolean isMultiStructure = False;
                string  multistructureSrID = '';
                for(HexaBPM__Service_Request__c srObj : [select id,	OB_Is_Multi_Structure_Application__c
                from HexaBPM__Service_Request__c where id =: srId AND Recordtype.developername = 'In_Principle' ]){
                    if (srObj.OB_Is_Multi_Structure_Application__c == 'Yes') {
                        isMultiStructure = true;
                    }
                }
                
                respWrap.debugger = isMultiStructure;
                if(!isMultiStructure ){
                    for(HexaBPM__SR_Doc__c srDoc : [
                                                        SELECT id,
                                                                File_Name__c,
                                                                HexaBPM__Document_Master__r.HexaBPM__Code__c 
                                                              FROM HexaBPM__SR_Doc__c
                                                         WHERE HexaBPM__Service_Request__c = :srId 
                                                           AND HexaBPM_Amendment__c =:amedId 
                                                           AND HexaBPM__Document_Master__r.HexaBPM__Code__c =:docMasterCode
                                                         LIMIT 1
                                                    ]
                       )
                    {
                        mapSRDocs.put(srDoc.HexaBPM__Document_Master__r.HexaBPM__Code__c,srDoc); 
                    }
                }else{
                    respWrap.debugger = 'in';
                    for(HexaBPM__SR_Doc__c srDoc : [
                                                        SELECT id,
                                                                File_Name__c,
                                                                HexaBPM__Document_Master__r.HexaBPM__Code__c 
                                                              FROM HexaBPM__SR_Doc__c
                                                         WHERE  HexaBPM_Amendment__c =:amedId 
                                                           AND HexaBPM__Document_Master__r.HexaBPM__Code__c =:docMasterCode
                                                         LIMIT 1
                                                    ]
                       )
                    {
                        respWrap.debugger = 'in SOQL';
                        mapSRDocs.put(srDoc.HexaBPM__Document_Master__r.HexaBPM__Code__c,srDoc); 
                    }
                }
                    
    
                    
                    respWrap.mapSRDocs = mapSRDocs;
            }
            
            return respWrap;
        }
    
        @AuraEnabled
        public static ResponseWrapper viewRelatedSRDocs(string reqWrapPram)
        {
            
             ResponseWrapper respWrap = new ResponseWrapper();
             RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);
            
             string srId = reqWrap.srId;
             string docMasterCode = reqWrap.docMasterCode;
             string amedId = reqWrap.amedId;
             
            //Preparing the SRDocs for the amendment
            map<string,HexaBPM__SR_Doc__c> mapSRDocs = new map<string,HexaBPM__SR_Doc__c>();
            if( !(String.isBlank( srId )) 
                        && !(String.isBlank(amedId)) 
                                && !(String.isBlank(docMasterCode))
               ) 
              
            {
    
                Map<String, Schema.SObjectType> sObjType  = Schema.getGlobalDescribe();
    
                Schema.SObjectType newAmend = sObjType.get('HexaBPM_Amendment__c');
                Schema.DescribeSObjectResult r = newAmend.getDescribe();
                Schema.SObjectType oldAmend = sObjType.get('Amendment__c');
                Schema.DescribeSObjectResult r2 = oldAmend.getDescribe();
    
                String NewAmendkeyPrefix = r.getKeyPrefix();
                String oldAmendkeyPrefix = r2.getKeyPrefix();
    
                String keyCode = amedId.subString(0,3);
    
                if(keyCode == NewAmendkeyPrefix){
                    //new amendment logic
                    for(HexaBPM__SR_Doc__c srDoc : [SELECT id,File_Name__c, HexaBPM__Document_Master__r.HexaBPM__Code__c ,HexaBPM__Doc_ID__c FROM HexaBPM__SR_Doc__c WHERE 
                            HexaBPM_Amendment__c =:amedId AND HexaBPM__Document_Master__r.HexaBPM__Code__c =:docMasterCode LIMIT 1
                    ]){
    
                    respWrap.relDoc = srDoc;
                    mapSRDocs.put(srDoc.HexaBPM__Document_Master__r.HexaBPM__Code__c,srDoc); 
                    }
                    respWrap.mapSRDocs = mapSRDocs;
    
                }else if(keyCode == oldAmendkeyPrefix){
    
                    try{
                    
                     //old amendment logic
    
                    string oldDocMasterCode = '';
    
                    for(  OB_Old_Sr_Doc_Code_Mapping__mdt metaDataObj : [SELECT id,Old_Document_Code__c from OB_Old_Sr_Doc_Code_Mapping__mdt WHERE Hexa_Document_Code__c =: docMasterCode  LIMIT 1]){
                        oldDocMasterCode = metaDataObj.Old_Document_Code__c;
                    }
    
                    if(Test.isRunningTest()){
                         oldDocMasterCode = docMasterCode;    
                    }
    
                    SR_Doc__c oldSrDoc = new SR_Doc__c();
                        if(oldDocMasterCode != ''){
                            for(  SR_Doc__c srDoc : [SELECT id from SR_Doc__c WHERE Amendment__c =:amedId AND Document_Master__r.Code__c = :oldDocMasterCode LIMIT 1]){
                                oldSrDoc = srDoc;
                                
                            }
                        }
                    
    
                    respWrap.debugger = oldSrDoc;
                        
                    if(oldSrDoc.id != null){
                        for(ContentDocumentLink cdl : [SELECT ContentDocumentId,ContentDocument.CreatedDate,LinkedEntityId,ContentDocument.Title FROM ContentDocumentLink where LinkedEntityId =: oldSrDoc.id] ){
                            respWrap.CDl = cdl.ContentDocumentId;
                            respWrap.fileName = cdl.ContentDocument.Title;
                        }
                        if(respWrap.CDl == null){
    
                            for(Attachment attIterator : [select Id, Name, ContentType, ParentId, Description, Body from Attachment WHERE ParentID = :oldSrDoc.id LIMIT 1] ){
                                ContentVersion objCntVersion = new ContentVersion();
                                objCntVersion.Title = attIterator.Name;
                                objCntVersion.PathOnClient = '/' + attIterator.Name;
                                objCntVersion.VersionData = (attIterator.Body == null ? Blob.valueOf('.') : attIterator.Body);
                                objCntVersion.Description = attIterator.Description;
                                objCntVersion.SharingPrivacy = 'N'; // Can be always public.
                                objCntVersion.FirstPublishLocationId = attIterator.ParentId; // Parent Id
                                //objCntVersion.OwnerId = attIterator.OwnerId;
                                // To avoid "Documents in a user's private library must always be owned by that user" error.
                                //objCntVersion.CreatedById = attIterator.OwnerId; 
                                //objCntVersion.CreatedDate = attIterator.CreatedDate;
                                //objCntVersion.FirstPublishLocationId = attIterator.OwnerId;
                                insert objCntVersion;
    
                                Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:objCntVersion.Id].ContentDocumentId;
    
                                ContentDocumentLink cDe = new ContentDocumentLink();
                                cDe.ContentDocumentId = conDoc;
                                cDe.LinkedEntityId = amedId; 
                                cDe.ShareType = 'v'; 
                                //cDe.Visibility = 'InternalUsers';
                                insert cDe;
    
                                respWrap.CDl = conDoc;
                                respWrap.fileName = attIterator.Name;
    
                            }
                        }
                    }
                     
                    }catch(DMLException e) {
                    
                       string DMLError = e.getdmlMessage(0) + '';
                       if(DMLError == null) {
                         DMLError = e.getMessage() + '';
                        }
                       respWrap.errorMessage = DMLError;
                    }
    
                    
    
                }
                   
    
            }
            
            return respWrap;
        }
            
        @AuraEnabled
        public static ResponseWrapper processOCR(String fileId) 
        {
            //OCR Wrapper
            OB_OCRHelper.RequestWrapper ocrReqWrap = new OB_OCRHelper.RequestWrapper();
            OB_OCRHelper.ResponseWrapper ocrRespWrap  = new OB_OCRHelper.ResponseWrapper();
            
            
            //Current class wrapper.
            //RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);
            ResponseWrapper respWrap = new ResponseWrapper();
            
            // preparing request.
            ocrReqWrap.fileId = fileId; 
            if(!Test.isRunningTest())
            {
                  ocrRespWrap = OB_OCRHelper.processOCR(ocrReqWrap);
                  respWrap.mapFieldApiObject  = ocrRespWrap.mapFieldApiObject;
            }
          
            
            return respWrap;
            
        }
        
        
         public class SectionItemWrapper {
             /*
            @AuraEnabled public String FieldApi { get; set; }
            @AuraEnabled public Object FieldValue { get; set; }
            @AuraEnabled public Object LookupValue { get; set; }
            @AuraEnabled public String FieldType { get; set; }
            @AuraEnabled public String ObjectApi { get; set; }
            @AuraEnabled public boolean bRenderSecDetail { get; set; }
            @AuraEnabled public string sectionItemId { get; set; }
            @AuraEnabled public HexaBPM__Section_Detail__c sectionDetailObj { get; set; }
            
            
            public SectionItemWrapper(){
             
            }
              */
        }
      
         public class RequestWrapper
        {
            @AuraEnabled public string flowId { get; set; }
            @AuraEnabled public string pageId {get;set;}
            @AuraEnabled public string srId { get; set; }
            @AuraEnabled public string docMasterCode{ get; set; }
            
            //@AuraEnabled public HexaBPM__Service_Request__c objRequest{get;set;}
            //@AuraEnabled public Map<String,String> objRequestMap { get; set; }
            //@AuraEnabled public string documentId {get;set;}
            //@AuraEnabled public string secObjID{get;set;}
            // amedId
            @AuraEnabled public String amedId {get;set;}
            
            public RequestWrapper()
            {
            }
        }
        
        
        public class ResponseWrapper
        {
          
          
            // For OCR
            @AuraEnabled public Map<String,Object> mapFieldApiObject {get;set;}
            
            // Need to remove in future
            @AuraEnabled public String pageActionName{get;set;}
             // Need to remove in future
            @AuraEnabled public List<String> passportData {get;set;}
            
            //For Load SRDoc
            @AuraEnabled public map<string,HexaBPM__SR_Doc__c> mapSRDocs  {get;set;}
    
            @AuraEnabled public HexaBPM__SR_Doc__c relDoc  {get;set;}
    
            @AuraEnabled public string CDl  {get;set;}
            @AuraEnabled public string fileName  {get;set;}
            @AuraEnabled public object debugger  {get;set;}
            @AuraEnabled public string errorMessage {get;set;}
                   
            public ResponseWrapper()
            {
               
            }
        }
    }