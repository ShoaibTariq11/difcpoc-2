/*
    Author      : Sai Kalyan
    Date        : 15-Mar-2020
    Description : validation helper for SR Trigger
  
    -----------------------------------------------------------------------
*/
public without sharing class ApplicationValidation_SRTrigger {
    
    public static void isValidServiceRequest(List<Service_Request__c> lstServiceRequest){
        string MainEntityId;
        boolean CheckValidation = false;
        for(User usr:[Select Id,ContactId,Contact.AccountId from user where Id=:userinfo.getuserid() and ContactId!=null]){
            MainEntityId = usr.Contact.AccountId;
            CheckValidation = true;
        }
        
        
        for(Service_Request__c sr:lstServiceRequest){
            if(CheckValidation && sr.Customer__c!=null && sr.Customer__c!= MainEntityId){
              //  trigger.new[0].addError('Please change the Customer.');
            }
        }
        

    }
}