global class SearchAndReplaceServiceRequest implements Database.Batchable < sObject > {

    global final String Query;


    global SearchAndReplaceServiceRequest(String q) {

        Query = q;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List < Service_Request__c > scope) {
        for (Service_Request__c Sr: scope) {
            Sr.Contact_Number_After_Office_Hours__c='+971569803616';
            Sr.Contact_Number_During_Office_Hours__c='+971569803616';
            Sr.Current_Registered_Phone__c='+971569803616';
            Sr.Foreign_Registered_Phone__c='+971569803616';
            Sr.Send_SMS_To_Mobile__c='+971569803616';
            Sr.Mobile_Number__c='+971569803616';
            Sr.Courier_Mobile_Number__c='+971569803616';
            Sr.Phone_No_Outside_UAE__c='+971569803616';
            Sr.Courier_Cell_Phone__c='+971569803616';
            Sr.Residence_Phone_No__c='+971569803616';
            Sr.Mobile_No_Previous_Sponsor__c='+971569803616';
            Sr.Sponsor_Mobile_No__c='+971569803616';
            Sr.Office_Telephone_Previous_Sponsor__c='+971569803616';
            Sr.Sponsor_Office_Telephone__c='+971569803616';
            Sr.P_O_Box_Previous_Sponsor__c='+971569803616';
            Sr.Residence_Telephone_Previous_Sponsor__c='+971569803616';
            Sr.Sponsor_Residence_Telephone__c='+971569803616';
            Sr.Work_Phone__c='+971569803616';
            Sr.Email__c =Sr.Email__c != NULL ? Sr.Email__c +'.invalid' : 'test@difc.ae.invalid';
            Sr.Email_Address__c  = Sr.Email_Address__c != NULL ? Sr.Email_Address__c +'.invalid' : 'test@difc.ae.invalid';
			SR.Additional_Email__c = Sr.Additional_Email__c != NULL ? SR.Additional_Email__c +'.invalid': 'test@difc.ae.invalid';
        }
        database.update(scope,false);
    }

    global void finish(Database.BatchableContext BC) {}
}