/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class LookAndFeelCls_Test {

    static testMethod void myUnitTest() {

        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Fit_Out_Units','Look_Feel_Approval_Request','Request_Contractor_Access','Fit_Out_Service_Request','Fit_Out_Induction_Request','Permit_to_Work','Permit_to_add_or_remove_equipment',
                                'Fit_Out_Contact','Revision_of_Fit_Out_Request','Update_Contact_Details','Data_Center_Requestor','NOC_Bloomberg','Individual_Tenant','Landlord','Tool','Lease_Application_Request')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        Business_Hours__c bh= new Business_Hours__c();
        BH.name='Fit-Out';
        BH.Business_Hours_Id__c='01m20000000BOf6';
        insert BH; 
        
        SR_Template__c testSrTemplate = new SR_Template__c();
    
        testSrTemplate.Name = 'Look and Feel Approval Request';
        testSrTemplate.Menutext__c = 'Look & Feel Approval Request';
        testSrTemplate.SR_RecordType_API_Name__c = 'Look_Feel_Approval_Request';
        testSrTemplate.SR_Group__c = 'Fit-Out & Events';
        testSrTemplate.Active__c = true;
        
        insert testSrTemplate;
        
        SR_Steps__c srstep = new SR_Steps__c();
        insert srstep;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Service_Request__c SR1= new Service_Request__c();
        SR1.RecordTypeId = mapRecordType.get('Look_Feel_Approval_Request');
        SR1.Customer__c = objAccount.Id;
        //SR.Service_Category__c = 'New';       
        insert SR1;
        Service_Request__c SR = new Service_Request__c();
        SR.RecordTypeId = mapRecordType.get('Fit_Out_Service_Request');
        SR.Customer__c = objAccount.Id;
        //SR.SR_Group__c='Fit-Out & Events';
        SR.Linked_SR__c = SR1.id;     
        SR.Type_of_Request__c = 'Tenant Authorized Representative';   
        
        insert SR;
	    Building__c building = Test_CC_FitOutCustomCode_DataFactory.getTestBuilding();
	    insert building;
	    List<Unit__c> unitsList = Test_CC_FitOutCustomCode_DataFactory.getTestUnits(building.id);
	    insert unitsList;
	    Lease__c leaseItem = Test_CC_FitOutCustomCode_DataFactory.getTestLease(objAccount.id);
	    insert leaseItem;
	    Lease_Partner__c leasepartner = Test_CC_FitOutCustomCode_DataFactory.getTestleasePartner(objAccount.Id, unitsList[0].id, leaseItem.id);
	    insert leasepartner;
	    list<Amendment__c> amendlist = new list<Amendment__c> ();
	    Amendment__c amnd1 = new Amendment__c (RecordTypeId=mapRecordType.get('Fit_Out_Units'),Lease_Partner__c=leasepartner.id,ServiceRequest__c=SR1.id);
	    amendlist.add(amnd1);
	    insert amendlist;
	    String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
	    String profileid =  System.Label.Contractor_Profile_Id;
	    //ApexPages.StandardController sc = new ApexPages.StandardController(SR1);
	    Contact con = new Contact(lastName='TestLookup');
	    insert con;
	    User userRec = new User(Contactid=con.id,Alias = 'standt', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId =profileid,TimeZoneSidKey='America/Los_Angeles',UserName=uniqueUserName);
	    //insert userRec;
	    //System.runAs(userRec) {
            ApexPages.StandardController sc = new ApexPages.StandardController(SR1);
        	LookAndFeelCls testAccPlan = new LookAndFeelCls(sc);
        	testAccPlan.setAddButtonVisibility(SR1);
        	testAccPlan.fetchAllLeasePartnersOFContractor();
        	testAccPlan.addAmendments();
	    //}
    }
}