/*****************************************************************************************************************************
    Author      :   Sai Kalyan
    Date        :   02-June-2020
    Description :   
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.1    02-June-2020   Sai        Intial Version
*****************************************************************************************************************************/

@isTest
public with sharing class CC_RegisteredAgentTest {
    
    static testmethod void test1(){
        
        
        Account objAccount = new Account();
            objAccount.Name = 'Test Account123';
            objAccount.Place_of_Registration__c ='India';
            objAccount.ROC_Status__c = 'Active';
            insert objAccount;
            
             Account objAccount1 = new Account();
            objAccount1.Name = 'Test Account123';
            objAccount1.Place_of_Registration__c ='India';
            objAccount1.ROC_Status__c = 'Active';
            objAccount1.Sys_Office__c = 'Test';
            objAccount1.Office__c = 'Test';
            objAccount1.Building_Name__c = 'Test';
            objAccount1.PO_Box__c = '44333';
            objAccount1.Street__c = '44333';
            objAccount1.City__c = '44333';
            objAccount1.Emirate__c = 'Dubai';
            insert objAccount1;
            
         String strRecType ;   
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Add_or_Remove_a_Registered_Agent' and sObjectType='Service_Request__c']){
            strRecType = rectyp.Id;
        }   
            
            Service_Request__c objSR = new Service_Request__c();
            objSR.Customer__c = objAccount.Id;
            objSR.Date_of_Declaration__c = Date.today();
            if(strRecType!='')
                objSR.RecordTypeId = strRecType;
            insert objSR;       
            
            Amendment__c eachAmendment = new Amendment__c();
            eachAmendment.Shareholder_Company__c = objAccount1.ID;
            eachAmendment.Status__c = 'Active';
            eachAmendment.ServiceRequest__c = objSR.Id;
            eachAmendment.Customer__c = objAccount.ID;
            INSERT eachAmendment;
              
            Step__c objStep = new Step__c();
            objStep.SR__c = objSR.Id;
            insert objStep;
            
        CC_RegisteredAgent.createRegisteredAgent(objStep.ID);
        CC_RegisteredAgent.UpdateOperatingLocation(objStep.ID);
    }
}