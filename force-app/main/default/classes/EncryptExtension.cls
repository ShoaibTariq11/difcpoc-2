/***************************************************************
* Class Name :EncryptExtension 
* Description : Encrypting the UBO details and sending with secure email template Assembla #5623
* Created Date:  14/08/2018 Assembla #5623  
* Modified Date: 14/04/2019  - replacing the Amendment object with UBO Data v1.1  
***************************************************************/
global without sharing class EncryptExtension {
    
    
    // Blob Key is used to encrypt or decrypt the records,If the calue changed,same wil be commmunicated to Security team
    public static String cryptoCode = EncryptCryptoCode.hideCryptoCode();
    public static Blob cryptoKey = Blob.valueOf(cryptoCode);
    
    //URL Encoding with KEY
    public static string generateURLEncryptValue(string encryData){
        string rtnResponse='';
        rtnResponse = generateEncryptValue(encryData);
        return rtnResponse;         
    }
    
    public static string generateEncryptValue(string encryData){
        string rtnResponse='';
        try{
            if(encryData!=null){
                //system.debug('--numberResponse---'+encryData);
                Blob data = Blob.valueOf(encryData);
                Blob encryptedData = Crypto.encryptWithManagedIV('AES128',cryptoKey,data);
                String b64Data = EncodingUtil.base64Encode(encryptedData);
                rtnResponse = b64Data;
               
            }else{
                rtnResponse = encryData;
            }

        }catch(Exception e){
            system.debug('---'+e);
        }
         return rtnResponse;         
    }
    public static string generateDecryptValue(string updateEncrypt){
        string decryptedText ='';
        Blob key = cryptoKey;
        try{
            
            if(updateEncrypt!=null){
                Blob encodedEncryptedBlob = EncodingUtil.base64Decode(updateEncrypt);
                Blob decryptedBlob = Crypto.decryptWithManagedIV('AES128', key, encodedEncryptedBlob);
                decryptedText = decryptedBlob.toString();
            }
        
         }catch(Exception e){
            system.debug('----'+e);
         }
        return decryptedText;
        
    }
    
    public static string generateURLDecryptValue(string updateEncrypt){
        string decryptedText='';
        Blob key = cryptoKey;
        try{
            
            if(updateEncrypt!=null && updateEncrypt!=''){
               
                Blob encodedEncryptedBlob = EncodingUtil.base64Decode(updateEncrypt);
                
                Blob decryptedBlob = Crypto.decryptWithManagedIV('AES128', key, encodedEncryptedBlob);
                
                decryptedText = decryptedBlob.toString();
                system.debug('----'+decryptedText);
               decryptedText = EncodingUtil.urlDecode(decryptedText, 'UTF-8');
            }
        
         }catch(Exception e){
            system.debug('----'+e);
         }
       
        return decryptedText;
        
    }  
    /* Method used to execute the encryption from SR verification step */
    
    webservice static void SendForEncryption(Step__c stp){
        //if(stp.SR__r.Legal_Structures__c =='FRC' || stp.SR__r.Legal_Structures__c =='RLLP' || stp.SR__r.Legal_Structures__c =='RP' || stp.SR__r.Legal_Structures__c =='RLP'){
            //executeEncyrptionLogic(stp.id);Disabling v1.1
            executeUBODATAEncyrptionLogic(stp.id);
        //}   
    }
    
    /* Encrypt the Amendment recors to Galaxkey */
    
   
    Public static void executeEncyrptionLogic(id stepID){
        List<step__c> stp = [select id,SR__c,sr__r.name,sr__r.Customer__c from step__c where id=: stepID];       
        if(stp.size()>0){
            string SRID = stp[0].SR__c;
            system.debug('------'+SRID);
            List<amendment__c> amendmentDetails = new List<amendment__c>();
            List<amendment__c> updateamendment = new List<amendment__c>();
            set<string> amendSetId = new set<string>();
            List<UBO_Secure__c> insertUBO = new List<UBO_Secure__c>();
        
            //SavePoint sp = database.setSavepoint();
            
        
            amendmentDetails = [select id,Registration_No__c,BC_UBO_Type__c,Name_of_the_regulator__c,Director_Appointed_Date__c ,Related_Amendment_Populated__c,Shareholder_Name__c,name,IsUBOSecured__c,Title_new__c,Gender__c,Given_Name__c,Family_Name__c,Date_of_Birth__c,Place_of_Birth__c,Passport_No__c,Passport_Expiry_Date__c,Nationality_list__c,Email_Address__c,Name_of_Body_Corporate_Shareholder__c,Registration_Date__c,Place_of_Registration__c,Phone__c,Apt_or_Villa_No__c,
            Building_Name__c,Street__c,PO_Box__c,Permanent_Native_City__c,Permanent_Native_Country__c ,Emirate_State__c,Post_Code__c,Phone_No__c,Person_Email__c,ServiceRequest__r.Foreign_Registration_Name__c,ServiceRequest__r.Legal_Structures__c,Company_Name__c,Customer__r.name,Customer__r.Company_Type__c,Relationship_Type__c,ServiceRequest__c,Amendment_Type__c,Shareholder_Company__c,Shareholder_Company__r.Name,Name_of_incorporating_Shareholder__c,Shareholder_enjoy_direct_benefits__c,Mobile__c from Amendment__c  where ServiceRequest__c =:SRID and IsUBOSecured__c=false];
        
            for(amendment__c itr:amendmentDetails){
                if(Label.EncryptRecordTypes.contains(itr.Relationship_Type__c)){
                    amendSetId.add(itr.Id);
                }
            }
        
        if(amendSetId.size()>0){
            List<UBO_Secure__c> uboList = [select id,Amendment_Type__c,Registration_No__c,BC_UBO_Type__c,Name_of_the_regulator__c,Company_Name__c,Apt_or_Villa_No__c,Date_of_Birth__c,DoestheShareholderEnjoyBenefit__c,Building_Name__c,Amendment__c,EmailAddress__c,Emirate_State__c,FamilyName__c,Gender__c,GivenName__c,MobileNumber__c,Name_of_Body_Corporate_Shareholder__c,Nationality__c,PassportExpiryDate__c,PassportNo__c,Permanent_Native_City__c,Person_Email__c,ServiceRequest__c,PhoneNo__c,PhoneNumber__c,Place_of_Birth__c,Place_of_Registration__c,PO_Box__c,Post_Code__c,Registration_Date__c,RelatedAmendment__c,Shareholder_Name__c,Street_Address__c,Title_new__c from UBO_Secure__c where Amendment__c IN:amendSetId];
            if(uboList.size()<1){
                for(Amendment__c amnd :amendmentDetails){
                    if(Label.EncryptRecordTypes.contains(amnd.Relationship_Type__c)){    
                        UBO_Secure__c ubo = new UBO_Secure__c();
                        ubo.Amendment__c = amnd.Id;
                        ubo.Apt_or_Villa_No__c = generateEncryptValue(amnd.Apt_or_Villa_No__c);
                        ubo.Building_Name__c = generateEncryptValue(amnd.Building_Name__c);
                        //ubo.Current_Name__c = generateEncryptValue(amnd.);
                        ubo.Date_of_Birth__c = generateEncryptValue(string.valueof(amnd.Date_of_Birth__c));
                        ubo.DoestheShareholderEnjoyBenefit__c = generateEncryptValue(string.valueof(amnd.Shareholder_enjoy_direct_benefits__c));
                        ubo.EmailAddress__c = generateEncryptValue(amnd.Email_Address__c);
                        ubo.Emirate_State__c = generateEncryptValue(amnd.Emirate_State__c);
                        ubo.FamilyName__c = generateEncryptValue(amnd.Family_Name__c);
                        ubo.Gender__c = generateEncryptValue(amnd.Gender__c);
                        ubo.GivenName__c = generateEncryptValue(amnd.Given_Name__c);
                        ubo.MobileNumber__c = generateEncryptValue(amnd.Mobile__c);
                        ubo.Name_of_Body_Corporate_Shareholder__c = amnd.Name_of_Body_Corporate_Shareholder__c;//generateEncryptValue(amnd.Name_of_Body_Corporate_Shareholder__c);
                        ubo.Nationality__c = generateEncryptValue(amnd.Nationality_list__c);
                        ubo.PassportExpiryDate__c = generateEncryptValue(string.valueof(amnd.Passport_Expiry_Date__c));
                        ubo.PassportNo__c = generateEncryptValue(amnd.Passport_No__c);
                        ubo.Permanent_Native_City__c = generateEncryptValue(amnd.Permanent_Native_City__c);
                        system.debug('---email--'+generateEncryptValue(amnd.Person_Email__c));
                        ubo.Person_Email__c = generateEncryptValue(amnd.Person_Email__c);
                        ubo.PhoneNo__c = generateEncryptValue(amnd.Phone__c);
                        system.debug('---Phone--'+generateEncryptValue(amnd.Phone__c));
                        ubo.PhoneNumber__c = generateEncryptValue(amnd.Phone_No__c);
                        ubo.Place_of_Birth__c = generateEncryptValue(amnd.Place_of_Birth__c);
                        ubo.Place_of_Registration__c = generateEncryptValue(amnd.Place_of_Registration__c);
                        ubo.PO_Box__c = generateEncryptValue(amnd.PO_Box__c);
                        ubo.Post_Code__c = generateEncryptValue(amnd.Post_Code__c);
                        ubo.Registration_Date__c = generateEncryptValue(string.valueof(amnd.Registration_Date__c));
                        ubo.RelatedAmendment__c = generateEncryptValue(string.valueof(amnd.Related_Amendment_Populated__c));
                        ubo.Shareholder_Name__c = generateEncryptValue(amnd.Shareholder_Name__c);
                        ubo.Street_Address__c = generateEncryptValue(amnd.Street__c);
                        ubo.Title_new__c = generateEncryptValue(amnd.Title_new__c);
                        ubo.Name_of_incorporating_Shareholder__c = generateEncryptValue(amnd.Name_of_incorporating_Shareholder__c);
                        ubo.Director_Appointed_Date__c= generateEncryptValue(string.valueof(amnd.Director_Appointed_Date__c));
                        ubo.Company_Name__c = amnd.Company_Name__c;//generateEncryptValue(amnd.Company_Name__c);
                        ubo.BC_UBO_Type__c= generateEncryptValue(amnd.BC_UBO_Type__c);
                        ubo.Name_of_the_regulator__c = generateEncryptValue(amnd.Name_of_the_regulator__c);
                        ubo.Permanent_Native_Country__c= generateEncryptValue(amnd.Permanent_Native_Country__c);
                        ubo.Registration_No__c = amnd.Registration_No__c;
                        ubo.Amendment_Type__c = amnd.Amendment_Type__c;
                        
                        insertUBO.add(ubo);
                        if(Label.EncryptAmendmentRecords == 'TRUE'){
                            Amendment__c upAMD = new Amendment__c (id=amnd.Id,IsUBOSecured__c = true,Apt_or_Villa_No__c=null,Building_Name__c=null,Date_of_Birth__c=null,Company_Name__c=null,Shareholder_enjoy_direct_benefits__c=false,Email_Address__c=null,Emirate_State__c=null,Family_Name__c=null,Gender__c=null,Given_Name__c=null,Mobile__c=null,Name_of_Body_Corporate_Shareholder__c=null,Nationality_list__c=null,Passport_Expiry_Date__c=null,Passport_No__c=null,Permanent_Native_City__c=null,Person_Email__c=null,Phone__c=null,Place_of_Birth__c=null,Place_of_Registration__c=null,PO_Box__c=null,Post_Code__c=null,Registration_Date__c=null,Street__c=null,Title_new__c=null,Name_of_incorporating_Shareholder__c=null,Director_Appointed_Date__c=null,BC_UBO_Type__c=null,Name_of_the_regulator__c=null,Permanent_Native_Country__c=null);
                            updateamendment.add(upAMD);
                        }
                    }
                }
            }
            if(insertUBO.size()>0){
            Boolean isAmdupdate = false;
                Database.SaveResult[] srList = Database.insert(insertUBO,true);
                for(Database.SaveResult sr : srList) {
                   if (sr.isSuccess()){
                         isAmdupdate = true;
                   }
                } 
                if(isAmdupdate){
                    if(updateamendment.size()>0 ){//&& label.UBOupdateBlank=='TRUE')
                    system.debug('-------'+updateamendment.size());
                    //update UBO Amendement value as blank
                    Database.SaveResult[] updateList = Database.update(updateamendment,true); 
                    }
                }
            }
        }
        
        }
        
    }
    
   /* Encrypt the UBO DATA recors to Galaxkey */
    //Method used to replace the amendment object to UBO Date v1.1 
    
    Public static void executeUBODATAEncyrptionLogic(id stepID){
        List<step__c> stp = [select id,SR__c,sr__r.name,sr__r.Customer__c from step__c where id=: stepID];       
        string SRID = stp[0].SR__c;
        system.debug('------'+SRID);
        List<UBO_Data__c> UBODataDetails = new List<UBO_Data__c>();
        List<UBO_Data__c> updateUBOData = new List<UBO_Data__c>();
        set<string> UBODataSetId = new set<string>();
        List<UBO_Secure__c> insertUBO = new List<UBO_Secure__c>();
        
        //SavePoint sp = database.setSavepoint();
        
        
        UBODataDetails = [select Nationality_New__c,Date_of_Cessation__c,Status__c,Passport_Number_New__c,Email_New__c,Last_Name_New__c,First_Name_New__c,RecordType.DeveloperName,Client_Entity_Name__c,Type__c,id,Date_of_Birth__c,Gender__c,Passport_Expiry_Date__c,Passport_Number__c,Email__c,Place_of_Birth__c,Title__c,Date_Of_Becoming_a_UBO__c,First_Name__c,Last_Name__c,Passport_Data_of_Issuance__c,Entity_Name__c,Date_Of_Registration__c,Name_of_Regulator_Exchange_Govt__c,Select_Regulator_Exchange_Govt__c,Reason_for_exemption__c,Entity_Type__c,Phone_Number__c,Apartment_or_Villa_Number__c,Emirate_State_Province__c,Nationality__c,Building_Name__c,Street_Address__c,City_Town__c,Country__c,PO_Box__c,Post_Code__c,Service_Request__c from UBO_Data__c where Service_Request__c =:SRID and IsUBOSecured__c=false];
        
        
        for(UBO_Data__c itr:UBODataDetails){
            //if(itr.Relationship_Type__c=='UBO'){
                UBODataSetId.add(itr.Id);
            //}
        }
        
        if(UBODataSetId.size()>0){
            List<UBO_Secure__c> uboList = [select Date_of_Cessation__c,UBO_Status__c,id,Client_Entity_Name__c,Amendment_Type__c,Registration_No__c,BC_UBO_Type__c,Name_of_the_regulator__c,Company_Name__c,Apt_or_Villa_No__c,Date_of_Birth__c,DoestheShareholderEnjoyBenefit__c,Building_Name__c,UBOData__c,EmailAddress__c,Emirate_State__c,FamilyName__c,Gender__c,GivenName__c,MobileNumber__c,Name_of_Body_Corporate_Shareholder__c,Nationality__c,PassportExpiryDate__c,PassportNo__c,Permanent_Native_City__c,Permanent_Native_Country__c,Person_Email__c,ServiceRequest__c,PhoneNo__c,PhoneNumber__c,Place_of_Birth__c,Place_of_Registration__c,PO_Box__c,Post_Code__c,Registration_Date__c,RelatedAmendment__c,Shareholder_Name__c,Street_Address__c,Title_new__c,DateofBecomingUBO__c,PassportDateofIssuance__c from UBO_Secure__c where UBOData__c IN:UBODataSetId];
            if(uboList.size()<1){
                for(UBO_Data__c UBOData :UBODataDetails)
                {
                    if(UBOData.RecordType.DeveloperName.equals('Individual')){
                        UBO_Secure__c UBOSecure = new UBO_Secure__c();
                        
                        //Reference fields
                        UBOSecure.UBOData__c = UBOData.Id;
                        
                        //Individual Fields
                        UBOSecure.Date_of_Birth__c = generateEncryptValue(string.valueof(UBOData.Date_of_Birth__c));
                        UBOSecure.Gender__c = generateEncryptValue(UBOData.Gender__c);
                        UBOSecure.PassportExpiryDate__c = generateEncryptValue(string.valueof(UBOData.Passport_Expiry_Date__c));
                        UBOSecure.Place_of_Birth__c = generateEncryptValue(UBOData.Place_of_Birth__c);
                        UBOSecure.Title_new__c = generateEncryptValue(UBOData.Title__c);
                        UBOSecure.DateofBecomingUBO__c = generateEncryptValue(string.valueof(UBOData.Date_Of_Becoming_a_UBO__c));
                        UBOSecure.PassportDateofIssuance__c = generateEncryptValue(string.valueof(UBOData.Passport_Data_of_Issuance__c));
                        
                        //First Name 
                        //UBOSecure.Old_Given_Name__c= generateEncryptValue(UBOData.First_Name__c);
                        //UBOSecure.GivenName__c = generateEncryptValue(UBOData.First_Name_New__c);
                        System.debug('UBOData.First_Name_New__c==>'+UBOData.First_Name_New__c);
                        System.debug('UBOData.First_Name__c==>'+UBOData.First_Name__c);
                        
                        if(!string.isBlank(UBOData.First_Name_New__c))
                        {
                            UBOSecure.GivenName__c = generateEncryptValue(UBOData.First_Name_New__c);
                            UBOSecure.Old_Given_Name__c= generateEncryptValue(UBOData.First_Name__c);
                        }
                        else
                        {
                            UBOSecure.GivenName__c= generateEncryptValue(UBOData.First_Name__c);
                            //UBOSecure.Old_Given_Name__c='';
                        }
                        
                        
                        //Last name 
                        //UBOSecure.Old_Family_Name__c= generateEncryptValue(UBOData.Last_Name__c);
                        //UBOSecure.FamilyName__c = generateEncryptValue(UBOData.Last_Name_New__c);
                        
                        if(!string.isBlank(UBOData.Last_Name_New__c))
                        {
                            UBOSecure.FamilyName__c = generateEncryptValue(UBOData.Last_Name_New__c);
                            UBOSecure.Old_Family_Name__c= generateEncryptValue(UBOData.Last_Name__c);
                        }
                        else
                        {
                            UBOSecure.FamilyName__c= generateEncryptValue(UBOData.Last_Name__c);
                            //UBOSecure.Old_Family_Name__c='';
                        }
                        //System.debug(' UBOSecure.GivenName__c==>'+ UBOSecure.GivenName__c);
                        
                        //Passport number 
                        //UBOSecure.Old_PassportNo__c= generateEncryptValue(UBOData.Passport_Number__c);
                        //UBOSecure.PassportNo__c = generateEncryptValue(UBOData.Passport_Number_New__c);
                        
                        if(!string.isBlank(UBOData.Passport_Number_New__c))
                        {
                            UBOSecure.PassportNo__c = generateEncryptValue(UBOData.Passport_Number_New__c);
                            UBOSecure.Old_PassportNo__c= generateEncryptValue(UBOData.Passport_Number__c);
                        }
                        else
                        {
                            UBOSecure.PassportNo__c = generateEncryptValue(UBOData.Passport_Number__c);
                            //UBOSecure.Old_PassportNo__c= '';
                        }
                        
                    
                        //Nationality
                        //UBOSecure.Nationality_New__c= generateEncryptValue(UBOData.Nationality_New__c);
                        // UBOSecure.Nationality__c = generateEncryptValue(UBOData.Nationality__c);
                        
                        if(!string.isBlank(UBOData.Nationality_New__c))
                        {
                            
                            UBOSecure.Nationality__c= generateEncryptValue(UBOData.Nationality_New__c);
                            UBOSecure.Old_Nationality__c= generateEncryptValue(UBOData.Nationality__c);
                            
                        }
                        else
                        {
                            UBOSecure.Nationality__c = generateEncryptValue(UBOData.Nationality__c);
                            //UBOSecure.Old_PassportNo__c= '';
                        }
                        
                        //UBOSecure.Person_Email__c = generateEncryptValue(UBOData.Email_New__c);
                          //  UBOSecure.Old_Person_Email__c= generateEncryptValue(UBOData.Email__c);
                          
                          if(!string.isBlank(UBOData.Email_New__c))
                        {
                            UBOSecure.Person_Email__c = generateEncryptValue(UBOData.Email_New__c);
                            UBOSecure.Old_Person_Email__c= generateEncryptValue(UBOData.Email__c);
                        }
                        else
                        {
                            UBOSecure.Person_Email__c = generateEncryptValue(UBOData.Email__c);
                            //UBOSecure.Old_PassportNo__c= '';
                        }
                        
                        
                        
                        
                        
                        //if(UBOData.Status__c=='New' || UBOData.Status__c=='Remove'){
                            
                            
                            
                            
                            
                            UBOSecure.Date_of_Cessation__c= generateEncryptValue(string.valueof(UBOData.Date_of_Cessation__c));
                           
                        //}
                        //if(UBOData.Status__c=='Update'){
                            
                            
                           
                            
                            
                       // }
                        
                        //Body Corporate fields
                       // UBOSecure.Company_Name__c = generateEncryptValue(string.valueof(UBOData.Entity_Name__c));
                        UBOSecure.Company_Name__c = UBOData.Entity_Name__c;
                        UBOSecure.Registration_Date__c = generateEncryptValue(string.valueof(UBOData.Date_Of_Registration__c));
                        UBOSecure.Name_of_the_regulator__c = generateEncryptValue(UBOData.Name_of_Regulator_Exchange_Govt__c);
                        UBOSecure.Place_of_Registration__c = generateEncryptValue(UBOData.Select_Regulator_Exchange_Govt__c);
                        UBOSecure.Exempted_Entity__c = generateEncryptValue(UBOData.Reason_for_exemption__c);
                        
                        // Common address fields for both Individual and Body Corporate
                        UBOSecure.PhoneNumber__c = generateEncryptValue(UBOData.Phone_Number__c);
                        UBOSecure.Apt_or_Villa_No__c = generateEncryptValue(UBOdata.Apartment_or_Villa_Number__c);
                        UBOSecure.Emirate_State__c = generateEncryptValue(UBOData.Emirate_State_Province__c);
                        UBOSecure.Building_Name__c = generateEncryptValue(UBOData.Building_Name__c);
                        UBOSecure.Street_Address__c = generateEncryptValue(UBOData.Street_Address__c);
                        UBOSecure.Permanent_Native_City__c = generateEncryptValue(UBOData.City_Town__c);
                        UBOSecure.Permanent_Native_Country__c  = generateEncryptValue(UBOData.Country__c);
                        UBOSecure.PO_Box__c = generateEncryptValue(UBOData.PO_Box__c);
                        UBOSecure.Post_Code__c = generateEncryptValue(UBOData.Post_Code__c);
                        UBOSecure.Type_of_ownership__c = generateEncryptValue(UBOData.Entity_Type__c);
                        UBOSecure.UBO_Direct_Indirect__c = UBOData.Type__c;//generateEncryptValue(UBOData.Type__c);
                        //UBOSecure.Client_Entity_Name__c = generateEncryptValue(UBOData.Client_Entity_Name__c);
                        UBOSecure.Client_Entity_Name__c = UBOData.Client_Entity_Name__c;
                        
                        insertUBO.add(UBOSecure);
                        
                        If(Label.EncryptUBODataRecords=='TRUE'){
                            UBO_Data__c upAMD = new UBO_Data__c(id=UBOData.Id,IsUBOSecured__c = true,Date_of_Birth__c = null,Gender__c = null,Passport_Expiry_Date__c = null,Passport_Number__c = null,Email__c = null,Place_of_Birth__c = null,Title__c = null,Date_Of_Becoming_a_UBO__c = null,First_Name__c = null,Last_Name__c = null,Passport_Data_of_Issuance__c = null,Entity_Name__c = null,Date_Of_Registration__c = null,Name_of_Regulator_Exchange_Govt__c = null,Select_Regulator_Exchange_Govt__c = null,Reason_for_exemption__c = null,Entity_Type__c = null,Phone_Number__c = null,Apartment_or_Villa_Number__c = null,Emirate_State_Province__c = null,Nationality__c = null,Building_Name__c = null,Street_Address__c = null,City_Town__c = null,Country__c = null,PO_Box__c = null,Post_Code__c = null);
                            updateUBOData.add(upAMD);
                        }
                }
                
                }
            }
            if(insertUBO.size()>0){
            system.debug('---updateUBOData---'+updateUBOData.size());
            Boolean isAmdupdate = false;
                Database.SaveResult[] srList = Database.insert(insertUBO,true);
                Database.SaveResult[] updateList = Database.update(updateUBOData,true); 
            }
        }
        
    } 
    
   
}