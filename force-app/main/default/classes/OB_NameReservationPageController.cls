public with sharing class OB_NameReservationPageController {
@AuraEnabled
public static RespondWrap getCompanyNameInfo(String requestWrapParam)  {

	//declaration of wrapper
	RequestWrap reqWrap = new RequestWrap();
	RespondWrap respWrap =  new RespondWrap();

	//deseriliaze.
	reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
    
    fieldNameAndLableWrap fieldToDisp = new fieldNameAndLableWrap();

	/*map<string, string> companyNameFieldstoDisplay = new map<string, string> {
		'Name' => 'Company Name',
		'Arabic_Entity_Name__c' => 'Company Name Arabic',
		'Trading_Name__c' => 'Trading Name',
		'Arabic_Trading_Name__c' => 'Trading Name Arabic',
		'Ends_With__c' => 'Ends With',
		'Arabic_Ends_With__c' => 'Ends With Arabic'
	};

	for(Company_Name__c relNameObj : [SELECT id, Name,Arabic_Entity_Name__c,Arabic_Trading_Name__c,Arabic_Ends_With__c,Ends_With__c,
	                                  Trading_Name__c from Company_Name__c WHERE Application__c =: reqWrap.srId LIMIT 1]) {
		respWrap.reservedName = relNameObj;
	}

	for(string field :companyNameFieldstoDisplay.keySet()) {
		if(respWrap.reservedName.get(field) != null) {
			fieldNameAndLableWrap fieldToDisp = new fieldNameAndLableWrap();
			fieldToDisp.fieldAPIName = field;
			fieldToDisp.FieldLable = companyNameFieldstoDisplay.get(field);
			fieldToDisp.fieldValue = respWrap.reservedName.get(field);
			respWrap.fieldsToDisplay.add(fieldToDisp);
		}
	}*/

	return respWrap;
}

// ------------ Wrapper List ----------- //

public class RequestWrap {

@AuraEnabled public String srId;

public RequestWrap(){
}
}

public class RespondWrap {

@AuraEnabled public Company_Name__c reservedName;
@AuraEnabled public list<fieldNameAndLableWrap> fieldsToDisplay = new list<fieldNameAndLableWrap>();                                                                                                                                                                                                                                          public RespondWrap(){
}
}

public class fieldNameAndLableWrap {
@AuraEnabled public string fieldAPIName { get; set; }
@AuraEnabled public string fieldLable { get; set; }
@AuraEnabled public object fieldValue { get; set; }

public fieldNameAndLableWrap() {
	fieldAPIName = '';
	fieldLable = '';
}
}
}