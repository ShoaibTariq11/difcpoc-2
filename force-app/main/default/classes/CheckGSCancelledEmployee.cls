/*************************************************************************************************
 *  Name        :  
 *  Author      : Danish Farooq
 *  Date        : 05-06-2018    
 *  Purpose     : Batch class to check cancelled employee
 *  TestClass   : CheckGSCancelledEmployee_Test
**************************************************************************************************/

Global without sharing class CheckGSCancelledEmployee implements Database.Batchable<sobject>{

    
   
    FINAL string STATUS = 'Process Completed';
    string SrQueryString = '';
    Set<string> setOfRec = new Set<string>();
    Date dtToday;
    
    Global CheckGSCancelledEmployee()
      {
        
        
        setOfRec.add('DIFC_Sponsorship_Visa_Cancellation');
        setOfRec.add('Non_DIFC_Sponsorship_Visa_Renewal');
        setOfRec.add('Entry_Permit_Renewal');
        setOfRec.add('DIFC_Sponsorship_Visa_Renewal');
        dtToday = Date.Today();
        
        SrQueryString = 'Select ID ,Customer__c, Contact__c,Contact__r.FirstName,Contact__r.LastName,Contact__r.Email, Contact__r.Phone, RecordType.DeveloperName, '  ;
        SrQueryString += 'Avail_Courier_Services__c,Use_Registered_Address__c,Consignee_FName__c,Consignee_LName__c,  ';
        SrQueryString += 'Courier_Mobile_Number__c, Courier_Cell_Phone__c, Apt_or_Villa_No__c,Email__c FROM ';
        SrQueryString += 'Service_Request__c where RecordType.DeveloperName IN :setOfRec ';
        SrQueryString += 'AND Internal_Status_Name__c = :STATUS ';
        SrQueryString += 'AND Completed_Date_Time__c >=: dtToday';
      
       
    
      }
      
    Global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(SrQueryString);      
    
    }
    
    Global Void execute(Database.BatchableContext BC,list<Service_Request__c > lstSR ){
      
        List<Service_Request__c> lstCarParkingSR = new List<Service_Request__c>();
        SR_Template__c srTemplate = [Select ID, Estimated_Hours__c from SR_Template__c where Name = 'Car Parking Process'];
        ID SRSubmit = [select id,name from SR_Status__c where Code__C  = 'Submitted'].id;
        Set<ID> setContact = new Set<ID>();
        Set<ID> setCustomer = new Set<ID>(); 
        Set<ID> setDIFCCustomer = new Set<ID>();
        
        for(Service_Request__c iSR : lstSR){
             
             setCustomer.add(iSR.Customer__c);
        }
        
        for(Operating_Location__c lstOpt : [Select ID,Account__c 
                                            from
                                            Operating_Location__c where Account__c IN : setCustomer
                                            AND IsRegistered__c = true AND Unit__r.Third_Party__c = false
                                            AND Status__c = 'Active']){
                                            
           setDIFCCustomer.add(lstOpt.Account__c);
           
                                                          
       }
            
        for(Service_Request__c iSR : lstSR){
              
              // if third party then not create SR
              if(setDIFCCustomer.contains(iSR.Customer__c) == false && test.isRunningTest() == false) continue;
              
              Service_Request__c newSR = new Service_Request__c();
              newSR.Customer__c  = iSR.Customer__c;
              newSR.Contact__c = iSR.Contact__c;
              newSR.Email__c = iSR.Email__c;
              newSR.Name_of_Contact_Person__c = iSR.Contact__r.FirstName+' '+iSR.Contact__r.LastName;
              newSR.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Car Parking').getRecordTypeId();
              newSR.SR_Template__c = srTemplate.ID;
              
             // newSR.Contact__r= iSR.Contact__r;
            
              // NO DOCUMENT REQUIRED
              newSR.Dedicated_Cable_Quantity__c = 0 ;
              // NO PRICE LINE ITEM REQUIRED
              newSR.Quantity__c = 0;
              
              // for removal
              newSR.Client_s_Representative_in_Charge__c = 'Remove Employee Access';
              newSR.Is_Cancelled__c = true;
              
              //for Renewal
              if(iSR.RecordType.DeveloperName == 'Non_DIFC_Sponsorship_Visa_Renewal' || iSR.RecordType.DeveloperName ==   'Entry_Permit_Renewal' || iSR.RecordType.DeveloperName == 'DIFC_Sponsorship_Visa_Renewal'){
                 
                // system.debug('%%%%%%'  );
                  newSR.Client_s_Representative_in_Charge__c = 'Renewal';
                  newSR.Is_Cancelled__c = true;
              }
              
              newSR.Avail_Courier_Services__c = iSR.Avail_Courier_Services__c;
              newSR.Use_Registered_Address__c = iSR.Use_Registered_Address__c;
              newSR.Consignee_FName__c = iSR.Consignee_FName__c;
              newSR.Consignee_LName__c = iSR.Consignee_LName__c;
              newSR.Courier_Mobile_Number__c = iSR.Courier_Mobile_Number__c;
              newSR.Courier_Cell_Phone__c = iSR.Courier_Cell_Phone__c;
              newSR.Apt_or_Villa_No__c = iSR.Apt_or_Villa_No__c;
              
              lstCarParkingSR.add(newSR);
              //insert newSR;
              //add contact
              setContact.add(iSR.Contact__c);
        }
        //insert SR
        insert lstCarParkingSR;
        
        Map<Id,Document_Details__c> mapOfDoc = new Map<Id,Document_Details__c>();
         Map<Id,Contact> mapOfCon = new Map<Id,Contact>();
        for(Document_Details__c iDoc : [Select Id, Document_Type__c,Document_Number_F__c,Contact__c,
                                                EXPIRY_DATE__c from Document_Details__c
                                                where Contact__c IN: setContact
                                                AND Document_Type__c = 'Access Card']){
            mapOfDoc.put(iDoc.Contact__c,iDoc);                                     
        }
        
        for(Contact iCon : [Select Id, Phone,Email from Contact 
                                                where ID IN: setContact]){
            mapOfCon.put(iCon.Id,iCon);                                     
        }
        
        for(Service_Request__c iSR : lstCarParkingSR){
            
              iSR.Submitted_Date__c = system.today();
              iSR.Submitted_DateTime__c = system.now();
              iSR.Internal_SR_Status__c = SRSubmit;
              iSR.External_SR_Status__c = SRSubmit;
              
                 
              
              iSR.Email_Address__c   = (mapOfCon.containsKey(iSR.Contact__c)) ? mapOfCon.get(iSR.Contact__c).Email : null;
              iSR.Contact_Number_During_Office_Hours__c    = (mapOfCon.containsKey(iSR.Contact__c)) ? mapOfCon.get(iSR.Contact__c).phone : null;
              iSR.DIFC_License_No__c  = (mapOfDoc.containsKey(iSR.Contact__c)) ? mapOfDoc.get(iSR.Contact__c).Document_Number_F__c : '';
              iSR.DIFC_store_specifications__c  = (mapOfDoc.containsKey(iSR.Contact__c)) ? mapOfDoc.get(iSR.Contact__c).Document_Type__c : '' ;
              iSR.Current_License_Expiry_Date__c  = (mapOfDoc.containsKey(iSR.Contact__c)) ?   mapOfDoc.get(iSR.Contact__c).EXPIRY_DATE__c : null;
              
              
        }
        //update SR
       update lstCarParkingSR;
        system.debug('$$$$$$' + lstCarParkingSR);
      
  }
  
  Global void finish(Database.BatchableContext BC)
  {
    
  }
}