/**
*Author : Merul Shah
*Description : This fetch list of all existing Ind/BC of role other location.
**/
public without sharing class OB_RegAddOthrLocExistController
{

    @AuraEnabled   
    public static ResponseWrapper  deleteAmed(String reqWrapPram)
    {
        //reqest wrpper
        OB_RegAddOthrLocExistController.RequestWrapper reqWrap = (OB_RegAddOthrLocExistController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_RegAddOthrLocExistController.RequestWrapper.class);
        
        //response wrpper
        OB_RegAddOthrLocExistController.ResponseWrapper respWrap = new OB_RegAddOthrLocExistController.ResponseWrapper();
       
        
        String amedId = reqWrap.amedId;
        
        HexaBPM_Amendment__c amedTemp = new HexaBPM_Amendment__c( id = amedId );
        delete amedTemp;
        
        
        return respWrap;
    }
    

    
   
    
   
    
    
    public class RequestWrapper 
    {
        //@AuraEnabled public Id flowId { get; set; }
        //@AuraEnabled public Id pageId {get;set;}
        //@AuraEnabled public Id srId { get; set; }
        @AuraEnabled public String amedId{get;set;}
        
        public RequestWrapper()
        {
        
        }
    }

    public class ResponseWrapper 
    {
        //@AuraEnabled public SRWrapper  srWrap{get;set;}
        //@AuraEnabled public AmendmentWrapper amedWrap{get;set;}
        
        public ResponseWrapper()
        {}
    }
    
    
}