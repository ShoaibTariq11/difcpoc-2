public class SuperUserAccessController {
    
    public String userName{get;set;}
    public String selectedAccount{get;set;}
    public List<Selectoption> lstOptions {get;set;}
    public User eachUser;
    public List<SuperUserWrapper> lstWrapper {get; set;}
    
    public SuperUserAccessController() {
        
        lstOptions = new List<Selectoption>();
        lstOptions.add(new SelectOption('-None-','-None-'));
    } 
    
    public void populateRelatedAccounts(){
        
        system.debug('$$$$'+userName);
        if(userName !=null && userName !=''){
            eachUser = new User();
            eachUser = [SELECT FirstName,LastName,Email,ContactID,Community_User_Role__c FROM User where UserName=:userName];
            
            for(AccountContactRelation eachRelation:[SELECT Account.Name FROM AccountContactRelation where ContactID =:eachUser.ContactID AND 
            										(Contact.Account.ROC_Status__c='Active' OR Contact.Account.ROC_Status__c='Not Renewed')]){
                lstOptions.add(new SelectOption(eachRelation.AccountId,eachRelation.Account.Name));
            }
        }
    }
    
    public void populateASignatories(){
        system.debug('!!@@##'+selectedAccount);
        lstWrapper = new List<SuperUserWrapper>();
        for(Relationship__c eachRelation:[SELECT Id,Contact_Name__c,Email__c,Title__c,
        								  Phone_No__c,Individual_First_Name__c,Object_Contact__r.Middle_Names__c,Individual_Last_Name__c,
        								  Passport_No__c,Relationship_Nationality__c,Passport_Issue_Date__c,
        								  Passport_Expiry_Date__c,Birth_date__c,Place_of_Birth__c,
        								  Contact_Phone__c FROM Relationship__c where Relationship_Type__c='Is Authorized Signatory for' AND 
        								  Subject_Account__c=:selectedAccount AND Active__c=true]){
        	lstWrapper.add(new SuperUserWrapper(eachRelation));
        }
    }
    
    public pagereference saveSuperUser(){
        
        List<HexaBPM_Amendment__c> lstAmendments = new List<HexaBPM_Amendment__c>();
        HexaBPM__SR_Template__c srTemplate = new HexaBPM__SR_Template__c();
        srTemplate = [SELECT Id FROM HexaBPM__SR_Template__c where Name ='Super User Authorization' limit 1];
         
        HexaBPM__SR_Status__c srStatus = new HexaBPM__SR_Status__c();
        srStatus = [SELECT Id FROM HexaBPM__SR_Status__c where HexaBPM__Code__c='draft' limit 1];
        
        map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
        for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='HexaBPM__Service_Request__c']){
            mapSRRecordTypes.put(objType.DeveloperName,objType);
        }
        
        map<string,RecordType> mapAmendmentRecordTypes = new map<string,RecordType>();
        for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='HexaBPM_Amendment__c']){
            mapAmendmentRecordTypes.put(objType.DeveloperName,objType);
        }
        
        List<SuperUserWrapper> lstSuperUser = new List<SuperUserWrapper>();
        
        for(SuperUserWrapper eachAmendment:lstWrapper){
        	if(eachAmendment.isSelect){
        		lstSuperUser.add(eachAmendment);
        	}
        }
        
        if(lstSuperUser.size()>0){
	        HexaBPM__Service_Request__c srRequest = new HexaBPM__Service_Request__c();
	        srRequest.HexaBPM__Customer__c = selectedAccount;
	        srRequest.HexaBPM__SR_Template__c = srTemplate.ID;
	        srRequest.first_name__c = eachUser.FirstName;
	        srRequest.HexaBPM__Contact__c = eachUser.ContactID;
	        srRequest.last_name__c = eachUser.LastName;
	        srRequest.HexaBPM__Email__c = eachUser.Email;
	        srRequest.RecordtypeID = mapSRRecordTypes.get('Superuser_Authorization').Id;
	        srRequest.Portal_User_Name__c = userName;
	        srRequest.HexaBPM__External_SR_Status__c = srStatus.ID;
	        srRequest.HexaBPM__Internal_SR_Status__c = srStatus.ID;
	        INSERT srRequest;
	        
	        srRequest.HexaBPM__External_SR_Status__c ='a3x3N0000000Aiv';
	        srRequest.HexaBPM__Internal_SR_Status__c ='a3x3N0000000Aiv';
	        UPDATE srRequest;
	        
	        for(SuperUserWrapper eachAmendment:lstWrapper){
	        	
	        	if(eachAmendment.isSelect){
	        		HexaBPM_Amendment__c amendment = new HexaBPM_Amendment__c();
	        		amendment.ServiceRequest__c = srRequest.ID;
	        		amendment.Roles__c = 'Authorised Signatory';
	        		amendment.Role__c = 'Authorised Signatory';
	        		amendment.Entity_Name__c = selectedAccount;
	        		amendment.Email__c = eachAmendment.relationShipRecord.Email__c;
	        		amendment.Title__c = eachAmendment.relationShipRecord.Title__c;
	        		amendment.Mobile__c  = eachAmendment.relationShipRecord.Phone_No__c;
	        		amendment.First_Name__c = eachAmendment.relationShipRecord.Individual_First_Name__c;
	        		amendment.Last_Name__c = eachAmendment.relationShipRecord.Individual_Last_Name__c;
	        		amendment.Passport_No__c = eachAmendment.relationShipRecord.Passport_No__c;
	        		amendment.Nationality_list__c  = eachAmendment.relationShipRecord.Relationship_Nationality__c;
	        		amendment.Passport_Issue_Date__c = eachAmendment.relationShipRecord.Passport_Issue_Date__c;
	        		amendment.Passport_Expiry_Date__c = eachAmendment.relationShipRecord.Passport_Expiry_Date__c;
	        		amendment.Date_of_Birth__c = eachAmendment.relationShipRecord.Birth_date__c;
	        		amendment.recordtypeId = mapAmendmentRecordTypes.get('Individual').Id;
	        		lstAmendments.add(amendment);
	        	}        	
	        }
	        
	        if(lstAmendments.size()>0){
	        	database.insert(lstAmendments);
	        }
	        pagereference pref = new Pagereference('/apex/FormReviewSuperAdminPage?srNumber='+srRequest.Id);
	        pref.setredirect(true);
	        return pref;
        }
        else {
        	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select atleast one Authorized Signatory to processed.'));
        	return null;
        }
    }
    
    public pagereference cancelAction(){
    
        pagereference pref = new Pagereference('https://uatfull-difcportal.cs128.force.com/digitalOnboarding/s/login/');
        pref.setredirect(true);
        return pref;
    }
    
    public class SuperUserWrapper{
    	
    	public boolean isSelect{get;set;}
    	public Relationship__c relationShipRecord{get;set;}
    	
    	public SuperUserWrapper(Relationship__c relationShipRecordVar){
    		relationShipRecord = relationShipRecordVar;
    	}
    }
}