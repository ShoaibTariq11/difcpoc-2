/******************************************************************************************
 *  Author      : Durga Prasad
 *  Date        : 04-Mar-2020
 *  Description : Controller to show the only some documents related to Establishment Card
 ********************************************************************************************/
public without sharing class OB_ECDocumentsController {
    public string SRID{get;set;}
    public string ApplicationNumber{get;set;}
    public string ApplicationId;
    public list<ApplicationDocument> lstApplicationDocuments{get;set;}
    public OB_ECDocumentsController(){
        SRID = apexpages.currentPage().getParameters().get('Id');
        for(Service_Request__c objSR:[Select Id,OB_Application__c,Name from Service_Request__c where Id=:SRID and OB_Application__c!=null]){
            ApplicationId = objSR.OB_Application__c;
            ApplicationNumber = objSR.Name;
        }
        //ApplicationId = SRID;
        LoadDocuments();
    }
    public void LoadDocuments(){
        lstApplicationDocuments = new list<ApplicationDocument>();
        if(ApplicationId!=null){
            for(HexaBPM__SR_Doc__c doc:[Select Id,HexaBPM__Service_Request__r.Name,Amendment_Name__c,HexaBPM__Document_Name__c,HexaBPM__Document_Description__c,HexaBPM__Doc_ID__c,HexaBPM__Status__c from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:ApplicationId and HexaBPM__SR_Template_Doc__r.Visible_to_GS__c=true]){
                //ApplicationNumber = doc.HexaBPM__Service_Request__r.Name;
                ApplicationDocument objDoc = new ApplicationDocument();
                objDoc.Name = doc.HexaBPM__Document_Name__c;
                objDoc.Description = doc.HexaBPM__Document_Description__c;
                objDoc.DocumentId = doc.HexaBPM__Doc_ID__c;
                objDoc.Status = doc.HexaBPM__Status__c;
                objDoc.AmendmentName = doc.Amendment_Name__c;
                lstApplicationDocuments.add(objDoc);
            }
        }
    }
    public class ApplicationDocument{
        public string Name{get;set;}
        public string Description{get;set;}
        public string Status{get;set;}
        public string DocumentId{get;set;}
        public string AmendmentName{get;set;}
        public ApplicationDocument(){
            Name = '';
            Description = '';
            Status = '';
            DocumentId = '';
            AmendmentName = '';
        }
    }
    public pagereference ViewSR(){
        pagereference pg = new pagereference('/'+SRID);
        pg.setRedirect(true);
        return pg;
    }
}