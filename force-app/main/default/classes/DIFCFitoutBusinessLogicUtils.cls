public with sharing class DIFCFitoutBusinessLogicUtils {
    
    /***********************************************************************
	*	Parameter 		:	Set of account ids  - setOfAccountIds
	*	Return 			:	SOQL in string format 
	*	Created By		:	Mudasir Wani
	*	Created Date	:	31-March-2020
	*************************************************************************/
    public static List<Step__c> getListOfCustomerFeedbackSteps(Set<id> setOfStepIds){
    	String stringOfIds = DIFCBusinessLogicUtils.prepareStringOfIds(setOfStepIds);
    	String relSOQLFieldsToQuery ='Step_Name__c';
		String relSOQLFilters = 'Step_No__c = \'Has DIFC Sponsored Employee\' AND status__c =\'Expired\' and Subject_Account__c in ('+stringOfIds+') and Object_Contact__r.Is_Absconder__c = false and End_Date__c != LAST_N_DAYS:30';
    	//return DIFCDataServiceUtils.getRelationshipList(relSOQLFieldsToQuery , relSOQLFilters);
    	return null;
    }
}