public with sharing class AccountStatementReportUtils {

    public static decimal calculateOpeningBalance(String accountId,Date startDate ,Date endDate){
    	decimal PortalBalance = 0;
    	// Starting portal balance at the time of start date of the statement
            AggregateResult[] totalPostedAmount = [SELECT Transaction_Type__c, SUM(Amount__c)
                                                    FROM Company_Transaction_Detail__c
                                                    WHERE Client__c =: accountId AND Posting_Date__c <: startDate
                                                    GROUP BY Transaction_Type__c ];
            for (AggregateResult ar: totalPostedAmount) {
                if (ar.get('Transaction_Type__c') == 'Credit') {
                    PortalBalance += (Decimal) ar.get('expr0');
                } else if (ar.get('Transaction_Type__c') == 'Debit') {
                    PortalBalance -= (Decimal) ar.get('expr0');
                }
            }
            system.debug('opening Balance --'+PortalBalance);
            return PortalBalance;
    }   
    
    public static void mapofTransactionDetails(String accountId,Date startDate ,Date endDate){
    	for (Company_Transaction_Detail__c ctd: [Select Id, Transaction_Id__c, Posting_Date__c, Reference_Document_Number__c,Consider_for_Account_Statement__c 
                                                        FROM Company_Transaction_Detail__c WHERE Client__c =:accountId AND Posting_Date__c <: startDate]) {
            if (string.isnotBlank(ctd.Reference_Document_Number__c) && ctd.Reference_Document_Number__c.startswith('A17')) {//ROC transactions
               // mapCompTransactionDetailWithRefDocNum.put(ctd.Reference_Document_Number__c, ctd);
            } 
            //v1.9 Start
            else if (string.isnotBlank(ctd.Reference_Document_Number__c) && ctd.Reference_Document_Number__c.equalsIgnoreCase('rorp')) {
               // if( mapRORPDocNotoCompTransactionDetail.containsKey( ctd.Reference_Document_Number__c ) ){
                    //List<Company_Transaction_Detail__c> TempListCTD = mapRORPDocNotoCompTransactionDetail.get( ctd.Reference_Document_Number__c );
                    //TempListCTD.add( ctd );
                //}else{
                    //mapRORPDocNotoCompTransactionDetail.put( ctd.Reference_Document_Number__c, new List<Company_Transaction_Detail__c>{ctd});                        
                //}
            }
            //v1.9 End
            else {
                //mapCompTransactionDetail.put(ctd.Transaction_Id__c, ctd);
            }
    	}
    }
      
    public static void getTransactionsBeforeStartDate(){
    	
    }
    public static void getTransactionsNotPostedToSAP(){
    	
    }
    
    public static void getHexaBPMPriceItems(){
    	
    }
    
    public static void getReceiptsPostedToSAPWithinDates(){
    	
    }
    
    public static void getAllRefundsPostedBetweenDates(){
    	
    }
    
    public static void getAllRequestsSubmittedBetweenDates(){
    	
    }
    
    public static void getAllApplicationsSubmittedBetweenDates(){
    	
    }
    
    public static void getUpgradedPriceItems(){
    	
    }
    
    public static void getManualTransactions(){
    	
    }
    
}