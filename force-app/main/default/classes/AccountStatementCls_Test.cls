@isTest
private class AccountStatementCls_Test {

    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        RecordType rt =[SELECT Id, Name, DeveloperName, SobjectType FROM RecordType where SobjectType ='Account' and Name='Contractor Account'];
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.RORP_License_No__c = '001234';
        objAccount.Registration_License_No__c = '0001';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.ROC_Status__c = 'Active';
        objAccount.RecordTypeId =rt.id;
        insert objAccount;    
        
        map<string,string> mapReceiptRecordTypes = new map<string,string>();
        for(Schema.RecordTypeInfo obj :  Receipt__c.SObjectType.getDescribe().getRecordTypeInfos()){
            mapReceiptRecordTypes.put(obj.getName(),obj.getRecordTypeId());
        }
        system.debug('mapReceiptRecordTypes are : '+mapReceiptRecordTypes);
        
        list<Receipt__c> lstReceipts = new list<Receipt__c>();
        Receipt__c objReceipt = new Receipt__c();
        objReceipt.Customer__c = objAccount.Id;
        objReceipt.Payment_Status__c = 'Success';
        objReceipt.Amount__c = 10000;
        objReceipt.Receipt_Type__c = 'Card';
        lstReceipts.add(objReceipt);
        objReceipt = new Receipt__c();
        objReceipt.Customer__c = objAccount.Id;
        objReceipt.Payment_Status__c = 'Success';
        objReceipt.Amount__c = 10000;
        objReceipt.Receipt_Type__c = 'Cash';
        lstReceipts.add(objReceipt);
        objReceipt = new Receipt__c();
        objReceipt.Customer__c = objAccount.Id;
        objReceipt.Payment_Status__c = 'Success';
        objReceipt.Amount__c = 10000;
        objReceipt.Receipt_Type__c = 'Cheque';
        lstReceipts.add(objReceipt);
        insert lstReceipts;  

        list<Service_Request__c> lstSR = new list<Service_Request__c>();
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Contractor__c = objAccount.Id;
        objSR.Submitted_Date__c = Date.today();
        objSR.SR_Group__c = 'Fit-Out & Events';
        objSR.Pre_GoLive__c = false;
        lstSR.add(objSR);
        
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.Contractor__c = objAccount.Id;
        objSR1.Submitted_Date__c = Date.today();
        objSR1.SR_Group__c = 'Fit-Out & Events';
        objSR1.Pre_GoLive__c = false; 
        objSR1.Is_Cancelled__c= true;  
        objSR1.SAP_Unique_No__c = '332324324324';     
        lstSR.add(objSR1);
        
        Service_Request__c objSR2 = new Service_Request__c();
        objSR2.Customer__c = objAccount.Id;
        objSR2.Contractor__c = objAccount.Id;
        objSR2.Submitted_Date__c = Date.today();
        objSR2.SR_Group__c = 'Fit-Out & Events';
        objSR2.Pre_GoLive__c = false; 
        objSR2.SAP_Unique_No__c = '33232432432444';     
        lstSR.add(objSR2);      
        RecordType srRecordType =[SELECT Id, Name, DeveloperName, SobjectType FROM RecordType where SobjectType ='Service_request__c' and DeveloperName='Request_for_Contractor_Wallet_Refund'];
        SR_Status__c srStatus = new SR_Status__c(Name ='Approved',Code__c='APPROVED');
        insert srStatus;
        Service_Request__c objSR3 = new Service_Request__c();
        objSR3.Customer__c = objAccount.Id;
        objSR3.Contractor__c = objAccount.Id;
        objSR3.RecordTypeId = srRecordType.id;
        objSR3.External_SR_Status__c = srStatus.id;
        objSR3.Submitted_Date__c = Date.today();
        objSR3.SR_Group__c = 'Fit-Out & Events';
        objSR3.Pre_GoLive__c = false; 
        objSR3.SAP_Unique_No__c = '3323243243241';     
        lstSR.add(objSR3); 
        
        insert lstSR;
        
        SR_Price_Item__c objSRItem = new SR_Price_Item__c();
        objSRItem.ServiceRequest__c = objSR.Id;
        objSRItem.Status__c = 'Invoiced';
        objSRItem.Price__c = 1000;
        insert objSRItem;
        
        SR_Price_Item__c objSRItem1 = new SR_Price_Item__c();
        objSRItem1.ServiceRequest__c = objSR1.Id;
        objSRItem1.Status__c = 'Invoiced';
        objSRItem1.Price__c = 1000;
        insert objSRItem1;        

        SR_Price_Item__c objSRItem2 = new SR_Price_Item__c();
        objSRItem2.ServiceRequest__c = objSR2.Id;
        objSRItem2.Status__c = 'Invoiced';
        objSRItem2.Price__c = 1000;
        insert objSRItem2; 
        
        SR_Price_Item__c objSRItem3 = new SR_Price_Item__c();
        objSRItem3.ServiceRequest__c = objSR3.Id;
        objSRItem3.Status__c = 'Invoiced';
        objSRItem3.Price__c = 1000;
        insert objSRItem3;         
        
        Company_Transaction_Detail__c ct = new Company_Transaction_Detail__c();
        ct.Client__c = objAccount.Id;
        ct.Posting_Date__c = Date.today().adddays(-20);
        ct.Transaction_Type__c = 'Credit';
        ct.Amount__c = 10;
        insert ct;
        
        date startDate = Date.today().adddays(-1);
        date endDate = Date.today().adddays(10); 
         
        PageReference pageRef = Page.AccountStatementPDF;
        Test.setCurrentPage(pageRef);
              
        Apexpages.currentPage().getParameters().put('startDate', string.valueOf(startDate) );        
        Apexpages.currentPage().getParameters().put('endDate', string.valueOf(endDate) ); 
        Apexpages.currentPage().getParameters().put('regNo', objAccount.Registration_License_No__c  );  


        Profile testProfile = [SELECT Id 
                               FROM profile
                               WHERE Name = 'DIFC Customer Community User Custom' 
                               LIMIT 1];
    
        Contact cont = new Contact(FirstName='test1',LastName='test2',Email='test.1@example.com');
        cont.AccountId = objAccount.Id;
        insert cont;
        
        User testUser = new User(LastName = 'test2', 
                                 Username = 'test.user.1@example.com', 
                                 Email = 'test.1@example.com', 
                                 Alias = 'testu1', 
                                 TimeZoneSidKey = 'GMT', 
                                 LocaleSidKey = 'en_GB', 
                                 EmailEncodingKey = 'ISO-8859-1', 
                                 ProfileId = testProfile.Id, 
                                 ContactId = cont.Id,
                                 Community_User_Role__c='Company Services'+';'+'Employee Services'+';'+'Property Services',
                                 LanguageLocaleKey = 'en_US');     
        insert testUser;
        
        System.runas(testUser){
            AccountStatementCls ac = new AccountStatementCls();
            ac.accountType ='Contractor_Account';
            ac.activeLicenseNumber = '001234';
            ac.search();
            ac.generateStatement();         
        }
    }
}