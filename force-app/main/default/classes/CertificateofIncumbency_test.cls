/***********************************************************
*Apex Class : CertificateofIncumbency_test 
*Description: Test class for CertificateofIncumbency
*Creaed Date : 02-Jul-2019
*Assembla : #4979
************************************************************/
@isTest
private class CertificateofIncumbency_test {
    static testMethod void myUnitTest() {
        test.startTest();
       
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        objAccount.ROC_Status__c = 'Active';
        objAccount.RecordTypeId = [select Id,Name,DeveloperName from RecordType where DeveloperName ='DFSA_and_Share' AND SobjectType='Account' limit 1].id;
        objAccount.Financial_Year_End__c = '31st December';
        //objAccount.Active_License__c = objLic.id;
        insert objAccount;
        
        License__c objLic = new License__c();
        objLic.License_Issue_Date__c = system.today();
        objLic.License_Expiry_Date__c = system.today().addDays(365);
        objLic.Status__c = 'Active';
        objLic.Account__c = objAccount.id;
        insert objLic;
        
        objAccount.Active_License__c = objLic.id;
        update objAccount;
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test2432@difcportal.com';
        
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser123@testorg.com');
        insert objUser;
        
        Account objAccount1 = new Account();
        objAccount1.Name = 'Test Custoer 2';
        objAccount1.E_mail__c = 'test@test.com';
        objAccount1.BP_No__c = '001235';
        objAccount1.Company_Type__c = 'Retail';
        objAccount1.Sector_Classification__c = 'Retail';
        objAccount1.Legal_Type_of_Entity__c = 'LTD';
        objAccount1.ROC_Status__c = 'Active';
        insert objAccount1;
        
        Contact con = new Contact(lastname ='TestEventService', AccountId=objAccount.id);
        con.recordTypeId=[select Id,Name,DeveloperName from RecordType where DeveloperName='Portal_User' AND SobjectType='Contact'].id;
        con.FirstName = 'firstName';
        con.LastName = 'Lastname';
        con.email =  'Lastname@gmail.com';
        con.Passport_No__c = 'Passpot1234';
        con.Role__c = 'Company Services';
        insert con;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        SR_Status__c objSRStatus;
           
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Submitted';
        objSRStatus.Code__c = 'Submitted';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Draft';
        objSRStatus.Code__c = 'DRAFT';
        lstSRStatus.add(objSRStatus);  
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Approved';
        objSRStatus.Code__c = 'Approved';
        lstSRStatus.add(objSRStatus);
             
        insert lstSRStatus;
        
        
        Service_Request__c  srRec = new Service_Request__c ();
        
        ApexPages.StandardController controller = new ApexPages.StandardController(srRec); 
        
        CertificateofIncumbency clsObj = new CertificateofIncumbency(controller);
        srRec.RecordTypeId =[select Id,Name,DeveloperName from RecordType where DeveloperName='Certificate_of_Incumbency' AND SobjectType='Service_Request__c'].id;
        clsObj.SaveConfirmation();
        clsObj.EditSR();
        clsObj.SubmitRequest();
        test.stopTest();
    }
}