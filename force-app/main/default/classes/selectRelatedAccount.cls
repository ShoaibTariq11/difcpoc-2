public class selectRelatedAccount{
    
    public List<wrapperClsList> accwrapperList {get;set;}
    public string selectedacc {get;set;}
    public string contId ;
    public List<wrapperClsList> getaccList(){
        User u = [SELECT Id, ContactId,Contact.AccountId, Contact.Account.Name FROM User WHERE Id = :UserInfo.getUSerId()];
        //accList = [select Id,Name,Company_Type__c from account where Contact.Id='0030E00000cyDO6'];
        if(u.ContactId==null){
            contId = '0033N0000084f3b';
        }else{
            contId = u.ContactId;
        }
        List<AccountContactRelation> accContactList = [SELECT AccountId,Account.Name,ContactId,IsActive FROM AccountContactRelation WHERE ContactId=:contId];
        
        accwrapperList = new List<wrapperClsList>();
        for(AccountContactRelation itr:accContactList){
            accwrapperList.add(new wrapperClsList(itr));
        }
        return accwrapperList ;
    }    
    public PageReference updateAccList(){      
        try{ 
        Boolean isAccSelected;
        String selAccname = null;
        for(wrapperClsList itr: getaccList()) {
        selectedacc = System.currentPagereference().getParameters().get('accid'); 
            system.debug('-------'+itr.accSelid+'----'+selectedacc);
            if(selectedacc==itr.acList.AccountId){
                system.debug('-------'+itr.acList.AccountId);
                selAccname = itr.acList.Account.Name;
                //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM,itr.acList.Account.Name);         
                //ApexPages.addMessage(myMsg); 
                isAccSelected = true;
            }
        }
        if(isAccSelected){
            contact c = [select id,accountid from contact where id=:contId];
            c.accountid = selectedacc;
            update c;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM,'Account switched with -'+selAccname);         
            ApexPages.addMessage(myMsg); 
        }
            //update accList;
        }
        catch(DmlException ex){
            ApexPages.addMessages(ex);
        }
        return null;
    }
    public class wrapperClsList {
        public AccountContactRelation acList {get; set;}
        public Boolean accSelid {get;set;}
        public wrapperClsList(AccountContactRelation ac){
            acList = ac;
            accSelid = false;
        }
    }
}