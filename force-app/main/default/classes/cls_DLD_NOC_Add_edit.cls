public without sharing class cls_DLD_NOC_Add_edit {


    public string RecordTypeId;
    public map<string,string> mapParameters;
    public String CustomerId;
    public Account ObjAccount{get;set;}
    public Service_Request__c SRData{get;set;}
    public boolean ValidTosubmit;
    public List<Property__c> ListProperties{get;set;}
    
    public List<UBO_Data__c> ListUBOData{get;set;}
    
    public boolean  IsUBOrequred{get;set;}
    public Map<string,string> BcShareholder;
   
    public cls_DLD_NOC_Add_edit(ApexPages.StandardController controller) {
      
      IsUBOrequred=false;
      
       List<String> fields = new List<String> {'Customer__c','Legal_Structures__c'};
        if (!Test.isRunningTest()) controller.addFields(fields); // still covered
        
       SRData=(Service_Request__c)controller.getRecord();
     ValidTosubmit=true;
     
     ListProperties=new List<Property__c>();
     ListUBOData=new List<UBO_Data__c>();
       BcShareholder=new Map<string,string>();
       mapParameters = new map<string,string>();        
        if(apexpages.currentPage().getParameters()!=null)
        mapParameters = apexpages.currentPage().getParameters();              
       if(mapParameters.get('RecordType')!=null) 
       RecordTypeId= mapParameters.get('RecordType');    
       else
      RecordTypeId=Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('DLD_NOC').getRecordTypeId();
         
       
            for(User objUsr:[select id,ContactId,Email,Phone,Contact.Account.Exempted_from_UBO_Regulations__c,Contact.Account.Legal_Type_of_Entity__c,Contact.AccountId,Contact.Account.Company_Type__c,
                             Contact.Account.Financial_Year_End__c,Contact.Account.Next_Renewal_Date__c,Contact.Account.Name,
                             Contact.Account.Qualifying_Type__c,Contact.Account.Restrict_certificate_of_good_standing__c,Contact.Account.Contact_Details_Provided__c,
                             Contact.Account.Index_Card__c,Contact.Account.Index_Card_Status__c,Contact.Account.Is_Foundation_Activity__c  
                             from User where Id=:userinfo.getUserId()])
                {
                 CustomerId = objUsr.Contact.AccountId;
                 ObjAccount= objUsr.Contact.Account;
                 system.debug('ObjAccount==>'+ObjAccount);
                 IsUBOrequred=objUsr.Contact.Account.Restrict_certificate_of_good_standing__c;
                 if(SRData.id==null)
                 {
                    SRData.Customer__c = objUsr.Contact.AccountId;
                    SRData.RecordTypeId=RecordTypeId;
                    SRData.Email__c = objUsr.Email;
                    SRData.Legal_Structures__c=objUsr.Contact.Account.Legal_Type_of_Entity__c;
                    SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                    SRData.Entity_Name__c=objUsr.Contact.Account.Name;
                     SRData.Sponsor_Mother_Full_Name__c=SRData.Legal_Structures__c;
                  }
                 
                  
              
              }
              
                     BcShareholder =new Map<string,string>();
                
                    if(BcShareholder.isEmpty())  
                  for(Relationship__c ObjRel:[select Object_Account__c,Object_Account__r.Name from Relationship__c where Subject_Account__c=:SRData.Customer__c and Active__c=true and System_Relationship_Type__c='Shareholder' and Object_Account__c!=null and Subject_Account__c!=null ])
                  {
                      System.debug('==========BcShareholder=========='+BcShareholder);
                      BcShareholder.put(ObjRel.Object_Account__c,ObjRel.Object_Account__r.Name);
                      SRData.Sponsor_Mother_Full_Name__c=SRData.Legal_Structures__c+'_UBO';
                  }
                
    
    
     
    
    }
    
    //********************************************* UBO ***************************
    
    public List<SelectOption> getBCUBOs()
    {
        System.debug('==========BcShareholder=========='+BcShareholder);
        List<SelectOption> options = new List<SelectOption>();
         for( string  f : BcShareholder.keyset())
           {
              options.add(new SelectOption(f, BcShareholder.get(f)));
           }
        return options;        
        
    }
    
     public List<UBO_Data__c> getSrListUBO()
        {
              ListUBOData=[select id,Country__c,BC_UBO__c,Entity_Name__c,Nationality__c, I_am_Exempt_entities__c, Title__c, Gender__c,First_Name__c,Last_Name__c, Passport_Number__c,Date_of_Birth__c,Place_of_Birth__c,Passport_Data_of_Issuance__c,Passport_Expiry_Date__c from UBO_Data__c where Service_Request__c!=null and  Service_Request__c=:SRData.id];
              return ListUBOData;
              
              
              
        }
        
    public UBO_Data__c ObjUBO{get;set;}
    public  void AddUBORecord()
    {
        ObjUBO=new UBO_Data__c();
          ObjUBO.RecordTypeId=Schema.SObjectType.UBO_Data__c.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();
        ObjUBO.Status__c='Draft';
    }
    
      public  void UboValueChange()
    {
        string passedParam1 = Apexpages.currentPage().getParameters().get('ubotype');
        ObjUBO.I_am_Exempt_entities__c=passedParam1;
       // if(passedParam1=='Yes')
      //  ObjUBO.RecordTypeId=Schema.SObjectType.UBO_Data__c.getRecordTypeInfosByDeveloperName().get('Body_Corporation_UBO').getRecordTypeId();
      //  else
      //  ObjUBO.RecordTypeId=Schema.SObjectType.UBO_Data__c.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();
         
    }
    
    public  void SaveUBORowAndAdd()
    {
        SaveUBORow();
        AddUBORecord();
    }
    
    public  void SaveUBORow()
    {
      try     
         { 
 
            boolean InsertRe=true;

            if(SRData.id==null)
            upsert SRData;
        
            if(ObjUBO.I_am_Exempt_entities__c=='Yes')
            if(![select id,BC_UBO__c from UBO_Data__c where Service_Request__c!=null and  Service_Request__c=:SRData.id and I_am_Exempt_entities__c='Yes' and BC_UBO__c=:ObjUBO.BC_UBO__c].isEmpty())
            {
            //BC_UBO__c
                InsertRe=false;
            }
        
        
            if(InsertRe)
            {
                ObjUBO.Service_Request__c=SRData.id;
                ObjUBO.Entity_Name__c=BcShareholder.get(ObjUBO.BC_UBO__c);
                ObjUBO.Entity_Type__c='NOC Ubo';
                upsert ObjUBO;
                
                  System.debug('ObjUBO===>'+ObjUBO);
            ObjUBO=null;
                
            }
            else
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'The Ultimate Beneficial Owner already added in the list.');         
                ApexPages.addMessage(myMsg);   
            }

          
       
        }catch (Exception e)     
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());         
                ApexPages.addMessage(myMsg);    
            }  
        
        
        
    }
    
       public void removingUBORow()
    {
         Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
         if(ListUBOData.size()>=param)
         {
            UBO_Data__c TempObjPro=ListUBOData[param];
            ListUBOData.remove(param);
            if(TempObjPro.id!=null)
            delete TempObjPro;
         }
         
         
        
    }
    
    
    //********************************************* Properties***************************
    
        public List<Property__c> getSrListProperties()
        {
              ListProperties=[select id,Account__c,Area_sq_feet__c,Building_name__c,Building_No__c,Community__c,Floor_No__c,Municipality_No__c,Plot_No__c,Property_No__c,Property_Type__c,Service_Request__c,Status__c,Unit_No__c from Property__c where Service_Request__c!=null and  Service_Request__c=:SRData.id];
              
              return ListProperties;
              
              
        }
        
    public  void RowValueChange()
    {
        string passedParam1 = Apexpages.currentPage().getParameters().get('myParam');
        ObjPro.Property_Type__c=passedParam1 ;
    }
    
    public Property__c ObjPro{get;set;}
    public  void AddRecord()
    {
        ObjPro=new Property__c();
       // ObjPro.Status__c='Draft';
    }
    public  void CancelSaveRow()
    {
     ObjPro=null;
      ObjUBO=null;
    }
    
  public  void SavePropertyRowAndAdd()
    {
        SaveRow();
        AddRecord();
    }
    
    public  void SaveRow()
    {
      try     
            {  
        if(SRData.id==null)
        upsert SRData;
       
       ObjPro.Service_Request__c=SRData.id;
       ObjPro.Account__c=SRData.Customer__c;
       
       upsert ObjPro;
       ObjPro=null;
        }catch (Exception e)     
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());         
                ApexPages.addMessage(myMsg);    
            }  
        
        
        
    }
    public void removingRow()
    {
         Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
         if(ListProperties.size()>=param)
         {
            Property__c TempObjPro=ListProperties[param];
            ListProperties.remove(param);
            if(TempObjPro.id!=null)
            delete TempObjPro;
         }
         
         
        
    }
 //****************************************************************************************************************************
   
public string  validationRule()
{
    
    string Errormessage='';
    
    if(ListUBOData.isEmpty() && SRData.Sponsor_Mother_Full_Name__c.contains('UBO'))
    {
        Errormessage='Please provide Ultimate Beneficial Owner Details.';
    }
    else if(SRData.Sponsor_Mother_Full_Name__c.contains('UBO'))
    {
        //ListUBOData..UBO_Data__c
        
        map<string,string> BDShareholders=new map<string,string>();
        for(string Keyval:BcShareholder.keyset())
            BDShareholders.put(Keyval,Keyval);
        
        for( UBO_Data__c  tempUBO : ListUBOData)
        {
            if(BDShareholders.containsKey(tempUBO.BC_UBO__c))
            {
                BDShareholders.remove(tempUBO.BC_UBO__c);
            }
        }
        if(BDShareholders.size()>0)
        {
              Errormessage='Please provide the Ultimate Beneficial Owner Details for each of the listed body corporate shareholder (or equivalent).';
            
        }
        
    }
    
    
    
    
    
    if(ListProperties.isEmpty())
    {
        Errormessage='Please provide Property details.';
    }
    
    //if(SRData.In_Accordance_with_Article_12_1bj__c!=true && SRData.Sponsor_Mother_Full_Name__c=='UBO_Company')
   // {
    //    Errormessage='Please provide all the required information.';
   // }
    
    if(SRData.In_accordance_with_Article_10_1__c!=true ||
    SRData.In_Accordance_with_Article_11__c!=true ||
    SRData.In_Accordance_with_Article_12_1a__c!=true )
    {
       Errormessage='Please provide all the required information.';

    }
    
    
    
    
    
    
    
    
    
    return Errormessage;
    
}   
    public  PageReference SaveRecord()
    {
    
    string Errormessage=validationRule();
    
    boolean isvalid=String.isBlank(Errormessage);
    
  
    
        if(isvalid) 
        {       
        
            try     
            {  
                upsert SRData;
                PageReference acctPage = new ApexPages.StandardController(SRData).view();        
                acctPage.setRedirect(true);        
                return acctPage;     
            }catch (Exception e)     
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());         
                ApexPages.addMessage(myMsg);    
            }  
         }
         else
         {
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Errormessage);         
                ApexPages.addMessage(myMsg); 
         }
   
    return null;
    
       
    }
    
}