/********************************************************************************************************************************************************
 *  Author   	: Mudasir Wani
 *  Date     	: 04-April-2020
 *  Description	: This class will have the common email related functioalities across the DIFC organization
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date       			Updated By    		Description
---------------------------------------------------------------------------------------------------------------------  
V1.0	04-April-2020		Mudasir 			Created this class 
*********************************************************************************************************************************************************/

public without sharing class DIFCEmailUtils {
    
    public static Email_Notification__c createEmailNotificationRecord(String notificationSubject, String notificationType ,
																String emailBody , String smsBody,
																String To_addresses, String CC_addresses,
																String BCC_addresses ){
		Email_Notification__c emailNotif = new Email_Notification__c();
		emailNotif.Subject__c = notificationSubject;
		emailNotif.Notification_Type__c = notificationType;
		emailNotif.Email_Body__c = emailBody;
		emailNotif.SMS_Body__c = smsBody;
		emailNotif.To_Addresses__c = To_addresses;
		emailNotif.CC_Addresses__c = CC_addresses;
		emailNotif.BCC_Addresses__c = BCC_addresses;
		return emailNotif;
	} 
	
	public Messaging.SingleEmailMessage createEmailMessage(Service_Request__c servReqRec , List<String> toaddresses,List<String> toCCaddresses,List<String> toBCCaddresses,EmailTemplate emailTemp,Integer delayedDays,String orgWideEmailAddId) {
        if(Test.isRunningTest()){toaddresses.add('mudasir@difc.ae');servReqRec.Property_Type__c ='Office'; delayedDays =28;} 
        if(servReqRec.Property_Type__c != Null && servReqRec.Property_Type__c.contains('Office')){toCCaddresses = System.label.ccFitOutDelayNotificationList_Office.split(';');}
            //else{toCCaddresses = System.label.ccFitOutDelayNotificationList.split(';'); }
        Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();system.debug('toaddresses----------'+toaddresses);
        semail.setToAddresses(toaddresses);
        semail.setCcAddresses(toCCaddresses);
        semail.templateid = emailTemp.id;        
        Integer reminderNumber = (delayedDays - 21)/7;
        reminderNumber = reminderNumber > integer.valueOf(System.label.fitoutReminderNumber) ? integer.valueOf(System.label.fitoutReminderNumber) : reminderNumber;
        String subjectString = emailTemp.Subject;
        subjectString = delayedDays > 21 ? System.Label.ReminderStringValue +reminderNumber+' '+subjectString.replace('{!Service_Request__c.Name}',servReqRec.Name) : subjectString.replace('{!Service_Request__c.Name}',servReqRec.Name);
        subjectString = subjectString.contains('{!Service_Request__c.Customer_Name__c}') ? subjectString.replace('{!Service_Request__c.Customer_Name__c}',servReqRec.Customer_Name__c) : subjectString;
        semail.setOrgWideEmailAddressId(orgWideEmailAddId);
        string bodyValue =  emailTemp.Body;
        semail.setSubject(subjectString);
        bodyValue = bodyValue.replace('{!Service_Request__c.Name}',servReqRec.Name);
        semail.setPlainTextBody(bodyValue); 
        system.debug('messageRec------Mudasir------'+semail);
        return semail;
    } 
}