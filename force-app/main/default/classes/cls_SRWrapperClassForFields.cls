/*****************************************************************************************************************************
    Author      :   Swati Sehrawat
    Date        :   21st July 2016
    Description :   This class is used as a wrapper class for Service Request object to reuse the fields
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    V1.1    05-12-2016  Swati         as per ticket 3502
    V1.2    10-12-2016  Veera         as per ticket 4104
    --------------------------------------------------------------------------------------------------------------------------             
*****************************************************************************************************************************/

public with sharing class cls_SRWrapperClassForFields{

    //auto Populated fields on portal for client details
    public string client {get;set;}
    public string clientId {get;set;}
    public string licenseNumber {get;set;}
    public string indexCard {get;set;}
    public string mobile {get;set;}
    public string email {get;set;}
    
    //fieldsforSRtitleanddescription
    public string srTitle {get;set;}
    public string srDescription {get;set;}
    
    //fields used for picklistvalues onpage for GS Comfort Letters
    public string requestLicense {get;set;}
    public string letterHajj {get;set;}
    public string letterUmrah {get;set;}
    public string authorityCompany {get;set;}
    public string authorityTemporary {get;set;}
    public string emirateConfirmation {get;set;}
    public string emirateLicense {get;set;}
    public string emirateCompanyVehicle {get;set;}
    public string emiratePersonalVehicle {get;set;}
    public string emirateHajj {get;set;}
    public string emirateBoat {get;set;}
    public string emirateMarine {get;set;}
    public string emirateCompany {get;set;}
    public string emirateUmrah {get;set;}
    public string emiratePort {get;set;}
    public string emirateImmigration {get;set;}
    public string formatConfirmation {get;set;}
    public string formatCompany {get;set;}
    public string serviceCompanyVehicle {get;set;}
    public string servicePersonalVehicle {get;set;}
    public string originalOrSoftCopy {get;set;} //V1.1
    public string addresseNameConfirmation {get;set;}
    public string addresseNamePort {get;set;}
    public string managerAuthority {get;set;} //V1.2
    public string managerEmirates  {get;set;} //V1.2
    public string managerAccount   {get;set;} //V1.2
    //checkboxestocheckwhichallcomfortlettersareneededonvfpage for GS Comfort Letters
    public boolean confirmation {get;set;}
    public boolean NOCboat {get;set;}
    public boolean NOCdriving {get;set;}
    public boolean NOCmarine {get;set;}
    public boolean NOCpassport {get;set;}
    public boolean NOCtemporary {get;set;}
    public boolean NOCPersonalVehicle {get;set;}
    public boolean NOCCompanyVehicle {get;set;}
    public boolean NOCcompany {get;set;}
    public boolean NOChajj {get;set;}
    public boolean NOCumrah {get;set;}
    public boolean NOCimmigration {get;set;}
    public boolean NOCliquor {get;set;}
    public boolean NOCPortEntry {get;set;}
     public boolean NOCMagrothercomp {get;set;}
    
    //fields used for picklistvalues onpage for Embassy Letter
    public string LetterFormat {get;set;}
    public string PurposeofVisit {get;set;}
    public string EmirateEmbassy {get;set;}
    public string LetterToConsulate {get;set;}
    public string typeOfVisitVisa {get;set;}
    
    //Dropdown values for picklist used on thepage for GS Comfort Letters/Embassy Letter
    public List<SelectOption> serviceCategoryList {get;set;}
    public List<SelectOption> requestLicenseList {get;set;}
    public List<SelectOption> letterHajjList {get;set;}
    public List<SelectOption> letterUmrahList {get;set;}
    public List<SelectOption> authorityCompanyList {get;set;}
    public List<SelectOption> authorityTemporaryList {get;set;}
    public List<SelectOption> emirate1List {get;set;}
    public List<SelectOption> emirate2List {get;set;}
    public List<SelectOption> letterFormatList {get;set;}
    public List<SelectOption> applicantOriginalList {get;set;}
    public List<SelectOption> purposeOfList {get;set;}
    public List<SelectOption> typeOfVisitVisaList {get;set;}
    
    public cls_SRWrapperClassForFields(string RecordTypeName){
        client='';
        clientId='';
        licenseNumber='';
        indexCard='';
        mobile='';
        email='';
        srTitle='';
        srDescription='';
        requestLicense='';
        letterHajj='';
        letterUmrah='';
        authorityCompany='';
        authorityTemporary='';
        emirateConfirmation='';
        emirateLicense='';
        emirateCompanyVehicle='';
        emiratePersonalVehicle='';
        emirateHajj='';
        emirateBoat='';
        emirateMarine='';
        emirateCompany='';
        emirateUmrah='';
        emiratePort='';
        emirateImmigration='';
        formatConfirmation='';
        formatCompany='';
        serviceCompanyVehicle = '';
        servicePersonalVehicle = '';
        confirmation=false;
        NOCboat=false;
        NOCdriving=false;
        NOCmarine=false;
        NOCpassport=false;
        NOCtemporary=false;
        NOCPersonalVehicle=false;
        NOCCompanyVehicle=false;
        NOCcompany=false;
        NOChajj=false;
        NOCumrah=false;
        NOCimmigration=false;
        NOCliquor=false;    
        
        LetterFormat = '';
        PurposeofVisit = '';
        EmirateEmbassy = '';
        LetterToConsulate = '';
        typeOfVisitVisa = '';
        
        requestLicenseList = new List<SelectOption>();
        serviceCategoryList = new List<SelectOption>();
        letterHajjList = new List<SelectOption>();
        letterUmrahList = new List<SelectOption>();
        authorityCompanyList = new List<SelectOption>();
        authorityTemporaryList = new List<SelectOption>();
        emirate1List = new List<SelectOption>();
        emirate2List = new List<SelectOption>();
        letterFormatList = new List<SelectOption>();
        applicantOriginalList = new List<SelectOption>();
        purposeOfList = new List<SelectOption>();
        typeOfVisitVisaList = new List<SelectOption>();
        
        if(recordTypeName == 'GS_Comfort_Letters'){
            initializeValuesForComfortLetters();    
        }
        
        if(recordTypeName == 'Consulate_or_Embassy_Letter'){
            initializeValuesForEmbassyLetters();    
        }
    }
    
    public void initializeValuesForEmbassyLetters(){
        typeOfVisitVisaList.add(new SelectOption('','--None--'));
        typeOfVisitVisaList.add(new SelectOption('Single Entry','Single Entry'));
        typeOfVisitVisaList.add(new SelectOption('Multiple Entry','Multiple Entry'));
        typeOfVisitVisaList.add(new SelectOption('Multiple visits for three months','Multiple visits for three months'));
        typeOfVisitVisaList.add(new SelectOption('Multiple visits for six months','Multiple visits for six months'));
        
        purposeOfList.add(new SelectOption('','--None--'));
        purposeOfList.add(new SelectOption('Personal','Personal'));
        purposeOfList.add(new SelectOption('Business','Business'));
        
        letterHajjList.add(new SelectOption('','--None--'));
        letterHajjList.add(new SelectOption('Consulate General Of','Consulate General Of'));
        letterHajjList.add(new SelectOption('Consulate Of','Consulate Of'));
        letterHajjList.add(new SelectOption('Embassy Of','Embassy Of'));
        
        letterFormatList.add(new SelectOption('','--None--'));
        letterFormatList.add(new SelectOption('Arabic','Arabic'));
        letterFormatList.add(new SelectOption('English','English'));
        
        emirate1List.add(new SelectOption('','--None--'));
        emirate1List.add(new SelectOption('Abu Dhabi','Abu Dhabi'));
        emirate1List.add(new SelectOption('Dubai','Dubai'));
    }
    
    public void initializeValuesForComfortLetters(){
        
        serviceCategoryList.add(new SelectOption('','--None--'));
        serviceCategoryList.add(new SelectOption('New','New'));
        serviceCategoryList.add(new SelectOption('Renewal','Renewal'));
        
        requestLicenseList.add(new SelectOption('','--None--'));
        requestLicenseList.add(new SelectOption('Light Vehicle','Light Vehicle'));
        requestLicenseList.add(new SelectOption('Motor Cycle','Motor Cycle'));
        
        letterHajjList.add(new SelectOption('','--None--'));
        letterHajjList.add(new SelectOption('Consulate General Of','Consulate General Of'));
        letterHajjList.add(new SelectOption('Consulate Of','Consulate Of'));
        letterHajjList.add(new SelectOption('Embassy Of','Embassy Of'));
        
        letterUmrahList.add(new SelectOption('','--None--'));
        letterUmrahList.add(new SelectOption('Consulate General Of','Consulate General Of'));
        letterUmrahList.add(new SelectOption('Consulate Of','Consulate Of'));
        letterUmrahList.add(new SelectOption('Embassy Of','Embassy Of'));
        
        authorityCompanyList.add(new SelectOption('','--None--'));
        authorityCompanyList.add(new SelectOption('Department of Economic Development','Department of Economic Development'));
        authorityCompanyList.add(new SelectOption('RAK Free Trade Zone Authority','RAK Free Trade Zone Authority'));
        authorityCompanyList.add(new SelectOption('Dubai Technology and Media Free Zone','Dubai Technology and Media Free Zone'));
        authorityCompanyList.add(new SelectOption('Dubai Multi Commodities Centre','Dubai Multi Commodities Centre'));
        authorityCompanyList.add(new SelectOption('Dubai Airport Free Zone Authority','Dubai Airport Free Zone Authority'));
        authorityCompanyList.add(new SelectOption('Jebel Ali Free Zone Authority','Jebel Ali Free Zone Authority'));
        authorityCompanyList.add(new SelectOption('Others','Others'));
        
        authorityTemporaryList.add(new SelectOption('','--None--'));
        authorityTemporaryList.add(new SelectOption('Dubai Technology and Media Free Zone','Dubai Technology and Media Free Zone'));
        authorityTemporaryList.add(new SelectOption('Dubai Multi Commodities Centre','Dubai Multi Commodities Centre'));
        authorityTemporaryList.add(new SelectOption('Dubai Airport Free Zone Authority','Dubai Airport Free Zone Authority'));
        authorityTemporaryList.add(new SelectOption('Jebel Ali Free Zone Authority','Jebel Ali Free Zone Authority'));
        authorityTemporaryList.add(new SelectOption('Dubai Healthcare City','Dubai Healthcare City'));
        authorityTemporaryList.add(new SelectOption('Dubai International Academic City','Dubai International Academic City'));
        authorityTemporaryList.add(new SelectOption('Dubai Gold and Diamond Park','Dubai Gold and Diamond Park'));
        authorityTemporaryList.add(new SelectOption('Dubai Silicon Oasis (DSO)','Dubai Silicon Oasis (DSO)'));
        authorityTemporaryList.add(new SelectOption('Roads & Transport Authority','Roads & Transport Authority'));
        
        emirate1List.add(new SelectOption('','--None--'));
        emirate1List.add(new SelectOption('Abu Dhabi','Abu Dhabi'));
        emirate1List.add(new SelectOption('Dubai','Dubai'));
        
        emirate2List.add(new SelectOption('','--None--'));
        emirate2List.add(new SelectOption('Abu Dhabi','Abu Dhabi'));
        emirate2List.add(new SelectOption('Dubai','Dubai'));
        emirate2List.add(new SelectOption('Sharjah','Sharjah'));
        emirate2List.add(new SelectOption('Ajman','Ajman'));
        emirate2List.add(new SelectOption('Umm Al Quwain','Umm Al Quwain'));
        emirate2List.add(new SelectOption('Ras Al Khaimah','Ras Al Khaimah'));
        emirate2List.add(new SelectOption('Al Fujairah','Al Fujairah'));
        
        letterFormatList.add(new SelectOption('','--None--'));
        letterFormatList.add(new SelectOption('Arabic','Arabic'));
        letterFormatList.add(new SelectOption('English','English'));
        
        applicantOriginalList.add(new SelectOption('','--None--'));
        applicantOriginalList.add(new SelectOption('Original','Original'));
        applicantOriginalList.add(new SelectOption('Soft Copy','Soft Copy'));
    }
    
}