/*********************************************************************************************************************
*  Name     : SecurityStepStatusUpdateBatch 
*  Author   : 
*  Purpose  : This class used to get response from Dubai police portel
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
 V1.0                             12/09/2019
**/

global class SecurityStepStatusUpdateBatch implements database.batchable<sobject>,Database.AllowsCallouts, Database.Stateful{
    
    global database.querylocator start(database.batchablecontext bc){
        
        String statusRequest = Label.Pending_Review_Status; 
        String serviceType = 'Application for Incorporation / Registration';
        String stepName = 'Security Email Approval';
        String s = 'SELECT Id,Name,Status__c, Step__c.SR__r.Name FROM Step__c where SR__r.Service_Type__c=:serviceType AND Step_Name__c =:stepName AND Status__c =:statusRequest';
        system.debug('=query==='+s);
        return database.getquerylocator(s);
    }
    
    global void execute(database.batchablecontext bc, List<Step__c> lstStepData){
        
        String USERNAME = Label.DYFC_API_User_Name;   //This Variable used to Store API user User Name
        String PASSWORD = Label.DYFC_Password;        //This Variable used to Store API user PASSWORD
        String CONSUMER_KEY = Label.DIFC_Consumer_KEY; //This Variable used to Store CONSUMER_KEY
        String CONSUMER_SECRET = Label.DIFC_Consumer_Secret; //This Variable used to Store CONSUMER_SECRET
        List<Step__c> lstStepDataUpdate = new List<Step__c>();
        
        Blob headerValue = Blob.valueOf(USERNAME + ':' + PASSWORD);
        String authorizationHeader = Label.Dubai_Police_Response_URL+CONSUMER_KEY+'&client_secret='+CONSUMER_SECRET+'&username='+USERNAME+'&password='+PASSWORD;
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(authorizationHeader);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        HttpResponse response = http.send(request);
         system.debug('---response----'+response); 
        system.debug('---response--body--'+response.getBody()); 
        if(response.getStatusCode() == 200){    
            
            Http http1 = new Http();
            HttpRequest request1 = new HttpRequest();
            HttpResponse response1 = new HttpResponse(); 
            
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            Object BearerCode1 = results.get('access_token');
            String BearerCode = String.valueOf(BearerCode1);
            BearerCode = 'Bearer '+BearerCode;
            system.debug('---BearerCode----'+BearerCode);
            for(Step__c eachStep:lstStepData){
                system.debug(eachStep.SR__r.Name+'=eachStep==='+eachStep.Id);
                request1 = new HttpRequest();
                request1.setHeader('Authorization',BearerCode);
                request1.setEndpoint(Label.Dubai_Police_Response_URL_2+ eachStep.SR__r.Name);
                request1.setMethod('GET');
                response1 = http1.send(request1);
                system.debug('---response1.getStatusCode()-----'+Label.Dubai_Police_Response_URL_2+ Step__c.SR__r.Name);
                system.debug('---response1.getBody()-----'+response1.getBody());
               
                if(response1.getStatusCode() == 200){
                   
                    String Status  = '';
                    String comments ='';
                    
                    if(!test.isRunningTest()){
                        DubaiPoliceJSONResponse myClass = DubaiPoliceJSONResponse.parse(response1.getBody());
                        Status = myClass.IssuanceDto.requestCommonData.status;
                        comments = myClass.IssuanceDto.requestCommonData.policeNotes;
                    }
                    else{
                     Status = 'Approved';
                     comments = 'test'; 
                    }
                    
                    if(String.isNotBlank(Status) && Status =='APPROVED'){
                        eachStep.Status__c = Label.Verified_SR_Status_ID;
                        eachStep.Step_Notes__c = comments;
                        eachStep.Closed_Date__c = system.today();
                        lstStepDataUpdate.add(eachStep);
                    } 
                    else if(String.isNotBlank(Status) && Status =='NEED_MORE_INFO'){
                        eachStep.Status__c = Label.Return_for_More_Information_Step_Status_ID;
                        eachStep.OwnerID= Label.ROC_Officer_Queue;
                        eachStep.Step_Notes__c = comments;
                        lstStepDataUpdate.add(eachStep);
                    }
                    else if(String.isNotBlank(Status) && Status =='Rejected'){
                        eachStep.Status__c = Label.Rejected_Step_Status_ID;
                        eachStep.Step_Notes__c = comments;
                        lstStepDataUpdate.add(eachStep);
                    }
                }      
            }
            
            if(lstStepDataUpdate!=null && lstStepDataUpdate.size()>0){
                database.update(lstStepDataUpdate);
            }   
        }
    }
    
    global void finish(database.batchablecontext bc){
    
    }
    
}