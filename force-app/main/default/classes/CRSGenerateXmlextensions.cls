public with sharing class CRSGenerateXmlextensions {

public  CRS_Master__c crsform{get;set;}

    public CRSGenerateXmlextensions(ApexPages.StandardController controller) 
    {
 
        this.crsform= (CRS_Master__c )controller.getRecord();

    }
    
  
     public static void submitMassXML( id crsformId,string Nameform) 
     {
       
        
       // Folder docFolder = [Select Id from Folder where Name='CRS_Data' limit 1];
         
          
          /*  
        Document xmlDoc = new Document();
        xmlDoc.Name = 'Updated CRS '+crsform.Name+' XML - '+System.today().year();
        xmlDoc.Body = targetBlob ;
        xmlDoc.Type = 'txt';
        xmlDoc.FolderId = docFolder.Id;
        insert xmlDoc;
        */
       /* 
    Attachment a = new Attachment();
    a.Body =Blob.valueOf(CRS_generate_XML.generateMassXml(crsformId)) ;
    a.Name='Updated CRS '+Nameform+' XML Mass- '+System.today().year()+'.xml';
    a.parentid=crsformId;
    a.ContentType='.xml';
    
    
    insert a;
    
    */
    CRS_Master__c CRSYear=[select Batch_Size__c from CRS_Master__c where id=:crsformId];
    string Query='select id,Name from CRS_Form__c where Mass_Upload__c=true and Is_Active__c=true and CRS_Year__c=\''+crsformId+'\' ';
    String formYearId=crsformId;
    String formYearName=Nameform;
    
    
    if(test.isrunningtest())
    {
    Query+=' limit 10';
    Id batchInstanceId = Database.executeBatch(new BatchCRSGenerateXml(Query,formYearId,formYearName));
    }
    else
        Id batchInstanceId = Database.executeBatch(new BatchCRSGenerateXml(Query,formYearId,formYearName),Integer.valueOf(CRSYear.Batch_Size__c) );
     

     
     }
    
    public PageReference submitXML() 
     {
       
        
       // Folder docFolder = [Select Id from Folder where Name='CRS_Data' limit 1];
          //  Blob targetBlob = Blob.valueOf(CRS_generate_XML.generateXml(crsform.id));
          
          /*  
        Document xmlDoc = new Document();
        xmlDoc.Name = 'Updated CRS '+crsform.Name+' XML - '+System.today().year();
        xmlDoc.Body = targetBlob ;
        xmlDoc.Type = 'txt';
        xmlDoc.FolderId = docFolder.Id;
        insert xmlDoc;
        */
      
    Attachment a = new Attachment();
    a.Body =Blob.valueOf(CRS_generate_XML.generateXml(crsform.id)) ;
    
    
    a.Name='CRS '+crsform.Name+' XML -'+DateTime.now().minute()+'-'+DateTime.now().second()+'.xml';
    
    a.parentid=crsform.Id;
    a.ContentType='.xml';
    
    
    insert a;
    
     submitMassXML(crsform.Id,crsform.Name) ;
         return new PageReference ('/'+crsform.id);
     
     }

}