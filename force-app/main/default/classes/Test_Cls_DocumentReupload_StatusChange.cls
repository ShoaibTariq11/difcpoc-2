/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_Cls_DocumentReupload_StatusChange {

    static testMethod void myUnitTest() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        
        string conRT;
		for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
        	conRT = objRT.Id;
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.Phone = '1234567890';
        objContact.RecordTypeId=conRT;
        insert objContact;
          
        Product2 objProduct = new Product2();
        objProduct.Name = 'DIFC Product';
        objProduct.IsActive = true;
        insert objProduct;
          
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Application_of_Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Menutext__c = 'Application_of_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        objTemplate.Refund_Item__c = objProduct.Id;
        insert objTemplate;
        
        Step_Template__c objStepType = new Step_Template__c();
        objStepType.Name = 'Approve Request';
        objStepType.Code__c = 'APPROVE_REQUEST';
        objStepType.Step_RecordType_API_Name__c = 'General';
        objStepType.Summary__c = 'Approve Request';
        insert objStepType;
        
        Pricing_Line__c objPricingLine = new Pricing_Line__c();
        objPricingLine.Product__c = objProduct.Id;
        objPricingLine.Active__c = true;
        objPricingLine.Priority__c = 12;
        objPricingLine.Price_Line_Condition__c = '(1 OR 2)';
        insert objPricingLine;
        
        Dated_Pricing__c objDP = new Dated_Pricing__c();
        objDP.Pricing_Line__c = objPricingLine.Id;
        objDP.Unit_Price__c = 1200;
        objDP.Appllication_Type__c = 'Add';
        objDP.Date_From__c = system.today().addYears(-2);
        objDP.Date_To__c = system.today().addYears(2);
        objDP.Use_List_Price__c = true;
        insert objDP;
        
        Pricing_Range__c objPriceRange = new Pricing_Range__c();
        objPriceRange.Dated_Pricing__c = objDP.Id;
        objPriceRange.Range_Start__c = 0;
        objPriceRange.Range_End__c = 2000;
        objPriceRange.Unit_Price__c = 1200;        
        insert objPriceRange;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'AR Third Party NOC';
        insert objDocMaster;
        
        Document_Details__c objDocDetails = new Document_Details__c();
        objDocDetails.Account__c = objAccount.Id;
        objDocDetails.EXPIRY_DATE__c = system.today().addYears(1);
        objDocDetails.DOCUMENT_STATUS__c = 'Active';
        objDocDetails.ISSUE_DATE__c = system.today();
        objDocDetails.DOCUMENT_TYPE__c = 'PIC';
        insert objDocDetails;
        
        list<Status__c> lstStatus = new list<Status__c>();
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Awaiting Approval';
        objStatus.Type__c = 'Start';
        objStatus.Code__c = 'AWAITING_APPROVAL';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Approved';
        objStatus.Type__c = 'Intermediate';
        objStatus.Code__c = 'APPROVED';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Awaiting Re-upload';
        objStatus.Code__c = 'AWAITING_RE_UPLOAD';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Rejected';
        objStatus.Type__c = 'End';
        objStatus.Code__c = 'REJECTED';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Document Re-uploaded';
        objStatus.Type__c = 'End';
        objStatus.Code__c = 'DOCUMENT_RE_UPLOADED';
        lstStatus.add(objStatus);
        
        insert lstStatus;
        
        SR_Steps__c objSRStep = new SR_Steps__c();
        objSRStep.SR_Template__c = objTemplate.Id;
        objSRStep.Step_RecordType_API_Name__c = 'General';
        objSRStep.Step_Template__c = objStepType.Id; // Step Type
        objSRStep.Summary__c = 'Approve Request';
        objSRStep.Step_No__c = 1.0;
        objSRStep.Start_Status__c = lstStatus[0].Id; // Default Step Status
        objSRStep.Active__c = true;
        objSRStep.Sys_Create_Condition__c = true;
        insert objSRStep;
        
        list<SR_Template_Docs__c> lstSRTempDocs = new list<SR_Template_Docs__c>();
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.On_Submit__c = false;
        objTempDocs.Generate_Document__c = true;
        objTempDocs.Original_Sighted_at__c = objSRStep.Id;
        objTempDocs.Added_through_Code__c = false;
        lstSRTempDocs.add(objTempDocs);
        
        objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.On_Submit__c = false;
        objTempDocs.Generate_Document__c = true;
        objTempDocs.Original_Sighted_at__c = objSRStep.Id;
        objTempDocs.Added_through_Code__c = true;
        lstSRTempDocs.add(objTempDocs);
        
        insert lstSRTempDocs;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        
        SR_Status__c objSRStatus;
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Closed';
        objSRStatus.Code__c = 'CLOSED';
        objSRStatus.Type__c = 'End';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Rejected';
        objSRStatus.Code__c = 'REJECTED';
        objSRStatus.Type__c = 'Rejected';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Awaiting Approval';
        objSRStatus.Code__c = 'AWAITING_APPROVAL';
        objSRStatus.Type__c = 'Start';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Approved';
        objSRStatus.Code__c = 'APPROVED';
        objSRStatus.Type__c = 'Intermediate';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Submitted';
        objSRStatus.Code__c = 'SUBMITTED';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Draft';
        objSRStatus.Code__c = 'DRAFT';
        lstSRStatus.add(objSRStatus);
        
        insert lstSRStatus;
        
        list<Transition__c> lstTransitions = new list<Transition__c>();
        Transition__c objTransition = new Transition__c();
        objTransition.From__c = lstStatus[2].Id; // awaiting approval
        objTransition.To__c = lstStatus[4].Id; // approved
        lstTransitions.add(objTransition);
        
        objTransition = new Transition__c();
        objTransition.From__c = lstStatus[1].Id; // approved
        objTransition.To__c = lstStatus[2].Id; // Rejected
        lstTransitions.add(objTransition);
        
        insert lstTransitions;
        
        list<Step_Transition__c> lstStepTransitions = new list<Step_Transition__c>();
        Step_Transition__c objStepTran;
        objStepTran = new Step_Transition__c();
        objStepTran.SR_Step__c = objSRStep.Id;
        objStepTran.SR_Status_Internal__c = lstSRStatus[0].Id;
        objStepTran.SR_Status_External__c = lstSRStatus[0].Id;
        objStepTran.Parent_SR_Status__c = lstSRStatus[0].Id;
        objStepTran.Parent_Step_Status__c = lstStatus[4].Id;
        objStepTran.Transition__c = lstTransitions[0].Id;
        lstStepTransitions.add(objStepTran);
        
        objStepTran = new Step_Transition__c();
        objStepTran.SR_Step__c = objSRStep.Id;
        objStepTran.SR_Status_Internal__c = lstSRStatus[1].Id;
        objStepTran.SR_Status_External__c = lstSRStatus[1].Id;
        objStepTran.Parent_Step_Status__c = lstStatus[4].Id;
        objStepTran.Transition__c = lstTransitions[1].Id;
        lstStepTransitions.add(objStepTran);
        
        insert lstStepTransitions;
        
                 
        Service_Request__c objSRGrandParent = new Service_Request__c();
        objSRGrandParent.Customer__c = objAccount.Id;
        objSRGrandParent.Email__c = 'testsr@difc.com';
        objSRGrandParent.Internal_SR_Status__c = lstSRStatus[5].Id; // changing the status to Draft
        objSRGrandParent.External_SR_Status__c = lstSRStatus[5].Id; // changing the status to Draft
        objSRGrandParent.Contact__c = objContact.Id;
        objSRGrandParent.SR_Template__c = objTemplate.Id;
        RecursiveControlCls.isUpdatedAlready=true;
        insert objSRGrandParent;
          
        Step__c objStepGrandParent1 = new Step__c();
        objStepGrandParent1.SR__c = objSRGrandParent.Id;
        objStepGrandParent1.SR_Step__c = objSRStep.Id;
        objStepGrandParent1.Status__c = lstStatus[2].Id;
        RecursiveControlCls.isUpdatedAlready=true;
        insert objStepGrandParent1;
        
        system.test.startTest(); 
        Service_Request__c objSRParent = new Service_Request__c();
        objSRParent.Parent_SR__c = objSRGrandParent.Id;
        objSRParent.Parent_Step__c = objStepGrandParent1.Id;
        objSRParent.Customer__c = objAccount.Id;
        objSRParent.Email__c = 'testsr@difc.com';
        objSRParent.Internal_SR_Status__c = lstSRStatus[5].Id; // changing the status to Draft
        objSRParent.External_SR_Status__c = lstSRStatus[5].Id; // changing the status to Draft
        objSRParent.Contact__c = objContact.Id;
        objSRParent.SR_Template__c = objTemplate.Id;
        RecursiveControlCls.isUpdatedAlready=true;
        insert objSRParent;
          
        Step__c objStepParent1 = new Step__c();
        objStepParent1.SR__c = objSRParent.Id;
        objStepParent1.SR_Step__c = objSRStep.Id;
        objStepParent1.Status__c = lstStatus[2].Id;
        RecursiveControlCls.isUpdatedAlready=true;
        insert objStepParent1;
          
         
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Email__c = 'testsr@difc.com';
        objSR.Internal_SR_Status__c = lstSRStatus[5].Id; // changing the status to Draft
        objSR.External_SR_Status__c = lstSRStatus[5].Id; // changing the status to Draft
        objSR.Contact__c = objContact.Id;
        objSR.Parent_Step__c = objStepParent1.Id;
        objSR.Parent_SR__c = objSRParent.Id;
        objSR.SR_Template__c = objTemplate.Id;
        RecursiveControlCls.isUpdatedAlready=true;
        insert objSR;
          
        Step__c objStepParent = new Step__c();
        objStepParent.SR__c = objSR.Id;
        objStepParent.SR_Step__c = objSRStep.Id;
        objStepParent.Status__c = lstStatus[2].Id;
        RecursiveControlCls.isUpdatedAlready=true;
        insert objStepParent;


        for(integer i=0;i<lstStepTransitions.size();i++){
          lstStepTransitions[i].SR_Step__c = objSRStep.Id; 
        }
        update lstStepTransitions;
        RecursiveControlCls.isUpdatedAlready=true;
        Cls_DocumentReupload_StatusChange.UpdateStatus(objStepParent.Id, lstStatus[4].Id);
        system.test.stopTest(); 
    }
}