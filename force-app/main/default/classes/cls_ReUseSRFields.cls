public with sharing class cls_ReUseSRFields {
    
    public service_request__c objSR {get; set;}
    
    public cls_ReUseSRFields(ApexPages.StandardController controller){
        string strSRID = Apexpages.currentPage().getParameters().get('Id');
        objSR = new service_request__c();
        objSR = [select id,Registered_Address__c,Approving_Authority__c,Email_Address__c,Sponsor_Street__c,Sponsor_Establishment_No__c,
                        Sponsor_First_Name__c, Sponsor_Last_Name__c, Sponsor_Visa_Expiry_Date__c, Sponsor_Visa_No__c,
                        Sponsor_Mobile_No__c, Sponsor_Nationality__c,Current_Registered_Country__c,Sponsor_P_O_Box__c,Sponsor_Passport_No__c,
                        Sponsor_Office_Telephone__c,Sponsor_Middle_Name__c,Title_Sponsor__c
                 from service_request__c where id=: strSRID];
    }
    
    public cls_ReUseSRFields(){
        
    }
}