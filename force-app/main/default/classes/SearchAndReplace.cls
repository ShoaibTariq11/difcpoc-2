global class SearchAndReplace implements Database.Batchable < sObject > {

    global final String Query;


    global SearchAndReplace(String q) {

        Query = q;
        system.debug('Query---'+Query);
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List < Contact > scope) {
        for (Contact ObjContact: scope) {
            system.debug('ObjContact---'+ObjContact);
            //ObjContact.email.replace('@', '=@');
            ObjContact.email = ObjContact.email != NULl ?ObjContact.email+'.invalid' : ObjContact.email;
            ObjContact.MobilePhone = '+971569803616';
        }
        system.debug('scope---'+scope);
        database.update(scope,false) ;
    }

    global void finish(Database.BatchableContext BC) {}
}