/**************************************************************************************************************
Author      : Swati Sehrawat
Date        : 07/Sep/2014
ClassName   : FO_cls_DocumentViewer
Description :
    -This class is only used for Fit-Out Process 
    -This Class is used to insert the documents based on the type of document.
    -It will display the documents inserted related to the current object and provided option for viewing, editing and deleting the document.
    -The selected document can be viewed in a frame. 
    -Defaulted Folder in which documents has to be uploaded based on the object name     
    ---------------------------------------------------------------------------------------------------------------------
    Modification History
    ---------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    ---------------------------------------------------------------------------------------------------------------------             
    V1.0    09/07/2014  Swati         Ceated
    V2.0    03/23/2016  Claude        Added logic to change redirect logic in portal page
    V2.1    05/25/2016  Claude        Added pagereference redirect when uploading documents
    V2.2    05/29/2016  Claude        Added logic to disable verified checkbox 
    V2.3    05/31/2016  Claude        Added section header for Final Inspection and DCD/DEWA Step
    V2.4    06/03/2016  Claude        Added field in Service Request query to get createdby name
    V2.5    07/17/2016  Claude        Removed 'Employee Services' as part of condition in determining client
    V2.6    07/25/2016  Claude        Added logic for SR Docs that do not have SR Template Docs
    v2.7    08/08/2017  M.Prem          To see the behaviour for Re-upload link
****************************************************************************************************************/
//v2.7 - start - M.Prem
//global without sharing class FO_cls_DocumentViewer { 
//End

global with sharing class FO_cls_DocumentViewer {
     
    public service_request__c objSR                             {get;set;}
    public SR_Doc__c objSRDocEdit                               {get;set;}
    
    public transient Set<String> orderedSteps                               {get;set;}
    public transient map<string,list<sr_doc__c>> mapOfSRDocUploaded         {get;set;}
    public transient map<string,list<sr_doc__c>> mapOfSRDocGenerated        {get;set;}
    
    public List<SelectOption> statusList                        {get;set;}
    
    public string strObjectId                                   {get;set;}
    public string documentManualId                              {get;set;}
    public String selectedStatus                                {get;set;}
    public string strObjectName                                 {get;set;}
    public string userType                                      {get;set;}
    public string FileName                                      {get;set;}
    public string strSRDocId                                    {get;set;}
    public string strdifcComments                               {get;set;}
    public string strExternalComments                           {get;set;}
    public string strAttachmentName                             {get;set;}
    public string strError                                      {get;set;}
    public string AgrDocId                                      {get;set;}
    public string typeOfSR                                      {get;set;}
    public string stepId                                        {get;set;}
    public String stepName                                      {get;set;}
    
    public Attachment document                                  {get;set;}
     
    public boolean isportalUser                                 {get;set;}
    public boolean isIDAMAUser                                  {get;set;}
    public boolean isDIFCUser                                   {get;set;}
    public Boolean isClient                                     {get;set;}
    public Boolean hasSteps                                     {get;set;}
    
    public Boolean isForReupload                                {get;set;}
    
    //V2.3
    public Boolean isDCDDewa                                    {get;set;}
     
    public Boolean isStepClosed                                 {get;set;}
    public Boolean isUploadStepAvailable                        {get;set;}
    public Boolean isSrNotSubmitted                             {get;set;}
    public DateTime stepCreationDate                            {get;set;}
    public Boolean isDcdDewaStep                                {get;set;}
    
    //V2.4 - boolean variables to check service request owner for documents without steps
    public Boolean isOwnerContractor                            {get;set;}
    public Boolean isOwnerClient                                {get;set;}
    public Boolean isInternalUser                               {get;set;}
    public Boolean isLastStep                                   {get;set;}
    public Boolean hasSrKey                                     {get;set;}
    
    //string FrameDisplay       = 'None';
    string ImgDisplay           = 'None';
    //string displayTable           = 'block';
    string DocumentID           = '';
    //string dvDocsVisibility       = '';
    //string selectedFolder     = '';
    //string strDefaultFolder     = '';
    string Imagesrc;
    string serviceRequestId;
     
    public FO_cls_DocumentViewer(){
        init();
    }
    
    /**
     * Initialize variables and collections, and 
     * executes onload methods
     * @author      Claude Manahan
     * @date        06/01/2016
     */
    void init(){
        
        getUrlParameters();
        
        initializeStatusPicklist();
        
        ////setdvDocsVisibility('block');
        
        initializeVariables();
        
        checkCurrentStep();
        
        initializeSrDocsList();
        
        checkPreviousSteps();
        
        checkLastStep();
    }
    
    /**
     * Initialize variables and collections
     * @author      Claude Manahan
     * @date        06/01/2016
     */
    void initializeVariables(){
        
        strdifcComments = '';
        strExternalComments = '';
        typeOfSR = '';
        selectedStatus = '';
        stepName = '';
        
        isSrNotSubmitted = false;
        isPortalUser = false;
        isIDAMAUser = false;
        isDIFCUser = false;
        isStepClosed = true;
        isClient = false;
        hasSteps = false;
        hasSrKey = false;
        //isOwnerInternal = false;
        
        objSR = new service_request__c();
        document = new Attachment();
        
        mapOfSRDocUploaded = new map<string,list<sr_doc__c>>();
        mapOfSRDocGenerated = new map<string,list<sr_doc__c>>();
    }
    
    /**
     * Initializes the status picklist 
     * @author      Claude Manahan
     * @date        06/01/2016
     */
    void initializeStatusPicklist(){
        
        statusList = new List<SelectOption>();
        
        statusList.add(new SelectOption('','--None--'));
        statusList.add(new SelectOption('Re-upload','Re-upload'));
        statusList.add(new SelectOption('Uploaded','Uploaded'));    
    }
    
    /**
     * Checks the last step
     * @author      Claude Manahan
     * @date        06/03/2016
     */
    void checkLastStep(){
        
        List<Step__c> lastSteps = 
                [select id, 
                        Step_Name__c,
                        Status__r.Type__c,
                        Status__c,
                        CreatedDate,Owner__c from Step__c where sr__c=:serviceRequestId 
                        ORDER BY Name DESC];
                        
        
        isLastStep = String.isNotBlank(stepId) && !lastSteps.isEmpty() && lastSteps[0].Id == stepId;
    }
    
    
    /**
     * Checks the current step referenced
     * @author      Claude Manahan
     * @date        06/01/2016
     */
    void checkCurrentStep(){
        
        if(String.isNotBlank(stepId)){
            // Get any step which indicates re-upload
            List<Step__c> relatedStep = [select id, Step_Name__c ,Status__r.Type__c,Status__c,CreatedDate,Owner__c from Step__c where id=:stepId];
            
            stepCreationDate = !relatedStep.isEmpty() ? relatedStep[0].CreatedDate : null;
            
            isStepClosed = !relatedSTep.isEmpty() && String.isNotBlank(relatedStep[0].Status__r.Type__c) && relatedStep[0].Status__r.Type__c.equals('End'); 
            
            isUploadStepAvailable = 
                !(relatedSTep.isEmpty() && String.isNotBlank(relatedStep[0].Status__r.Type__c) &&
                    relatedStep[0].Status__r.Type__c.equals('End')) && 
                    (relatedStep[0].Step_Name__c.equals('Re-upload Document') ||
                        relatedStep[0].Step_Name__c.equals('Upload Revised Documents') 
                        );
                    
            isDcdDewaStep = !(relatedSTep.isEmpty() && String.isNotBlank(relatedStep[0].Status__r.Type__c) &&
                    relatedStep[0].Status__r.Type__c.equals('End')) && 
                    relatedStep[0].Step_Name__c.equals('Notify FOSP about DCD and DEWA inspection dates');
                    
            stepName = !relatedSTep.isEmpty()? relatedStep[0].Step_Name__c : '';
            
        } 
    }
    
    /**
     * Checks if the Service request already has steps
     * @author      Claude Manahan
     * @date        06/01/2016
     */
    void checkPreviousSteps(){
        hasSteps = ![SELECT Id FROM Step__c WHERE SR__c = :serviceRequestId].isEmpty();
    }
    
    /**
     * Gets the URL parameters passed to the page
     * @author      Claude Manahan
     * @date        06/01/2016
     */
    void getUrlParameters(){
        
        serviceRequestId = ApexPages.CurrentPage().GetParameters().Get('id');
        stepId = ApexPages.CurrentPage().GetParameters().Get('stepId');
    }
    
    /**
     * Initializes the SR Doc collection for the VF Page
     * @author      Claude Manahan
     * @original    Swati Sehrawat
     * @date        06/01/2016
     */
    void initializeSrDocsList(){
        
        list<sr_doc__c> tempSRDocUploaded = new list<sr_doc__c>();
        list<sr_doc__c> tempSRDocGenerated = new list<sr_doc__c>();
        
        String srId = serviceRequestId;
        
        if(string.isNotBlank(srId)){
            
            objSR = initializeServiceRequest(srId);
            tempSRDocUploaded = initiliazeSRDoc(srId, false);
            tempSRDocGenerated = initiliazeSRDoc(srId, true);
            
            if(!tempSRDocUploaded.isEmpty()){
                mapOfSRDocUploaded = initializeMapofSRDoc(tempSRDocUploaded);
            }
            if(!tempSRDocGenerated.isEmpty()){
                mapOfSRDocGenerated = initializeMapofSRDoc(tempSRDocGenerated);
            }
            
            sortSteps(mapOfSRDocUploaded); // Sort the steps
            
            hasSrKey = mapOfSRDocUploaded.containsKey(objSr.Name);
            
            checkUserAccessLevel();
            
            if(isportalUser){
                documentManualId = relatedDocumentManual();
            }
        }
    }
     
    /*public void setdisplayTable(string value){
        displayTable=value;
    }

    public string getdisplayTable(){
        return displayTable;
    }*/
       
    /*public void setFrameDisplay(string value){
        FrameDisplay=value;
    }
       
    public string getFrameDisplay(){
        return FrameDisplay;
    }*/
       
    public void setImgDisplay(string value){
        ImgDisplay=value;
    }
       
    public string getImgDisplay(){
        return ImgDisplay;
    }
       
    public void setDocumentID(string value){
        DocumentID=value;
    }
       
    public string getDocumentID(){
        return DocumentID;
    }
       
    public void setImagesrc(string value){
        Imagesrc=value;
    }
       
    public string getImagesrc(){
        return Imagesrc;
    }
    
    /*public string getdvDocsVisibility(){
        return dvDocsVisibility;
    }
        
    public void //setdvDocsVisibility(string strDoc){
        dvDocsVisibility = strDoc; 
    } */ 
     
    public service_request__c initializeServiceRequest(string srId){
        
        service_request__c objSR = new service_request__c();
        
        objSR = [select id,
                        External_Status_Name__c,
                        NOC_for__c,
                        Name,
                        Owner.Name,
                        CreatedBy.ContactId,
                        Createdby.contact.role__c from service_request__c where id=:srId]; // V2.3 - Claude - Added NOC_for__c field for Final Inspection and DCD/DEWA Step
        
        //V2.3 - Claude - Start - Checks if the Service request has DCD/DEWA
        
        Set<String> dcdValues = new Set<String>{'DCD','DEWA','DCD and DEWA'}; 
        
        isDCDDewa = String.isNotBlank(objSR.NOC_for__c) && (dcdValues.contains(objSR.NOC_for__c));
        isSrNotSubmitted = String.isNotBlank(objSR.External_Status_Name__C) && objSR.External_Status_Name__C.equals('Draft');
        
        //V2.3 - Claude - End
        
        //V2.4 - Claude - Start
        
        Set<String> contactRoleList = String.isNotblank(objSR.CreatedBy.ContactId) && 
                                        String.isNotBlank(objSr.CreatedBy.Contact.Role__c) ?
                                         new Set<String>(objSr.CreatedBy.Contact.Role__c.split(';')) : new Set<String>(); 
        
        /* Check if the Service Request Owner roles */
        isOwnerContractor = String.isNotblank(objSR.CreatedBy.ContactId) && 
                            String.isNotBlank(objSr.CreatedBy.Contact.Role__c) &&
                            (contactRoleList.contains('Fit-Out Services') || 
                            contactRoleList.contains('Event Services')); 
                            
        isOwnerClient = String.isNotblank(objSR.CreatedBy.ContactId) && 
                            String.isNotBlank(objSr.CreatedBy.Contact.Role__c) &&
                            contactRoleList.contains('Company Services');
                            
        //isOwnerInternal = String.isBlank(ObjSr.CreatedBy.ContactId);
        
        //V2.4 - claude - End
        
        return objSR;
    }
     
     public list<sr_doc__c> initiliazeSRDoc(string srId, boolean generated){
        
        list<sr_doc__c> tempSRDocList = new list<sr_doc__c>();
        
        tempSRDocList = [select id,createddate,Is_Not_Required__c,SR_Template_Doc__c,
                         SR_Template_Doc__r.Validate_In_Code_at_Step_No__c,lastmodifieddate,
                         SR_Template_Doc__r.Validate_In_Code_at_Step_No__r.Summary__c,
                         Step__r.Status_Code__c,
                         Requirement__c,Document_Description_External__c,Service_Request__c,
                         Service_Request__r.Internal_Status_Name__c,Service_Request__r.name,
                         Service_Request__r.External_Status_Name__c,name,Doc_ID__c,
                         Received_Physically__c,IDAMA_External_Comments__c,IDAMA_internal_Comments__c,
                         contractor_Comments__c,difc_Comments__c,Document_Type__c,
                         Status__c,Preview_Download_Document__c,
                         Step__r.Owner.Name,
                         Step__r.Owner__c,
                         Step__r.Status__r.Type__c,
                         Step__r.Step_Name__c,
                         Step__c,
                         Re_Upload_date__c,
                         Original_Verified__c,
                         Sys_IsGenerated_Doc__c
                  from SR_Doc__c 
                  where Service_Request__c=:srId 
                  and Sys_IsGenerated_Doc__c=:generated 
                  order by Is_Not_Required__c, name asc NULLS FIRST]; 
                    //Original_Verified__c
        return tempSRDocList;
     }

     public map<string,list<sr_doc__c>> initializeMapofSRDoc(List<sr_doc__c> listSRDoc){
        
        map<string,list<sr_doc__c>> mapOfSRDoc = new map<string,list<sr_doc__c>>();
        
        for(sr_doc__c sa : listSRDoc){         
            
            system.debug('-----sa---'+sa);
            
            if(sa.SR_Template_Doc__r.Validate_In_Code_at_Step_No__c!=null){
                
                System.debug('Mapped based on step' + sa.Name);
                
                if(mapOfSRDoc.containsKey(sa.SR_Template_Doc__r.Validate_In_Code_at_Step_No__r.Summary__c)){
                    List<sr_doc__c> SAList = mapOfSRDoc.get(sa.SR_Template_Doc__r.Validate_In_Code_at_Step_No__r.Summary__c);
                    SAList.add(sa);       
                    mapOfSRDoc.put(sa.SR_Template_Doc__r.Validate_In_Code_at_Step_No__r.Summary__c,SAList);
                }
                else{
                    List<sr_doc__c> SAList = new List<sr_doc__c>();
                    SAList.add(sa);
                    mapOfSRDoc.put(sa.SR_Template_Doc__r.Validate_In_Code_at_Step_No__r.Summary__c,SAList);
                }
                
            } else if(String.isNotBlank(sa.Step__c) && !sa.Sys_IsGenerated_Doc__c){ //V2.6 - Claude - Additional logic for SR Template Docs
                
                System.debug('Mapped based on step' + sa.Name);
                
                if(mapOfSRDoc.containsKey(sa.Step__r.Step_Name__c)){
                    List<sr_doc__c> SAList = mapOfSRDoc.get(sa.Step__r.Step_Name__c);
                    SAList.add(sa);       
                    mapOfSRDoc.put(sa.Step__r.Step_Name__c,SAList);
                }
                else{
                    List<sr_doc__c> SAList = new List<sr_doc__c>();
                    SAList.add(sa);
                    mapOfSRDoc.put(sa.Step__r.Step_Name__c,SAList);
                }
            }
            
            else{
                System.debug('Mapped based on Service Request Name' + sa.Name);
                if(mapOfSRDoc.containsKey(sa.Service_Request__r.name)){
                    List<sr_doc__c> SAList = mapOfSRDoc.get(sa.Service_Request__r.name);
                    SAList.add(sa);       
                    mapOfSRDoc.put(sa.Service_Request__r.name,SAList);
                }
                else{
                    List<sr_doc__c> SAList = new List<sr_doc__c>();
                    SAList.add(sa);
                    mapOfSRDoc.put(sa.Service_Request__r.name,SAList);
                }
            }
            system.debug('---mapOfSRDoc---'+mapOfSRDoc.keySet());
        }
        
        system.debug('---mapOfSRDoc---'+mapOfSRDoc.keySet());
        return mapOfSRDoc;
    }
     
    void sortSteps(map<string,list<sr_doc__c>> mapOfSRDoc){ 
        
        // Query the steps by the name associated, and order them by step number
        List<Step__c> associatedSteps = [SELECT Name,Step_Name__c FROM Step__c WHERE SR__c = :serviceRequestId AND Step_Name__c in :mapOfSRDoc.keySet() ORDER BY Name DESC];
        
        orderedSteps = new Set<String>();
        
        if(!associatedSteps.isEmpty()){
            
            // Build the step map
            for(Step__c s : associatedSteps){
                System.debug('Step Name: ' + s);
                orderedSteps.add(s.Step_Name__c);
            }
            
        }
    }
     
    /**
     * Checks the User Access level
     * @author      Swati Sehrawat
     */ 
    public void checkUserAccessLevel(){
        list<User> curUser = [select id,ContactId,contact.role__c,ProfileId,profile.name,Profile.UserLicenseId,
                                     Profile.UserLicense.Name,Profile.UserLicense.LicenseDefinitionKey 
                              from User where Id=:userInfo.getUserId()];
        
        if(!curUser.isEmpty()){
            
            if(curUser[0].ContactId!=null){
                isPortalUser = true;
                userType = 'Community';
                /*if(curUser[0].Contact.Role__c == 'Event Services'){
                    typeOfSR = 'Event';
                }
                else if(curUser[0].Contact.Role__c == 'Fit-Out Services'){
                    typeOfSR = 'Fit-Out';
                }*/
                typeOfSR = curUser[0].Contact.Role__c == 'Event Services' ? 'Event' : 
                            curUser[0].Contact.Role__c == 'Fit-Out Services' ? 'Fit-Out' : '';
                
                isClient = String.isNotBlank(curUser[0].Contact.Role__c) && curUser[0].Contact.Role__c.contains('Company Services');
            } else{
                isPortalUser = false;
                userType = 'salesforce';
            }
            
            if(curUser[0].profile.name.contains('DIFC')){
                isDIFCUser = true;
            }
            
            if(curUser[0].profile.name.contains('FOSP') || curUser[0].profile.name.contains('System Administrator')){
                isIDAMAUser = true;
            }
            
            //V2.4 - Claude - Start
            isInternalUser = String.isBlank(curUser[0].contactId);
            //V2.4 - Claude - End
            
        }
     }
    
    /**
     * Gets the related document manual
     * @author      Swati Sehrawat
     */ 
    public string relatedDocumentManual(){
        
        string DocumentId='';
        
        if(typeOfSR == 'Event'){
            list<document> manual = [select id from document where DeveloperName=:'DIFC_Fit_Out_Manual'];   
            if(!manual.isempty()){
                DocumentId =  manual[0].id;
            }
        } else if(typeOfSR == 'Fit-Out'){
            list<document> manual = [select id from document where DeveloperName=:'DIFC_Event_Manual'];   
            if(!manual.isempty()){
                DocumentId =  manual[0].id;
            }
        }
        
        return DocumentId;
    }
    
    /**
     * Uploads the SR Doc with the selected attachment
     * @author      Swati Sehrawat
     */   
    public PageReference Upload() { //V2.1 - Claude <- changed from 'void' to 'Pagereference' return type
        
        system.debug('----document----'+document);
        
        if(objSR!=null){
            
            SR_Doc__c objRA = new SR_Doc__c();    
            
            try {
                document.Name = FileName;
                
                if(FileName!=null && FileName.indexof('.')>-1 && FileName.SubString(FileName.indexof('.')+1,FileName.length())!=''){
                    string contyp = FileName.SubString(FileName.indexof('.')+1,FileName.length());
                    if(contyp!=null && contyp.toLowerCase()=='pdf'){
                        document.ContentType = 'application/pdf';
                    }
                }
                
                if(document!=null){
                    if(document.body!=null){
                        
                        document.parentId = strSRDocId;
                        upsert document;//Inserting the document 
                        
                        /*Creating the related document attachment record */
                        objRA.Id = strSRDocId;
                        
                        if(document.Id!=null){
                            objRA.Doc_ID__c = string.valueOf(document.Id);
                            
                            //V2.3 - Start - Claude
                            
                            /* Query the original SR Doc and Check is status. If it's for Re-upload, set the re-upload date */
                            SR_Doc__c orgSrDoc = [SELECT Status__c, (SELECT Id, Name FROM Attachments) FROM SR_Doc__c WHERE Id = :strSRDocId];
                            
                            Boolean isForReupload = ( String.isNotBlank(orgSrDoc.Status__c) && (orgSrDoc.Status__c.equals('Re-upload') || orgSrDoc.Status__c.equals('Re-uploaded') ) ) 
                                                || (!orgSrDoc.attachments.isEmpty() && orgSrDoc.attachments.size() > 1) ;
                            
                            if(isForReupload){
                                objRA.Re_Upload_date__c = Datetime.now();
                            }
                            
                            //V2.3 - End - Claude
                            objRA.Status__c = 'Uploaded';
                            
                        }
                        
                        if(strdifcComments!=null && strdifcComments!='' && strdifcComments.trim()!='')
                            objRA.difc_Comments__c = strdifcComments;
                            
                        if(strExternalComments!=null && strExternalComments!='' && strExternalComments.trim()!='')
                            objRA.contractor_Comments__c = strExternalComments;
                            
                        if(objSR!=null){
                            objRA.Service_Request__c = objSR.id;
                            objRA.Document_Type__c = document.ContentType;
                            
                            try{
                                update objRA;
                            }catch(Exception e){
                                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
                            }
                        }
                        
                        setDocumentID(string.valueof(document.Id));
                        
                        if(document != null){ 
                       
                            /*The below If block checks for whether the file format is valid format or not*/
                            if(FileName!=null && FileName!=''){
                                
                                if(FileName.SubString(FileName.indexof('.')+1,FileName.length())=='jpg'|| FileName.SubString(FileName.indexof('.')+1,FileName.length())=='gif'|| FileName.SubString(FileName.indexof('.')+1,FileName.length())=='bmp'|| FileName.SubString(FileName.indexof('.')+1,FileName.length())=='png'){
                                    setImgDisplay('none');
                                    // setFrameDisplay('none');                            
                                }
                           
                                if(FileName.SubString(FileName.indexof('.')+1,FileName.length())=='rtf' || FileName.SubString(FileName.indexof('.')+1,FileName.length())=='tif' || FileName.SubString(FileName.indexof('.')+1,FileName.length())=='TIF' || FileName.SubString(FileName.indexof('.')+1,FileName.length())=='doc'|| FileName.SubString(FileName.indexof('.')+1,FileName.length())=='docx'|| FileName.SubString(FileName.indexof('.')+1,FileName.length())=='ppt'|| FileName.SubString(FileName.indexof('.')+1,FileName.length())=='pptx' || FileName.SubString(FileName.indexof('.')+1,FileName.length())=='xls'|| FileName.SubString(FileName.indexof('.')+1,FileName.length())=='xlsx'){
                                    setImagesrc('');
                                    setImgDisplay('none');
                                    // setFrameDisplay('none');                                    
                                }
                                                                                                                  
                                if(FileName.SubString(FileName.indexof('.')+1,FileName.length())=='pdf' || FileName.SubString(FileName.indexof('.')+1,FileName.length())=='txt' ){
                                    setImgDisplay('none');
                                    // setFrameDisplay('block');                               
                                }
                            }
                            
                            //setdvDocsVisibility('block');//for visibility of dvDocs div
                            // setdisplayTable('block');            
                        }
                    } else {
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error: Please specify a file to upload.'));
                        //setdvDocsVisibility('none');//for hiding of dvDocs div
                        // setdisplayTable('none');
                    }
                }
            } catch (DMLException e) {//To catch the DML exception while uploading the file
                    
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
                //setdvDocsVisibility('block');//for hiding of dvDocs div
                // setdisplayTable('block');
                //document = new Attachment();
                
            } /*finally {
                document = new Attachment();
            }*/
        }else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'There is no Object Id'));   
        }
        
        document = new Attachment(); // This will always re-initialize
        
        return returnToPage();
    }
    
    /**
     * Refreshes the page with the added parameters prior to redirect
     * @author      Claude Manahan
     */ 
    PageReference returnToPage(){
        
        //V2.1 - Start - Added 25-5-2016 - Claude
        String serviceRequestId = ApexPages.CurrentPage().GetParameters().Get('id');
        String serviceRequestStepId = ApexPages.CurrentPage().GetParameters().Get('stepId');
        
        String returl = '/apex/FO_DocumentViewer?id='+serviceRequestId;
        
        if(String.isNotBlank(serviceRequestStepId)){
            returl += '&stepId=' + stepId; 
        }
        
        Pagereference uploadRedirect = new PageReference(returl);
        uploadRedirect.setRedirect(true);
        
        return uploadRedirect;
        //V2.1 - End - Claude
    }
    
    /**
     * Queries the SR Doc for revisions
     * @author      Swati Sehrawat
     */  
    public void EditDoc(){
        
        objSRDocEdit = new SR_Doc__c();
        
        strError='';
        
        if(String.isNotBlank(AgrDocId)){
            
            // Query the SR Doc 
            list<SR_Doc__c> lstSRDoc = [select id,Name,contractor_Comments__c,difc_Comments__c,Doc_ID__c,
                                               Document_Type__c,Original_Verified__c,
                                               Status__c, idama_external_comments__c,idama_internal_comments__c 
                                        from SR_Doc__c where Id=:AgrDocId];
            
            if(lstSRDoc!=null && lstSRDoc.size()>0){
                objSRDocEdit = lstSRDoc[0];
                selectedStatus = lstSRDoc[0].Status__c; // set the selected status
            }
        }
        
        disableVerifiedCheck(); 
    }
    
    /**
     * Saves any changes to the SR Doc
     * @author      Swati Sehrawat
     */ 
    public void SaveDoc(){
        
        try{
            if(objSRDocEdit!=null && objSRDocEdit.id!=null){
                
                strError = '';
                
                objSRDocEdit.Status__c = selectedStatus;
                
                objSRDocEdit.Original_Verified__c = String.isNotBlank(selectedStatus) && selectedStatus.equals('Re-upload') ? false : objSRDocEdit.Original_Verified__c;
                
                update objSRDocEdit;
            }
        }catch(Exception e){
            System.debug('The error ==> ' + e.getMessage() );
        }
    }
    
    /**
     * Disables the 'Original Verified' checkbox whenever the status is set to 'Re-upload' 
     * @author      Claude Manahan
     */ 
    public void disableVerifiedCheck(){
        isForReupload = String.isNotBlank(selectedStatus) && selectedStatus.equals('Re-upload');
        objSRDocEdit.Original_Verified__c = String.isNotBlank(selectedStatus) && selectedStatus.equals('Re-upload') ? false : objSRDocEdit.Original_Verified__c;
    }
     
    /**
     * Redirects the user back to the Service Request/SR Page
     * @author      Swati Sehrawat
     * @return      pg          the page reference of the service request/step
     */ 
    public PageReference Back_To_SR(){
        
        //V1.1 - Start - Claude - added logic if page is accessed using portal
        
        /* 
         * If the page is a portal, return the full url.
         * Else, return with simple url 
         */
         
        Boolean isPortal = URL.getCurrentRequestUrl().toExternalForm().contains('customers');
        
        /* 
         * Build the url accordingly
         */
        String defaultUrl = isPortal ? 'https://' + ApexPages.currentPage().getHeaders().get('Host') + '/customers/' : '/';
        String retId = String.isNotBlank(stepId) ?  stepId : objSR.id;
        String retUrl = defaultUrl + retId;
        
        pagereference pg = new pagereference(retUrl);
        
        System.debug('The reference: ' + pg);
        
        pg.setRedirect(false);
        return pg;
        
        //V1.1 - End
    }
}