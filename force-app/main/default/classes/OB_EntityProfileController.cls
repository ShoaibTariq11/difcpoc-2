/*
 *  get account information of loggedIn user
 **/
public without sharing class OB_EntityProfileController {
    


    @AuraEnabled public static RespondWrap getAccountDetail() {

    
        RespondWrap respWrap = new RespondWrap();
        User loggedInUser  = new User();

        // query user information
        for(User userOBj :[Select contactid, Contact.AccountId,Contact.Account.Name,Contact.Account.Is_Commercial_Permission__c, 
                       contact.Account.RecordType.DeveloperName, contact.Account.BP_No__c,
                       contact.Contractor__c
                       from User where Id = :UserInfo.getUserId()]) {
                            loggedInUser = userOBj;
                            respWrap.userObj = userOBj;
                         }
            
            //string bPnumber = loggedInUser.Contact.Account.BP_No__c;
                
                if(loggedInUser.Contact.AccountId != null) {
                    respWrap.accountId = loggedInUser.Contact.AccountId;
                    if(!(loggedInUser.Contact.Account.Is_Commercial_Permission__c == 'Yes')){
                        respWrap.EmployeeRelationships = EmployeeRelationshipDetails(respWrap.accountId);

                    }
                    
                    respWrap.CompanyRelationships = PrimaryRelationshipDetails(respWrap.accountId);
                    respWrap.dataProtectionData = getpermitlist(respWrap.accountId);
                    if(loggedInUser.Contact.Account.BP_No__c != null){
                        respWrap.visaQuotaInfo = AccountBalanceInfoCls.getPortalBalance(loggedInUser.Contact.Account.BP_No__c);
                        respWrap.visaQuotaInfoJSON = respWrap.visaQuotaInfo.split('\n');
                    }                   
                }
                            
        return respWrap;
    }



     public static relationshipWrapper EmployeeRelationshipDetails(string accountID) {

        relationshipWrapper respWrap = new relationshipWrapper();


        for( Relationship__c relObj :  [select Id, Name, End_Date__c, Relationship_Group__c, Subject_Account__c, Object_Account__c, Object_Contact__c, Object_Account__r.Name, Object_Contact__r.Name, Object_Contact__r.Visa_Expiry_Date__c, Object_Contact__r.Is_Absconder__c, Object_Contact__r.Is_Violator__c, Object_Contact__r.Active_Employee__c, Active__c, Relationship_Type_formula__c from Relationship__c where(End_Date__c > TODAY OR(Active__c = TRUE AND Relationship_Type__c = 'Has DIFC Sponsored Employee')) AND Relationship_Group__c = 'GS' AND Subject_Account__c = :accountID order by Name]){
            respWrap.RelationshipsList.add(relObj);
            respWrap.listSize += 1;
        }

        return respWrap;



    } 

     public static relationshipWrapper PrimaryRelationshipDetails(string accountID) {

        relationshipWrapper respWrap = new relationshipWrapper();


        for( Relationship__c relObj :[select Id, Name, End_Date__c, Start_Date__c, Relationship_Group__c, Subject_Account__c, Object_Account__c, Object_Contact__c, Object_Account__r.Name, Object_Contact__r.Name, Active__c, Relationship_Type_formula__c from Relationship__c where Relationship_Group__c = 'ROC' AND Subject_Account__c = :accountID order by Name]){
            respWrap.RelationshipsList.add(relObj);
            respWrap.listSize += 1;

        }

return respWrap;


    } 

public static List<Permit__c> getpermitlist(string accountID) {
        List<Permit__c> PermitList = 
            [SELECT id,Name,Permit_Type__c,(select Id,Name,Name_of_Jurisdiction__c,RecordType.Name from Data_Protections__r) FROM Permit__c WHERE  Active__c=True and Account__c=:accountID order by Permit_Type__c];
        //V1.5
        if(PermitList == null || PermitList.isEmpty()){
            PermitList = new list<Permit__c>();
            Date dDate;
            for(Permit__c objP : [select Id,Name,Permit_Type__c,Date_To__c,(select Id,Name,Name_of_Jurisdiction__c,RecordType.Name from Data_Protections__r) FROM Permit__c where Account__c=:accountID order by Date_To__c desc]){
                if(dDate == null || dDate == objP.Date_To__c){
                    dDate = objP.Date_To__c;
                    PermitList.add(objP);
                }else
                    break;
            }           
        }
           return PermitList ;
    }
    


    public class RespondWrap {
        @AuraEnabled public account accountObj { get; set; }
        @AuraEnabled public user userObj { get; set; }
        @AuraEnabled public string accountId { get; set; }
        @AuraEnabled public relationshipWrapper CompanyRelationships { get; set; }
        @AuraEnabled public relationshipWrapper EmployeeRelationships { get; set; }
        @AuraEnabled public List<Permit__c> dataProtectionData  { get; set; }
        @AuraEnabled public string visaQuotaInfo  { get; set; }
        @AuraEnabled public list<string> visaQuotaInfoJSON  { get; set; }
        public RespondWrap() {
            CompanyRelationships = new relationshipWrapper();
            EmployeeRelationships = new relationshipWrapper();
            dataProtectionData = new List<Permit__c> ();
            visaQuotaInfoJSON = new list<String>();
        }
    }

        public class relationshipWrapper {
        @AuraEnabled public list<Relationship__c> RelationshipsList { get; set; }
        @AuraEnabled public integer listSize { get; set; }
        public relationshipWrapper() {
            RelationshipsList = new list<Relationship__c>();
            listSize = 0;
        }
    }

    

}