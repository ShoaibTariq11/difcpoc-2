public without sharing class AcceptCaseController{

    @AuraEnabled
    public static Case getCase(Id caseId) {
        return [SELECT IsClosed,OwnerId,Status FROM Case WHERE Id = :caseId];
    }
    
    @AuraEnabled
    public static string invalidCase(Id caseId) {
        Case caseObj = new Case(
            Id=caseId,
            Status='Invalid',
            GN_Type_of_Feedback__c = 'Invalid'
        );
        try{
            update caseObj;
        }
        catch(System.DmlException ex){
            return ex.getDmlMessage(0);
        }
        return 'The Case was marked as Invalid successfully';
    }
    
    @AuraEnabled
    public static string saveCaseWithOwner(Id caseId) {
        boolean hasAccess = false;
        string CaseOwnerId;
        for(Case caseRec : [SELECT OwnerId FROM Case WHERE Id = :caseId]){
            CaseOwnerId = caseRec.OwnerId;
        }
        System.debug('CaseOwnerId '+CaseOwnerId );
        System.debug('userinfo.getuserid()'+userinfo.getuserid());
        if(CaseOwnerId!=null && CaseOwnerId.startsWith('005')){
            if(CaseOwnerId == userinfo.getuserid()){
                hasAccess = true;
            }else{
                
                return 'Case is already accepted by another user.';
            }
      }else if(CaseOwnerId!=null && CaseOwnerId.startsWith('00G')){
        for(GroupMember GrpMem:[select Id,GroupId,UserOrGroupId from GroupMember where GroupId=:CaseOwnerId and UserOrGroupId=:userinfo.getuserid()]){
              hasAccess = true;
          }
      }
        if(hasAccess){
            Case caseObj = new Case(
                    Id=caseId,
                    OwnerId=UserInfo.getUserId(),
                    Status='In Progress'
                    );
            //CaseTriggerHelper.OwnerChangeFromButton=false;
            try{
            	update caseObj;
            }
            catch(System.DmlException ex){
                return ex.getDmlMessage(0);
            }
            return 'Case was assigned to you successfully';
        }
        else
            return 'You are not able to accept this Case since you do not belong to this queue.';
        /*
        contact.AccountId = accountId;
        upsert contact;
        return contact;
        */
    }
    @AuraEnabled
    public static String getUserId() {
        return UserInfo.getUserId();
    }

}