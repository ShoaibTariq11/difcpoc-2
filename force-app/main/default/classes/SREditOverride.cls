/**
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.1    11-4-2016   Claude        Added logic to check if verify step for idama is approved
    V1.2    18-4-2016   Claude        Revised logic from 'Completed' to 'Submitted'
    V1.3    08-5-2016   Claude        Added 'Project_On_Hold_due_to_Cancellation__c' field as part of validation
    V1.4    18-5-2016   Claude        Added 'Request for Technical Clarification Form' type to prevent DIFC users from editing the form
    v1.5    29/06/2016  Sravan        Display error message when the user from the GS Team ReadOnly Tkt #3051
    v1.6    25-8-2016   Claude        Added exemption for DIFC users for Technical Clarification Form   
    V1.7    06-09-2016  Swati         Added logic for Comfort letter
    v1.8    20/08/2016  Ravi          Added the redirection condition for Multiplese Lease Registration
    V1.9    01-9-2016   Claude        Added new 'Request for Contractor Access' page as part of redirect
    V2.0    12-3-2017   Claude        Added new Custom page for type 'PO BOX' for GS (3022); removed legacy commented code
    V2.1    06/Nov/2018  Arun        Added Code allow Admin to defind VF page on SR template 
**********************************************************************************************************************/
public without sharing class SREditOverride {
    
    public Service_Request__c ServiceRequest            {get;set;}
    public Boolean BlockUser                            {get;set;}
    public Boolean foSRCompleted                        {get;set;}
    public Boolean foOnHold                             {get;set;}
    public string ReturnURL                             {get;set;}
    public String errorMsg                              {get;set;}
    
    
    private String profileName;
    
    public SREditOverride(Apexpages.Standardcontroller con){
        
        ServiceRequest = new Service_Request__c();
        ServiceRequest = (Service_Request__c)con.getRecord();
        
        system.debug('ServiceRequest is : '+ServiceRequest);
        
        ReturnURL = Apexpages.currentPage().getParameters().get('retURL');
        
        if(ReturnURL == null || ReturnURL == ''){
            ReturnURL = Site.getpathprefix()+'/'+ServiceRequest.Id;
        }
        
        BlockUser = false;
        foSRCompleted=false;
        foOnHold=false;
        errorMsg= '';
        
        profileName = '';
        
        system.debug('--ReturnURL----'+ReturnURL);
    }
    
    public Pagereference VerifyRequest(){
        
        /*
         * Set the current user profile
         */
        getUserProfile();

        /* Query the service request */     
        if(String.isNotBlank(ServiceRequest.Id)){
            getServiceRequestDetails(); 
        }
        
        PageReference validateReference;
        
        if(ServiceRequest !=null){
                         
            /* START OF CUSTOM SR Validations and Navigation Logic */
                         
            validateReference = checkGsReadOnly();  // Since this is the first validation, no need to add a condition to fire
            
            /* Notes: 
             * 1. By adding '!hasMessage()', we check if a message has already been set due to a validation to prevent the other validations from firing
             * 2. By adding 'validateReference == null', we check if the validations did not return any reference
             */
        
            if(!hasMessage() && validateReference == null) validateReference = checkSrDraftStatus();
        
            if(!hasMessage() && validateReference == null) validateReference = checkFitOutCompleted();
            
            if(!hasMessage() && validateReference == null) validateReference = checkFitOutOnHold();
            
            if(!hasMessage() && validateReference == null) validateReference = checkFitOutInduction();
            
            if(!hasMessage() && validateReference == null) validateReference = checkRorp();
            
            if(!hasMessage() && validateReference == null) validateReference = checkContractorAccess();
            
            if(!hasMessage() && validateReference == null) validateReference = gsNavigation();
            
            //2.1
            if(!hasMessage() && validateReference == null) validateReference = SrNavigation();
            
            
            /* NOTE: If you need add more validations or other navigation behavior,
             *       put them inside a method, and call them within this Page Action.
             */
             
            /* END OF CUSTOM SR Validations and Navigation Logic */ 
            
        }
        
        /* DEFAULT REDIRECT; DO NOT TOUCH */
        if(validateReference == null && !hasMessage()) validateReference = new Pagereference(Site.getpathprefix()+'/'+ServiceRequest.Id+'/e?retURL='+Apexpages.currentPage().getParameters().get('retURL')+'&nooverride=1');
        
        return validateReference;
    }
    
    /**
     * Dedicated navigation logic for GS
     */
    private Pagereference checkGsReadOnly(){
        
         if(Userinfo.getUserType().equals('Standard')){        
            
            // V1.5
            if(ServiceRequest !=null && ServiceRequest.SR_Group__c == 'GS' && 
                (ServiceRequest.External_Status_Name__c.equals('Submitted') || 
                    ServiceRequest.Internal_Status_Name__c == 'Submitted' || 
                    ServiceRequest.Submitted_Date__c !=null)){
                        
                set<id> gsTeamReadOnlyGrp = new set<id>();
                
                List<Group> groupId = new List<Group>{[select id from Group where DeveloperName = 'GS_Team_Read_Only']};
                
                for(GroupMember gm :[select UserOrGroupId from GroupMember where GroupId = :groupId]){
                    gsTeamReadOnlyGrp.add(gm.UserOrGroupId);
                }
                
                if(gsTeamReadOnlyGrp.contains(Userinfo.getUserId())){
                    setErrorMessage('You are not authorized to edit the service request post submission');
                }
            }
            // V1.5
        }
        
        return null;
    }
    
    /**
     * Checks if the SR is still in DRAFT
     */
    private PageReference checkSrDraftStatus(){
        
        if(Userinfo.getUserType() != 'Standard'){
        
            if(ServiceRequest.External_Status_Name__c != 'Draft'){
                setErrorMessage('This request is already sumitted and cannot be edited.');
            }
        }
        
        return null;
        
    }
    
    private Pagereference checkFitOutCompleted(){
        
        if(ServiceRequest!=null && ServiceRequest.SR_Group__c == 'Fit-Out & Events' && ServiceRequest.External_SR_Status__r.Type__c == 'End'){
                
            if(profileName!='System Administrator'){
                
                foSRCompleted = true;
                
                setErrorMessage('This request is Completed and cannot be edited.');
            }
            
        }
        
        return null;
    }
    
    private Pagereference checkFitOutOnHold(){
    
        if(ServiceRequest!=null && ServiceRequest.SR_Group__c == 'Fit-Out & Events'){
                
            if(profileName!='System Administrator'){
                
                foOnHold = true;
                
                if(ServiceRequest.Project_on_hold__c == true && !(ServiceRequest.Record_Type_Name__c.contains('Site_Visit'))){
                    setErrorMessage('This request is on hold and cannot be edited.');
                } 
                
                if(ServiceRequest.Project_On_Hold_due_to_Cancellation__c == true){
                    setErrorMessage('This request is on hold due to cancellation and cannot be edited.');
                }
                
                
            }
        }
        
        return null;
    }
    
    private Pagereference checkFitOutInduction(){
        
         Set<String> validInductionStatus = new Set<String>{'Induction Rescheduled','Induction Completed','Induction Scheduled'}; // V1.2 - Claude - Added 'Induction Scheduled'
            
        //V1.1 - Start - Claude
        if(ServiceRequest != null && !validInductionStatus.contains(ServiceRequest.External_Status_Name__c) && ( ServiceRequest.Record_Type_Name__c.equals('Event_Induction_Request') || ServiceRequest.Record_Type_Name__c.equals('Fit_Out_Induction_Request') )  ){
            setErrorMessage('Please complete the induction before updating the request.');
        }
        //V1.1 - End
        
        return null;
    }
    
    private PageReference checkContractorAccess(){
        
        // V1.9 - Claude - Start - New Request for contractor access page
        if(ServiceRequest.Record_Type_Name__c == 'Request_Contractor_Access' ){
            user objUser = [select profile.name from user where id=:userinfo.getuserid()];
            if(ServiceRequest.External_Status_Name__c != 'Draft' && objUser.profile.name!='System Administrator'){
                errorMsg = 'This request is already sumitted and cannot be edited.';
                return null;
            } else {
                return new Pagereference('/apex/NewRequestContractorAccess?srId='+ServiceRequest.Id);
            }
        }
        // V1.9 - Claude - End
        
        return null;
    }
    
    private Pagereference checkRorp(){
        
        //V1.11 // v1.6
        if(ServiceRequest.Record_Type_Name__c == 'Lease_Registration' || 
            ServiceRequest.Record_Type_Name__c == 'Mortgage_Registration' || 
            ServiceRequest.Record_Type_Name__c == 'Mortgage_Variation' || 
            ServiceRequest.Record_Type_Name__c == 'Discharge_Mortgage' ){
                
            Pagereference objRef = new Pagereference('/apex/RORPLeaseRegistration?id='+ServiceRequest.Id);
            ReturnURL = '/apex/RORPLeaseRegistration?id='+ServiceRequest.Id;
            
            //v1.11
            if(ServiceRequest.Record_Type_Name__c == 'Mortgage_Registration'){
                objRef = new Pagereference('/apex/RORPMortgageRegistration?id='+ServiceRequest.Id);
                ReturnURL = '/apex/RORPMortgageRegistration?id='+ServiceRequest.Id;
            }
            
            if(ServiceRequest.Record_Type_Name__c == 'Mortgage_Variation' || ServiceRequest.Record_Type_Name__c =='Discharge_Mortgage'){
                objRef = new Pagereference('/apex/RORPMortgageVariation?id='+ServiceRequest.Id);
                ReturnURL = '/apex/RORPMortgageVariation?id='+ServiceRequest.Id;
            }
            
            if(ServiceRequest.Submitted_Date__c != null){
                
                ReturnURL += '&isDetail=true';
                setErrorMessage('This request is already sumitted and cannot be edited.');
                
            }else{
                System.debug('@@');
                objRef.getParameters().put('isDetail','false');
                return objRef;
            }
        }//end of v1.6
        
        return null;
        
    }
    
    private PageReference srNavigation()
    {
        
        Pagereference objRef;
                
        if(ServiceRequest!=null && ServiceRequest.SR_Template__r.VF_Page_API_Name__c!=null)
        {
            objRef = new Pagereference('/apex/'+ServiceRequest.SR_Template__r.VF_Page_API_Name__c);
            objRef.getParameters().put('RecordType',ServiceRequest.RecordTypeId);
            objRef.getParameters().put('RecordTypeName',ServiceRequest.Record_Type_Name__c);    
            objRef.getParameters().put('id',ServiceRequest.id); 
        }
        
        if(objRef != null) objRef.setRedirect(true);
        return objRef;
        
    }
    private PageReference gsNavigation(){
        
        Pagereference objRef;
                
        if(ServiceRequest!=null && ServiceRequest.Record_Type_Name__c.equals('GS_Comfort_Letters')){
            objRef = new Pagereference('/apex/GSComfortLetters');
            objRef.getParameters().put('RecordType',ServiceRequest.RecordTypeId);
            objRef.getParameters().put('RecordTypeName',ServiceRequest.Record_Type_Name__c);    
            objRef.getParameters().put('id',ServiceRequest.id); 
        }
        
        // V2.0 - Claude - Start
        if(ServiceRequest!=null && ServiceRequest.Record_Type_Name__c.equals('PO_BOX')){
            objRef = new Pagereference('/apex/GSPoBox');
            objRef.getParameters().put('id',ServiceRequest.id); 
            System.debug('@@@');
        }
        // V2.0 - Claude - End
        
        if(ServiceRequest.Record_Type_Name__c.equals('Consulate_or_Embassy_Letter')){
            objRef = new Pagereference('/apex/GSEmbassyLetter');
            objRef.getParameters().put('RecordType',ServiceRequest.RecordTypeId);
            objRef.getParameters().put('RecordTypeName',ServiceRequest.Record_Type_Name__c);    
            objRef.getParameters().put('id',ServiceRequest.id); 
        }
        
        if(objRef != null) objRef.setRedirect(true);
        return objRef;
        
    }
    
    private void getServiceRequestDetails(){
        
        ServiceRequest = [select Id,
                                 RecordTypeId,
                                 Name,
                                 Record_Type_Name__c,
                                 SR_Template__r.VF_Page_API_Name__c,
                                 Project_on_hold__c,
                                 Project_On_Hold_due_to_Cancellation__c,
                                 SR_Group__c,Customer__c,
                                 External_Status_Name__c,
                                 External_SR_Status__r.Type__c,
                                 Internal_Status_Name__c,
                                 Submitted_Date__c from Service_Request__c 
                                 where Id=:ServiceRequest.Id ];
        
    }
    
    private void getUserProfile(){
        profileName = [select profile.name from user where id=:userinfo.getuserid()].Profile.Name;
    }
    
    private void setErrorMessage(String message){
        
        BlockUser = true;
        errorMsg = message;
        
    }
    
    private Boolean hasMessage(){
        return String.isNotBlank(errorMsg);
    }
    
        
}