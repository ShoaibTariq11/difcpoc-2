/*
    Author      : Durga Prasad
    Date        : 17-Nov-2019
    Description : Custom code to create Draft AOR Financial Service Request
    ---------------------------------------------------------------------------------------
*/
global with sharing class CC_Create_AOR_Financial implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        if(stp!=null && stp.HexaBPM__SR__c!=null){
            if(!System.IsBatch() && !System.isFuture())
                OB_CustomCodeHelper.CreateSR(stp.HexaBPM__SR__c,'AOR_Financial');
        }
        return strResult;
    }
}