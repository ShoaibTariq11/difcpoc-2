/*
    class to show the list of documents and the configurable buttons.
*/
public without sharing class OB_DocumentsListController {
    
    @AuraEnabled
    public static RespondWrap getSrDocs(String requestWrapParam)  {
        
        //declaration of wrapper
        RequestWrap reqWrap  = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
        
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);

        //query for document record
        respWrap.docWrap   = [SELECT id, HexaBPM__Document_Name__c, HexaBPM__Document_Description__c, HexaBPM__Status__c, HexaBPM_Amendment__c FROM HexaBPM__SR_Doc__c WHERE  HexaBPM__Service_Request__c = :reqWrap.srId];
        
        //query for button
        respWrap.ButtonSection = [SELECT id,HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection' 
                                          AND HexaBPM__Section__r.HexaBPM__Page__c = :reqWrap.pageId LIMIT 1];
        
        return respWrap;
        }
    
    @AuraEnabled
    public static ButtonResponseWrapper getButtonAction(string SRID,string ButtonId){
        
        
        ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
        HexaBPM__Service_Request__c objRequest = new HexaBPM__Service_Request__c(Id=SRID);
        PageFlowControllerHelper.objSR = objRequest;
        PageFlowControllerHelper objPB = new PageFlowControllerHelper();
        
        PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
        system.debug('@@@@@@@@2 responseNextPage '+responseNextPage);
        respWrap.pageActionName = responseNextPage.pg;
        respWrap.communityName = responseNextPage.communityName;
        respWrap.CommunityPageName = responseNextPage.CommunityPageName;
        respWrap.sitePageName = responseNextPage.sitePageName;
        respWrap.strBaseUrl = responseNextPage.strBaseUrl;
        respWrap.srId = objRequest.Id;
        respWrap.flowId = responseNextPage.flowId;
        respWrap.pageId = responseNextPage.pageId;
        respWrap.isPublicSite = responseNextPage.isPublicSite;
        
        system.debug('@@@@@@@@2 respWrap.pageActionName '+respWrap.pageActionName);
        return respWrap;
    }
    public class ButtonResponseWrapper{
        @AuraEnabled public String pageActionName{get;set;}
        @AuraEnabled public string communityName{get;set;}
        @AuraEnabled public String errorMessage{get;set;}
        @AuraEnabled public string CommunityPageName{get;set;}
        @AuraEnabled public string sitePageName{get;set;}
        @AuraEnabled public string strBaseUrl{get;set;}
        @AuraEnabled public string srId{get;set;}
        @AuraEnabled public string flowId{get;set;}
        @AuraEnabled public string  pageId{get;set;}
        @AuraEnabled public boolean isPublicSite{get;set;}
        
    }
    
    
    
     /* ---------------------------
            Wrapper List
        --------------------------*/
    public class RequestWrap{
        @AuraEnabled
        public String flowID { get; set; }
            
        @AuraEnabled
        public String pageId { get; set; }
        
        @AuraEnabled
        public String srId { get; set; }
        
        
        public RequestWrap(){}
    }
    
    public class RespondWrap{
        
        
       
        @AuraEnabled
        public list<HexaBPM__SR_Doc__c> docWrap { get; set; }
        
        @AuraEnabled 
        public HexaBPM__Section_Detail__c ButtonSection { get; set; }
        
        public RespondWrap(){}
    }

}