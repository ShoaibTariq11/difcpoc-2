/**
* An apex page controller that exposes the site forgot password functionality
*/
global with sharing class ForgotPasswordController {
    // @AuraEnabled
    //public static String username {get; set;}   
    
    //public ForgotPasswordController() {}
    
    @AuraEnabled
    global static String forgotPassowrd(String username) {
        String procesMsg = '';
        System.debug('=========ForgotPassword=======');
        if (Site.isValidUsername(username)) {
            
            Site.forgotPassword(username);
            procesMsg = 'Success';
        }
        else {
            
            procesMsg = 'Error';
        }
        
        return procesMsg;
        
    }
}