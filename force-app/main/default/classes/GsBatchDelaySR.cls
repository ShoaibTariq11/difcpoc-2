/*
    Author      :   Ravi
    Description :   This class is used to update the SR Status to Closed
    --------------------------------------------------------------------------------------------------------------------------
	Modification History
 	--------------------------------------------------------------------------------------------------------------------------
	V.No	Date		Updated By    	Description
	--------------------------------------------------------------------------------------------------------------------------             
 	V1.0    25-04-2015	Ravi			Created   
*/
global class GsBatchDelaySR implements Database.Batchable<sObject>{
	global list<Service_Request__c> start(Database.BatchableContext BC ){
		list<Service_Request__c> lstSRs = [select Id,Name,(select Id,Step_Template__c,Step_Template__r.SR_Status__c from Steps_SR__r where SAP_TMAIL__c != null AND Step_Template__r.SR_Status__c != null) from Service_Request__c where Adhering_to_Safe_Horbour__c = true AND SR_Group__c='GS'];
		return lstSRs;
	}
	
	global void execute(Database.BatchableContext BC, list<Service_Request__c> lstSRs){
		list<Service_Request__c> lstSRToUpdate = new list<Service_Request__c>();
		try{
			for(Service_Request__c objSR : lstSRs){
				if(objSR.Steps_SR__r != null && objSR.Steps_SR__r.size() > 0){
					Service_Request__c objTempSR = new Service_Request__c(Id=objSR.Id);
					objTempSR.External_SR_Status__c = objSR.Steps_SR__r[0].Step_Template__r.SR_Status__c;
					objTempSR.Adhering_to_Safe_Horbour__c = false;
					lstSRToUpdate.add(objTempSR);
				}
			}
			if(!lstSRToUpdate.isEmpty())
				update lstSRToUpdate;
		}catch(Exception ex){
			Log__c objLog = new Log__c();
			objLog.Type__c = 'Schedule Batch GS Delay SR Update';
			objLog.Description__c = 'Exceptio is : '+ex.getMessage()+'\nLine # '+ex.getLineNumber();
			insert objLog;
		}
	}
	
	global void finish(Database.BatchableContext BC){		
	}
}