/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CC_CreateCertificate_of_StatusTest {
    static testMethod void myUnitTest() {
         // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(2);
        insert insertNewAccounts;
        
        //crreate contact
        Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
        insert con; 
        
        //crreate contact
        Contact con2 = new Contact(LastName ='testCon2',AccountId = insertNewAccounts[1].Id);
        insert con2; 
        
        //create createSRTemplate
        list<HexaBPM__SR_Template__c> createSRTemplateList = new list<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle'});
        insert createSRTemplateList;
        
        //create create MultiStructure SRTemplate
        list<HexaBPM__SR_Template__c> MultiStructureTemplateList = new list<HexaBPM__SR_Template__c>();
        MultiStructureTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'Multi_Structure'});
        insert MultiStructureTemplateList;
        
        HexaBPM__Document_Master__c DM = new HexaBPM__Document_Master__c();
        DM.HexaBPM__Code__c = 'CERTIFICATE_OF_STATUS';
        DM.Document_Description__c = 'Certificate of Status - Generated	';
        DM.Name = 'Certificate of Status - Generated';
        insert DM;
        
        HexaBPM__SR_Template_Docs__c SRTD = new HexaBPM__SR_Template_Docs__c();
        SRTD.HexaBPM__SR_Template__c = MultiStructureTemplateList[0].Id;
        SRTD.HexaBPM__Generate_Document__c = true;
        SRTD.HexaBPM__Document_Master__c = DM.Id;
        SRTD.HexaBPM__On_Submit__c = true;
        insert SRTD;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Identify ultimate beneficial owners';
        listPage[0].HexaBPM__Render_by_Default__c = false;
        listPage[0].Community_Page__c = 'test';
        insert listPage;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        insert section;
        
        section.HexaBPM__Default_Rendering__c=true;
        update section;
        
        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
        HexaBPM__Section__c section1 = new HexaBPM__Section__c();
        section1.HexaBPM__Default_Rendering__c=false;
        section1.HexaBPM__Section_Type__c='CommandButtonSection';
        section1.HexaBPM__Section_Description__c='PageBlockSection';
        section1.HexaBPM__layout__c='2';
        section1.HexaBPM__Page__c =listPage[0].id;
        insert section1;
        section.HexaBPM__Default_Rendering__c=true;
        update section1;
        
        HexaBPM__Section_Detail__c sec1= new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c=section1.id;
        sec1.HexaBPM__Component_Type__c='Input Field';
        sec1.HexaBPM__Field_API_Name__c='first_name__c';
        sec1.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c='0';
        sec1.HexaBPM__Mark_it_as_Required__c=true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c=false;
        sec1.Submit_Request__c = true;
        insert sec1;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(3, new List<string> {'AOR_Financial', 'In_Principle','Multi_Structure'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        
        insertNewSRs[1].HexaBPM__customer__c=insertNewAccounts[0].Id;
        insertNewSRs[0].HexaBPM__customer__c=insertNewAccounts[0].Id;
        insertNewSRs[2].HexaBPM__customer__c=insertNewAccounts[0].Id;
        insertNewSRs[2].HexaBPM__Contact__c = con2.id;
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[2].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        
        insertNewSRs[0].Entity_Type__c = 'Financial - related';
        insertNewSRs[1].Entity_Type__c = 'Financial - related';
        insertNewSRs[2].Entity_Type__c = 'Financial - related';
        insertNewSRs[2].Entity_Name__c = 'Test Financial - related';
        
        map<string,string> MapRecordTypes = new map<string,string>();
        for(RecordType RT:[Select Id,DeveloperName from RecordType where sObjectType='HexaBPM__Service_Request__c' and IsActive=true]){
        	MapRecordTypes.put(RT.DeveloperName.tolowercase(),RT.Id);
        }
        insertNewSRs[0].recordtypeId = MapRecordTypes.get('in_principle');
        insertNewSRs[1].recordtypeid = MapRecordTypes.get('multi_structure');
        insertNewSRs[2].recordtypeid = MapRecordTypes.get('multi_structure');
        insertNewSRs[0].HexaBPM__Email__c = 'test.test@test.com';
        insert insertNewSRs;
        
        insertNewSRs[2].HexaBPM__Parent_SR__c = insertNewSRs[0].Id;
        
        update insertNewSRs[2];
        
        HexaBPM__Page_Navigation_Rule__c objPgNavRule = new HexaBPM__Page_Navigation_Rule__c();
        objPgNavRule.HexaBPM__Rule_Name__c = 'Page Rule';
        objPgNavRule.HexaBPM__Page__c = listPage[0].id;
        objPgNavRule.HexaBPM__Section_Detail__c = sec.id;
        objPgNavRule.HexaBPM__Rule_Text_Condition__c='HexaBPM__Service_Request__c->Id#!=#null';  
        objPgNavRule.HexaBPM__Section__c=section.id;
        objPgNavRule.HexaBPM__Rule_Type__c='Render Rule';
        insert ObjPgNavRule;
        
        HexaBPM__Page_Flow_Condition__c ObjPgFlowCond = new HexaBPM__Page_Flow_Condition__c();
        ObjPgFlowCond.HexaBPM__Object_Name__c = 'HexaBPM__Service_Request__c';
        ObjPgFlowCond.HexaBPM__Field_Name__c = 'Id';
        ObjPgFlowCond.HexaBPM__Operator__c = '!='; 
        ObjPgFlowCond.HexaBPM__Value__c = 'null';
        ObjPgFlowCond.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule.id; 
        insert ObjPgFlowCond;
        
        HexaBPM__Page_Flow_Action__c ObjPgFlowAction = new HexaBPM__Page_Flow_Action__c();
        ObjPgFlowAction.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule.id;
        insert ObjPgFlowAction;
        
        HexaBPM__Document_Master__c doc= new HexaBPM__Document_Master__c();
        doc.HexaBPM__Available_to_client__c = true;
        doc.HexaBPM__Code__c = 'Structure_diagram';
        doc.Download_URL__c = 'test';
        insert doc;
        
        HexaBPM__SR_Doc__c srDoc = new HexaBPM__SR_Doc__c();
        srDoc.File_Name__c = 'Test file';
        srDoc.HexaBPM__Document_Master__c = doc.Id;
        srDoc.HexaBPM__Service_Request__c = insertNewSRs[0].Id;
        insert srDoc;
        
        Id p = [select id from profile where name='DIFC Contractor Community Plus User Custom'].id;      
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = con.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        insert user;
        system.runAs(user){
            
            test.startTest();
                        
            HexaBPM__Step__c step = new HexaBPM__Step__c();
            step.HexaBPM__SR__c = insertNewSRs[0].Id;
            insert step;
            
            HexaBPM__SR_Status__c endStatus = new HexaBPM__SR_Status__c();
            endStatus.HexaBPM__Type__c = 'End';
            endStatus.HexaBPM__Code__c = 'End';
            insert endStatus;
            
            insertNewSRs[2].HexaBPM__Internal_SR_Status__c = endStatus.Id;
            update insertNewSRs[2];
            
            insertNewSRs[2] = [SELECT Id,HexaBPM__IsClosedStatus__c,HexaBPM__Internal_SR_Status__c,HexaBPM__Internal_SR_Status__r.HexaBPM__Type__c,HexaBPM__Parent_SR__c FROM HexaBPM__Service_Request__c WHERE Id=:insertNewSRs[2].Id];
            
            system.debug('HexaBPM__Type__c==>'+insertNewSRs[2].HexaBPM__Internal_SR_Status__r.HexaBPM__Type__c);
            system.debug('HexaBPM__IsClosedStatus__c==>'+insertNewSRs[2].HexaBPM__IsClosedStatus__c);
            system.debug('HexaBPM__Parent_SR__c==>'+insertNewSRs[2].HexaBPM__Parent_SR__c);
            system.debug('HexaBPM__SR__c==>'+step.HexaBPM__SR__c);
            CC_CreateCertificate_of_Status ccCode = new CC_CreateCertificate_of_Status();
            ccCode.EvaluateCustomCode(null,step);
            
            CC_ProcessMultiStructureOnNameApproval objccApr = new CC_ProcessMultiStructureOnNameApproval();
            objccApr.EvaluateCustomCode(null,step);
            
            test.stopTest();
        }
    }
}