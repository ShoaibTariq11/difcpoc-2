/*
    Author      :   Ghanshyam
    Description :   This Batch will be used to send the email every quaterly to the user where the field Send otification?(send_Notification__C) is true
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date            Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.0    23/12/2018      Ghanshyam       Created
    V1.1    07/01/2019      Azfer           Updated the SOQL and the Execute Method
*/
global class quaterly_Email_Alert_Batch implements Database.Batchable<sObject>{

    global database.querylocator start(Database.BatchableContext BC){
        String Query = 'Select Id, Name, ContactId, Contact.Account.ROC_Status__c, Email '+
                        'From User '+
    		            'WHERE Send_Notification__c = True';
        if(Test.isRunningTest()){
            Query = 'Select Id, Name, ContactId, Contact.Account.ROC_Status__c, Email '+
                        'From User '+
    		            'WHERE Send_Notification__c = True '+
                		'LIMIT 1';
        }
        return Database.getQueryLocator(Query);
    }

    global void execute(Database.BatchableContext BC, List<User> userList){
        EmailTemplate emailTemplate = [select Id, Body,HtmlValue,DeveloperName  from EmailTemplate where DeveloperName = 'DIFC_Security_policies_quarterly'];
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'noreply.portal@difc.ae'];    
        
        list<Messaging.SingleEmailMessage> allmails = new list<Messaging.SingleEmailMessage>();
        for (User Users : (List<User>)userList) {
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setSaveAsActivity(false);
            email.setOrgWideEmailAddressId(owea[0].id);   
            email.setToAddresses(new String[] {Users.Email});
            email.setTargetObjectId(Users.id);
            email.setTemplateId(emailTemplate.id);    
            allmails.add(email);
        }
        Messaging.sendEmail(allMails);   
    }
    
    global void finish(Database.BatchableContext BC) {
        system.debug('Email send Successfully!');
    }
}