/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=false)
private class TestListingAdvancedSearch {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'Test Building';
        objBuilding.Building_No__c = '00000001';
        insert objBuilding;
        
        Unit__c objUnit = new Unit__c();
        objUnit.Building__c = objBuilding.Id;
        objUnit.SAP_Unit_No__c = '0000000000001';
        insert objUnit;
        
        Listing__c objList = new Listing__c();
        objList.Unit__c = objUnit.Id;
        objList.Status__c = 'Active';
        objList.Start_Date__c = system.today();
        objList.End_Date__c = system.today().addDays(30);
        objList.Featured__c = true;
        insert objList;
        
        Service_Request__c objSR = new Service_Request__c();
        insert objSR;
        
        SR_Doc__c objSRDoc = new SR_Doc__c();
        objSRDoc.Service_Request__c = objSR.Id;
        objSRDoc.Name = 'Photograph 1';
        objSRDoc.Doc_ID__c = objSR.Id;
        objSRDoc.Listing__c = objList.Id;
        objSRDoc.Listing_Doc_Status__c = 'Active';
        insert objSRDoc;
        
        ListingAdvancedSearch objListingAdvancedSearch = new ListingAdvancedSearch();
        
    }
}