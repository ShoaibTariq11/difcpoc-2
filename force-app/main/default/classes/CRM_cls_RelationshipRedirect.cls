/******************************************************************************************
 *  Name        : Test_cls_Crm 
 *  Author      : Claude Manahan
 *  Company     : NSI JLT
 *  Date        : 2016-05-04
 *  Description : Class which handles redirects when a relationship is created through
 *				  the Account and Opportunity detail page
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   04-05-2016	 Claude		   Created
*******************************************************************************************/
public without sharing class CRM_cls_RelationshipRedirect {
	
	/**
	 * Map of the fields that needs to be pre-populated
	 */
	private Map<String, String> fieldMap;
	
	/**
	 * Redirects the page to the Relationship Create page
	 * with pre-populated values
	 */
	public PageReference redirectToUrl(){
		
		fieldMap = new Map<String,String>();
		
		/* Get the fields map */
		fieldMap = createDefaultValues();
		
		/* Customize the Id fields */
		String accountFieldId = fieldMap.get('Account');
		String activeFieldId = fieldMap.get('Active').substring(0,14);
		String opportunityId = fieldMap.get('Opportunity');
		
		/*
		 * NOTE: For lookup fields, you need to pre-populate both the 
		 * 		 text and hidden field in the form for the values to be 
		 *		 successfully saved
		 */
		 
		/* Check if the account id exists in the URL */
		String accountId = 
			String.isBlank(String.valueOf(Apexpages.currentPage().getParameters().get('accountId'))) ? 
			String.valueOf(Apexpages.currentPage().getParameters().get('oppAcctId')) : String.valueOf(Apexpages.currentPage().getParameters().get('accountId'));
		
		/* Check if the name exists in the URL */
		String accountName = 
			String.isBlank(String.valueOf(Apexpages.currentPage().getParameters().get('accountName'))) ? 
			String.valueOf(Apexpages.currentPage().getParameters().get('oppAcctName')) : String.valueOf(Apexpages.currentPage().getParameters().get('accountName'));
		
		/* Check if the opportunity id exists in the URL */	
		String oppId = 
			String.isBlank(String.valueOf(Apexpages.currentPage().getParameters().get('oppId'))) ? 
			'' : String.valueOf(Apexpages.currentPage().getParameters().get('oppId')); 
		
		/* Check if the opportunity name exists in the URL */
		String oppName = 
			String.isBlank(String.valueOf(Apexpages.currentPage().getParameters().get('oppName'))) ? 
			'' : String.valueOf(Apexpages.currentPage().getParameters().get('oppName'));
		
		/* 
		 * Build the URL
		 * NOTE: By default, the lookup DOM element names
		 *		 have a '_lkid' suffix appended in its name.
		 *		 Lookup fields always come in pairs: 
		 *		  a.) The hidden field
		 *		  b.) The text field
		 */
     	String redirectUrl = 
     		'/a0I/e?' + activeFieldId + 'L' + '=1&' 
     		+ accountFieldId  + '_lkid=' + accountId + 
     		'&' + accountFieldId + '=' + accountName + 
     		'&' + opportunityId + '=' + oppName + '&' + 
     		opportunityId  + '_lkid=' + oppId;
     	
     	return new PageReference(redirectUrl); // Return the new URL
	}
	
	/**
	 * Gets the element names of the fields in the form
	 */
	private Map<String, String> createDefaultValues(){
	    
	    PageReference p = new PageReference('/' + Relationship__c.SObjectType.getDescribe().getKeyPrefix() + '/e?nooverride=1');
	    
	    // Create a mock HTML content for Account and Opportunity fields
    	String htmlMock = '<label for="CF00N20000029XX2i"><span class="requiredMark"></span>Account</label>';
    	htmlMock += '<label for="CF00N20000019XY2i"><span class="requiredMark"></span>Opportunity</label>';
    	htmlMock += '<label for="CF00N20000019GY2i">Active</label>';
	    
	    String html = Test.isRunningTest() ? htmlMock : p.getContent().toString();
	    
	    Map<String, String> labelToId = new Map<String, String>();
	    Matcher m = Pattern.compile('<label for="(.*?)">(<span class="requiredMark">\\*</span>)?(.*?)</label>').matcher(html);
	    
	    while (m.find()) {
	        String label = m.group(3);
	        String id = m.group(1);
	        labelToId.put(label, id);
	    }
	    
	    return labelToId;
	}
	
	/**
	 * Class Extension Constructor
	 * @param 		controller		the sObject Standard Controller instance
	 */
    public CRM_cls_RelationshipRedirect(ApexPages.StandardController controller){}
    
}