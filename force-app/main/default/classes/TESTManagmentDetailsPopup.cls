/******************************************************************************************
 *  Class Name  : TESTManagmentDetailsPopup
 *  Author      : Sai kalyan Sanisetty 
 *  Company     : DIFC
 *  Date        : 24 NOV 2019        
 *  Description :                   
 ----------------------------------------------------------------------
   Version     Date              Author                Remarks                                                 
  =======   ==========        =============    ==================================
    v1.1    24 NOV 2019           Sai                Initial Version  
*******************************************************************************************/


@isTest
public class TESTManagmentDetailsPopup{
	
	public static list<Service_Request__c> lstServiceRequest;
	
	public static void testData(){
		
		List<Account> lstAccount = new List<Account>();
		
		lstServiceRequest = TestUtilityClass.createServiceRequestData(1,false);
		lstAccount = TestUtilityClass.accountDataInsert(1,true);
		Compliance__c compliance = new Compliance__c();
		compliance.Name = Label.Confirmation_Statement;
		compliance.Account__c = lstAccount[0].ID; 
		compliance.Status__c = Label.Defaulted;
		INSERT compliance; 
		lstServiceRequest[0].Customer__c = lstAccount[0].ID;
		INSERT lstServiceRequest;
	}
	
	static testmethod void positiveTest1(){
		testData();
		
		Test.startTest();
			ApexPages.StandardController stdJR = new ApexPages.StandardController(lstServiceRequest[0]);
			ManagmentDetailsPopup mPopup = new ManagmentDetailsPopup(stdJR);
		Test.stopTest();
		
	}	
}