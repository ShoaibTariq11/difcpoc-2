/******************************************************************************************
 *  Name        : OB_ApplicationDocTriggerHandler
 *  Author      : Durga Kandula
 *  Company     : PwC
 *  Date        : 30-Jan-2020
 *  Description : Trigger Handler for OB_ApplicationDocTrigger
 ----------------------------------------------------------------------------------------                               
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------   
 
            
*******************************************************************************************/
public without sharing class OB_ApplicationDocTriggerHandler{
    public static void OnBeforeInsert(list<HexaBPM__SR_Doc__c> TriggerNew){
       for(HexaBPM__SR_Doc__c doc:TriggerNew){
           doc.HexaBPM__Sys_RandomNo__c = Apex_Utilcls.getRandNo(4);
           doc.Sys_AlphaNumeric_Random__c = Apex_Utilcls.getRandNo(6);
       }
    }
}