/***********************************************************************************
 *  Author   : Arun 
 *  Purpose  : Start batch to update companies Active License
  --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
V.No    Date        Updated By  Description
 ----------------------------------------------------------------------------------------              
 
 V1.1   29/July/2019   Sai       Updated Operating location SOQL     
 
 **********************************************************************************************/


global  class ScheduledBatchActiveLicense implements Schedulable {
     
     global void execute(SchedulableContext sc)
     {
     string SQL='select id,Registered_Active_Lease__c,Active_Leases__c,Lease_Expiry_Date__c,(select id,Lease__c from Operating_Locations__r where Status__c=\'Active\' AND Area__c !=\'0\' ORDER BY Area_WF__c desc limit 1),(select id from Lease__r where Status__c=\'Active\') from Account where (ROC_Status__c=\'Active\' OR ROC_Status__c=\'CP-Active\')';
     
        BatchActiveLicense obj = new BatchActiveLicense(SQL);
       
        database.executeBatch(obj,1);   
    }
}