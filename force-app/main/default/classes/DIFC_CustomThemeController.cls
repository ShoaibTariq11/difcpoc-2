public with sharing class DIFC_CustomThemeController {
    
    @AuraEnabled
    public static RespondWrap fetchAccoutName(String requestWrapParam)  {
    
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
        
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
        
        for(user loggedinUser :OB_AgentEntityUtils.getUserById(userInfo.getUserId())) {
            respWrap.loggedInUser = loggedinUser;
        }
        return respWrap;
    }
        
        
    // ------------ Wrapper List ----------- //
        
    public class RequestWrap{
    
        @AuraEnabled public String userId;                                                                                                                                                                                                                                             public RequestWrap(){}
    }
        
    public class RespondWrap{
    
        @AuraEnabled public user loggedInUser;                                                                                                                                                                                                                                             public RespondWrap(){}
    }
}