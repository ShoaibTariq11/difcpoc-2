/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class Test_GS_DataFactory {
	
	public static final String DEFAULT_IDENTIFICATION_NUMBER = '2/6/1234567';
	
	public static final String DEFAULT_GS_SR_GROUP = 'GS';

	public static Account getGsTestAccount(){
		return Test_CC_FitOutCustomCode_DataFactory.getTestAccount();
	}
	
	public static License__c getGsTestLicense(Id accountId){
		return Test_CC_FitOutCustomCode_DataFactory.getTestLicense(accountId);
	}
	
	public static SR_Template__c getGsTestSrTemplate(String recordTypeName){
		return Test_CC_FitOutCustomCode_DataFactory.getTestSRTemplate(recordTypeName,recordTypeName,DEFAULT_GS_SR_GROUP);
	}
	
	public static Identification__c getGsTestIdentification(Id accountId){
		
		Identification__c objId = new Identification__c();
        objId.Id_No__c = DEFAULT_IDENTIFICATION_NUMBER;
        objId.Valid_From__c = system.today().addYears(-1);
        objId.Valid_To__c = system.today().addYears(1);
        objId.Identification_Type__c = 'Access Card';
        objId.Account__c = accountId;
        
        return objId;
	}
	
}