/*
    Created By  : Shabbir - DIFC on 13 Oct,2015
    Description : Test class for SRValidations,Amnd_Recordtype_AssgnTrg 
--------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By      Description
 ----------------------------------------------------------------------------------------              
 ----------------------------------------------------------------------------------------
*/
@isTest(seealldata=false)
private class Test_RORPLeaseValidations {
    
    public static testmethod void testRORPLeaseValidationAndPenalty(){
        
        Account objAccount  = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c ='Pakistan';
        objAccount.ROC_Status__c = 'Active';
        objAccount.BP_No__c='0000008978';
        insert objAccount;  
        
        Building__c build = new Building__c();
        build.Name = 'Central Park Towers';
        build.Company_Code__c = '5300';
        build.Permit_Date__c = Date.today();
        insert build;
        
        Unit__c unit2 = new Unit__c();
        unit2.Name='102';
        unit2.Unit_Usage_Type__c = 'Residential 1 Bedroom';
        unit2.Building__c = build.id;
        unit2.Building_Name__c = 'Central Park Towers';
        unit2.Unit_Square_Feet__c = 2300;
        insert unit2;  
        
        Lease__c lease = new Lease__c();
        lease.name = '103715';
        lease.status__c = 'Active';
        lease.Lease_Types__c = 'Purchased Property Caveat';
        lease.Type__c = 'Purchased';
        insert lease;
        
        Lease_Partner__c lp = new Lease_Partner__c();
        lp.Type_of_Lease__c = 'Purchased';
        lp.Lease_Types__c = 'Purchased Property Caveat';
        lp.Lease__c =lease.Id; 
        lp.Unit__c = unit2.Id;
        insert lp;
 
        SR_Status__c objStatus = new SR_Status__c();
        objStatus.Name = 'Draft';
        objStatus.Code__c = 'DRAFT';
        insert objStatus; 

        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Lease Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Lease_Registration';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Lease Certificate';
        objDocMaster.Code__c = 'Lease Certificate';
        insert objDocMaster;

        Document_Master__c objDocMasterDeveloper = new Document_Master__c();
        objDocMasterDeveloper.Name = 'Lease_Certificate_Developer';
        objDocMasterDeveloper.Code__c = 'Lease_Certificate_Developer';
        insert objDocMasterDeveloper;

        Document_Master__c objDocMasterCaveat = new Document_Master__c();
        objDocMasterCaveat.Name = 'Lease_Certificate_onPlan_Caveat';
        objDocMasterCaveat.Code__c = 'Lease_Certificate_onPlan_Caveat';
        insert objDocMasterCaveat;
        
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.Create_Generate_SR_Doc__c = true;
        insert objTempDocs;        

        SR_Template_Docs__c objTempDocs3 = new SR_Template_Docs__c();
        objTempDocs3.SR_Template__c = objTemplate.Id;
        objTempDocs3.Document_Master__c = objDocMasterCaveat.Id;
        objTempDocs3.Create_Generate_SR_Doc__c = true;
        insert objTempDocs3;        

        SR_Template_Docs__c objTempDocs2 = new SR_Template_Docs__c();
        objTempDocs2.SR_Template__c = objTemplate.Id;
        objTempDocs2.Document_Master__c = objDocMasterDeveloper.Id;
        objTempDocs2.Create_Generate_SR_Doc__c = true;
        insert objTempDocs2;   
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Lease_Registration' ,  'Landlord' , 'Individual_Tenant','Lease_Unit')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }        
        
        list<Service_Request__c> lstSR = new list<Service_Request__c>();
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = mapRecordType.get('Lease_Registration');
        objSR.unit__c = unit2.Id;
        objSR.building__c = build.Id;
        objSR.Type_of_Lease__c = 'Lease';
        objSR.SR_Group__c = 'RORP';
        objSR.Lease_Commencement_Date__c = Date.today().adddays(-30);
        objSR.Lease_Expiry_Date__c = Date.today().addyears(2);
        
        insert objSR;
        
        list<Amendment__c> amendlist = new list<Amendment__c> ();
        Amendment__c amnd1 = new Amendment__c (RecordTypeId=mapRecordType.get('Landlord'),ServiceRequest__c=objSR.id ,  Amendment_Type__c = 'Landlord' ,Type_of_Landlord__c = 'Individual', Given_Name__c = 'shab' ,Family_Name__c = 'ahmed',Passport_No__c ='123490' , Nationality_list__c = 'Pakistan');
        amendlist.add(amnd1);

        Amendment__c amnd2 = new Amendment__c (RecordTypeId=mapRecordType.get('Individual_Tenant'),ServiceRequest__c=objSR.id ,  Amendment_Type__c = 'Tenant' , Given_Name__c = 'shab2' ,Family_Name__c = 'ahmed2',Passport_No__c ='87123490' , Nationality_list__c = 'Pakistan');
        amendlist.add(amnd2);
	
		Amendment__c amnd3 = new Amendment__c (RecordTypeId=mapRecordType.get('Lease_Unit'),ServiceRequest__c=objSR.id ,  Amendment_Type__c = 'Unit' , Unit__c = unit2.Id );
        amendlist.add(amnd3);

        insert amendlist;        
                     
         Amendment__c updateAmend = amendlist[0];
         updateAmend.Given_Name__c = 'test';
         //update updateAmend;
        
        SRValidations.GenerateSRDoc(objSR.id);
        
        Step__c objStp = new Step__c();
        objStp.SR__c = objSR.Id;
        
        CC_RORPCodeCls.CreateLeaseCertificateLeaseRegistration(objStp);
        
        CC_RORPCodeCls.updateNoOfUnits(objSR);
        
        delete amnd2;
    }
    
}