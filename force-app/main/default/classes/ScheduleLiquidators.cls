/******************************************************************************************
 *  Author      : Selva
 *  Date        : 2019-07-02    
 *  Purpose     : Class for scheduling the email notificaion for Liquidators   
*******************************************************************************************/

global class ScheduleLiquidators implements Schedulable {
    
    global ScheduleLiquidators(){
    
    }
    global void execute(SchedulableContext SC) {
    
        String nextFireTime = '0 0 0 20 SEP,DEC ? *';
        system.debug('--'+nextFireTime );
        SendNotificationLiquidators obj = new SendNotificationLiquidators ();
        Id jobId = System.schedule('Class-sendSRDocumentInMail'+system.now(), nextFireTime , new SendNotificationLiquidators());
        
    }
}