@isTest
private class TestAttachmentTrigger {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        //objAccount.Active__c = true;
        objAccount.Phone = '1234567890';
        insert objAccount;
      
      map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
      for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
        mapSRRecordTypes.put(objType.DeveloperName,objType);
      }
      User_SR__c UserSR = new User_SR__c(Username__c='test@test.com');
        UserSR.Customer__c = objAccount.Id;
        //SR.Submitted_DateTime__c = DateTime.now();
        //Date SRDate = Date.today();
        //SR.Submitted_Date__c = subDate;
        UserSR.Title__c = 'Mrs.';
        UserSR.First_Name__c = 'Finley';
        UserSR.Middle_Name__c = 'Ashton';
        UserSR.Last_Name__c = 'Kate';
        UserSR.Position__c = 'Engineer';
        UserSR.Place_of_Birth__c = 'USA';
        UserSR.Date_of_Birth__c = Date.newinstance(1980,10,17);
        UserSR.Email__c = 'test@test.com';
        UserSR.Roles__c = 'Company Services';
        UserSR.Mobile_Number__c = '+971505674534';
        UserSR.Nationality__c = 'Pakistan';
        UserSR.Passport_Number__c = 'ASD121323';
        UserSR.User_Form_Action__c = 'Create New User'; 
        insert UserSR;
      
      Service_Request__c objSR = new Service_Request__c();
      objSR.Customer__c = objAccount.Id;
      //objSR.Passport_No__c = 'K812345';
      //objSR.Lease_Start_Date__c = system.today();
      //objSR.Rent_Commencement_Date__c = system.today().addYears(1);
      objSR.RecordTypeId = mapSRRecordTypes.get('Annual_Return').Id;
      objSR.Entity_Name_checkbox__c = true;
      objSR.Registered_Office_Address__c = true;
      objSR.Registered_Phone_Number__c = true;
      objSR.Business_Activity__c = true;
      objSR.Name_Address_DOB_of_Company_Secretary__c = true;
      objSR.Name_Address_DOB_of_Directors__c = true;
      objSR.Name_Address_DOB_of_Shareholders__c = true;
      objSR.Total_No_Value_Class_of_Allot_Shares__c = true;
      insert objSR;
      
      SR_Doc__c objSRDoc = new SR_Doc__c();
      objSRDoc.Service_Request__c = objSR.Id;
      insert objSRDoc;
      
      Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'AR Third Party NOC';
        insert objDocMaster;
        
        Lease__c objLease = new Lease__c();
        objLease.Account__c = objAccount.Id;
        objLease.Start_Date__c = system.today().addyears(1);
        objLease.End_Date__c = system.today();
        insert objLease;
        
        list<Attachment> lstAttachments = new list<Attachment>();
      Attachment objAttachment = new Attachment();
      objAttachment.Name = 'Test objSRDoc';
      objAttachment.Body = blob.valueOf('test');
      objAttachment.ParentId = objSRDoc.Id;
      lstAttachments.add(objAttachment);
      
      objAttachment = new Attachment();
      objAttachment.Name = 'Test objDocMaster';
      objAttachment.Body = blob.valueOf('test');
      objAttachment.ParentId = objDocMaster.Id;
      lstAttachments.add(objAttachment);
      
      objAttachment = new Attachment();
      objAttachment.Name = 'Test objSR';
      objAttachment.Body = blob.valueOf('test');
      objAttachment.ParentId = objSR.Id;
      lstAttachments.add(objAttachment);
      
      objAttachment = new Attachment();
      objAttachment.Name = 'Lease Offer.pdf';
      objAttachment.Body = blob.valueOf('test');
      objAttachment.ParentId = objLease.Id;
      lstAttachments.add(objAttachment);
      
      objAttachment = new Attachment();
      objAttachment.Name = 'New User Access Form.pdf';
      objAttachment.Body = blob.valueOf('test');
      objAttachment.ParentId = UserSR.Id;
      lstAttachments.add(objAttachment);
      
      objSR = new Service_Request__c();
      objSR.Customer__c = objAccount.Id;
      //objSR.Passport_No__c = 'K812345';
      //objSR.Lease_Start_Date__c = system.today();
      //objSR.Rent_Commencement_Date__c = system.today().addYears(1);
      objSR.RecordTypeId = mapSRRecordTypes.get('DIFC_Sponsorship_Visa_New').Id;
      objSR.Entity_Name_checkbox__c = true;
      objSR.Registered_Office_Address__c = true;
      objSR.Registered_Phone_Number__c = true;
      objSR.Business_Activity__c = true;
      objSR.Name_Address_DOB_of_Company_Secretary__c = true;
      objSR.Name_Address_DOB_of_Directors__c = true;
      objSR.Name_Address_DOB_of_Shareholders__c = true;
      objSR.Total_No_Value_Class_of_Allot_Shares__c = true;
      
      
      insert objSR;
      
      SR_Status__c srs = new SR_Status__c();
      srs.Name= 'Submitted';
      srs.Type__c = 'End';
      srs.code__c = 'SUBMITTED';
      insert srs;
      
      objSR.external_SR_Status__c= srs.id;
      objSR.Internal_SR_status__c= srs.id;
      objSR.Statement_of_Undertaking__c =true;
      update objSR;
      
      objSRDoc = new SR_Doc__c();
      objSRDoc.Name = 'Coloured Photo';
      objSRDoc.Service_Request__c = objSR.Id;      
      insert objSRDoc;
      service_Request__c sr = [select id,submitted_Date__c  from service_request__c where id=:objSR.id];
      system.debug('****'+sr.submitted_Date__c);
      
      objAttachment = new Attachment();
      objAttachment.Name = 'New User Access Form.pdf';
      objAttachment.Body = blob.valueOf('test');
      objAttachment.ParentId = objSRDoc.Id;
      lstAttachments.add(objAttachment);
      
      insert lstAttachments;
      
      delete lstAttachments;
      
      objAttachment = new Attachment();
      objAttachment.Name = 'test.jpg';
      objAttachment.Body = blob.valueOf('test');
      objAttachment.ParentId = objSRDoc.Id;
      insert objAttachment ;
      
      
    }
}