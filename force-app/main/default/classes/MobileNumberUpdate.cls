public without sharing class MobileNumberUpdate {
    
    @InvocableMethod(label='MobileNumberUpdate' description='Mobile Number Update') 
    public static void MobileNumberUpdate(List<Id> stepIds){
  		string res = 'Success';
  		Step__c objStp = [select id,name,mobile_No__c,status__c,step_name__c,SR__c from Step__c where id in : stepIds];
  		system.debug('objStp-----------'+objStp);
  		try {
        	List<Service_request__c> servList = new List<Service_request__c>();
			for(Service_request__c servReq : [Select id,Mobile_number__c,Would_you_like_to_opt_our_free_couri__c from service_request__c where id=:objStp.SR__c]){
				servReq.Mobile_number__c = objStp.mobile_No__c != Null ? objStp.mobile_No__c : Null;
				servList.add(servReq);
			}
			update servList;
			objStp.status__c='a1M20000001iL9Z';
			update objStp;
			for(Service_Request__c serv : servList){
				if(serv.Would_you_like_to_opt_our_free_couri__c =='Yes'){
					CC_CourierCls.CreateShipment(objStp.id);
				}
			}
        } catch (exception e) { res = e.getMessage(); }
    }
}