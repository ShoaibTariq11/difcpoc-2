global class SearchAndReplaceLead implements Database.Batchable < sObject > {

    global final String Query;


    global SearchAndReplaceLead(String q) {

        Query = q;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List < Lead > scope) {
        for (Lead leadRec: scope) {
            
            leadRec.Email =leadRec.Email != NULL ? leadRec.Email +'.invalid' : 'test@difc.ae.invalid';
            leadRec.Phone = leadRec.Phone != NULl ? '+971569803616': leadRec.Phone;
            leadRec.MobilePhone = leadRec.MobilePhone != NULl ? '+971569803616': leadRec.MobilePhone;
			leadRec.Alternative_Phone_No__c = leadRec.Alternative_Phone_No__c != NULl ? '+971569803616': leadRec.Alternative_Phone_No__c;
        }
        database.update(scope,false);
    }

    global void finish(Database.BatchableContext BC) {}
}