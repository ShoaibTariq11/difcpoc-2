/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_StepTrgHandler {

    static testMethod void myUnitTest() {
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        String objRectype;
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Dissolution') AND SobjectType='Service_Request__c']){
            objRectype = objRT.id;
        }
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = objRectype;
        
        insert objSR; 
        
        Step_Template__c stepTemplate = new Step_Template__c();
        stepTemplate.name = 'Clearance from all the Departments';
        stepTemplate.Code__c = 'CLEARANCE_FROM_ALL_THE_DEPARTMENTS';
        stepTemplate.Step_RecordType_API_Name__c = 'Clearance_from_all_the_Departments';
        insert stepTemplate;
        
        Status__c stepStatus = new Status__c();
        stepStatus.name = 'Pending Clearance';
        stepStatus.Code__c = 'PENDING_CLEARANCE';
        insert stepStatus;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        objStep.Step_Template__c = stepTemplate.id;
        objStep.Status__c = stepStatus.id;
        objStep.Government_Services__c = true;
        insert objStep;
    }
    
    static testMethod void myUnitTest1() {
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Last RB';
        objContact.Email = 'test@difcportal.com';
        objContact.AccountId = objAccount.Id;
        objContact.FirstName = 'Nagaboina';
        objContact.RecordTypeId = [SELECT ID FROM RecordType WHERE DeveloperName = 'Portal_User' AND SobjectType = 'Contact'].ID;
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Employee Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
            
        
        String objRectype;
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('GS_Comfort_Letters') AND SobjectType='Service_Request__c']){
            objRectype = objRT.id;
        }
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = objRectype;
        objSR.SR_Group__c = 'GS';
        
        insert objSR; 
        
        Step_Template__c stepTemplate = new Step_Template__c();
        stepTemplate.name = 'Clearance from all the Departments';
        stepTemplate.Code__c = 'CLEARANCE_FROM_ALL_THE_DEPARTMENTS';
        stepTemplate.Step_RecordType_API_Name__c = 'Clearance_from_all_the_Departments';
        insert stepTemplate;
        
        Status__c stepStatus = new Status__c();
        stepStatus.name = 'Pending Clearance';
        stepStatus.Code__c = 'PENDING_CLEARANCE';
        insert stepStatus;
        
        Status__c stepStatus1 = new Status__c();
        stepStatus1.name = 'cleared';
        stepStatus1.Code__c = 'CLEARED';
        insert stepStatus1;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        objStep.Step_Template__c = stepTemplate.id;
        objStep.Status__c = stepStatus.id;
        objStep.Government_Services__c = true;
        insert objStep;
        
        objStep.Status__c = stepStatus1.id;
        update objStep;
        
    }
    
     static testMethod void myUnitTest2() {
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Last RB';
        objContact.Email = 'test@difcportal.com';
        objContact.AccountId = objAccount.Id;
        objContact.FirstName = 'Nagaboina';
        objContact.RecordTypeId = [SELECT ID FROM RecordType WHERE DeveloperName = 'GS_Contact' AND SobjectType = 'Contact'].ID;
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Employee Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
            
        
        String objRectype;
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Company_Temporary_Work_Permit_Request_Letter') AND SobjectType='Service_Request__c']){
            objRectype = objRT.id;
        }
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = objRectype;
        objSR.SR_Group__c = 'GS';
        objSR.contact__c = objContact.id;
        
        insert objSR; 
        
        Step_Template__c stepTemplate = new Step_Template__c();
        stepTemplate.name = 'Temporary Work Permit Form is Typed';
        stepTemplate.Code__c = 'Temporary Work Permit Form is Typed';
        stepTemplate.Step_RecordType_API_Name__c = 'General';
        insert stepTemplate;
        
        Status__c stepStatus = new Status__c();
        stepStatus.name = 'Awaiting Review';
        stepStatus.Code__c = 'Awaiting_Review';
        insert stepStatus;
        
        Status__c stepStatus1 = new Status__c();
        stepStatus1.name = 'Closed';
        stepStatus1.Code__c = 'Closed';
        insert stepStatus1;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        objStep.Step_Template__c = stepTemplate.id;
        objStep.Status__c = stepStatus.id;
        objStep.first_Name_Arabic__c ='test';
        objStep.Last_Name_Arabic__c ='test';
        
        objStep.Government_Services__c = true;
        insert objStep;
        
        
      SR_Doc__c objSRDoc = new SR_Doc__c();
      objSRDoc.Name = 'Coloured Photo';
      objSRDoc.Service_Request__c = objSR.Id;  
      objSRDoc.status__c ='Uploaded';    
      insert objSRDoc;
      
      
      Attachment objAttachment = new Attachment();
      objAttachment.Name = 'New User Access Form.pdf';
      objAttachment.Body = blob.valueOf('test');
      objAttachment.ParentId = objSRDoc.Id;
      
      
      insert objAttachment;
        
        objStep.Status__c = stepStatus1.id;
        update objStep;
        
    }
}