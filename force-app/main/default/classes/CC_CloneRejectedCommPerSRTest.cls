/*
    Author      : Leeba
    Date        : 12-April-2020
    Description : Test class for CC_CloneRejectedCommPerSR
    --------------------------------------------------------------------------------------
*/

@isTest
public class CC_CloneRejectedCommPerSRTest{

    public static testMethod void CC_CloneRejectedCommPerSR() {
    
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
      
        
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
       insert objsrTemp;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        objHexaSR.HexaBPM__Email__c = 'test@test.com';
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
        
        HexaBPM__Document_Master__c docmaster = new HexaBPM__Document_Master__c ();
        docmaster.HexaBPM__Code__c ='Commerical_Permission_Letter';
        insert docmaster;
        
        HexaBPM__SR_Template_Docs__c objSRTempdoc = new HexaBPM__SR_Template_Docs__c();
        objSRTempdoc.Visible_to_GS__c = true;
        objSRTempdoc.HexaBPM__Document_Master__c = docmaster.id;
        insert objSRTempdoc;
        
        HexaBPM__SR_Doc__c objSrDoc = new HexaBPM__SR_Doc__c();
        objSrDoc.HexaBPM__Service_Request__c = objHexaSR.Id;
        objsrDoc.HexaBPM__Document_Master__c = docmaster.id;
        objSRDoc.HexaBPM__SR_Template_Doc__c = objSRTempdoc.Id;
        insert objSRDoc;
       
       Blob b = Blob.valueOf('Test Data');
        
        Attachment attachment = new Attachment();
        attachment.ParentId = objSRDoc.Id;
        attachment.Name = 'Commercial Permission Letter';
        attachment.Body = b;
        insert(attachment);
        
        Test.startTest();
         HexaBPM__Step__c step = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.HexaBPM__Email__c                               
                                  from HexaBPM__Step__c where Id=:objHexastep.Id];
        CC_CloneRejectedCommPerSR CC_CloneRejectedCommPerSRObj = new CC_CloneRejectedCommPerSR();
        CC_CloneRejectedCommPerSRObj.EvaluateCustomCode(objHexaSR,step); 
        Test.stopTest();
       
    }
 }