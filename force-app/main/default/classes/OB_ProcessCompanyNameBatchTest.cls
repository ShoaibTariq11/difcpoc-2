/*
    Author      : Leeba
    Date        : 30-March-2020
    Description : Test class for OB_ProcessCompanyNameBatch 
    --------------------------------------------------------------------------------------
*/

@isTest
public class OB_ProcessCompanyNameBatchTest {

     public static testmethod void test1(){
     
          Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
       contact con = new Contact();
       con.LastName = 'test';
       con.FirstName = 'test';
       con.Email = 'test@test.com';
       insert con;   

        Profile p = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community Plus User Custom']; 
         
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com',contactid = con.Id);
       
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
       insert objsrTemp;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.OB_Application__c = objHexaSR.Id;
        insert objSR;
        
        
       Company_Name__c objcomp = new Company_Name__c();
       objcomp.Application__c = objHexaSR.Id;
       objcomp.Entity_Name__c = 'test';
       objcomp.Trading_Name__c = 'test';
       objcomp.Arabic_Entity_Name__c = 'أكبر يساف';
       objcomp.Arabic_Trading_Name__c = 'أكبر يساف';
       objcomp.Status__c = 'Draft';
       objcomp.Date_of_Approval__c = system.today()-100;
       insert objcomp;
       
       Test.startTest();
       OB_ProcessCompanyNameBatch obj = new OB_ProcessCompanyNameBatch();
       DataBase.executeBatch(obj);
       Test.stopTest();

     
     }

}