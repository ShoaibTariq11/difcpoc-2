/*********************************************************************************************************************
*  Name     : CC_GSNewCodeClass 
*  Author   : Syeda Fatima
*  Purpose  : This is custom code class for GS processes.
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
V1.0          Syeda                 16-9-2019                To be called by CustomCode_UtilCls

**********************************************************************************************************************/

public without sharing class CC_GSNewCodeClass 
{
    public static string  CC_GSCodeClsMain(Step__c step,string MethodName)
   {    
       system.debug('main method called');
        if(MethodName=='checkRelatedRecords')
        {
            return checkRelatedRecords(step);
        }

       else if(MethodName=='gsoClearanceStep')
       {
            return gsoClearanceStep(step);
       }
        else if(MethodName=='SendMedicalServiceToSAP')
       {
            return SendMedicalServiceToSAP(step);
       }
       
        else return 'Success';
    }
     
  // Start of V.1.0
    public static string checkRelatedRecords(Step__c stp){system.debug('inner method called'); string res = 'Success'; List<Attachment> relatedAttachments = [select id from Attachment where parentId =: stp.Id]; if(relatedAttachments.size() == 0 ){ res = 'Please either attach visa or provide comments first.';} return res;    }
  // End of V.1.0
  // 
 public static string  gsoClearanceStep(Step__c stp)
   {    
       String res = 'Success';
       list <Step__c> stp1 = [Select id from Step__c where Id=:stp.Id and SR__r.Transfer_to_account__r.PSA_Deposit__c != 0 AND (SR__r.Transfer_to_account__r.Index_Card_Status__c !=: 'Not Available' OR SR__r.Transfer_to_account__r.Index_Card_Status__c !=: 'Cancelled') AND (Step_Name__c =: 'Front Desk Review' AND  Step_Status__c =: 'Posted') ];
       if(stp1.size()>0)
       {
           res = 'You cannot mark Government Services true as this Client has either an Active Index Card or the PSA Deposit is not zero or it has an active PO Box or an Active SR';
          
       }
       return res;
   }
       
  public static string SendMedicalServiceToSAP(Step__c objStep)
     {
         
         GsMedicalTestScheduledHandler.MedicalTestScheduled(objStep.Id);
         
          return 'Success';
         
     }
     
}