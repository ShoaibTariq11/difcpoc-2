public class CancelGSrequest 
{
    public static String myString {get; set;}
    public static boolean intStatus{get;set;}
    // public static boolean priItem{get;set;}
    private ApexPages.StandardController sctrl;
    private Service_Request__c SR1;
    public Id SRID =null;
    
    //Constructor 
    public CancelGSrequest(ApexPages.StandardController stdCtrl)
    {
        
        SRID = stdCtrl.getRecord().id;
        this.SR1 = (Service_Request__c)stdCtrl.getRecord();
        this.sctrl = stdCtrl;
        for(Service_Request__c sr : [select Id,Name,Internal_Status_Name__c,SR_Template__r.Menu__c, RecordType.DeveloperName, (Select id,name,status__c from SR_Price_Items1__r) from Service_Request__c where id=:SRID and SR_Group__c='GS'  limit 1])
        {
            this.SR1 = sr;
            if(sr.Internal_Status_Name__c == 'Rejected')
            {
                system.debug('2222instattsu' +sr.Internal_Status_Name__c);
                intStatus = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'The request is already rejected'));
                system.debug('333333333333333333' +intStatus);
            }
            
            
            else if (sr.SR_Price_Items1__r != Null && sr.SR_Price_Items1__r.size() > 0)
            {
                for(SR_Price_Item__c prItem : sr.SR_Price_Items1__r)
                {
                    if(prItem.Status__c == 'Invoiced')
                    {
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Price Item already posted to SAP , hence cannot be cancelled using this functionality'));
                        intStatus = true;
                    }
                }
                                
            }
        }
    }
    
    
    // Code we will invoke on page load.
    public void autoRun() 
    {
        String SRID = ApexPages.currentPage().getParameters().get('id');
        system.debug('!!!!!!ID' +SRID);
        system.debug('intStatus11111' +intStatus);
        Id cancelledStatusId = [Select id from SR_Status__c where name ='Cancelled' limit 1].id;
        Id rejectedStatusId = [Select id from SR_Status__c where Code__c ='REQUEST_REJECTED' limit 1].id;
        SR1.External_SR_Status__c = rejectedStatusId;
        SR1.Internal_SR_Status__c = rejectedStatusId;
        SR1.Is_Cancelled__c = true;
        SR1.Description__c = myString;
        system.debug('!!!!!!!!!!---' +myString);
        
        List<SR_Price_Item__c> lstPrItem = new List <SR_Price_Item__c>();
        if(SR1.SR_Price_Items1__r != Null && SR1.SR_Price_Items1__r.size() > 0 )
        {
            for(SR_Price_Item__c priceItm : SR1.SR_Price_Items1__r)
            {
                if(priceItm.Status__c!='Invoiced')
                {
                    priceItm.status__c = 'Cancelled';
                    lstPrItem.add(priceItm);
                }
                
            }
            if(lstPrItem.size()>0)
            {
                update lstPrItem;
            }
        }
        
        try
        {
            update SR1;
        }
        catch(Exception e)
        {
            e.getMessage();
        }
        
    }
    
}