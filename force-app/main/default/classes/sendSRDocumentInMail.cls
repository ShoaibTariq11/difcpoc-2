/******************************************************************************************
 *  Author     : Selva
 *  Date       : 30-04-2019
 *  Description  : Send SR document in mail after document generated
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    30-04-2019  Selva        Created
****************************************************************************************************************/
global class sendSRDocumentInMail implements Schedulable {
    
   global List<String> srIDS;
    
   global sendSRDocumentInMail (List<Id> srId){
       srIDS = srId;
   } 
   global void execute(SchedulableContext SC) {
   
       callfuturetoSendEmail(srIDS);
        system.debug('---aaa----'+srIDS.size());
   }
   
   public static void callfuturetoSendEmail(List<Id> srId){
        
        
        //while(System.Now().millisecond()< start+1000){ 
        if(srId.size()>0){
            try{
                Map<string,Send_Email_with_Docs__c>  establishmentCardMigrationComp = Send_Email_with_Docs__c.getAll();   
                list<Service_Request__c> listSR = [SELECT Id,Name,Record_Type_Name__c,Email__c,CreatedBy.ContactId,Portal_Service_Request_Name__c,
                Send_SMS_To_Mobile__c,Client_Name__c,Customer__c,Contact__c,Entity_Name__c,Proposed_Trading_Name_1__c,
                Account_Legal_Entity__c,Is_Applicant_Salary_Above_AED_20_000__c,Create_Item__c
                FROM Service_Request__c where id = :srId];     
                OrgWideEmailAddress orgWideNoReply = new OrgWideEmailAddress();
                for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress where Address = 'noreply.portal@difc.ae']) {
                    orgWideNoReply = owa;
                } 
    
                if(listSR != null && listSR.size() > 0){
                    
                    for(Service_Request__c srreq : listSR){
                        //Variable Initializations
                        string emailTemplateName = '';
                        boolean attachDocforChangeOfEntityName = false;
                        set<Id> srDocIds = new set<Id>();
                        set<string> requiredNames = new set<string>();                  
                        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                        list<Messaging.EmailFileAttachment>  attachlist  = new list<Messaging.EmailFileAttachment>();  
                        
                        for(Send_Email_with_Docs__c cs : establishmentCardMigrationComp.values()){
                            if(cs.Record_Type_Name__c == srreq.Record_Type_Name__c && srreq.Account_Legal_Entity__c!=null && srreq.Record_Type_Name__c != 'Freehold_Transfer_Registration'){
                                if(cs.Applicable_Legal_Structure__c.contains(srreq.Account_Legal_Entity__c)){
                                    if(string.isNotBlank(cs.Document_type__c))
                                        requiredNames.addAll(cs.Document_type__c.split(','));
                                    if(string.isNotBlank(cs.Document_Type_2__c))
                                        requiredNames.addAll(cs.Document_Type_2__c.split(','));
                                    if(string.isNotBlank(cs.Document_Type_3__c)){
                                        requiredNames.addAll(cs.Document_Type_3__c.split(','));
                                        }
                                    if(srreq.Record_Type_Name__c=='Application_of_Registration' && srreq.Account_Legal_Entity__c=='LTD SPC'){
                                        emailTemplateName = cs.Email_Template_Name__c;
                                    }
                                    else if(cs.Applicable_Legal_Structure__c!='LTD SPC'){
                                        emailTemplateName = (srreq.Create_Item__c == 'True') ? cs.Email_Template_Name1__c : cs.Email_Template_Name__c;
                                    }
                                    system.debug('---'+cs.Name+'---'+srreq.Create_Item__c+'----'+emailTemplateName +'----'+cs.Email_Template_Name1__c+'----'+cs.Email_Template_Name__c);
                                }
                            }
                            else if(cs.Record_Type_Name__c == srreq.Record_Type_Name__c && srreq.Record_Type_Name__c == 'Freehold_Transfer_Registration'){
                                if(string.isNotBlank(cs.Document_type__c))
                                        requiredNames.addAll(cs.Document_type__c.split(','));
                                    if(string.isNotBlank(cs.Document_Type_2__c))
                                        requiredNames.addAll(cs.Document_Type_2__c.split(','));
                                    if(string.isNotBlank(cs.Document_Type_3__c))
                                        requiredNames.addAll(cs.Document_Type_3__c.split(','));
                                    
                                    emailTemplateName = (srreq.Create_Item__c == 'True') ? cs.Email_Template_Name1__c : cs.Email_Template_Name__c;
                            }

                        }
                        system.debug('------requiredNames--------'+requiredNames);
                        list<SR_Doc__c> doc = [SELECT Id, Name,CreatedById,Service_Request__c FROM SR_Doc__c where Service_Request__c = :srreq.Id and Document_Master__r.Code__c in :requiredNames];            
                        system.debug('--SR doc --'+doc.size());
                        if(srreq.Record_Type_Name__c == 'Change_of_Entity_Name' && (srreq.Entity_Name__c !='' || srreq.Proposed_Trading_Name_1__c!= '')){
                            attachDocforChangeOfEntityName = true;
                        }

                        system.debug('srreq'+ srreq); 
                        
                        system.debug(requiredNames+'requiredNames');
                        
                        for(SR_Doc__c srDoc : doc){
                            srDocIds.add(srDoc.id); 
                        } 
                        
                        list<Attachment> attlist = [SELECT ParentId, Id, Name, Body,contentType FROM Attachment where ParentId = :srDocIds];      
                        for(Attachment att : attlist){
                            Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
                            attach.setContentType(att.contentType);
                            attach.setFileName(att.Name);
                            attach.setInline(True);
                            attach.Body = att.Body;
                            attachlist.add(attach);
                        }     
                        system.debug('attachlist'+attachlist);

                        system.debug('emailTemplateName'+emailTemplateName);       
                        EmailTemplate template = [Select id,Name,DeveloperName from EmailTemplate where DeveloperName =:emailTemplateName ];    
                         
                         if(!Test.isRunningTest()){
                             email.setTargetObjectId(srreq.CreatedBy.ContactId);   
                         }
                            
                         if(srreq.Record_Type_Name__c == 'Freehold_Transfer_Registration'){
                         List<Step__c > stepId= [select id,Step_Name__c ,SR__r.Legal_Structures__c, status__c, SR__c,SR_Record_Type__c from Step__c where SR_Record_Type__c='Freehold_Transfer_Registration' and Step_Name__c='Verification of Application' and SR__c=:srreq.Id];
                             if(stepId.size()>0){
                                 email.setWhatId(stepId[0].ID);  
                             }
                         }else{
                             email.setWhatId(srreq.Id); 
                         }
                         email.setToAddresses(new String[] {srreq.Email__c});
                         system.debug('--srreq.Email__c--'+srreq.Email__c);
                         email.setTemplateId(template.id);
                         email.setSaveAsActivity(false);
                         email.setOrgWideEmailAddressId(orgWideNoReply.id);
                         
                         for(Send_Email_with_Docs__c cs : establishmentCardMigrationComp.values()){
                             if(cs.Record_Type_Name__c == srreq.Record_Type_Name__c && srreq.Record_Type_Name__c != 'Change_of_Entity_Name' ){
                                 email.setFileAttachments(attachlist);
                             }else If(srreq.Record_Type_Name__c == 'Change_of_Entity_Name' && attachDocforChangeOfEntityName){
                                 email.setFileAttachments(attachlist);
                             }
                         }
                         
                         /*
                         if(srreq.Record_Type_Name__c == 'Application_of_Registration')
                            email.setFileAttachments(attachlist);
                         else if(srreq.Record_Type_Name__c == 'Change_of_Entity_Name' && attachDocforChangeOfEntityName)
                            email.setFileAttachments(attachlist);
                        */
                        
                         if(!Test.isRunningTest()){
                            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {Email});
                         }
                    }  // end of for loop for each SR
                }
            
            }catch (Exception e){
                insert LogDetails.CreateLog(null, 'cls_ApprovalNotificationWithAttachment/SendApprovalEmailWithAttachment', 'SR Id is ' + srId + 'Line # '+e.getLineNumber()+'\n'+e.getMessage());
            }           
         
        } // end of main If
        //}
    }
}