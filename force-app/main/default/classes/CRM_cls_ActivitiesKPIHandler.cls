/******************************************************************************************
 *  Name        : CRM_cls_ActivitiesKPIHandler 
 *  Author      : Claude Manahan
 *  Company     : NSI JLT
 *  Date        : 2016-12-13
 *  Description : Utility class for KPI Module of CRM (Tasks/Events)
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   13-12-2016   Claude        Created
 V1.1	25-01-2017	 Claude		   Added new counter for RM Meetings (#3858)
*******************************************************************************************/
public with sharing class CRM_cls_ActivitiesKPIHandler {
    
    /**
     * Creates the KPI records for newly created
     * account records
     * @params      ids                 List of Account Ids
     */
    @InvocableMethod(label='Update KPI Record' description='Creates/Update a KPI record for an Account')
    public static void createKPIForActivities(List<Id> ids){
       CRM_cls_KPI_Utils.upsertKpiRecords(ids, System.Today().year());
    }
    
}