/**
 * Description : Controller for OB_AuthorizedSignatory component
 *
 * ****************************************************************************************
 * History :
 
 V1.1         Sai
 *
 */

public without sharing class OB_QApplicantController {

    public static OB_QApplicantController.ServiceListWrapper respWrap;



    /*Lightning Componenet: OB_QualifiedApplicantDetail
     * Get the existing amendment record.
     */
    @AuraEnabled
    public static ServiceListWrapper viewAmendment(string srId, string amendmentID) {
        System.debug('srId======' + srId);
        System.debug('amendmentID======' + amendmentID);
        //ServiceListWrapper serviceWrapper = new ServiceListWrapper();
        respWrap = new OB_QApplicantController.ServiceListWrapper();
        List<HexaBPM__Service_Request__c> serviceList = [SELECT id, Registered_with_DIFC__c, Qualified_Applicant_Purpose__c, 
                                                         first_name__c, Qualified_Applicant_Entity_Name__c, Select_Qualified_Applicant_Type__c, 
                                                         Trading_Name__c, Former_Name__c, Date_of_Registration__c, Place_of_Registration__c, Business_Sector__c, 
                                                         Country_of_Registration__c, Registered_No__c, Telephone_No__c, Address_Line_1__c, Address_Line_2__c, 
                                                         Country__c, City_Town__c, State_Province_Region__c, Po_Box_Postal_Code__c, Govt_Entity_Name__c, 
                                                         Emirate__c, Decree_No__c, Name_of_the_Fund__c, Registration_No__c, Jurisdiction__c, Financial_Service_Regulator__c, 
                                                         Details_of_the_qualifying_purpose__c, Include_regulated_entity__c, Name_of_the_Regulated_Firm__c, Entity_Type__c, 
                                                         I_Agree_Prescribed__c, RecordType.DeveloperName, (SELECT id, First_Name__c, 
                                                         Last_Name__c, Middle_Name__c, Nationality_list__c, RecordType.DeveloperName, Qualified_Applicant_Type__c, Entity_Name__c, 
                                                         Entity_Name__r.Name, Entity_Name__r.Registration_Number__c, Is_this_Entity_registered_with_DIFC__c, Select_the_Qualified_Applicant_Type__c, 
                                                         Former_Name__c, Financial_Service_Regulator__c, Date_of_Registration__c, 
                                                         Place_of_Registration__c, Country_of_Registration__c, Registered_No__c, 
                                                         Telephone_No__c, Address__c, Apartment_or_Villa_Number_c__c, PO_Box__c, 
                                                         Emirate_State_Province__c, Permanent_Native_City__c, Permanent_Native_Country__c, 
                                                         Govt_Entity_Name__c, Emirate__c, Name_of_the_Fund__c, Registration_No__c, 
                                                         Name_of_the_Fund_Administrator_Manager__c, Trading_Name__c, Jurisdiction__c, Company_Name__c
                                                         FROM Amendments__r where id = :amendmentID and RecordType.DeveloperName = 'Body_Corporate') FROM HexaBPM__Service_Request__c
                                                         WHERE id = :srId];
        if(!serviceList.isEmpty()) {
            respWrap.service = serviceList [0];
            respWrap.amendment = serviceList [0].Amendments__r;
            // respWrap.lookupLabel =  serviceList[0].Amendments__r.Entity_Name__c;
        }
        return respWrap;

    }

    /*Lightning Componenet: OB_QualifiedApplicant
     * Get the amendment record.
     */
    @AuraEnabled
    public static ServiceListWrapper createWrap(string srId, string pageId, String flowId) {
        //String flowId = reqWrap.flowId;
        respWrap = new OB_QApplicantController.ServiceListWrapper();
        List<HexaBPM__Service_Request__c> serviceList = [SELECT id, Registered_with_DIFC__c, Qualified_Applicant_Purpose__c, 
                                                         first_name__c, Qualified_Applicant_Entity_Name__c, Select_Qualified_Applicant_Type__c, 
                                                         Trading_Name__c, Former_Name__c, Date_of_Registration__c, Place_of_Registration__c, 
                                                         Country_of_Registration__c, Registered_No__c, Telephone_No__c, Business_Sector__c, Select_Qualified_Purpose__c, 
                                                         Emirate__c, Decree_No__c, Name_of_the_Fund__c, Registration_No__c, Jurisdiction__c, Financial_Service_Regulator__c, 
                                                         Details_of_the_qualifying_purpose__c, Include_regulated_entity__c, Name_of_the_Regulated_Firm__c, I_Agree_Prescribed__c, 
                                                         HexaBPM__External_SR_Status__c, HexaBPM__External_SR_Status__r.Name, Type_of_Control__c, 
                                                         HexaBPM__Internal_SR_Status__c, HexaBPM__Internal_Status_Name__c, (SELECT id, First_Name__c, Last_Name__c, Middle_Name__c, 
                                                         Nationality_list__c, RecordType.DeveloperName, Qualified_Applicant_Type__c, Please_state_the_name_of_the_Regulator__c, 
                                                         Entity_Name__c, Entity_Name__r.name, Entity_Name__r.Registration_Number__c, Is_this_Entity_registered_with_DIFC__c, Select_the_Qualified_Applicant_Type__c, 
                                                         Former_Name__c, Financial_Service_Regulator__c, Date_of_Registration__c, Place_of_Registration__c, Country_of_Registration__c, 
                                                         Registered_No__c, Telephone_No__c, Address__c, Apartment_or_Villa_Number_c__c, PO_Box__c, Emirate_State_Province__c, 
                                                         Permanent_Native_City__c, Permanent_Native_Country__c, Govt_Entity_Name__c, Emirate__c, Decree_No__c, 
                                                         Name_of_the_Fund__c, Registration_No__c, Name_of_the_Fund_Administrator_Manager__c, Trading_Name__c, Jurisdiction__c, Company_Name__c
                                                         FROM Amendments__r where RecordType.DeveloperName = 'Body_Corporate' AND Role__c INCLUDES('Qualified Applicant')) FROM HexaBPM__Service_Request__c
                                                         WHERE id = :srId];
        if(!serviceList.isEmpty()) {
            respWrap.service = serviceList [0];

            for(HexaBPM_Amendment__c amendObjRel :serviceList [0].Amendments__r) {
                amendWrap amendClass = new amendWrap();
                amendClass.amendobj = amendObjRel;
                respWrap.amendWrapList.add(amendClass);
            }


            for(HexaBPM__Section_Detail__c sectionObj :[SELECT id, HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection'
                                                        AND HexaBPM__Section__r.HexaBPM__Page__c = :pageId LIMIT 1]) {
                respWrap.ButtonSection = sectionObj;
            }

        }

        for(Declarations__mdt declare :[Select ID, Declaration_Text__c, Application_Type__c, Field_API_Name__c, Record_Type_API_Name__c, Object_API_Name__c
                                        From Declarations__mdt
                                        Where  Record_Type_API_Name__c = 'In_Principle'
                                        AND Field_API_Name__c = 'I_Agree_Prescribed__c'
                                        AND Object_API_Name__c = 'HexaBPM__Service_Request__c']) {
            if(String.IsNotBlank(declare.Record_Type_API_Name__c) && declare.Record_Type_API_Name__c.EqualsIgnorecase('In_Principle')) {
                respWrap.SrDeclarationText = declare.Declaration_Text__c;
            }

        }

        for(HexaBPM__SR_Doc__c srDoc :[SELECT id, File_Name__c, HexaBPM__Document_Master__r.HexaBPM__Code__c
                                       FROM HexaBPM__SR_Doc__c
                                       WHERE HexaBPM__Service_Request__c = :srId
                                       AND HexaBPM_Amendment__c = null
                                       AND HexaBPM__Document_Master__r.HexaBPM__Code__c in ('Transaction_structure_diagram')
                                       LIMIT 1
            ]) {
            
                respWrap.uploadedFileName = srDoc.File_Name__c;
             
        }
        
        for(HexaBPM__SR_Doc__c srDoc :[SELECT id, File_Name__c, HexaBPM__Document_Master__r.HexaBPM__Code__c
                                       FROM HexaBPM__SR_Doc__c
                                       WHERE HexaBPM__Service_Request__c = :srId
                                       AND HexaBPM_Amendment__c = null
                                       AND HexaBPM__Document_Master__r.HexaBPM__Code__c in ('Holding_Company_Structure_Diagram')
                                       LIMIT 1
            ]) {
            
                respWrap.HoldingDiagram = srDoc.File_Name__c;
        }
        

        //respWrap.srDoc = viewSRDocs(srId);
        return respWrap;
    }


    /*Lightning Componenet: OB_QualifiedApplicantExisting
     * To remove the existing amendment.
     */
    @AuraEnabled
    public static ServiceListWrapper removeAmendment(String amendmentID, String srId) {

        System.debug('----amendmentID-' + amendmentID);
        respWrap = new OB_QApplicantController.ServiceListWrapper();

        try{


            System.debug('----srId---' + srId);
            if(String.IsNotBlank(amendmentID)) {
                List<HexaBPM_Amendment__c> amendRecordList = [Select ID, Role__c, Recordtype.DeveloperName From HexaBPM_Amendment__c where Id = :amendmentID];
                if(amendRecordList != null && amendRecordList.size() > 0) {
                    if(amendRecordList [0].Role__c == 'Qualified Applicant') {
                        delete amendRecordList [0];
                    } else {
                        list<string> amendRoles = amendRecordList [0].Role__c.split(';');
                        if(amendRoles.indexOf('Qualified Applicant') !=-1) {
                            amendRoles.remove(amendRoles.indexOf('Qualified Applicant'));
                        }
                        amendRecordList [0].Role__c = String.join(amendRoles, ';');
                        update amendRecordList [0];
                    }

                    //Calculate Risk
                    OB_RiskMatrixHelper.CalculateRisk(srId);
                    respWrap = createWrap(srId, '', '');
                    System.debug('------respWrap-----' + respWrap);
                }
            } else {
                respWrap.errorMessage = 'Amendment record is not present';
            }
        } catch(Exception e) {
            string DMLError = e.getMessage();
            respWrap.errorMessage = DMLError;
            system.debug('##########DMLError ' + DMLError);
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,DMLError));

        }
        return respWrap;
    }

    /*Lightning Componenet: OB_QualifiedApplicantdetail
     * Method to save amendment record.
     */
    @AuraEnabled
    //public static onSaveWrapper onAmedSaveDB(HexaBPM__Service_Request__c sericeobj, HexaBPM_Amendment__c amdobj, String pageId, String flowId) {
    public static onSaveWrapper amendSave(String lookupvalue, HexaBPM__Service_Request__c sericeobj, HexaBPM_Amendment__c amdobj, String pageId, String flowId, map<string, string> docMap) {

        System.debug('srId==========save===>');
        OB_QApplicantController.onSaveWrapper saveRespWrap = new OB_QApplicantController.onSaveWrapper();
        saveRespWrap.errorMessage = 'Success';
        boolean isDuplicate = false;
        String srId = sericeobj.Id;
        System.debug('srId==========save===>' + srId);
        HexaBPM_Amendment__c dupAmend = new HexaBPM_Amendment__c();

        try{

            String registrationNo = String.isNotBlank(amdobj.Registration_No__c) ? amdobj.Registration_No__c :'';

            for(HexaBPM_Amendment__c amd :[Select ID, Registration_No__c, 
                                           recordtype.developername, Role__c
                                           From HexaBPM_Amendment__c
                                           Where ServiceRequest__c = :srId
                                           AND (recordtype.developername = 'Body_Corporate'
                                           AND Id != :amdobj.Id)
                                           AND Registration_No__c = :registrationNo
                                           Limit 1]) {
                system.debug('==registrationNo=duplicate== ' + amd.Role__c);
                amdobj.Id = amd.Id;
                if(String.IsNotBlank(amd.Role__c) && amd.Role__c.containsIgnorecase('Qualified Applicant')) {
                    system.debug('==inside dup== ' + amd.Role__c);
                    isDuplicate = true;
                    break;
                }


                if(amd.Role__c != NULL
                   && !amd.Role__c.containsIgnorecase('Qualified Applicant')) {
                    amdobj.Role__c = amd.Role__c + ';Qualified Applicant';

                } 
                else if(amd.Role__c == NULL) {
                    amdobj.Role__c = 'Qualified Applicant';
                    system.debug('#####  reqWrap.amedWrap.amedObj.Role__c alone ' + amdobj.Role__c);
                }


            }


            // duplicate record throw an error
            if(isDuplicate) {
                saveRespWrap.errorMessage = 'This person/corporate already exist';

            } else {
                upsert amdobj;
                System.debug('amdobj+++++++++++++++++++++++++++++' + amdobj); 
                if(sericeobj.Id != null) {
                    update sericeobj;
                }

                saveRespWrap.updatedAmendObj = amdobj;
                System.debug('DIFC+++++++++++++++++++++++++++++' + docMap);
                //Insert SR Docs
                if(docMap != null) {
                    System.debug('docMap+++++++++++++++++++++++++++++' + docMap);



                    OB_AmendmentSRDocHelper.RequestWrapper srDocReq = new OB_AmendmentSRDocHelper.RequestWrapper();
                    OB_AmendmentSRDocHelper.ResponseWrapper srDocResp = new OB_AmendmentSRDocHelper.ResponseWrapper();
                    srDocReq.amedId = amdobj.Id;
                    srDocReq.srId = srId;
                    system.debug('@@##$###'+docMap);
                    srDocReq.docMasterContentDocMap = docMap;
                    system.debug('@@##$###'+docMap);
                    srDocResp = OB_AmendmentSRDocHelper.reparentSRDocsToAmendment(srDocReq);

                    //OB_QApplicantController.saveSRDoc(srId, amdobj.Id, docMap);
                }



                if(String.IsNotBlank(sericeobj.Id)) {
                    // initialize the list again
                    //calculate Risk
                    OB_RiskMatrixHelper.CalculateRisk(sericeobj.Id);

                }
            }
        } catch(DMLException e) {
            saveRespWrap.errorMessage = e.getdmlMessage(0) + '';
            if(e.getdmlMessage(0) == null) {
                saveRespWrap.errorMessage = e.getMessage() + '';
            }

        }

        saveRespWrap.ServiceWrapper = createWrap(srId, pageId, flowId);
        return saveRespWrap;
    }

    /*Lightning Componenet: OB_QualifiedApplicant
     * Method to save Service Request.
     */
    @AuraEnabled
    public static onSaveWrapper saveOnlySr(HexaBPM__Service_Request__c sericeobj, String pageId, String flowId) {

        System.debug('srId==========save===>');
        OB_QApplicantController.onSaveWrapper saveRespWrap = new OB_QApplicantController.onSaveWrapper();
        saveRespWrap.errorMessage = 'Success';
        String srId = sericeobj.Id;
        try{
            // update sericeobj;
            if(String.IsnotBlank(srId)) {

                update sericeobj;

                //Calculate Risk
                OB_RiskMatrixHelper.CalculateRisk(srId);
            } else {
                respWrap.errorMessage = 'Service Request is null';
            }
        } catch(DMLException e) {
            saveRespWrap.errorMessage = e.getdmlMessage(0) + '';
            if(e.getdmlMessage(0) == null) {
                saveRespWrap.errorMessage = e.getMessage() + '';
            }
        }
        saveRespWrap.ServiceWrapper = createWrap(srId, pageId, flowId);
        return saveRespWrap;
    }

    /*Lightning Componenet: OB_QualifiedApplicantDetail
     * Get srDoc of the amendment.
     */

    /*

     public static void saveSRDoc(string srId, string parentId, map<string, string> docMap) {

     list<string> lstDocMasterCodes = new list<string>();
     for (string docMasterCode :docMap.keySet())
     lstDocMasterCodes.add(docMasterCode);

     map<string, string> mapDocMaster = new map<string, string>();
     for(HexaBPM__Document_Master__c docMaster :[select id, HexaBPM__Code__c from HexaBPM__Document_Master__c where HexaBPM__Code__c in :lstDocMasterCodes]) {
     mapDocMaster.put(docMaster.id, docMaster.HexaBPM__Code__c);
     }
     //delete the previous SR Docs for the same parent and documentMasterCode
     if(lstDocMasterCodes != null && lstDocMasterCodes.size() > 0)
     delete [select id from HexaBPM__SR_Doc__c where HexaBPM_Amendment__c = :parentId and HexaBPM__Document_Master__r.HexaBPM__Code__c in :lstDocMasterCodes];

     list<HexaBPM__SR_Doc__c> lstSRDocs = new list<HexaBPM__SR_Doc__c>();
     OB_AmendmentSRDocHelper.RequestWrapper reqWrap = new OB_AmendmentSRDocHelper.RequestWrapper();
     reqWrap.srId = srId;
     reqWrap.amedId = parentId;
     reqWrap.docMasterContentDocMap = docMap;
     OB_AmendmentSRDocHelper.ResponseWrapper repatenResp = new OB_AmendmentSRDocHelper.ResponseWrapper();
     repatenResp = OB_AmendmentSRDocHelper.reparentSRDocsToAmendment(reqWrap);
     //string srDocumentId = OB_QueryUtilityClass.createCompanyNameSRDoc(srId, parentId, documentMasterCode);
     list<ContentDocumentLink> lstContentDocumentLink = new list<ContentDocumentLink>();
     string docMasterCode;
     for(HexaBPM__SR_Doc__c srDoc :lstSRDocs) {
     System.debug('mapDocMaster.get(srDoc.HexaBPM__Document_Master__c)' + mapDocMaster.get(srDoc.HexaBPM__Document_Master__c));
     docMasterCode = mapDocMaster.get(srDoc.HexaBPM__Document_Master__c);
     if(docMasterCode != null && docMap.get(docMasterCode) != null) {
     ContentDocumentLink cDe = new ContentDocumentLink();
     cDe.ContentDocumentId = docMap.get(docMasterCode);
     cDe.LinkedEntityId = srDoc.Id; // you can use objectId,GroupId etc
     cDe.ShareType = 'I'; // Inferred permission, checkout description of ContentDocumentLink object for more details
     cDe.Visibility = 'AllUsers';
     lstContentDocumentLink.add(cDe);
     }
     }
     if(lstContentDocumentLink != null && lstContentDocumentLink.size() > 0)
     insert lstContentDocumentLink;

     //Delete the DocumentLink from the SR - Cleanup of Files
     delete [select id from ContentDocumentLink where ContentDocumentId in :docMap.values() and LinkedEntityId = :srId];

     }
     */


    /*Lightning Componenet: OB_QualifiedApplicantExistintg
     * Get the SrDoc on click of view Details.
     */
    @AuraEnabled
    public static map<string, map<string, string>> viewSRDocs(string srId, string amendmentId) {
        //Preparing the SRDocs for the amendment
        map<string, map<string, string>> mapSRDocs = new map<string, map<string, string>>();
        map<string, string> mapIdName = new map<string, string>();
        if(String.isNotBlank(amendmentId)) {
            for(HexaBPM__SR_Doc__c srDoc :[SELECT id, File_Name__c, HexaBPM__Document_Master__r.HexaBPM__Code__c
                                           FROM HexaBPM__SR_Doc__c
                                           WHERE HexaBPM__Service_Request__c = :srId
                                           AND HexaBPM_Amendment__c = :amendmentId
                                           AND HexaBPM__Document_Master__r.HexaBPM__Code__c in('Certificate_of_Incorporation')
                                           LIMIT 1
                ]) {

                mapIdName.put('Id', srDoc.id);
                mapIdName.put('FileName', srDoc.File_Name__c);
                mapSRDocs.put(srDoc.HexaBPM__Document_Master__r.HexaBPM__Code__c, mapIdName);

            }
        }

        return mapSRDocs;
    }
    /*Lightning Componenet: OB_QualifiedApplicant
     * Method to initialse the amend wrappper.
     */
    @AuraEnabled
    public static amendWrap initAmendment(String srId) {

        amendWrap respWrap = new amendWrap();



        HexaBPM_Amendment__c amedObj = new HexaBPM_Amendment__c();
        amedObj.ServiceRequest__c = srId;
        amedObj.RecordTypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Body_Corporate');
        amedObj.Role__c = 'Qualified Applicant';

        respWrap.amendobj = amedObj;

        return respWrap;
    }

    // wrapper class with @AuraEnabled and {get;set;} properties
    public class onSaveWrapper {
        @AuraEnabled
        public String errorMessage { get; set; }
        @AuraEnabled
        public ServiceListWrapper ServiceWrapper { get; set; }
        @AuraEnabled
        public HexaBPM_Amendment__c updatedAmendObj { get; set; }
        @AuraEnabled
        public string testing { get; set; }

        public onSaveWrapper() {
            ServiceWrapper = new ServiceListWrapper();
        }
    }

    // wrapper class with @AuraEnabled and {get;set;} properties
    public class amendWrap {
        @AuraEnabled
        public HexaBPM_Amendment__c amendobj { get; set; }


        public amendWrap() {
            //amendobj = new HexaBPM_Amendment__c();
        }
    }

    // wrapper class with @AuraEnabled and {get;set;} properties
    public class ServiceListWrapper {
        @AuraEnabled
        public HexaBPM__Service_Request__c service { get; set; }
        @AuraEnabled
        public HexaBPM_Amendment__c amendment { get; set; }
        @AuraEnabled
        public list<amendWrap> amendWrapList { get; set; }
        @AuraEnabled
        public string uploadedFileName { get; set; }
        
        @AuraEnabled
        public string HoldingDiagram { get; set; }     //V1.1

        // wrapper class with @AuraEnabled and {get;set;} properties
        @AuraEnabled
        public map<string, map<string, string>> srDoc { get; set; }
        @AuraEnabled
        public String errorMessage { get; set; }
        @AuraEnabled
        public id Amendmentid { get; set; }
        @AuraEnabled public String lookupLabel { get; set; }
        @AuraEnabled
        public HexaBPM__Section_Detail__c ButtonSection { get; set; }
        @AuraEnabled public Boolean isDraft { get; set; }
        @AuraEnabled public string viewSRURL { get; set; }
        @AuraEnabled public String SrDeclarationText { get; set; }



        public ServiceListWrapper() {
            amendWrapList = new list<amendWrap>();

        }

    }

    public static void saveSRDoc(string srId, map<string, string> docMap) {

        //delete the previous SR Docs for the same parent and documentMasterCode
        if(docMap != null && docMap.size() > 0)
            delete [select id from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c = :srId and HexaBPM__Document_Master__r.HexaBPM__Code__c in :docMap.keySet()];
        system.debug('@@#$$$$$##'+docMap+'))))((()))'+srId);
        OB_QueryUtilityClass.createCompanySRDoc(srId, null, docMap);
    }


    @AuraEnabled
    public static ButtonResponseWrapper getButtonAction(string SRID, string ButtonId, string pageId, HexaBPM__Service_Request__c srOBj, map<string, string> docMap) {
        ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
        if(srOBj != null) {
            try {
                if(srOBj.Qualified_Applicant_Purpose__c == 'Qualified Purpose') {

                    srOBj.Type_of_Control__c = '';
                    update srOBj;

                } else {
                    update srOBj;
                }
                system.debug('@@#$$$$$##'+docMap);
                if(docMap != null) {
                    saveSRDoc(srId, docMap);
                }



                if(srOBj.Qualified_Applicant_Purpose__c == 'Qualified Purpose') {

                    list<HexaBPM_Amendment__c> amendListToBeDeleted = new list<HexaBPM_Amendment__c>();
                    list<HexaBPM_Amendment__c> amendListToBeUpdated = new list<HexaBPM_Amendment__c>();

                    for(HexaBPM_Amendment__c amendObj :[select id, Role__c from HexaBPM_Amendment__c WHERE RecordType.DeveloperName = 'Body_Corporate' AND Role__c INCLUDES('Qualified Applicant')
                                                        AND ServiceRequest__c = :srId]) {
                        if(amendObj.Role__c == 'Qualified Applicant') {
                            amendListToBeDeleted.add(amendObj);
                        } else {
                            list<string> amendRoles = amendObj.Role__c.split(';');
                            if(amendRoles.indexOf('Qualified Applicant') !=-1) {
                                amendRoles.remove(amendRoles.indexOf('Qualified Applicant'));
                            }
                            amendObj.Role__c = String.join(amendRoles, ';');
                            amendListToBeUpdated.add(amendObj);
                        }
                    }

                    if(amendListToBeDeleted.size() > 0) {
                        delete amendListToBeDeleted;
                    }
                    if(amendListToBeUpdated.size() > 0) {
                        update amendListToBeUpdated;
                    }

                }

            } catch(Exception e) {
                string DMLError = e.getdmlMessage(0) + '';
                if(DMLError == null) {
                    DMLError = e.getMessage() + '';
                }
                respWrap.errorMessage = DMLError;
            }

        }

        HexaBPM__Service_Request__c objRequest = OB_QueryUtilityClass.QueryFullSR(SRID);
        /* if(typeofControl != null){
         objRequest.Type_of_Control__c = typeofControl;
         } */


        objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, SRID, pageId)
            ;
        upsert objRequest;
        //calculate risk
        OB_RiskMatrixHelper.CalculateRisk(objRequest.Id);

        PageFlowControllerHelper.objSR = objRequest;
        PageFlowControllerHelper objPB = new PageFlowControllerHelper();

        PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
        system.debug('@@@@@@@@2 responseNextPage ' + responseNextPage);
        respWrap.pageActionName = responseNextPage.pg;
        respWrap.communityName = responseNextPage.communityName;
        respWrap.CommunityPageName = responseNextPage.CommunityPageName;
        respWrap.sitePageName = responseNextPage.sitePageName;
        respWrap.strBaseUrl = responseNextPage.strBaseUrl;
        respWrap.srId = objRequest.Id;
        respWrap.flowId = responseNextPage.flowId;
        respWrap.pageId = responseNextPage.pageId;
        respWrap.isPublicSite = responseNextPage.isPublicSite;

        system.debug('@@@@@@@@2 respWrap.pageActionName ' + respWrap.pageActionName);
        return respWrap;
    }
    public class ButtonResponseWrapper {
        @AuraEnabled public String pageActionName { get; set; }
        @AuraEnabled public string communityName { get; set; }
        @AuraEnabled public String errorMessage { get; set; }
        @AuraEnabled public string CommunityPageName { get; set; }
        @AuraEnabled public string sitePageName { get; set; }
        @AuraEnabled public string strBaseUrl { get; set; }
        @AuraEnabled public string srId { get; set; }
        @AuraEnabled public string flowId { get; set; }
        @AuraEnabled public string pageId { get; set; }
        @AuraEnabled public boolean isPublicSite { get; set; }

    }
}