Public class SRDocTriggerHandler implements TriggerFactoryInterface {

	
	public void executeBeforeInsertTrigger(list<sObject> lstNewRecords){
        
    }
	
	public void executeBeforeUpdateTrigger(list<sObject> lstNewRecords,map<Id,sObject> mapOldRecords){
            
    }
    
    public void executeBeforeInsertUpdateTrigger(list<sObject> lstNewRecords,map<Id,sObject> mapOldRecords){
        
    } 
    
    public void executeAfterInsertTrigger(list<sObject> lstNewRecords){
    	list<SR_Doc__c> newRecords = (List<SR_Doc__c>)lstNewRecords;
    	InvalidateSRDocs(newRecords);
    	
    }
    
    public void executeAfterUpdateTrigger(list<sObject> lstNewRecords,map<Id,sObject> mapOldRecords){
      
    }
    
    public void executeAfterInsertUpdateTrigger(list<sObject> lstNewRecords,map<Id,sObject> mapOldRecords){
    	
    } 
    
    public void InvalidateSRDocs(List<SR_Doc__c> srDocToInsert){
        set<id> customerSet = new set<id>();
        set<id> srDocSet;
        system.debug('<---------------------------------------->');
    	if(srDocToInsert !=null && srDocToInsert.size() >0){   		
    		// Check custom setting to identify whether the document in current context should be considered for changes
    		Expire_ELicenses__c expEl  = Expire_ELicenses__c.getOrgDefaults();
    		srDocSet = new set<id>();
    		set<string> srRecTyp = new set<string>();
    		set<string> srDocNames = new set<string>();
    		if(expEl !=null && expEl.Service_Request_Record_Type__c !=null)
    			srRecTyp.addAll(expEl.Service_Request_Record_Type__c.split(';'));
    		if(expEl !=null && expEl.SR_Doc_to_Expire__c !=null)	
    			srDocNames.addAll(expEl.SR_Doc_to_Expire__c.split(';'));
    		for(SR_Doc__c srd :srDocToInsert){
    			if(srDocNames.size()>0 && srDocNames.contains(srd.Name)){
    				customerSet.add(srd.Customer__c);    
    				srDocSet.add(srd.id);				
    			}
    		}    		
    		if(customerSet !=null && srDocSet !=null && customerSet.size()>0 && srDocSet.size()>0){
    			List<SR_Doc__c> srdocsToUpdate = new List<SR_Doc__c>();
    			for(sr_Doc__c srd : [select id,customer__c from SR_Doc__c where Customer__c in : customerSet  and id not in: srDocSet and SR_RecordType_Name__c in :srRecTyp and Document_Validity__c = true and Name in : srDocNames]){
    				srd.Sys_Expire_Document__c= true;
    				srdocsToUpdate.add(srd);
    			}
    			
    			if(srdocsToUpdate !=null && srdocsToUpdate.size()>0)
    				update srdocsToUpdate;   			
    			
    		}    		
    	}    	
    }
    
   
   
}