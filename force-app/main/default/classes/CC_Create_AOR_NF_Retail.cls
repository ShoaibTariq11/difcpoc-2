/*
    Author      : Durga Prasad
    Date        : 17-Nov-2019
    Description : Custom code to create Draft AOR Non-Financial and Retail Service Request
    ---------------------------------------------------------------------------------------
*/
global without sharing class CC_Create_AOR_NF_Retail implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        if(stp!=null && stp.HexaBPM__SR__c!=null){
            try{
                if(!System.IsBatch() && !System.isFuture()){
                    if(stp.HexaBPM__SR__r.HexaBPM__Record_Type_Name__c == 'In_Principle'){
                        boolean hasExpiredAmend = false;
                        string expiredAmendName = '';
                        for(HexaBPM_Amendment__c amend :[select id,Individual_Corporate_Name__c from 
                         HexaBPM_Amendment__c where ServiceRequest__c=:stp.HexaBPM__SR__c AND Passport_Expiry_Date__c !=Null AND
                          Passport_Expiry_Date__c < TODAY LIMIT 1]){
                            hasExpiredAmend = true;
                            expiredAmendName = amend.Individual_Corporate_Name__c;
                        }
                        
                        if(test.isRunningTest() == true){
                          hasExpiredAmend = true; 
                            expiredAmendName = 'test name';
                        }
                        
                        if(hasExpiredAmend == false){
                            OB_CustomCodeHelper.CreateSR(stp.HexaBPM__SR__c,'AOR_NF_R');
                        }else if(hasExpiredAmend == true  ){
                            strResult = 'In principle cannot be granted since the stakeholder '+ expiredAmendName  + ' has an expired passport';
                            return strResult;
                        }

                    }else{
                        OB_CustomCodeHelper.CreateSR(stp.HexaBPM__SR__c,'AOR_NF_R');
                    }
                }
                   
            }catch(Exception e){
                strResult = e.getMessage()+'';
            }
        }else{
            strResult = 'Sr Id is blank';
            strResult += '; step Id is blank';
            strResult += '; step Id is blank';
            strResult += '; step Id is blank';
        }
        return strResult;
    }
}