/******************************************************************************************
 *  Class Name  : FlashNewsController
 *  Author      : Sai
 *  Company     : DIFC
 *  Date        : 16 Dec 2019        
 *  Description :                
 ----------------------------------------------------------------------
   Version     Date              Author                Remarks                                                 
  =======   ==========        =============    ==================================
    v1.1   16 Dec 2019           Sai                  Initial Version  
	V1.2   30 Jan 2020           Sai                   
	v1.3   16 March 2020         Merul               Chnges for role based structure
*******************************************************************************************/
public without sharing class FlashNewsController {
	
	@AuraEnabled
    public static List<FlashNewsWrapper> getFlashNews(){
    	
    	User eachUser = new User();
    	FlashNewsWrapper flashNews = new FlashNewsWrapper();
    	List<FlashNewsWrapper> lstFlashNews = new List<FlashNewsWrapper>();
    	List<Service_Notification__c> lstServiceNotification = new List<Service_Notification__c>();
    	List<String> lstNotification = new List<String>();
    	List<String> lstContractorRole = new List<String>();
		lstContractorRole = Label.Contractors_Role.split(',');
		String commutyUsrRole = '';
		
		eachUser  = [
					 SELECT Community_User_Role__c,
							Contact.Id,
							Contact.Account.Id,
							contact.Contractor__r.Recordtype.DeveloperName,
							OB_Is_New_Community_Profile__c 
					   FROM User 
					  WHERE ID=:userinfo.getUserId()
					];

		// v1.3   16 March 2020         Merul               Chnges for role based structure
		if( eachUser.OB_Is_New_Community_Profile__c )
		{
			
			for( AccountContactRelation accContactRel : [
																SELECT Id,
																	   AccountId,
																	   ContactId,
																	   Roles,
																	   Access_Level__c 
																FROM AccountContactRelation
															   WHERE ContactId =: eachUser.Contact.Id
																 AND AccountId =: eachUser.Contact.Account.Id
														]
				)
				{
					commutyUsrRole = accContactRel.Roles;
					//hasSRAccess = ( accContactRel.Access_Level__c == 'Read/Write' ? true : false);
					//commutyUsrRole = eachUser.Community_User_Role__c;
				}

				
		}
		else 
		{
					commutyUsrRole = eachUser.Community_User_Role__c;
		}
		system.debug('@@@@@@@@@ commutyUsrRole '+commutyUsrRole);
    	
		lstServiceNotification = [
										SELECT Department__c,Notification_Details__c 
										  FROM Service_Notification__c 
										 WHERE Department__c !='' 
										   AND is_Active__c = true 
									  ORDER BY Sort_Order__c ASC
								 ];
								 
		system.debug('@@@@@@@@@ lstServiceNotification '+lstServiceNotification);
    		
		//system.debug('!!@@!!!'+eachUser.Community_User_Role__c);	
		// v1.3   16 March 2020         Merul               Chnges for role based structure
		//if(string.isNotBlank(eachUser.Community_User_Role__c))
		if( string.isNotBlank( commutyUsrRole ) )
		{
			// v1.3   16 March 2020         Merul               Chnges for role based structure
			//if(eachUser.Community_User_Role__c.contains(Label.Company_Services))
			system.debug('@@@@@@@@@ Label.Company_Services '+Label.Company_Services);
			if( commutyUsrRole.contains(Label.Company_Services)  )
			{
    			flashNews = new FlashNewsWrapper();
				lstNotification = new List<String>();
				for(Service_Notification__c eachNotification:lstServiceNotification){
					if(eachNotification.Department__c == Label.Company_Services){
						lstNotification.add(eachNotification.Notification_Details__c );
					}
				}
				flashNews.Department = Label.Company_Services;
            	flashNews.Notification_Details= lstNotification;
            	lstFlashNews.add(flashNews);
			}
    	
    	
			if(commutyUsrRole.contains(Label.Employee_Services))
			{
	    		flashNews = new FlashNewsWrapper();
				lstNotification = new List<String>();	
				for(Service_Notification__c eachNotification:lstServiceNotification){
					if(eachNotification.Department__c == Label.Employee_Services){
						lstNotification.add(eachNotification.Notification_Details__c );
					}
				}
				flashNews.Department = Label.Employee_Services;
	        	flashNews.Notification_Details= lstNotification;
	        	lstFlashNews.add(flashNews);
			}
			
			if(commutyUsrRole.contains(Label.Property_Services))
			{
				flashNews = new FlashNewsWrapper();
				lstNotification = new List<String>();
				for(Service_Notification__c eachNotification:lstServiceNotification){
					if(eachNotification.Department__c == Label.Property_Services){
						lstNotification.add(eachNotification.Notification_Details__c );
					}
				}
				flashNews.Department = Label.Property_Services;
	        	flashNews.Notification_Details= lstNotification;
	        	lstFlashNews.add(flashNews);
			}

    	}
    	
		if(String.isNotBlank(eachUser.contact.Contractor__r.RecordType.DeveloperName) 
				&& lstContractorRole.contains(eachUser.contact.Contractor__r.RecordType.DeveloperName))
		{
        	
				flashNews = new FlashNewsWrapper();
				lstNotification = new List<String>();
				for(Service_Notification__c eachNotification:lstServiceNotification)
				{
					if(eachNotification.Department__c == Label.Fitout_Services)
					{
						lstNotification.add(eachNotification.Notification_Details__c );
					}
				}
				flashNews.Department = Label.Fitout_Services;
				flashNews.Notification_Details= lstNotification ;
				lstFlashNews.add(flashNews);
		}

		system.debug('@@@@@@@@@ lstFlashNews '+lstFlashNews);
		return lstFlashNews;
    }
	
	public class FlashNewsWrapper{
        
        @AuraEnabled
        public String Department{get;set;}
        @AuraEnabled
        public List<String> Notification_Details {get;set;}
    }
}