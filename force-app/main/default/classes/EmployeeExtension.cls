/******************************************************************************************
 *  Author   : Shoaib Tariq
 *  Company  : 
 *  Date     : 4/28/2020
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    4/28/2020  shoaib    Created
**********************************************************************************************************************/
public class EmployeeExtension {
    
    public List<Cancelled_Employees__c> wrapAccountList {get;set;}
        
    Public EmployeeExtension(ApexPages.StandardController controller){
        wrapAccountList = getEmployeeList(apexpages.currentpage().getparameters().get('Id'));
    }
    /**
     * getEmployeeList
     * recordId
     * List<Cancelled_Employees__c>
     */
    public static List<Cancelled_Employees__c> getEmployeeList(String recordId){
        return[SELECT Id,
               			Designation__c,
               			Email__c,
               			Experience_In_Years__c,
               			First_Name__c,Last_Name__c,
               			Mobile__c,
                        Passport_Number__c,
               			Service_Request__c 
               FROM Cancelled_Employees__c
               WHERE Service_Request__c =:recordId];
    }
    

}