/**
* An apex page controller that exposes the site login functionality
*/
global with sharing class CommunitiesLoginController {
    
    @AuraEnabled
    global static String checkPortal( String username, String password ) {
        try {
            ApexPages.PageReference lgn = Site.login(username, password, '');
           // system.debug('====lgn===='+lgn);
            aura.redirect(lgn);
           
            	return null;
            
        }
        catch (Exception ex) {
            return ex.getMessage();
        }
    }
    
    global CommunitiesLoginController () {}
    
    // Code we will invoke on page load.
    global PageReference forwardToAuthPage() {
        System.debug('login--');
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        String displayType = System.currentPageReference().getParameters().get('display');
        return Network.forwardToAuthPage(startUrl, displayType);
    }
}