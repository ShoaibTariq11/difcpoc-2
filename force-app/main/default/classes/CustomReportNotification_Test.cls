@IsTest(SeeAllData=true)
private class CustomReportNotification_Test{
      
       @IsTest(SeeAllData=true) 
      private static void testReportNotification() {
         
        Report report = [SELECT Id FROM Report limit 1];
        Test.startTest();
        
        insert new CustomEmailNotificaition__c(Name = 'test',subject__c = 'test',body__c = 'test',email_addresses__c='abc@hotmail.com');
        
        Reports.ReportInstance reportInstance = Reports.ReportManager.runAsyncReport(report.Id, true);
        
        Test.stopTest();
        
        Custom_ReportNotification objCustomNotify = new Custom_ReportNotification ();     
        Reports.NotificationActionContext context = 
            new Reports.NotificationActionContext(
                reportInstance, 
                new Reports.ThresholdInformation( 
                    new List<Reports.EvaluatedCondition> {new Reports.EvaluatedCondition('RecordCount', 
                               'Record Count', Double.valueOf(0), Double.valueOf(1), '0!T','0!T',
                                Reports.EvaluatedConditionOperator.GREATER_THAN) }));
        objCustomNotify .execute(context);
        
     
     
     }
        
}