/**-------------------------------------------------------------------
 * Description : Controller for OB_CommercialPermission component
 * -------------------------------------------------------------------
 * History :
 * [03.NOV.2020] Prateek Kadkol - Code Creation
 * -------------------------------------------------------------------
*/
public without sharing class OB_CommercialPermissionController {
    
    @AuraEnabled
	public static RespondWrap fetchAmendRec(String requestWrapParam) {

		//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();

		//deseriliaze.
		reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
		system.debug(reqWrap);


		respWrap.recordId = Schema.SObjectType.HexaBPM_Amendment__c.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
		respWrap.amendWrapp = FetchUpdatedAmendListHelper(reqWrap.srId);

		for(HexaBPM__Section_Detail__c sectionObj :[SELECT id, HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection'
												    AND HexaBPM__Section__r.HexaBPM__Page__c = :reqWrap.pageId LIMIT 1]) {
			respWrap.ButtonSection = sectionObj;
		}

		
		return respWrap;

	}

	@AuraEnabled
	public static RespondWrap initNewAmendment(String requestWrapParam) {

		//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();

		//deseriliaze.
		reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);

		string recordId = Schema.SObjectType.HexaBPM_Amendment__c.getRecordTypeInfosByName().get('Individual').getRecordTypeId();

		respWrap.recordId = recordId;




		HexaBPM_Amendment__c amedObj = new HexaBPM_Amendment__c();
		amedObj.ServiceRequest__c = reqWrap.srId;
		amedObj.RecordTypeId = recordId;
		amedObj.Role__c = 'Authorised Individual';

		respWrap.newAmendToCreate = amedObj;

		return respWrap;
	}

	@AuraEnabled
	public static amendListWrapp FetchUpdatedAmendListHelper(String srId) {

		amendListWrapp respWrap = new amendListWrapp();

		string recordId = Schema.SObjectType.HexaBPM_Amendment__c.getRecordTypeInfosByName().get('Individual').getRecordTypeId();

		for (HexaBPM_Amendment__c amendObj :[SELECT id, Name__c, Title__c, First_Name__c, Middle_Name__c, Last_Name__c, 
										     Former_Name__c, Passport_No__c, Nationality_list__c, Date_of_Birth__c, 
										     Gender__c, Place_of_Birth__c, Passport_Issue_Date__c, Passport_Expiry_Date__c, 
										     Place_of_Issue__c, Telephone_No__c, Email__c, Is_this_individual_a_PEP__c, Address__c, 
										     PO_Box__c, ServiceRequest__c, Type_of_Authority__c,Please_provide_the_type_of_authority__c,
										     Apartment_or_Villa_Number_c__c, Building_Name__c, Permanent_Native_Country__c, 
										     Permanent_Native_City__c, Emirate_State_Province__c, Certified_passport_copy__c, 
										     Post_Code__c, Role__c FROM HexaBPM_Amendment__c WHERE ServiceRequest__c = :srId AND
										     RecordTypeId = :recordId AND Role__c != null]) {

			list<string> amendRole = amendObj.Role__c.split(';');

			if(amendRole.contains('Authorised Individual') && amendRole.size() == 1) {
				respWrap.onlyCurrentRoleAmendList.add(amendObj);
			} else if(amendRole.contains('Authorised Individual') && amendRole.size() > 1) {
				respWrap.currentRoleAndOtherAmendList.add(amendObj);
			} else if(!amendRole.contains('Authorised Individual') && amendRole.size() > 0 && !amendRole.contains('DPO') && !amendRole.contains('Operating Location')) {
				respWrap.otherRolesAmendList.add(amendObj);
			}
		}

		return respWrap;

	}

	@AuraEnabled
	public static RespondWrap amendRemoveAction(String requestWrapParam) {

		//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();

		//deseriliaze.
		reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
		system.debug(reqWrap);

		HexaBPM_Amendment__c amendObjToUpdate = reqWrap.amendObj;

		try {
			if(reqWrap.isAssigned) {
				list<string> amendRoles = amendObjToUpdate.Role__c.split(';');
				if(amendRoles.indexOf('Authorised Individual') !=-1) {
					amendRoles.remove(amendRoles.indexOf('Authorised Individual'));
				}
				amendObjToUpdate.Role__c = String.join(amendRoles, ';');
				update amendObjToUpdate;

			} else {
				delete amendObjToUpdate;
			}
			respWrap.amendWrapp = FetchUpdatedAmendListHelper(reqWrap.srId);
			respWrap.errorMessage = 'no error recorded';
			respWrap.updatedRec = amendObjToUpdate;
		} catch(exception e) {
			respWrap.errorMessage = e.getMessage();
		}


		return respWrap;
	}

	@AuraEnabled
	public static RespondWrap amenSaveAction(String requestWrapParam) {

		//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();
		boolean isDuplicate = false;

		//deseriliaze.
		reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
		system.debug(reqWrap);

		HexaBPM_Amendment__c amendObj = reqWrap.amendObj;
		//amendObj.id = reqWrap.amendId;

		String passport = String.isNotBlank(amendObj.Passport_No__c) ? amendObj.Passport_No__c :'';
		String nationality = String.IsNotBlank(amendObj.Nationality_list__c) ? amendObj.Nationality_list__c.toLowerCase() :'';


		for(HexaBPM_Amendment__c amd :[Select ID, First_Name__c, Passport_No__c, Nationality_list__c, 
									   recordtype.developername, Role__c
									   FROM HexaBPM_Amendment__c
									   WHERE ServiceRequest__c = :amendObj.ServiceRequest__c
									   AND (recordtype.developername = 'Individual'
									   AND Id != :amendObj.Id)
									   AND Passport_No__c = :passport
									   AND Nationality_list__c = :nationality
									   Limit 1]) {

			amendObj.Id = amd.Id;
			// For director dup check.
			if(String.IsNotBlank(amd.Role__c) && amd.Role__c.containsIgnorecase('Authorised Individual')) {

				isDuplicate = true;
				break;
			}

			if(amd.Role__c != NULL
			   && !amd.Role__c.containsIgnorecase('Authorised Individual')) {
				amendObj.Role__c = amd.Role__c + ';Authorised Individual';
			} 
			else if(amd.Role__c == NULL) {
				amendObj.Role__c = 'Authorised Individual';
			}


		}


		if(isDuplicate) {
			respWrap.errorMessage = 'This person already exist';
		} else {



			try {

				upsert amendObj;
				//Insert SR Docs
				/* if(reqWrap.docMap != null) {

					Map<String, String> docMasterContentDocMap = reqWrap.docMap;
					String srId = reqWrap.srId;
					String amendID = amendObj.Id;


					OB_AmendmentSRDocHelper.RequestWrapper srDocReq = new OB_AmendmentSRDocHelper.RequestWrapper();
					OB_AmendmentSRDocHelper.ResponseWrapper srDocResp = new OB_AmendmentSRDocHelper.ResponseWrapper();
					srDocReq.amedId = amendObj.Id;
					srDocReq.srId = srId;
					srDocReq.docMasterContentDocMap = docMasterContentDocMap;

					srDocResp = OB_AmendmentSRDocHelper.reparentSRDocsToAmendment(srDocReq);
					
				} */
				respWrap.amendWrapp = FetchUpdatedAmendListHelper(reqWrap.srId);
				respWrap.errorMessage = 'no error recorded';
				respWrap.updatedRec = amendObj;
			} catch(DMLException e) {
				string DMLError = e.getdmlMessage(0) + '';
				if(DMLError == null) {
					DMLError = e.getMessage() + '';
				}
				respWrap.errorMessage = DMLError;
			}
		}
		return respWrap;
	}


	public class RequestWrap {
		@AuraEnabled
		public String flowId { get; set; }

		@AuraEnabled
		public String pageId { get; set; }

		@AuraEnabled
		public String srId { get; set; }

		@AuraEnabled
		public String amendId { get; set; }

		@AuraEnabled
		public HexaBPM_Amendment__c amendObj { get; set; }

		@AuraEnabled
		public Boolean isAssigned { get; set; }

		@AuraEnabled
		public map<string, string> docMap { get; set; }


		public RequestWrap() {
		}
	}

	public class RespondWrap {

		@AuraEnabled
		public amendListWrapp amendWrapp { get; set; }

		@AuraEnabled
		public string errorMessage { get; set; }

		@AuraEnabled
		public string recordId { get; set; }

		@AuraEnabled
		public HexaBPM_Amendment__c updatedRec { get; set; }

		@AuraEnabled
		public HexaBPM_Amendment__c newAmendToCreate { get; set; }

		@AuraEnabled
		public HexaBPM__Section_Detail__c ButtonSection { get; set; }

		@AuraEnabled public SRWrapper srWrap { get; set; }

		public RespondWrap() {
			AmendWrapp = new amendListWrapp();
		}
	}

	public class amendListWrapp {

		@AuraEnabled
		public list<HexaBPM_Amendment__c> otherRolesAmendList { get; set; }

		@AuraEnabled
		public list<HexaBPM_Amendment__c> currentRoleAndOtherAmendList { get; set; }

		@AuraEnabled
		public list<HexaBPM_Amendment__c> onlyCurrentRoleAmendList { get; set; }


		public amendListWrapp() {

			otherRolesAmendList = new list<HexaBPM_Amendment__c>();
			currentRoleAndOtherAmendList = new list<HexaBPM_Amendment__c>();
			onlyCurrentRoleAmendList = new list<HexaBPM_Amendment__c>();

		}
	}

	// dynamic button section
	@AuraEnabled
	public static ButtonResponseWrapper getButtonAction(string SRID, string ButtonId, string pageId) {


		ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
		HexaBPM__Service_Request__c objRequest = OB_QueryUtilityClass.QueryFullSR(SRID);
		PageFlowControllerHelper.objSR = objRequest;
		PageFlowControllerHelper objPB = new PageFlowControllerHelper();

		respWrap.tester = OB_QueryUtilityClass.minRecValidationCheck(SRID, 'Authorised Individual');
		objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, SRID, pageId);
		upsert objRequest;

		OB_RiskMatrixHelper.CalculateRisk(SRID);

		PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
		system.debug('@@@@@@@@2 responseNextPage ' + responseNextPage);
		respWrap.pageActionName = responseNextPage.pg;
		respWrap.communityName = responseNextPage.communityName;
		respWrap.CommunityPageName = responseNextPage.CommunityPageName;
		respWrap.sitePageName = responseNextPage.sitePageName;
		respWrap.strBaseUrl = responseNextPage.strBaseUrl;
		respWrap.srId = objRequest.Id;
		respWrap.flowId = responseNextPage.flowId;
		respWrap.pageId = responseNextPage.pageId;
		respWrap.isPublicSite = responseNextPage.isPublicSite;

		system.debug('@@@@@@@@2 respWrap.pageActionName ' + respWrap.pageActionName);
		return respWrap;
	}
	public class ButtonResponseWrapper {
		@AuraEnabled public String pageActionName { get; set; }
		@AuraEnabled public string communityName { get; set; }
		@AuraEnabled public String errorMessage { get; set; }
		@AuraEnabled public string CommunityPageName { get; set; }
		@AuraEnabled public string sitePageName { get; set; }
		@AuraEnabled public string strBaseUrl { get; set; }
		@AuraEnabled public string srId { get; set; }
		@AuraEnabled public string flowId { get; set; }
		@AuraEnabled public string pageId { get; set; }
		@AuraEnabled public boolean isPublicSite { get; set; }
		@AuraEnabled public boolean tester { get; set; }

	}
	public class SRWrapper {
		@AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }

		@AuraEnabled public String declarationIndividualText { get; set; }
		@AuraEnabled public String declarationCorporateText { get; set; }

		@AuraEnabled public String viewSRURL { get; set; }
		@AuraEnabled public Boolean isDraft { get; set; }


		public SRWrapper() {
		}
	}
    
}