/*
    Author      : Leeba
    Date        : 4-April-2020
    Description : Custom code to clone the Rejected Commercial Permission SR and create as Draft
    ---------------------------------------------------------------------------------------
*/
global without sharing class CC_CloneRejectedCommPerSR implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        if(stp!=null && stp.HexaBPM__SR__c!=null){
          try{
            system.debug('====CC_CloneRejectedSR=======');
              OB_CustomCodeHelper.SRDeepClone(stp.HexaBPM__SR__c,null);
          }catch(Exception e){
            strResult = e.getMessage()+'';
          }
        }
        return strResult;
    }
}