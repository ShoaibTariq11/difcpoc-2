global class UpdateLeadEmail implements database.Batchable<sobject>{
    
    
    
    global database.querylocator start(database.BatchableContext bc){
        
       
        String s = 'SELECT Id,Email,Phone FROM Lead';
        return database.getquerylocator(s);   
        
    }
    global void execute(database.BatchableContext bc,List<Lead> scope){
        List<Lead> lstLead = new List<Lead>();
        
        for(Lead eachLead:scope){
            eachLead.Email = 'test@DIFCtest.com';
            eachLead.Phone = '+971000000000';
            lstLead.add(eachLead);
        }
        
        if(lstLead.size() >0){
            UPDATE lstLead;
        }
        
    }
    global void finish(database.BatchableContext bc){
    
    }
}