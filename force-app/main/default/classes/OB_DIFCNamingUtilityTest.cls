@isTest
public class OB_DIFCNamingUtilityTest {
    
    public static testmethod void test1(){
        
        
         // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'In_Principle', 'Name_Check'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});

        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].legal_structures__c='Foundation';
        insertNewSRs[0].legal_structures__c='Foundation';
        insertNewSRs[1].Type_of_Entity__c='General Partnership (GP)';
        insertNewSRs[0].Type_of_Entity__c='General Partnership (GP)';
        insertNewSRs[1].HexaBPM__customer__c=insertNewAccounts[0].Id;
        insertNewSRs[0].HexaBPM__customer__c=insertNewAccounts[0].Id;

        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();

        insert insertNewSRs;
        
         test.startTest();
        
        string testString = OB_DIFCNamingUtility.manageDIFCLable(insertNewSRs[1] , 'shareholder');
        string testString2 = OB_DIFCNamingUtility.manageDIFCLable(insertNewSRs[0] , 'director');
        string testString3 = OB_DIFCNamingUtility.manageDIFCLable(insertNewSRs[0] , 'wrongInput');
        string testString4  = OB_DIFCNamingUtility.getActivityLable('Foundation');
        string testString5  = OB_DIFCNamingUtility.getActivityLable('wrongInput');
        insertNewSRs[1].Type_of_Entity__c='';
        update insertNewSRs[1];
        string testString6 = OB_DIFCNamingUtility.manageDIFCLable(insertNewSRs[1] , 'fake');
        
         test.stopTest();

        
    }

}