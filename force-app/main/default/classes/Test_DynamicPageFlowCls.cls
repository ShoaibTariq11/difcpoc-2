/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_DynamicPageFlowCls {

    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        
        Account objAcc = new Account();
        objAcc.Name = 'Test';
        insert objAcc;
        
        Currency__c cur = new Currency__c();
        cur.Name = 'USD';
        cur.Exchange_Rate__c = 0.27;
        cur.Active__c = true;
        insert cur;
        
        
        Page_Flow__c objPF = new Page_Flow__c();
        objPF.Name = 'Test Flow';
        objPF.Master_Object__c = 'Service_Request__c';
        objPF.Flow_Description__c = 'Test';
        objPF.Record_Type_API_Name__c = 'Application_of_Registration__c';
        insert objPF;
        
        Page__c objPg = new Page__c();
        objPg.Name = 'Page1';
        objPg.Page_Description__c = 'Page1';
        objPg.Is_Custom_Component__c = false;
        objPg.Page_Order__c = 1;
        objPg.What_Id__c = 'Service_Request__c';
        objPg.Render_By_Default__c = true;
        objPg.No_Quick_navigation__c = false;
        objPg.VF_Page_API_Name__c = 'Process_Flow';
        objPg.Page_Flow__c = objPF.Id;
        insert objPg;
        
        Page__c objPg2 = new Page__c();
        objPg2.Name = 'Page2';
        objPg2.Page_Description__c = 'Page2';
        objPg2.Is_Custom_Component__c = true;
        objPg2.Page_Order__c = 2;
        objPg2.What_Id__c = 'Service_Request__c';
        objPg2.Render_By_Default__c = true;
        objPg2.No_Quick_navigation__c = false;
        objPg2.VF_Page_API_Name__c = 'AmendmentDetails?Type=Authorized Representative';
        objPg2.Page_Flow__c = objPF.Id;
        insert objPg2;
        
        Section__c objSec = new Section__c();
        objSec.Page__c = objPg.Id;
        objSec.Name = 'Page1 Section1';
        objSec.Default_Rendering__c = true;
        objSec.Layout__c = '1';
        objSec.Section_Description__c = 'Test';
        objSec.Order__c = 1;
        objSec.Section_Title__c = 'Test Type';
        objSec.Section_Type__c = 'PageblockSection';
        insert objSec;
        
        Section__c objSec2 = new Section__c();
        objSec2.Page__c = objPg.Id;
        objSec2.Name = 'Page1 Section2';
        objSec2.Section_Description__c = 'Test';
        objSec2.Default_Rendering__c = true;
        objSec2.Layout__c = '1';
        objSec2.Order__c = 2;
        objSec2.Section_Title__c = 'Test Type';
        objSec2.Section_Type__c = 'CommandButtonSection';
        insert objSec2;
        
        Section__c objSec3 = new Section__c();
        objSec3.Page__c = objPg.Id;
        objSec3.Name = 'Page1 Section3';
        objSec3.Default_Rendering__c = false;
        objSec3.Order__c = 1;
        objSec3.layout__c = '1';
        objSec3.Section_Description__c = 'Test';
        objSec3.Section_Title__c = 'Test Type';
        objSec3.Section_Type__c = 'PageblockSection';
        insert objSec3;
        
        
        Section_Detail__c objSecDet = new Section_Detail__c();
        objSecDet.Section__c = objSec3.Id;
        objSecDet.Order__c = 1;
        objSecDet.Component_Type__c = 'Input Field';
        objSecDet.Object_Name__c = 'Service_Request__c';
        objSecDet.Field_API_Name__c = 'Customer__c';
        objSecDet.Render_By_Default__c = true;
        objSecDet.Component_Label__c = 'Client';
        objSecDet.Field_Description__c = 'Customer has to select';
        objSecDet.Mark_it_as_Required__c = true;
        insert objSecDet;
        
        Section_Detail__c objSecDetOut = new Section_Detail__c();
        objSecDetOut.Section__c = objSec3.Id;
        objSecDetOut.Order__c = 2;
        objSecDetOut.Render_By_Default__c = true;
        objSecDetOut.Component_Type__c = 'Output Field';
        objSecDetOut.Object_Name__c = 'Service_Request__c';
        objSecDetOut.Field_API_Name__c = 'Name';
        objSecDetOut.Component_Label__c = 'Client';
        objSecDetOut.Field_Description__c = 'Service Request No :';
        objSecDetOut.Mark_it_as_Required__c = true;
        insert objSecDetOut;
        
        Section_Detail__c objSecDetOuta = new Section_Detail__c();
        objSecDetOuta.Section__c = objSec3.Id;
        objSecDetOuta.Order__c = 3;
        objSecDetOuta.Render_By_Default__c = false;
        objSecDetOuta.Component_Type__c = 'Output Field';
        objSecDetOuta.Object_Name__c = 'Service_Request__c';
        objSecDetOuta.Field_API_Name__c = 'Name';
        objSecDetOuta.Component_Label__c = 'Client';
        objSecDetOuta.Field_Description__c = 'Service Request No :';
        objSecDetOuta.Mark_it_as_Required__c = true;
        insert objSecDetOuta;
        
        Page_Flow_Condition__c pfcSr = new Page_Flow_Condition__c();
        pfcSr.Section_Detail__c = objSecDetOuta.id;
        insert pfcSr;
        
        Page_Flow_Condition__c pfcSrS = new Page_Flow_Condition__c();
        pfcSrS.Section_Detail__c = objSecDetOuta.id;
        insert pfcSrS;
        
        
        
        
        Page_Navigation_Rule__c SecRenRule = new Page_Navigation_Rule__c();
        SecRenRule.Section__c = objSec3.Id;
        SecRenRule.Rule_Name__c = 'Test';
        SecRenRule.Rule_Text_Condition__c = 'Service_Request__c->Customer__c#!=#Null';
        SecRenRule.Rule_Condition__c = '1';
        insert SecRenRule;
                
        Apexpages.currentPage().getParameters().put('FlowId',objPF.Id);
        
        DynamicPageFlowCls obj = new DynamicPageFlowCls();
        obj.objSR.Customer__c = objAcc.Id;
        obj.objSR.Entity_Name__c = 'Test Entity Name';
        obj.prepare_Page_Description();
        obj.getClass();
        obj.getDyncPgMainPB();
        obj.RePrepareSection();
        obj.CommitRecord();
        obj.ValidateCompanyName(obj.objSR.Entity_Name__c,obj.objSR.Customer__c);
        obj.strNavigatePageId = objPg2.Id;
        obj.goTopage();
        obj.Cancel_Request();
    }
    static testMethod void myUnitTest2() {
        // TO DO: implement unit test
        
        Account objAcc = new Account();
        objAcc.Name = 'Test';
        insert objAcc;
        
        SR_Template__c objTempl = new SR_Template__c();
        objTempl.SR_RecordType_API_Name__c = 'Application_Of_Registration';
        objTempl.Active__c = true;
        objTempl.Is_through_Flow__c = true;
        insert objTempl;
        
        
        Service_Request__c SR = new Service_Request__c();
        SR.Customer__c = objAcc.Id;
        SR.Entity_Name__c = 'Test Entity Name2';
        SR.SR_Template__c = objTempl.Id;
        insert SR;
        
        Page_Flow__c objPF = new Page_Flow__c();
        objPF.Name = 'Test Flow';
        objPF.Master_Object__c = 'Service_Request__c';
        objPF.Flow_Description__c = 'Test';
        objPF.Record_Type_API_Name__c = 'Application_Of_Registration';
        insert objPF;
        
        Page__c objPg = new Page__c();
        objPg.Name = 'Page1';
        objPg.Page_Description__c = 'Page1';
        objPg.Is_Custom_Component__c = false;
        objPg.Page_Order__c = 1;
        objPg.What_Id__c = 'Service_Request__c';
        objPg.Render_By_Default__c = true;
        objPg.No_Quick_navigation__c = false;
        objPg.VF_Page_API_Name__c = 'Process_Flow';
        objPg.Page_Flow__c = objPF.Id;
        insert objPg;
        
        Page__c objPg2 = new Page__c();
        objPg2.Name = 'Page2';
        objPg2.Page_Description__c = 'Page2';
        objPg2.Is_Custom_Component__c = true;
        objPg2.Page_Order__c = 2;
        objPg2.What_Id__c = 'Service_Request__c';
        objPg2.Render_By_Default__c = true;
        objPg2.No_Quick_navigation__c = false;
        objPg2.VF_Page_API_Name__c = 'AmendmentDetails?Type=Authorized Representative';
        objPg2.Page_Flow__c = objPF.Id;
        insert objPg2;
        
        Section__c objSec = new Section__c();
        objSec.Page__c = objPg.Id;
        objSec.Name = 'Page1 Section1';
        objSec.Default_Rendering__c = true;
        objSec.Layout__c = '1';
        objSec.Section_Description__c = 'Test';
        objSec.Order__c = 1;
        objSec.Section_Title__c = 'Test Type';
        objSec.Section_Type__c = 'PageblockSection';
        insert objSec;
        
        Section__c ButtonSec = new Section__c();
        ButtonSec.Page__c = objPg.Id;
        ButtonSec.Name = 'Page1 Section2';
        ButtonSec.Section_Description__c = 'Test';
        ButtonSec.Default_Rendering__c = true;
        ButtonSec.Layout__c = '1';
        ButtonSec.Order__c = 2;
        ButtonSec.Section_Title__c = 'Test Type';
        ButtonSec.Section_Type__c = 'CommandButtonSection';
        insert ButtonSec;
        
        Section__c objSec3 = new Section__c();
        objSec3.Page__c = objPg.Id;
        objSec3.Name = 'Page1 Section3';
        objSec3.Default_Rendering__c = false;
        objSec3.Order__c = 1;
        objSec3.layout__c = '1';
        objSec3.Section_Description__c = 'Test';
        objSec3.Section_Title__c = 'Test Type';
        objSec3.Section_Type__c = 'PageblockSection';
        insert objSec3;
        
        
        Section_Detail__c objSecDet = new Section_Detail__c();
        objSecDet.Section__c = objSec3.Id;
        objSecDet.Order__c = 1;
        objSecDet.Component_Type__c = 'Input Field';
        objSecDet.Object_Name__c = 'Service_Request__c';
        objSecDet.Field_API_Name__c = 'Customer__c';
        objSecDet.Render_By_Default__c = true;
        objSecDet.Component_Label__c = 'Client';
        objSecDet.Field_Description__c = 'Customer has to select';
        objSecDet.Mark_it_as_Required__c = true;
        insert objSecDet;
        
        Section_Detail__c objSecDetOut = new Section_Detail__c();
        objSecDetOut.Section__c = objSec3.Id;
        objSecDetOut.Order__c = 2;
        objSecDetOut.Render_By_Default__c = true;
        objSecDetOut.Component_Type__c = 'Output Field';
        objSecDetOut.Object_Name__c = 'Service_Request__c';
        objSecDetOut.Field_API_Name__c = 'Name';
        objSecDetOut.Component_Label__c = 'Client';
        objSecDetOut.Field_Description__c = 'Service Request No :';
        objSecDetOut.Mark_it_as_Required__c = true;
        insert objSecDetOut;
        
        
        Section_Detail__c NextBtn = new Section_Detail__c();
        NextBtn.Section__c = ButtonSec.Id;
        NextBtn.Order__c = 2;
        NextBtn.Render_By_Default__c = true;
        NextBtn.Component_Type__c = 'Command Button';
        NextBtn.Navigation_Directions__c = 'Forward';
        NextBtn.Button_Location__c = 'Top';
        NextBtn.Button_Position__c = 'Left';
        NextBtn.Component_Label__c = 'Client';
        insert NextBtn;
        
        Section_Detail__c CancBtn = new Section_Detail__c();
        CancBtn.Section__c = ButtonSec.Id;
        CancBtn.Order__c = 2;
        CancBtn.Button_Position__c = 'Center';
        CancBtn.Render_By_Default__c = true;
        CancBtn.Component_Type__c = 'Command Button';
        CancBtn.Component_Label__c = 'Client';
        CancBtn.Commit_the_Record__c = true;
        insert CancBtn;
        
        Section_Detail__c SaveBtn = new Section_Detail__c();
        SaveBtn.Section__c = ButtonSec.Id;
        SaveBtn.Order__c = 3;
        SaveBtn.Button_Position__c = 'Center';
        SaveBtn.Navigation_Directions__c = 'Previous';
        SaveBtn.Render_By_Default__c = true;
        SaveBtn.Component_Type__c = 'Command Button';
        SaveBtn.Component_Label__c = 'Client';
        SaveBtn.Cancel_Request__c = true;
        insert SaveBtn;
        
        Section_Detail__c PrevBtn = new Section_Detail__c();
        PrevBtn.Section__c = ButtonSec.Id;
        PrevBtn.Order__c = 4;
        NextBtn.Button_Location__c = 'Bottom';
        NextBtn.Button_Position__c = 'Right';
        NextBtn.Navigation_Directions__c = 'Previous';
        PrevBtn.Render_By_Default__c = true;
        PrevBtn.Component_Type__c = 'Command Button';
        PrevBtn.Component_Label__c = 'Client';
        insert PrevBtn;
        
        Page_Flow_Action__c objNextAct = new Page_Flow_Action__c();
        objNextAct.Is_Custom_Component__c = true;
        objNextAct.Page__c = objPg2.Id;
        objNextAct.Section_Detail__c = NextBtn.Id;
        objNextAct.S_No__c = 1;
        objNextAct.Page_Flow__c = objPF.Id;
        insert objNextAct;
        
        Page_Navigation_Rule__c SecRenRule = new Page_Navigation_Rule__c();
        SecRenRule.Section__c = objSec3.Id;
        SecRenRule.Rule_Name__c = 'Test';
        SecRenRule.Rule_Text_Condition__c = 'Service_Request__c->Customer__c#!=#Null';
        SecRenRule.Rule_Condition__c = '1';
        insert SecRenRule;
                
        Apexpages.currentPage().getParameters().put('FlowId',objPF.Id);
        Apexpages.currentPage().getParameters().put('Id',SR.Id);
        
        DynamicPageFlowCls obj = new DynamicPageFlowCls();
        obj.objSR.Customer__c = objAcc.Id;
        obj.objSR.Entity_Name__c = 'Test Entity Name';
        obj.prepare_Page_Description();
        obj.getClass();
        obj.getDyncPgMainPB();
        obj.RePrepareSection();
        obj.CommitRecord();
        obj.ValidateCompanyName(obj.objSR.Entity_Name__c,obj.objSR.Customer__c);
        obj.strNavigatePageId = objPg2.Id;
        obj.stringActionId = NextBtn.Id;
        obj.DynamicButtonAction();
        
        //PreparePageBlockUtil.Cancel_SR();
    }
    
    static testmethod void myUnitTest3(){
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        objAccount.Block_Company_Services__c = true;
        insert objAccount;
        
       
        
        
        map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
        for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
            mapSRRecordTypes.put(objType.DeveloperName,objType);
        }
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = mapSRRecordTypes.get('Application_of_Registration').Id;
        objSR.Entity_Name__c = 'Test Entity Name';        
        objSR.Pre_Entity_Name_Arabic__c='abc';
        objSR.SR_Group__c = 'ROC';
        
        insert objSR;
        
        Page_Flow__c objPF = new Page_Flow__c();
        objPF.Name = 'Test Flow';
        objPF.Master_Object__c = 'Service_Request__c';
        objPF.Flow_Description__c = 'Test';
        objPF.Record_Type_API_Name__c = 'Application_Of_Registration';
        insert objPF;
        
        Page__c objPg = new Page__c();
        objPg.Name = 'Page1';
        objPg.Page_Description__c = 'Page1';
        objPg.Is_Custom_Component__c = false;
        objPg.Page_Order__c = 1;
        objPg.What_Id__c = 'Service_Request__c';
        objPg.Render_By_Default__c = true;
        objPg.No_Quick_navigation__c = false;
        objPg.VF_Page_API_Name__c = 'Process_Flow';
        objPg.Page_Flow__c = objPF.Id;
        insert objPg;
        
        Page__c objPg2 = new Page__c();
        objPg2.Name = 'Page2';
        objPg2.Page_Description__c = 'Page2';
        objPg2.Is_Custom_Component__c = true;
        objPg2.Page_Order__c = 2;
        objPg2.What_Id__c = 'Service_Request__c';
        objPg2.Render_By_Default__c = true;
        objPg2.No_Quick_navigation__c = false;
        objPg2.VF_Page_API_Name__c = 'AmendmentDetails?Type=Authorized Representative';
        objPg2.Page_Flow__c = objPF.Id;
        insert objPg2;
        
        Section__c objSec = new Section__c();
        objSec.Page__c = objPg.Id;
        objSec.Name = 'Page1 Section1';
        objSec.Default_Rendering__c = true;
        objSec.Layout__c = '1';
        objSec.Section_Description__c = 'Test';
        objSec.Order__c = 1;
        objSec.Section_Title__c = 'Test Type';
        objSec.Section_Type__c = 'PageblockSection';
        insert objSec;
        
        Section__c ButtonSec = new Section__c();
        ButtonSec.Page__c = objPg.Id;
        ButtonSec.Name = 'Page1 Section2';
        ButtonSec.Section_Description__c = 'Test';
        ButtonSec.Default_Rendering__c = true;
        ButtonSec.Layout__c = '1';
        ButtonSec.Order__c = 2;
        ButtonSec.Section_Title__c = 'Test Type';
        ButtonSec.Section_Type__c = 'CommandButtonSection';
        insert ButtonSec;
        
        Section__c objSec3 = new Section__c();
        objSec3.Page__c = objPg.Id;
        objSec3.Name = 'Page1 Section3';
        objSec3.Default_Rendering__c = false;
        objSec3.Order__c = 1;
        objSec3.layout__c = '1';
        objSec3.Section_Description__c = 'Test';
        objSec3.Section_Title__c = 'Test Type';
        objSec3.Section_Type__c = 'PageblockSection';
        insert objSec3;
        
        
        Section_Detail__c objSecDet = new Section_Detail__c();
        objSecDet.Section__c = objSec3.Id;
        objSecDet.Order__c = 1;
        objSecDet.Component_Type__c = 'Input Field';
        objSecDet.Object_Name__c = 'Service_Request__c';
        objSecDet.Field_API_Name__c = 'Customer__c';
        objSecDet.Render_By_Default__c = true;
        objSecDet.Component_Label__c = 'Client';
        objSecDet.Field_Description__c = 'Customer has to select';
        objSecDet.Mark_it_as_Required__c = true;
        insert objSecDet;
        
        Section_Detail__c objSecDetOut = new Section_Detail__c();
        objSecDetOut.Section__c = objSec3.Id;
        objSecDetOut.Order__c = 2;
        objSecDetOut.Render_By_Default__c = true;
        objSecDetOut.Component_Type__c = 'Output Field';
        objSecDetOut.Object_Name__c = 'Service_Request__c';
        objSecDetOut.Field_API_Name__c = 'Name';
        objSecDetOut.Component_Label__c = 'Client';
        objSecDetOut.Field_Description__c = 'Service Request No :';
        objSecDetOut.Mark_it_as_Required__c = true;
        insert objSecDetOut;
        
        
        Section_Detail__c NextBtn = new Section_Detail__c();
        NextBtn.Section__c = ButtonSec.Id;
        NextBtn.Order__c = 2;
        NextBtn.Render_By_Default__c = true;
        NextBtn.Component_Type__c = 'Command Button';
        NextBtn.Navigation_Directions__c = 'Forward';
        NextBtn.Button_Location__c = 'Top';
        NextBtn.Button_Position__c = 'Left';
        NextBtn.Component_Label__c = 'Client';
        insert NextBtn;
        
        Section_Detail__c CancBtn = new Section_Detail__c();
        CancBtn.Section__c = ButtonSec.Id;
        CancBtn.Order__c = 2;
        CancBtn.Button_Position__c = 'Center';
        CancBtn.Render_By_Default__c = true;
        CancBtn.Component_Type__c = 'Command Button';
        CancBtn.Component_Label__c = 'Client';
        CancBtn.Commit_the_Record__c = true;
        insert CancBtn;
        
        Section_Detail__c SaveBtn = new Section_Detail__c();
        SaveBtn.Section__c = ButtonSec.Id;
        SaveBtn.Order__c = 3;
        SaveBtn.Button_Position__c = 'Center';
        SaveBtn.Navigation_Directions__c = 'Previous';
        SaveBtn.Render_By_Default__c = true;
        SaveBtn.Component_Type__c = 'Command Button';
        SaveBtn.Component_Label__c = 'Client';
        SaveBtn.Cancel_Request__c = true;
        insert SaveBtn;
        
        Section_Detail__c PrevBtn = new Section_Detail__c();
        PrevBtn.Section__c = ButtonSec.Id;
        PrevBtn.Order__c = 4;
        NextBtn.Button_Location__c = 'Bottom';
        NextBtn.Button_Position__c = 'Right';
        NextBtn.Navigation_Directions__c = 'Previous';
        PrevBtn.Render_By_Default__c = true;
        PrevBtn.Component_Type__c = 'Command Button';
        PrevBtn.Component_Label__c = 'Client';
        insert PrevBtn;
        
        Page_Flow_Action__c objNextAct = new Page_Flow_Action__c();
        objNextAct.Is_Custom_Component__c = true;
        objNextAct.Page__c = objPg2.Id;
        objNextAct.Section_Detail__c = NextBtn.Id;
        objNextAct.S_No__c = 1;
        objNextAct.Page_Flow__c = objPF.Id;
        insert objNextAct;
        
        Page_Navigation_Rule__c SecRenRule = new Page_Navigation_Rule__c();
        SecRenRule.Section__c = objSec3.Id;
        SecRenRule.Rule_Name__c = 'Test';
        SecRenRule.Rule_Text_Condition__c = 'Service_Request__c->Customer__c#!=#Null';
        SecRenRule.Rule_Condition__c = '1';
        insert SecRenRule;
                
        Apexpages.currentPage().getParameters().put('FlowId',objPF.Id);
        Apexpages.currentPage().getParameters().put('Id',objSR.Id);
        
        DynamicPageFlowCls obj = new DynamicPageFlowCls();
        obj.objSR.Customer__c = objAccount.Id;
        obj.objSR.Entity_Name__c = 'Test Entity Name';
        
       
        obj.strNavigatePageId = objPg2.Id;
        obj.stringActionId = NextBtn.Id;
        obj.DynamicButtonAction();
        obj.goTopage();
        
         SR_Type_Conflicts__c st = new SR_Type_Conflicts__c();
        st.Name= 'Allotment / Change in Authorized';
        st.Record_type__c = 'Allotment of Shares Membership Interest';
        st.Conflicting_Record_Type__c = 'Change in Authorized Shares or Nominal Value';
        st.On_Create__c = true;
        insert st;
        
        
        Service_Request__c objSRN = new Service_Request__c();
        objSRN.Customer__c = objAccount.Id;
        objSRN.RecordTypeId = mapSRRecordTypes.get('Allotment_of_Shares_Membership_Interest').Id;
        objSRN.Entity_Name__c = 'Test Entity Name';        
        objSRN.Pre_Entity_Name_Arabic__c='abc';
        objSRN.SR_Group__c = 'ROC';
        
        insert objSRN;      
        
        obj.Check_conflicting_Open_Srs('Change_in_Authorized_Shares_or_Nominal_Value',objAccount.Id,null);
        
        SR_Status__c srs = new SR_Status__c();
        srs.Name ='Draft';
        srs.Code__c ='DRAFT';
        insert srs;
        
        
        DynamicPageFlowCls objx = new DynamicPageFlowCls();   
        Service_Request__c objSRNS = new Service_Request__c();
        objSRNS.Customer__c = objAccount.Id;
        objSRNS.RecordTypeId = mapSRRecordTypes.get('Application_of_Registration').Id;
        objSRNS.Entity_Name__c = 'Test Entity Name';        
        objSRNS.Pre_Entity_Name_Arabic__c='abc';
        objSRNS.SR_Group__c = 'ROC';
        objSRNS.Internal_SR_Status__c = srs.id;
        objSRNS.External_SR_Status__c = srs.id;  
        objx.objSR =    objSRNS;         
        objx.DynamicButtonAction();
        objx.goTopage();
        Apexpages.currentPage().getParameters().put('FlowId',objPF.Id);
        Apexpages.currentPage().getParameters().put('Id',objSR.Id); 
        objx.GotoMainScreen();      
    }
}