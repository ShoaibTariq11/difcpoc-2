/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_CC_RelationshipAmendmentsCls {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        string strRecType = '';
        for(RecordType rectyp:[select id from RecordType where Name='Notice of conversion of General or Limited Partner' and sObjectType='Service_Request__c']){
        	strRecType = rectyp.Id;
        }
        
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        insert objAcc;
        
        Relationship__c objRel = new Relationship__c();
        objRel.Subject_Account__c = objAcc.Id;
        objRel.Active__c = true;
        objRel.Relationship_Type__c = 'Has Limited Partner';
        insert objRel;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAcc.Id;
        objSR.Relationship__c = objRel.Id;
        if(strRecType!='')
        	objSR.RecordTypeId = strRecType;
        objSR.Type_of_Partner__c = 'Limited Partner';
        insert objSR;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        insert objStep;
        
        Step__c stp = [select id,SR__c,SR__r.Customer__c,SR__r.Type_of_Partner__c,SR__r.Relationship__c from Step__c where Id=:objStep.Id];
        CC_RelationshipAmendmentsCls.Convert_General_or_Limited_Partner(stp);
    }
    static testMethod void myUnitTest2() {
        string strRecType = '';
        for(RecordType rectyp:[select id from RecordType where Name='Notice of conversion of General or Limited Partner' and sObjectType='Service_Request__c']){
        	strRecType = rectyp.Id;
        }
        
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        insert objAcc;
        
        Relationship__c objRel = new Relationship__c();
        objRel.Subject_Account__c = objAcc.Id;
        objRel.Active__c = true;
        objRel.Relationship_Type__c = 'Has General Partner';
        insert objRel;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAcc.Id;
        objSR.Relationship__c = objRel.Id;
        if(strRecType!='')
        	objSR.RecordTypeId = strRecType;
        objSR.Type_of_Partner__c = 'General Partner';
        insert objSR;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        insert objStep;
        
        Step__c stp = [select id,SR__c,SR__r.Customer__c,SR__r.Type_of_Partner__c,SR__r.Relationship__c from Step__c where Id=:objStep.Id];
        CC_RelationshipAmendmentsCls.Convert_General_or_Limited_Partner(stp);
    }
}