/*****************************************************************************************************************************
    Author      :   Swati Sehrawat
    Date        :   21st July 2016
    Description :   This class is used as a controller for page GSComfortLetters
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date            Updated By    Description
    V1.0    25th Oct 2016   Swati         Change in arabic version of Umm Al Quwain
    V1.1    05-12-2016      Swati         as per ticket 851
    V1.2    18-01-2017      Swati         as per ticket 3843
    V1.3    09-Feb-2020     Arun          Tikect #7278  
    --------------------------------------------------------------------------------------------------------------------------             
*****************************************************************************************************************************/
public without sharing class cls_GSEmbassyLetter {
    //is looged in from portal or backend
    public boolean isPortal {get; set;}
    
    //is it new/Edit or detail page, used for rendering conditions
    public boolean isDetailPage {get; set;}
    
    //is it new page or edit page
    public boolean isNewPage {get; set;}
    
    //Main SR object used on page and in class
    public service_request__c objSR {get; set;}
    
    //Main SR template Object used on page to show description
    public SR_Template__c objSRTemplate {get; set;}
    
    public cls_SRGenericClassForMethods classObj;
    
    map<string, string> emirateArabicConversion;
    map<string, string> consulateArabicConversion;
    
    public List<String> countryList {get; set;}
    
    public string recordTypeName;
    
    public map<string,cls_SRWrapperClassForFields> objSRWrapList {get; set;}
    
    //Constructor defined
    public cls_GSEmbassyLetter (){
        
    }
    
    public cls_GSEmbassyLetter(Apexpages.Standardcontroller con){
        //get values from URL
        string recordType = Apexpages.currentPage().getParameters().get('RecordType');
        recordTypeName = Apexpages.currentPage().getParameters().get('RecordTypeName');
        string srID = Apexpages.currentPage().getParameters().get('id');
        string isDetail = Apexpages.currentPage().getParameters().get('isDetail');
        
        isPortal = false;
        isNewPage = true;
        objSR = new service_request__c();
        
        objSR.recordTypeId = recordType;
        classObj = new cls_SRGenericClassForMethods();
        
        if(string.isNotBlank(isDetail) && isDetail == 'true'){
            isDetailPage = true;    
        }
        else{
            isDetailPage = false;
        }
        
        if(string.isNotBlank(srID)){
            isNewPage = false;
            objSR = classObj.getServiceRequest(srID);   // get service request from the SR object
            if(objSR!=null){
                initializePopulatedValuesFromSR(objSR); // initialize values on page from SR
            }
        }
        
        if(Userinfo.getUserType() != 'Standard'){
            isPortal = true;
            user tempObj = classObj.getPortalDetails();
            objSR.Customer__c = tempObj.contact.AccountId;
            objSR.License_Number__c = tempObj.contact.Account.Registration_License_No__c;
            objSR.Establishment_Card_No__c = tempObj.contact.Account.Index_Card_Number__c;
            objSR.Send_SMS_To_Mobile__c = tempObj.phone;
            objSR.Email__c = tempObj.email;
            
        }
        
        if(isDetailPage == false){
            objSRTemplate = new SR_Template__c();
            objSRTemplate = classObj.getSRTemplate(recordTypeName); //get SR Template details to show on page
        }   
        
        //Map created to save Urdu format in fields used for SR documents
        emirateArabicConversion = new map<string, string>();
        emirateArabicConversion.put('Dubai','دبي');
        emirateArabicConversion.put('Abu Dhabi','أبوظبي');
        emirateArabicConversion.put('Ajman','عجمان');
        emirateArabicConversion.put('Al Fujairah','الفجيرة');
        emirateArabicConversion.put('Ras Al Khaimah','رأس الخيمة');
        emirateArabicConversion.put('Sharjah','الشارقة');
        emirateArabicConversion.put('Umm Al Quwain','أم القيوين'); //V1.0
        
        consulateArabicConversion = new map<string, string>();
        consulateArabicConversion.put('Consulate General Of','القنصل العام');
        consulateArabicConversion.put('Consulate Of','القنصل');
        consulateArabicConversion.put('Embassy Of','السفير');
    }

/*************************************************************Public Methods used on Page******************************************************************************************/    
    
    /************************************************************************************************************
    Method Name: saveRequest
    Functionality: save or update SR Record and redirect to detail page of SR
    **************************************************************************************************************/
    public pageReference saveRequest(){
        pageReference redirectPage = null;
        try{
            if(objSR!=null){
                
                string message = 'Success';
                
                message = checkValidationsBeforeSaving();
                if(message.contains('Success')){
                    redirectPage = populateAndSaveSR(); 
                }
                else{
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,message));
                }
            }
            if(redirectPage!=null){
                redirectPage.setRedirect(true); 
                return redirectPage;
            }
            else{
            }
        }catch(DmlException e) {
            Integer numErrors = e.getNumDml();
            for(Integer i=0;i<numErrors;i++) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getDmlMessage(i)));
            }
        } 
        catch(exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
        }
        return redirectPage;
    }
    
    /************************************************************************************************************
    Method Name: cancelRequest
    Functionality: cancel and redirect to home page or standard page depending on logged in user
    **************************************************************************************************************/
     public pageReference cancelRequest(){//V1.1
        pageReference objRef = null;
        try{
            if(IsDetailPage == true){ //Detail Page Button
                if(objSR.Submitted_Date__c != null && objSR.External_Status_Name__c!='Approved'){
                    string result = GsServiceCancellation.ProcessCancellation(objSR.id);    
                    if(result!='Cancellation submitted successfully'){
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,result));   
                    }
                    else{
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,result));
                    }
                    if(result=='Cancellation submitted successfully'){
                        objRef = new Pagereference('/'+objSR.Id);   
                    }
                }
                
                else{
                    delete objSR;
                    if(isPortal)
                        objRef = new Pagereference('/apex/home');
                    else
                        objRef = new Pagereference('/home/home.jsp');
                }
            }else if(IsDetailPage == false){ //Edit Page Button
                if(objSR.Id != null)
                    objRef = new Pagereference('/'+objSR.Id);
                else{
                    if(isPortal)
                        objRef = new Pagereference('/apex/home');
                    else
                        objRef = new Pagereference('/home/home.jsp');
                }
            }
            if(objRef != null)
                objRef.setRedirect(true);
            return objRef;
        }catch(Exception ex){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please contact support.'));
        }
        return objRef;
    }
    
    public pageReference editRequest(){
        pageReference redirectPage = null;
        try{
            if(objSR!=null){
                redirectPage = classObj.EditServiceRequest(objSR);
                
                if(redirectPage!=null){
                    redirectPage.setRedirect(true); 
                }
            }
        }catch(exception ex){
        }
        return redirectPage;
    }
    
    public pageReference submitRequest(){
        pageReference redirectPage = null;
        try{
            if(objSR!=null){
                if(string.isNotBlank(objSR.country__c)){
                    countryList = new List<String>();
                    countryList.addAll(objSR.country__c.split(';'));
                }
                //V1.2
                list<service_request__c> tempList = [Select (Select id From SR_Price_Items1__r), (Select id From Amendments__r) From Service_Request__c where id=:objSR.Id];
                if(tempList!=null && tempList.size()>0 && tempList[0].SR_Price_Items1__r!=null && tempList[0].SR_Price_Items1__r.size()>0 && 
                   tempList[0].Amendments__r!=null && tempList[0].Amendments__r.size()>0 && countryList.size() == tempList[0].Amendments__r.size() && 
                   countryList.size() == tempList[0].Amendments__r.size()){
                    redirectPage = classObj.SubmitServiceRequest(objSR);
                    if(redirectPage!=null){
                        redirectPage.setRedirect(true); 
                    }   
                }
                else{
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Please try saving SR again, some required information is missing'));
                }
                
            }
        }catch(exception ex){
        }
        return redirectPage;
    }
    
    
    public pageReference confirmUpgrade(){
        pageReference redirectPage = null;
        try{
            if(objSR!=null){
                string result = GsUpgradeDetails.UpgradeToVIP(objSR.id);
                if(result == 'Success'){
                    redirectPage = classObj.upgradeSR(objSR,recordTypeName);
                    if(redirectPage!=null){
                        redirectPage.setRedirect(true); 
                    }
                }
            }
        }catch(exception ex){
        }
        return redirectPage;
    }

/*************************************************************Private Methods used within class******************************************************************************************/  
    
    /************************************************************************************************************
    Method Name: initializePopulatedValuesFromSR
    Functionality: initialize dropdown values and checkboxes again on page from values that are saved in SR record
    **************************************************************************************************************/
    private void initializePopulatedValuesFromSR(Service_request__c objSR){
        if(objSR!=null){
            if(string.isNotBlank(objSR.country__c)){
                countryList = new List<String>();
                countryList.addAll(objSR.country__c.split(';'));    
                
                list<Amendment__c> tempListToShow = new list<Amendment__c>();
                tempListToShow = [select id,Letter_Format__c,Permanent_Native_Country__c,Letter_To_Consulate_Embassy__c,
                                           Purpose_of_Visit__c, Type_of_Visit_Visa__c, emirate__c 
                                    from Amendment__c where ServiceRequest__c=:objSR.id];
                if(tempListToShow!=null && tempListToShow.size()>0){
                    objSRWrapList = new map<string,cls_SRWrapperClassForFields>();
                    for(Amendment__c amendObj : tempListToShow){
                        cls_SRWrapperClassForFields objSRWrap = new cls_SRWrapperClassForFields(recordtypename);
                        objSRWrap.LetterFormat = amendObj.Letter_Format__c;
                        objSRWrap.LetterToConsulate = amendObj.Letter_To_Consulate_Embassy__c;
                        objSRWrap.PurposeofVisit = amendObj.Purpose_of_Visit__c;
                        objSRWrap.typeOfVisitVisa = amendObj.Type_of_Visit_Visa__c;
                        objSRWrap.EmirateEmbassy = amendObj.emirate__c;
                        objSRWrapList.put(amendObj.Permanent_Native_Country__c,objSRWrap);          
                    }
                }
                //V1.2
                if(objSRWrapList!=null && objSRWrapList.size()>0){
                    for(string keyCheck : countryList){
                        if(objSRWrapList.ContainsKey(keyCheck)==false){
                            objSRWrapList.put(keyCheck,new cls_SRWrapperClassForFields(recordtypename));
                        }
                    }   
                }
            }
        }   
    }
    
    private pageReference populateAndSaveSR(){
        pageReference redirectPage = null;
        integer counter = 0;
        if(objSR!=null){
            objSR.recordTypeId = classObj.getRecordTypeId('Consulate or Embassy Letter');   
                
        }
        redirectPage = classObj.upsertServiceRequest(objSR,recordTypeName);
        if(objSR.id!=null && (objSR.External_Status_Name__c == 'Draft' || string.isBlank(objSR.External_Status_Name__c))){
            list<Amendment__c> tempListToDeleteAmendment = new list<Amendment__c>();
            tempListToDeleteAmendment = [select id from Amendment__c where ServiceRequest__c=:objSR.id];
            if(tempListToDeleteAmendment!=null && tempListToDeleteAmendment.size()>0){
                delete tempListToDeleteAmendment;
            }
            
            list<SR_Price_Item__c> tempListToDeletePrice = new list<SR_Price_Item__c>();
            tempListToDeletePrice = [select id from SR_Price_Item__c where ServiceRequest__c=:objSR.id];
            if(tempListToDeletePrice!=null && tempListToDeletePrice.size()>0){
                delete tempListToDeletePrice;
            }
            
            list<Amendment__c> tempListToInsert = new list<Amendment__c>();
            list<SR_Price_Item__c> priceItemInsert = new list<SR_Price_Item__c>();
            
            if(countryList!=null && countryList.size()>0){
                Id recordTypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByName().get('Consulate or Embassy Letter').getRecordTypeId();
                
                Pricing_Line__c pricingLineExpress = new Pricing_Line__c();
                Pricing_Line__c pricingLineNormal = new Pricing_Line__c();
                list<Pricing_Line__c> tempList1 = [select id,Material_Code__c,product__c, (select Unit_Price__c,Unit_Price_in_USD__c from Dated_Pricing__r where Unit_Price__c != null AND ( Date_To__c = null OR Date_To__c >=: system.today() )) from Pricing_Line__c where Material_Code__c=:'GO-VIP112' or Material_Code__c=:'GO-00112'];
                if(tempList1!=null && !tempList1.isEmpty()){
                    for(Pricing_Line__c obj : tempList1){
                        if(obj.Material_Code__c == 'GO-VIP112'){
                            pricingLineExpress = obj;
                        }
                        else if(obj.Material_Code__c == 'GO-00112'){
                            pricingLineNormal = obj;
                        }
                    }
                }
                
                for(string temp : countryList){
                    //V1.2
                    if(objSRWrapList!=null && objSRWrapList.get(temp) != null && objSRWrapList.get(temp).EmirateEmbassy!=null && objSRWrapList.get(temp).LetterFormat!=null && 
                       objSRWrapList.get(temp).LetterToConsulate!=null && objSRWrapList.get(temp).PurposeofVisit!=null && objSRWrapList.get(temp).typeOfVisitVisa!=null){
                        
                        Amendment__c amendObj = new Amendment__c();
                        amendObj.Permanent_Native_Country__c = temp;    
                        amendObj.ServiceRequest__c = objSR.id;
                        amendObj.recordTypeId = recordTypeId;
                        amendObj.customer__c = objSR.customer__c;
                        amendObj.Contact__c = objSR.contact__c;
                        amendObj.Letter_Format__c = objSRWrapList.get(temp).LetterFormat;
                        amendObj.Letter_To_Consulate_Embassy__c = objSRWrapList.get(temp).LetterToConsulate;
                        amendObj.Purpose_of_Visit__c = objSRWrapList.get(temp).PurposeofVisit;
                        amendObj.Type_of_Visit_Visa__c = objSRWrapList.get(temp).typeOfVisitVisa;
                        amendObj.emirate__c = objSRWrapList.get(temp).EmirateEmbassy;
                        tempListToInsert.add(amendObj);
                        
                        SR_Price_Item__c tempObj = new SR_Price_Item__c();
                        tempObj.ServiceRequest__c = objSR.id;
                        tempObj.Status__c = 'Added';
                        if(objSR.Express_Service__c ==  true && pricingLineExpress!=null){
                            tempObj.Product__c = pricingLineExpress.product__c;
                            tempObj.Pricing_Line__c = pricingLineExpress.id;
                            if(pricingLineExpress.Dated_Pricing__r!=null && pricingLineExpress.Dated_Pricing__r.size()>0){
                                tempObj.Price__c = pricingLineExpress.Dated_Pricing__r[0].Unit_Price__c;
                                tempObj.Price_in_USD__c = pricingLineExpress.Dated_Pricing__r[0].Unit_Price_in_USD__c;
                            }
                        }
                        else if(objSR.Express_Service__c ==  false && pricingLineNormal!=null){
                            tempObj.Product__c = pricingLineNormal.product__c;
                            tempObj.Pricing_Line__c = pricingLineNormal.id;
                            if(pricingLineExpress.Dated_Pricing__r!=null && pricingLineNormal.Dated_Pricing__r.size()>0){
                                tempObj.Price__c = pricingLineNormal.Dated_Pricing__r[0].Unit_Price__c;
                                tempObj.Price_in_USD__c = pricingLineNormal.Dated_Pricing__r[0].Unit_Price_in_USD__c;
                            }
                        }
                        priceItemInsert.add(tempObj);
                    }
                }
                
                if(tempListToInsert!=null && tempListToInsert.size()>0){
                    insert tempListToInsert;
                }
                
                if(priceItemInsert!=null && priceItemInsert.size()>0){
                    insert priceItemInsert;
                }
            }
        }
        return redirectPage;
    }
    
    
    private string checkValidationsBeforeSaving(){
        string message = 'Success';
        if(string.isBlank(objSR.contact__c)){
            message = 'Applicant is required for the SR';   
            return message;
        }
        if(string.isBlank(objSR.country__c)){
            message = 'Country is required for the SR';     
            return message;
        }
        //V1.2
        if(objSRWrapList!=null && objSRWrapList.size()>0){
            for (string obj : objSRWrapList.keySet()){
                if(objSRWrapList.get(obj).EmirateEmbassy=='' || objSRWrapList.get(obj).LetterFormat=='' || objSRWrapList.get(obj).LetterToConsulate=='' ||
                   objSRWrapList.get(obj).PurposeofVisit=='' || objSRWrapList.get(obj).typeOfVisitVisa=='' || objSRWrapList.get(obj).EmirateEmbassy==null || 
                   objSRWrapList.get(obj).LetterFormat==null || objSRWrapList.get(obj).LetterToConsulate==null || objSRWrapList.get(obj).PurposeofVisit==null || 
                   objSRWrapList.get(obj).typeOfVisitVisa==null){
                   message = 'Required Fields are missing, Please provide all the details.';        
                }   
            }   
            return message;
        }
        return message;
    }
    
    //1.3 
     public List<SelectOption> getSponsorList()
     {
        
         List<SelectOption> options = new List<SelectOption>();
          options.add(new SelectOption('','--None--'));
       
        for(Relationship__c contSpn : [select id,Object_Contact__c,Relationship_Name__c from Relationship__c where Subject_Account__c=:objSR.Customer__c and Object_Contact__c!=null and  Relationship_Type__c='Has DIFC Sponsored Employee' and Active__c=true order by Relationship_Name__c])
        {
          
          
             options.add(new SelectOption(contSpn.Object_Contact__c,contSpn.Relationship_Name__c));

           
            
        }
        
         return options;

         
    }
    
    public void showCountryMethod(){
        countryList = new List<string>();
        if(objSR!=null && string.isNotBlank(objSR.country__c)){
            countryList.addAll(objSR.country__c.split(';'));    
            if(countryList.size()>5){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Only 5 Countries can be selected for SR'));    
            }   
            else{
                map<string,cls_SRWrapperClassForFields> tempMap = new map<string,cls_SRWrapperClassForFields>();
                if(isNewPage==true){
                    objSRWrapList = new map<string,cls_SRWrapperClassForFields>();  
                }
                for(string temp : countryList){
                    cls_SRWrapperClassForFields objSRWrap = new cls_SRWrapperClassForFields(recordtypename);
                    if(objSRWrapList!=null && objSRWrapList.get(temp)!=null){
                        objSRWrap = objSRWrapList.get(temp);
                    }
                    tempMap.put(temp,objSRWrap);        
                }
                objSRWrapList = new map<string,cls_SRWrapperClassForFields>();
                objSRWrapList.putAll(tempMap);
            }
        }
        else{
            objSRWrapList = new map<string,cls_SRWrapperClassForFields>();  
        }
    }
}