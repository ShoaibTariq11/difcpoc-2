/**
 *  Author   : Swati Sehrawat
 *  Company  : NSI JLT
 *  Date     : 26-May-2016  
 *  Test class for cls_AutomatedFineProcess
 * 
 */
 
@isTest
private class Test_automatedFineProcess {

    static testMethod void testMethod1() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        String objRectype;
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Issue_Fine') AND SobjectType='Service_Request__c']){
            objRectype = objRT.id;
        }
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = objRectype;
        objSR.Send_SMS_To_Mobile__c = '+971 9999999999';
        insert objSR;  
        
        ApexPages.StandardController controller = new ApexPages.StandardController(objSR);
        cls_automatedFineProcess objTemp = new cls_automatedFineProcess(controller);
        
        PageReference pageRef = Page.automatedFineProcess;
        Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getparameters().put('TypeOfSR', 'Objection');
        ApexPages.CurrentPage().getparameters().put('fineId', objSR.id);
        ApexPages.CurrentPage().getparameters().put('isObjection', 'true');
        cls_automatedFineProcess tempObj = new cls_automatedFineProcess();
        tempObj.initializeObjectionSR(string.valueOf(objSR.id),'Objection');
        tempObj.initializeObjectionSR(string.valueOf(objSR.id),'reduceFine');
    }
    
    static testMethod void testMethod2() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
    
        String objRectype;
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Issue_Fine') AND SobjectType='Service_Request__c']){
            objRectype = objRT.id;
        }
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = objRectype;
        objSR.Send_SMS_To_Mobile__c = '+971 9999999999';
        insert objSR;  
        
        compliance__c objComp = new compliance__c();
        objComp.Name = 'Annual Return';
        objComp.Name = 'Open';
        objComp.start_date__c = system.today();
        objComp.account__c = objAccount.id;
        objComp.status__c = 'Open';
        
        insert objComp;
        
        PageReference pageRef = Page.automatedFineProcess;
        Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getparameters().put('TypeOfSR', 'Payment');
        ApexPages.CurrentPage().getparameters().put('fineId', objSR.id);
        ApexPages.CurrentPage().getparameters().put('isObjection', 'false');
        cls_automatedFineProcess tempObj = new cls_automatedFineProcess();
        tempObj.initializeObjectionSR(string.valueOf(objSR.id),'Objection');
        tempObj.saveObjectionSR();
        tempObj.finePaymentSR();
        tempObj.updateExceptionDate();
        tempObj.getAllRelatedCompliances();
        tempObj.selectAll();
    }
    
    static testMethod void testMethod3() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
    
        String objRectype;
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Issue_Fine') AND SobjectType='Service_Request__c']){
            objRectype = objRT.id;
        }
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = objRectype;
         objSR.Send_SMS_To_Mobile__c = '+971 9999999999';
        insert objSR;  
        
        compliance__c objComp = new compliance__c();
        objComp.Name = 'Annual Return';
        objComp.Name = 'Defaulted';
        objComp.start_date__c = system.today();
        objComp.account__c = objAccount.id;
        objComp.Update_Email_Template_Body__c=true;
        insert objComp;
      //   cls_automatedFineProcess tempObj = new cls_automatedFineProcess();
         
       SR_PRICE_ITEM__C srPrice = new SR_PRICE_ITEM__C ();
       srPrice.Status__c  = 'Consumed';
       srPrice.ServiceRequest__c = objSR.ID;
       srPrice.Price__c= 500;
       insert srPrice;
        
       // tempObj.priceItemToDelete = srPrice.ID;
        
        PageReference pageRef = Page.automatedFineProcess;
        Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getparameters().put('TypeOfSR', 'reduceFine');
        ApexPages.CurrentPage().getparameters().put('fineId', objSR.id);
        ApexPages.CurrentPage().getparameters().put('isObjection', 'false');
        cls_automatedFineProcess tempObj = new cls_automatedFineProcess();
        tempObj.hasNext = false;
        tempObj.isAllowedToEdit();
        
        tempObj.getRelatedPriceItem();
        tempObj.reDirectToMainSR();
        tempObj.priceItemToDelete = srPrice.ID;
        tempObj.deletePriceItem();
        
    }
    static testmethod void test4(){
        test.startTest();
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
User portalAccountOwner1 = new User(
UserRoleId = portalRole.Id,
ProfileId = profile1.Id,
Username = '1h@difc.ae.uatfull' + System.now().millisecond() ,
Alias = 'sfdc',
Email='dotnetcodex@gmail.com',
EmailEncodingKey='UTF-8',
Firstname='Dhanik',
Lastname='Sahni',
LanguageLocaleKey='en_US',
LocaleSidKey='en_US',
TimeZoneSidKey='America/Chicago'
);
Database.insert(portalAccountOwner1);
        system.runas(portalAccountOwner1){
       
        Id conRecId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Profile p = [select Id,name from Profile where Name='DIFC Customer Community User Custom' limit 1];
        
        Account objAccount = new Account (Name = 'newAcc1',OwnerId = portalAccountOwner1.Id);  
        objAccount.ROC_Status__c  = 'Active';
        insert objAccount ;
        
        Contact con = new Contact (
        AccountId = objAccount.id,
        LastName = 'portalTestUser',
        RecordTypeID = conRecId 
        );
        insert con;
        
        User newUser = new User(
        profileId = p.id,
        username = 'newUser@yahoo.com',
        email = 'pb@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias='nuser',
        lastname='lastname',
        contactId = con.id
        );
        insert newUser;
        
        String objRectype;
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Issue_Fine') AND SobjectType='Service_Request__c']){
            objRectype = objRT.id;
        }
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = objRectype;
        
        insert objSR;  
        
        
        
            List<SR_Price_Item__c> srList = new List<SR_Price_Item__c>();
            SR_Price_Item__c objSRItem = new SR_Price_Item__c();
            objSRItem.ServiceRequest__c = objSR.Id;
            objSRItem.Status__c = 'Added';
            objSRItem.Price__c = 1000;
            insert objSRItem;
            srList.add(objSRItem);
            
            PageReference pgRef = Page.FineProcessReverseFine;
            Test.setCurrentPageReference(pgRef);
            ApexPages.CurrentPage().getparameters().put('TypeOfSR', 'Payment');
            ApexPages.CurrentPage().getparameters().put('fineId', objSR.id);
            ApexPages.CurrentPage().getparameters().put('isObjection', 'false');
             ApexPages.CurrentPage().getparameters().put('id', objSR.id);
            cls_automatedFineProcess fn = new cls_automatedFineProcess();
            //fn.OldlistOfFine = new List<SR_Price_Item__c>();
            //fn.listOfFine = new List<SR_Price_Item__c>();
            fn.objSR = objSR;
            fn.getRelatedPriceItem();
            PageReference benpg =  fn.savevalue();
            }
        test.stopTest();
    }
}