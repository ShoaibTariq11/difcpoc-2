/******************************************************************************************
 *  Author      : Shiva TathiReddy
 *  Company     : SAPCLE
 *  Date        : 26-Jan-2019
 *  Description : Security regulations to file for financing and further addendum/amendment of the properties
 *
*/
 
global without sharing class SecurityRegulations {
  global static String approveAndUpdateRequest(Step__c stp) {
      String response='';
      if(String.isNotBlank(stp.SR__c)) {
          String srId = stp.SR__c;
          try {
              Service_Request__c sr = [SELECT Id,Name,Customer__c,Internal_SR_Status__c,External_SR_Status__c,(SELECT Id,Amendment_Collateral_Change__c,Collateral_Value__c,Reason_for_Correction__c,Type_of_Record__c,Ammendment__c,Collateral_Description__c,Continuation__c,Description__c,Form_Type__c,Miscellaneous__c,Number_of_Pages__c,Optional_Filer_Reference_Data__c,Service_Request__c,Termination__c,Type_of_Amendment__c,Initial_Financial_Statement_No__c FROM Approved_Forms__r WHERE Service_Request__c  =: srId),(SELECT Id,Name,Approved_Form__c,City__c,Country__c,First_Name__c,Jurisdiction_of_Organisation__c,Last_Name__c,Mailing_Address__c,Middle_Name__c,Organisation_Name__c,Organisational_ID__c,Service_Request__c,Suffix__c,Type__c,Type_of_Organisation__c,Master_Id__c,Authorized__c FROM Debtors_SecuredParties__r WHERE Service_Request__c =: srId),(SELECT Id,Name,Organisation_Name__c,First_Name__c,Last_Name__c,Middle_Name__c,Record_Number__c,Search_Response__c,Date_Record_Filed__c,Additional_identifying_Information__c FROM Information_Requests__r WHERE Service_Request__c =: srId) FROM Service_Request__c WHERE Id =: srId];
              Approved_Form__c af = sr.Approved_Forms__r[0];
              Approved_Form_Master__c initialApprovedForm = new Approved_Form_Master__c();
              if(af.Form_Type__c == 'Financing Statement Addendum (Form 2)' || af.Form_Type__c == 'Financing Statement Additional Party (Form 3)' || af.Form_Type__c == 'Financing Statement Amendment (Form 4)' || af.Form_Type__c == 'Financing Statement Amendment Addendum (Form 5)' || af.Form_Type__c == 'Financing Statement Amendment Additional Party (Form 6)' || af.Form_Type__c == 'Correction Statement (Form 7)') {
                  initialApprovedForm = [SELECT Id,Form_Type__c,Date_Record_Filed__c,Date_Record_Closed__c,Termination__c,Amendment_Collateral_Change__c,Collateral_Value__c,Reason_for_Correction__c,Type_of_Record__c,(SELECT Id,Active__c,Approved_Form_Master__c,Collateral_Description__c FROM Security_Collaterals__r WHERE Approved_Form_Master__r.Name =: af.Initial_Financial_Statement_No__c),(SELECT Id,Name,Approved_Form_Master__c,City__c,Country__c,First_Name__c,Jurisdiction_of_Organisation__c,Last_Name__c,Mailing_Address__c,Middle_Name__c,Organisation_Name__c,Organisational_ID__c,Primary__c,Suffix__c,Type__c,Type_of_Organisation__c FROM Debtor_Secured_Party_Master__r WHERE Approved_Form_Master__r.Name =: af.Initial_Financial_Statement_No__c) FROM Approved_Form_Master__c WHERE Name =: af.Initial_Financial_Statement_No__c];
              }
              if(af.Form_Type__c == 'Financing Statement (Form 1)') {
                String approvedFORMDate = system.today().format();
                  Approved_Form_Master__c afm = new Approved_Form_Master__c(Name=approvedFORMDate,Form_Type__c=af.Form_Type__c,Date_Record_Filed__c=System.today(),Date_Record_Closed__c=System.today().addYears(5).addDays(-1),Customer__c=sr.Customer__c,Ammendment__c=af.Ammendment__c,Collateral_Description__c=af.Collateral_Description__c,Continuation__c=af.Continuation__c,Description__c=af.Description__c,Miscellaneous__c=af.Miscellaneous__c,Number_of_Pages__c=af.Number_of_Pages__c,Optional_Filer_Reference_Data__c=af.Optional_Filer_Reference_Data__c,Request_No__c=sr.Name,Termination__c=af.Termination__c,Type_of_Amendment__c=af.Type_of_Amendment__c);
                  insert afm;
                  Security_Collateral__c sc = new Security_Collateral__c(Active__c=true,Collateral_Value__c=af.Collateral_Value__c,Collateral_Date__c=System.today(),Approved_Form_Master__c=afm.Id,Collateral_Description__c=af.Collateral_Description__c);
                  insert sc;
                  List<DebtorSecured_Party_Master__c> dspms = new List<DebtorSecured_Party_Master__c>();
                  for(DebtorSecured_Party__c dsp : sr.Debtors_SecuredParties__r) {
                      DebtorSecured_Party_Master__c dspm = new DebtorSecured_Party_Master__c(Approved_Form_Master__c=afm.Id,Type__c=dsp.Type__c,Organisation_Name__c=dsp.Organisation_Name__c,First_Name__c=dsp.First_Name__c,Last_Name__c=dsp.Last_Name__c,Middle_Name__c=dsp.Middle_Name__c,Suffix__c=dsp.Suffix__c,Mailing_Address__c=dsp.Mailing_Address__c,City__c=dsp.City__c,Country__c=dsp.Country__c,Type_of_Organisation__c=dsp.Type_of_Organisation__c,Organisational_ID__c=dsp.Organisational_ID__c,Jurisdiction_of_Organisation__c=dsp.Jurisdiction_of_Organisation__c);
                      dspms.add(dspm);
                  }
                  if(!dspms.isEmpty()) {
                      insert dspms;
                  }
                  
              } else if(af.Form_Type__c == 'Financing Statement Additional Party (Form 3)' || af.Form_Type__c == 'Financing Statement Amendment Additional Party (Form 6)') {
                  List<DebtorSecured_Party_Master__c> dspms = new List<DebtorSecured_Party_Master__c>();
                  for(DebtorSecured_Party__c dsp : sr.Debtors_SecuredParties__r) {
                      DebtorSecured_Party_Master__c dspm = new DebtorSecured_Party_Master__c(Approved_Form_Master__c=initialApprovedForm.Id,Type__c=dsp.Type__c,Organisation_Name__c=dsp.Organisation_Name__c,First_Name__c=dsp.First_Name__c,Last_Name__c=dsp.Last_Name__c,Middle_Name__c=dsp.Middle_Name__c,Suffix__c=dsp.Suffix__c,Mailing_Address__c=dsp.Mailing_Address__c,City__c=dsp.City__c,Country__c=dsp.Country__c,Type_of_Organisation__c=dsp.Type_of_Organisation__c,Organisational_ID__c=dsp.Organisational_ID__c,Jurisdiction_of_Organisation__c=dsp.Jurisdiction_of_Organisation__c);
                      dspms.add(dspm);
                  }
                  if(!dspms.isEmpty()) {
                      insert dspms;
                  }
                  initialApprovedForm.Form_Type__c = af.Form_Type__c;
              } else if(af.Form_Type__c == 'Financing Statement Amendment (Form 4)') {
                  initialApprovedForm.Form_Type__c = af.Form_Type__c;
                  if(af.Termination__c) {
                    initialApprovedForm.Termination__c = true;
                      initialApprovedForm.Date_Record_Closed__c = System.today();
                  } else {
                      initialApprovedForm.Continuation__c = true;
                      Map<String,String> masterDebtPtyId = new Map<String,String>();
                      for(DebtorSecured_Party_Master__c dspmM : initialApprovedForm.Debtor_Secured_Party_Master__r) {
                          masterDebtPtyId.put(dspmM.Name,dspmM.Id);
                      }
                      List<DebtorSecured_Party_Master__c> insertDspms = new List<DebtorSecured_Party_Master__c>();
                      for(DebtorSecured_Party__c dsp : sr.Debtors_SecuredParties__r) {
                          if(af.Type_of_Amendment__c == 'Add') {
                              //DebtorSecured_Party_Master__c dspm = new DebtorSecured_Party_Master__c(Id=masterDebtPtyId.get(dsp.Master_Id__c),Approved_Form_Master__c=initialApprovedForm.Id,Type__c=dsp.Type__c,Organisation_Name__c=dsp.Organisation_Name__c,First_Name__c=dsp.First_Name__c,Last_Name__c=dsp.Last_Name__c,Middle_Name__c=dsp.Middle_Name__c,Suffix__c=dsp.Suffix__c,Mailing_Address__c=dsp.Mailing_Address__c,City__c=dsp.City__c,Country__c=dsp.Country__c,Type_of_Organisation__c=dsp.Type_of_Organisation__c,Organisational_ID__c=dsp.Organisational_ID__c,Jurisdiction_of_Organisation__c=dsp.Jurisdiction_of_Organisation__c);
                              DebtorSecured_Party_Master__c dspm = new DebtorSecured_Party_Master__c(Approved_Form_Master__c=initialApprovedForm.Id,Type__c=dsp.Type__c,Organisation_Name__c=dsp.Organisation_Name__c,First_Name__c=dsp.First_Name__c,Last_Name__c=dsp.Last_Name__c,Middle_Name__c=dsp.Middle_Name__c,Suffix__c=dsp.Suffix__c,Mailing_Address__c=dsp.Mailing_Address__c,City__c=dsp.City__c,Country__c=dsp.Country__c,Type_of_Organisation__c=dsp.Type_of_Organisation__c,Organisational_ID__c=dsp.Organisational_ID__c,Jurisdiction_of_Organisation__c=dsp.Jurisdiction_of_Organisation__c);
                              insertDspms.add(dspm);
                          }
                      }
                      if(!insertDspms.isEmpty()) {
                          insert insertDspms;
                      }
                      if(String.isNotBlank(af.Amendment_Collateral_Change__c)) {
                          for(Security_Collateral__c scr : initialApprovedForm.Security_Collaterals__r) {
                              scr.Active__c = false;
                          }
                          update initialApprovedForm.Security_Collaterals__r;
                          Security_Collateral__c sc = new Security_Collateral__c(Active__c=true,Collateral_Date__c=System.today(),Approved_Form_Master__c=initialApprovedForm.Id,Collateral_Description__c=af.Collateral_Description__c);
                          insert sc;
                      }
                  }
              } else if(af.Form_Type__c == 'Financing Statement Addendum (Form 2)') {
                  initialApprovedForm.Form_Type__c = af.Form_Type__c;
                  for(Security_Collateral__c scr : initialApprovedForm.Security_Collaterals__r) {
                      scr.Active__c = false;
                  }
                  update initialApprovedForm.Security_Collaterals__r;
                  Security_Collateral__c sc = new Security_Collateral__c(Active__c=true,Collateral_Value__c=af.Collateral_Value__c,Collateral_Date__c=System.today(),Approved_Form_Master__c=initialApprovedForm.Id,Collateral_Description__c=af.Collateral_Description__c);
                  insert sc;
              } else if(af.Form_Type__c == 'Financing Statement Amendment Addendum (Form 5)') {
                  initialApprovedForm.Form_Type__c = af.Form_Type__c;
                  for(Security_Collateral__c scr : initialApprovedForm.Security_Collaterals__r) {
                      scr.Active__c = false;
                  }
                  update initialApprovedForm.Security_Collaterals__r;
                  Security_Collateral__c sc = new Security_Collateral__c(Active__c=true,Collateral_Value__c=af.Collateral_Value__c,Collateral_Date__c=System.today(),Approved_Form_Master__c=initialApprovedForm.Id,Collateral_Description__c=af.Description__c);
                  insert sc;
              } else if(af.Form_Type__c == 'Correction Statement (Form 7)') {
                initialApprovedForm.Form_Type__c = af.Form_Type__c;
              } else if(af.Form_Type__c == 'Information Request (Form 8)') {
                  initialApprovedForm.Form_Type__c = af.Form_Type__c;
                  List<DebtorSecured_Party_Master__c> debtMasterLst = new List<DebtorSecured_Party_Master__c>();
                  if(af.Search_Response__c == 'All') {
                      debtMasterLst = [SELECT Id,Name,Approved_Form_Master__c,City__c,Country__c,First_Name__c,Jurisdiction_of_Organisation__c,Last_Name__c,Mailing_Address__c,Middle_Name__c,Organisation_Name__c,Organisational_ID__c,Primary__c,Suffix__c,Type__c,Type_of_Organisation__c FROM DebtorSecured_Party_Master__c WHERE Approved_Form_Master__r.Name =: af.Initial_Financial_Statement_No__c];
                  } else if(af.Search_Response__c == 'Unlapsed') {
                      debtMasterLst = [SELECT Id,Name,Approved_Form_Master__c,City__c,Country__c,First_Name__c,Jurisdiction_of_Organisation__c,Last_Name__c,Mailing_Address__c,Middle_Name__c,Organisation_Name__c,Organisational_ID__c,Primary__c,Suffix__c,Type__c,Type_of_Organisation__c FROM DebtorSecured_Party_Master__c WHERE Approved_Form_Master__r.Name =: af.Initial_Financial_Statement_No__c AND (Approved_Form_Master__r.Date_Record_Closed__c = null OR Approved_Form_Master__r.Date_Record_Closed__c > TODAY)];
                  }
                  DebtorSecured_Party_Master__c mdsp = new DebtorSecured_Party_Master__c();
                  for(DebtorSecured_Party_Master__c dspm : debtMasterLst) {
                      if(sr.Information_Requests__r[0].Organisation_Name__c == dspm.Organisation_Name__c || (sr.Information_Requests__r[0].First_Name__c == dspm.First_Name__c && sr.Information_Requests__r[0].Middle_Name__c == dspm.Middle_Name__c && sr.Information_Requests__r[0].Last_Name__c == dspm.Last_Name__c)) {
                          mdsp = dspm;
                      }
                  }
              }
              update initialApprovedForm;
              response = 'Success';
          } catch(Exception e) {
              response = 'failed';
          }
      }
      return response;
  }
}