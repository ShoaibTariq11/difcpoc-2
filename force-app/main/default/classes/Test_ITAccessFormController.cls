/*
    Created By  : Shabbir - DIFC on 11 Sep,2015
    Description : Test class for ITAccessFormController 
--------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By      Description
 ----------------------------------------------------------------------------------------              
 ----------------------------------------------------------------------------------------
*/
@isTest(seealldata=false)
private class Test_ITAccessFormController {

    public static Account objAccount;
    public static Service_Request__c objSR;
    public static Step__c objStep;
    public static Contact objContact;
    public static SR_Template__c objSRTemp;
    public static License__c objLicence;
    public static CountryCodes__c objCC; 
    public static Lookup__c objNationality;
    public static Attachment objPassportCopy;
    public static List<User> objUser;
    public static User portalAccountOwner1;
    public static Profile testProfile;
  
    static void init(){
       /* 
        
         */
        objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c ='Pakistan';
        objAccount.ROC_Status__c = 'Active';
        objAccount.BP_No__c='0000008978';
        //objAccount.OwnerId = portalAccountOwner1.Id;
        insert objAccount;      

        objSRTemp = new SR_Template__c(name = 'IT User Access Form',SR_RecordType_API_Name__c='IT_User_Access_Form');
        insert objSRTemp;
        
        objNationality = new Lookup__c(Type__c='Nationality',Name='USA');
        insert objNationality;
        
        objLicence = new License__c( Name = '0001', status__c='Active',Account__c=objAccount.id);
        insert objLicence;
        
        objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        

        string strRecType = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='IT_User_Access_Form' and sObjectType='Service_Request__c']){
            strRecType = rectyp.Id;
        }    
        
        Date subDate = Date.today();
        objSR = new Service_Request__c(); 
        objSR.Customer__c = objAccount.Id;
        objSR.SR_Template__c = objSRTemp.Id;
        objSR.Submitted_DateTime__c = DateTime.now();
        objSR.Submitted_Date__c = subDate;
        objSR.Title__c = 'Mrs.';
        objSR.First_Name__c = 'Finley';
        objSR.Send_SMS_To_Mobile__c = '+971562345234';
        objSR.Middle_Name__c = 'Ashton';
        objSR.License_Number__c = '0001';
        objSR.Last_Name__c = 'Kate';
        objSR.Position__c = 'Engineer';
        objSR.Place_of_Birth__c = 'USA';
        objSR.Nationality_list__c  = objNationality.Name;
        objSR.Date_of_Birth__c = Date.newinstance(1980,10,17);
        objSR.Email__c = 'test@test.com';
        objSR.Roles__c = 'IT Services';
        objSR.Nationality__c = objNationality.id;
        objSR.Passport_Number__c = 'ASD00023';
        objSR.Username__c='test@test.com';
        objSR.RecordTypeId = strRecType;
        insert objSR;
        
        objPassportCopy = new Attachment();    
        objPassportCopy.Name='PassportCopy.png';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        objPassportCopy.body=bodyBlob;
        objPassportCopy.parentId=objSR.Id;
        insert objPassportCopy;        
        
        string contRecordTypeId;
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='Portal_User' AND SobjectType='Contact' AND IsActive=true])
            contRecordTypeId = objRT.Id;  
                  
        objContact = new Contact(firstname='Timmy',lastname='John',Passport_No__c='ASD00023',accountId=objAccount.id,Email='test@test.com',RecordTypeId=contRecordTypeId);
        insert objContact;  

        Contact objContact2 = new Contact(firstname='Timmy',lastname='John',Passport_No__c='ASD00023',accountId=objAccount.id,Email='test@test.com',RecordTypeId=contRecordTypeId);
        insert objContact2;
        
        testProfile = [SELECT Id 
                               FROM profile
                               WHERE Name = 'DIFC Customer Community User Custom' 
                               LIMIT 1];
        user objUser1 = new User(LastName = 'test2', 
                                 Username = 'test@test.com', 
                                 Email = 'test_unique20169@test.com', 
                                 Alias = 'testu1', 
                                 TimeZoneSidKey = 'GMT', 
                                 LocaleSidKey = 'en_GB', 
                                 EmailEncodingKey = 'ISO-8859-1', 
                                 ProfileId = testProfile.Id, 
                                 ContactId = objContact.Id,
                                 Community_User_Role__c='Company Services'+';'+'IT Services',
                                 CompanyName = objAccount.Id,
                                 LanguageLocaleKey = 'en_US',IsActive=true); 
                                 
        user objUser2 = new User(LastName = 'test3', 
                                 Username = 'test3@test.com', 
                                 Email = 'test_unique2041693@test.com', 
                                 Alias = 'testu1', 
                                 TimeZoneSidKey = 'GMT', 
                                 LocaleSidKey = 'en_GB', 
                                 EmailEncodingKey = 'ISO-8859-1', 
                                 ProfileId = testProfile.Id, 
                                 ContactId = objContact2.Id,
                                 Community_User_Role__c='IT Services',
                                 CompanyName = objAccount.Id,
                                 LanguageLocaleKey = 'en_US',IsActive=true);
        objUser = new List<user>();
        objUser.add(objUser1);       
        objUser.add(objUser2);
        insert objUser;  
             
   

  }
    public static testmethod void testCreateUser(){
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
User portalAccountOwner1 = new User(
UserRoleId = portalRole.Id,
ProfileId = profile1.Id,
Username = 'dotnetcodex@gmail.com' + System.now().millisecond() ,
Alias = 'sfdc',
Email='dotnetcodex@gmail.com',
EmailEncodingKey='UTF-8',
Firstname='Dhanik',
Lastname='Sahni',
LanguageLocaleKey='en_US',
LocaleSidKey='en_US',
TimeZoneSidKey='America/Chicago',
IsActive = true
);
Database.insert(portalAccountOwner1);
        system.runas(portalAccountOwner1){
       
        init();
        
         system.Test.startTest();
        
        PageReference pageRef = Page.ITAccessForm;
        
        Test.setCurrentPageReference(pageRef);

        Attachment objPassportCopy1 = new Attachment();    
        objPassportCopy1.Name='PassportCopy.png';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        objPassportCopy1.body=bodyBlob;
             
        ITAccessFormController userformCls2 = new ITAccessFormController();
        ApexPages.StandardController sc = new ApexPages.standardController(objSR);
        ITAccessFormController userform = new ITAccessFormController(sc);
        
       
        userform.disableInput = false;
        userform.disableOptions = false;
        userform.RePrepare();
        userform.getFlag();
        userform.setFlag(true);
        userform.getdisplayAcc();
        userform.setdisplayAcc(true);
        userform.getaccountName();
        userform.setaccountName('test');
        userform.disablename1();
        userform.enablename1();
        userform.disableopt();
        userform.enableopt();
        userform.ComName = 'abc';
        userform.ChangeOption();
        userform.getPassportCopy();
        userformCls2.BPNo = Null ;
        userformCls2.isConsent =false;
        
        userformCls2.saveInfo();
        
        ITAccessFormController.revokeUser(userformCls2.sUser.Id);
        ITAccessFormController.removeUser(objUser[0].id,'Company Services',false);
        
        userform.propServices = false;
        userform.sUser = new Service_Request__c(Customer__c=objAccount.Id);
        
        SR_Status__c objStatus = new SR_Status__c();
        objStatus.Name = 'Submitted';
        objStatus.Code__c = 'SUBMITTED';
        insert objStatus;
        
        userformCls2.compServices=false;
        userformCls2.empServices=true;
        userformCls2.sUser.Customer__c = objAccount.Id;
        userformCls2.sUser.License_Number__c = '0001';
        userformCls2.sUser.Title__c = 'Mrs.';
        userformCls2.sUser.First_Name__c = 'Finley';
        userformCls2.sUser.Send_SMS_To_Mobile__c = '+971562345234';
        userformCls2.sUser.Middle_Name__c = 'Ashton';
        userformCls2.sUser.Last_Name__c = 'Kate';
        userformCls2.sUser.Position__c = 'Engineer';
        userformCls2.sUser.Place_of_Birth__c = 'USA';
        userformCls2.sUser.Nationality_list__c  = objNationality.Name;
        userformCls2.sUser.Date_of_Birth__c = Date.newinstance(1980,10,17);
        userformCls2.sUser.Email__c = 'test@test.com';
        userformCls2.sUser.Roles__c = 'IT Services';
        userformCls2.sUser.Nationality__c = objNationality.id;
        userformCls2.sUser.Passport_Number__c = 'ASD00023';
        userformCls2.sUser.Username__c = 'test@test.com';
        userformCls2.BPNo = null;
        system.debug('userformCls2.sUser.License_Number__c: ' + userformCls2.sUser.License_Number__c);
        userformCls2.isConsent =true;
        userformCls2.PassportCopy = objPassportCopy1;
        userformCls2.BPNo = Null ;
        
        userformCls2.saveInfo();
        
        
        userformCls2.sUser.License_Number__c ='10';
        userformCls2.sUser.Username__c = 'test@test.com';
        userformCls2.BPNo = Null ;
        userformCls2.saveInfo();
        userformCls2.saveRevokeSR();
        
        Step__c stp  = new Step__c();
        stp.SR__c = objSR.Id;
        insert stp;
        
        ITAccessFormController.createUser(stp);
        
        //update on contact passport
        objContact.Passport_No__c='ASD400023';
        update objContact;
        
        userformCls2.compServices=false;
        userformCls2.empServices=true;
        userformCls2.sUser.Customer__c = objAccount.Id;
        userformCls2.sUser.License_Number__c = '0001';
        userformCls2.sUser.Title__c = 'Mrs.';
        userformCls2.sUser.First_Name__c = 'Finley';
        userformCls2.sUser.Send_SMS_To_Mobile__c = '+971562345234';
        userformCls2.sUser.Middle_Name__c = 'Ashton';
        userformCls2.sUser.Last_Name__c = 'Kate';
        userformCls2.sUser.Position__c = 'Engineer';
        userformCls2.sUser.Place_of_Birth__c = 'USA';
        userformCls2.sUser.Nationality_list__c  = objNationality.Name;
        userformCls2.sUser.Date_of_Birth__c = Date.newinstance(1980,10,17);
        userformCls2.sUser.Email__c = 'test@test.com';
        userformCls2.sUser.Roles__c = 'IT Services';
        userformCls2.sUser.Nationality__c = objNationality.id;
        userformCls2.sUser.Passport_Number__c = 'ASD00023';
        userformCls2.sUser.Username__c = 'test@test.com';
        userformCls2.BPNo = null;
        system.debug('userformCls2.sUser.License_Number__c: ' + userformCls2.sUser.License_Number__c);
        userformCls2.isConsent =true;
        userformCls2.PassportCopy = objPassportCopy1;
        userformCls2.BPNo = Null ;
        
        userformCls2.saveInfo();
        userformCls2.saveRevokeSR();
        
        userformCls2.compServices=false;
        userformCls2.empServices=true;
        userformCls2.sUser.Customer__c = objAccount.Id;
        userformCls2.sUser.License_Number__c = '01';
        userformCls2.sUser.Title__c = 'Mrs.';
        userformCls2.sUser.First_Name__c = 'Finley';
        userformCls2.sUser.Send_SMS_To_Mobile__c = '+971562345234';
        userformCls2.sUser.Middle_Name__c = 'Ashton';
        userformCls2.sUser.Last_Name__c = 'Kate';
        userformCls2.sUser.Position__c = 'Engineer';
        userformCls2.sUser.Place_of_Birth__c = 'USA';
        userformCls2.sUser.Nationality_list__c  = objNationality.Name;
        userformCls2.sUser.Date_of_Birth__c = Date.newinstance(1980,10,17);
        userformCls2.sUser.Email__c = 'test@test.com';
        userformCls2.sUser.Roles__c = 'IT Services';
        userformCls2.sUser.Nationality__c = objNationality.id;
        userformCls2.sUser.Passport_Number__c = 'ASD0$0023';
        userformCls2.sUser.Username__c = 'test@test.com';
        userformCls2.BPNo = null;
        system.debug('userformCls2.sUser.License_Number__c: ' + userformCls2.sUser.License_Number__c);
        userformCls2.isConsent =true;
        userformCls2.PassportCopy = objPassportCopy1;
        userformCls2.BPNo = Null ;
        
        userformCls2.saveInfo();
        ITAccessFormController.createUser(stp);
        ITAccessFormController.usernameExists('test@test.com');
        ITAccessFormController.revokeUser(userformCls2.sUser.Id);
        
        system.Test.stopTest();
        }
    
    }
   public static testmethod void testCreateUser2(){
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = 'dotnetcodex@gmail.com' + System.now().millisecond() ,
        Alias = 'sfdc',
        Email='dotnetcodex@gmail.com',
        EmailEncodingKey='UTF-8',
        Firstname='Dhanik',
        Lastname='Sahni',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago',
        IsActive = true
        );
        Database.insert(portalAccountOwner1);
        system.runas(portalAccountOwner1){
        init();
        
            system.Test.startTest();
            
            PageReference pageRef = Page.ITAccessForm;
            
            Test.setCurrentPageReference(pageRef);

            Attachment objPassportCopy1 = new Attachment();    
            objPassportCopy1.Name='PassportCopy.png';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            objPassportCopy1.body=bodyBlob;
                 
            ITAccessFormController userformCls2 = new ITAccessFormController();
            ApexPages.StandardController sc = new ApexPages.standardController(objSR);
            ITAccessFormController userform = new ITAccessFormController(sc);
            
           
            userform.disableInput = false;
            userform.disableOptions = false;
            userform.RePrepare();
            userform.getFlag();
            userform.setFlag(true);
            userform.getdisplayAcc();
            userform.setdisplayAcc(true);
            userform.getaccountName();
            userform.setaccountName('test');
            userform.disablename1();
            userform.enablename1();
            userform.disableopt();
            userform.enableopt();
            userform.ComName = 'abc';
            userform.ChangeOption();
            userform.getPassportCopy();
            userformCls2.BPNo = Null ;
            userformCls2.isConsent =false;
            
            userformCls2.saveInfo();
            
            ITAccessFormController.revokeUser(userformCls2.sUser.Id);
            ITAccessFormController.removeUser(objUser[1].id,'IT Services',false);
            
            userform.propServices = false;
            userform.sUser = new Service_Request__c(Customer__c=objAccount.Id);
            
            SR_Status__c objStatus = new SR_Status__c();
            objStatus.Name = 'Active';
            objStatus.Code__c = 'SUBMITTED';
            insert objStatus;
            
            userformCls2.compServices=false;
            userformCls2.empServices=true;
            userformCls2.sUser.Customer__c = objAccount.Id;
            userformCls2.sUser.License_Number__c = '0001';
            userformCls2.sUser.Title__c = 'Mrs.';
            userformCls2.sUser.First_Name__c = 'Finley';
            userformCls2.sUser.Send_SMS_To_Mobile__c = '+971562345234';
            userformCls2.sUser.Middle_Name__c = 'Ashton';
            userformCls2.sUser.Last_Name__c = 'Kate';
            userformCls2.sUser.Position__c = 'Engineer';
            userformCls2.sUser.Place_of_Birth__c = 'USA';
            userformCls2.sUser.Nationality_list__c  = objNationality.Name;
            userformCls2.sUser.Date_of_Birth__c = Date.newinstance(1980,10,17);
            userformCls2.sUser.Email__c = 'test@test.com';
            userformCls2.sUser.Roles__c = 'IT Services';
            userformCls2.sUser.Nationality__c = objNationality.id;
            userformCls2.sUser.Passport_Number__c = 'A00023';
            userformCls2.sUser.Username__c = 'test3@test.com';
            userformCls2.BPNo = null;
            system.debug('userformCls2.sUser.License_Number__c: ' + userformCls2.sUser.License_Number__c);
            userformCls2.isConsent =true;
            userformCls2.PassportCopy = objPassportCopy1;
            userformCls2.BPNo = Null ;
            
            userformCls2.saveInfo();
            
            Step__c stp  = new Step__c();
            stp.SR__c = objSR.Id;
            insert stp;
            
            ITAccessFormController.createUser(stp);
            
            userformCls2.saveInfo();
            ITAccessFormController.usernameExists('test@test.com');
            ITAccessFormController.revokeUser(userformCls2.sUser.Id);
            
            List<user> uList = new List<user>();
            for(user itr:objUser){
                user u =new user(Id =itr.id,isActive=false,Community_User_Role__c=null);
                uList.add(u);
            }
            
            update uList;
            
            ITAccessFormController.createUser(stp);
            delete stp;
            delete objSR;
            userformCls2.saveInfo();
            ITAccessFormController.usernameExists('test@test.com');
            ITAccessFormController.revokeUser(userformCls2.sUser.Id);
            
            system.Test.stopTest();
        }
    
    }
}