@isTest
public class EmployeeComponentController_Test {
 public static testMethod void testgetEmployeeList() {
     
        Account objAccount 			= new Account();
        objAccount.Name 			= 'Test Custoer 1';
        objAccount.E_mail__c 		= 'test@test.com';
        objAccount.BP_No__c 		= '001234';
        objAccount.Company_Type__c  ='Financial - related';
        objAccount.Sector_Classification__c 	= 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c 		= 'LTD';
        objAccount.ROC_Status__c 				= 'Active';
        objAccount.Financial_Year_End__c 		= '31st December';
        insert objAccount;
        
        License__c objLic = new License__c();
        objLic.License_Issue_Date__c 	= system.today();
        objLic.License_Expiry_Date__c 	= system.today().addDays(365);
        objLic.Status__c 				= 'Active';
        objLic.Account__c 				= objAccount.id;
        insert objLic;
        
        objAccount.Active_License__c = objLic.id;
        update objAccount;
        Id portalUserId 				= Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact 				= new Contact();
        objContact.firstname 			= 'Test Contact';
        objContact.lastname 			= 'Test Contact1';
        objContact.accountId 			= objAccount.id;
        objContact.recordTypeId 		= portalUserId;
        objContact.Email 				= 'test@difcportal.com';
        objContact.BP_No__c             = 'Test111';
        objContact.Is_Absconder__c      = false;
        objContact.Passport_No__c       = '1234567';
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        
        User objUser 	= new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = objProfile.Id,
                           ContactId=objContact.Id,Community_User_Role__c='Company Services',
                           TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        Relationship__c thisRelatioShip = new Relationship__c();
        thisRelatioShip.Object_Contact__c = objContact.Id;
        thisRelatioShip.Subject_Account__c = objAccount.id;
        thisRelatioShip.Relationship_Type__c = 'Has DIFC Sponsored Employee';
        thisRelatioShip.End_Date__c = System.today().adddays(30);
        thisRelatioShip.Relationship_Group__c ='GS';
        thisRelatioShip.Active__c = true;
        insert thisRelatioShip; 
        
        SR_Status__c thisStatus = new SR_Status__c();
        thisStatus.Code__c = 'SUBMITTED';
        insert thisStatus;
            
        Test.startTest(); 
     
        EmployeeComponentController.thisWrapper('active','',1,'Object_Contact__r.FirstName',false);
        EmployeeComponentController.thisWrapper('cancelled','',1,'Object_Contact__r.FirstName',false);
        EmployeeComponentController.thisWrapper('','tes',1,'Object_Contact__r.FirstName',true);
        String testStr ='[{"FirstName":"Ahmed","LastName":"Al Janahi","mobile":"+971569803616","email":"test@difctest.com.invalid.invalid","desg":"MANAGER","exp":"yes","bpNumber":"2121117"}]';
        String newtestStr ='[{"parentId":"1234567","fileName":"Al Janahi","base64Data":"+971569803616","contentType":".png","experience":"3","linkedUrl":"2","bpNumber":"2121117"}]';
        EmployeeComponentController.saveSR(testStr,newtestStr);
        String newJson = '[{"selectedField": "" ,"operator": "LIKE","value": "","disabled": "false"}]';
        
        ViewCancelledEmployeeController.getWrapperList('Te', 1,  newJson);
     
        Test.stopTest();
     
    
     
 }
}