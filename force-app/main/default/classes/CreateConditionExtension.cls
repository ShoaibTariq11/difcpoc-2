/***********************************************************
*Apex Class: CreateConditionExtension
*Decsription: extend the CC_Createcondition class methods
*Created: 18-06-2019 Selva  #6752 Thomson reuters changes
---------------------------------------------------------------------------------------------------------------------------
Modification History
--------------------------------------------------------------------------------------------------------------------------
V.No    Date            Updated By    Description
--------------------------------------------------------------------------------------------------------------------------
 v1.1    02/Jan/2020    selva        Updated checkAMLNatioality #7477
 v1.2    12/May/2020    selva        Added to check economysubstance 'Review and Approve' create condition
***********************************************************/
Public class CreateConditionExtension{
    
    public static Boolean isReviewExe;
    
    
    Public static Boolean checkCreateMethod(string methodName,string srID,Boolean bResult){
        
        Boolean isvalid = bResult;
        if(methodName == 'checkAMLNatioality'){
            isvalid = checkAMLNatioality(srID);
        }
        else if(methodName == 'checkCreateBankdocs'){
            isvalid = checkCreateBankdocs(srID);
        }
        else if(methodName == 'checkCreateFinancialdocs'){
        system.debug('---checkCreateFinancialdocs--');
            isvalid = checkCreateFinancialdocs(srID);
        }
        else if(methodName == 'checkUBONationality'){
        system.debug('---checkUBONationality--');
            isvalid = checkUBONationality(srID);
        }
        else if(methodName == 'checkIsAMLorSecRejected'){
        system.debug('---checkIsAMLorSecRejected--');
           // isvalid = checkIsAMLorSecRejected(srID);
        }
        else if(methodName == 'isAMLCreated'){
        system.debug('---isAMLCreated--');
            isvalid = isAMLCreated(srID);
        }
        else if(methodName == 'checkReviewandApprove'){
        system.debug('---checkReviewandApprove--');
            isvalid = checkReviewandApprove(srID);
        }
        else if(methodName == 'checkAMLCreate'){
        system.debug('---checkAMLCreate--');
            isvalid = checkAMLCreate(srID);
        }
        else if(methodName == 'checkUBOAMLCreate'){
        system.debug('---checkUBOAMLCreate--');
            isvalid = checkUBOAMLCreate(srID);
        }
        else if(methodName == 'UBOReviewandApprove'){
        system.debug('---UBOReviewandApprove--');
            isvalid = UBOReviewandApprove(srID);
        }else if(methodName == 'isReviewandApproveCreated'){
        system.debug('---isReviewandApproveCreated--');
            isvalid = isReviewandApproveCreated(srID);
        }else if(methodName == 'checkEconomicSubstance'){
        system.debug('---checkEconomicSubstance--');
            isvalid = checkEconomicSubstance(srID);
        }
        else if(methodName == 'ReviewandApproveTwo'){
        system.debug('---ReviewandApproveTwo--');
            isvalid = ReviewandApproveTwo(srID); //#9733
        }
        return isvalid;
    }
    //method to check system running in future
    public static void SRAutoApproveNormal(string srID){
        SRAutoApprove(srID);
    }
    @future
    public static void SRAutoApproveFuture(string srID){
        SRAutoApprove(srID);
    }
    public static void SRAutoApproveTwoNormal(string srID){
        SRAutoApproveTwo(srID);
    }
    @future
    public static void SRAutoApproveTwoFuture(string srID){
        SRAutoApproveTwo(srID);
    }
    public static void UBOAutoApproveNormal(string srID){
        UBOAutoApprove(srID); 
    }
    @future
    public static void UBOAutoApproveFuture(string srID){
        UBOAutoApprove(srID); 
    }
    // Method to check Amedment nationality with High sanctioned country list
    public static boolean checkAMLNatioality(String SRID){ // v1.1 added
        Boolean isTRStatusOpen = false;
        try{
        if(!CC_CreateConditions.Check_IS_Removed_SR_Amendment(SRID)){
            List<Amendment__c> lstAmendment = [Select id,SR_Record_Type__c,Amendment_Type__c,Place_of_Registration__c,Nationality_list__c,Name,ServiceRequest__c,Given_Name__c, Gender__c, Date_of_Birth__c From Amendment__c Where ServiceRequest__c =: SRID and Status__c!='Removed'];
            Map<string,TR_Sanctioned_Nationality__c>  trCheckStatus = TR_Sanctioned_Nationality__c.getAll();   
            List<TR_Sanctioned_Nationality__c> TRList = new List<TR_Sanctioned_Nationality__c>();
            for(Amendment__c objAmnd: lstAmendment){
                TR_Sanctioned_Nationality__c TRNationality  = new TR_Sanctioned_Nationality__c();
                if(objAmnd.Amendment_Type__c=='Individual' && objAmnd.Nationality_list__c !=null){
                    TRNationality  = TR_Sanctioned_Nationality__c.getValues(objAmnd.Nationality_list__c);
                    system.debug('-TR 74--'+TRNationality );
                    if(TRNationality !=null){
                        TRList.add(TRNationality );
                    }
                }
                if(objAmnd.Amendment_Type__c=='Body Corporate' && objAmnd.Place_of_Registration__c !=null){
                    TRNationality  = TR_Sanctioned_Nationality__c.getValues(objAmnd.Place_of_Registration__c);
                    system.debug('-TR 79--'+TRNationality );
                    if(TRNationality !=null){
                        TRList.add(TRNationality );
                    }
                }
            }
            if(TRList.size()>0){
                isTRStatusOpen = true;
            }
            }
            system.debug('-checkAMLNatioality--'+isTRStatusOpen);
        }catch(Exception e){
             system.debug('---'+e);
        }
        return isTRStatusOpen;
    }
    // Method to check UBO nationality with High sanctioned country list
    public static boolean checkUBONationality(String SRID){ // v1.1 added
        Boolean isTRStatusOpen = false;
        try{
            List<UBO_Data__c> uboList = [Select id,RecordType.Name,Nationality__c,Nationality_New__c,Country__c From UBO_Data__c Where Service_Request__c =: SRID];
            Map<string,TR_Sanctioned_Nationality__c>  trCheckStatus = TR_Sanctioned_Nationality__c.getAll();   
            List<TR_Sanctioned_Nationality__c> TRList = new List<TR_Sanctioned_Nationality__c>();
            for(UBO_Data__c objUBO: uboList){
                TR_Sanctioned_Nationality__c TRNationality  = new TR_Sanctioned_Nationality__c();
                
                if(objUBO.RecordType.Name=='Individual' && objUBO.Nationality_New__c !=null){
                    TRNationality  = TR_Sanctioned_Nationality__c.getValues(objUBO.Nationality_New__c);
                    system.debug('-TR 105--'+TRNationality ); //# 9445
                    if(TRNationality !=null){
                        TRList.add(TRNationality );
                    }
                }
                if(objUBO.RecordType.Name=='Individual' && objUBO.Nationality__c!=null){
                    TRNationality  = TR_Sanctioned_Nationality__c.getValues(objUBO.Nationality__c);
                    system.debug('-TR 109--'+TRNationality );
                    if(TRNationality !=null){
                        TRList.add(TRNationality );
                    }
                }
                if(objUBO.RecordType.Name=='Individual' && objUBO.Country__c !=null){
                    TRNationality  = TR_Sanctioned_Nationality__c.getValues(objUBO.Country__c );
                    system.debug('-TR 115--'+TRNationality );
                    if(TRNationality !=null){
                        TRList.add(TRNationality );
                    }
                }
                if(objUBO.RecordType.Name=='Body Corporation UBO' && objUBO.Nationality__c!=null){
                    TRNationality  = TR_Sanctioned_Nationality__c.getValues(objUBO.Nationality__c);
                    system.debug('-TR 120--'+TRNationality );
                    if(TRNationality !=null){
                        TRList.add(TRNationality );
                    }
                }
                if(objUBO.RecordType.Name=='Body Corporation UBO' && objUBO.Country__c !=null){
                    TRNationality  = TR_Sanctioned_Nationality__c.getValues(objUBO.Country__c);
                    system.debug('-TR 125--'+TRNationality );
                    if(TRNationality !=null){
                        TRList.add(TRNationality );
                    }
                }
            }
            if(TRList.size()>0){
                isTRStatusOpen = true;
            }
            
            system.debug('-TR status --'+isTRStatusOpen);
        }catch(Exception e){
             system.debug('---'+e);
        }
        return isTRStatusOpen;
    }
    // Method to check if any of the 6 UBO fields updated //#9445
    public static boolean checkUBOFieldsUpdated(String SRID){ // v1.1 added
        Boolean isUBOFieldsUpdated = false;
        try{
            List<UBO_Data__c> uboList = [Select id,Name_Contact_Details__c,Update_Address_Details__c,Update_Passport_Details__c,RecordType.Name,Nationality__c,Nationality_New__c,Country__c From UBO_Data__c Where Service_Request__c =: SRID and Status__c!='Removed'];
           
            for(UBO_Data__c objUBO: uboList){
                if(objUBO.Name_Contact_Details__c==true || objUBO.Update_Address_Details__c ==true || objUBO.Update_Passport_Details__c==true){
                    isUBOFieldsUpdated = true;
                }
            }
        }catch(Exception e){
             system.debug('---'+e);
        }
        return isUBOFieldsUpdated;
    }
    public static boolean checkCreateBankdocs(string servId){
        Boolean isIndividualNation = false;
        List<Amendment__c> lstAmendment = [Select id,Family_Name__c,Given_Name__c,Date_of_Birth__c,Nationality_list__c,Name,ServiceRequest__c,ServiceRequest__r.Record_Type_Name__c, Gender__c,Registration_Date__c,Company_Name__c,Place_of_Registration__c,Shareholder_Company__c From Amendment__c Where Amendment_Type__c='Individual' and ServiceRequest__c =: servId]; 
        Map<string,TR_Sanctioned_Nationality__c>  CheckbankNation = TR_Sanctioned_Nationality__c.getAll();
        for(TR_Sanctioned_Nationality__c tcs:CheckbankNation.values()){
            for(Amendment__c itr: lstAmendment){
                if(tcs.Nationality__c== itr.Nationality_list__c){
                    isIndividualNation = true;
                    return isIndividualNation ;
                }
            }
        }
        return isIndividualNation;
    }
    public static boolean checkCreateFinancialdocs(string servId){
        Boolean isBCNation = false;
        List<Amendment__c> lstAmendment = [Select id,Family_Name__c,Given_Name__c,Date_of_Birth__c,Nationality_list__c,Name,ServiceRequest__c,ServiceRequest__r.Record_Type_Name__c, Gender__c,Registration_Date__c,Company_Name__c,Place_of_Registration__c,Shareholder_Company__c From Amendment__c Where Amendment_Type__c='Body Corporate' and ServiceRequest__c =: servId]; 
        Map<string,TR_Sanctioned_Nationality__c>  CheckbankNation = TR_Sanctioned_Nationality__c.getAll();
        for(TR_Sanctioned_Nationality__c tcs:CheckbankNation.values()){
            for(Amendment__c itr: lstAmendment){
                if(tcs.Nationality__c== itr.Place_of_Registration__c){
                    isBCNation = true;
                    return isBCNation;
                }
            }
        }
       system.debug('---isBCNation--'+isBCNation);
        return isBCNation;
    }
    // Auto Approve the SR if AML and Security step verified
    public static void SRAutoApprove(string srID){// v1.1 added
    system.debug('--SRAutoApprove--'+srID);
         Boolean isBCNation = false;
        //step__c stp = [select id,SR__c,status__c,ownerID from step__c where SR__c=:srID and Step_Name__c='Review and Approve'];
        List<Step__c> stepList = [select id,Step_Name__c,SR__c,Step_Code__c,Status_Code__c from Step__c where SR__c=:srID];
        
        //List<Status__c> statusList = new List<Status__c>([select id,name from Status__c where name ='Verified']);
        //List<SR_status__c> srStatusList = new list<SR_status__c>([select id,name from SR_status__c where name ='Approved']);
        List<service_request__c> serviceReqDetails = new List<service_request__c>([select id,Customer__r.ROC_Status__c from Service_Request__c where id=:stepList[0].SR__c]);
        
        Boolean isVAVerified = false;
        Boolean isQRVerified = false;
        Boolean isSRAddReq = CC_CreateConditions.Check_IS_Add_in_SR_Amendment(stepList[0].SR__c);
        Boolean isAddRequestverified = false;
        Boolean isUpdateRequestVerified = false;
        
       step__c stpupdate;
        
        if(stepList.size()>0){
            for(Step__c itr: stepList){  
                system.debug('--step name --'+itr.Step_Code__c+'---'+itr.Status_Code__c);
                if(itr.Step_Code__c=='AML_APPROVAL' && (itr.Status_Code__c=='VERIFIED' || itr.Status_Code__c=='APPROVED' )){ //update and add request
                    isVAVerified = true;
                }
                if(isSRAddReq){ // Add request
                    if(itr.Step_Code__c=='SECURITY_EMAIL_APPROVAL' && itr.Status_Code__c=='VERIFIED'){
                        isQRVerified = true;
                    }
                }
                if(itr.Step_Name__c=='Review and Approve'){
                    stpupdate = new step__c(id=itr.id,status__c = id.valueof(Label.StepStatusVer),ownerID = userinfo.getUserID());
                }
            }
        }
        system.debug('--isSRAddReq--'+isSRAddReq+'--'+isVAVerified+'---'+isQRVerified);
        //add request
        if(isSRAddReq && isVAVerified && isQRVerified){
            isAddRequestverified = true;
        }else if(!isSRAddReq && isVAVerified){
            isUpdateRequestVerified = true;
        }
        
        if(isAddRequestverified || isUpdateRequestVerified){
            //stp.status__c = statusList[0].id;
            
            //stp.status__c = id.valueof('a1M20000001iL9SEAU');
            serviceReqDetails[0].External_SR_Status__c = id.valueof(Label.stepstatusApp) ; //srStatusList[0].id;
            serviceReqDetails[0].Internal_SR_Status__c = id.valueof(Label.stepstatusApp) ; //srStatusList[0].id;
            try{
                //stp.ownerID = userinfo.getUserID();
                if(stpupdate.Id!=null){
                    Update stpupdate;
                }
                isBCNation = true;
                Update serviceReqDetails;
                
            }
            catch(Exception e){
                
               Log__c objLog = new Log__c();
               objLog.Type__c = 'SR Auto Approval method';
               objLog.Description__c = 'Auto Approval failed'+e.getMessage()+' Line # '+e.getLineNumber();              
               insert objLog;

            }
        }
        //return isBCNation;
    }
    // Auto Approve the SR if UBO and Security step verified
    public static void UBOAutoApprove(string srID){// v1.1 added
    system.debug('--UBOAutoApprove--'+srID);
         Boolean isBCNation = false;
        //step__c stp = [select id,SR__c,status__c,ownerID from step__c where SR__c=:srID and Step_Name__c='Review and Approve'];
        List<Step__c> stepList = [select id,Step_Name__c,SR__c,Step_Code__c,Status_Code__c from Step__c where SR__c=:srID];
       
        List<service_request__c> serviceReqDetails = new List<service_request__c>([select id,Customer__r.ROC_Status__c from Service_Request__c where id=:stepList[0].SR__c]);
        
        Boolean isVAVerified = false;
        Boolean isQRVerified = false;
        Boolean isSRAddReq = CC_CreateConditions.CheckUBODataAdded(stepList[0].SR__c);
        Boolean isAddRequestverified = false;
        Boolean isUpdateRequestVerified = false;
        
       step__c stpupdate;
        
        if(stepList.size()>0){
            for(Step__c itr: stepList){  
                system.debug('--step name --'+itr.Step_Code__c+'---'+itr.Status_Code__c);
                if(isSRAddReq){ // Add request
                   if(checkUBONationality(stepList[0].SR__c)){
                        if(itr.Step_Code__c=='AML_APPROVAL' && (itr.Status_Code__c=='VERIFIED' || itr.Status_Code__c=='APPROVED' )){ //update and add request
                            isVAVerified = true;
                        }
                    }else{
                        isVAVerified = true;
                    }
                    if(itr.Step_Code__c=='SECURITY_EMAIL_APPROVAL' && itr.Status_Code__c=='VERIFIED'){
                        isQRVerified = true;
                    }
                }
                if(!isSRAddReq){ // Add request
                    if(checkUBONationality(stepList[0].SR__c)){
                        if(itr.Step_Code__c=='AML_APPROVAL' && (itr.Status_Code__c=='VERIFIED' || itr.Status_Code__c=='APPROVED' )){ //update and add request
                            isVAVerified = true;
                        }
                    }else{
                        isVAVerified = true;
                    }
                }
                if(itr.Step_Name__c=='Review and Approve'){ //Label.ROC_Officer_Queue
                    stpupdate = new step__c(id=itr.id,status__c = id.valueof(Label.StepStatusVer),ownerID = userinfo.getuserid());
                }
            }
        }
        //add request
        if(isSRAddReq && isVAVerified && isQRVerified){
            isAddRequestverified = true;
        }else if(!isSRAddReq && isVAVerified){
            isUpdateRequestVerified = true;
        }
        
        if(isAddRequestverified || isUpdateRequestVerified){
            
            serviceReqDetails[0].External_SR_Status__c = id.valueof(Label.stepstatusApp) ;
            serviceReqDetails[0].Internal_SR_Status__c = id.valueof(Label.stepstatusApp) ;
            try{
                //stp.ownerID = userinfo.getUserID();
                if(stpupdate.Id!=null){
                    Update stpupdate;
                }
                isBCNation = true;
                Update serviceReqDetails;
                
            }
            catch(Exception e){
                
               Log__c objLog = new Log__c();
               objLog.Type__c = 'SR Auto Approval method';
               objLog.Description__c = 'Auto Approval failed'+e.getMessage()+' Line # '+e.getLineNumber();              
               insert objLog;

            }
        }
        //return isBCNation;
    }
    public static boolean isAMLCreated(string strSRID){ // v1.1 added
        boolean bResult = false;
        List<Step__c> stepList = [select id,Step_Name__c,SR__c,Step_Code__c,Status_Code__c from Step__c where SR__c=:strSRID and Step_Code__c='AML_APPROVAL'];
        if(stepList.size()<=2){
            for(Step__c stp :stepList){
                bResult = true;
            }
        }
        return bResult;
    }
    public static boolean isReviewandApproveCreated(string strSRID){// v1.1 added
        boolean bResult = false;
        List<Step__c> stepList = [select id,Step_Name__c,SR__c,Step_Code__c,Status_Code__c from Step__c where SR__c=:strSRID and Step_Code__c='REVIEW_AND_APPROVE'];
        if(stepList.size()<=1){
            for(Step__c stp :stepList){
                bResult = true;
            }
        }
         system.debug('--isReviewandApproveCreated--'+bResult);
        return bResult;
       
    }
    // Check Review and Approve create condition
    public static boolean checkReviewandApprove(String SRID){// v1.1 added
        Boolean isstepRACreate = false;
        List<Step__c> stepList = [select id,SR__r.Name,Step_Name__c,SR_Record_Type_Text__c,SR__c,Step_Code__c,Status_Code__c from Step__c where SR__c=:SRID];
        Integer rvstep = 0;
        string srRecType = stepList[0].SR_Record_Type_Text__c;
        for(Step__c itr:stepList){
            if(itr.Step_Code__c=='REVIEW_AND_APPROVE'){
                rvstep++;
            }
        }
        Boolean isAddRequest = CC_CreateConditions.Check_IS_Add_in_SR_Amendment(SRID);
        if(rvstep<=1){
            String Quick_Rev = null;
            String Security_Email = null;
            String AML_step = null;
            String VER_OF_APP = null;
            
            for(Step__c itr:stepList){
                if(itr.Step_Code__c=='QUICK_REVIEW_BY_EMPLOYEE'){
                    Quick_Rev = itr.Status_Code__c;
                }
                if(itr.Step_Code__c=='SECURITY_EMAIL_APPROVAL'){
                    Security_Email = itr.Status_Code__c;
                }
                if(itr.Step_Code__c=='AML_APPROVAL'){
                    AML_step = itr.Status_Code__c;
                }
                if(itr.Step_Code__c=='VERIFICATION_OF_APPLICATION'){
                    VER_OF_APP = itr.Status_Code__c;
                }
                
            }
            //Add request
            if(isAddRequest){
                if(Quick_Rev =='VERIFIED' && Security_Email =='VERIFIED' && AML_step=='APPROVED' && VER_OF_APP=='VERIFIED'){
                    isstepRACreate  = true;
                }
                if(Quick_Rev =='VERIFIED' && Security_Email =='REJECTED' && AML_step=='CLOSED' && VER_OF_APP=='VERIFIED'){
                    isstepRACreate  = true;
                }
                if(Quick_Rev =='VERIFIED' && Security_Email =='CLOSED' && AML_step=='REJECTED' && VER_OF_APP=='VERIFIED'){
                    isstepRACreate  = true;
                }
                if(Quick_Rev=='VERIFIED' && Security_Email == 'CLOSED' && AML_step=='REJECTED' && VER_OF_APP=='CLOSED'){
                    isstepRACreate  = true;
                }
                if(Quick_Rev =='VERIFIED' && Security_Email =='REJECTED' && AML_step=='CLOSED' && VER_OF_APP=='CLOSED'){
                    isstepRACreate  = true;
                }
            }
            //Update request
            if(!isAddRequest){
                if((VER_OF_APP=='VERIFIED' && AML_step=='APPROVED') && checkAMLNatioality(SRID)){
                    isstepRACreate  = true;
                }
                if((VER_OF_APP=='VERIFIED' && AML_step=='REJECTED') && checkAMLNatioality(SRID)){
                    isstepRACreate  = true;
                }
                
                if(srRecType =='Sale_or_Transfer_of_Shares_or_Membership_Interest'){
                    if(VER_OF_APP=='VERIFIED'){
                        isstepRACreate  = true;
                    }
                }else if(srRecType =='Shareholder_holding_shares_on_trust'){
                    if(VER_OF_APP=='VERIFIED'){
                        isstepRACreate  = true;
                    }
                }else{
                    if(VER_OF_APP=='VERIFIED' && !checkAMLNatioality(SRID)){
                        isstepRACreate  = true;
                    }
                }
            }
            /**
            Log__c objLog = new Log__c();
            objLog.Type__c = 'checkReviewandApprove';
            objLog.Description__c = Quick_Rev+'-'+Security_Email+'-'+AML_step+'-'+VER_OF_APP+'--size-'+stepList.size()+'--'+checkAMLNatioality(SRID);
            insert objLog;
            **/
        }
        system.debug('--isstepRACreate--'+isstepRACreate);
        return isstepRACreate;
    }
    // Check Review and Approve create condition for Phase 2 SR types
    public static boolean ReviewandApproveTwo(String SRID){//#9733
        Boolean isstepRACreate = false;
        List<Step__c> stepList = [select id,SR__r.Name,Step_Name__c,SR_Record_Type_Text__c,SR__c,Step_Code__c,Status_Code__c from Step__c where SR__c=:SRID];
        Integer rvstep = 0;
        string srRecType = stepList[0].SR_Record_Type_Text__c;
        for(Step__c itr:stepList){
            if(itr.Step_Code__c=='REVIEW_AND_APPROVE'){
                rvstep++;
            }
        }
        Boolean isAddRequest = CC_CreateConditions.Check_IS_Add_in_SR_Amendment(SRID);
        if(rvstep<=1){
            String AML_step = null;
            String VER_OF_APP = null;
            
            for(Step__c itr:stepList){
                if(itr.Step_Code__c=='VERIFICATION_OF_APPLICATION'){
                    VER_OF_APP = itr.Status_Code__c;
                }
                if(itr.Step_Code__c=='AML_APPROVAL'){
                    AML_step = itr.Status_Code__c;
                }
            }
            //Add request
            if(isAddRequest){
                if(AML_step=='APPROVED' && VER_OF_APP=='VERIFIED'){
                    isstepRACreate  = true;
                }
                if(AML_step=='CLOSED' && VER_OF_APP=='VERIFIED'){
                    isstepRACreate  = true;
                }
                if(AML_step=='REJECTED' && VER_OF_APP=='VERIFIED'){
                    isstepRACreate  = true;
                }
            }
            //Update request
            if(!isAddRequest){
                if((VER_OF_APP=='VERIFIED' && AML_step=='APPROVED') && checkAMLNatioality(SRID)){
                    isstepRACreate  = true;
                }
                if((VER_OF_APP=='VERIFIED' && AML_step=='REJECTED') && checkAMLNatioality(SRID)){
                    isstepRACreate  = true;
                }
                if(VER_OF_APP=='VERIFIED' && !checkAMLNatioality(SRID)){
                    isstepRACreate  = true;
                }
            }
            /**
            Log__c objLog = new Log__c();
            objLog.Type__c = 'checkReviewandApprove';
            objLog.Description__c = Quick_Rev+'-'+Security_Email+'-'+AML_step+'-'+VER_OF_APP+'--size-'+stepList.size()+'--'+checkAMLNatioality(SRID);
            insert objLog;
            **/
        }
        system.debug('--isstepRACreate--'+isstepRACreate);
        return isstepRACreate;
    }
    //Check AML create conditions // v1.1 added
    public static boolean checkAMLCreate(String SRID){
        Boolean isstepRACreate = false;
        List<Step__c> stepList = [select id,Step_Name__c,SR__c,Step_Code__c,Status_Code__c from Step__c where SR__c=:srID];
        String Quick_Rev = null;
        String VER_OF_APP = null;
        
        for(Step__c itr:stepList){
            if(itr.Step_Code__c=='QUICK_REVIEW_BY_EMPLOYEE'){
                Quick_Rev = itr.Status_Code__c;
            }
            if(itr.Step_Code__c=='VERIFICATION_OF_APPLICATION'){
                VER_OF_APP = itr.Status_Code__c;
            }
        }
        //if(checkAMLNatioality(SRID)){
            //Add request
            if(CC_CreateConditions.Check_IS_Add_in_SR_Amendment(SRID)){
                if(Quick_Rev =='VERIFIED'){
                    isstepRACreate  = true;
                }
            }
            //Update request
            if(!CC_CreateConditions.Check_IS_Add_in_SR_Amendment(SRID) && CreateConditionExtension.checkAMLNatioality(SRID)){
                if(VER_OF_APP=='VERIFIED'){
                    isstepRACreate  = true;
                }
            }
        //}
        system.debug('---isstepRACreate--'+isstepRACreate);
        return isstepRACreate;
    }
    
     //Check AML create conditions for UBO SR // v1.1 added
    public static boolean checkUBOAMLCreate(String SRID){
        Boolean isstepRACreate = false;
        List<Step__c> stepList = [select id,Step_Name__c,SR__c,Step_Code__c,Status_Code__c from Step__c where SR__c=:srID];
        String Quick_Rev = null;
        String VER_OF_APP = null;
        
        for(Step__c itr:stepList){
            if(itr.Step_Code__c=='QUICK_REVIEW_BY_EMPLOYEE'){
                Quick_Rev = itr.Status_Code__c;
            }
            if(itr.Step_Code__c=='VERIFICATION_OF_APPLICATION'){
                VER_OF_APP = itr.Status_Code__c;
            }
            
        }
        if(checkUBONationality(SRID)){
            //Add request
            if(CC_CreateConditions.Check_IS_Add_in_SR_Amendment(SRID)){
                if(Quick_Rev =='VERIFIED'){
                    isstepRACreate  = true;
                }
            }
            //Update request
            if(!CC_CreateConditions.Check_IS_Add_in_SR_Amendment(SRID)){
                if(VER_OF_APP=='VERIFIED'){
                    isstepRACreate  = true;
                }
            }
        }
        return isstepRACreate;
    }
    
    // Check Review and Approve create condition
    public static boolean UBOReviewandApprove(String SRID){// v1.1 added
        Boolean isstepRACreate = false;
        if(!isReviewandApproveCreated(SRID)){
            try{
            List<Step__c> stepList = [select id,Step_Name__c,SR__c,Step_Code__c,Status_Code__c from Step__c where SR__c=:srID];
            String Quick_Rev = null;
            String Security_Email = null;
            String AML_step = null;
            String VER_OF_APP = null;
            
            for(Step__c itr:stepList){
                if(itr.Step_Code__c=='QUICK_REVIEW_BY_EMPLOYEE'){
                    Quick_Rev = itr.Status_Code__c;
                }
                if(itr.Step_Code__c=='SECURITY_EMAIL_APPROVAL'){
                    Security_Email = itr.Status_Code__c;
                }
                if(itr.Step_Code__c=='AML_APPROVAL'){
                    AML_step = itr.Status_Code__c;
                }
                if(itr.Step_Code__c=='VERIFICATION_OF_APPLICATION'){
                    VER_OF_APP = itr.Status_Code__c;
                }
                
            }
            //Add request
            if(CC_CreateConditions.CheckUBODataAdded(SRID)){
                if(checkUBONationality(SRID)){
                    if(Quick_Rev =='VERIFIED' && Security_Email =='VERIFIED' && AML_step=='APPROVED' && VER_OF_APP=='VERIFIED'){
                        isstepRACreate  = true;
                    }
                    if(Quick_Rev =='VERIFIED' && Security_Email =='REJECTED' && AML_step=='CLOSED' && VER_OF_APP=='VERIFIED'){
                        isstepRACreate  = true;
                    }
                    if(Quick_Rev =='VERIFIED' && Security_Email =='CLOSED' && AML_step=='REJECTED' && VER_OF_APP=='VERIFIED'){
                        isstepRACreate  = true;
                    }
                    if(Quick_Rev=='VERIFIED' && Security_Email == 'CLOSED' && AML_step=='REJECTED' && VER_OF_APP=='CLOSED'){
                        isstepRACreate  = true;
                    }
                    if(Quick_Rev =='VERIFIED' && Security_Email =='REJECTED' && AML_step=='CLOSED' && VER_OF_APP=='CLOSED'){
                        isstepRACreate  = true;
                    }
                }else{
                    if(Quick_Rev =='VERIFIED' && Security_Email =='VERIFIED' && VER_OF_APP=='VERIFIED'){
                        isstepRACreate  = true;
                    }
                    if(Quick_Rev =='VERIFIED' && Security_Email =='REJECTED' && VER_OF_APP=='VERIFIED'){
                        isstepRACreate  = true;
                    }
                    if(Quick_Rev =='VERIFIED' && Security_Email =='CLOSED' && VER_OF_APP=='VERIFIED'){
                        isstepRACreate  = true;
                    }
                    if(Quick_Rev=='VERIFIED' && Security_Email == 'CLOSED' && VER_OF_APP=='CLOSED'){
                        isstepRACreate  = true;
                    }
                    if(Quick_Rev =='VERIFIED' && Security_Email =='REJECTED' && VER_OF_APP=='CLOSED'){
                        isstepRACreate  = true;
                    }
                }
                
            }
            //Update request
            if(CC_CreateConditions.CheckUBODataUpdate(SRID)){
                if((VER_OF_APP=='VERIFIED' && AML_step=='APPROVED') && checkUBONationality(SRID)){
                    isstepRACreate  = true;
                }
                if((VER_OF_APP=='VERIFIED' && AML_step=='REJECTED') && checkUBONationality(SRID)){
                    isstepRACreate  = true;
                }
                if(VER_OF_APP=='VERIFIED' && !checkUBONationality(SRID)){
                    isstepRACreate  = true;
                }
            }
             
            }
            catch(Exception e){
                Log__c objLog = new Log__c();
                objLog.Type__c = 'checkReviewandApprove';
                objLog.Description__c =e.getMessage();
                insert objLog;
            }
        }
        return isstepRACreate;
    }
    // Auto Approve the SR if AML and Security step verified for Phase two SR types
    public static void SRAutoApproveTwo(string srID){// v1.1 added
    system.debug('--SRAutoApprove--'+srID);
         Boolean isBCNation = false;
        //step__c stp = [select id,SR__c,status__c,ownerID from step__c where SR__c=:srID and Step_Name__c='Review and Approve'];
        List<Step__c> stepList = [select id,Step_Name__c,SR__c,Step_Code__c,Status_Code__c from Step__c where SR__c=:srID];
        
        List<service_request__c> serviceReqDetails = new List<service_request__c>([select id,Customer__r.ROC_Status__c from Service_Request__c where id=:stepList[0].SR__c]);
        
        Boolean isVAVerified = false;
        Boolean isAMLVerified = false;
        Boolean isSRAddReq = CC_CreateConditions.Check_IS_Add_in_SR_Amendment(stepList[0].SR__c);
        Boolean isAddRequestverified = false;
        Boolean isUpdateRequestVerified = false;
        
       step__c stpupdate;
        
        if(stepList.size()>0){
            for(Step__c itr: stepList){  
                if(itr.Step_Code__c=='AML_APPROVAL' && (itr.Status_Code__c=='VERIFIED' || itr.Status_Code__c=='APPROVED' )){ //update and add request
                    isAMLVerified = true;
                }
                if(itr.Step_Code__c=='VERIFICATION_OF_APPLICATION' && itr.Status_Code__c=='VERIFIED'){
                    isVAVerified = true;
                }
                if(itr.Step_Name__c=='Review and Approve'){
                    stpupdate = new step__c(id=itr.id,status__c = id.valueof(Label.StepStatusVer),ownerID = userinfo.getUserID());
                }
            }
        }
        //add request
        if(isSRAddReq && isAMLVerified && isVAVerified){
            isAddRequestverified = true;
        }else if(!isSRAddReq && checkAMLNatioality(SRID) && isAMLVerified && isVAVerified){
            isUpdateRequestVerified = true;
        }
        else if(!isSRAddReq && !checkAMLNatioality(SRID) && isVAVerified){
            isUpdateRequestVerified = true;
        }
        
        if((isAddRequestverified || isUpdateRequestVerified) && stpupdate!=null){
            //stp.status__c = statusList[0].id;
             List<Thompson_Reuters_Check__c> trclst = [Select t.Id,t.Step__c,t.Contact__c,TRCase_No__c,TR_Case_Status__c From Thompson_Reuters_Check__c t Where ServiceRequest__c =:srID and TRCase_No__c!=null];
             Boolean isTRCheck= false;
                if(trclst.size()>0){
                    for(Thompson_Reuters_Check__c trc:trclst){
                        if(trc.TRCase_No__c!=null && trc.TR_Case_Status__c!= system.label.TR_Case_Status){
                            isTRCheck = true;
                        }
                    }
                }
                if(!isTRCheck){
                    serviceReqDetails[0].External_SR_Status__c = id.valueof(Label.stepstatusApp) ; //srStatusList[0].id;
                    serviceReqDetails[0].Internal_SR_Status__c = id.valueof(Label.stepstatusApp) ; //srStatusList[0].id;
                    try{
                        //stp.ownerID = userinfo.getUserID();
                        if(stpupdate.Id!=null){
                            Update stpupdate;
                        }
                        isBCNation = true;
                        Update serviceReqDetails;
                        
                    }
                    catch(Exception e){
                        
                       Log__c objLog = new Log__c();
                       objLog.Type__c = 'SR Auto Approval method';
                       objLog.Description__c = 'Auto Approval failed'+e.getMessage()+' Line # '+e.getLineNumber();              
                       insert objLog;

                    }
                }
           
        }
        //return isBCNation;
    }
    // v1.2 Start
    public static boolean checkEconomicSubstance(String SRID){// v1.2 
        Boolean isFinancialYearMatched = false;
        List<service_request__c> SRData = new List<service_request__c>([select id,Customer__r.ROC_Status__c,Customer__r.Financial_Year_End__c,Customer__c from Service_Request__c where id=:SRID]);
        
        List<EconomicSubstanceFinancialYearEnd__mdt> finList = [select Id,Financial_Year_End__c from EconomicSubstanceFinancialYearEnd__mdt where Financial_Year_End__c=:SRData[0].Customer__r.Financial_Year_End__c limit 1];
            if(finList.size()>0){
                isFinancialYearMatched =  true;
            }
         return isFinancialYearMatched;
    }
    
}