/***********************************************************************
* Class: CalculateWorkingDays
* Description: we can use this class to calculate the working days
* Created : 12/06/2019 Selva
* Modified :
* v1.1      selva   #7384   Update the logic to calculate the holidays days difference
***********************************************************************/
Public class CalculateWorkingDays{

    public Integer calculateWorkingDaysBetweenTwoDates(Date date1,Date date2){
        Integer allWorkingDays=0;
        /************
        if(date1!=null && date2!=null){
        
        List<Holiday> holidays=[Select h.StartTimeInMinutes, h.Name, h.ActivityDate From Holiday h];
        
        Integer allDaysBetween = date1.daysBetween(date2);
        system.debug('--allDaysBetween--'+allDaysBetween);
        for(Integer k=0;k<allDaysBetween ;k++ ){
            if(checkifItisWorkingDay(date1.addDays(k),holidays)){
                allWorkingDays++;
            } 
        }
        }
        ***************/
        allWorkingDays = calculateWorkingDays(date1,date2); //v1.1 added
        system.debug('----allWorkingDays---'+allWorkingDays);
        return allWorkingDays;
    
    }
    /****
    public boolean checkifItisWorkingDay(Date currentDate,List<Holiday> holidays){
        Date weekStart  = currentDate.toStartofWeek().addDays(-1);
        system.debug('--week start--'+weekStart);
        for(Holiday hDay:holidays){
        system.debug('--hDay--'+hDay.ActivityDate+'----'+currentDate);
            if(currentDate.daysBetween(hDay.ActivityDate) == 0){
                     return false;
            }
        }
        system.debug('---ss---'+weekStart.daysBetween(currentDate));
        if(weekStart.daysBetween(currentDate) ==0 || weekStart.daysBetween(currentDate) == 5){
           return false;
        } else{
            return true;
        } 
    }
    // method to calculate the days difference excluding weekends- since we including the weekends so comment out this method
    
    public static Integer daysBetweenExcludingWeekends(Datetime startDate, Datetime endDate) {
        Integer i = 0;

        while (startDate < endDate) {
            if (startDate.format('E') != 'Fri' && startDate.format('E') != 'Sat') {
                i++;
            }
            startDate = startDate.addDays(1);
        }
    return i;
    }****/
    //v1.1 added
    public static Integer calculateWorkingDays(Date startDate, Date endDate)  {    
        Set<Date> holidaysSet = new Set<Date>();  
         
        for(Holiday currHoliday :[Select h.StartTimeInMinutes, h.Name, h.ActivityDate From Holiday h]){  
            holidaysSet.add(currHoliday.ActivityDate);  
        }  
         
        Integer workingDays = 0;  
         
        for(integer i=0; i <= startDate.daysBetween(endDate); i++){  
            Date dt = startDate + i;  
            DateTime currDate = DateTime.newInstance(dt.year(), dt.month(), dt.day());  
            String todayDay = currDate.format('EEEE');  
           
            if(todayDay != 'Friday1' && todayDay !='Saturday1' && (!holidaysSet.contains(dt))){  
                    workingDays = workingDays + 1;  
                }     
               
        }      
    
        return workingDays-1;  
    }  
 
}