/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_DocumentDetailTrg {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test

        Id gsContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();

        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;

        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = gsContactRecordTypeId;
        insert objContact;

        Relationship__c rel = new Relationship__c();
        rel.Subject_Account__c = objAccount.Id;
        rel.Object_Contact__c = objContact.Id;
        rel.Relationship_Type__c = 'Has Temporary Employee';
        insert rel;       
       
        Document_Details__c empVisa = new Document_Details__c();
        empVisa.Contact__c = objContact.Id;
        empVisa.Document_Type__c = 'Temporary Work Permit';
        empVisa.DOCUMENT_STATUS__c = 'Active';
        empVisa.ISSUE_DATE__c = system.today().addDays(-3);
        empVisa.EXPIRY_DATE__c = system.today().addDays(5);
        insert empVisa;
       
        empVisa.EXPIRY_DATE__c = system.today().addDays(-8);
        update empVisa;   
    
    }
    

    static testMethod void entryPermitUnitTest() {
        // TO DO: implement unit test

        Id gsContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();

		Id gsSRTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('DIFC Sponsorship Visa-New').getRecordTypeId();

        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;

        Service_Request__c newSR = new Service_Request__c();
        newSR.recordTypeId = gsSRTypeId;
        newSR.Customer__c = objAccount.Id;
        newSR.Service_Category__c = 'New';
        insert newSR;

        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = gsContactRecordTypeId;
        objContact.SR__c = newSR.Id;
        insert objContact;

        Document_Details__c empVisa = new Document_Details__c();
        empVisa.Contact__c = objContact.Id;
        empVisa.Document_Type__c = 'Entry Permit';
        empVisa.DOCUMENT_STATUS__c = 'Active';
        empVisa.ISSUE_DATE__c = system.today().addDays(-3);
        empVisa.EXPIRY_DATE__c = system.today().addDays(5);
        empVisa.Document_Number__c = '1234';
        insert empVisa;
       
    
    }    
    
}