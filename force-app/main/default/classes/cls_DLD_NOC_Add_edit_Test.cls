@isTest
public without sharing class cls_DLD_NOC_Add_edit_Test
{

static testmethod void myTest1(){
      
    map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DLD_NOC')])
        {
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        objAccount.ROC_Status__c = 'Active';
        objAccount.Financial_Year_End__c = 'Yes';
        insert objAccount;
        
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
         system.runAs(objUser){
        
    Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        objSR.RecordTypeId = mapRecordTypeIds.get('DLD_NOC');
        objSR.Service_Category__c = 'New';
        objSR.I_agree__c=true;
        objSR.Type_of_Lease__c = 'Lease';
        objSR.Rental_Amount__c = 1234;
        objSR.Financial_Year_End_mm_dd__c ='Test';
         objSR.Sponsor_Mother_Full_Name__c='sss';
         
         objSR.In_Accordance_with_Article_11__c=true;
         objSR.In_accordance_with_Article_10_1__c=true;
         objSR.In_Accordance_with_Article_12_1a__c=true;
         
         
         
         
        insert objSR;
        
        Property__c Objpro=new Property__c();
        Objpro.Service_Request__c=objSR.id;
        Objpro.Account__c=objAccount.id;
        Objpro.Property_Type__c='Plot';
        Objpro.Property_No__c='Plot';
        insert Objpro;
        
         Property__c Objpro1=new Property__c();
        Objpro1.Service_Request__c=objSR.id;
        Objpro1.Account__c=objAccount.id;
        Objpro1.Property_Type__c='Plot';
        Objpro1.Property_No__c='Peelot';
        insert Objpro1;
        
        
        Apexpages.currentPage().getParameters().put('RecordType',mapRecordTypeIds.get('DLD_NOC'));
        Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
            
    cls_DLD_NOC_Add_edit confirmation = new cls_DLD_NOC_Add_edit (con);
    confirmation.getBCUBOs();
    confirmation.getSrListUBO();
    confirmation.AddUBORecord();
    confirmation.UboValueChange();
    confirmation.SaveUBORowAndAdd();
    
    confirmation.SaveUBORow();
   // confirmation.removingUBORow();
    confirmation.getSrListProperties();
      confirmation.AddRecord();
   Apexpages.currentPage().getParameters().put('myParam','1');
        confirmation.RowValueChange();
      
        confirmation.CancelSaveRow();
        confirmation.SavePropertyRowAndAdd();
        confirmation.validationRule();
        confirmation.SaveRecord();  
          
    // Apexpages.currentPage().getParameters().put('RecordType',mapRecordTypeIds.get('DLD_NOC'));
    }
  
   
  }
  static testmethod void myTest2()
  {  
  Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        objAccount.ROC_Status__c = 'Active';
        objAccount.Financial_Year_End__c = 'Yes';
        insert objAccount;
        
        Account objAccount1 = new Account();
        objAccount1 .Name = 'Test Custoer 1';
        objAccount1 .E_mail__c = 'test@test.com';
        objAccount1 .BP_No__c = '222222222';
        objAccount1 .Company_Type__c = 'Financial - related';
        objAccount1 .Sector_Classification__c = 'Authorised Market Institution';
        objAccount1 .Legal_Type_of_Entity__c = 'LTD';
        objAccount1 .ROC_Status__c = 'Active';
        objAccount1 .Financial_Year_End__c = 'Yes';
        insert objAccount1 ;
        
        
        
        
        Relationship__c ObjRel=new Relationship__c();
        ObjRel.Subject_Account__c=objAccount.id;
        ObjRel.Active__c=true;
        ObjRel.Relationship_Type__c='Is Shareholder Of';
          ObjRel.Object_Account__c=objAccount1.id;
     insert ObjRel;
              
        
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.in';
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.in', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.in');
        insert objUser;
        
         system.runAs(objUser){
        Service_Request__c objSR2 = new Service_Request__c();
        objSR2.Sponsor_Mother_Full_Name__c='UBO';
        Apexpages.Standardcontroller con1 = new Apexpages.Standardcontroller(objSR2 );
          cls_DLD_NOC_Add_edit confirmation = new cls_DLD_NOC_Add_edit (con1 );
           confirmation.getBCUBOs();
          confirmation.validationRule();
        confirmation.SaveRecord();  
        
         }
  
  }
   

}