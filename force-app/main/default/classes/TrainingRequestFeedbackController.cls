public without sharing class TrainingRequestFeedbackController {
    public string comments {get;set;}
    public string rating {get;set;}
    public boolean isThank {get;set;}
    public boolean isSubmitted {get;set;}
    public string recordId;
    public string emailRefId;
    public TrainingRequestFeedbackController(){
        recordId = ApexPages.currentPage().getParameters().get('Id');
    	isThank = false;
        isSubmitted = false;
        rating = 'Good';
        for(Training_Request__c trainingRequest : [select Status__c from Training_Request__c where id = :recordId]){
            if(trainingRequest.Status__c != 'Attended')
                isSubmitted = true;
        }
    }
    public pageReference doSubmit(){
        try{
            Case_Feedback__c feedbackObj = new Case_Feedback__c();
            feedbackObj.RecordTypeId=  Schema.SObjectType.Case_Feedback__c.getRecordTypeInfosByName().get('Training Request').getRecordTypeId();
            feedbackObj.TrainingRequest__c = recordId;
            feedbackObj.Rating__c = rating;
            feedbackObj.Comment__c = comments;
            insert feedbackObj;
            isThank = true;
            return null;
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage()));
            return null;
        }
    }
}