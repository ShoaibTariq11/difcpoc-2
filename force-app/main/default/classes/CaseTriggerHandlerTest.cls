@isTest
public with sharing class CaseTriggerHandlerTest {
    
    public static Case caseobj;
    public static Case_SLA__c sla;
    public static Case_SLA_Logic__c logic;
    @isTest static void test(){
        
        
        logic= new Case_SLA_Logic__c(
        Global_SLA__c=true,
        Awaiting_Response_Days__c=20);
        
        insert logic;
        
        caseObj = new Case(
        GN_Type_of_Feedback__c = 'Complaint',
        Type = 'Parking',
        Status = 'In Progress',
        Origin = 'Phone',
        Subject = 'Testing',
        Full_Name__c='Raghav',
        GN_Account_Name_Non_DIFC__c = 'test',
        GN_Proposed_Due_Date__c = System.today() + 5);
        
        insert caseObj;
        
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Case');
            insert testQueue;
        }
        
        //Case aCase = new Case();
        //insert aCase;
        caseObj.OwnerId = testGroup.Id;
        
        update caseObj;
        
        Task n= new Task(
        WhatId=caseObj.id,
        Status='Completed');
        
        insert n;
        
        
        sla= new Case_SLA__c(
        Parent__c=caseObj.id,
        Until__c=null,
        Business_Hours_Id__c='01m20000000BOf6', 
        From__c=system.now(),
        Due_Date_Time__c=system.now().addmonths(1),
        Change_Type__c='Owner');
        
        insert sla;
        
        caseObj.Status='Awaiting Response';
        caseObj.GN_Type_of_Feedback__c='Enquiry';
        update caseObj;
    }
    
    @isTest static void test1(){
        
        system.debug('test1 class');
        logic= new Case_SLA_Logic__c(
        Global_SLA__c=false,
        Awaiting_Response_Days__c=20);
        
        insert logic;
        
        caseObj = new Case(
        GN_Type_of_Feedback__c = 'Complaint',
        Type = 'Parking',
        Status = 'Awaiting Response',
        Origin = 'Phone',
        Subject = 'Testing',
        Full_Name__c='Raghav',
        GN_Sys_Case_Queue__c='Retail',
        GN_Account_Name_Non_DIFC__c = 'test',
        GN_Proposed_Due_Date__c = System.today() + 5);
        
        insert caseObj;
        
        Task n= new Task(
        WhatId=caseObj.id,
        Status='Completed');
        
        insert n;
        
        sla= new Case_SLA__c(
        Parent__c=caseObj.id,
        Until__c=null,
        Business_Hours_Id__c='01m20000000BOf6', 
        From__c=system.now(),
        Due_Date_Time__c=system.now().addmonths(1),
        Change_Type__c='Owner');
        
        insert sla;
        
        caseObj.Status='New';
        update caseObj;
    }
    
    @isTest static void test3(){
        
        system.debug('test3 class');
        logic= new Case_SLA_Logic__c(
        Global_SLA__c=true,
        Awaiting_Response_Days__c=20);
        
        insert logic;
        
        caseObj = new Case(
        GN_Type_of_Feedback__c = 'Enquiry',
        Type = 'Parking',
        Status = 'In Progress',
        Origin = 'Phone',
        Subject = 'Testing',
        GN_Account_Name_Non_DIFC__c = 'test',
        Full_Name__c='Raghav');
        
        insert caseObj;
        
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Case');
            insert testQueue;
        }
        
        //Case aCase = new Case();
        //insert aCase;
        caseObj.OwnerId = testGroup.Id;
        
        update caseObj;
        
        Task n= new Task(
        WhatId=caseObj.id,
        Status='Completed');
        
        insert n;
        
        
        sla= new Case_SLA__c(
        Parent__c=caseObj.id,
        Until__c=null,
        Business_Hours_Id__c='01m20000000BOf6', 
        From__c=system.now(),
        Due_Date_Time__c=system.now().addmonths(1),
        Change_Type__c='Owner');
        
        insert sla;
        
        caseObj.Status='Awaiting Response';
        caseObj.GN_Type_of_Feedback__c='Complaint';
        caseObj.GN_Proposed_Due_Date__c = System.today() + 5;
        update caseObj;
    }
    @isTest static void test4(){
        
        system.debug('test4 class');
        logic= new Case_SLA_Logic__c(
        Global_SLA__c=true,
        Awaiting_Response_Days__c=20);
        
        insert logic;
        
        caseObj = new Case(
        GN_Type_of_Feedback__c = 'Enquiry',
        Type = 'Parking',
        Status = 'In Progress',
        Origin = 'Email',
        Subject = 'Testing',
        GN_Account_Name_Non_DIFC__c = 'test',
        Full_Name__c='Raghav');
        
        insert caseObj;
        
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Case');
            insert testQueue;
        }
        
        //Case aCase = new Case();
        //insert aCase;
        caseObj.OwnerId = testGroup.Id;
        
        update caseObj;
        
        Task n= new Task(
        WhatId=caseObj.id,
        Status='Completed');
        
        insert n;
        
        
        sla= new Case_SLA__c(
        Parent__c=caseObj.id,
        Until__c=null,
        Business_Hours_Id__c='01m20000000BOf6', 
        From__c=system.now(),
        Due_Date_Time__c=system.now().addmonths(1),
        Change_Type__c='Owner');
        
        insert sla;
        
        caseObj.Status='Awaiting Response';
        caseObj.GN_Type_of_Feedback__c='Complaint';
        caseObj.GN_Proposed_Due_Date__c = System.today() + 5;
        update caseObj;
    }
    @isTest static void test5(){
    
        
        system.debug('test4 class');
        logic= new Case_SLA_Logic__c(
        Global_SLA__c=false,
        Awaiting_Response_Days__c=20);
        
        insert logic;
        
        caseObj = new Case(
        GN_Type_of_Feedback__c = null,
        Type = 'Parking',
        Status = 'In Progress',
        Origin = 'Email',
        Subject = 'Testing',
        GN_Account_Name_Non_DIFC__c = 'test',
        Full_Name__c='Raghav');
        
        insert caseObj;
        
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Case');
            insert testQueue;
        }
        
        //Case aCase = new Case();
        //insert aCase;
        caseObj.OwnerId = testGroup.Id;
        
        update caseObj;
        
        
    }
    
    @isTest static void test6(){
    
        Account objAcct = new Account();
        objAcct.Name = 'Test Account';
        objAcct.Registration_License_No__c = '123456';
        insert objAcct;
    
        system.debug('test4 class');
        logic= new Case_SLA_Logic__c(
        Global_SLA__c=false,
        Awaiting_Response_Days__c=20);
        
        insert logic;
        
        caseObj = new Case(
        GN_Type_of_Feedback__c = null,
        Type = 'Parking',
        Status = 'In Progress',
        Origin = 'Email',
        Subject = 'Testing',
        Full_Name__c='Raghav',
        GN_Registration_No__c = '123456');
        
        insert caseObj;
        
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Case');
            insert testQueue;
        }
        
        //Case aCase = new Case();
        //insert aCase;
        caseObj.OwnerId = testGroup.Id;
        update caseObj; 
    }
     @isTest static void ownershipvalidation(){
         
        Trigger_Settings__c setting = new Trigger_Settings__c();
        setting.Name = 'CaseOwnershipValidation';
        setting.is_Active__c = true;
        insert setting;
        
        ApexCodeUtility.skipTriggerValidation = false;
        logic= new Case_SLA_Logic__c(
        Global_SLA__c=true,
        Awaiting_Response_Days__c=20);
        
        insert logic;
        
        
        
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        
         User objUser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'DIFC Customer Service'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
            //UserRoleId = r.Id
        );
        insert objUser;
        User newobjUser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'DIFC Customer Service'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
            //UserRoleId = r.Id
        );
        insert newobjUser;
        System.runAs(objUser)
        {
            caseObj = new Case(
            GN_Type_of_Feedback__c = 'Complaint',
            Type = 'Parking',
            Status = 'In Progress',
            Origin = 'Phone',
            Subject = 'Testing',
            Full_Name__c='Raghav',
            GN_Proposed_Due_Date__c = System.today() + 5,
            GN_Account_Name_Non_DIFC__c = 'test');
            
            insert caseObj;
            
        
            
            //QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Case');
            //insert testQueue;
        }
        caseObj.OwnerId = newobjUser.Id;
        try{
            update caseObj;
        }
        catch(Exception ex){
            Boolean expectedExceptionThrown =  ex.getMessage().contains('Owner should belong') ? true : false;
            System.assertEquals(expectedExceptionThrown, true);
        }
        //Case aCase = new Case();
        //insert aCase;
        
        
        Task n= new Task(
        WhatId=caseObj.id,
        Status='Completed');
        
        insert n;
        
        
        sla= new Case_SLA__c(
        Parent__c=caseObj.id,
        Until__c=null,
        Business_Hours_Id__c='01m20000000BOf6', 
        From__c=system.now(),
        Due_Date_Time__c=system.now().addmonths(1),
        Change_Type__c='Owner');
        
        insert sla;
        
        caseObj.Status='Awaiting Response';
        caseObj.GN_Type_of_Feedback__c='Enquiry';
        update caseObj;
    }
    @isTest static void test7(){
    
        Account objAcct = new Account();
        objAcct.Name = 'Test Account';
        objAcct.Registration_License_No__c = '123456';
        insert objAcct;
    
        system.debug('test4 class');
        logic= new Case_SLA_Logic__c(
        Global_SLA__c=false,
        Awaiting_Response_Days__c=20);
        
        insert logic;
        
        caseObj = new Case(
        GN_Type_of_Feedback__c = null,
        Type = 'Parking',
        Status = 'Awaiting Response - Client',
        Origin = 'Email',
        Subject = 'Testing',
        Full_Name__c='Raghav',
        GN_Sys_Case_Queue__c = 'IT',
        GN_Registration_No__c = '123456');
        
        insert caseObj;
        
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Case');
            insert testQueue;
        }
        
        //Case aCase = new Case();
        //insert aCase;
        caseObj.OwnerId = testGroup.Id;
        caseObj.Status = 'Awaiting Response - Internal Department';
        update caseObj; 
    }
}