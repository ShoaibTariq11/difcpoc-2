@isTest(seeAllData=false)
public class TestPayment {
    static testMethod void myUnitTest() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Cust 1';        
        objAccount.BP_No__c = '12345';
		objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        string accId=string.valueof(objAccount.id);
        CLS_Payment.Paymentformsubmit(accId,100,'a');
        
        PageReference pageRef = Page.VF_PaymentStatusUpdate;
        Test.setCurrentPageReference(pageRef);
        
        
        Receipt__c R = new Receipt__c();
        R.Customer__c=objAccount.id;
        R.Amount__c=100; 
        R.Receipt_Type__c='Card';
        R.Transaction_Date__c=system.now();           
               
        Id rt=Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get('Card').getRecordTypeId();
        R.RecordTypeId=rt;
        insert R;
        
        Test.starttest();
        
        
        /*
        Receipt__c exR= new Receipt__c();
        exR.Customer__c=objAccount.id;
        exR.Amount__c = 100;
        exR.Receipt_Type__c = 'Cash';
        insert exR;
        */
        
        Test.setMock(WebServiceMock.class, new TestPaymentQueryWebservice());
        //CLS_BatchPaymentReconcile theBatch = new CLS_BatchPaymentReconcile();
        //theBatch.start( (Database.BatchableContext) null );
        //theBatch.execute( (Database.BatchableContext) null, new List<Receipt__c>{ newR} );    
        //theBatch.execute( (Database.BatchableContext) null, null );    
        //theBatch.finish( (Database.BatchableContext) null );
        
        
        
        TestPaymentQueryWebservice.request_x.merchantId = 'a';
        TestPaymentQueryWebservice.request_x.orderNumber = 'b';
        TestPaymentQueryWebservice.request_x.atrn = 'c';
        TestPaymentQueryWebservice.request_x.txntype = '01';
        
         WS_PaymentQuery.InvokeEcomWebServicesPort Query= new WS_PaymentQuery.InvokeEcomWebServicesPort();
         Query.timeout_x = 100000;
        
        String EncResp1= Query.InvokeVoidWS('a','b');
        String EncResp2= Query.InvokeFullAuthReversalWS('a','b');
        String EncResp3= Query.InvokeCaptureWS('a','b');    
        String EncResp4= Query.InvokeReversalWS('a','b','c');
            
        CLS_BatchPaymentReconcile cls = new  CLS_BatchPaymentReconcile();
        //database.executebatch(cls);
        
        cls.EncryptStr('abc');
        //cls.DecryptStr('LxSZngL0MyIwRk68lhOvPEZSFwLB1cvxsjLGGVOZ61D2F5axI0xWN0wxlR8sOhm0ic/vyYwyDZgAYffmNO5ry9w5fnr5rmyno5gYO8dFhptHwemK0lsUIBsbu4js5CpyZop0vycWo78AXBW65eDCag==');
        Receipt__c newR =[select id,name,Payment_Gateway_Ref_No__c,Amount__c,Card_Amount__c from Receipt__c where Receipt_Type__c= 'Card' AND Payment_Inquiry_Done__c=false AND Payment_Status__c= '' limit 1];
        String message = newR.name+'|1001312000011127|AED|1000.00|PENDING|5450545351321|CC|VISA|00000|No Error|01|Enrolled|07|120405|ACCEPT|No Fraud|YES|350.50|USD||||||';
        //Get the encryption key from custom label
            String keyval= Label.Encryption_Key;
            Blob cryptoKey = EncodingUtil.base64Decode(keyval);
            
            //Get the initialization vector 
            Blob iv = Blob.valueof('0123456789abcdef');
            
            //Convert the request string to Blob
            Blob data = Blob.valueOf(message);
            
            //Encrypt the data using Salesforce Crypto class
            Blob encryptedData = Crypto.encrypt('AES256', cryptoKey,iv, data);
            system.debug('EEEE=>'+encryptedData) ;
            
            // Convert the encrypted data to string
            String encryptedMessage  = EncodingUtil.base64Encode(encryptedData); 
            System.debug('EEEESSSS=>'+encryptedMessage);
        ApexPages.CurrentPage().getparameters().put('responseParameter',encryptedMessage);
        
        CLS_PaymentStatusUpdate cls1 = new CLS_PaymentStatusUpdate();
        cls1.updateReceipt();
        PageReference pageRef1 = Page.VF_PaymentStatusUpdate;
        Test.setCurrentPageReference(pageRef1);
        String message1 = Label.Merchant_ID+'|'+newR.name+'|1001312000011127|AED|1000.00|FAILURE|5450545351321|CC|VISA|00000|No Error|01|Enrolled|07|120405|ACCEPT|No Fraud|YES|350.50|USD||||||';
          //Convert the request string to Blob
            Blob data1 = Blob.valueOf(message1);
            
            //Encrypt the data using Salesforce Crypto class
            Blob encryptedData1 = Crypto.encrypt('AES256', cryptoKey,iv, data1);
            system.debug('EEEE=>'+encryptedData) ;
            
            // Convert the encrypted data to string
            String encryptedMessage1  = EncodingUtil.base64Encode(encryptedData1); 
            System.debug('EEEESSSS=>'+encryptedMessage);
        ApexPages.CurrentPage().getparameters().put('responseParameter',encryptedMessage1);
        ApexPages.CurrentPage().getparameters().put('mode','query');
        CLS_PaymentStatusUpdate cls2 = new CLS_PaymentStatusUpdate();
        cls2.updateReceipt();
        
        //String chron= '0 55 23 28 02 ? 2032';
         Datetime dt = system.now().addMinutes(1);
        string chron= dt.second()+' '+dt.minute()+' '+dt.hour()+' '+dt.day()+' '+dt.month()+' '+' ? '+ dt.year();
        // Schedule the test job
        String jobId1 = System.schedule('ScheduleApexTest1',chron, new CLS_BatchPublicRegistry());
        String jobId2 = System.schedule('ScheduleApexTest2',chron, new SchedulePaymentInquiry());
        
        Test.stoptest();
        
        /*
        
        Account acc= new Account();
        acc.name='test';
        acc.Push_to_PR__c=true;
        insert acc;
        
        CLS_BatchPublicRegistry.callPR();
        */
        
    }
}