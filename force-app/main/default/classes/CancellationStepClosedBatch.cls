global class CancellationStepClosedBatch implements Database.Batchable<SObject>, Database.Stateful{    
    
    Map <id,Step__c> stepList;
   
    global CancellationStepClosedBatch (){
       stepList= new Map<id,step__c>();
              
    }
   
    global Database.QueryLocator start(Database.BatchableContext BC){
        Datetime dt =  system.now().addMinutes(-60);
        Datetime dt1 = system.now();
        string SRGroup ='GS';
        string status  = 'CLOSED';
        
         string stepName = system.label.StepCancellationform;
        
        List<string> stepnames = new List<string>();
        
        for(string s : stepName.split(',')){
        
           stepnames.add(s); 
        }
        
        
       //List<string> stepnames = new List<string>{'Cancellation Form Typed'};
        string srStatus = 'The application is under process';
        return Database.getQueryLocator('Select Id, OwnerId,DNRD_Receipt_No__c,Step_Id__c,sr__r.name,Applicant_Name__c,Customer__c,Step_Name__c,SR_Status__c,SAP_PR_No__c,Closed_Date_Time__c,Name From step__c  where DNRD_Receipt_No__c !=null and  Closed_Date_Time__c >=: dt and Closed_Date_Time__c < :dt1 and Sr_group__c = :SRGroup and Step_Name__c in : stepnames and Status_Code__c = : status and sr_status__c =:srStatus  ');
    }
   
    global void execute(Database.BatchableContext BC, List<SObject> scope) {
       
          for(SObject s : scope){
            step__c step = (step__c) s;
            stepList.put(step.id,step);
        }
        
    }
   
    global void finish(Database.BatchableContext BC) {
        if(! stepList.isEmpty()) {
            
         
                String finalstr = 'DNRD Receipt No,Step Id, Service Request No,Applicant Name,Customer, step Name,SAP PR Number,SR Status,Closed Date Time \n';
                String attName = 'step' + system.now().format('YYYYMMDDhhmm') + '.csv';
                for(Id id  : stepList.keySet()){
                    string suc = 'sucesss';
                    step__c stp = stepList.get(id);
                    string recordString = '"'+stp.DNRD_Receipt_No__c+'","'+stp.Name+'","'+stp.sr__r.name+'","'+stp.Applicant_Name__c+'","'+stp.Customer__c+'","'+stp.Step_Name__c+'","'+stp.SAP_PR_No__c+'","'+stp.SR_Status__c+'","'+stp.Closed_Date_Time__c+'",""\n';
                    finalstr = finalstr +recordString;
                }
               
            string EmailAdd =  system.label.BatchEmails ;  
            List<string> Emails = new List<string>();
            
            for(string s : EmailAdd.split(',')){                
                Emails.add(s);              
            }    
               
            Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
            blob csvBlob = Blob.valueOf(finalstr);
            string csvname= 'step.csv';
            csvAttc.setFileName(attName );
            csvAttc.setBody(csvBlob);
            Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
            String[] toAddresses = Emails;
            String subject ='Cancellation form Typed CSV';
            email.setSubject(subject);
            email.setToAddresses( toAddresses );
            email.setPlainTextBody('Cancellation  from Step CSV ');
            email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
               
               
               
               
               /*Create the email attachment    
                Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                efa.setFileName(attName);
                efa.setBody(Blob.valueOf(finalstr));
                efa.setSubject('step csv');
                emailAttList.add(efa);
                 
          
         Sets the paramaters of the email
            email.setSubject('step csv');
            email.setToAddresses( new String[] {'c-veera.narayana@difc.ae'} );
            email.setPlainTextBody( body );
            email.setFileAttachments(emailAttList);
           
         Sends the email
            Messaging.SendEmailResult [] r =
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});*/
        }
    }
}