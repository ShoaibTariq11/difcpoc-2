global class SearchAndReplaceSteps implements Database.Batchable < sObject > {

    global final String Query;


    global SearchAndReplaceSteps(String q) {

        Query = q;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List < Step__c > scope) {
        for (Step__c Sr: scope) {
            
            Sr.Sys_Additional_Email__c =Sr.Sys_Additional_Email__c != NULL ? Sr.Sys_Additional_Email__c +'.invalid' : 'test@difc.ae.invalid';
            Sr.SAP_EMAIL__c  = Sr.SAP_EMAIL__c != NULL ? Sr.SAP_EMAIL__c +'.invalid' : 'test@difc.ae.invalid';
            Sr.Contractor_email_for_fit_out__c =Sr.Contractor_email_for_fit_out__c != NULL ? Sr.Contractor_email_for_fit_out__c +'.invalid' : 'test@difc.ae.invalid';
            Sr.Client_Email_for_fit_out__c  = Sr.Client_Email_for_fit_out__c != NULL ? Sr.Client_Email_for_fit_out__c +'.invalid' : 'test@difc.ae.invalid';
            //Sr.Awaiting_Originals_Email_Text__c =Sr.Awaiting_Originals_Email_Text__c != NULL ? Sr.Awaiting_Originals_Email_Text__c +'.invalid' : 'test@difc.ae.invalid';
            Sr.Applicant_Email__c  = Sr.Applicant_Email__c != NULL ? Sr.Applicant_Email__c +'.invalid' : 'test@difc.ae.invalid';
			Sr.Applicant_Mobile__c = '+971569803616';
            SR.Mobile_No__c = '+971569803616';
            SR.Telephone_Number__c = '+971569803616';
        }
        database.update(scope,false);
    }

    global void finish(Database.BatchableContext BC) {}
}