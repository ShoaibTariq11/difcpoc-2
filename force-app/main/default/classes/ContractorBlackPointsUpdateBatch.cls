/*
    Author        :   Mudasir
    Description   :   This class is used to update the contractor black point calculation 
	Functionality :   Once the Violation has passed a year we need to pick all those and need to update the contractor black points
    --------------------------------------------------------------------------------------------------------------------------
	Modification History
 	--------------------------------------------------------------------------------------------------------------------------
	V.No	Date				Updated By    	Description
	--------------------------------------------------------------------------------------------------------------------------             
 	V1.0    07-October-2018		Mudasir			Created   
*/
global class ContractorBlackPointsUpdateBatch implements Schedulable ,Database.Batchable<sObject>{
	global Void execute(SchedulableContext context){
		ContractorBlackPointsUpdateBatch executebatch = new ContractorBlackPointsUpdateBatch();
		database.executeBatch(executebatch,1);
	}
	
	global list<Account> start(Database.BatchableContext BC ){
        Date createdDateYearValue = system.today().addYears(-1);
        Date createdDate_3_months = system.today().addMonths(-3);
        Date createdDate_6_months = system.today().addMonths(-6);
        list<Violation__c> violationList;
        if(!Test.isRunningTest()){
        	 violationList = [SELECT Id,Contractor__c,Contractor__r.Blacklist_Point_3_months__c, Contractor__r.Blacklist_Point_1_year__c,Contractor__r.Blacklist_Point_6_months__c FROM Violation__c  where (createdDate =:createdDate_3_months or createdDate =:createdDate_6_months or createdDate =:createdDateYearValue) and Contractor__c !=Null];
        }else{ violationList = [SELECT Id,Contractor__c,Contractor__r.Blacklist_Point_3_months__c, Contractor__r.Blacklist_Point_1_year__c, Contractor__r.Blacklist_Point_6_months__c FROM Violation__c];  }
        Map<id,Account> accountMap = new Map<id,Account>();
        if(violationList != Null && violationList.size()> 0){
        	for(Violation__c violationRec : violationList){
	        	Account acc = new Account(id=violationRec.Contractor__c,Blacklist_Point_3_months__c = violationRec.Contractor__r.Blacklist_Point_3_months__c, Blacklist_Point_1_year__c = violationRec.Contractor__r.Blacklist_Point_1_year__c, Blacklist_Point_6_months__c = violationRec.Contractor__r.Blacklist_Point_6_months__c);
	            accountMap.put(violationRec.Contractor__c,acc);
	        }
        }
		return accountMap.values();
	}
	
	global void execute(Database.BatchableContext BC, list<Account> contractorList){
       list<Account> lstSRToUpdate = new list<Account>();
		try{
			for(Account contractorRecord : contractorList){
				//Black point calculation 
				lstSRToUpdate.add(calculateContractorblackPoints(contractorRecord));
			}
			if(!lstSRToUpdate.isEmpty())
				update lstSRToUpdate;
		}catch(Exception ex){ Log__c objLog = new Log__c(); objLog.Type__c = 'Schedule Batch fot the contractor black point updated'; objLog.Description__c = 'Exception is : '+ex.getMessage()+'\nLine # '+ex.getLineNumber(); insert objLog;}
	}
	
	global void finish(Database.BatchableContext BC){	
        
	}
	public static Account calculateContractorblackPoints(Account contractorRecord){
		Date dateYearBefore = System.today().addYears(-1);
		Integer days = dateYearBefore.daysBetween(System.Today());
		String SOQLQuery = 'SELECT Id, contractor__c, Service_Request__c, Fine_Amount_Formula__c, CreatedDate, Related_Request__c, Violation_Date__c, Waive_Fine_Amount__c, Waive_Black_Points__c, Blacklist_Point__c FROM Violation__c WHERE Contractor__c =\'' + contractorRecord.id + '\' and Violation_Date__c = LAST_N_DAYS:' + days + ' ORDER BY Violation_Date__c DESC';
		list < Violation__c > listViolation = Database.Query(SOQLQuery);
		Integer blackListPointsThreeMonths = 0;
        Integer blackListPointsSixMonths = 0;
        Integer blackListPointsOneYear = 0;
		if (!listViolation.isEmpty()) {
			for (Violation__c v: listViolation) {
				/* First Loop: 3 months */
	            if (v.Violation_Date__c.daysBetween(System.Today()) <= 90) {
	                blackListPointsThreeMonths += Integer.valueOf(v.Blacklist_Point__c);
	            }
				/* First Loop: 6 months */
	            if (v.Violation_Date__c.daysBetween(System.Today()) <= 180) {
	                blackListPointsSixMonths += Integer.valueOf(v.Blacklist_Point__c);
	            }
				/* First Loop: 1 year */
	            if (v.Violation_Date__c.daysBetween(System.Today()) <= days) {
	                blackListPointsOneYear += Integer.valueOf(v.Blacklist_Point__c);
	            }
	
	        }
		}
		contractorRecord.Blacklist_Point_3_months__c = blackListPointsThreeMonths;
        contractorRecord.Blacklist_Point_6_months__c = blackListPointsSixMonths;
        contractorRecord.Blacklist_Point_1_year__c = blackListPointsOneYear == 0 ? blackListPointsSixMonths : blackListPointsOneYear;
        return contractorRecord;
	}
}