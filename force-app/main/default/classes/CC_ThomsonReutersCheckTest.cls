/*
* OB_CheckTRCaseStatusSchedule,CC_ThomsonReutersCheck
**/
@isTest
public with sharing class CC_ThomsonReutersCheckTest {
    @isTest
    private static void initThomsonReutersCheckTest(){
        
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insert insertNewContacts;

        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle'});
        insert createSRTemplateList;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].Entity_Type__c = 'Financial - related';
        insertNewSRs[1].Entity_Type__c = 'Financial - related';
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get(Label.Self_Registration_Record_Type).getRecordTypeId();
        insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_Financial').getRecordTypeId();
        insert insertNewSRs;
        List<HexaBPM__Step_Template__c> stepTemplate = OB_TestDataFactory.createStepTemplate(1,
                                                        new List<String>{'Test'},
                                                        new List<String>{'Test'},
                                                        new List<String>{'In_Principle','AOR_Financial'},
                                                        new List<String>{'Test'});

        
        HexaBPM__SR_Steps__c objSRSteps = new HexaBPM__SR_Steps__c();
        objSRSteps.HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        objSRSteps.HexaBPM__Step_Template__c = stepTemplate[0].Id;
        objSRSteps.HexaBPM__Active__c=true;
        insert objSRSteps;
        List<HexaBPM__Step__c> stepList = OB_TestDataFactory.createSteps(2,insertNewSRs,new List<HexaBPM__SR_Steps__c>{objSRSteps});

        List<HexaBPM_Amendment__c> listAmendment = new List<HexaBPM_Amendment__c>();
        
        HexaBPM_Amendment__c objAmendment = new HexaBPM_Amendment__c();
        objAmendment = new HexaBPM_Amendment__c();
        objAmendment.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment.Nationality_list__c = 'India';
        objAmendment.Gender__c = 'Male';
        objAmendment.Role__c = 'Shareholder;UBO';
        objAmendment.Passport_No__c = '12345';
        objAmendment.Date_of_Birth__c = system.today().addYears(-18);
        objAmendment.ServiceRequest__c = insertNewSRs[0].Id;
        objAmendment.First_Name__c = 'Individual 2';
        listAmendment.add(objAmendment);
        
        HexaBPM_Amendment__c objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment1.First_Name__c = 'Individual 1';
        objAmendment1.Nationality_list__c = 'India';
        objAmendment1.Passport_No__c = '1234';
        objAmendment1.Role__c = 'UBO';
        objAmendment1.Gender__c = 'Male';
        objAmendment1.Date_of_Birth__c = system.today().addYears(-18);
        objAmendment1.ServiceRequest__c = insertNewSRs[0].Id;
        objAmendment1.Type_of_Ownership_or_Control__c='The individual(s) who exercises significant control or influence over the Foundation or its Council';
        listAmendment.add(objAmendment1);

        HexaBPM_Amendment__c objAmendment2 = new HexaBPM_Amendment__c();
        objAmendment2 = new HexaBPM_Amendment__c();
        objAmendment2.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Body_Corporate');
        objAmendment2.Nationality_list__c = 'India';
        objAmendment2.Registration_No__c = '1234';
        objAmendment2.Role__c = 'Shareholder';
        objAmendment2.Company_Name__c = 'Corporate';
        objAmendment2.Registration_Date__c = system.today();
        objAmendment2.ServiceRequest__c = insertNewSRs[0].Id;
        objAmendment2.Type_of_Ownership_or_Control__c='The individual(s) who exercises significant control or influence over the Foundation or its Council';
        listAmendment.add(objAmendment2);
        insert listAmendment;
        
        Thompson_Reuters_Check__c rc = new Thompson_Reuters_Check__c();
        rc.Entity_Id__c = '12T_est';
        rc.TR_Check_Result__c = '12T_est';
        rc.Category__c = '12T_est';
        rc.Name__c = '12T_est';
        rc.Original_Script__c = '12T_est';
        rc.Score__c = '12T_est';
        rc.Secondary_Address_Info__c = '12T_est';
        rc.Secondary_City_Info__c = '12T_est';
        rc.Secondary_Country_Info__c = '12T_est';
        rc.Secondary_Date_Of_Birth_Info__c = '12T_est';
        rc.Secondary_Gender_Info__c = 'MALE';
        rc.Entity_Type__c = 'INDIVIDUAL';
        rc.TRCase_No__c = '2000';
        insert rc;
        
        TR_Contact__c trc = new TR_Contact__c(HexaBPM_ServiceRequest__c=insertNewSRs[0].Id,OB_Amendment__c =listAmendment[0].id,
        Gender__c=listAmendment[0].Gender__c,
        Date_of_Birth__c=listAmendment[0].Date_of_Birth__c,
        Nationality_list__c=listAmendment[0].Nationality_list__c);
        
        trc.Given_Name__c = listAmendment[0].First_Name__c;
        
        trc.TR_Contact_type__c = 'Individual';
        trc.Given_Name__c = listAmendment[0].First_Name__c;
        trc.Thomson_Reuters_Check__c = rc.Id;
       
        insert trc;
        
        List<TR_Contact__c> trConList = [Select ID,Name From TR_Contact__c where Id =:trc.Id ];
        
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new CC_ThomsonReutersCheckMockTest(trConList[0].Name));
        //DOM.XMLNode node = XMLNode[ELEMENT,sdResultSet,null,null,null,[XMLNode[ELEMENT,indexDate,null,null,null,[XMLNode[TEXT,null,null,null,null,null,2019-12-24T17:08:33.970+04:00,]],null,], XMLNode[ELEMENT,sdQuery,null,null,null,[XMLNode[ELEMENT,clientID,null,null,null,[XMLNode[TEXT,null,null,null,null,null,TRC-20988,]],null,], XMLNode[ELEMENT,clientType,null,null,null,[XMLNode[TEXT,null,null,null,null,null,CUSTOMER,]],null,], XMLNode[ELEMENT,createCaseOnMatches,null,null,null,[XMLNode[TEXT,null,null,null,null,null,CREATECASE,]],null,], XMLNode[ELEMENT,types,null,null,null,[XMLNode[ELEMENT,type,null,null,null,[XMLNode[TEXT,null,null,null,null,null,INDIVIDUAL,]],null,]],null,], XMLNode[ELEMENT,indDob,null,null,null,[XMLNode[TEXT,null,null,null,null,null,1959-09-23T00:00:00+04:00,]],null,], XMLNode[ELEMENT,indGender,null,null,null,[XMLNode[TEXT,null,null,null,null,null,FEMALE,]],null,], XMLNode[ELEMENT,name,null,null,null,[XMLNode[TEXT,null,null,null,null,null,SITA MAHA LAKSHMI RAMADUGULA,]],null,], XMLNode[ELEMENT,nationalityName,null,null,null,[XMLNode[TEXT,null,null,null,null,null,India,]],null,], XMLNode[ELEMENT,ruleId,null,null,null,[XMLNode[TEXT,null,null,null,null,null,RULE151,]],null,]],null,], XMLNode[ELEMENT,sdResults,null,null,null,null,null,]],null,];
        //CC_ThomsonReutersCheck.checkTRCaseStatus('2000');
        
        /*OB_CheckTRCaseStatusSchedule obj = new OB_CheckTRCaseStatusSchedule();
        DataBase.executeBatch(obj); */
        
        String body = '{"DIFC":"<?xml version="1.0" encoding="UTF-8" standalone="yes"?><sdResultSet><indexDate>2019-12-24T17:08:33.970+04:00</indexDate><sdQuery><clientID>"'+trConList[0].Name+'"</clientID><clientType>CUSTOMER</clientType><createCaseOnMatches>CREATECASE</createCaseOnMatches><types><type>INDIVIDUAL</type></types><name>fsdf fdsf</name><nationalityName></nationalityName><ruleId>RULE151</ruleId></sdQuery><sdResults/></sdResultSet>"}';
        Integer code= 200;
        String status= 'pass';
        Map<String, String> responseHeaders= new Map<String, String>();
        CC_ThomsonReutersCheck cc = new CC_ThomsonReutersCheck();
        CC_ThomsonReutersCheck.caseNumber='2000';
        CC_ThomsonReutersCheck.TRCheckObject= rc;
        cc.EvaluateCustomCode(insertNewSRs[0],stepList[0]);
        Delete trConList;
        cc.EvaluateCustomCode(insertNewSRs[0],stepList[0]);
        test.stoptest();
    }
    
     @isTest
    private static void initThomsonReutersCheckTest2(){
        
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insert insertNewContacts;

        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle'});
        insert createSRTemplateList;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].Entity_Type__c = 'Financial - related';
        insertNewSRs[1].Entity_Type__c = 'Financial - related';
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get(Label.Self_Registration_Record_Type).getRecordTypeId();
        insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_Financial').getRecordTypeId();
        insert insertNewSRs;
        List<HexaBPM__Step_Template__c> stepTemplate = OB_TestDataFactory.createStepTemplate(1,
                                                        new List<String>{'Test'},
                                                        new List<String>{'Test'},
                                                        new List<String>{'In_Principle','AOR_Financial'},
                                                        new List<String>{'Test'});

        
        HexaBPM__SR_Steps__c objSRSteps = new HexaBPM__SR_Steps__c();
        objSRSteps.HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        objSRSteps.HexaBPM__Step_Template__c = stepTemplate[0].Id;
        objSRSteps.HexaBPM__Active__c=true;
        insert objSRSteps;
        List<HexaBPM__Step__c> stepList = OB_TestDataFactory.createSteps(2,insertNewSRs,new List<HexaBPM__SR_Steps__c>{objSRSteps});

        List<HexaBPM_Amendment__c> listAmendment = new List<HexaBPM_Amendment__c>();
        
        HexaBPM_Amendment__c objAmendment = new HexaBPM_Amendment__c();
        objAmendment = new HexaBPM_Amendment__c();
        objAmendment.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment.Nationality_list__c = 'India';
        objAmendment.Gender__c = 'Male';
        objAmendment.Role__c = 'Shareholder;UBO';
        objAmendment.Passport_No__c = '12345';
        objAmendment.Date_of_Birth__c = system.today().addYears(-18);
        objAmendment.ServiceRequest__c = insertNewSRs[0].Id;
        objAmendment.First_Name__c = 'Individual 2';
        listAmendment.add(objAmendment);
        
        HexaBPM_Amendment__c objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment1.First_Name__c = 'Individual 1';
        objAmendment1.Nationality_list__c = 'India';
        objAmendment1.Passport_No__c = '1234';
        objAmendment1.Role__c = 'UBO';
        objAmendment1.Gender__c = 'Male';
        objAmendment1.Date_of_Birth__c = system.today().addYears(-18);
        objAmendment1.ServiceRequest__c = insertNewSRs[0].Id;
        objAmendment1.Type_of_Ownership_or_Control__c='The individual(s) who exercises significant control or influence over the Foundation or its Council';
        listAmendment.add(objAmendment1);

        HexaBPM_Amendment__c objAmendment2 = new HexaBPM_Amendment__c();
        objAmendment2 = new HexaBPM_Amendment__c();
        objAmendment2.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Body_Corporate');
        objAmendment2.Nationality_list__c = 'India';
        objAmendment2.Registration_No__c = '1234';
        objAmendment2.Role__c = 'Shareholder';
        objAmendment2.Company_Name__c = 'Corporate';
        objAmendment2.Registration_Date__c = system.today();
        objAmendment2.ServiceRequest__c = insertNewSRs[0].Id;
        objAmendment2.Type_of_Ownership_or_Control__c='The individual(s) who exercises significant control or influence over the Foundation or its Council';
        listAmendment.add(objAmendment2);
        insert listAmendment;
        
        Thompson_Reuters_Check__c rc = new Thompson_Reuters_Check__c();
        rc.Entity_Id__c = '12T_est';
        rc.TR_Check_Result__c = '12T_est';
        rc.Category__c = '12T_est';
        rc.Name__c = '12T_est';
        rc.Original_Script__c = '12T_est';
        rc.Score__c = '12T_est';
        rc.Secondary_Address_Info__c = '12T_est';
        rc.Secondary_City_Info__c = '12T_est';
        rc.Secondary_Country_Info__c = '12T_est';
        rc.Secondary_Date_Of_Birth_Info__c = '12T_est';
        rc.Secondary_Gender_Info__c = 'MALE';
        rc.Entity_Type__c = 'INDIVIDUAL';
        rc.TRCase_No__c = '2000';
        insert rc;
        
        TR_Contact__c trc = new TR_Contact__c(HexaBPM_ServiceRequest__c=insertNewSRs[0].Id,OB_Amendment__c =listAmendment[0].id,
        Gender__c=listAmendment[0].Gender__c,
        Date_of_Birth__c=listAmendment[0].Date_of_Birth__c,
        Nationality_list__c=listAmendment[0].Nationality_list__c);
        
        trc.Given_Name__c = listAmendment[0].First_Name__c;
        
        trc.TR_Contact_type__c = 'Individual';
        trc.Given_Name__c = listAmendment[0].First_Name__c;
        trc.Thomson_Reuters_Check__c = rc.Id;
       
        insert trc;
        
        List<TR_Contact__c> trConList = [Select ID,Name From TR_Contact__c where Id =:trc.Id ];
        
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new CC_ThomsonReutersCheckMockTest(trConList[0].Name));
         //OB_CheckTRCaseStatusSchedule sh1 = new OB_CheckTRCaseStatusSchedule();
        String sch ='0 0 0 * * ?'; 
        //System.schedule('Test', sch,sh1);
        test.stoptest();
    }
}