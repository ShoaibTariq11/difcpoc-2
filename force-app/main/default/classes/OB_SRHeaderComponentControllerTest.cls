/*
    Author      : Leeba
    Date        : 1-April-2020
    Description : Test class for OB_SRHeaderComponentController and OB_SRHeaderComponentControllerHelper
    --------------------------------------------------------------------------------------
*/
@isTest
public class OB_SRHeaderComponentControllerTest {
    public static testmethod void test1(){
       
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insertNewAccounts[0].OB_Sector_Classification__c = 'Investment Fund';
        insertNewAccounts[0].Company_Type__c = 'Financial - related';
        insert insertNewAccounts;
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(2,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('Converted_User_Registration', 1);
        listPageFlow[0].Display_as_Index__c = true;
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        insert listPage;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insert insertNewSRs;
        
        HexaBPM__SR_Status__c objstatus = new HexaBPM__SR_Status__c();
        objstatus.HexaBPM__Code__c = 'NOT_INTERESTED'; 
        objstatus.Name = 'Submitted';      
        insert objstatus;
        
        HexaBPM__Step_Template__c objstepTemp = new HexaBPM__Step_Template__c();
        objstepTemp.HexaBPM__Code__c = 'BD_ENTITY_WITHDRAWAL_REVIEW';
        objstepTemp.HexaBPM__Step_RecordType_API_Name__c = 'General';
        insert objstepTemp;
        
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = objstatus.id;
        insertNewSRs[0].HexaBPM__SR_Template__c = createSRTemplateList[0].id;
        update insertNewSRs[0];
              
        HexaBPM__SR_Steps__c objsrStep = new HexaBPM__SR_Steps__c();
        objsrStep.HexaBPM__SR_Template__c = createSRTemplateList[0].id;
        objsrStep.HexaBPM__Step_Template__c = objstepTemp.id;
        objsrStep.HexaBPM__Step_RecordType_API_Name__c = 'General';
        objsrStep.HexaBPM__Estimated_Hours__c = 2;
        insert objsrStep;
        
        // create pricing 
        HexaBPM__Pricing_Line__c pricing = OB_TestDataFactory.createSRPriceItem();
        insert pricing;
        // Create Products
        List<Product2> products = OB_TestDataFactory.createProduct(new List<String>{'OB Name Reservation', 'OB Name Reservation 1'});
        insert products;
        //create priceitem
        List<HexaBPM__SR_Price_Item__c> createSRPriceItemObj = OB_TestDataFactory.createSRPriceItem(products,insertNewSRs[0].id,pricing.id);
        insert createSRPriceItemObj;
        Test.startTest();
           Id p = [select id from profile where name='DIFC Customer Community User Custom'].id;      
            Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
            insert con;  
                      
            User user = new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                    ContactId = con.Id,
                    timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
           
            insert user;
        system.runAs(user){
           OB_SRHeaderComponentController.loadPageFlow();
           OB_SRHeaderComponentController.getBaseUrl();
           
           OB_SRHeaderComponentController.PageWrapper wrapperObj = new OB_SRHeaderComponentController.PageWrapper();
           wrapperObj.pageObj = listPage[0];
           wrapperObj.communityPageURL = '/test';
           wrapperObj.isLocked = 'false';
           wrapperObj.srPageStatus = 'Complete';
           OB_SRHeaderComponentController.getCommunityURLBasedOnSR(insertNewSRs[0],wrapperObj);
           OB_SRHeaderComponentController.fetchLockingUnlockingWrap(listPageFlow[0].id,insertNewSRs[0].id);
            
           OB_SRHeaderComponentController.RequestWrapper reqWrap = new OB_SRHeaderComponentController.RequestWrapper();
           OB_SRHeaderComponentController.ResponseWrapper resWrap = new OB_SRHeaderComponentController.ResponseWrapper();
           reqWrap.srToWithDraw = insertNewSRs[0].id;
           reqWrap.withdrawalReason = 'test';
           String requestString = JSON.serialize(reqWrap);
           resWrap = OB_SRHeaderComponentController.withdrawApplication(requestString);
           
        }
       Test.stopTest();
    }
}