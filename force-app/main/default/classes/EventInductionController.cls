/***************************************************************************************************************************************************
 *  Author   : Mudasir Wani
 *  Date     : 19 september 2018
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By		Description
1.0		19/09/2018	Mudaisr			Created this class for the Event induction process 


****************************************************************************************************************************************************/
public with sharing class EventInductionController {
	
	//Event service request binded to the page EventInductionRequest
    public Service_Request__c evtServiceRequest{get;set;}
    
    //Auto-Populate Event organizer name on the Event SR
    public string accountName{get;set;}
    
    //Auto-Populate Event organizer related Service Request
    public string relatedRequest{get;set;}
    
    //Fetch the instructions for the related SR from its associated template
    public List<SR_Template__c> SRTemp{get;set;}
    
    //Get the user detils
    private User userDetail; 
    //Get the related contact details
    private Contact contactRecord;
    
     /**
     * Stores the page title
     */
    public String pageTitle{get;set; }
    
    //Added Boolean flag to check blank fields
    public List<String> errMessage{get; set;}
    
    public boolean isContractorAvailable{get;set;}
    
    private String recordTypeId{get;set;}
    
    //Start the process with the constructor
    public EventInductionController(){
    	if(initialize())executeOnload();
    }
    /**
     * Initializes the class members
     */
    @TestVisible private boolean initialize(){
    	evtServiceRequest = new Service_Request__c();
    	accountName ='';
    	relatedRequest = '';
    	SRTemp = new List<SR_Template__c>();
    	userDetail = new User();
    	contactRecord = new Contact();
    	errMessage = new List<String>();
    	isContractorAvailable = false;
    	return true;
    }
    
    /**
     * Populate the required data on the loading of the required page.
     */
    @TestVisible private void executeOnload(){
    	
    	 /*
         * Set the page title
         */
        pageTitle = 'DIFC - Dubai International Financial Centre | Event Service Request Form';
        recordTypeId = ApexPages.currentPage().getParameters().get('RecordType');
    	//Get the event induction template details
    	SRTemp = [select id,autofill_contact__c,Sub_menu__c,SR_Description__c,SR_RecordType_API_Name__c,Menu__c,Document_Type__c,Menutext__c from SR_Template__c where SR_RecordType_API_Name__c='Event_Induction_Request'];
    	populateOrganizerAndLicenseDetails();
    	autopopulateRequestDetails();
    }
    
    /*
    * This method will populate the organizer and license details for event induction request
    * 
    */
    @TestVisible private void populateOrganizerAndLicenseDetails(){
    	//Get user details
    	userDetail = [Select id,name,contactid from User where id=:UserInfo.getUserId()];
    	//Get the contact and account related to the loggedin user
    	contactRecord = [Select id,MobilePhone,name,Account.Name,AccountId,Account.Contractor_License_Expiry_Date__c,Account.PO_Box__c,Account.Fax,Account.Registered_Address__c from Contact where id=:userDetail.contactId ];
    	//The related field values are taken from the contact keeping in view sometimes we update the contact details.
    	evtServiceRequest.Customer__c = contactRecord.AccountId;
    	evtServiceRequest.Contractor__c = contactRecord.AccountId;
    	evtServiceRequest.Expiry_Date__c = contactRecord.Account.Contractor_License_Expiry_Date__c;
    	evtServiceRequest.Mobile_Number__c = contactRecord.MobilePhone;
    	accountName = contactRecord.Account.Name;
    	evtServiceRequest.Company_Name__c = contactRecord.Account.Name;
    }
    /*
    * Auto populate the related SR details along with passport and nationality
    */
    @TestVisible private void autopopulateRequestDetails(){
    	//Get the latest approved "Request for Contractor Wallet" request related to this Event Organizer
    	Service_Request__c eventWalletRequest = [select id,Name_of_the_Authority__c,name,Passport_Number__c, Nationality__c,Nationality_list__c from Service_Request__c where Customer__c=:contactRecord.AccountId and Service_Type__c='Request for Contractor Wallet' and External_Status_Name__c='Approved' order by Submitted_Date__c DESC limit 1];
    	evtServiceRequest.Nationality__c = eventWalletRequest.Nationality__c;
    	evtServiceRequest.Passport_Number__c = eventWalletRequest.Passport_Number__c;
    	evtServiceRequest.Nationality_list__c = eventWalletRequest.Nationality_list__c;
    	evtServiceRequest.Linked_SR__c = eventWalletRequest.id;
    	evtServiceRequest.recordTypeId = recordTypeId;
        evtServiceRequest.Name_of_the_Authority__c = eventWalletRequest.Name_of_the_Authority__c;
    	relatedRequest = eventWalletRequest.Name;
    }
    public PageReference saveSRrequest(){
    	insert evtServiceRequest;
    	PageReference servReqRecPage = new ApexPages.StandardController(evtServiceRequest).view();
        servReqRecPage.setRedirect(true);
        return servReqRecPage;
    }
    public PageReference submitSRrequest(){
    	return null;
    }
    public PageReference cancelRequest(){
    	return null;
    }
    
}