@isTest
private class CRM_cls_EmailUtilsTest {

    @isTest static void getClientContacts_Test() {
        Account AccountObj = new Account(Name = 'Test Account');
        insert AccountObj;

        List<Contact> ListContact = new List<Contact>();
        
        //for(Integer i = 0; i < 5; i++){
        Integer i = 0;
        ListContact.add(new Contact( FirstName = 'Test', LastName = 'Last ' + i, AccountId = AccountObj.Id ) );
        //}

        insert ListContact;
        
        Test.StartTest();

            CRM_cls_EmailUtils.getClientContacts( AccountObj.Id );

        Test.StopTest();
    }

    @isTest static void getPointPersonsTest() {
        Account AccountObj = new Account(Name = 'Test Account', Company_Type__c = 'Financial - related');
        insert AccountObj;
        
        Test.StartTest();

            CRM_cls_EmailUtils.getPointPersons( AccountObj.Id );

        Test.StopTest();
    }

    @isTest static void createEmailLinkWithRMTest() {
        Account AccountObj = new Account(Name = 'Test Account', Company_Type__c = 'Financial - related');
        insert AccountObj;
        
        List<Contact> ListContact = new List<Contact>();
        Integer i = 0;
        ListContact.add(new Contact( FirstName = 'Test', LastName = 'Last ' + i, AccountId = AccountObj.Id ) );
        insert ListContact;

        Opportunity OpportunityObj = new Opportunity( AccountId = AccountObj.Id );
        OpportunityObj.Name = AccountObj.Name + ' Opp';
        OpportunityObj.CloseDate = Date.today().AddDays(31);
        OpportunityObj.StageName = 'Created';
        //OpportunityObj.Stage_Steps__c = 'In Progress';
        insert OpportunityObj;

        Test.StartTest();

            CRM_cls_EmailUtils.createEmailLinkWithRM( OpportunityObj.Id, OpportunityObj.AccountId, 
                                                        OpportunityObj.StageName, 'test@test.com', 
                                                        OpportunityObj.Stage_Steps__c , 'Test', 
                                                        'Test_Email_Template_Code' );

        Test.StopTest();
    }   

    @isTest static void getEmailTemplateByStageTest() {
        
        Account AccountObj = new Account(Name = 'Test Account', Company_Type__c = 'Financial - related');
        insert AccountObj;
        
        List<Contact> ListContact = new List<Contact>();
        Integer i = 0;
        ListContact.add(new Contact( FirstName = 'Test', LastName = 'Last ' + i, AccountId = AccountObj.Id ) );
        insert ListContact;

        Opportunity OpportunityObj = new Opportunity( AccountId = AccountObj.Id );
        OpportunityObj.Name = AccountObj.Name + ' Opp';
        OpportunityObj.CloseDate = Date.today().AddDays(31);
        OpportunityObj.StageName = 'Created';
        //OpportunityObj.Stage_Steps__c = 'In Progress';
        insert OpportunityObj;

        Test.StartTest();

            CRM_cls_EmailUtils.createEmailLinkWithRM( OpportunityObj.Id, OpportunityObj.AccountId, 
                                                        OpportunityObj.StageName, 'test@test.com', 
                                                        OpportunityObj.Stage_Steps__c , 'Test', 
                                                        '' );

        Test.StopTest();
    }

    @isTest static void getEmailAttachmentsByRecipientTest() {

        Account AccountObj = new Account(Name = 'Test Account', Company_Type__c = 'Financial - related');
        insert AccountObj;
        
        List<Contact> ListContact = new List<Contact>();
        Integer i = 0;
        ListContact.add(new Contact( FirstName = 'Test', LastName = 'Last ' + i, AccountId = AccountObj.Id ) );
        insert ListContact;

        Task TaskObj = new Task();

        TaskObj.WhatId = AccountObj.Id;
        TaskObj.WhoId  = ListContact[0].Id;
        TaskObj.Subject = 'Test Task';
        TaskObj.ActivityDate = Date.Today();
        insert TaskObj;

        Attachment AttachmentObj = new Attachment();
        AttachmentObj.Name = 'Unit Test';
        Blob bodyBlob = Blob.valueOf('Unit Test');
        AttachmentObj.body = bodyBlob;
        AttachmentObj.parentId = TaskObj.id;
        insert AttachmentObj;

        //Insert emailmessage for case
        EmailMessage EmailMessagObj = new EmailMessage();
        EmailMessagObj.FromAddress = 'test@test.com';
        EmailMessagObj.Incoming = True;
        EmailMessagObj.ToAddress= 'test@test.com';
        EmailMessagObj.Subject = 'Test email';
        EmailMessagObj.HtmlBody = 'Test email body';
        EmailMessagObj.RelatedToId = AccountObj.Id;
        //EmailMessagObj.ActivityId = TaskObj.Id;
        insert EmailMessagObj;
        
        Test.StartTest();

            CRM_cls_EmailUtils.getEmailAttachmentsByRecipient(AccountObj.Id, ListContact[0].Id );

        Test.StopTest();
    }   

    @isTest static void getMassEmailActivitiesTest() {
        
        Account AccountObj = new Account(Name = 'Test Account', Company_Type__c = 'Financial - related');
        insert AccountObj;
        
        List<Contact> ListContact = new List<Contact>();
        Integer i = 0;
        ListContact.add(new Contact( FirstName = 'Test', LastName = 'Last ' + i, AccountId = AccountObj.Id ) );
        insert ListContact;

        Task TaskObj = new Task();

        TaskObj.WhatId = AccountObj.Id;
        TaskObj.WhoId  = ListContact[0].Id;
        TaskObj.Subject = 'Mass Email';
        TaskObj.ActivityDate = Date.Today();
        insert TaskObj;
        
        Test.StartTest();

            CRM_cls_EmailUtils.getMassEmailActivities();

        Test.StopTest();
    }
    
    static testmethod void testFinal(){
        
        Account AccountObj = new Account(Name = 'Test Account', Company_Type__c = 'Financial - related');
        insert AccountObj;
        
        List<Contact> ListContact = new List<Contact>();
        Integer i = 0;
        ListContact.add(new Contact( FirstName = 'Test', LastName = 'Last ' + i, AccountId = AccountObj.Id ) );
        insert ListContact;

        Opportunity OpportunityObj = new Opportunity( AccountId = AccountObj.Id );
        OpportunityObj.Name = AccountObj.Name + ' Opp';
        OpportunityObj.CloseDate = Date.today().AddDays(31);
        OpportunityObj.StageName = 'Created';
        insert OpportunityObj;
        
        CRM_cls_EmailUtils.OppAccountDFSAEmail(OpportunityObj,AccountObj,'application pending',system.today());
    }
}