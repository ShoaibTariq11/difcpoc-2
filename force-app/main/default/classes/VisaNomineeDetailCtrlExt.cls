public class VisaNomineeDetailCtrlExt {
	
    public Long_Term_Visa_Nomination__c LongTermVisaNominationObj;
    public Long_Term_Visa_Nomination__c LongTermVisaNominationAfterSubmissionObj {get;set;}
    public transient Attachment AttachmentObj;
    public transient Attachment ConsentFormObj;
    public Boolean ShowSubmitMsg {get;set;} 
    
    public VisaNomineeDetailCtrlExt(ApexPages.StandardController ctrl){
        ShowSubmitMsg = false;
        
        LongTermVisaNominationObj = (Long_Term_Visa_Nomination__c) ctrl.getRecord();
    }
    
    public Attachment getAttachmentObj(){
        if( AttachmentObj == null ){
            AttachmentObj = new Attachment();
		}
        return AttachmentObj;
    }
    
    public Attachment getConsentFormObj(){
        if( ConsentFormObj == null ){
            ConsentFormObj = new Attachment();
		}
        return ConsentFormObj;
    }
    
    
    
    public void SubmitForm(){
        if( AttachmentObj.body != null && ConsentFormObj.body != null){	
            
            insert LongTermVisaNominationObj;
            
            AttachmentObj.parentId = LongTermVisaNominationObj.Id;
        	insert AttachmentObj;
            
            ConsentFormObj.parentId = LongTermVisaNominationObj.Id;
        	insert ConsentFormObj;
            
            LongTermVisaNominationAfterSubmissionObj = [ SELECT Id,Name,Nominee_Name__c FROM Long_Term_Visa_Nomination__c WHERE Id=: LongTermVisaNominationObj.Id ];
            
            ShowSubmitMsg = true;
            
        }
    }
}