/*************************************************************************************************
 *  Name        : SavedRequestsController
 *  Author      : Ravi
 *  Company     : NSI JLT
 *  Date        : 2014-15-12     
 *  Purpose     : This class is to call the Courier webservice    
  ---------------------------------------------------------------------------------------------------------------------
   Modification History
  ---------------------------------------------------------------------------------------------------------------------
   V.No    Date        Updated By    Description
  --------------------------------------------------------------------------------------------------------------------- 
	V1.1    22-Sep-2015		Shabbir       Added logic for Marketing forms saved request
	V1.2	16-Dec-2015		Ravi		  Changed the title for BC Services from Property Service to Business Centre Service
	V1.3    17-Dec-2015		Shabbir       Added logic for Other saved request
	v1.4	22-Dec-2015		Diana		  Added CreatedBy.Name and SR_Template__r.Menu__c in the query
	v1.5	31-Jan-2015		Ravi		  Added the Listing Services access
	v1.5	01-Feb-2015		Ravi		  Added the Fitout Services access
	v1.6    17-May-2016		Claude		  Added some documents for saved drafts
	v1.7    06-June-2016	Swati		  Added logic to show fine issued
	V1.8    30-October-2016 Sravan		  Added the Logic to show Mortgage Requests
**************************************************************************************************/
public without sharing class SavedRequestsController {

    public class CustomSettingsException extends Exception{}
    
    public string accId{get;set;}
    public string contractorId{get;set;}
    public set<string> accessibleRecordTypesMortgage; // V1.8

    /*public List<Service_Request__c> SavedROCReq {get; set;} 
    public List<Service_Request__c> SavedGSReq {get; set;} 
    public List<Service_Request__c> PageSavedRequests {set; get;}
    public List<Service_Request__c> PageSavedRequestsGS {set; get;}
    
    //public List<Portal_Settings__c>columnsList {set; get;} //Not used
    
    // maintaining paging state
    private Integer pageSize;
    private Integer totalPageNumber;
    private Integer currentPageNumber;
    public string accessType{set;get;} 
    
    private Integer pageSizeGS;
    private Integer totalPageNumberGS;
    private Integer currentPageNumberGS;

    public string srId_toRemove{set; get;}
    */
    public SavedRequestsController() {
     
       string userId = userinfo.getUserId();
       for(User usr:[select id,contact.contractor__c,Account_Id__c,Community_User_Role__c from User where id=:userId]){
           accId = usr.Account_Id__c;
           if(usr.contact!=null && usr.contact.contractor__c!=null){
	        	contractorId = usr.contact.contractor__c;
	        }
           if(usr.Community_User_Role__c!=null && usr.Community_User_Role__c!=''){
	           /*if(usr.Community_User_Role__c.contains('Employee Services') && usr.Community_User_Role__c.contains('Company Services')){
	           		accessType='both';
	           }else if(usr.Community_User_Role__c.contains('Employee Services') && !(usr.Community_User_Role__c.contains('Company Services'))){
	           		accessType='GS';
	           }else if(usr.Community_User_Role__c.contains('Company Services') && !(usr.Community_User_Role__c.contains('Employee Services'))){
	           		accessType='ROC';
	           }*/
	           CommunityUserRoles = usr.Community_User_Role__c;
           }
           		
       }
        
        /*currentPageNumber = 0;
        totalPageNumber = 0;
        pageSize = 20; 
        
        currentPageNumberGS = 0;
        totalPageNumberGS = 0;
        pageSizeGS = 20; 
        
        BindSavedRequestsView (1);*/ 
        accessibleRecordTypesMortgage = SavedRequestsController.collectMortgageRecordTypes(CommunityUserRoles); // V1.8  
        LoadRequests();     
    }    

   /* // Pagination properites
    public Integer getCurrentPageNumber(){
        return currentPageNumber;
    }
    
    public Integer getPageSize(){
        return pageSize;
    }
    
    public Integer getTotalPageNumber(){
    
        if(totalPageNumber == 0 && SavedROCReq != null){
            totalPageNumber  = SavedROCReq.size() / pageSize;
            Integer calPageNumber = SavedROCReq.size() - (totalPageNumber * PageSize);
            if(calPageNumber > 0)
                totalPageNumber++;
        }
        return totalPageNumber;
    }
    
    public Integer getCurrentPageNumberGS(){
        return currentPageNumberGS;
    }
    
    public Integer getPageSizeGS(){
        return pageSizeGS;
    }
    
    public Integer getTotalPageNumberGS(){
    
        if(totalPageNumberGS == 0 && SavedGSReq != null){
            totalPageNumberGS  = SavedGSReq.size() / pageSizeGS;
            Integer calPageNumber = SavedGSReq.size() - (totalPageNumberGS * PageSizeGS);
            if(calPageNumber > 0)
                totalPageNumberGS++;
        }
        return totalPageNumberGS;
    }
    */
    /****************************************************/
        
    /**
     * Returns all pending steps based on user and account information.
     */        
    /*public PageReference SavedRequests() {
        string userId = userinfo.getUserId();
       
       SavedROCReq = [SELECT id, Name, SR_Menu_Text__c, External_SR_Status__c, CreatedDate, Portal_Service_Request_Name__c 
                        from Service_Request__c where Customer__c =:accId ORDER BY CreatedDate ASC]; 
        return null;
    }
    
    /**
     * Returns all pending steps based on user and account information.
             
    public PageReference SavedRequests() {
        string userId = userinfo.getUserId();
       
       SavedRequests = [SELECT id, Name, SR_Menu_Text__c, External_SR_Status__c, CreatedDate, Portal_Service_Request_Name__c 
                        from Service_Request__c where External_Status_Name__c='Draft' AND Customer__c =:accId ORDER BY CreatedDate ASC];                     
        return null;
    }
    */
  /*  @TestVisible private void loadSavedRequests(){
        try {
            string userId = userinfo.getUserId();
            User pUser = [select id,Community_User_Role__c from User where id=:userId];
            SavedGSReq = new list<Service_Request__c>();
            SavedROCReq = new list<Service_Request__c>();
            for(Service_Request__c SR :[SELECT id, Name, SR_Menu_Text__c, SR_Template__r.Menu__c,External_Status_Name__c, CreatedDate, Portal_Service_Request_Name__c 
                             from Service_Request__c where External_Status_Name__c='Draft' AND Record_Type_Name__c!='DM_SR' AND Customer__c =:accId AND Pre_GoLive__c != true ORDER BY CreatedDate ASC]){
                if(SR.SR_Template__r.Menu__c!=null && SR.SR_Template__r.Menu__c =='Employee Services'){
                	system.debug('SR.SR_Template__r.Menu__c   '+SR.SR_Template__r.Menu__c);
                	SavedGSReq.add(SR);     
                }else{
                	SavedROCReq.add(SR);  
                }
            }

        }catch(Exception ex){
            System.debug('Exception: Loading standard view. \nDetail' + ex.getMessage());            
        }        
    }    

    
    
    public PageReference BindSavedRequestsView(Integer newPageIndex) {
        
        try {

			loadSavedRequests();
            
            PageSavedRequests = new List<Service_Request__c>();
            PageSavedRequestsGS = new List<Service_Request__c>();
            
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            Transient Integer counterGS = 0;
            Transient Integer minGS = 0;
            Transient Integer maxGS = 0;
            
            if (newPageIndex > currentPageNumber){
                min = currentPageNumber * pageSize;
                max = newPageIndex * pageSize;
                minGS = currentPageNumber * pageSize;
                maxGS = newPageIndex * pageSize;
            }
            else{
                max = newPageIndex * pageSize;
                min = max - pageSize;
                maxGS = newPageIndex * pageSize;
                minGS = maxGS - pageSize;
            }            

            for(Service_Request__c a : SavedROCReq){
                counter++;
                if (counter > min && counter <= max)
                    PageSavedRequests.add(a);
            }
            for(Service_Request__c a : SavedGSReq){
                counter++;
                if (counter > min && counter <= max)
                    PageSavedRequestsGS.add(a);
            }
            currentPageNumber = newPageIndex;
            currentPageNumberGS = newPageIndex;
            
            if ((PageSavedRequestsGS == null || PageSavedRequestsGS.size() <= 0) && (PageSavedRequests == null || PageSavedRequests.size() <= 0))
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Data not available for this view.'));
			//if (PageSavedRequestsGS == null || PageSavedRequestsGS.size() <= 0 && (accessType=='both' || accessType=='GS'))
            //   ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Data not available for this view.'));

        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
        return null;
    }
    
    
   /* public PageReference deleteDraftRequest(){
        System.debug('Deleting SR Id:' + srId_toRemove);
        if(srId_toRemove.length() > 0){
	        delete [SELECT id FROM Service_Request__c WHERE id=:srId_toRemove];
        }
		
        return BindSavedRequestsView(currentPageNumber);
    }
    
    /*
    public boolean getNextButtonEnabled(){
        return (currentPageNumber > 0 && currentPageNumber < totalPageNumber);
    }
    
    public boolean getPreviousButtonEnabled(){
        return currentPageNumber > 1;
    }*/

   /* public PageReference nextBtnClick(){
        return BindSavedRequestsView(currentPageNumber + 1);
    } 
        
    public PageReference previousBtnClick(){
        return BindSavedRequestsView(currentPageNumber - 1);
    }    
    
    public PageReference firstBtnClick(){
        return BindSavedRequestsView(1);
    }
    public PageReference lastBtnClick(){
        return BindSavedRequestsView(totalPageNumber);
    }     
    
    public Boolean getPreviousButtonEnabled(){
        return !(currentPageNumber > 1);
    }
    public Boolean getNextButtonDisabled(){
        if (SavedROCReq == null) return true;
        else return ((currentPageNumber * pageSize) >= SavedROCReq.size());
    }  
    
    /*GS Saved Requests Listview Buttons */
    
   /* public PageReference nextBtnClickGS(){
        return BindSavedRequestsView(currentPageNumberGS + 1);
    } 
        
    public PageReference previousBtnClickGS(){
        return BindSavedRequestsView(currentPageNumberGS - 1);
    }    
    
    public PageReference firstBtnClickGS(){
        return BindSavedRequestsView(1);
    }
    public PageReference lastBtnClickGS(){
        return BindSavedRequestsView(totalPageNumberGS);
    }     
    
    public Boolean getPreviousButtonEnabledGS(){
        return !(currentPageNumberGS > 1);
    }
    public Boolean getNextButtonDisabledGS(){
        if (SavedGSReq == null) return true;
        else return ((currentPageNumberGS * pageSizeGS) >= SavedGSReq.size());
    }
 	*/
 	// Ravi modified
 	public list<SavedRequests> AllRequests {get;set;} 
 	public Integer iPagesize = 20;
 	public string CommunityUserRoles {get;set;}
 	public Integer RowIndex {get;set;}
 	
 	public void LoadRequests(){
 		AllRequests = new list<SavedRequests>();
 		SavedRequests objSavedRequests;
 		list<string> lstSRTypes = new list<string>{'ROC','GS','RORP','IT','BC' , 'Marketing' , 'Other','Listing','Fit-Out & Events'}; //v1.1 Added Marketing in type of request , // v1.3
 		map<string,list<Service_Request__c>> mapSRs = new map<string,list<Service_Request__c>>();
 		map<string,list<string>> mapSRIds = new map<string,list<string>>();
 		list<Service_Request__c> lstSRs;
 		list<string> lstSRIds;
 		
 		// Start V1.8
 		list<service_Request__c> lstServiceRequest = new list<service_request__c>();  
		string profileName = [select id,Name from profile where id=:UserInfo.getProfileID() limit 1].name;
		
		if(profileName =='DIFC Mortgage Community User')
		{
			lstServiceRequest = [SELECT id,type_of_request__c,Record_Type_Name__c,Name,SR_Group__c, SR_Menu_Text__c, SR_Template__r.Menu__c,SR_Template__r.Sub_menu__c,External_Status_Name__c, CreatedDate, Portal_Service_Request_Name__c,CreatedBy.Name
                        from Service_Request__c where External_Status_Name__c='Draft' AND Record_Type_Name__c in:accessibleRecordTypesMortgage AND Customer__c =:accId AND Pre_GoLive__c != true AND Record_Type_Name__c!='DM_SR' ORDER BY CreatedDate ASC];		
		}else{
			
			lstServiceRequest = [SELECT id,type_of_request__c,Record_Type_Name__c,Name,SR_Group__c, SR_Menu_Text__c, SR_Template__r.Menu__c,SR_Template__r.Sub_menu__c,External_Status_Name__c, CreatedDate, Portal_Service_Request_Name__c,CreatedBy.Name
                        from Service_Request__c where (External_Status_Name__c='Draft' OR (External_Status_Name__c='Issued' AND Record_Type_Name__c='Issue_Fine')) AND Record_Type_Name__c!='DM_SR' AND Customer__c =:accId AND Pre_GoLive__c != true ORDER BY CreatedDate ASC];
		}
		
 		//End of V1.8 
 		
         //Diana : Modified the query //V1.7
      /*  for(Service_Request__c objSR :[SELECT id,type_of_request__c,Record_Type_Name__c,Name,SR_Group__c, SR_Menu_Text__c, SR_Template__r.Menu__c,SR_Template__r.Sub_menu__c,External_Status_Name__c, CreatedDate, Portal_Service_Request_Name__c,CreatedBy.Name
                        from Service_Request__c where (External_Status_Name__c='Draft' OR (External_Status_Name__c='Issued' AND Record_Type_Name__c='Issue_Fine')) AND Record_Type_Name__c!='DM_SR' AND Customer__c =:accId AND Pre_GoLive__c != true ORDER BY CreatedDate ASC]){
       */ // Commented for V1.8 
          for(Service_request__c objSR:lstServiceRequest){ // For loop commented for V1.8  
        	//if(objSR.SR_Group__c == 'BC')
        		//objSR.SR_Group__c = 'RORP';
        	lstSRs = mapSRs.containsKey(objSR.SR_Group__c) ? mapSRs.get(objSR.SR_Group__c) : new list<Service_Request__c>();
        	if(lstSRs.size() < iPagesize)
        		lstSRs.add(objSR);
        	mapSRs.put(objSR.SR_Group__c,lstSRs);
        	
        	lstSRIds = mapSRIds.containsKey(objSR.SR_Group__c) ? mapSRIds.get(objSR.SR_Group__c) : new list<string>();
        	lstSRIds.add(objSR.Id);
        	mapSRIds.put(objSR.SR_Group__c,lstSRIds);
        }
        System.debug('SRs Map:' + mapSRIds);
        System.debug('SRs Types:' + lstSRTypes);
        System.debug('IS Empty:' + !mapSRIds.isEmpty());
        if(!mapSRIds.isEmpty()){
        	lstSRIds = new list<string>();
        	Integer iC = 1;
        	map<Integer,list<string>> mapTempIds;
        	for(string SRGroup : lstSRTypes){
        		Boolean bFlg = false;
        		if(CommunityUserRoles.contains('Company Services') && (SRGroup == 'ROC' || SRGroup == 'RORP' || SRGroup == 'BC' || SRGroup == 'Marketing' || SRGroup == 'Other')){ //v1.1 Added Marketing condition , //v1.2
        			bFlg = true;
        		}else if(CommunityUserRoles.contains('Company Services') == false && CommunityUserRoles.contains('Property Services') && SRGroup == 'RORP'){
        			bFlg = true;
        		}else if((SRGroup == 'GS' && CommunityUserRoles.contains('Employee Services')) || 
        			(SRGroup == 'IT' && CommunityUserRoles.contains('IT Services')) || (SRGroup == 'Marketing' && CommunityUserRoles.contains('Marketing Services')) ){ //v1.1 Added Marketing condition
        			bFlg = true;
        		}
        		//v1.3
        		else if(SRGroup == 'Other' && (CommunityUserRoles.contains('Company Services') || CommunityUserRoles.contains('Employee Services') || CommunityUserRoles.contains('Property Services'))){
        			bFlg = true;
        		}
        		//V1.4
        		else if(SRGroup == 'Listing' && CommunityUserRoles.contains('Listing Services')){
        			bFlg = true;
        		}
        		//V1.5
        		else if(SRGroup == 'Fit-Out & Events' && (CommunityUserRoles.contains('Fit-Out Services') || CommunityUserRoles.contains('Event Services') )){
        			bFlg = true;
        		}
        		//V1.8
        		else if(SRGroup == 'RORP' && ((CommunityUserRoles.contains('Mortgage Registration')  || (CommunityUserRoles.contains('Discharge of Mortgage') || CommunityUserRoles.contains('Variation of Mortgage'))))){
        			bFlg = true;
        		}
        		
        		/*else if((SRGroup == 'ROC' && CommunityUserRoles.contains('Company Services')) || (SRGroup == 'GS' && CommunityUserRoles.contains('Employee Services')) || 
        			(SRGroup == 'IT' && CommunityUserRoles.contains('IT Services')) || ((SRGroup == 'RORP' || SRGroup == 'BC' )&& CommunityUserRoles.contains('Property Services')) )
        			bFlg = true;
        		*/
        		if(bFlg == false && SRGroup == 'Fit-Out & Events' && CommunityUserRoles.contains('Company Services')){
        			if(mapSRs.containsKey(SRGroup)){
        				list<Service_Request__c> lstSRTemp = new list<Service_Request__c>();
        				for(Service_Request__c obj : mapSRs.get(SRGroup)){
        					if(obj.Record_Type_Name__c == 'Request_Contractor_Access' || 
        						obj.Record_Type_Name__c == 'Project_Cancellation_Request' || //V1.6 - Added Project Cancellation, Request for Contractor Access and Update Contact Details type
        						obj.Record_Type_Name__c == 'Update_Contact_Details'){
        						bFlg = true;
        						lstSRTemp.add(obj);
        						//break;
        					}
        				}
        				mapSRs.put(SRGroup,lstSRTemp);
        			}
        		}
        		System.debug('SR Group Roles: ' + CommunityUserRoles);
        		System.debug('SR Group: ' + SRGroup);
        		System.debug('SR Group Keys: ' + mapSRIds.keySet());
        		System.debug('bFlg: ' + bFlg);
        		if(mapSRIds.containsKey(SRGroup) && bFlg){
        			objSavedRequests = new SavedRequests();
        			objSavedRequests.SRType = SRGroup;
        			objSavedRequests.RequestIds = mapSRIds.get(SRGroup);
        			objSavedRequests.CurrentPageNo = 1;
        			objSavedRequests.Requests = mapSRs.get(SRGroup);
        			objSavedRequests.Index = AllRequests.size();
        			mapTempIds = new map<Integer,list<string>>();
        			for(string srId : objSavedRequests.RequestIds){
	        			if(lstSRIds.size() < iPagesize)
	        				lstSRIds.add(srId);
	        			else{
	        				mapTempIds.put(iC,lstSRIds);
	        				lstSRIds = new list<string>();
	        				iC++;
	        				lstSRIds.add(srId);
	        			}
        			}
        			if(!lstSRIds.isEmpty()){
        				mapTempIds.put(iC,lstSRIds);
        			}
        			objSavedRequests.mapPageSRIds = mapTempIds;
        			objSavedRequests.TotalPages =  mapTempIds.size();
        			//v1.1 Added Marketing condition below
        			//V1.2
        			//V1.3
        			objSavedRequests.RoleType = (SRGroup == 'ROC') ? 'Company Service' : (SRGroup == 'GS') ? 'Employee Service' : (SRGroup == 'RORP') ? 'Property Service' : (SRGroup == 'IT') ? 'IT Services' : (SRGroup == 'Marketing') ? 'Marketing Services' : (SRGroup == 'BC') ? 'Business Centre Service' : (SRGroup == 'Other') ? 'Other' : (SRGroup == 'Listing') ? 'Listing Service' : (SRGroup=='Fit-Out & Events') ? 'Fit-Out & Events Service' : ''; 
        			AllRequests.add(objSavedRequests);
        		}
        	}
        }
 	}
 	
 	public void getRequests(Integer PageNo){
 		if(RowIndex != null && AllRequests != null && RowIndex <= AllRequests.size()){
 			list<string> lstIds = AllRequests[RowIndex].mapPageSRIds.get(PageNo);
 			if(lstIds != null && !lstIds.isEmpty()){
 				AllRequests[RowIndex].Requests = new list<Service_Request__c>();
 				AllRequests[RowIndex].Requests = [SELECT id, Name,SR_Group__c, SR_Menu_Text__c, SR_Template__r.Menu__c,External_Status_Name__c, CreatedDate, Portal_Service_Request_Name__c,CreatedBy.Name,SR_Template__r.Sub_menu__c
                         from Service_Request__c where Id IN : lstIds AND Pre_GoLive__c != true ORDER BY CreatedDate ASC];
                AllRequests[RowIndex].CurrentPageNo = PageNo;
 			}
 		}
 	}
 	
 	public void getNextRecords(){
 		if(RowIndex != null && AllRequests != null && RowIndex <= AllRequests.size()){
 			getRequests((AllRequests[RowIndex].CurrentPageNo+1));
 		}
 	}
 	public void getPreviousRecords(){
 		if(RowIndex != null && AllRequests != null && RowIndex <= AllRequests.size()){
 			getRequests((AllRequests[RowIndex].CurrentPageNo-1));
 		}
 	}
 	public void getFirstRecords(){
 		if(RowIndex != null && AllRequests != null && RowIndex <= AllRequests.size()){
 			getRequests(1);
 		}
 	}
 	public void getLastRecords(){
 		if(RowIndex != null && AllRequests != null && RowIndex <= AllRequests.size()){
 			getRequests((AllRequests[RowIndex].TotalPages.intValue()));
 		}
 	}
 	
 	// V1.8
	public static set<string> collectMortgageRecordTypes(string communityUserRole)
	{
		set<string> accessibleRecordTypes = new set<string>();
	   if(communityUserRole.contains('Mortgage Registration'))
			accessibleRecordTypes.add('Mortgage_Registration');
		if(communityUserRole.contains('Discharge of Mortgage'))
			accessibleRecordTypes.add('Discharge_Mortgage');
		if(communityUserRole.contains('Variation of Mortgage'))
			accessibleRecordTypes.add('Mortgage_Variation');
	   
	   return accessibleRecordTypes;
	
	}
 	
	public class SavedRequests{
		public string SRType {get;set;}
		public string RoleType {get;set;}
		public Integer CurrentPageNo {get;set;}
		public decimal TotalPages {get;set;}
		public list<Service_Request__c> Requests {get;set;}
		public list<string> RequestIds {get;set;}
		public Integer Index {get;set;}
		public map<Integer,list<string>> mapPageSRIds {get;set;}
		public string typeOfRequest {get;set;}
	}
      
}