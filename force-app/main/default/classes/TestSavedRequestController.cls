@isTest(seeAllData=false)
private class TestSavedRequestController {
    static testMethod void TestSavedRequests(){
        
        Profile objProfile = [SELECT Id 
                               FROM profile
                               WHERE Name = 'DIFC Customer Community User Custom' 
                               LIMIT 1];
   
		  Account a = new Account(Name='Test Account Name' , ROC_Status__c='Active', Legal_Type_of_Entity__c='All;LLP;LTD;');
		  insert a;
		 
		  Contact c = new Contact(FirstName='test1' , LastName = 'Contact Last Name', AccountId = a.id);
		  insert c;
		  
		  list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        SR_Status__c objSRStatus;
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Closed';
        objSRStatus.Code__c = 'CLOSED';
        objSRStatus.Type__c = 'End';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Rejected';
        objSRStatus.Code__c = 'REJECTED';
        objSRStatus.Type__c = 'Rejected';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Awaiting Approval';
        objSRStatus.Code__c = 'AWAITING_APPROVAL';
        objSRStatus.Type__c = 'Start';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Approved';
        objSRStatus.Code__c = 'APPROVED';
        objSRStatus.Type__c = 'Intermediate';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Submitted';
        objSRStatus.Code__c = 'SUBMITTED';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Draft';
        objSRStatus.Code__c = 'DRAFT';
        lstSRStatus.add(objSRStatus);
        
        insert lstSRStatus;
		  
		User user = new User();
		user.ProfileID = objProfile.Id;
		user.EmailEncodingKey = 'ISO-8859-1';
		user.LanguageLocaleKey = 'en_US';
		user.TimeZoneSidKey = 'America/New_York';
		user.LocaleSidKey = 'en_US';
		user.FirstName = 'first';
		user.LastName = 'last';
		user.Username = 'test@appirio.com';   
		user.CommunityNickname = 'testUser123';
		user.Alias = 't1';
		user.Email = 'no@email.com';
		user.IsActive = true;
		user.ContactId = c.Id;
		user.Community_User_Role__c='Company Services';
		insert user;
        
        list<Service_Request__c> lstSRs = new list<Service_Request__c>();
        Service_Request__c objSR  = new Service_Request__c();
    	objSR.customer__c = a.Id;
    	objSR.Send_SMS_To_Mobile__c = '';
    	objSR.SR_Group__c = 'BC';
    	lstSRs.add(objSR);
    	
    	objSR  = new Service_Request__c();
    	objSR.customer__c = a.Id;
    	objSR.SR_Group__c = 'IT';
    	objSR.Send_SMS_To_Mobile__c = '';
    	lstSRs.add(objSR);
    	
    	insert lstSRs;
        system.runAs(user) {
        	
        	SavedRequestsController objSavedRequestsController = new SavedRequestsController();
        	objSavedRequestsController.RowIndex = 0;
        	objSavedRequestsController.getNextRecords();
        	objSavedRequestsController.RowIndex = 0;
        	objSavedRequestsController.getPreviousRecords();
        	objSavedRequestsController.RowIndex = 0;
        	objSavedRequestsController.getFirstRecords();
        	objSavedRequestsController.RowIndex = 0;
        	objSavedRequestsController.getLastRecords();
        }
    }
        /*
        SavedRequestsController savedReqController = new SavedRequestsController(); 

        savedReqController.getNextButtonDisabled();
        savedReqController.loadSavedRequests();

        savedReqController.BindSavedRequestsView(1); 
        savedReqController.BindSavedRequestsView(-1);   
        
		savedReqController.SavedRequests();
		//savedReqController.SavedRequests = [SELECT id, Name, SR_Menu_Text__c, External_Status_Name__c, CreatedDate, Portal_Service_Request_Name__c 
        //                     from Service_Request__c where External_Status_Name__c='Draft'];
        savedReqController.getCurrentPageNumber();
		savedReqController.getTotalPageNumber();
        
        savedReqController.srId_toRemove = 'a0012345';        
       // savedReqController.deleteDraftRequest();
        
        savedReqController.firstBtnClick();
        savedReqController.nextBtnClick();
        savedReqController.previousBtnClick();
        savedReqController.lastBtnClick();
        
        savedReqController.getNextButtonDisabled();
        savedReqController.getPreviousButtonEnabled();
        
        
        
         savedReqController.getCurrentPageNumberGS();
		savedReqController.getTotalPageNumberGS();
        
        savedReqController.srId_toRemove = 'a0012345';        
        //savedReqController.deleteDraftRequest();
        
        savedReqController.firstBtnClickGS();
        savedReqController.nextBtnClickGS();
        savedReqController.previousBtnClickGS();
        savedReqController.lastBtnClickGS();
        
        savedReqController.getNextButtonDisabledGS();
        savedReqController.getPreviousButtonEnabledGS();
    }*/
}