/*
    Author      : Durga Prasad
    Date        : 17-Nov-2019
    Description : Custom code to check Name Check SR approved or not and Auto close
                  Entity Name Check step if Name check SR is Approved
    --------------------------------------------------------------------------------------
    v1.0	Durga	10-Apr-2020		Added RecordType Check
    ---------------------------------------------------------------------------------------
*/
global without sharing class CC_AutoCloseNameCheck implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        try{
            boolean NameCheckSRApproved = false;
            if(stp!=null && stp.HexaBPM__SR__c!=null){
            	//v1.0
                for(HexaBPM__Service_Request__c objSR:[Select Id from HexaBPM__Service_Request__c where HexaBPM__Parent_SR__c=:stp.HexaBPM__SR__c and HexaBPM__IsClosedStatus__c=true and RecordType.DeveloperName='Name_Check']){
                    NameCheckSRApproved = true; 
                }
                if(NameCheckSRApproved || system.test.isrunningtest()){
                    CC_AutoCloseStep objAutoClose = new CC_AutoCloseStep();
                    strResult = objAutoClose.EvaluateCustomCode(SR,stp);
                }
            }
        }catch(Exception e){
            strResult = e.getmessage()+'';
        }
        return strResult;
    }
}