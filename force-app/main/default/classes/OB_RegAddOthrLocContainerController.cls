/**
*Author : Merul Shah
*Description : This is the data provider for Other Location module..
**/

public without sharing class OB_RegAddOthrLocContainerController
{
    
    @AuraEnabled   
    public static ResponseWrapper  getExistingAmendment(String reqWrapPram)
    {
         
            String declarationIndividualText = '';
            String declarationCorporateText = '';
            for(Declarations__mdt declare: [Select ID,Declaration_Text__c,Application_Type__c,Field_API_Name__c,Record_Type_API_Name__c,Object_API_Name__c 
                                             From Declarations__mdt 
                                                    Where (Record_Type_API_Name__c= 'Individual' OR Record_Type_API_Name__c= 'Body_Corporate')
                                                    AND Field_API_Name__c= 'I_Agree_Reg_Add__c'
                                                    AND Object_API_Name__c = 'HexaBPM_Amendment__c'])
             {
               
               if(String.IsNotBlank(declare.Record_Type_API_Name__c) && declare.Record_Type_API_Name__c.EqualsIgnorecase('Individual'))
               {
                 declarationIndividualText = declare.Declaration_Text__c;
               }
               else if(String.IsNotBlank(declare.Record_Type_API_Name__c) && declare.Record_Type_API_Name__c.EqualsIgnorecase('Body_Corporate'))
               {
                 declarationCorporateText = declare.Declaration_Text__c;
               }
               
            }
         
         //reqest wrpper
         OB_RegAddOthrLocContainerController.RequestWrapper reqWrap = (OB_RegAddOthrLocContainerController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_RegAddOthrLocContainerController.RequestWrapper.class);
         //response wrpper
         OB_RegAddOthrLocContainerController.ResponseWrapper respWrap = new OB_RegAddOthrLocContainerController.ResponseWrapper();
         
        
        
         String srId = reqWrap.srId;
         system.debug('##### srId  '+ srId );
         
         for(HexaBPM__Service_Request__c serObj : [SELECT id,
                                                          
                                                          Entity_Type__c,
                                                          Business_Sector__c,
                                                          Type_of_Entity__c,
                                                          Setting_Up__c,  
                                                          Records_and_registers_held_by__c,
                                                          Location_of_records_and_registers__c,
                                                          (
                                                          SELECT id,Role__c,
                                                                  Director_Employee_of_the_CSP__c,
                                                                  Certified_passport_copy__c,
                                                                  ServiceRequest__c,
                                                                  Title__c,
                                                                  First_Name__c,
                                                                  Middle_Name__c,
                                                                  Last_Name__c,
                                                                  Former_Name__c,
                                                                  Passport_No__c,
                                                                  Nationality_list__c,
                                                                  Date_of_Birth__c,
                                                                  Gender__c,
                                                                  Place_of_Birth__c,
                                                                  Passport_Issue_Date__c,
                                                                  Passport_Expiry_Date__c,
                                                                  Place_of_Issue__c,
                                                                  Are_you_a_resident_in_the_U_A_E__c,
                                                                  U_I_D_No__c,
                                                                  Mobile__c,
                                                                  Email__c,
                                                                  Is_this_individual_a_PEP__c,
                                                                  Address__c,
                                                                  Apartment_or_Villa_Number_c__c,
                                                                  Permanent_Native_Country__c,
                                                                  Permanent_Native_City__c,
                                                                  Emirate_State_Province__c,
                                                                  PO_Box__c, 
                                                                  RecordType.DeveloperName,
                                                                  ServiceRequest__r.Id,
                                                                  Is_this_Entity_registered_with_DIFC__c,
                                                                  Company_Name__c,
                                                                  Entity_Name__c,
                                                                  Registration_No__c,
                                                                  Date_of_Registration__c,
                                                                  Place_of_Registration__c,
                                                                  Country_of_Registration__c,
                                                                  Telephone_No__c,
                                                                  Entity_Name__r.Name,
                                                                  Individual_Corporate_Name__c,
                                                                  I_Agree_Reg_Add__c
                                                             FROM Amendments__r
                                                          ) 
                                                     FROM HexaBPM__Service_Request__c
                                                    WHERE id =:srId
                                                     
                                                  ] )
         {
             
             OB_RegAddOthrLocContainerController.SRWrapper srWrap = new OB_RegAddOthrLocContainerController.SRWrapper();
             srWrap.srObj = serObj;
             srWrap.amedWrapLst = new List<OB_RegAddOthrLocContainerController.AmendmentWrapper>();
             srWrap.amedShrHldrWrapLst = new List<OB_RegAddOthrLocContainerController.AmendmentWrapper>();
             srWrap.declarationIndividualText = declarationIndividualText;
             srWrap.declarationCorporateText = declarationCorporateText;
             
             for(HexaBPM_Amendment__c amed : serObj.Amendments__r)
             {
                  OB_RegAddOthrLocContainerController.AmendmentWrapper amedWrap = new OB_RegAddOthrLocContainerController.AmendmentWrapper();
                  amedWrap.amedObj = amed;
                  amedWrap.isIndividual = ( amed.RecordType.DeveloperName == 'Individual'  ? true : false);
                  amedWrap.isBodyCorporate =  ( amed.RecordType.DeveloperName == 'Body_Corporate' ? true : false );
                  amedWrap.lookupLabel = ( amed.Entity_Name__c != NULL ? amed.Entity_Name__r.Name : '');
                  
                  if( !(String.isBlank(amed.Role__c)) 
                     			&&  amed.Role__c.indexOf('Operating Location') != -1 
                    				&& ( amedWrap.isIndividual  ||  amedWrap.isBodyCorporate )
                    )
                  {
                      srWrap.amedWrapLst.add(amedWrap);
                  }
                  
                  
                  
             }
             
             //Always one SR so can be in loop.
             respWrap.srWrap = srWrap;
            
         }
         
       for(HexaBPM__Section_Detail__c sectionObj: [SELECT id,HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection' 
                                                    AND HexaBPM__Section__r.HexaBPM__Page__c = :reqWrap.pageId LIMIT 1]) {
         respWrap.ButtonSection =  sectionObj;                                            
                                                    }
         return respWrap;
         
    }
    /*
    @AuraEnabled   
    public static ResponseWrapper  getConvertToDirector(String reqWrapPram)
    {
        //reqest wrpper
         OB_RegAddOthrLocContainerController.RequestWrapper reqWrap = (OB_RegAddOthrLocContainerController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_RegAddOthrLocContainerController.RequestWrapper.class);
         //response wrpper
         OB_RegAddOthrLocContainerController.ResponseWrapper respWrap = new OB_RegAddOthrLocContainerController.ResponseWrapper();
         
         String amendmentID = reqWrap.amendmentID;
         
         HexaBPM_Amendment__c  amedTemp = new HexaBPM_Amendment__c(id = amendmentID );
         amedTemp.Is_Director__c = true;
         update amedTemp;
         
         return respWrap;
        
    }
    */
    
    @AuraEnabled 
public HexaBPM__Section_Detail__c ButtonSection { get; set; }

@AuraEnabled
    public static ButtonResponseWrapper getButtonAction(string SRID,string ButtonId, string pageId){
        
        
        ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
        HexaBPM__Service_Request__c objRequest = new HexaBPM__Service_Request__c(Id=SRID);
        PageFlowControllerHelper.objSR = objRequest;
        PageFlowControllerHelper objPB = new PageFlowControllerHelper();
        
        objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, SRID, pageId);
        upsert objRequest;
        
        PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
        system.debug('@@@@@@@@2 responseNextPage '+responseNextPage);
        respWrap.pageActionName = responseNextPage.pg;
        respWrap.communityName = responseNextPage.communityName;
        respWrap.CommunityPageName = responseNextPage.CommunityPageName;
        respWrap.sitePageName = responseNextPage.sitePageName;
        respWrap.strBaseUrl = responseNextPage.strBaseUrl;
        respWrap.srId = objRequest.Id;
        respWrap.flowId = responseNextPage.flowId;
        respWrap.pageId = responseNextPage.pageId;
        respWrap.isPublicSite = responseNextPage.isPublicSite;
        
        system.debug('@@@@@@@@2 respWrap.pageActionName '+respWrap.pageActionName);
        return respWrap;
    }
    public class ButtonResponseWrapper{
        @AuraEnabled public String pageActionName{get;set;}
        @AuraEnabled public string communityName{get;set;}
        @AuraEnabled public String errorMessage{get;set;}
        @AuraEnabled public string CommunityPageName{get;set;}
        @AuraEnabled public string sitePageName{get;set;}
        @AuraEnabled public string strBaseUrl{get;set;}
        @AuraEnabled public string srId{get;set;}
        @AuraEnabled public string flowId{get;set;}
        @AuraEnabled public string  pageId{get;set;}
        @AuraEnabled public boolean isPublicSite{get;set;}
        
    }
    
    
    public class SRWrapper 
    {
        @AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }
        @AuraEnabled public List<OB_RegAddOthrLocContainerController.AmendmentWrapper> amedWrapLst { get; set; }
        @AuraEnabled public List<OB_RegAddOthrLocContainerController.AmendmentWrapper> amedShrHldrWrapLst { get; set; }
        @AuraEnabled public String declarationIndividualText { get; set; }
        @AuraEnabled public String declarationCorporateText { get; set; }
        
                
        public SRWrapper()
        {}
    }
    
    public class AmendmentWrapper 
    {
        @AuraEnabled public HexaBPM_Amendment__c amedObj { get; set; }
        @AuraEnabled public Boolean isIndividual { get; set; }
        @AuraEnabled public Boolean isBodyCorporate { get; set; }
        @AuraEnabled public String lookupLabel { get; set; }
        
        
        
        public AmendmentWrapper()
        {}
    }
    
    
    public class RequestWrapper 
    {
        @AuraEnabled public Id flowId { get; set; }
        @AuraEnabled public Id pageId {get;set;}
        @AuraEnabled public Id srId { get; set; }
        @AuraEnabled public AmendmentWrapper amedWrap{get;set;}
        @AuraEnabled public String amendmentID{get;set;}
        
       
      
        public RequestWrapper()
        {}
    }

    public class ResponseWrapper 
    {
        @AuraEnabled public SRWrapper  srWrap{get;set;}
        
        @AuraEnabled 
        public HexaBPM__Section_Detail__c ButtonSection { get; set; }
        
        public ResponseWrapper()
        {}
    }

}