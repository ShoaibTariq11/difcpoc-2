public with sharing class StepProcessBuilderProcesses {
    @InvocableMethod(label='stepProcessBuilderInvocation' description='stepProcessBuilderInvocation') 
    public static void stepProcessBuilderStepName(List<id> stepIds){
		for(Step__c stepRec : [Select id,step_name__c from Step__c where id in :stepIds]){
			if(stepRec.step_Name__c =='Medical Fitness Test Scheduled')SendMedicalServiceToSAP(stepRec.id);
		}
    }
    public static string SendMedicalServiceToSAP(Id objStepId)
    {
         //GsMedicalTestScheduledHandler.MedicalTestScheduled(objStepId);
         return 'Success';
    }
}