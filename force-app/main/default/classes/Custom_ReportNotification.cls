public class Custom_ReportNotification implements Reports.NotificationAction{

    public void execute(Reports.NotificationActionContext context){

        //Reports.ReportInstance c = context.getReportInstance();
        Reports.ReportResults results = context.getReportInstance().getReportResults();
        
        ID reportID = results.getReportMetadata().getID();
       
        string url = Label.Salesforce_URL+reportID ;
        String name = results.getReportMetadata().getName();
        Reports.ReportFactWithDetails factDetails  = (Reports.ReportFactWithDetails)results.getFactMap().get('0!T');
    //   Reports.ReportFactWithSummaries  factSummaries   = (Reports.ReportFactWithSummaries )results.getFactMap().get('!T');
        integer NumberOfRows = 0;
        if(!test.isRunningTest()){
          
                
                if(factDetails  != null)
               NumberOfRows = factDetails.getRows().size();
            else
                NumberOfRows = -1;
        }
           
            
        ApexPages.PageReference objPage = new ApexPages.PageReference('/'+reportID +'?csv=1');


        Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
        attachment.setFileName(name+'.csv');
        
        if(test.isRunningTest())
            attachment.setBody(Blob.valueOf('UNIT.csv'));
        else
            attachment.setBody(objPage.getContent());
            
        attachment.setContentType('text/csv');
        
       
    
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
       CustomEmailNotificaition__c objCustomNotification;
        
        if(test.isRunningTest())
            objCustomNotification = CustomEmailNotificaition__c.getValues('test'); 
          else
           objCustomNotification = CustomEmailNotificaition__c.getValues(name); 
           
           if(objCustomNotification == null) {
               system.debug('$$$$ No Custom setting found');
               return;
           }
           
       message.setSubject(objCustomNotification.subject__c);
      // string showRowCount = (NumberOfRows  > -1) ? 'There are total '+NumberOfRows +' active notifications' :'';
       message.setHTMLBody(objCustomNotification.body__c);
    
      message.setToAddresses( objCustomNotification.email_addresses__c.split(',') );
      OrgWideEmailAddress owa =  [select id, Address, DisplayName from OrgWideEmailAddress where Address = 'portal@difc.ae' limit 1];
      message.setOrgWideEmailAddressId(owa.Id);
    
        message.setFileAttachments(new Messaging.EmailFileAttachment[] { attachment } );
        Messaging.sendEmail( new Messaging.SingleEmailMessage[] { message } );


    }
}