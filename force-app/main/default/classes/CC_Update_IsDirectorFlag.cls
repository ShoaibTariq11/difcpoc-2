/**************************************************************************************************
* Name               : CC_Update_IsDirectorFlag                                                              
* Description        : CC Code to update Director and authorized Signatory Flag.                                        
* Created Date       : 18th March 2020                                                                *
* Created By         : Leeba Shibu (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version    Author    Date           Comment                                                                *
  
**************************************************************************************************/

global without sharing class CC_Update_IsDirectorFlag implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
         List<HexaBPM_Amendment__c> lstAppAmmendments = new List<HexaBPM_Amendment__c>();
         MAP<Id,HexaBPM_Amendment__c> ammMap = new MAP<Id,HexaBPM_Amendment__c>();
         try{
             if(stp!=null && stp.HexaBPM__SR__c!=null)
             {
               // where  ServiceRequest__c =:stp.HexaBPM__SR__c and Roles__c includes('Director')
            
				 
               for(HexaBPM_Amendment__c AN:[select Id,Is_Authorized_Signatory_Flag__c,Is_Director_Flag__c,Roles__c from HexaBPM_Amendment__c where  ServiceRequest__c =:stp.HexaBPM__SR__c and Roles__c includes('Director')])
               {
                   
                    AN.Is_Director_Flag__c = true;                
                	lstAppAmmendments.add(AN);               
               }  
             
                for(HexaBPM_Amendment__c AN:[select Id,Is_Director_Flag__c,Is_Authorized_Signatory_Flag__c,Roles__c from HexaBPM_Amendment__c where ServiceRequest__c =:stp.HexaBPM__SR__c and Roles__c includes('Authorised Signatory')])
               {
                
                	AN.Is_Authorized_Signatory_Flag__c = true;
                	lstAppAmmendments.add(AN);
               }  
                 
                ammMap.putall(lstAppAmmendments);
                if(ammMap.size()>0){
                    update ammMap.values();
                 }
            }
        }catch(Exception e){
            strResult = e.getMessage()+'';
        
        }
        
        return strResult;
    }
    
}