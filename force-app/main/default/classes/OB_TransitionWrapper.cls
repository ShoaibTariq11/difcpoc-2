public without sharing class OB_TransitionWrapper {
    @AuraEnabled
    public HexaBPM__Status__c objStatus{get;set;}
    @AuraEnabled
    public HexaBPM__Step_Transition__c objSRStepTrans{get;set;}
}