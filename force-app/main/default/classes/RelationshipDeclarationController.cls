/*
    Created By  : Shabbir - DIFC on 29 July,2018
    Description : Custom class to use in declaration generation
--------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By      Description
----------------------------------------------------------------------------------------              
 ----------------------------------------------------------------------------------------
*/

public class RelationshipDeclarationController {
    
 private final Amendment__c objAmendment;
 public string typeOfCompany {get; set;} 
 public string legalStructureType {get; set;}
 public string lawType {get; set;} 
 public string agreementType {get; set;} 
 public string point1a {get; set;}
 public string point3a {get; set;}
 public string bcType_point4a {get; set;}  
 public string bcType_point4b {get; set;}
 public string bcType_point4c {get; set;} 
 public string point5a {get; set;}
 public string point6a {get; set;}
 public Boolean showMore2Points {get; set;}
 
 
 public RelationshipDeclarationController(ApexPages.StandardController stdController){
      this.objAmendment = (Amendment__c)stdController.getRecord(); 
      typeOfCompany = '';
      legalStructureType = '';
      lawType = '';
      agreementType = '';
      point1a = '';
      bcType_point4a = '';
      bcType_point4b = '';
      bcType_point4c = '';
      point5a = '';
      point6a = '';
      point3a = '';
      showMore2Points = false;
      checkCompanyType();
      checkTotalPoints();
          
 }
 
 public void checkTotalPoints(){
    if(objAmendment.Relationship_Type__c == 'Shareholder' || objAmendment.Relationship_Type__c == 'Member' || objAmendment.Relationship_Type__c == 'Designated Member' || objAmendment.Relationship_Type__c == 'General Partner' || objAmendment.Relationship_Type__c == 'Limited Partner' || objAmendment.Relationship_Type__c == 'Founding Member' || objAmendment.Relationship_Type__c == 'Services Personnel') {
           showMore2Points = true;
       }
     
 }
 public void checkCompanyType(){
     
     // All LTD legal structure and LLC
     if(objAmendment.ServiceRequest__r.Legal_Structures__c == 'LTD' || objAmendment.ServiceRequest__r.Legal_Structures__c == 'LTD SPC' || 
        objAmendment.ServiceRequest__r.Legal_Structures__c == 'LTD PCC' || objAmendment.ServiceRequest__r.Legal_Structures__c == 'LTD IC' ||
        objAmendment.ServiceRequest__r.Legal_Structures__c == 'LTD National' || objAmendment.ServiceRequest__r.Legal_Structures__c == 'LTD Supra National' || objAmendment.ServiceRequest__r.Legal_Structures__c == 'LLC'){
        typeOfCompany = 'Company';
        legalStructureType = 'Company'; 
        lawType = 'Companies';
        agreementType = 'Articles of Association';
        point3a = 'incorporation';
        bcType_point4a = 'In the event an incorporator is a body corporate, '; 
        bcType_point4b = 'of the incorporator';
        bcType_point4c = 'I also declare that the Articles of Association will only be signed by the incorporators or their authorized individuals.';
        point5a = 'Company';
        point6a = 'Article 158';
     }
     
     // NPIO legal structure
     if(objAmendment.ServiceRequest__r.Legal_Structures__c == 'NPIO'){
        typeOfCompany = 'Non Profit Incorporated Organization';
        legalStructureType = 'Non Profit Incorporated Organization (NPIO)'; 
        lawType = 'Non Profit Incorporated Organization';
        agreementType = 'Charter of the Organization';
        point3a = 'incorporation';
        bcType_point4a = 'In the event a Founding Member is a body corporate, ';
        bcType_point4b = 'of the Founding Member';
        bcType_point4c = 'I also declare that the Charter of the Organization will only be signed by the Founding Members or their authorized individuals.';
        point5a = 'NPIO';
        point6a = '';
     }   

     // LLP legal structure
     if(objAmendment.ServiceRequest__r.Legal_Structures__c == 'LLP'){
        typeOfCompany = 'Limited Liability Partnership';
        legalStructureType = 'Limited Liability Partnership (LLP)'; 
        lawType = 'Limited Liability Partnership';
        agreementType = 'Limited Liability Partnership Agreement';
        point3a = 'incorporation';
        bcType_point4a = 'In the event a member of LLP is a body corporate, ';
        bcType_point4b = 'incorporator';
        bcType_point4c = 'I also declare that the Limited Liability Partnership Agreement will only be signed by the authorized individuals.';
        point5a = 'LLP';
        point6a = 'Article 51';
     }

     // RLLP legal structure
     if(objAmendment.ServiceRequest__r.Legal_Structures__c == 'RLLP'){
        legalStructureType = 'Recognized Limited Liability Partnership (RLLP)';
        lawType = 'Limited Liability Partnership';
        typeOfCompany = 'Foreign Limited Liability Partnership';            
        agreementType = 'Limited Liability Partnership Agreement';
        point3a = 'registration';
        bcType_point4a = '';
        bcType_point4b = '';
        bcType_point4c = 'I also declare that this form will only be signed by the authorized person(s).';
        point5a = 'Foreign Limited Liability Partnership';
        point6a = 'Article 51';
     }       

     // GP legal structure
     if(objAmendment.ServiceRequest__r.Legal_Structures__c == 'GP'){
        legalStructureType = 'General Partnership (GP)';
        lawType = 'General Partnership';
        typeOfCompany = 'General Partnership';          
        agreementType = '';
        point1a = 'Partner';
        point3a = 'registration';
        bcType_point4a = 'In the event a partner of GP is a body corporate, ';
        bcType_point4b = '';
        bcType_point4c = '';
        point5a = 'Partnership';
        point6a = 'Article 61';
     }  

     // LP legal structure
     if(objAmendment.ServiceRequest__r.Legal_Structures__c == 'LP'){
        legalStructureType = 'Limited Partnership (LP)';
        lawType = 'Limited Partnership';
        typeOfCompany = 'Limited Partnership';          
        agreementType = '';
        point3a = 'registration';
        bcType_point4a = 'In the event a partner of LP is a body corporate, ';
        bcType_point4b = '';
        bcType_point4c = 'I also declare that the Partnership Agreement will only be signed by the all partner or their authorized individuals';
        point5a = 'Partnership';
        point6a = 'Article 52';
     }

     // RP legal structure
     if(objAmendment.ServiceRequest__r.Legal_Structures__c == 'RP'){
        legalStructureType = 'Recognised Partnership (RP)';
        lawType = 'Limited Partnership';
        typeOfCompany = 'Recognised Partnership';           
        agreementType = '';
        point3a = 'registration';
        bcType_point4a = '';
        bcType_point4b = 'of the Recognised Partnership';
        bcType_point4c = '';
        point5a = 'Recognised Partnership';
        point6a = 'Article 61';
     }   

     // RLP legal structure
     if(objAmendment.ServiceRequest__r.Legal_Structures__c == 'RLP'){
        legalStructureType = 'Recognised Limited Partnership (RLP)';
        lawType = 'Limited Partnership';
        typeOfCompany = 'Recognised Limited Partnership';           
        agreementType = '';
        point3a = 'registration';
        bcType_point4a = '';
        bcType_point4b = 'Recognised Limited Partnership';
        bcType_point4c = '';
        point5a = 'Partnership';
        point6a = 'Article 52';
     }   
     
     // FRC legal structure
     if(objAmendment.ServiceRequest__r.Legal_Structures__c == 'FRC'){
        legalStructureType = 'Recognised Company';
        lawType = 'Companies';
        typeOfCompany = 'Foreign Company';          
        agreementType = '';
        point3a = 'registration';
        bcType_point4a = '';
        bcType_point4b = 'of the incorporator';
        bcType_point4c = '';
        point5a = 'Foreign Company';
        point6a = 'Article 158';
     }   
     
 }
 
 

}