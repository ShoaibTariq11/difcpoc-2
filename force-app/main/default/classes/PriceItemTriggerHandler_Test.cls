/*
    Author      :   Shoaib Tariq
    Description :   
    --------------------------------------------------------------------------------------------------------------------------
  Modification History
   --------------------------------------------------------------------------------------------------------------------------
  V.No  Date    Updated By      Description
  --------------------------------------------------------------------------------------------------------------------------             
   V1.0    17-02-2020  shoaib      Created
*/
@isTest
public class PriceItemTriggerHandler_Test {
    
    static testMethod void triggerTestCase(){
        
        Service_Request__c objSR2 = new Service_Request__c();
        objSR2.Submitted_Date__c = Date.today();
        objSR2.SR_Group__c      = 'Fit-Out & Events';
        objSR2.Pre_GoLive__c    = false; 
        objSR2.SAP_Unique_No__c = '33232432432444'; 
        objSR2.RecordTypeId     = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('Miscellaneous_Services').getRecordTypeId();
        insert objSR2;
        
        SR_Price_Item__c objSRItem = new SR_Price_Item__c();
        objSRItem.ServiceRequest__c = objSR2.Id;
        objSRItem.Status__c = 'Invoiced';
        objSRItem.Price__c = 1000;
        insert objSRItem;
        
        Test.startTest();
          objSRItem.Transaction_Number__c = '123456789012345';
          Update objSRItem;
        Test.stopTest();
        
     
        
    }
}