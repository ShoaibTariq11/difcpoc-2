/*
    Author      :   Sai
    Description :   This class is create test data for all objects
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.1    30-04-2019   Sai             

*/
@isTest
public class TestUtilityClass {

    public static List<Service_Request__c> createServiceRequestData(Integer recordCount,Boolean isInsert){
    	
    	List<Service_Request__c> lstSRequest = new List<Service_Request__c>();
    	Service_Request__c sRequest;
    	
    	for(Integer i=0;i<recordCount;i++){
    		sRequest = new Service_Request__c();
    		sRequest.Date_of_Declaration__c = Date.today();
        	sRequest.Email__c = 'test@test.com';
        	sRequest.Service_Category__c = 'New';
        	sRequest.Type_of_Lease__c = 'Lease';
        	sRequest.Rental_Amount__c = 1234;
        	sRequest.Legal_Structures__c = 'FRC';
    		lstSRequest.add(sRequest);
    	}
    	
    	if(lstSRequest.size()>0 && isInsert){
    		database.insert(lstSRequest);
    	}
    	return lstSRequest;
    }
    
    public static List<Amendment__c> amendmentDataInsert(Integer recordCount,Boolean isInsert,Id srRecordID){
    	
    	List<Amendment__c> lstAmendments = new List<Amendment__c>();
    	
    	Amendment__c amendments;
    	for(Integer i=0;i<recordCount;i++){
    		
    		amendments = new Amendment__c();
    		amendments.ServiceRequest__c = srRecordID;
    		amendments.Apt_or_Villa_No__c = 'TEST 1';
			amendments.Building_Name__c = 'TEST 1'; 
			amendments.Date_of_Birth__c = Date.today().AddYears(-19); 
			amendments.Emirate_State__c = 'TEST 1'; 
			amendments.Family_Name__c = 'TEST 1'; 
			amendments.Gender__c = 'Male'; 
			amendments.Given_Name__c = 'TEST 1'; 
			amendments.Job_Title__c = 'TEST 1'; 
			amendments.Middle_Name__c = 'TEST 1'; 
			amendments.Nationality_list__c = 'TEST 1'; 
			amendments.Passport_Expiry_Date__c = Date.today().AddYears(10); 
			amendments.Passport_No__c = 'TEST 1'; 
			amendments.Permanent_Native_City__c = 'TEST 1'; 
			amendments.Permanent_Native_Country__c = 'TEST 1'; 
			amendments.Person_Email__c = 'TEST@TEST.COM'; 
			amendments.Place_of_Birth__c = 'TEST 1'; 
			amendments.PO_Box__c = 'TEST 1'; 
			amendments.Post_Code__c = 'TEST 1'; 
			amendments.Status__c = 'TEST 1'; 
			amendments.Street__c = 'TEST 1'; 
			amendments.Title_new__c = 'Mr'; 
			amendments.Amendment_Type__c = 'Individual';
			amendments.Relationship_Type__c = 'General Partner';
			amendments.Gender__c = 'Male';
			amendments.Mobile__c = '+9712233445566';
			//amendments.Ownership_Percentage__c = 100;
			
        	lstAmendments.add(amendments);
    	  }
    	
    	if(lstAmendments.size()>0 && isInsert){
    		database.insert(lstAmendments);
    	}
    	return lstAmendments;
    } 
    
    public static List<Relationship__c> relationShipData(Integer rcordCount,Boolean isInsert){
    	
    	List<Relationship__c> lstRelationship = new List<Relationship__c>();
    	Relationship__c relations;
    	
    	for(integer i=0;i<rcordCount;i++){
    		relations = new Relationship__c();
    		relations.Active__c =  true;
    		lstRelationship.add(relations);
    	}
    	
    	if(lstRelationship.size() >0 && isInsert){
    		database.insert(lstRelationship);
    	}
    	return lstRelationship;
    }
    
    public Static list<Account> accountDataInsert(Integer recordCount,Boolean isInsert){
    	
    	List<Account> lstAccount = new List<Account>();
    	Account accountRecord;
    	
    	for(Integer i=0;i<recordCount;i++){
    		accountRecord = new Account();
    		accountRecord.Name = 'Test Account';
	        accountRecord.E_mail__c = 'test@test.com';
	        accountRecord.BP_No__c = '001234';
	        accountRecord.Company_Type__c = 'Financial - related';
	        accountRecord.Sector_Classification__c = 'Authorised Market Institution';
	        accountRecord.Legal_Type_of_Entity__c = 'LTD';
	        accountRecord.ROC_Status__c = 'Under Formation';
	        accountRecord.Financial_Year_End__c = '31st December';
    		lstAccount.add(accountRecord);
    	}
    	if(lstAccount.size()>0 && isInsert){
    		database.insert(lstAccount);
    	}
    	return lstAccount;
    }
     public Static list<Contact> contactDataInsert(Integer recordCount,Boolean isInsert,Id AccountID){
    	
    	List<Contact> lstContact = new List<Contact>();
    	Contact contactRecord;
    	
    	for(Integer i=0;i<recordCount;i++){
    		contactRecord = new Contact();
    		contactRecord.LastName = 'Test Contact';
    		contactRecord.AccountID = AccountID;
    		contactRecord.Portal_Username__c = 'test@testDIFC.com';
    		contactRecord.Previous_Nationality__c  = 'India';
    		contactRecord.Gender__c = 'Male';
    		contactRecord.MobilePhone = '+9719988776655';
    		contactRecord.UID_Number__c = '33223333';
    		
    		lstContact.add(contactRecord); 
    	}
    	if(lstContact.size()>0 && isInsert){
    		database.insert(lstContact);
    	}
    	return lstContact;
    }
    
    public static User insertPortalUser(String ContactID){
    	
    	Profile p = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom']; 
        List<Account> lstAccount = new List<Account>();
        List<Contact> lstContact = new List<Contact>();
        
        User u = new User(Alias = 'DIFC', Email='DIFCPortaluser@DIFCTest.com', 
            EmailEncodingKey='UTF-8', LastName='DIFC Portal User ', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id,ContactID=ContactID,
            TimeZoneSidKey='America/Los_Angeles', UserName='DIFCPortaluser@DIFCTest.comUAT');
            return u;
    }
}