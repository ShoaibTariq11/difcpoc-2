/*
	Author		:	Durga Prasad
	Description	:	Custom Code which will validate whether user uploads the document which is configured
					to upload in an particular step. 	
					
Modification History :
V.No	Name	Date		Description
1.1		Ravi	13/072016	Added the SR condition in the query to check for pendinf SR Doc	

*/
public without sharing class CC_Validate_ReqDocument {
	public static string Document_Upload_Check(Step__c stp){
		string strResult = null;
		try{
			strResult = 'Success';
			if(stp.SR_Step__c!=null){
				string strDocuments = '';
				//v1.1 - added the SR condition in the query
				for(SR_Doc__c srdoc:[select id,Doc_ID__c,SR_Template_Doc__c,Document_Name__c from SR_Doc__c where SR_Template_Doc__r.Validate_In_Code_at_Step_No__c=:stp.SR_Step__c and Doc_ID__c=null AND Service_Request__c =:stp.SR__c]){
					if(strDocuments=='')
						strDocuments = SRDOC.Document_Name__c;
					else
						strDocuments = strDocuments +' , '+SRDOC.Document_Name__c;
				}
				if(strDocuments!=null && strDocuments!=''){
					return 'Please upload "'+strDocuments+'" document(s) by clicking Download/Upload Doc from Service Request in order to proceed.';
				}
			}
			system.debug('---strResult----'+strResult);
			return strResult;
		}catch(Exception e){
             strResult = string.valueOf(e.getMessage());
             system.debug('----line--'+e.getLineNumber());
        } 
        return strResult;
	}
}