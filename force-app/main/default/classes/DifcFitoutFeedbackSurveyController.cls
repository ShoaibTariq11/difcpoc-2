public class DifcFitoutFeedbackSurveyController {
	@AuraEnabled 
	public static Step__c getStepDetails(String spepIdValue)
    {  
        Step__c stepRec =[Select id,name,SR__c,SR__r.Name,Step_name__c,Client_Name__c,SR_Status__c,SR_Menu_Text__c,SR__r.Contractor__r.Name from step__c where id=:spepIdValue];
        return stepRec;
    }
    @AuraEnabled 
    public static List<Feedback_Survey__c> getRelatedQuestions(String stepIdVal)
    {
        List<Survey_Question__c> serveyQuestions = [SELECT Id,Question__c, Name from Survey_Question__c where active__c =true ORDER BY Name];
        List<Feedback_Survey__c> feedbackSurveyQuestions = new List<Feedback_Survey__c>();
        for(Survey_Question__c serveyQuestion: serveyQuestions){
            Feedback_Survey__c feedbackServey = new Feedback_Survey__c();
            feedbackServey.Question__c = serveyQuestion.Question__c;
            feedbackServey.Extra_Feedback__c = '';
            feedbackServey.Rating__c = 0;
            feedbackServey.Step__c = stepIdVal;
            feedbackServey.Survey_Question_ID__c = serveyQuestion.id; 
            system.debug('feedbackServey--'+feedbackServey.Question__c);
            feedbackSurveyQuestions.add(feedbackServey);
        }
        system.debug('feedbackSurveyQuestions--'+feedbackSurveyQuestions.size());
        return feedbackSurveyQuestions;
    }
    
    @AuraEnabled 
    public static void insertFitoutFeedback(List<Feedback_Survey__c> feedbackSurveys)
    {
        system.debug('feedbackSurveyQuestions--'+feedbackSurveys);
        insert feedbackSurveys;
        updateStep(feedbackSurveys[0].step__c);
    } 
    
    @AuraEnabled 
    public static ServeyFeedbackWrapper getServeyFeedbackWrapperRecord(String spepIdValue){
        system.debug('Step Id is ---'+spepIdValue);
        ServeyFeedbackWrapper servFeedWrapper = new ServeyFeedbackWrapper();
        servFeedWrapper.stepRecordValue = getStepDetails(spepIdValue);
        servFeedWrapper.feedbackSurveyList= getRelatedQuestions(spepIdValue);
        system.debug('servFeedWrapper---'+servFeedWrapper.feedbackSurveyList[0]);
        return servFeedWrapper;
    }
    public class ServeyFeedbackWrapper{
        @AuraEnabled 
        public Step__c stepRecordValue{get;set;}
        @AuraEnabled 
        public List<Feedback_Survey__c> feedbackSurveyList{get;set;}
    }
    public static void updateStep(String stepId)
    {
        Step__c step = new Step__c(id=stepId, Status__c='a1M3N000000Bb8v');
        update step;
    } 
}