@isTest
public class WayFindingJSONClassTest {
    
    public static List<Account> lstAccount;
    public static List<Account> lstAccount1;
    public static List<Operating_Location__c> lstoLocation;
    public static Company_Business_Hours__c businessHours;
    public static Company_Business_Hours__c businessHours1;

    public static void testData(Integer accountCount){
        
        lstAccount = new List<Account>();
        lstoLocation = new List<Operating_Location__c>();
        
        Operating_Location__c location;
         
        Account eachAccount;
        
        for(Integer i=0;i<accountCount;i++){
            eachAccount = new Account();
            eachAccount.Name = 'Test'+i;
            eachAccount.Trade_Name__c = 'Test Trade';
            eachAccount.RORP_License_No__c = '8877779';
            eachAccount.Registered_Address__c = 'Dubai';
            eachAccount.Building_Name__c = 'DIFC';
            eachAccount.Hide_the_Company__c = false;
            eachAccount.ROC_Status__c = 'Active';
            eachAccount.Registration_License_No__c = '23329';
            eachAccount.Company_Type__c ='Retail';
            eachAccount.Company_Type__c ='Retail';
            eachAccount.Description ='test description';
            eachAccount.Sector_Classification__c ='Business Services';
            eachAccount.Street__c = 'test';
            eachAccount.City__c  ='test City';
            eachAccount.Country__c = 'United Arab Emirates';
            eachAccount.Postal_Code__c = '1234';
            eachAccount.Phone = '+971512345678';
            eachAccount.Website = 'www.assdd.com';
            eachAccount.Company_Type__c ='Retail';
            eachAccount.Sub_Legal_Type_of_Entity__c=null;
                   
            lstAccount.add(eachAccount);
        }
        
        Account eachAccount2 =  new Account();
        eachAccount2 = new Account();
        eachAccount2.Name = 'Test1222';
        eachAccount2.Trade_Name__c = 'Test Trade';
        eachAccount2.RORP_License_No__c = '8877709';
        eachAccount2.Registered_Address__c = 'Dubai';
        eachAccount2.Building_Name__c = 'DIFC';
        eachAccount2.Hide_the_Company__c = false;
        eachAccount2.ROC_Status__c = 'Active';
        eachAccount2.Registration_License_No__c = '23349';
        eachAccount2.Company_Type__c ='Retail';
        eachAccount2.Company_Type__c ='Retail';
        eachAccount2.Description ='test description';
        eachAccount2.Sector_Classification__c ='Business Services';
        eachAccount2.Street__c = 'test';
        eachAccount2.City__c  ='test City';
        eachAccount2.Country__c = 'United Arab Emirates';
        eachAccount2.Postal_Code__c = '1235';
        eachAccount2.Phone = '+971512345678';
        eachAccount2.Website = 'www.assdd.com';
        eachAccount2.Company_Type__c ='Retail';
        eachAccount2.Sub_Legal_Type_of_Entity__c=null;
        lstAccount.add(eachAccount2);
        
        if(lstAccount.size()>0){
            INSERT lstAccount;
        }
        Building__c eachBuilding = new Building__c();
        eachBuilding.Name = 'DIFC';
        eachBuilding.Building_No__c = '8888';
        INSERT eachBuilding;
        
        Unit__c eachUnit = new Unit__c();
        eachUnit.Unit_Unique_Ref__c = '00998877';
        eachUnit.Name = '88888';
        eachUnit.Building__c = eachBuilding.ID;
        eachUnit.Floor__c = '2';
        eachUnit.Unit_Square_Feet__c =10;
        INSERT eachUnit;
        
        for(Integer i=0;i<accountCount;i++)
        {
            location= new Operating_Location__c();
            location.Status__c = 'Active';
            location.IsRegistered__c = TRUE;
            location.Account__c = lstAccount[i].ID;
            location.Unit__c = eachUnit.ID;
            lstoLocation.add(location); 
        }
        
        INSERT lstoLocation;
        
        License_Activity_Master__c master = new License_Activity_Master__c();
        master.Name = 'ActivityMaster';
        master.Type__c = 'License Activity';
        INSERT master;
        License_Activity__c license = new License_Activity__c();
        license.Account__c = lstAccount[0].ID;
        license.End_Date__c = null;
        license.Activity__c = master.ID;
        INSERT license;
        businessHours = new Company_Business_Hours__c();
        businessHours.Sunday__c = '6 AM to 9 PM';
        businessHours.Monday__c = '6 AM to 9 PM';
        businessHours.Tuesday__c = '6 AM to 9 PM';
        businessHours.Wednesday__c = '6 AM to 9 PM';
        businessHours.Thursday__c = '6 AM to 9 PM';
        businessHours.Friday__c = '6 AM to 9 PM';
        businessHours.Saturday__c = '6 AM to 9 PM';
        businessHours.Sunday_To__c = '10 PM';
        businessHours.Monday_To__c = '10 PM';
        businessHours.Tuesday_To__c = '10 PM';
        businessHours.Wednesday_To__c = '10 PM';
        businessHours.Thursday_To__c = '10 PM';
        businessHours.Friday_To__c = '10 PM';
        businessHours.Saturday_To__c = '10 PM';
        businessHours.Day__c ='Monday';
        
        businessHours.Account__c = lstAccount[0].ID;
        INSERT businessHours;
        
        WayFinding_Categories__c wc = new WayFinding_Categories__c();
        wc.name = 'Retail1';
        wc.Category__c = 'Shop';
        wc.Sector_Classification__c = 'Retail';
        insert wc;
        
        Company_Images__c ci = new Company_Images__c();
        ci.Image_URL__c ='https://upload.wikimedia.org/wikipedia/commons/f/fa/Apple_logo_black.svg';
        ci.Account__c = lstAccount[0].ID;
        ci.Active__c=true;
        insert ci;
        
        Account accH = new Account(id = lstAccount[0].Id,Hosting_Company__c=lstAccount[1].ID);
        update accH;
        
    }
    
    
     public static void testData11(Integer accountCount)
    {
        testData(1);
        License_Activity_Master__c master = new License_Activity_Master__c();
        master.Name = 'ActivityMaster';
        master.Type__c = 'License Activity';
        INSERT master;
        License_Activity__c license = new License_Activity__c();
        license.Account__c = lstAccount[0].ID;
        license.End_Date__c = null;
        license.Activity__c = master.ID;
        INSERT license;
        businessHours1 = new Company_Business_Hours__c();
        businessHours1.Sunday__c = '6 AM to 9 PM';
        businessHours1.Monday__c = '6 AM to 9 PM';
        businessHours1.Tuesday__c = '6 AM to 9 PM';
        businessHours1.Wednesday__c = '6 AM to 9 PM';
        businessHours1.Thursday__c = '6 AM to 9 PM';
        businessHours1.Friday__c = '6 AM to 9 PM';
        businessHours1.Saturday__c = '6 AM to 9 PM';
        businessHours1.Sunday_To__c = '10 PM';
        businessHours1.Monday_To__c = '10 PM';
        businessHours1.Tuesday_To__c = '10 PM';
        businessHours1.Wednesday_To__c = '10 PM';
        businessHours1.Thursday_To__c = '10 PM';
        businessHours1.Friday_To__c = '10 PM';
        businessHours1.Saturday_To__c = '10 PM';
        businessHours1.Day__c ='Tuesday';
        
        businessHours1.Account__c = lstAccount[0].ID;
        INSERT businessHours1;
        
    }
    
    public static void testData12(Integer accountCount)
    {
        testData(1);
        License_Activity_Master__c master = new License_Activity_Master__c();
        master.Name = 'ActivityMaster';
        master.Type__c = 'License Activity';
        INSERT master;
        License_Activity__c license = new License_Activity__c();
        license.Account__c = lstAccount[0].ID;
        license.End_Date__c = null;
        license.Activity__c = master.ID;
        INSERT license;
        businessHours1 = new Company_Business_Hours__c();
        businessHours1.Sunday__c = '6 AM to 9 PM';
        businessHours1.Monday__c = '6 AM to 9 PM';
        businessHours1.Tuesday__c = '6 AM to 9 PM';
        businessHours1.Wednesday__c = '6 AM to 9 PM';
        businessHours1.Thursday__c = '6 AM to 9 PM';
        businessHours1.Friday__c = '6 AM to 9 PM';
        businessHours1.Saturday__c = '6 AM to 9 PM';
        businessHours1.Sunday_To__c = '10 PM';
        businessHours1.Monday_To__c = '10 PM';
        businessHours1.Tuesday_To__c = '10 PM';
        businessHours1.Wednesday_To__c = '10 PM';
        businessHours1.Thursday_To__c = '10 PM';
        businessHours1.Friday_To__c = '10 PM';
        businessHours1.Saturday_To__c = '10 PM';
        businessHours1.Day__c ='Sunday';
        
        businessHours1.Account__c = lstAccount[0].ID;
        INSERT businessHours1;
        
    }

    public static void testData13(Integer accountCount)
    {
        testData(1);
        License_Activity_Master__c master = new License_Activity_Master__c();
        master.Name = 'ActivityMaster';
        master.Type__c = 'License Activity';
        INSERT master;
        License_Activity__c license = new License_Activity__c();
        license.Account__c = lstAccount[0].ID;
        license.End_Date__c = null;
        license.Activity__c = master.ID;
        INSERT license;
        businessHours1 = new Company_Business_Hours__c();
        businessHours1.Sunday__c = '6 AM to 9 PM';
        businessHours1.Monday__c = '6 AM to 9 PM';
        businessHours1.Tuesday__c = '6 AM to 9 PM';
        businessHours1.Wednesday__c = '6 AM to 9 PM';
        businessHours1.Thursday__c = '6 AM to 9 PM';
        businessHours1.Friday__c = '6 AM to 9 PM';
        businessHours1.Saturday__c = '6 AM to 9 PM';
        businessHours1.Sunday_To__c = '10 PM';
        businessHours1.Monday_To__c = '10 PM';
        businessHours1.Tuesday_To__c = '10 PM';
        businessHours1.Wednesday_To__c = '10 PM';
        businessHours1.Thursday_To__c = '10 PM';
        businessHours1.Friday_To__c = '10 PM';
        businessHours1.Saturday_To__c = '10 PM';
        businessHours1.Day__c ='Wednesday';
        
        businessHours1.Account__c = lstAccount[0].ID;
        INSERT businessHours1;
        
    }
    
    public static void testData14(Integer accountCount)
    {
        testData(1);
        License_Activity_Master__c master = new License_Activity_Master__c();
        master.Name = 'ActivityMaster';
        master.Type__c = 'License Activity';
        INSERT master;
        License_Activity__c license = new License_Activity__c();
        license.Account__c = lstAccount[0].ID;
        license.End_Date__c = null;
        license.Activity__c = master.ID;
        INSERT license;
        businessHours1 = new Company_Business_Hours__c();
        businessHours1.Sunday__c = '6 AM to 9 PM';
        businessHours1.Monday__c = '6 AM to 9 PM';
        businessHours1.Tuesday__c = '6 AM to 9 PM';
        businessHours1.Wednesday__c = '6 AM to 9 PM';
        businessHours1.Thursday__c = '6 AM to 9 PM';
        businessHours1.Friday__c = '6 AM to 9 PM';
        businessHours1.Saturday__c = '6 AM to 9 PM';
        businessHours1.Sunday_To__c = '10 PM';
        businessHours1.Monday_To__c = '10 PM';
        businessHours1.Tuesday_To__c = '10 PM';
        businessHours1.Wednesday_To__c = '10 PM';
        businessHours1.Thursday_To__c = '10 PM';
        businessHours1.Friday_To__c = '10 PM';
        businessHours1.Saturday_To__c = '10 PM';
        businessHours1.Day__c ='Friday';
        
        businessHours1.Account__c = lstAccount[0].ID;
        INSERT businessHours1;
        
    }

    public static void testData15(Integer accountCount)
    {
        testData(1);
        License_Activity_Master__c master = new License_Activity_Master__c();
        master.Name = 'ActivityMaster';
        master.Type__c = 'License Activity';
        INSERT master;
        License_Activity__c license = new License_Activity__c();
        license.Account__c = lstAccount[0].ID;
        license.End_Date__c = null;
        license.Activity__c = master.ID;
        INSERT license;
        businessHours1 = new Company_Business_Hours__c();
        businessHours1.Sunday__c = '6 AM to 9 PM';
        businessHours1.Monday__c = '6 AM to 9 PM';
        businessHours1.Tuesday__c = '6 AM to 9 PM';
        businessHours1.Wednesday__c = '6 AM to 9 PM';
        businessHours1.Thursday__c = '6 AM to 9 PM';
        businessHours1.Friday__c = '6 AM to 9 PM';
        businessHours1.Saturday__c = '6 AM to 9 PM';
        businessHours1.Sunday_To__c = '10 PM';
        businessHours1.Monday_To__c = '10 PM';
        businessHours1.Tuesday_To__c = '10 PM';
        businessHours1.Wednesday_To__c = '10 PM';
        businessHours1.Thursday_To__c = '10 PM';
        businessHours1.Friday_To__c = '10 PM';
        businessHours1.Saturday_To__c = '10 PM';
        businessHours1.Day__c ='Thursday';
        
        businessHours1.Account__c = lstAccount[0].ID;
        INSERT businessHours1;
        
    }
    
    public static void testData16(Integer accountCount)
    {
        testData(1);
        License_Activity_Master__c master = new License_Activity_Master__c();
        master.Name = 'ActivityMaster';
        master.Type__c = 'License Activity';
        INSERT master;
        License_Activity__c license = new License_Activity__c();
        license.Account__c = lstAccount[0].ID;
        license.End_Date__c = null;
        license.Activity__c = master.ID;
        INSERT license;
        businessHours1 = new Company_Business_Hours__c();
        businessHours1.Sunday__c = '6 AM to 9 PM';
        businessHours1.Monday__c = '6 AM to 9 PM';
        businessHours1.Tuesday__c = '6 AM to 9 PM';
        businessHours1.Wednesday__c = '6 AM to 9 PM';
        businessHours1.Thursday__c = '6 AM to 9 PM';
        businessHours1.Friday__c = '6 AM to 9 PM';
        businessHours1.Saturday__c = '6 AM to 9 PM';
        businessHours1.Sunday_To__c = '10 PM';
        businessHours1.Monday_To__c = '10 PM';
        businessHours1.Tuesday_To__c = '10 PM';
        businessHours1.Wednesday_To__c = '10 PM';
        businessHours1.Thursday_To__c = '10 PM';
        businessHours1.Friday_To__c = '10 PM';
        businessHours1.Saturday_To__c = '10 PM';
        businessHours1.Day__c ='Saturday';
        
        businessHours1.Account__c = lstAccount[0].ID;
        INSERT businessHours1;
        
    }


  public static void testData17(Integer accountCount)
    {
        testData(1);
        License_Activity_Master__c master = new License_Activity_Master__c();
        master.Name = 'ActivityMaster';
        master.Type__c = 'License Activity';
        INSERT master;
        License_Activity__c license = new License_Activity__c();
        license.Account__c = lstAccount[0].ID;
        license.End_Date__c = null;
        license.Activity__c = master.ID;
        INSERT license;
        businessHours1 = new Company_Business_Hours__c();
        businessHours1.Sunday__c = '6 AM to 9 PM';
        businessHours1.Monday__c = '6 AM to 9 PM';
        businessHours1.Tuesday__c = '6 AM to 9 PM';
        businessHours1.Wednesday__c = '6 AM to 9 PM';
        businessHours1.Thursday__c = '6 AM to 9 PM';
        businessHours1.Friday__c = '6 AM to 9 PM';
        businessHours1.Saturday__c = '6 AM to 9 PM';
        businessHours1.Sunday_To__c = '10 PM';
        businessHours1.Monday_To__c = '10 PM';
        businessHours1.Tuesday_To__c = '10 PM';
        businessHours1.Wednesday_To__c = '10 PM';
        businessHours1.Thursday_To__c = '10 PM';
        businessHours1.Friday_To__c = '10 PM';
        businessHours1.Saturday_To__c = '10 PM';
        businessHours1.Day__c ='Thursday';
        
        businessHours1.Account__c = lstAccount[0].ID;
        INSERT businessHours1;
        
    }

           
    static testmethod void singleRecordTest()
    {
       testData(1);
        negativeTest();
    
        TEST.startTest();
            RestContext.request = new RestRequest();
            RestContext.request.params.put('Registration_Number','887777' );
            CLS_CreatePRJSON_NanoLiteController.jsonResponse();
        TEST.stopTest();
    }
    
    static testmethod void singleRecordTest11()
    {
        testData11(3);
        negativeTest();
    
        TEST.startTest();
            RestContext.request = new RestRequest();
            RestContext.request.params.put('Registration_Number','887777' );
            CLS_CreatePRJSON_NanoLiteController.jsonResponse();
        TEST.stopTest();
    }
   static testmethod void singleRecordTest12()
    {
        testData12(3);
        negativeTest();
    
        TEST.startTest();
            RestContext.request = new RestRequest();
            RestContext.request.params.put('Registration_Number','887777' );
            CLS_CreatePRJSON_NanoLiteController.jsonResponse();
        TEST.stopTest();
    }
    
     static testmethod void singleRecordTest13()
    {
        testData13(3);
        negativeTest();
    
        TEST.startTest();
            RestContext.request = new RestRequest();
            RestContext.request.params.put('Registration_Number','887777' );
            CLS_CreatePRJSON_NanoLiteController.jsonResponse();
        TEST.stopTest();
    }
    
    
    static testmethod void singleRecordTest14()
    {
        testData14(3);
        negativeTest();
    
        TEST.startTest();
            RestContext.request = new RestRequest();
            RestContext.request.params.put('Registration_Number','887777' );
            CLS_CreatePRJSON_NanoLiteController.jsonResponse();
        TEST.stopTest();
    }
    
    static testmethod void singleRecordTest15()
    {
        testData15(3);
        negativeTest();
    
        TEST.startTest();
            RestContext.request = new RestRequest();
            RestContext.request.params.put('Registration_Number','8877779' );
            CLS_CreatePRJSON_NanoLiteController.jsonResponse();
        TEST.stopTest();
    }
    
    static testmethod void singleRecordTest16()
    {
        testData16(3);
        negativeTest();
    
        TEST.startTest();
            RestContext.request = new RestRequest();
            RestContext.request.params.put('Registration_Number','0066' );
            CLS_CreatePRJSON_NanoLiteController.jsonResponse();
        TEST.stopTest();
    }
    
    
    static testmethod void singleRecordTest1(){
       // testData(1);
        TEST.startTest();
            RestContext.request = new RestRequest();
           RestContext.request.params.put('Registration_Number','2170' );
            CLS_CreatePRJSON_NanoLiteController.jsonResponse();
        TEST.stopTest();
    }
    
    static testmethod void singleRecordTestJson1(){
       testData(1);
        TEST.startTest();
            WayFindingJSONClass.jsonParser('8877779','mode');
        TEST.stopTest();
    }
    static testmethod void singleRecordTestJson2(){
       testData(1);
        TEST.startTest();
            WayFindingJSONClass.jsonParser('8877779','List');
        TEST.stopTest();
    }
    
    static testmethod void singleRecordTestJson6(){
       testData(1);
       
        TEST.startTest();
            WayFindingJSONClass.jsonParser('8877779','List');
        TEST.stopTest();
    }
    
     static testmethod void singleRecordTestJson7(){
      
       testData(1);
       lstAccount[0].Registration_License_No__c = '';
       lstAccount[0].Arabic_Name__c = '';
       lstAccount[0].Trading_Name_Arabic__c = '';
       lstAccount[0].Arabic_Office__c = '';
       lstAccount[0].Arabic_Building_Name__c = '';
       lstAccount[0].Arabic_Street__c = '';
       lstAccount[0].Arabic_Emirate__c = '';
       lstAccount[0].Arabic_Country__c = '';
       lstAccount[0].Sector_Classification__c = '';
       lstAccount[0].Hide_the_Company__c = false;
       lstAccount[0].Company_Type__c = 'Financial';
       
       UPDATE lstAccount[0];
       
        TEST.startTest();
            WayFindingJSONClass.jsonParser('8877779','');
        TEST.stopTest();
    }
  
       
    static testmethod void negativeTest(){
        
        List<Account> lstAccountUpdate = new List<Account>();
        lstoLocation = new List<Operating_Location__c>();
        Operating_Location__c location;
        Account eachAccount = new Account();
        for(integer i=0;i<10;i++){
            eachAccount = new Account();
            eachAccount.Name = 'Test'+i;
            eachAccount.Trade_Name__c = Null;
            eachAccount.RORP_License_No__c = '233233'+i;
            eachAccount.Registered_Address__c = Null;
            eachAccount.Building_Name__c = Null;
            eachAccount.Hide_the_Company__c = false;
            eachAccount.ROC_Status__c = 'Active';
            eachAccount.Registration_License_No__c = '233233'+i;
            eachAccount.Company_Type__c =Null;
            eachAccount.Sector_Classification__c = null;
            eachAccount.City__c= null;
            eachAccount.Country__c =null;
            eachAccount.Postal_Code__c =null;
            eachAccount.Phone = null;
            eachAccount.Website = null;
            eachAccount.Company_Type__c = null;
            
            
            
            
            lstAccountUpdate.add(eachAccount);
        }
        INSERT lstAccountUpdate;
        
        Building__c eachBuilding = new Building__c();
        eachBuilding.Name = null;
        eachBuilding.Building_No__c = null;
        INSERT eachBuilding;
        
        Unit__c eachUnit = new Unit__c();
        eachUnit.Unit_Unique_Ref__c = null;
        eachUnit.Name = '88888';
        eachUnit.Building__c = eachBuilding.ID;
        eachUnit.Floor__c = '2';
        eachUnit.Unit_Square_Feet__c =1;
        INSERT eachUnit;
        
        for(Integer i=0;i<10;i++){
            location= new Operating_Location__c();
            location.Status__c = 'Active';
            location.IsRegistered__c = TRUE;
            location.Account__c = lstAccountUpdate[i].ID;
            location.Unit__c = eachUnit.ID;
            lstoLocation.add(location); 
        }
        
        INSERT lstoLocation;
        
        
        businessHours = new Company_Business_Hours__c();
        businessHours.Sunday__c = null;
        businessHours.Monday__c = null;
        businessHours.Tuesday__c = null;
        businessHours.Wednesday__c = null;
        businessHours.Thursday__c = null;
        businessHours.Friday__c = null;
        businessHours.Saturday__c = null;
        businessHours.Sunday_To__c = null;
        businessHours.Monday_To__c = null;
        businessHours.Tuesday_To__c = null;
        businessHours.Wednesday_To__c = null;
        businessHours.Thursday_To__c = null;
        businessHours.Friday_To__c = null;
        businessHours.Saturday_To__c = null;
        businessHours.Day__c = null;
        businessHours.Account__c = lstAccountUpdate[0].ID;
        INSERT businessHours;
        //TEST.startTest();
            CLS_CreatePRJSON_NanoLiteController.jsonResponse();
          //   WayFindingJSONClass.testsample();
        //TEST.stopTest();
    }
    
	    static testmethod void testbyDIFCdeveloper(){
	    	
	    	testData(1);
	    	List<Account> lstAccountsUpdate = new List<Account>();
	    	for(Account eacAccount:lstAccount){
	    		eacAccount.Registration_License_No__c = '';
	    		eacAccount.Arabic_Name__c = '';
	    		eacAccount.Trading_Name_Arabic__c = '';
	    		eacAccount.Arabic_Office__c = '';
	    		eacAccount.Arabic_Building_Name__c  = '';
	    		eacAccount.Arabic_Street__c = '';
	    		eacAccount.Arabic_City__c = '';
	    		eacAccount.Arabic_Emirate__c = '';
	    		eacAccount.Sector_Classification__c = '';
	    		eacAccount.Company_Type__c = 'Financial';
	    		lstAccountsUpdate.add(eacAccount);
	    	}
	    	UPDATE lstAccountsUpdate;
	    	WayFindingJSONClass.jsonParser('','List');
	    	
	    }
	    
	    static testmethod void testbyDIFCdeveloper5(){
	    	
	    	testData(1);
	    	List<Account> lstAccountsUpdate = new List<Account>();
	    	for(Account eacAccount:lstAccount){
	    		eacAccount.Registration_License_No__c = '';
	    		eacAccount.Arabic_Name__c = '';
	    		eacAccount.Trading_Name_Arabic__c = '';
	    		eacAccount.Arabic_Office__c = '';
	    		eacAccount.Arabic_Building_Name__c  = '';
	    		eacAccount.Arabic_Street__c = '';
	    		eacAccount.Arabic_City__c = '';
	    		eacAccount.Arabic_Emirate__c = '';
	    		eacAccount.Sector_Classification__c = '';
	    		eacAccount.Company_Type__c = 'Retail';
	    		lstAccountsUpdate.add(eacAccount);
	    	}
	    	UPDATE lstAccountsUpdate;
	    	WayFindingJSONClass.jsonParser('','List');
	    	
	    }
	    
	     static testmethod void testbyDIFCdeveloper2(){
	    	
	    	testData(1);
	    	List<Account> lstAccountsUpdate = new List<Account>();
	    	for(Account eacAccount:lstAccount){
	    		eacAccount.Registration_License_No__c = '';
	    		eacAccount.Arabic_Name__c = '';
	    		eacAccount.Trading_Name_Arabic__c = '';
	    		eacAccount.Arabic_Office__c = '';
	    		eacAccount.Arabic_Building_Name__c  = '';
	    		eacAccount.Arabic_Street__c = '';
	    		eacAccount.Arabic_City__c = '';
	    		eacAccount.Arabic_Emirate__c = '';
	    		eacAccount.Sector_Classification__c = '';
	    		eacAccount.Company_Type__c = 'Non Financial';
	    		eacAccount.Operating_Location__c = null;
	    		lstAccountsUpdate.add(eacAccount);
	    	}
	    	UPDATE lstAccountsUpdate;
	    	WayFindingJSONClass.jsonParser('','List');
	    	
	    }
    
    
}