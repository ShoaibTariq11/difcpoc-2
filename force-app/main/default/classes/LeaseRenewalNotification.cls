/*************************************************************************************************
 *  Name        : LeaseRenewalNotificationSchd 
 *  Author      : Danish Farooq
 *  Date        : 22-01-2018    
 *  Purpose     : Batch class to Send notificiation clients (DIFC companies) 
                    to reminder them to renew their lease 30 days 
                    before the lease expiry date
                    Ticket # 3377
 *  TestClass   : LeaseRenewalNotificationTest
**************************************************************************************************/
Global without sharing class LeaseRenewalNotification implements Database.Batchable<sobject>{

    Private String SrQueryString;
    OrgWideEmailAddress  owa;
    EmailTemplate templateId ;
    string LeaseStatus = 'Active';
    String retail = 'Retail';
    String Commercial='Commercial';
    //ID id1 = 'a1Q1j0000004gnt';
    list<string> Leasetypes = new list<string>{'Leased Property','Leased Property Registration'};
        
        
        
  Global LeaseRenewalNotification()
  {
         
        SrQueryString = 'Select ID ,Lease__r.Account__r.Name,Lease__r.Account__c FROM Tenancy__c Where ';
        SrQueryString += 'Lease_Status__c =:LeaseStatus AND Days_left_in_Renewal__c<=30 ';
        SrQueryString += 'AND Days_left_in_Renewal__c > 0 AND lease__r.Visa_Factor__c > 0 '; 
        SrQueryString += 'AND Lease__r.Account__c != NULL  AND Third_Party__c = true  AND LeaseRenewalEmailSent__c= False ';
        SrQueryString += 'AND Lease_Types__c IN :Leasetypes AND (Unit_Usage_Type__c =:retail OR Unit_Usage_Type__c = :Commercial) ';
        
        owa =  [select id, Address, DisplayName from OrgWideEmailAddress 
                where Address = 'noreply.portal@difc.ae' limit 1];
        templateId = [Select id from EmailTemplate where name = 'RORP - Lease Renewal Notification'];
    
  }
  
  Global Database.QueryLocator start(Database.BatchableContext BC)
  {
    return Database.getQueryLocator(SrQueryString);      
    
  }
  
  Global Void execute(Database.BatchableContext BC,list<Tenancy__c > leaseList){
      
     Set<ID> setOfAccount = new Set<ID>();
     for(Tenancy__c ilease : leaseList){
         setOfAccount.add(ilease.Lease__r.Account__c);
     }
     List<string> emailAddressesList = new List<string>();
     List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
      map<Id, List<id>> Acctocon = new map<Id, List<id>>();
      list<ID> Conids = new list<ID>();
    for(Contact con :[Select ID, Email, AccountID from Contact where AccountID IN : setOfAccount
                      AND RecordType.DeveloperName='Portal_User'
                      /* AND Portal_Username__c ='C-Shaikh.ZoebAkhtar@difc.ae' */
                      AND Id IN (select ContactId from User where Contact.AccountId IN : setOfAccount AND 
                      IsActive = true AND Community_User_Role__c INCLUDES ('Company Services','Property Services'))]){
                          if(Acctocon.containsKey(Con.AccountID)){
                              Conids = new list<id>();
                              Conids.addAll(Acctocon.get(Con.AccountID));
                              ConIds.add(Con.Id);
                              Acctocon.put(Con.AccountID,ConIds);
                         //     system.debug('!!@@@'+Acctocon);    
                          }
                          else
                          {
                              Conids = new list<id>();
                              ConIds.add(Con.Id);
                              Acctocon.put(Con.AccountID,ConIds);
                          }
                          
                             
      // system('test debug:'+ConIds);
    // mails.add(SendEmail(con.ID,templateId.Id,owa.Id)); 
      }
      system.debug('Acctocon: '+Acctocon);
      for(Id actId : Acctocon.keySet()){
          mails.add(SendEmail(Acctocon.get(actId),templateId.Id,owa.Id));    
          
      }
      
      system.debug('size of mails:'+mails.size());
      system.debug('!!@@@'+mails); 
      
      Messaging.sendEmail(mails,false);
      
      List<Tenancy__C> TenancyList = New List<Tenancy__C>();
        for(Tenancy__C T : leaseList){
            T.LeaseRenewalEmailSent__c = True;
            TenancyList.add(T);
            
        }
        If (TenancyList.size()>0){
            Update TenancyList;
            
        }
    
                    
  }
  
  
   // Messaging.SingleEmailMessage SendEmail(ID contactID ,ID templateId, ID orgWideAddressID)
   Messaging.SingleEmailMessage SendEmail(list<ID> ConIdss ,ID templateId, ID orgWideAddressID){
       Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
             message.setTargetObjectId(Label.LeaseRenewalTargetID);
       message.setTreatTargetObjectAsRecipient(false);
       String[] ccAddresses = new String[] {Label.RORPLeaserenewalNotification};
       message.setccAddresses(ccAddresses);
       message.setOrgWideEmailAddressId(orgWideAddressID);
       message.setTemplateID(templateId);
       message.setSaveAsActivity(false);
       message.setToAddresses(ConIdss);
       return message;
      // Messaging.sendEmail( new Messaging.SingleEmailMessage[] { message } );

      
  }
  
  Global void finish(Database.BatchableContext BC)
  {
    
  }

}