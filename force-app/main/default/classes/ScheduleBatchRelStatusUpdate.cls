/*
    Author      :   Veera
    Description :   This class is used to update the Relationship Status to inactive
    --------------------------------------------------------------------------------------------------------------------------
  Modification History
   --------------------------------------------------------------------------------------------------------------------------
  V.No  Date    Updated By      Description
  --------------------------------------------------------------------------------------------------------------------------             
   V1.0    17-10-2017  Veera      Created   
*/
global without sharing class ScheduleBatchRelStatusUpdate implements Schedulable {
  global void execute(SchedulableContext ctx) {
    BatchRelStatusUpdate obj = new BatchRelStatusUpdate();
    database.executeBatch(obj);
  }
}