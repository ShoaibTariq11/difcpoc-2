/*
 * Author: Diana Correa
 * Purpose: This is the login controller for mobile login page.
 * 
 */

global with sharing class MobileCustomLoginController {
    global String username{get;set;}
    global String password{get;set;}
  
    
    global PageReference forwardToCustomAuthPage() {
        return new PageReference( '/MobileCustomLogin');
    }
    global PageReference login() {
       
        return Site.login(username, password, '/MobileHome');
    }

}