public without sharing class DIFC_Payment {
	// current logged in user account detail
    public static string userAccountID;
    
    public static string userID;
    
   @AuraEnabled
    public static string makePayment(String reqWrapPram ) {
    	RequestWrapper reqWrap = (DIFC_Payment.RequestWrapper) JSON.deserializeStrict(reqWrapPram, DIFC_Payment.RequestWrapper.class);
        system.debug('==reqWrap===='+reqWrap);
        /*Contains logged in user Id*/ 
        userId = UserInfo.getUserId();
        // query user information
        for(User usr : [Select contactid,Contact.AccountId,Contact.Account.Name from User where Id =: userId]){
            if(usr.contactid != null){
                //userContactId = usr.contactid;
                if(usr.Contact.AccountId != null){
                    userAccountID = usr.Contact.AccountID;
                }
            }
        }
        system.debug('-----userAccountID---'+userAccountID);
        //userAccountID = '0011x00000TvELIAA3';
        string receipt = DIFC_Payment.createReceiptRecord(reqWrap);
        return receipt;
    }
   
    @AuraEnabled
    public static ProductInfo viewProductInformation(string srId, string category){
        
        ProductInfo prod = new ProductInfo();
        decimal totalAmt = 0;
        string accountId;
        for(User usr : [Select contactid,Contact.AccountId,Contact.Account.Name from User where Id =: userId]){
            accountId = usr.Contact.AccountID;
        }
        if(String.isNotBlank(category)  && category == 'namereserve'){
             for(HexaBPM__SR_Price_Item__c srPriceItem : [select HexaBPM__Product__r.Name,HexaBPM__Price__c  from HexaBPM__SR_Price_Item__c 
                                                            where HexaBPM__ServiceRequest__c = :srId 
                                                            //and HexaBPM__ServiceRequest__r.HexaBPM__Customer__c = :accountId 
                                                            and HexaBPM__Product__r.ProductCode = 'OB Name Reservation' 
                                                            and HexaBPM__Status__c = 'Blocked' limit 1]){
                prod.lstPriceItem.add(srPriceItem);
                totalAmt = totalAmt + srPriceItem.HexaBPM__Price__c;                               
            }
        }
        else
        {
            for(HexaBPM__SR_Price_Item__c srPriceItem : [select HexaBPM__Product__r.Name,HexaBPM__Price__c from HexaBPM__SR_Price_Item__c
                                                        where HexaBPM__ServiceRequest__c = :srId 
                                                        //and HexaBPM__ServiceRequest__r.HexaBPM__Customer__c = :accountId 
                                                         and HexaBPM__Product__r.ProductCode != 'OB Name Reservation'
                                                        and HexaBPM__Status__c = 'Blocked']){
                    prod.lstPriceItem.add(srPriceItem);
                    totalAmt = totalAmt + srPriceItem.HexaBPM__Price__c;                               
            }
        }
        prod.total = totalAmt;
        return prod;
    }
    public static string createReceiptRecord(RequestWrapper reqWrap){
    	system.debug('=====reqWrap===='+reqWrap);
    	Id rt=Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get(reqWrap.paymentType).getRecordTypeId();
    	//Create a receipt record with values entered by the customer
        Receipt__c R = new Receipt__c();
        R.Customer__c=userAccountID;
        R.Amount__c=Decimal.valueof(reqWrap.amount); 
        R.Receipt_Type__c=reqWrap.paymentType;
        R.Transaction_Date__c=system.now(); 
        R.RecordTypeId=rt;
        R.Checkout_Payment_ID__c=reqWrap.checkoutPaymentID;
        R.Payment_Status__c=reqWrap.receiptStatus;
        R.Card_Type__c=reqWrap.cardType;
        system.debug('======recipt record=='+R);
        insert R; 
        string receiptReference;
        for(Receipt__c receipt : [select name from Receipt__c where id = :R.Id])
            receiptReference = receipt.name;

        return receiptReference;
        
    }
   @AuraEnabled
    public static List <String> getselectOptions(sObject objObject, string fld) {
        system.debug('objObject --->' + objObject);
        system.debug('fld --->' + fld);
        List < String > allOpts = new list < String > ();
        // Get the object type of the SObject.
        Schema.sObjectType objType = objObject.getSObjectType();
        
        System.debug('objType is' + objType);
        // Describe the SObject using its object type.
        
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        System.debug('objDescribe is' + objDescribe);
        System.debug('Result is' + objObject.getSObjectType().getDescribe().fields.getMap());
        // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        System.debug('fieldMap is' + fieldMap);
        
        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values = fieldMap.get(fld).getDescribe().getPickListValues();
        System.debug('values are '+values);
        // Add these values to the select option list.
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        allOpts.sort();
        return allOpts;
    }
    //Submitting for Name reservation approval process   
    public static void submitForNameApproval(String transactionId,String srId){
        system.debug('transactionId '+transactionId);
        system.debug('srId '+srId);
        for(HexaBPM__Service_Request__c loggedinUserCustApp : [Select id,Name_Payment_Transaction_Id__c from HexaBPM__Service_Request__c where Id=:srId limit 1]){
            loggedinUserCustApp.Name_Payment_Transaction_Id__c=transactionId;
            update loggedinUserCustApp;
        }
            
        List<Company_Name__c> lstCompNames = new List<Company_Name__c>();
        String submittedNameRefNumbers='';
        
        for(Company_Name__c cn: [select id,Entity_Name__c,OwnerId,Ends_With__c,Name,Stage__c,Status__c,Reserved_Name__c from Company_Name__c where Application__c =:srId]){
            cn.Stage__c='Submitted';
            cn.Status__c='In Progress'; 
            cn.Reserved_Name__c=true;
            lstCompNames.add(cn);
        }
        if(lstCompNames != null && lstCompNames.size() > 0)
            update lstCompNames;
    }
    @AuraEnabled
    public static ProductInfo createNameReservationSR(string srId, decimal amountPaid, string transactionId){
        
        string recordTypeName;
        string pageId;

        //Create Sub SR
        OB_NamingConventionHelper.CreateNameApprovalSR(srId);

        //Submit CompanyNames for Approval
        DIFC_Payment.submitForNameApproval(transactionId, srId);

        //Create Price Item as "Blocked"
        HexaBPM__SR_Price_Item__c priceItem = new HexaBPM__SR_Price_Item__c();
        priceItem.HexaBPM__ServiceRequest__c = srId;
        priceItem.HexaBPM__Price__c = amountPaid;
        priceItem.HexaBPM__Status__c = 'Blocked';
        for(Product2 product : [select id from Product2 where ProductCode = 'OB Name Reservation']){
            priceItem.HexaBPM__Product__c = product.id;
        }
        insert priceItem;

        OB_NamingConventionHelper.setPageTrackerForNameReservation(srId);


        ProductInfo productObj = new ProductInfo(); 
        productObj.total =  priceItem.HexaBPM__Price__c;
        productObj.name = 'Name Reservation';
        //productObj.lstPriceItem.add(priceItem);
        return productObj;
    }
	// Update the SR_Price_items when the payment is done.  
    // Update the SR Status to Submitted  
    //The below method is invoked from OB_CheckoutPayment Component and also from DIFC_TopupBalance class
    @AuraEnabled
    public static SRPayment updateSRLines(String srId ) {
        SRPayment paymentObj = new SRPayment();
        boolean isSuccess = false;
        boolean IsAllowedToSubmit = false;
        try{
            HexaBPM__Service_Request__c objRequest = new HexaBPM__Service_Request__c(id = srId);
            for(HexaBPM__Service_Request__c SR : [select Id from HexaBPM__Service_Request__c where id = :srId and HexaBPM__Internal_SR_Status__r.Name='Draft']){
                IsAllowedToSubmit = true;
            }
            if(IsAllowedToSubmit){
                //Updating status to submitted only for the Draft SR's
                for(HexaBPM__SR_Status__c status:[select id,Name from HexaBPM__SR_Status__c where (Name='Submitted' or Name= 'submitted') limit 1]){
                    objRequest.HexaBPM__Internal_SR_Status__c = status.id;
                    objRequest.HexaBPM__External_SR_Status__c = status.id;
                }
                objRequest.HexaBPM__FinalizeAmendmentFlg__c = true;
                update objRequest;

                //Updating the Price LineItems to "Blocked"
                list<HexaBPM__SR_Price_Item__c> srPriceItems = new list<HexaBPM__SR_Price_Item__c>();
                for(HexaBPM__SR_Price_Item__c srPriceItem : [SELECT id,HexaBPM__Status__c FROM HexaBPM__SR_Price_Item__c WHERE HexaBPM__ServiceRequest__c =: srId and HexaBPM__Status__c='Added']){
                    srPriceItem.HexaBPM__Status__c = 'Blocked';
                    srPriceItems.add(srPriceItem);
                }
                if(srPriceItems != null && srPriceItems.size() > 0)
                    update srPriceItems;
            }
            
            isSuccess = true;
        } catch (DMLException e){
            paymentObj.isError = true;
            string DMLError = e.getdmlMessage(0) + '';
			if(DMLError == null) {
				DMLError = e.getMessage() + '';
			}
            paymentObj.message = DMLError;
            System.debug('=====>'+ e.getMessage());
        }
        
        return paymentObj;
    }
    /**********************************Wrapper classes*********************************************************/	
    public class ResponseWrapper {
        @AuraEnabled public Boolean isVATNotRegistered		{get;set;}
    } 
    public class ProductInfo{
        @AuraEnabled public decimal total{get;set;}
        @AuraEnabled public list<HexaBPM__SR_Price_Item__c> lstPriceItem {get;set;}
        @AuraEnabled public string name {get;set;}
        public ProductInfo(){
            lstPriceItem = new list<HexaBPM__SR_Price_Item__c>();
            total = 0;
        }
    }   
    public class SRPayment{
        @AuraEnabled public string message {get;set;}
        @AuraEnabled public boolean isError {get;set;}
        @AuraEnabled public string receiptRef {get;set;}
        public SRPayment(){
            isError = false;
        }
    }
    public class RequestWrapper{
        @AuraEnabled public String amount 					{get;set;}
        @AuraEnabled public String paymentType 				{get;set;}
        @AuraEnabled public String checkoutPaymentID 		{get;set;}
        @AuraEnabled public String receiptStatus 			{get;set;}
        @AuraEnabled public String cardType 				{get;set;}
    }
}