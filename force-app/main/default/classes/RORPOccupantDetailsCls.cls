/******************************************************************************************
 *  Author   : Ravindra Babu Nagaboina
 *  Company  : NSI DMCC
 *  Date     : 26th Sep 2016   
 *  Description : This class will be used as controller for RORPOccupantDetails page which is being used for Lease_Registratoin SR.                       
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By          Description
 ----------------------------------------------------------------------------------------              
 V1.0  26/Sep/2016      Ravi                Created
 v1.1	10/04/2016		Ravi				Occupant Record Type was not assigned and its taking diffrent record types in prod
*******************************************************************************************/
public without sharing class RORPOccupantDetailsCls {
	
	public string SRNo {get;set;}
	public Service_Request__c ServiceRequest {get;set;}
	public Amendment__c AmendmentTemp {get;set;}
	public list<OccupantDetails> Occupants {get;set;}
	public Boolean showNewOccupant {get;set;}
	public list<Selectoption> lstUnits {get;set;}
	public Integer RowIndex {get;set;}
	public string OccupantRecordTypeId;
	
	public RORPOccupantDetailsCls(){
		ServiceRequest = new Service_Request__c();
		AmendmentTemp = new Amendment__c();
		if(Apexpages.currentPage().getParameters().get('id') != null){
			for(Service_Request__c objSR : [select Id,Name from Service_Request__c where Id=:Apexpages.currentPage().getParameters().get('id') ]){
				SRNo = objSR.Name;
				ServiceRequest = objSR;
			}
		}
		getOccupantInfo();
		getUnitsInfo();
	}

	public void getOccupantInfo(){
		Occupants = new list<OccupantDetails>();
		OccupantDetails objOccupantDetails;
		for(Amendment__c objAmd : [select Id,Name,ServiceRequest__c,First_Name__c,Middle_Name__c,Last_Name__c,Passport_No_Occupant__c,Nationality_Occupant__c,Emirates_ID_Number__c,Unit__c,Unit__r.Name from Amendment__c where ServiceRequest__c=:ServiceRequest.Id AND Amendment_Type__c = 'Occupant']){
			objOccupantDetails = new OccupantDetails();
			objOccupantDetails.OccupantAmendment = objAmd;
			objOccupantDetails.Index = Occupants.size();
			Occupants.add(objOccupantDetails);
		}
		
		for(RecordType objRT : [select Id from RecordType where DeveloperName = 'Occupant']){
			OccupantRecordTypeId = objRT.Id;
		}
	}

	public void AddOccupant(){
		AmendmentTemp = new Amendment__c();
		AmendmentTemp.ServiceRequest__c = ServiceRequest.Id;
		AmendmentTemp.Amendment_Type__c = 'Occupant';
		if(OccupantRecordTypeId != null && OccupantRecordTypeId != '')
			AmendmentTemp.RecordTypeId = OccupantRecordTypeId;
		showNewOccupant = true;
	}
	
	public void SaveOccupant(){
		if(AmendmentTemp != null){
			try{
				if(AmendmentTemp.Unit__c == null){
					Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please select the unit to continue.'));
					return;
				}
				upsert AmendmentTemp;
				
				getOccupantInfo();
				getUnitsInfo();
				showNewOccupant = false;
			}catch(Exception ex){
				
			}
		}
	}
	
	public void CancelOccupant(){
		AmendmentTemp = new Amendment__c();
		showNewOccupant = false;
	}
	
	public Pagereference BackToSR(){
		Pagereference objRef = new Pagereference('/'+ServiceRequest.Id);
		objRef.setRedirect(true);
		return objRef;
	}
	
	public void RemoveOccupant(){
		if(RowIndex != null && Occupants != null && RowIndex < Occupants.size()){
			if(Occupants[RowIndex].OccupantAmendment != null)
				delete Occupants[RowIndex].OccupantAmendment;
			Occupants.remove(RowIndex);
			Integer i = 0;
			if(!Occupants.isEmpty()){
				for(OccupantDetails obj : Occupants){
					obj.Index = i;
					i++;
				}
			}
			getUnitsInfo();
		}
	}
	
	public void getUnitsInfo(){
		lstUnits = new list<Selectoption>();
		list<Id> lstUnitIds = new list<Id>();
		if(Occupants != null){
			for(OccupantDetails objOD : Occupants){
				if(objOD.OccupantAmendment != null && objOD.OccupantAmendment.Unit__c != null){
					lstUnitIds.add(objOD.OccupantAmendment.Unit__c);
				}
			}
		}
		lstUnits.add(new Selectoption('','--None--'));
		for(Amendment__c objAmd : [select Id,Unit__c,Unit__r.Name from Amendment__c where ServiceRequest__c =:ServiceRequest.Id AND Amendment_Type__c = 'Unit' AND Unit__c != null AND Unit__c NOT IN : lstUnitIds]){
			lstUnits.add(new Selectoption(objAmd.Unit__c,objAmd.Unit__r.Name));
		}
	}
	
	public class OccupantDetails{
		public Amendment__c OccupantAmendment {get;set;}
		public Integer Index {get;set;}
	}
	
}