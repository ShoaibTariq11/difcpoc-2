/*
        Author      :   Shabbir
        Description :   This class is used to pass the account data in Operate system
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.0    30-06-2018  Shabbir         Created
*/    
public without sharing class OperateAccountPushCls {
    
    /**
        * Creates a lead in the operate system once condition met in SF
        * account records
        * @params      ids                 List of Account Ids
    */
    @InvocableMethod(label='Create Account in Operate' description='Creates a lead in the operate system once condition met in SF')
    public static void createAccountInOperate(list<Id> listAccountIds){
      OperateIntegrationCls.pushSFAccounttoOperate(listAccountIds);
    }    
    
 }