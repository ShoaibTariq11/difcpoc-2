/**********************************************************
Class Name: AppointmentCreationUtilityCls
Description: re developed the AppointmentCreationHandler logic
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By       Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    18-09-2019  Selva    #7365  Having issue on normal and express appointment scheduling,comment out the AppointmentCreationHandler and re developed the appointment logic
v1.1    18-Dec-2019 selva fixed the first slot rescheduling logic
********************************************************/
public class AppointmentCreationUtilityCls {

    public static Appointment__c Appntment = new Appointment__c();
    public static boolean isnextday = false;
    public static string Counter = system.label.Medical_Appointment_Counter_Mins;
    public static Integer LunchDuration = Integer.valueof(system.label.Lunch_Duration);
    public static Integer ExpressLunchDuration = Integer.valueof(system.label.Express_Lunch_Duration);
    public static string Lunch_Time = system.label.Lunch_Time;
    public static string Lunch_Time_Express = system.label.Lunch_Time_Express;
    public static string LunchEnd_Time = system.label.LunchEnd_Time;
    public static string LunchEnd_Time_Express = system.label.LunchEnd_Time_Express;
    public static integer Mins = Integer.valueof(Counter);
    public static integer ExpressMins = Integer.valueof(system.label.Medical_Express_Mins);
    public static integer ExpressMins10 = Integer.valueof(system.label.Express_Mins_10);
    public static string ExpressLunchHour = system.label.Express_Lunch_Hours;
    public static integer min5 = Integer.valueOF(system.label.min5);
    
    public static List<service_request__c> SR;
    public static BusinessHours BH ;
    public static BusinessHours BHNormal ;
    public static Boolean isInserted;
    public static Boolean isRescheduled;
    
    public static List<Appointment_Schedule_Counter__c> oldappointmentscheulerLst;
    public static Map<Datetime,string> checkRescheduleOldList;
    public static List<Appointment_Schedule_Counter__c> lastAppBySystem;
    public static Map<string,Datetime> existingAppCalendarLst;
    
    public static Datetime expressTestTime;
    public static Datetime normalTestTime;
    
    public static string MedicalAppointmentCreate(Step__c step) {
    
       SR = new List<service_request__c>();
       oldappointmentscheulerLst = new List<Appointment_Schedule_Counter__c>();
       checkRescheduleOldList = new Map<Datetime,string>();
       existingAppCalendarLst = new Map<string,Datetime>();
       lastAppBySystem = new List<Appointment_Schedule_Counter__c>();
       isInserted = false;
       isRescheduled = false;
       List < Appointment_Schedule_Counter__c > ListAppointmentCounter = new List < Appointment_Schedule_Counter__c > ();
       SR = [select id, Express_Service__c, name, first_Name__c, Last_Name__c from service_request__c where id =: Step.sr__c limit 1];
       
       if(!Test.isRunningTest()){
        bh = [SELECT Id, FridayStartTime, MondayEndTime, MondayStartTime, SaturdayEndTime, SaturdayStartTime, SundayEndTime, SundayStartTime, ThursdayEndTime, ThursdayStartTime, TuesdayEndTime, TuesdayStartTime, WednesdayStartTime, WednesdayEndTime FROM BusinessHours where name = 'GS- Medical Appointment Express' limit 1 ];
       }else{
           bh =[select id from BusinessHours where IsDefault=true];
       }
        
        //if appointment is Normal type
        if (SR.size() > 0 && SR[0].Express_Service__c == false){
            
            //DateTime dtNormal = step.createdDate.addDays(1);
            //dtNormal = Datetime.newInstance(2019,10,03,11,53,00);
            DateTime dtNormal;
            if(Test.isRunningTest()){
                dtNormal = normalTestTime;
            }else{
              dtNormal = step.createdDate;
            }
            
            //dtNormal = Datetime.newInstance(2019,12,24,01,05,00);
            dtNormal = dtNormal.addHours(8);
           // system.assertEquals(null, dtNormal);
            system.debug('--dtNormal-->'+dtNormal+'-----bh id-'+bh.Id);
            Boolean isWithin= BusinessHours.isWithin(bh.id, dtNormal);
            DateTime appointmentNormalStartTime;
            oldappointmentscheulerLst = appointmentCalendarList(dtNormal,'Normal');
           
            if(isWithin){
                appointmentNormalStartTime = CheckAppointmentSameday(isWithin,step,dtNormal,'Normal');
                if(appointmentNormalStartTime!=null){
                    Boolean isWithinDay= BusinessHours.isWithin(bh.id, appointmentNormalStartTime);
                    if(isWithinDay){
                        appointmentNormalStartTime = checklunchtime(appointmentNormalStartTime,'Normal');
                        insertAppointmentRecord(appointmentNormalStartTime,step,'Normal');
                    }else{
                        CheckAppointmentNextDay(isWithinDay,step,appointmentNormalStartTime,'Normal');
                    }
                }
            }else{
                CheckAppointmentNextDay(isWithin,step,dtNormal,'Normal');
            }
        }
        //if appointment is Express type
        else if (SR.size() > 0 && SR[0].Express_Service__c == true){
            
            DateTime dtNormal;
             if(Test.isRunningTest()){
                dtNormal = expressTestTime;
            }else{
              dtNormal = step.createdDate;
            }
            //dtNormal = Datetime.newInstance(2019,12,24,06,00,00);
            dtNormal = dtNormal.addMinutes(2);
            dtNormal = dtNormal.addMinutes(ExpressMins);
            Boolean isWithin= BusinessHours.isWithin(bh.id, dtNormal);
            DateTime appointmentExpressStartTime;
            oldappointmentscheulerLst = appointmentCalendarList(dtNormal,'Express');
            
            if(isWithin){
                
                appointmentExpressStartTime = CheckAppointmentSameday(isWithin,step,dtNormal,'Express');
               
                if(appointmentExpressStartTime!=null){
                    Boolean isWithinDay= BusinessHours.isWithin(bh.id, appointmentExpressStartTime);
                    if(isWithinDay){
                        appointmentExpressStartTime = checklunchtime(appointmentExpressStartTime,'Express');
                        Boolean isWithinSameDay= BusinessHours.isWithin(bh.id, appointmentExpressStartTime);
                        if(isWithinSameDay){
                         insertAppointmentRecord(appointmentExpressStartTime,step,'Express');
                        }else{
                            CheckAppointmentNextDay(isWithinDay,step,appointmentExpressStartTime,'Express');
                        }
                        
                    }else{
                        CheckAppointmentNextDay(isWithinDay,step,appointmentExpressStartTime,'Express');
                    }
                }
            }else{
                CheckAppointmentNextDay(isWithin,step,dtNormal,'Express');
            }
        }
        
        return 'Processed';
    }
    public static DateTime CheckAppointmentSameday(Boolean isWithin,Step__c step,DateTime dtNormal,string appType){
    
        DateTime appointmentStartTime;
        Date normalStartDate = Date.newinstance(dtNormal.year(), dtNormal.month(), dtNormal.day());
        if (isWithin){
            
            if(appType =='Express'){
                if(!existingAppCalendarLst.containsKey(string.valueOf(dtNormal.addMinutes(5)))){
                    appointmentStartTime = checklunchtime(dtNormal,appType);
                    insertAppointmentRecord(appointmentStartTime,step,appType);
                    return null;
                }else{
                    if(oldappointmentscheulerLst[0].Appointment_Time__c == lastAppBySystem[0].Appointment_Time__c){
                        appointmentStartTime = oldappointmentscheulerLst[0].Appointment_Time__c;
                    }
                    else if(lastAppBySystem[0].Appointment_Time__c < oldappointmentscheulerLst[0].Appointment_Time__c){
                        appointmentStartTime = lastAppBySystem[0].Appointment_Time__c;
                    }
                }
            }
            else{
                if(!existingAppCalendarLst.containsKey(string.valueOf(dtNormal.addMinutes(5)))){
                    appointmentStartTime = checklunchtime(dtNormal,appType);
                    insertAppointmentRecord(appointmentStartTime,step,appType);
                    return null;
                }else{
                   
                    if(!isRescheduled){
                        if(lastAppBySystem[0].Appointment_Time__c.addMinutes(mins) ==dtNormal){
                        appointmentStartTime = dtNormal.addMinutes(mins);
                        }else if(lastAppBySystem[0].Appointment_Time__c.addMinutes(mins) < dtNormal){
                            appointmentStartTime = dtNormal;
                        }else if(lastAppBySystem[0].Appointment_Time__c.addMinutes(mins) > dtNormal){
                            appointmentStartTime =lastAppBySystem[0].Appointment_Time__c;
                        }
                    }else if(isRescheduled){
                        appointmentStartTime = dtNormal;
                    }
                }
            }
        }
        return appointmentStartTime;
    }
    public static void  CheckAppointmentNextDay(Boolean isWithin,Step__c step,DateTime dtNormal,string appType){
    
    try{
        if(!isWithin){
            
            Datetime StartDatetime = BusinessHours.nextStartDate(BH.Id,dtNormal);
            Date nextNormalBusinessStartDate = Date.newinstance(StartDatetime.year(), StartDatetime.month(), StartDatetime.day());
            
            if(appType=='Express'){
                
                if(existingAppCalendarLst.get(string.valueof(StartDatetime.addMinutes(7)))==null){//end time check
                   if(checkRescheduleOldList.containsKey(StartDatetime.addMinutes(2))){
                       StartDatetime = StartDatetime.addMinutes(0);//Add start time
                   }else{
                       StartDatetime = StartDatetime.addMinutes(2);//Add start time
                   }
                    
                    StartDatetime = checklunchtime(StartDatetime,appType);
                    
                    insertAppointmentRecord(StartDatetime,step,appType);
                }else{
                    
                    DateTime appointmentStartTime;
                    if(dtNormal <=lastAppBySystem[0].Appointment_Time__c){
                        appointmentStartTime = CheckNextAvailableTime(lastAppBySystem[0].Appointment_Time__c,apptype);
                    }else{
                        appointmentStartTime = dtNormal;
                    }
                    Boolean isWithinDay= BusinessHours.isWithin(bh.id,appointmentStartTime);
                    if(isWithinDay){
                        appointmentStartTime = checklunchtime(appointmentStartTime,appType);
                        
                        insertAppointmentRecord(appointmentStartTime,step,appType);
                    }else{
                        
                        appointmentStartTime = appointmentStartTime.addMinutes(5);
                        CheckAppointmentNextDay(false,step,appointmentStartTime,appType);
                    }
                }
            }
            else{// Normal appointment check next day slot
                
                if(existingAppCalendarLst.get(string.valueof(StartDatetime.addMinutes(5)))==null){
                    
                    StartDatetime = checklunchtime(StartDatetime,appType);
                    insertAppointmentRecord(StartDatetime,step,appType);
                }else if(lastAppBySystem.size()>0){
                    //taking the last appointment datetime
                    
                    DateTime appointmentStartTime;
                    if(dtNormal <=lastAppBySystem[0].Appointment_Time__c){
                        appointmentStartTime = CheckNextAvailableTime(lastAppBySystem[0].Appointment_Time__c,apptype);
                    }else{
                        appointmentStartTime = dtNormal;
                    }
                    
                    Boolean isWithinDay= BusinessHours.isWithin(bh.id,appointmentStartTime);
                    if(isWithinDay){
                        appointmentStartTime = checklunchtime(appointmentStartTime,appType);
                        insertAppointmentRecord(appointmentStartTime,step,appType);
                    }else{
                        appointmentStartTime = appointmentStartTime.addMinutes(5);
                        CheckAppointmentNextDay(false,step,appointmentStartTime,appType);
                    }
                    
                }
            }
        }
    }
        catch(Exception ex){
            system.debug('--ex--'+ex);
            
            //MedicalAppointmentCreate(step);
            Log__c objLog = new Log__c();
            objLog.Type__c = 'Appointment Creation';
            objLog.Description__c = 'Exceptio is : ' + ex.getMessage()+'--Step Id--'+step.id+ '-nLine # --' + ex.getLineNumber();
            insert objLog;
        }
    }
    
    //******Check lunch time********************************//
    public static DateTime checklunchtime(DateTime currentAppointmentTime,string appType){
        
        DateTime newAppointmentDatetime;
        Date LunchDate = date.newInstance(currentAppointmentTime.year(),currentAppointmentTime.month(),currentAppointmentTime.Day());
        
        DateTime LunchStartTime = dateTime.newInstance(LunchDate, Time.newInstance(Integer.valueOf(string.valueof(Lunch_Time).split(':')[0]),Integer.valueof(string.valueof(Lunch_Time_Express).split(':')[1]), 0, 0));
        
        DateTime LunchEndTime = dateTime.newInstance(LunchDate, Time.newInstance(Integer.valueOf(string.valueof(LunchEnd_Time).split(':')[0]),Integer.valueof(string.valueof(LunchEnd_Time_Express).split(':')[1]), 0, 0));
        
        if(LunchStartTime < currentAppointmentTime && currentAppointmentTime < LunchEndTime){
            if(appType=='Express'){
                 newAppointmentDatetime = LunchEndTime.addMinutes(7);
            }else{
                 newAppointmentDatetime = LunchEndTime.addMinutes(mins);
            }
           
        }else{
            newAppointmentDatetime = currentAppointmentTime;
        }
      // system.debug('---Lunch Time-----'+currentAppointmentTime+'------'+LunchStartTime+'------'+LunchEndTime+'---'+newAppointmentDatetime+'---'+appType);
        return newAppointmentDatetime;
    }
    //******Insert Appointment record********************************//
    public static void insertAppointmentRecord(DateTime finalAppointmentTime,Step__c step,string appType){
        /********
        insertTemporaryAppointmentRecord(finalAppointmentTime,Step,appType);
        ***********/
    
        if(appType=='Express' || appType=='Normal'){
            Date nextNormalBusinessStartDate = Date.newinstance(finalAppointmentTime.year(), finalAppointmentTime.month(), finalAppointmentTime.day());
            system.debug('--oldappointmentscheulerLst--'+oldappointmentscheulerLst.size()+'----'+existingAppCalendarLst.values());
            system.debug('---'+finalAppointmentTime);
            
            if(oldappointmentscheulerLst.size()>0){
                
                /**
                if(existingAppCalendarLst.containsKey(string.valueof(finalAppointmentTime)) || existingAppCalendarLst.containsKey(string.valueof(finalAppointmentTime.addMinutes(5))) || existingAppCalendarLst.containsKey(string.valueof(finalAppointmentTime.addMinutes(7)))){ //added temp
                **/
                if(existingAppCalendarLst.containsKey(string.valueof(finalAppointmentTime.addMinutes(5)))){
                    for(Appointment_Schedule_Counter__c itr:oldappointmentscheulerLst){
                        
                        if(itr.Appointment_Time__c == finalAppointmentTime.addMinutes(5)&& (itr.Type__c!='RE-SCHEDULED')){
                            Boolean isWithinDay= BusinessHours.isWithin(bh.id, finalAppointmentTime);
                            if(isWithinDay){
                                finalAppointmentTime = CheckAppointmentSameday(isWithinDay,step,itr.Appointment_Time__c,appType);
                                break;
                            }else{
                                CheckAppointmentNextDay(isWithinDay,step,finalAppointmentTime,appType);
                                break;
                            }
                        }else if((itr.Appointment_Time__c == finalAppointmentTime.addMinutes(5)) && (itr.Type__c=='RE-SCHEDULED')){
                            isRescheduled = true;
                            
                            if(appType=='Express'){
                                finalAppointmentTime = itr.Appointment_Time__c;
                                break;
                            }else if(appType=='Normal'){
                                //finalAppointmentTime = oldappointmentscheulerLst[0].Appointment_Time__c;
                                finalAppointmentTime = itr.Appointment_Time__c;
                                break;
                            }
                            Boolean isWithinDay= BusinessHours.isWithin(bh.id, finalAppointmentTime);
                            system.debug('----finalAppointmentTime rescheduling---324---'+finalAppointmentTime);
                            if(isWithinDay){
                                finalAppointmentTime = CheckAppointmentSameday(isWithinDay,step,itr.Appointment_Time__c,appType);
                                break;
                            }else{
                                CheckAppointmentNextDay(isWithinDay,step,finalAppointmentTime,appType);
                                break;
                            }
                        }
                    }
                }
                
            }
        }
        system.debug('--finalAppointmentTime---'+finalAppointmentTime);
        Appntment.Appointment_StartDate_Time__c = finalAppointmentTime ;
        Appntment.Appointment_EndDate_Time__c = finalAppointmentTime.addMinutes(mins);
        Appntment.Step__c = step.id;
        Appntment.Service_Request__c = SR[0].id;
        Appntment.Subject__c = SR[0].name + '-' + SR[0].first_Name__c + '' + SR[0].Last_Name__c + '-' +appType;
       
        List < step__c > stp = [select id, Step_Code__c, Biometrics_Required__c from step__c where step_code__c = 'Emirates ID Registration is completed' and sr__r.id =: SR[0].id limit 1];
        if(stp.size()>0){
            Appntment.Bio_Metrics__c = stp[0].Biometrics_Required__c;
        }
         Database.SaveResult saveResultList;
        try{
            if (Appntment.Appointment_StartDate_Time__c != null) {
                isInserted = true;
                saveResultList = Database.insert(Appntment, true);
                system.debug('--Appntment--'+Appntment.Id);
            }
            if (saveResultList.isSuccess()) {
                updateScheduleCounter(Appntment,appType);
            }
        }catch(Exception ex){
            system.debug('--ex--'+ex);
            
            //MedicalAppointmentCreate(step);
            Log__c objLog = new Log__c();
            objLog.Type__c = 'Appointment Creation';
            objLog.Description__c = 'Exceptio is : ' + ex.getMessage()+'--Step Id--'+step.id+ '-nLine # --' + ex.getLineNumber();
            insert objLog;
        }
       
    }
    
    //*************Insert Scheduler counter record********************************//
    public static void updateScheduleCounter(Appointment__c Appntment,string appType){
    
        //update the scheduler counter when appointment created
        if(Appntment!=null){
            Appointment_Schedule_Counter__c appSchedule = new Appointment_Schedule_Counter__c();
            appSchedule.Appointment_Time__c = Appntment.Appointment_EndDate_Time__c;
            appSchedule.Application_Type__c = appType;
            appSchedule.Lunch_Duration__c = ExpressLunchDuration;
            appSchedule.Appointment__c = Appntment.id;
            Date LunchDate = date.newInstance(Appntment.Appointment_StartDate_Time__c.year(),Appntment.Appointment_StartDate_Time__c.month(),Appntment.Appointment_StartDate_Time__c.Day());
            DateTime LunchTime = dateTime.newInstance(LunchDate, Time.newInstance(Integer.valueOf(string.valueof(Lunch_Time).split(':')[0]),Integer.valueof(string.valueof(Lunch_Time_Express).split(':')[1]), 0, 0));
            appSchedule.Lunch_Time__c = LunchTime;
            system.debug('-------appSchedule-----'+appSchedule);
            if(appSchedule!=null){
                upsert appSchedule;
            }
        }
    }
    public static List<Appointment_Schedule_Counter__c> appointmentCalendarList (Datetime appTime,string appType){
    
        Date appDate = Date.newinstance(appTime.year(), appTime.month(), appTime.day());
        
        List<Appointment_Schedule_Counter__c> appointmentschedulerLst = [select id, Type__c,Appointment_Time__c, Lunch_Time__c, Lunch_Duration__c, Application_Type__c from Appointment_Schedule_Counter__c where DAY_ONLY(Appointment_Time__c) >=:appDate and Application_Type__c =:appType order by Appointment_Time__c DESC];
        
        for(Appointment_Schedule_Counter__c itr:appointmentschedulerLst){
            existingAppCalendarLst.put(string.valueof(itr.Appointment_Time__c),itr.Appointment_Time__c);
            if(itr.Type__c=='RE-SCHEDULED'){
                checkRescheduleOldList.put(itr.Appointment_Time__c,itr.Type__c);
            }
        }
        
        List < Appointment_Schedule_Counter__c > ListAppointmentCounternextDay = [select id, Appointment_Time__c, Lunch_Time__c, Lunch_Duration__c, Application_Type__c from Appointment_Schedule_Counter__c where DAY_ONLY(Appointment_Time__c) >=:appDate and Type__c!='RE-SCHEDULED' and Application_Type__c =:appType order by Appointment_Time__c DESC limit 1];
        
        if(ListAppointmentCounternextDay.size()>0){
            lastAppBySystem = ListAppointmentCounternextDay;
        }
        
        
        return appointmentschedulerLst;
    }
    
    public static Datetime CheckNextAvailableTime(Datetime derivedTime,string apptype){
        
        if(Test.isRunningTest()){
            existingAppCalendarLst.put(string.valueof(derivedTime.addMinutes(5)),derivedTime.addMinutes(5));
            checkRescheduleOldList.put(derivedTime.addMinutes(5),apptype);
        }
        Datetime nextAvailableSlot;
        Datetime nextDerivedTime;
       if(!existingAppCalendarLst.containsKey(string.valueof(derivedTime.addMinutes(5))) && !checkRescheduleOldList.containsKey(derivedTime.addMinutes(5)) && BusinessHours.isWithin(bh.id, derivedTime)){
            nextAvailableSlot = derivedTime;
        }else{
            nextAvailableSlot = derivedTime.addMinutes(5);
            Datetime StartDatetime = BusinessHours.nextStartDate(BH.Id,nextAvailableSlot);
            if(!BusinessHours.isWithin(bh.id, nextAvailableSlot)){
                Date nextNormalBusinessStartDate = Date.newinstance(StartDatetime.year(), StartDatetime.month(), StartDatetime.day());
                nextAvailableSlot = StartDatetime;
                
                if(apptype=='Express'){
                    if((checkRescheduleOldList.containsKey(nextAvailableSlot.addMinutes(2))==true) && (checkRescheduleOldList.containsKey(nextAvailableSlot.addMinutes(7)) ==false)){
                        nextAvailableSlot = nextAvailableSlot.addMinutes(7);
                    }
                }else if(apptype=='Normal'){
                    if((checkRescheduleOldList.containsKey(nextAvailableSlot) == true) && (checkRescheduleOldList.containsKey(nextAvailableSlot.addMinutes(5)))){
                        nextAvailableSlot = nextAvailableSlot.addMinutes(5);
                    }
                }
                if((nextAvailableSlot==StartDatetime) && (apptype =='Express')){
                        nextAvailableSlot = nextAvailableSlot.addMinutes(2);
                }
                if((nextAvailableSlot==StartDatetime) && (apptype =='Normal')){
                        nextAvailableSlot = nextAvailableSlot;
                }
            }
            else{
                CheckNextAvailableTime(nextAvailableSlot,apptype);
            }
        }
      
        return nextAvailableSlot;
    }
}