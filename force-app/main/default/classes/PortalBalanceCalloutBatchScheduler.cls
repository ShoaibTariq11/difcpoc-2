Global with sharing class PortalBalanceCalloutBatchScheduler implements Schedulable{
	
	public  Static string chronExp;
	Global void Execute(SchedulableContext sc){
		Database.ExecuteBatch(new  PortalBalanceCalloutBatch(),20);
	}
	
	Global static void startScheduler()
	{		
		if(chronExp !=null)
			System.Schedule('PortalBalanceCallOutBatch',chronExp,new PortalBalanceCalloutBatchScheduler());
	}
    
}