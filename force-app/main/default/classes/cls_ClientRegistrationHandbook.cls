/***********************************************************************************
 *  Author   : Swati
 *  Company  : NSI
 *  Date     : 27/04/2016
 *  Purpose  : This class is to populate client registration handbook 
  --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
V.No    Date        Updated By  Description
 ----------------------------------------------------------------------------------------              
V1.0    27/04/2016  Swati       Created
V1.1    10/04/2017  Claude      Added new logic for getting user guides (#3927)    
V1.2    07/12/2017  Arun        Added INTERMEDIATE HOLDING COMPANY document 
       
**********************************************************************************************/

public without sharing class cls_ClientRegistrationHandbook {
    
    public string strPageId {get;set;}    
    public String documentId {get;set;}
    public string strNavigatePageId{get;set;}
    public string pageTitle{get;set;}
    public string pageDescription{get;set;}
    public string strHiddenPageIds{get;set;}
    public string strActionId{get;set;}
    
    public String getSalesforceUrl() {
        return System.Url.getSalesforceBaseUrl().toExternalForm();
    }
    
    public map<string,string> mapParameters;
    
    service_request__c srObj;
    
    public cls_ClientRegistrationHandbook (){
        
        string srId = ApexPages.CurrentPage().GetParameters().Get('Id');   
        srObj = new service_request__c();
        document docObj = new document ();
        
        if(apexpages.currentPage().getParameters()!=null){
            mapParameters = apexpages.currentPage().getParameters();
        }
        
        if(mapParameters.get('FlowId')!=null && srId!=null){
            strPageId = mapParameters.get('PageId');
            if(strPageId!=null && strPageId!=''){
                for(Page__c page:[select id,Name,Page_Description__c from Page__c where Id=:strPageId]){
                    pageTitle = page.Name;
                    pageDescription = page.Page_Description__c;
                }
            }
        }
        
        strHiddenPageIds = PreparePageBlockUtil.getHiddenPageIds(mapParameters.get('FlowId'),srObj);
        
        if(string.isNotBlank(srId)){
            srObj = getRelatedSR(srId);
        }
        
        if(srObj!=null){
            if(srObj.Second_Language__c == 'General Partner'){
                srObj.Legal_Structures__c = 'LTD IC';
            }
            
            docObj = getRelatedHandbook (srObj.Legal_Structures__c);
            if(docObj!=null){
                documentId = docObj.id; 
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Handbook does not exit for the selected Legal Entity Type.'));
            }
        }
    }
    
    public service_request__c getRelatedSR (string srId){
        try{
            list<service_request__c> templistSR = [select id, Second_Language__c, Legal_Structures__c, 
                                                              Registration_Type__c, // V1.1 - Claude
                                                              Nature_of_business__c // V1.1 - Claude  
                                                              from service_request__c where id=:srId];
            if(templistSR!=null && !templistSR.isEmpty()){
                return  templistSR[0];
            }
        }catch(exception ex){
            system.debug('--ex---'+ex.getLineNumber()); 
        }
        return null;    
    }
    
    public document getRelatedHandbook (string legalStructure){
        try{
            list<folder> templistFolder = [select id from folder where DeveloperName = 'Client_Registration_Handbook'];
            if(templistFolder!=null && !templistFolder.isEmpty()){
                
                // V1.1 - Claude - Start
                /* Check for specific field values before checking legal structure */
                list<document> templistDoc;
                
                Set<String> representativeLegalStructs = new Set<String>{'FRC','RLP','RLLP','RP'};
                
                if(String.isNotBlank(srObj.Registration_Type__c) && srObj.Registration_Type__c.equals('Transfer')) 
                    templistDoc = [select id from document where Name='Transfer' and folderid IN : templistFolder];
                
                else if(String.isNotBlank(srObj.Nature_of_business__c) && 
                    srObj.Nature_of_business__c.contains('Representative Office') &&
                    representativeLegalStructs.contains(legalStructure))
                    templistDoc = [select id from document where Name='RepresentativeOffice' and folderid IN : templistFolder];
             //V1.2  
               else if(String.isNotBlank(srObj.Nature_of_business__c) &&  srObj.Nature_of_business__c.containsIgnoreCase('INTERMEDIATE HOLDING COMPANY'))
                    {
                        templistDoc = [select id from document where Name='Special_Purpose_Vehicle' and folderid IN : templistFolder];
                    }
                else 
                    templistDoc = [select id from document where Name=:legalStructure and folderid IN : templistFolder];
                
                // V1.1 - Claude - End
                
                if(templistDoc!=null && !templistDoc.isEmpty()) return  templistDoc[0];
            }
        }catch(exception ex){
            system.debug('--ex---'+ex.getLineNumber()); 
        }
        return null;
    }
    
    public Component.Apex.PageBlock getDyncPgMainPB(){
        PreparePageBlockUtil.FlowId = mapParameters.get('FlowId');
        PreparePageBlockUtil.PageId = mapParameters.get('PageId');
        PreparePageBlockUtil.objSR = srObj; 
        PreparePageBlockUtil objPB = new PreparePageBlockUtil();
        return objPB.getDyncPgMainPB();
    }

    public pagereference goTopage(){
        if(strNavigatePageId!=null && strNavigatePageId!=''){
            PreparePageBlockUtil objSidebarRef = new PreparePageBlockUtil();
            PreparePageBlockUtil.strSideBarPageId = strNavigatePageId;
            PreparePageBlockUtil.objSR = srObj;
            return objSidebarRef.getSideBarReference();
        }
        return null;
    }
    
    public pagereference DynamicButtonAction(){
        if(strActionId!=null && strActionId!=''){
            boolean isNext = false;
            boolean isAllowToproceed = false;
            for(Section_Detail__c objSec:[select id,Name,Component_Label__c,Navigation_Direction__c from Section_Detail__c where Id=:strActionId]){
                if(objSec.Navigation_Direction__c=='Forward'){
                    isNext = true;
                }else{
                    isNext = false;
                }
            }
            //if(isNext==true){
                PreparePageBlockUtil.FlowId = mapParameters.get('FlowId'); 
                PreparePageBlockUtil.PageId = mapParameters.get('PageId');
                PreparePageBlockUtil.objSR = srObj;
                PreparePageBlockUtil.ActionId = strActionId;
                PreparePageBlockUtil objPB = new PreparePageBlockUtil();
                pagereference pg = objPB.getButtonAction();
                return pg;
            //}
        }
        return null;
    }
}