public with sharing class OB_ConstantUtility {
 
 public static final RecordTypeConstantWrapper AMENDMENT_INDIVIDUAL_RT=new RecordTypeConstantWrapper('Individual','Individual');
    public static final RecordTypeConstantWrapper AMENDMENT_CORPORATE_RT=new RecordTypeConstantWrapper('Body Corporate','Body_Corporate');
    
    /*********************************************************************************************
    * @Description : Method to contain record type wrapper record.                               *
    *********************************************************************************************/
    public class RecordTypeConstantWrapper{
        public String name {get; private set;}
        public String developerName {get; private set;}
        public RecordTypeConstantWrapper(String name, String developerName){
            this.name = name;
            this.developerName = developerName;
        }
    }   
}