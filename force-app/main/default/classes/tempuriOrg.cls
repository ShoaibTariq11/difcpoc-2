public class tempuriOrg {
	
	public class BasicHttpBinding_IFZLicenseService {
	
		public String endpoint_x = Label.DSCDIFC_License_Endpoint_URL;
		public Map<String,String> inputHttpHeaders_x;
		public Map<String,String> outputHttpHeaders_x;
		public String clientCertName_x;
		public String clientCert_x;
		public String clientCertPasswd_x;
		public Integer timeout_x;
	
		private String[] ns_map_type_info = new String[]{'http://Services.dsc.gov.ae/FreezonesIntegration/Licenses/V01','servicesDscGovAeFreezonesintegratio'};

		public servicesDscGovAeFreezonesintegratio.ReplyMessage_element UpdateLicenses(servicesDscGovAeFreezonesintegratio.LicensesList_element request) {
	
			servicesDscGovAeFreezonesintegratio.LicensesList_element request_x = request;
			servicesDscGovAeFreezonesintegratio.ReplyMessage_element response_x;
			Map<String, servicesDscGovAeFreezonesintegratio.ReplyMessage_element> response_map_x = new Map<String, servicesDscGovAeFreezonesintegratio.ReplyMessage_element>();
			response_map_x.put('response_x', response_x);
			WebServiceCallout.invoke(
				this,
				request_x,
				response_map_x,
				new String[]{endpoint_x,
				'http://Services.dsc.gov.ae/FreezonesIntegration/Licenses/V01/IFZLicenseService/UpdateLicenses',
				'http://Services.dsc.gov.ae/FreezonesIntegration/Licenses/V01',
				'LicensesList',
				'http://Services.dsc.gov.ae/FreezonesIntegration/Licenses/V01',
				'ReplyMessage',
				'servicesDscGovAeFreezonesintegratio.ReplyMessage_element'}
			);
			response_x = response_map_x.get('response_x');
			return response_x;
		}		
	}
}