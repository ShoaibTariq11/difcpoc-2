/******************************************************************************************************************
    Author      :   Ravi
    --------------------------------------------------------------------------------------------------------------------------
	Modification History
 	--------------------------------------------------------------------------------------------------------------------------
	V.No    Date        Updated By    	Description
	--------------------------------------------------------------------------------------------------------------------------             
 	V1.1    20-03-2016	Ravi			Changed the Purpose of Notification Picklist value of DP - as per # 2532 
 	V1.2	14-08-2016	Sravan			Created a method that checks the owner as per Tkt # 3160
 	V1.3    02-01-2017  Sravan			Altered the conditions of the method created for V1.2 for TKT # 3160
*******************************************************************************************************************/
public without sharing class RocCreatePermits {
	public static string CreatePermits(Step__c objStep){
		Savepoint sp = Database.setSavepoint();
		try{	
			if(objStep.SR__r.Record_Type_Name__c == 'Application_of_Registration'){
				string result = CreateNewPermits(objStep,system.today(),system.today().addYears(1).addDays(-1));
				if(result != 'Success'){
					return result;
				}
			}else if(objStep.SR__r.Purpose_of_Notification__c != 'In relation to changing contact details only'){
				//End Date the Existing Permits
				Date StartDate,EndDate;
				//End Date the Existing Permits
				list<Permit__c> lstPermits = new list<Permit__c>();
				for(Permit__c objPermit : [select Id,Date_From__c,Date_To__c,Status__c from Permit__c where Account__c =: objStep.SR__r.Customer__c AND Date_From__c != null AND Date_To__c != null AND Status__c = 'Active']){
					StartDate = objPermit.Date_From__c.addYears(1);
					EndDate = objPermit.Date_To__c.addYears(1);
					objPermit.Status__c = 'Inactive';
					objPermit.Date_To__c = system.today();
					lstPermits.add(objPermit);
				}
				if(!lstPermits.isEmpty())
					update lstPermits;
				
				string result = CreateNewPermits(objStep,StartDate,EndDate);
				if(result != 'Success'){
					Database.rollback(sp);
					return result;
				}
			}
			
			//Data Controller Creation
			CreateDataControllerContact(objStep);
		}catch(Exception ex){
			system.debug('Exception is : '+ex.getMessage());
			Database.rollback(sp);
			return ''+ex.getMessage();
		}
		return 'Success';
	}
	
	public static string CreateDataControllerContact(Step__c objStep){
		if(objStep.SR__r.Purpose_of_Notification__c != null && objStep.SR__r.Purpose_of_Notification__c != ''){
			Contact objDPController = new Contact();
			Boolean isRelationShipFound = false;
			map<string,Contact> mapExistingContacts = new map<string,Contact>();
			
			Amendment__c objAmendment = new Amendment__c();
			list<Relationship__c> lstRels = new list<Relationship__c>();
			
			for(Amendment__c objAmd : [select Id,Name,Customer__c,Amendment_Type__c,First_Name__c,Last_Name__c,Address1__c,Address2__c,Address3__c,Building_Name__c,PO_Box__c, 
						Country_Of_Issue__c,Phone__c,Fax__c,Person_Email__c,Post_Code__c,Emirate_State__c,Mobile__c,Passport_No__c
						from Amendment__c where ServiceRequest__c=:objStep.SR__c AND Amendment_Type__c='Data Controller']){
				objAmendment = objAmd;
			}
			
			for(Relationship__c objRelationship : [select Id,Relationship_Type__c,Object_Contact__c,Object_Contact__r.Passport_No__c,Object_Contact__r.Country__c,End_Date__c,Active__c from Relationship__c where Active__c = true AND Relationship_Type__c='Data Controller' AND Subject_Account__c=:objStep.SR__r.Customer__c AND Object_Contact__r.RecordType.DeveloperName='Individual']){
				if(objRelationship.Object_Contact__r.Passport_No__c == objAmendment.Passport_No__c && objRelationship.Object_Contact__r.Country__c == objAmendment.Country_Of_Issue__c){
					isRelationShipFound = true;
					objDPController = new Contact(Id=objRelationship.Object_Contact__c);
				}else{
					//end date old Relationship and Create new one
					objRelationship.End_Date__c = system.today();
					objRelationship.Active__c = false;
					lstRels.add(objRelationship);
				}
			}
			
			for(Contact objContact : [select Id,AccountId,Passport_No__c,(select Id from Secondary_Contact__r where Relationship_Type__c='Data Controller') from Contact where Passport_No__c != null AND AccountId=:objStep.SR__r.Customer__c]){
				mapExistingContacts.put(objContact.Passport_No__c,objContact);
			}
			
			/*for(Amendment__c objAmd : [select Id,Name,Customer__c,Amendment_Type__c,First_Name__c,Last_Name__c,Address1__c,Address2__c,Address3__c,Building_Name__c,PO_Box__c, 
						Country_Of_Issue__c,Phone__c,Fax__c,Person_Email__c,Post_Code__c,Emirate_State__c,Mobile__c,Passport_No__c
						from Amendment__c where ServiceRequest__c=:objStep.SR__c AND Amendment_Type__c='Data Controller'])
			*/	
			if(objAmendment != null && objAmendment.Id != null){
				//objDPController = (mapExistingContacts.containsKey(objAmd.Passport_No__c))?new Contact(Id=mapExistingContacts.get(objAmd.Passport_No__c).Id):new Contact();
				objDPController.FirstName = objAmendment.First_Name__c;
				objDPController.LastName = objAmendment.Last_name__c;
				//objDPController.ADDRESS2_LINE1__c = objAmendment.Address1__c;
				//objDPController.ADDRESS2_LINE2__c = objAmendment.Address2__c+','+objAmendment.Building_Name__c+','+objAmendment.Emirate_State__c;
				string sAdr = objAmendment.Address1__c +' '+objAmendment.Address2__c;
				sAdr = sAdr.right(150);
				objDPController.ADDRESS_LINE2__c = sAdr;
				objDPController.ADDRESS_LINE1__c = objAmendment.Building_Name__c;
				objDPController.CITY1__c = objAmendment.Address3__c;
				objDPController.Emirate_State_Province__c = objAmendment.Emirate_State__c;
				objDPController.PO_Box_HC__c = objAmendment.PO_Box__c;
				objDPController.Postal_Code__c = objAmendment.Post_Code__c;
				objDPController.Country__c = objAmendment.Country_Of_Issue__c;
				objDPController.Phone = objAmendment.Phone__c;
				objDPController.Fax = objAmendment.Fax__c;
				objDPController.Email = objAmendment.Person_Email__c;
				objDPController.MobilePhone = objAmendment.Mobile__c;
				//objDPController.AccountId = objStep.SR__r.Customer__c;
				objDPController.Passport_No__c = objAmendment.Passport_No__c;
				objDPController.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Individual Contact').getRecordTypeId();
			}
			if(objDPController != null){
				upsert objDPController;
				if(isRelationShipFound == false){//if(mapExistingContacts.containsKey(objDPController.Passport_No__c) == false || (mapExistingContacts.containsKey(objDPController.Passport_No__c) && (mapExistingContacts.get(objDPController.Passport_No__c).Secondary_Contact__r == null || mapExistingContacts.get(objDPController.Passport_No__c).Secondary_Contact__r.size() == 0)))
					Relationship__c objRelationship = new Relationship__c();
					objRelationship.Active__c = true;
					objRelationship.Relationship_Type__c = 'Data Controller';
					objRelationship.Subject_Account__c = objStep.SR__r.Customer__c;
					objRelationship.Object_Contact__c = objDPController.Id;
					objRelationship.Start_Date__c = system.today();
					//insert objRelationship;
					lstRels.add(objRelationship);
				}
			}
			if(!lstRels.isEmpty()){
				upsert lstRels;
			}
		}
		return 'Success';
	}
	
	public static string CreateNewPermits(Step__c objStep,Date StartDate, Date EndDate){
		Service_Request__c objSR = new Service_Request__c();
		map<string,Permit__c> mapPermitsInsert = new map<string,Permit__c>();
		list<Data_Protection__c> lstDataProtectionsInsert = new list<Data_Protection__c>();
		License__c objLicense = new License__c();
		try{
			if(objStep.SR__r.Purpose_of_Notification__c != null && objStep.SR__r.Purpose_of_Notification__c != '' && objStep.SR__r.Purpose_of_Notification__c != 'To Inform the Commissioner of Data Protection that you do not Process Personal Data'){//v1.1
				// if Purpose of Noftification is any one of the above mentioned then only create the Permits.
				for(Service_Request__c obj : [select Id,Personal_Data_Processing_Procedure__c,Class_of_Personal_Data__c,How_will_Senstive_Personal_Data_Process__c,Description_of_Safeguards_SD__c from Service_Request__c where Id=:objStep.SR__c]){
					objSR = obj;
				}
				for(License__c objLic : [select Id,License_Issue_Date__c,License_Expiry_Date__c from License__c where Account__c =:objStep.SR__r.Customer__c AND License_Expiry_Date__c != null order by License_Expiry_Date__c desc ]){
					objLicense = objLic;
				}
				//for Permit type - 1
				Permit__c objPermit1 = new Permit__c();
				objPermit1.Name = 'Data Protection notification';
				objPermit1.Permit_Type__c = 'Notification to process personal data';
				objPermit1.Account__c = objStep.SR__r.Customer__c;
				objPermit1.Personal_Data_Processing_Procedure__c = objSR.Personal_Data_Processing_Procedure__c;
				objPermit1.Purpose_of_Processing_Data__c = objStep.SR__r.Purpose_of_Processing_Data__c;
				objPermit1.Other_Purpose_of_Processing_Data__c = objStep.SR__r.Other_Purpose_of_Processing_Data__c;
				objPermit1.Data_Subjects_Personal_Data__c = objStep.SR__r.Data_Subjects_Personal_Data__c;
				objPermit1.Other_Data_Subjects_Personal_Data__c = objStep.SR__r.Other_Data_Subjects_Personal_Data__c;
				objPermit1.Class_of_Personal_Data__c = objSR.Class_of_Personal_Data__c;
				objPermit1.Date_From__c = system.today();
				//objPermit1.Date_To__c = (EndDate != null)?EndDate:system.today().addYears(1).addDays(-1);
				objPermit1.Date_To__c = objLicense.License_Expiry_Date__c;
				objPermit1.License__c = objLicense.Id;
				objPermit1.Status__c = 'Active';
				/*if(objStep.SR__r.Processing_any_senstive_data__c == true && objStep.SR__r.In_accordance_with_Article_10_1__c == false){
					objPermit1.In_accordance_with_Article_10_1__c = false;
				}*/
				objPermit1.In_accordance_with_Article_10_1__c = objStep.SR__r.In_accordance_with_Article_10_1__c;
				
				//added on 12th Nov, as per new UAT testing, All the below fieds are related to Permit 1, if they are following Article 12 a, then only Permit 2 should apply
				if(objStep.SR__r.Transferring_Personal_Data_outside_DIFC__c){
					if(objStep.SR__r.In_Accordance_with_Article_11__c){
						objPermit1.In_Accordance_with_Article_11__c = objStep.SR__r.In_Accordance_with_Article_11__c;
						objPermit1.Recognized_Jurisdictions__c = objStep.SR__r.Recognized_Jurisdictions__c;
						if(objStep.SR__r.Recognized_Jurisdictions__c != null && objStep.SR__r.Recognized_Jurisdictions__c.indexOf('US Dept') >=0 ){
							objPermit1.Adhering_to_Safe_Horbour__c = objStep.SR__r.Adhering_to_Safe_Horbour__c;
						}
					}
					if(objStep.SR__r.In_Accordance_with_Article_12_1bj__c){
						objPermit1.In_Accordance_with_Article_12_1bj__c = objStep.SR__r.In_Accordance_with_Article_12_1bj__c;
						objPermit1.Provisions_of_Article_12__c = objStep.SR__r.Provisions_of_Article_12__c;
						
						for(Data_Protection__c objDP : [select Id,Address_Line_1__c,Address_Line_2__c,Assessed_the_Jurisdiction__c,City_Town__c,Country__c,Description_of_Safeguards__c,Email__c,Emirate_State_Province__c,Fax__c,IsActive__c,Is_Sensitive_Data__c,Jurisdiction__c,Mobile__c,Name_of_Jurisdiction__c,Person_Name__c,PO_Box__c,Post_Code__c,Telephone__c,Service_Request__c,Permit__c,RecordType.Name from Data_Protection__c where Service_Request__c=:objStep.SR__c AND Is_Sensitive_Data__c=false AND RecordType.Name='Jurisdiction']){
							Data_Protection__c objDPTemp = objDP.clone(false);
							objDPTemp.Jurisdiction__c = null;
							objDPTemp.Service_Request__c = null;
							lstDataProtectionsInsert.add(objDPTemp);
						}
					}
					if(objStep.SR__r.In_Accordance_with_Article_12_1a__c)
						objPermit1.In_Accordance_with_Article_12_1a__c = true;
				}
				mapPermitsInsert.put('Permit1',objPermit1);
				
				//for Permit type - 2
				if(objStep.SR__r.Transferring_Personal_Data_outside_DIFC__c && objStep.SR__r.In_Accordance_with_Article_12_1a__c){
					Permit__c objPermit2 = new Permit__c();
					objPermit2.Name = 'Permit to transfer personal data';
					objPermit2.Permit_Type__c = 'Permit to transfer personal data';
					objPermit2.Account__c = objStep.SR__r.Customer__c;
					/*
					The below fields are related to Permit 1
					if(objStep.SR__r.In_Accordance_with_Article_11__c){
						objPermit2.Recognized_Jurisdictions__c = objStep.SR__r.Recognized_Jurisdictions__c;
						if(objStep.SR__r.Recognized_Jurisdictions__c != null && objStep.SR__r.Recognized_Jurisdictions__c.indexOf('US Dept') >=0 ){
							objPermit2.Adhering_to_Safe_Horbour__c = objStep.SR__r.Adhering_to_Safe_Horbour__c;
						}
					}
					if(objStep.SR__r.In_Accordance_with_Article_12_1bj__c){
						objPermit2.Provisions_of_Article_12__c = objStep.SR__r.Provisions_of_Article_12__c;
						
						for(Data_Protection__c objDP : [select Id,Address_Line_1__c,Address_Line_2__c,Assessed_the_Jurisdiction__c,City_Town__c,Country__c,Description_of_Safeguards__c,Email__c,Emirate_State_Province__c,Fax__c,IsActive__c,Is_Sensitive_Data__c,Jurisdiction__c,Mobile__c,Name_of_Jurisdiction__c,Person_Name__c,PO_Box__c,Post_Code__c,Telephone__c,Service_Request__c,Permit__c,RecordType.Name from Data_Protection__c where Service_Request__c=:objStep.SR__c AND Is_Sensitive_Data__c=false AND RecordType.Name='Jurisdiction']){
							Data_Protection__c objDPTemp = objDP.clone(false);
							objDPTemp.Jurisdiction__c = null;
							objDPTemp.Service_Request__c = null;
							lstDataProtectionsInsert.add(objDPTemp);
						}
					}*/
					if(objStep.SR__r.In_Accordance_with_Article_12_1a__c){
						for(Data_Protection__c objDP : [select Id,Address_Line_1__c,Address_Line_2__c,Assessed_the_Jurisdiction__c,City_Town__c,Country__c,Description_of_Safeguards__c,Email__c,Emirate_State_Province__c,Fax__c,IsActive__c,Is_Sensitive_Data__c,Jurisdiction__c,Mobile__c,Name_of_Jurisdiction__c,Person_Name__c,PO_Box__c,Post_Code__c,Telephone__c,Service_Request__c,Permit__c,RecordType.Name from Data_Protection__c where Service_Request__c=:objStep.SR__c AND Is_Sensitive_Data__c=false AND RecordType.Name='Person']){
							Data_Protection__c objDPTemp = objDP.clone(false);
							objDPTemp.Jurisdiction__c = null;
							objDPTemp.Service_Request__c = null;
							lstDataProtectionsInsert.add(objDPTemp);
						}
					}
					if(objStep.SR__r.In_Accordance_with_Article_12_1a__c)
						objPermit2.In_Accordance_with_Article_12_1a__c = true;
						
					objPermit2.Date_From__c = system.today();
					//objPermit2.Date_To__c = system.today().addYears(1).addDays(-1);
					objPermit2.Date_To__c = objLicense.License_Expiry_Date__c;
					objPermit2.License__c = objLicense.Id;
					objPermit2.Status__c = 'Active';
					
					/*if(objStep.SR__r.Transferring_Personal_Data_outside_DIFC__c){
						objPermit2.Transferring_Personal_Data_Outside_DIFC__c = true;
						if(objStep.SR__r.In_Accordance_with_Article_11__c)
							objPermit2.In_Accordance_with_Article_11__c = true;
						if(objStep.SR__r.In_Accordance_with_Article_12_1a__c)
							objPermit2.In_Accordance_with_Article_12_1a__c = true;
						if(objStep.SR__r.In_Accordance_with_Article_12_1bj__c)
							objPermit2.In_Accordance_with_Article_12_1bj__c = true;
					}*/
					
					mapPermitsInsert.put('Permit2',objPermit2);					
				}
				
				//for Permit type - 3
				if(objStep.SR__r.Processing_any_senstive_data__c == true && objStep.SR__r.In_accordance_with_Article_10_1__c == false){
					Permit__c objPermit3 = new Permit__c();
					objPermit3.Name = 'Permit to process sensitive personal data';
					objPermit3.Permit_Type__c = 'Permit to process sensitive personal data';
					objPermit3.Account__c = objStep.SR__r.Customer__c;
					objPermit3.Nature_of_Sensitive_Personal_Data__c = objStep.SR__r.Nature_of_Senstive_Personal_Data__c;
					objPermit3.Purpose_of_Processing_Senstive_Data__c = objStep.SR__r.Purpose_of_Processing_Senstive_Data__c;
					objPermit3.Other_Purpose_of_Senstive_Data__c = objStep.SR__r.Other_Purpose_of_Senstive_Data__c;
					objPermit3.How_will_Senstive_Personal_Data_Process__c = objSR.How_will_Senstive_Personal_Data_Process__c;
					objPermit3.Data_Subjects_Senstive_Data__c = objStep.SR__r.Data_Subjects_Senstive_Data__c;
					objPermit3.Other_Data_Subjects_Senstive_Data__c = objStep.SR__r.Other_Data_Subjects_Senstive_Data__c;
					objPermit3.Description_of_Safeguards_SD__c = objSR.Description_of_Safeguards_SD__c;
					objPermit3.Date_From__c = system.today();
					//objPermit3.Date_To__c = system.today().addYears(1).addDays(-1);
					objPermit3.Date_To__c = objLicense.License_Expiry_Date__c;
					objPermit3.License__c = objLicense.Id;
					objPermit3.Status__c = 'Active';
					objPermit3.In_accordance_with_Article_10_1__c = objStep.SR__r.In_accordance_with_Article_10_1__c;
					mapPermitsInsert.put('Permit3',objPermit3);
					
					for(Data_Protection__c objDP : [select Id,Address_Line_1__c,Address_Line_2__c,Assessed_the_Jurisdiction__c,City_Town__c,Country__c,Description_of_Safeguards__c,Email__c,Emirate_State_Province__c,Fax__c,IsActive__c,Is_Sensitive_Data__c,Jurisdiction__c,Mobile__c,Name_of_Jurisdiction__c,Person_Name__c,PO_Box__c,Post_Code__c,Telephone__c,Service_Request__c,Permit__c,RecordType.Name from Data_Protection__c where Service_Request__c=:objStep.SR__c AND Is_Sensitive_Data__c=true AND RecordType.Name='Person']){
						Data_Protection__c objDPTemp = objDP.clone(false);
						objDPTemp.Jurisdiction__c = null;
						objDPTemp.Service_Request__c = null;
						lstDataProtectionsInsert.add(objDPTemp);
					}
				}
				
				if(!mapPermitsInsert.isEmpty()){
					insert mapPermitsInsert.values();
				}
				if(!lstDataProtectionsInsert.isEmpty()){
					for(Data_Protection__c objDP : lstDataProtectionsInsert){
						if(objDP.Is_Sensitive_Data__c == true){
							objDP.Permit__c = mapPermitsInsert.get('Permit3').Id;
						}else{
							if(objDP.RecordType.Name == 'Person')
								objDP.Permit__c = mapPermitsInsert.get('Permit2').Id;
							else
								objDP.Permit__c = mapPermitsInsert.get('Permit1').Id;
						}
					}
					insert lstDataProtectionsInsert;
				}
			}
			//added the if condition, if client opt for "To Inform the Commissioner of Data Protection that you do not Process Personal Data" they are not going to process any DP
			if(!mapPermitsInsert.isEmpty() || objStep.SR__r.Purpose_of_Notification__c == 'To Inform the Commissioner of Data Protection that you do not Process Personal Data'){ //V1.1
				//added on 5th Nov, when Permits got created should update the Account with what permits the customer have
				Account objAccount = new Account(Id=objStep.SR__r.Customer__c);
				objAccount.Has_Permit_1__c = false;
				objAccount.Has_Permit_2__c = false;
				objAccount.Has_Permit_3__c = false;
				if(!mapPermitsInsert.isEmpty()){
					for(string PermitType : mapPermitsInsert.keySet()){
						if(PermitType == 'Permit1')
							objAccount.Has_Permit_1__c = true;
						if(PermitType == 'Permit2')
							objAccount.Has_Permit_2__c = true;
						if(PermitType == 'Permit3')
							objAccount.Has_Permit_3__c = true;
					}
				}
				update objAccount;
			}
		}catch(Exception ex){
			return ex.getMessage();
		}
		return 'Success';
	}
	
	public static string CreatePermitsFromLicenseRenewal(Step__c objStep){
		map<string,Permit__c> mapExistingPermits = new map<string,Permit__c>();
		map<string,Permit__c> mapPermits = new map<string,Permit__c>();
		list<Data_Protection__c> lstDataProtectionsInsert = new list<Data_Protection__c>();
		try{
			//if(objStep.SR__r.No_Change_in_the_Data_Protection_Permit__c == true){
				for(Permit__c objPermit : [select Id,Name,Account__c,Date_From__c,Date_To__c,License__c,Status__c,Permit_Type__c,Personal_Data_Processing_Procedure__c,Purpose_of_Processing_Data__c,Other_Purpose_of_Processing_Data__c,Data_Subjects_Personal_Data__c,
							Other_Data_Subjects_Personal_Data__c,Class_of_Personal_Data__c,Recognized_Jurisdictions__c,Adhering_to_Safe_Horbour__c,Provisions_of_Article_12__c,
							Nature_of_Sensitive_Personal_Data__c,Purpose_of_Processing_Senstive_Data__c,Other_Purpose_of_Senstive_Data__c,How_will_Senstive_Personal_Data_Process__c,Data_Subjects_Senstive_Data__c,
							Other_Data_Subjects_Senstive_Data__c,Description_of_Safeguards_SD__c,
							(select Id,Address_Line_1__c,Address_Line_2__c,Assessed_the_Jurisdiction__c,City_Town__c,Country__c,Description_of_Safeguards__c,Email__c,Emirate_State_Province__c,Fax__c,IsActive__c,Is_Sensitive_Data__c,Jurisdiction__c,Mobile__c,Name_of_Jurisdiction__c,Person_Name__c,PO_Box__c,Post_Code__c,Telephone__c,
							Service_Request__c,Permit__c,RecordType.Name,RecordTypeId,Permit__r.Permit_Type__c from Data_Protections__r),
							License__r.License_Expiry_Date__c,In_accordance_with_Article_10_1__c,In_Accordance_with_Article_11__c,In_Accordance_with_Article_12_1a__c,In_Accordance_with_Article_12_1bj__c,Transferring_Personal_Data_Outside_DIFC__c
							from Permit__c where Account__c =:objStep.SR__r.Customer__c AND Status__c = 'Active']){
					
					Permit__c objPermitTemp = new Permit__c();
					objPermitTemp = objPermit.clone(false);
					objPermitTemp.Date_From__c = system.today();
					objPermitTemp.Date_To__c = objPermit.License__r.License_Expiry_Date__c;
					objPermitTemp.Status__c = 'Active';
					//end date the exiting Permits
					objPermit.Date_To__c = system.today();
					objPermit.Status__c = 'Inactive';
					if(objPermit.Permit_Type__c == 'Notification to process personal data'){
						//mapExistingPermits.put('Permit1',objPermit);
						mapPermits.put('Permit1',objPermitTemp);
						mapPermits.put('Permit1-Old',objPermit);
					}else if(objPermit.Permit_Type__c == 'Permit to transfer personal data'){
						//mapExistingPermits.put('Permit2',objPermit);
						mapPermits.put('Permit2',objPermitTemp);
						mapPermits.put('Permit2-Old',objPermit);
					}else if(objPermit.Permit_Type__c == 'Permit to process sensitive personal data'){
						//mapExistingPermits.put('Permit3',objPermit);
						mapPermits.put('Permit3',objPermitTemp);
						mapPermits.put('Permit3-Old',objPermit);
					}
					
					for(Data_Protection__c objDP : objPermit.Data_Protections__r){
						Data_Protection__c objDPTemp = objDP.Clone(false);
						lstDataProtectionsInsert.add(objDPTemp);
					}
					
				}
				
				if(!mapPermits.isEmpty()){
					upsert mapPermits.values();
					if(!lstDataProtectionsInsert.isEmpty()){
						for(Data_Protection__c objDP : lstDataProtectionsInsert){
							if(objDP.Is_Sensitive_Data__c == true || objDP.Permit__r.Permit_Type__c == 'Permit to process sensitive personal data'){
								objDP.Permit__c = mapPermits.get('Permit3').Id;
								objDP.Is_Sensitive_Data__c = true;
							}else{
								if(objDP.RecordType.Name == 'Person')
									objDP.Permit__c = mapPermits.get('Permit2').Id;
								else
									objDP.Permit__c = mapPermits.get('Permit1').Id;
								//objDP.Permit__c = mapPermits.get('Permit2').Id;
							}
						}
						insert lstDataProtectionsInsert;					
					}
				}
			//}
		}catch(Exception ex){
			return ''+ex.getMessage();
		}	
		return 'Success';
	}
	
	
	//V1.2
	
	public static string ChangeDPStepOwner(Step__C stepObj)
	{
		
		
		List<Group> rocGroupDetails = new List<Group>([select id,DeveloperName from Group where type ='Queue' and DeveloperName = 'RoC_Officer']);
		List<Step_Template__c> stpTempList = new List<Step_Template__c>([Select id,Name,Step_RecordType_API_Name__c from Step_Template__c where Step_RecordType_API_Name__c ='Review_and_Approve' limit 1]);
		
		if(stepObj !=null && (stepObj.SR__r.Purpose_of_Notification__c =='Prior to or immediately upon Personal Data Processing' || stepObj.SR__r.Purpose_of_Notification__c =='In relation to any changes related to the registrable particulars notified under the previous notification' || stepObj.SR__r.Purpose_of_Notification__c =='To Inform the Commissioner of Data Protection that you do not Process Personal Data' || stepObj.SR__r.Purpose_of_Notification__c =='In relation to changing contact details only')){
		
			if((!stepObj.SR__r.Transferring_Personal_Data_outside_DIFC__c && !stepObj.SR__r.Processing_any_senstive_data__c) || 
			  (stepObj.SR__r.Transferring_Personal_Data_outside_DIFC__c && (stepObj.SR__r.In_Accordance_with_Article_11__c || stepObj.SR__r.In_Accordance_with_Article_12_1bj__c) && !stepObj.SR__r.In_Accordance_with_Article_12_1a__c) ||
			  (stepObj.SR__r.Processing_any_senstive_data__c && stepObj.SR__r.In_accordance_with_Article_10_1__c) || stepObj.SR__r.Purpose_of_Notification__c =='To Inform the Commissioner of Data Protection that you do not Process Personal Data' || stepObj.SR__r.Purpose_of_Notification__c =='In relation to changing contact details only')		
			
			{
					if(rocGroupDetails !=null && rocGroupDetails.size()>0)
					{
						stepObj.ownerID = rocGroupDetails[0].id;
						stepObj.Sys_Step_OwnerId__c = rocGroupDetails[0].id;
					}
					if(stpTempList!=null && stpTempList.size()>0)
						stepObj.Step_Template__c = stpTempList[0].id;		
					try
					{
						Update stepObj;
					}Catch(Exception ex){
						return ex.getMessage();
					}	
					
			}
			
			
		}  
		
		return 'Success';
	}
		
}