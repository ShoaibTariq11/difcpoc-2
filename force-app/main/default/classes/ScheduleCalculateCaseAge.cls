/******************************************************************************************
 *  Author      : Kaavya Raghuram
 *  Company     : PWC
 *  Date        : 6 Mar 2018    
 *  Purpose     : Class for scheduling the calculation of case age for open cases         
*******************************************************************************************/

global class ScheduleCalculateCaseAge implements Schedulable {
    
    global void execute(SchedulableContext SC) {
        CLS_BatchCalculateCaseAge cls = new  CLS_BatchCalculateCaseAge();
        database.executebatch(cls,100);
    }
}