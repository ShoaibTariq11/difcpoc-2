/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=true)
private class TestGsCodeCoverageHelper {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Service_Request__c objSR;
        for(Service_Request__c obj : [select Id,Customer__c,Customer__r.BP_No__c from Service_Request__c where Customer__c != null AND Customer__r.BP_No__c != null AND Customer__r.ROC_Status__c='Active' limit 1]){
            objSR = obj;
        }
        if(objSR != null){
            GsUpgradeSRCls objGsUpgradeSRCls = new GsUpgradeSRCls();
            objGsUpgradeSRCls.ServiceRequest = objSR;
            
            Test.setMock(WebServiceMock.class, new TestAccountBalanceServiceCls());
            test.startTest();
            list<AccountBalenseService.ZSF_S_ACC_BAL> resActBals = new list<AccountBalenseService.ZSF_S_ACC_BAL>();
            AccountBalenseService.ZSF_S_ACC_BAL objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
            objActBal.KUNNR = objSR.Customer__r.BP_No__c;
            objActBal.UMSKZ = 'D';
            objActBal.WRBTR = '1234';
            resActBals.add(objActBal);
            objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
            objActBal.KUNNR = objSR.Customer__r.BP_No__c;
            objActBal.UMSKZ = 'H';
            objActBal.WRBTR = '1234';
            resActBals.add(objActBal);
            objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
            objActBal.KUNNR = objSR.Customer__r.BP_No__c;
            objActBal.UMSKZ = '9';
            objActBal.WRBTR = '1234';
            resActBals.add(objActBal);
            
            TestAccountBalanceServiceCls.resActBals = resActBals;
                    
            objGsUpgradeSRCls.AccountBalance();
        }
    }
    
    static testMethod void myUnitTest1() {
        SR_Price_Item__c objSRPriceItem;
        for(SR_Price_Item__c obj : [select Id,ServiceRequest__c,ServiceRequest__r.Customer__c,ServiceRequest__r.Customer__r.BP_No__c from SR_Price_Item__c where ServiceRequest__r.Customer__c != null AND ServiceRequest__r.Customer__r.BP_No__c != null AND ServiceRequest__r.Customer__r.ROC_Status__c='Active' AND Transaction_Id__c != null AND ServiceRequest__r.Record_Type_Name__c != 'Application_of_Registration' order by CreatedDate desc  limit 1 ]){
            objSRPriceItem = obj;
        }
        if(objSRPriceItem != null){
            Test.setMock(WebServiceMock.class, new TestGsCRMServiceMock());
            
            list<SAPGSWebServices.ZSF_S_GS_SERV_OP> lstGSItems = new list<SAPGSWebServices.ZSF_S_GS_SERV_OP>();
            SAPGSWebServices.ZSF_S_GS_SERV_OP objGSItem = new SAPGSWebServices.ZSF_S_GS_SERV_OP();
            objGSItem.ACCTP = 'P';
            objGSItem.BANFN = '50001234';
            objGSItem.VBELN = '05001234';
            objGSItem.BELNR = '1234567';
            objGSItem.WSTYP = 'SERV';
            objGSItem.MATNR = 'GO-00001';
            objGSItem.UNQNO = '12345675000215';
            objGSItem.SGUID = objSRPriceItem.Id;
            lstGSItems.add(objGSItem);
            TestGsCRMServiceMock.lstGSPriceItems = lstGSItems;
            try{
            GsUpgradeSRCls.UpgradePushToSAP(objSRPriceItem.Id);
            }catch(Exception ex){}
        }
    }
    
     static testMethod void GSUpgradeConfirmPayment() {
       SR_Price_Item__c objSRPriceItem = new SR_Price_Item__c();
        for(SR_Price_Item__c obj : [select Id,ServiceRequest__c,ServiceRequest__r.Customer__c,ServiceRequest__r.Customer__r.BP_No__c from SR_Price_Item__c where ServiceRequest__c != null AND Status__c = 'Added' AND Price__c > 0 limit 1]){
            objSRPriceItem = obj;
        }
        Service_Request__c objSR;
        for(Service_Request__c obj : [select Id,Name,Submitted_Date__c,Customer__c,External_Status_Name__c,SAP_Unique_No__c,Upgrade_Type__c,Express_Service__c,Customer__r.BP_No__c,SR_Template__c,Type_of_Request__c,SAP_MATNR__c,SAP_UMATN__c,Upgrade_Docs_Attached__c,Last_Date_of_Entry__c,Current_Visa_Status__c,Registered_EIA__c,Record_Type_Name__c,
                                          (select Id,Step_Name__c from Steps_SR__r where Step_Name__c =: Label.Entry_Permit_Step_Name AND Status_Code__c = 'CLOSED'),
                                          SR_Template__r.Is_Exit_Process__c,Portal_Service_Request_Name__c,Required_Docs_not_Uploaded__c,
                                          Avail_Courier_Services__c,Use_Registered_Address__c,Consignee_FName__c,Consignee_LName__c,Courier_Mobile_Number__c,Apt_or_Villa_No__c,Courier_Cell_Phone__c,
                                          IsGSLetter__c,SR_Group__c
                                          from Service_Request__c where Id=:objSRPriceItem.ServiceRequest__c LIMIT 1]){
                           	objSR =     obj;           	
                                          }
        if(objSRPriceItem != null){
              	Test.setMock(WebServiceMock.class, new TestServiceCreationCls());
        
			    list<SAPECCWebService.ZSF_S_RECE_SERV_OP> resItems = new list<SAPECCWebService.ZSF_S_RECE_SERV_OP>();
			    SAPECCWebService.ZSF_S_RECE_SERV_OP objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
			    objTemp.ACCTP = 'P';
			    objTemp.SGUID = objSRPriceItem.Id;
			    objTemp.SFMSG = 'Test Class';
			    objTemp.BELNR = '1234567';
			    resItems.add(objTemp);
			    
			    list<SAPECCWebService.ZSF_S_ACC_BAL> resActBals = new list<SAPECCWebService.ZSF_S_ACC_BAL>();
			    SAPECCWebService.ZSF_S_ACC_BAL objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
			    objActBal.KUNNR = '001234';
			    objActBal.UMSKZ = 'D';
			    objActBal.WRBTR = '1234';
			    resActBals.add(objActBal);
			    objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
			    objActBal.KUNNR = '001234';
			    objActBal.UMSKZ = 'H';
			    objActBal.WRBTR = '1234';
			    resActBals.add(objActBal);
			    objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
			    objActBal.KUNNR = '001234';
			    objActBal.UMSKZ = '9';
			    objActBal.WRBTR = '1234';
			    resActBals.add(objActBal);
			    
			    TestServiceCreationCls.resItems = resItems;
			    TestServiceCreationCls.resActBals = resActBals;
			    
			    GsUpgradeSRCls objGsUpgradeSRCls = new GsUpgradeSRCls(); 
			    objGsUpgradeSRCls.AvailablePortalBalance = 1000;
			    objGsUpgradeSRCls.AmountToCharge = 100;
			    objGsUpgradeSRCls.ServiceRequest = objSR;
			    
			    objGsUpgradeSRCls.ConfirmPayment();
			    
			    objGsUpgradeSRCls.CancelPayment();
			    
			    
			    
        }
    }
    
    static testMethod void GSUpgradePushCouriertoSAP() {
    	Service_Request__c objSR = new Service_Request__c();
       for(Service_Request__c obj : [select Id,Name,Submitted_Date__c,Customer__c,External_Status_Name__c,SAP_Unique_No__c,Upgrade_Type__c,Express_Service__c,Customer__r.BP_No__c,SR_Template__c,Type_of_Request__c,SAP_MATNR__c,SAP_UMATN__c,Upgrade_Docs_Attached__c,Last_Date_of_Entry__c,Current_Visa_Status__c,Registered_EIA__c,Record_Type_Name__c,
                                          (select Id,Step_Name__c from Steps_SR__r where Step_Name__c =: Label.Entry_Permit_Step_Name AND Status_Code__c = 'CLOSED'),
                                          SR_Template__r.Is_Exit_Process__c,Portal_Service_Request_Name__c,Required_Docs_not_Uploaded__c,
                                          Avail_Courier_Services__c,Use_Registered_Address__c,Consignee_FName__c,Consignee_LName__c,Courier_Mobile_Number__c,Apt_or_Villa_No__c,Courier_Cell_Phone__c,
                                          IsGSLetter__c,SR_Group__c
                                          from Service_Request__c where Record_Type_Name__c = 'DIFC_Sponsorship_Visa_New' AND isClosedStatus__c = false AND Pre_GoLive__c = false AND SR_Template__c != null limit 1  ]){
                objSR = obj;
                objSR.Avail_Courier_Services__c = 'Yes';
            }
        if(objSR != null){
              	GsUpgradeSRCls objGsUpgradeSRCls = new GsUpgradeSRCls();
              	objGsUpgradeSRCls.ServiceRequest = objSR;
              	objGsUpgradeSRCls.checkForCourierPrice(objSR);
			    
			    objGsUpgradeSRCls.CourierCheck();
			    objGsUpgradeSRCls.CourierAddressCheck();
				
				list<string> lst = new list<string>();
			    for(SR_Price_Item__c obj : [select Id,ServiceRequest__c,ServiceRequest__r.Customer__c,ServiceRequest__r.Customer__r.BP_No__c from SR_Price_Item__c where ServiceRequest__c != null AND Status__c = 'Added' AND Price__c > 0 AND Transaction_Id__c = null limit 1]){
		            lst.add(obj.Id);
		        }
							    
		        Test.setMock(WebServiceMock.class, new TestGsCRMServiceMock());
            
	            list<SAPGSWebServices.ZSF_S_GS_SERV_OP> lstGSItems = new list<SAPGSWebServices.ZSF_S_GS_SERV_OP>();
	            SAPGSWebServices.ZSF_S_GS_SERV_OP objGSItem = new SAPGSWebServices.ZSF_S_GS_SERV_OP();
	            objGSItem.ACCTP = 'P';
	            objGSItem.BANFN = '50001234';
	            objGSItem.VBELN = '05001234';
	            objGSItem.BELNR = '1234567';
	            objGSItem.WSTYP = 'SERV';
	            objGSItem.MATNR = 'GO-00001';
	            objGSItem.UNQNO = '12345675000215';
	            objGSItem.SGUID = lst[0];
	            objGSItem.SOPRP = 'P';
	            lstGSItems.add(objGSItem);
	            
	            objGSItem = new SAPGSWebServices.ZSF_S_GS_SERV_OP();
	            objGSItem.ACCTP = 'S';
	            objGSItem.BANFN = '50001234';
	            objGSItem.VBELN = '05001234';
	            objGSItem.BELNR = '1234567';
	            objGSItem.WSTYP = 'SERV';
	            objGSItem.MATNR = 'GO-00001';
	            objGSItem.UNQNO = '12345675000215';
	            objGSItem.SGUID = lst[0];
	            lstGSItems.add(objGSItem);
	            TestGsCRMServiceMock.lstGSPriceItems = lstGSItems;
            	
		        GsUpgradeSRCls.PushCouriertoSAP(lst);
                GsUpgradeSRCls objGsUpgradeSRCls1 = new GsUpgradeSRCls();
                objGsUpgradeSRCls1.PriceItemInfo();
              //  GsUpgradeSRCls objGsUpgradeSRCls = new GsUpgradeSRCls();
 				objGsUpgradeSRCls.ConfirmPayment();
        		objGsUpgradeSRCls.CancelPayment();
                objGsUpgradeSRCls1.UpgradeType = '';
                objGsUpgradeSRCls.ServiceRequest.Upgrade_Docs_Attached__c = false;
        		objGsUpgradeSRCls.UploadDocs();
 				objGsUpgradeSRCls.ConfirmUpgrade();
        }
    }
     static testMethod void GSUpgradePushCourier2() {
    	Service_Request__c objSR = new Service_Request__c();
       for(Service_Request__c obj : [select Id,Name,Submitted_Date__c,Customer__c,External_Status_Name__c,SAP_Unique_No__c,Upgrade_Type__c,Express_Service__c,Customer__r.BP_No__c,SR_Template__c,Type_of_Request__c,SAP_MATNR__c,SAP_UMATN__c,Upgrade_Docs_Attached__c,Last_Date_of_Entry__c,Current_Visa_Status__c,Registered_EIA__c,Record_Type_Name__c,
                                          (select Id,Step_Name__c from Steps_SR__r where Step_Name__c =: Label.Entry_Permit_Step_Name AND Status_Code__c = 'CLOSED'),
                                          SR_Template__r.Is_Exit_Process__c,Portal_Service_Request_Name__c,Required_Docs_not_Uploaded__c,
                                          Avail_Courier_Services__c,Use_Registered_Address__c,Consignee_FName__c,Consignee_LName__c,Courier_Mobile_Number__c,Apt_or_Villa_No__c,Courier_Cell_Phone__c,
                                          IsGSLetter__c,SR_Group__c
                                          from Service_Request__c where Record_Type_Name__c = 'DIFC_Sponsorship_Visa_New' AND isClosedStatus__c = false AND Pre_GoLive__c = false AND SR_Template__c != null limit 1  ]){
                objSR = obj;
                objSR.Avail_Courier_Services__c = 'Yes';
            }
        if(objSR != null){
            GsUpgradeSRCls thisGsUpgrade = new GsUpgradeSRCls();
            thisGsUpgrade.ServiceRequest = objSR;
            thisGsUpgrade.UpgradeType ='Cross';
            thisGsUpgrade.ServiceRequest.Upgrade_Docs_Attached__c = false;
            thisGsUpgrade.ServiceRequest.Visa_Expiry_Date__c = system.today().addDays(300);
            thisGsUpgrade.ServiceRequest.Passport_Date_of_Expiry__c = system.today().addDays(300);
            thisGsUpgrade.ServiceRequest.Last_Date_of_Entry__c = system.today().addDays(-100);
            thisGsUpgrade.ServiceRequest.Current_Visa_Status__c = 'Other';
            thisGsUpgrade.ServiceRequest.Avail_Courier_Services__c = 'No';
            thisGsUpgrade.ServiceRequest.Type_of_Request__c = 'Applicant Outside UAE';
            thisGsUpgrade.PriceItemInfo();
            /*thisGsUpgrade.CheckDocs();*/
            

        }
     }
    
}