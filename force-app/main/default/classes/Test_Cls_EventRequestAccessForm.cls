/******************************************************************************************
 *  Author   	: Claude Manahan
 *  Company  	: NSI JLT
 *  Description : Test Class for Cls_EventRequestAccessForm Class
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    22-06-2016	Claude		  Created
****************************************************************************************************************/
@isTest
private class Test_Cls_EventRequestAccessForm {

    @testSetup static void setTestData() {
    	
    	/* NOTE: THESE DETAILS DO NOT EXIST, AND ARE ONLY USED FOR TESTING */
        WebService_Details__c testCredentials = new WebService_Details__c();
        
        testCredentials.Name = 'Credentials';
        testCredentials.Password__c = 'thisistest12345678';
        testCredentials.SAP_User_Id__c = 'TEST_SAP';
        testCredentials.Username__c = 'TEST_SAP';
        
        insert testCredentials;
    }
    
    private static void createTestData(){
    	
    	insert Test_CC_FitOutCustomCode_DataFactory.getTestAccount();
		insert Test_CC_FitOutCustomCode_DataFactory.getTestBuilding();
		
		insert Test_CC_FitOutCustomCode_DataFactory.getCountryLookupList();
		insert Test_CC_FitOutCustomCode_DataFactory.getTestCountryCodes();
		
		/*
         * Create the required SR Status records
         */
        SR_Status__c testSubmittedStatus = Test_CC_FitOutCustomCode_DataFactory.getTestSRStatus('Submitted','SUBMITTED');
        SR_Status__c testDraftStatus = Test_CC_FitOutCustomCode_DataFactory.getTestSRStatus('Draft','DRAFT');
       
       	Status__c testStepStatus = new Status__c(Name='Approved',Code__c='APPROVED');
		
		insert testStepStatus;
        
		insert new List<SR_Status__c>{testSubmittedStatus,testDraftStatus};
    }
    
    static TestMethod void testRequestAccessEventsForm(){
    	
    	createTestData();
		Test_EventServiceRequestUtils.hasContractor = true;
		Test_EventServiceRequestUtils.hasValidation = false;
		Test_EventServiceRequestUtils.testRequestAccessEventsForm();
	}
	
	static TestMethod void testRequestAccessEventsFormWithValidation(){
		
		createTestData();
		Test_EventServiceRequestUtils.hasContractor = true;
		Test_EventServiceRequestUtils.hasValidation = true;
		Test_EventServiceRequestUtils.testRequestAccessEventsForm();
	}
}