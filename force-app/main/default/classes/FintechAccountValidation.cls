/*************************************************************************************************
 *  Name        : FintechAccountValidation
 *  Author      : Shoaib Tariq
 *  Company     : Tech Carrot
 *  Date        : 2020-02-03     
 *  Purpose     : This class is used to validate the fintech accounts.    
*/
public class FintechAccountValidation {
    
    public static Boolean isValidFintech(Service_Request__c thisServiceRequest){
        system.debug('thisServiceRequest###'+thisServiceRequest);
        Boolean isValidFintech = False;
        Service_Request__c thisSr = [SELECT Id,
                                     		Customer__r.FinTech_Classification__c,
                                            Customer__r.ROC_reg_incorp_Date__c
                                     FROM Service_Request__c
                                     WHERE Id=:thisServiceRequest.id];
        
        if(thisSr.Customer__r.FinTech_Classification__c == 'FinTech'
            	&& thisSr.Customer__r.ROC_reg_incorp_Date__c != null){
                
               Integer difference = Integer.valueOf(System.Today().monthsBetween(thisSr.Customer__r.ROC_reg_incorp_Date__c) / 12); 
                
                if(difference > 4){
                    isValidFintech = True;
                }  
            
        }
		return isValidFintech;        
    }

}