/**
*Author : Merul Shah
*Description : Display existing detail of Share Allocation module..
**/
public without sharing class OB_ShareAllocationDetailController
{
    
    
    @AuraEnabled   
    public static ResponseWrapper  addShareClass(String reqWrapPram)
    {
         //reqest wrpper
         RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);
         //response wrpper
         ResponseWrapper respWrap = new ResponseWrapper();
         AmendmentWrapper amedWrap = reqWrap.amedWrap;
         String customerId = amedWrap.amedObj.ServiceRequest__r.HexaBPM__customer__c;
         //system.debug('######## amedWrap.shrDetailWrapLst before '+amedWrap.shrDetailWrapLst.size());
         
         
         ShareholderDetailWrapper shrDetailWrap = new ShareholderDetailWrapper();
         shrDetailWrap.shareHolderDetObj  = new Shareholder_Detail__c();
         shrDetailWrap.shareHolderDetObj.OB_Amendment__c = amedWrap.amedObj.Id;
         shrDetailWrap.shareHolderDetObj.Account__c = customerId;
         shrDetailWrap.shareHolderDetObj.Status__c= 'Draft';
         shrDetailWrap.amedObj = amedWrap.amedObj;
         //respWrap.shrDetailWrapNew = shrDetailWrap;
         
         amedWrap.shrDetailWrapLst.add(shrDetailWrap);
         
         system.debug('######## amedWrap.shrDetailWrapLst after '+amedWrap.shrDetailWrapLst.size());
         respWrap.amedWrap = amedWrap;
         return respWrap;
         
    }
    
   
    
    public class AmendmentWrapper 
    {
        @AuraEnabled public HexaBPM_Amendment__c amedObj { get; set; }
        @AuraEnabled public Boolean isIndividual { get; set; }
        @AuraEnabled public Boolean isBodyCorporate { get; set; }
        @AuraEnabled public String lookupLabel { get; set; }
        @AuraEnabled public List<ShareholderDetailWrapper> shrDetailWrapLst{get;set;}
      
       
        
        public AmendmentWrapper()
        {}
    }
    
    
   public class AccountShareDetailWrapper 
    {
        @AuraEnabled public Account_Share_Detail__c accShrDetObj { get; set; }
        public AccountShareDetailWrapper()
        {}
    }
    
    
    public class ShareholderDetailWrapper 
    {
        @AuraEnabled public Shareholder_Detail__c shareHolderDetObj { get; set; }
        @AuraEnabled public HexaBPM_Amendment__c amedObj { get; set; }
        
        public ShareholderDetailWrapper()
        {}
    }
    
    
    public class RequestWrapper 
    {
        @AuraEnabled public Id flowId { get; set; }
        @AuraEnabled public Id pageId {get;set;}
        @AuraEnabled public Id srId { get; set; }
        @AuraEnabled public AmendmentWrapper amedWrap{get;set;}
        @AuraEnabled public String amendmentID{get;set;}
        @AuraEnabled public List<ShareholderDetailWrapper> shrDetailWrapLst{get;set;}
      
        
        public RequestWrapper()
        {}
    }

    public class ResponseWrapper 
    {
        //@AuraEnabled public SRWrapper  srWrap{get;set;}
        @AuraEnabled public AmendmentWrapper  amedWrap{get;set;}
        //@AuraEnabled public ShareholderDetailWrapper shrDetailWrapNew {get;set;}
        //AmendmentWrapper 
        
        //@AuraEnabled public List<AccountShareDetailWrapper> accShrDetailWrapLst{get;set;}
        //@AuraEnabled public List<ShareholderDetailWrapper> shrDetailWrapLst{get;set;}
        
        
        
        
        //@AuraEnabled 
        //public HexaBPM__Section_Detail__c ButtonSection { get; set; }
        
        public ResponseWrapper()
        {}
    }
    
}