/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestGsPendingInfo {

    static testMethod void myUnitTest() {
            Courier_Client_Info__c CCInfo = new Courier_Client_Info__c();
        CCInfo.Name = 'Courier Delivery';
        CCInfo.Username__c = 'abc@test.com';
        CCInfo.Password__c = 'abcd1234';
        CCInfo.Version_No__c = 'v1.0';
        CCInfo.Account_No__c = '1234';
        CCInfo.Account_Pin__c = '1234';
        CCInfo.Account_Entity__c = 'DXB';
        CCInfo.Account_Country_Code__c = 'AE';
        CCInfo.Person_Name__c = 'Test';
        CCInfo.Company_Name__c = 'DIFC';
        CCInfo.Phone_Number__c = '+9711234567';
        CCInfo.Cell_Phone__c = '+9711234567';
        CCInfo.Email_Address__c = 'test@test.com';
        insert CCInfo;
        
        // TO DO: implement unit test
        list<Endpoint_URLs__c> lstCS = new list<Endpoint_URLs__c>();
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS ECC';
        objEP.URL__c = 'http://GSECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://ECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'AccountBal';
        objEP.URL__c = 'http://AccountBal/sampledata';
        lstCS.add(objEP);
        insert lstCS;
                
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        list<Country_Codes__c> lstCC = new list<Country_Codes__c>();
        Country_Codes__c objCode = new Country_Codes__c();
        objCode.Name = 'India';
        objCode.Code__c = 'IN';
        objCode.Nationality__c = 'India';
        lstCC.add(objCode);
        insert lstCC;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new Status__c(Name='Posted',Code__c='POSTED'));
        lstStatus.add(new Status__c(Name='PENDING_CUSTOMER_INPUTS',Code__c='PENDING_CUSTOMER_INPUTS'));
        lstStatus.add(new Status__c(Name='Closed',Code__c='Closed'));
        insert lstStatus;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        lstSRStatus.add(new SR_Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstSRStatus.add(new SR_Status__c(Name='Posted',Code__c='POSTED'));
        lstSRStatus.add(new SR_Status__c(Name='Pending',Code__c='Pending'));
        insert lstSRStatus;
        
        list<Step_Template__c> lstStepTemplate = new list<Step_Template__c>();
        Step_Template__c objStepTemplate = new Step_Template__c();
        objStepTemplate.Code__c = 'REQUIRE_MORE_INFORMATION_FROM_CUSTOMER';
        objStepTemplate.Name = 'REQUIRE_MORE_INFORMATION_FROM_CUSTOMER';
        objStepTemplate.Step_RecordType_API_Name__c = 'General';
        lstStepTemplate.add(objStepTemplate);
        objStepTemplate = new Step_Template__c();
        objStepTemplate.Code__c = 'PENDING_CUSTOMER_INPUTS';
        objStepTemplate.Name = 'PENDING_CUSTOMER_INPUTS';
        objStepTemplate.Step_RecordType_API_Name__c = 'General';
        lstStepTemplate.add(objStepTemplate);
        insert lstStepTemplate;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DIFC_Sponsorship_Visa_New','Employment_Visa_from_DIFC_to_DIFC')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = mapRecordTypeIds.get('DIFC_Sponsorship_Visa_New');
        //objSR.Send_SMS_To_Mobile__c = '+97152123456';
        objSR.Email__c = 'testclass@difc.ae.test';
        objSR.Type_of_Request__c = 'Applicant Outside UAE';
        objSR.Port_of_Entry__c = 'Dubai International Airport';
        objSR.Title__c = 'Mr.';
        objSR.First_Name__c = 'India';
        objSR.Last_Name__c = 'Hyderabad';
        objSR.Middle_Name__c = 'Andhra';
        objSR.Nationality_list__c = 'India';
        objSR.Previous_Nationality__c = 'India';
        objSR.Qualification__c = 'B.A. LAW';
        objSR.Gender__c = 'Male';
        objSR.Date_of_Birth__c = Date.newInstance(1989,1,24);
        objSR.Place_of_Birth__c = 'Hyderabad';
        objSR.Country_of_Birth__c = 'India';
        objSR.Passport_Number__c = 'ABC12345';
        objSR.Passport_Type__c = 'Normal';
        objSR.Passport_Place_of_Issue__c = 'Hyderabad';
        objSR.Passport_Country_of_Issue__c = 'India';
        objSR.Passport_Date_of_Issue__c = system.today().addYears(-1);
        objSR.Passport_Date_of_Expiry__c = system.today().addYears(2);
        objSR.Religion__c = 'Hindus';
        objSR.Marital_Status__c = 'Single';
        objSR.First_Language__c = 'English';
        objSR.Email_Address__c = 'applicant@newdifc.test';
        objSR.Mother_Full_Name__c = 'Parvathi';
        objSR.Monthly_Basic_Salary__c = 10000;
        objSR.Monthly_Accommodation__c = 4000;
        objSR.Other_Monthly_Allowances__c = 2000;
        objSR.Emirate__c = 'Dubai';
        objSR.City__c = 'Deira';
        objSR.Area__c = 'Air Port';
        objSR.Street_Address__c = 'Dubai';
        objSR.Building_Name__c = 'The Gate';
        objSR.PO_BOX__c = '123456';
        objSR.Residence_Phone_No__c = '+97152123456';
        objSR.Work_Phone__c = '+97152123456';
        objSR.Domicile_list__c = 'India';
        objSR.Phone_No_Outside_UAE__c = '+97152123456';
        objSR.City_Town__c = 'Hyderabad';
        objSR.Address_Details__c = 'Banjara Hills, Hyderabad';
        objSR.Statement_of_Undertaking__c = true;
        objSR.Internal_SR_Status__c = lstSRStatus[1].Id;
        objSR.External_SR_Status__c = lstSRStatus[1].Id;
        objSR.Avail_Courier_Services__c = 'Yes';
        objSR.Consignee_FName__c = 'Test';
        objSR.Consignee_LName__c = 'Test';
        objSR.Courier_Mobile_Number__c = '+97152123456';
        objSR.Mobile_Number__c = '+97152123456';
        objSR.Use_Registered_Address__c = true;
        objSR.Courier_Cell_Phone__c = '+97152123456';
        insert objSR;
        
        objSR.Internal_SR_Status__c = lstSRStatus[1].Id;
        objSR.External_SR_Status__c = lstSRStatus[1].Id;
        update objSR;
        
        /*list<SR_Price_Item__c> lstPriceItems = new list<SR_Price_Item__c>();
        SR_Price_Item__c objItem;
        
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = objSR.Id;
        objItem.Price__c = 3210;
        objItem.Pricing_Line__c = lstPLs[0].Id;
        objItem.Product__c = lstProducts[0].Id;
        lstPriceItems.add(objItem);
        
        insert lstPriceItems;*/
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        objStep.SAP_Seq__c = '0010';
        insert objStep;
        
        test.startTest();
        
        Test.setMock(WebServiceMock.class, new TestGsCRMServiceMock());
        
        //Test Data Preparation for Next Activity
        list<SAPGSWebServices.ZSF_S_GS_NEXT_ACT> lstNextActivity = new list<SAPGSWebServices.ZSF_S_GS_NEXT_ACT>();
        SAPGSWebServices.ZSF_S_GS_NEXT_ACT objNextAct = new SAPGSWebServices.ZSF_S_GS_NEXT_ACT();
        objNextAct.ACCTP = 'P';
        objNextAct.ACRES = 'BO';
        objNextAct.UNQNO = '12345675000215';
        objNextAct.IDSDT = '2016-01-01';
        objNextAct.IDEDT = '2016-01-01';
        objNextAct.BANFN = '50001234';
        objNextAct.MATNR = 'GO-00001';
        objNextAct.UNQNO = '12345675000215';
        objNextAct.SEQNR = '0010';
        objNextAct.ACTVT = 'Under Process';
        lstNextActivity.add(objNextAct);
        objNextAct = new SAPGSWebServices.ZSF_S_GS_NEXT_ACT();
        objNextAct.ACCTP = 'P';
        objNextAct.ACRES = 'BO';
        objNextAct.UNQNO = '12345675000215';
        objNextAct.IDSDT = '2016-01-01';
        objNextAct.IDEDT = '2016-01-01';
        objNextAct.BANFN = '50001234';
        objNextAct.MATNR = 'GO-00001';
        objNextAct.UNQNO = '12345675000215';
        objNextAct.SEQNR = '0020';
        objNextAct.ACTVT = 'Online Entry permit typed';
        lstNextActivity.add(objNextAct);
        
        TestGsCRMServiceMock.lstNextActivity = lstNextActivity;
        
        Apexpages.currentPage().getParameters().put('SRId',objSR.Id);
        
        Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(new Pending__c());
        
        GsPendingInfo objGsPendingInfo = new GsPendingInfo(con);
        objGsPendingInfo.PendingDetails.Start_Date__c = system.now();
        objGsPendingInfo.PendingDetails.Is_Courier_Related__c = true;
        objGsPendingInfo.PendingDetails.Type_Of_Courier__c = 'Collection with Return';
        //try{
            objGsPendingInfo.SavePending();
        //}catch(Exception ex){}
        objGsPendingInfo.PendingDetails.Start_Date__c = system.now();
        objGsPendingInfo.PendingDetails.Remove_Pending__c = true;
        //try{
            objGsPendingInfo.SavePending();
        //}catch(Exception ex){}
        test.stopTest();
        
        Pending__c objPending = new Pending__c();
        objPending.Service_request__c = objSR.Id;
        objPending.Step__c = objStep.Id;
        objPending.Start_Date__c = system.now();
        objPending.Comments__c = 'Test COmments';
        insert objPending;
        
        GsPendingInfo.CreateCustomerStep(ObjPending, 'Test Account');
        
        Apexpages.currentPage().getParameters().put('id',objPending.Id);
        Apexpages.currentPage().getParameters().put('SRId',null);
        
        objGsPendingInfo = new GsPendingInfo(con);
        
        objPending.End_Date__c = system.now();
        objPending.Remove_Pending__c = true;
        update objPending;
        
        /*objPending = new Pending__c();
        objPending.Service_request__c = objSR.Id;
        objPending.Step__c = objStep.Id;
        objPending.Start_Date__c = system.now();
        objPending.Comments__c = 'Test COmments';
        objPending.Is_Courier_Related__c = true;
        insert objPending;
        
        objPending.End_Date__c = system.now();
        objPending.Remove_Pending__c = true;
        update objPending;
        */
        objGsPendingInfo.doAction();
        GsPendingInfo.SAPDateFormat(system.today());
        GsPendingInfo.SFDCDateFormat('2016-12-08');
        GsPendingInfo.PendingKeyFormat(system.now());
        GsPendingInfo.SAPHANADateFormat('20161208');
        GsPendingInfo.SAPHANATimeFormat('101010');
            
    }
    
     static testMethod void myUnitTest1() {
             Courier_Client_Info__c CCInfo = new Courier_Client_Info__c();
        CCInfo.Name = 'Courier Delivery';
        CCInfo.Username__c = 'abc@test.com';
        CCInfo.Password__c = 'abcd1234';
        CCInfo.Version_No__c = 'v1.0';
        CCInfo.Account_No__c = '1234';
        CCInfo.Account_Pin__c = '1234';
        CCInfo.Account_Entity__c = 'DXB';
        CCInfo.Account_Country_Code__c = 'AE';
        CCInfo.Person_Name__c = 'Test';
        CCInfo.Company_Name__c = 'DIFC';
        CCInfo.Phone_Number__c = '+9711234567';
        CCInfo.Cell_Phone__c = '+9711234567';
        CCInfo.Email_Address__c = 'test@test.com';
        insert CCInfo;
        // TO DO: implement unit test
        list<Endpoint_URLs__c> lstCS = new list<Endpoint_URLs__c>();
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS ECC';
        objEP.URL__c = 'http://GSECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://ECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'AccountBal';
        objEP.URL__c = 'http://AccountBal/sampledata';
        lstCS.add(objEP);
        insert lstCS;
                
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        list<Country_Codes__c> lstCC = new list<Country_Codes__c>();
        Country_Codes__c objCode = new Country_Codes__c();
        objCode.Name = 'India';
        objCode.Code__c = 'IN';
        objCode.Nationality__c = 'India';
        lstCC.add(objCode);
        insert lstCC;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new Status__c(Name='Posted',Code__c='POSTED'));
        lstStatus.add(new Status__c(Name='PENDING_CUSTOMER_INPUTS',Code__c='PENDING_CUSTOMER_INPUTS'));
        lstStatus.add(new Status__c(Name='Closed',Code__c='Closed'));
        insert lstStatus;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        lstSRStatus.add(new SR_Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstSRStatus.add(new SR_Status__c(Name='Posted',Code__c='POSTED'));
        lstSRStatus.add(new SR_Status__c(Name='Pending',Code__c='Pending'));
        insert lstSRStatus;
        
        list<Step_Template__c> lstStepTemplate = new list<Step_Template__c>();
        Step_Template__c objStepTemplate = new Step_Template__c();
        objStepTemplate.Code__c = 'REQUIRE_MORE_INFORMATION_FROM_CUSTOMER';
        objStepTemplate.Name = 'REQUIRE_MORE_INFORMATION_FROM_CUSTOMER';
        objStepTemplate.Step_RecordType_API_Name__c = 'General';
        lstStepTemplate.add(objStepTemplate);
        objStepTemplate = new Step_Template__c();
        objStepTemplate.Code__c = 'PENDING_CUSTOMER_INPUTS';
        objStepTemplate.Name = 'PENDING_CUSTOMER_INPUTS';
        objStepTemplate.Step_RecordType_API_Name__c = 'General';
        lstStepTemplate.add(objStepTemplate);
        insert lstStepTemplate;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DIFC_Sponsorship_Visa_New','Employment_Visa_from_DIFC_to_DIFC')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = mapRecordTypeIds.get('DIFC_Sponsorship_Visa_New');
        //objSR.Send_SMS_To_Mobile__c = '+97152123456';
        objSR.Email__c = 'testclass@difc.ae.test';
        objSR.Type_of_Request__c = 'Applicant Outside UAE';
        objSR.Port_of_Entry__c = 'Dubai International Airport';
        objSR.Title__c = 'Mr.';
        objSR.First_Name__c = 'India';
        objSR.Last_Name__c = 'Hyderabad';
        objSR.Middle_Name__c = 'Andhra';
        objSR.Nationality_list__c = 'India';
        objSR.Previous_Nationality__c = 'India';
        objSR.Qualification__c = 'B.A. LAW';
        objSR.Gender__c = 'Male';
        objSR.Date_of_Birth__c = Date.newInstance(1989,1,24);
        objSR.Place_of_Birth__c = 'Hyderabad';
        objSR.Country_of_Birth__c = 'India';
        objSR.Passport_Number__c = 'ABC12345';
        objSR.Passport_Type__c = 'Normal';
        objSR.Passport_Place_of_Issue__c = 'Hyderabad';
        objSR.Passport_Country_of_Issue__c = 'India';
        objSR.Passport_Date_of_Issue__c = system.today().addYears(-1);
        objSR.Passport_Date_of_Expiry__c = system.today().addYears(2);
        objSR.Religion__c = 'Hindus';
        objSR.Marital_Status__c = 'Single';
        objSR.First_Language__c = 'English';
        objSR.Email_Address__c = 'applicant@newdifc.test';
        objSR.Mother_Full_Name__c = 'Parvathi';
        objSR.Monthly_Basic_Salary__c = 10000;
        objSR.Monthly_Accommodation__c = 4000;
        objSR.Other_Monthly_Allowances__c = 2000;
        objSR.Emirate__c = 'Dubai';
        objSR.City__c = 'Deira';
        objSR.Area__c = 'Air Port';
        objSR.Street_Address__c = 'Dubai';
        objSR.Building_Name__c = 'The Gate';
        objSR.PO_BOX__c = '123456';
        objSR.Residence_Phone_No__c = '+97152123456';
        objSR.Work_Phone__c = '+97152123456';
        objSR.Domicile_list__c = 'India';
        objSR.Phone_No_Outside_UAE__c = '+97152123456';
        objSR.City_Town__c = 'Hyderabad';
        objSR.Address_Details__c = 'Banjara Hills, Hyderabad';
        objSR.Statement_of_Undertaking__c = true;
        objSR.Internal_SR_Status__c = lstSRStatus[1].Id;
        objSR.External_SR_Status__c = lstSRStatus[1].Id;
        objSR.Avail_Courier_Services__c = 'Yes';
        objSR.Consignee_FName__c = 'Test';
        objSR.Consignee_LName__c = 'Test';
        objSR.Courier_Mobile_Number__c = '+97152123456';
        objSR.Mobile_Number__c = '+97152123456';
        objSR.Use_Registered_Address__c = true;
        objSR.Courier_Cell_Phone__c = '+97152123456';
        insert objSR;
        
        objSR.Internal_SR_Status__c = lstSRStatus[1].Id;
        objSR.External_SR_Status__c = lstSRStatus[1].Id;
        update objSR;
        
        /*list<SR_Price_Item__c> lstPriceItems = new list<SR_Price_Item__c>();
        SR_Price_Item__c objItem;
        
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = objSR.Id;
        objItem.Price__c = 3210;
        objItem.Pricing_Line__c = lstPLs[0].Id;
        objItem.Product__c = lstProducts[0].Id;
        lstPriceItems.add(objItem);
        
        insert lstPriceItems;*/
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        objStep.SAP_Seq__c = '0010';
        insert objStep;
        
        test.startTest();
        
        Test.setMock(WebServiceMock.class, new TestGsCRMServiceMock());
        
        //Test Data Preparation for Next Activity
        list<SAPGSWebServices.ZSF_S_GS_NEXT_ACT> lstNextActivity = new list<SAPGSWebServices.ZSF_S_GS_NEXT_ACT>();
        SAPGSWebServices.ZSF_S_GS_NEXT_ACT objNextAct = new SAPGSWebServices.ZSF_S_GS_NEXT_ACT();
        objNextAct.ACCTP = 'P';
        objNextAct.ACRES = 'BO';
        objNextAct.UNQNO = '12345675000215';
        objNextAct.IDSDT = '2016-01-01';
        objNextAct.IDEDT = '2016-01-01';
        objNextAct.BANFN = '50001234';
        objNextAct.MATNR = 'GO-00001';
        objNextAct.UNQNO = '12345675000215';
        objNextAct.SEQNR = '0010';
        objNextAct.ACTVT = 'Under Process';
        lstNextActivity.add(objNextAct);
        objNextAct = new SAPGSWebServices.ZSF_S_GS_NEXT_ACT();
        objNextAct.ACCTP = 'P';
        objNextAct.ACRES = 'BO';
        objNextAct.UNQNO = '12345675000215';
        objNextAct.IDSDT = '2016-01-01';
        objNextAct.IDEDT = '2016-01-01';
        objNextAct.BANFN = '50001234';
        objNextAct.MATNR = 'GO-00001';
        objNextAct.UNQNO = '12345675000215';
        objNextAct.SEQNR = '0020';
        objNextAct.ACTVT = 'Online Entry permit typed';
        lstNextActivity.add(objNextAct);
        
        TestGsCRMServiceMock.lstNextActivity = lstNextActivity;
        
        Apexpages.currentPage().getParameters().put('SRId',objSR.Id);
        
        Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(new Pending__c());
        
        GsPendingInfo objGsPendingInfo = new GsPendingInfo(con);
        objGsPendingInfo.PendingDetails.Start_Date__c = system.now();
        objGsPendingInfo.PendingDetails.Is_Courier_Related__c = true;
        objGsPendingInfo.PendingDetails.Type_Of_Courier__c = 'Collection with Return';
        //try{
            objGsPendingInfo.SavePending();
        //}catch(Exception ex){}
        objGsPendingInfo.PendingDetails.Start_Date__c = system.now();
        objGsPendingInfo.PendingDetails.Remove_Pending__c = true;
        //try{
            objGsPendingInfo.SavePending();
        //}catch(Exception ex){}
        test.stopTest();
        
        Pending__c objPending = new Pending__c();
        objPending.Service_request__c = objSR.Id;
        objPending.Step__c = objStep.Id;
        objPending.Start_Date__c = system.now();
        objPending.Comments__c = 'Test COmments';
        objPending.Is_Courier_Related__c = true;
        insert objPending;
        
        GsPendingInfo.CreateCustomerStep(ObjPending, 'Test Account');
        
        Apexpages.currentPage().getParameters().put('id',objPending.Id);
        Apexpages.currentPage().getParameters().put('SRId',null);
        
        objGsPendingInfo = new GsPendingInfo(con);
        
        objPending.End_Date__c = system.now();
        objPending.Remove_Pending__c = true;
        update objPending;
        
        
        objGsPendingInfo.doAction();
        GsPendingInfo.SAPDateFormat(system.today());
        GsPendingInfo.SFDCDateFormat('2016-12-08');
        GsPendingInfo.PendingKeyFormat(system.now());
        GsPendingInfo.SAPHANADateFormat('20161208');
        GsPendingInfo.SAPHANATimeFormat('101010');
            
    }
}