public without sharing class GSAccessCardStatusUpdate {

    @InvocableMethod(Label = 'GSAccessCardStatusUpdate'
        description = 'GSAccessCardStatusUpdate')
    public static void GSAccessCardStatusUpdateMethod(List < id > ids) {
        GSAccessCardStatusUpdateFuture(ids);
    }

    @future(callout = true)
    public static void GSAccessCardStatusUpdateFuture(List < id > ids) {

        if (ids.size() > 0) {
        	Boolean updateSRStatusToProcessCompleted = DIFCApexCodeUtility.checkIfSRStatusToUpdate(ids);
            List < Service_request__c > SRList = new List < Service_request__c > ();
            List < SR_Status__c > SRstatus = [select id from SR_Status__c where code__c = 'Process Completed' limit 1];
            
            List<string> RecordTypes = new List<string>();

            List<Access_Card_Services__c>  AccessCardServices =  Access_Card_Services__c.getAll().values();
            
            for(Access_Card_Services__c acs : AccessCardServices ){
            
               RecordTypes.add(acs.Record_Type_Name__c);
            }
            
            for (step__c step: [select id, sr__c, step_name__c, SR__r.Record_Type_Name__c from step__c where id in: ids]) {
                Service_request__c SR = new Service_request__c();
                if (step.step_name__c == 'Completed' &&  (!RecordTypes.contains(step.SR__r.Record_Type_Name__c))) {
                    SR.id = step.sr__c;SR.External_SR_Status__c = SRstatus[0].id;SR.Internal_SR_Status__c = SRstatus[0].id;SRList.add(SR);
                } else if (step.step_name__c == 'Access Card' && RecordTypes.contains(step.SR__r.Record_Type_Name__c)){
                    SR.id = step.sr__c;
                    SR.External_SR_Status__c = SRstatus[0].id;
                    SR.Internal_SR_Status__c = SRstatus[0].id;
                    SRList.add(SR);
                }else if(updateSRStatusToProcessCompleted){
                	SR.id = step.sr__c;SR.External_SR_Status__c = SRstatus[0].id;SR.Internal_SR_Status__c = SRstatus[0].id;SRList.add(SR);
                }
            }
            if(SRList.size()>0)
            update SRList;
        }
    }
}