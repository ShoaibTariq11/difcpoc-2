public without sharing class OB_LeaseSpaceController {
    
    @AuraEnabled
    public static RespondWrap getLeadInfo(String requestWrapParam)  {
    
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
        
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);

        // query user information
		for(User usr :[SELECT AccountId from User where Id = :UserInfo.getUserId()]) {
			respWrap.accId = usr.AccountId;
        }
        
        
        list<lead> createdLeadList= new list<lead>();
        for(lead leadObj : [SELECT id from lead where OB_Account__c =: respWrap.accId ]){
            createdLeadList.add(leadObj);
            respWrap.createdLead = true;
        }
        return respWrap;
    }
        
        
    // ------------ Wrapper List ----------- //
        
    public class RequestWrap{
    
        @AuraEnabled public String userId;                                                                                                                                                                                                                                             public RequestWrap(){}
    }
        
    public class RespondWrap{
    
        @AuraEnabled public string accId;
        @AuraEnabled public boolean createdLead = false;                                                                                                                                                                                                                                             public RespondWrap(){}
    }
}