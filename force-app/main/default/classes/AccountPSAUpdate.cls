global with sharing class AccountPSAUpdate implements Database.Batchable < sObject > ,Database.AllowsCallouts, Schedulable {
    /*
    * Batch used for the dummy update of the reports
    */
    public static final string tempStringValue = Label.AccountTempFieldValue;
    global void execute(SchedulableContext ctx) {
        AccountPSAUpdate bachable = new AccountPSAUpdate();
        database.executeBatch(bachable,1);
    }

    String query = '';
    global Database.QueryLocator start(Database.BatchableContext bc) {
    	query = 'Select id,name,BP_NO__c,Temp__c,TempField__c from Account where temp__c!=\''+tempStringValue+'\' AND ROC_Status__c in  (\'Active\',\'Not Renewed\') AND RecordTypeId NOT IN (\'012200000003Ezl\',\'012200000003Ezm\')';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List < Account > scope) {
        for (Account acc: scope) { //Service_Request__c
            String str = getAccountBalance(acc.BP_NO__c);
            List<String> str2 = str.contains('PSA Available :') ? str.split('PSA Available : ') : new List<String> {str};
			acc.TempField__c = str2.size() > 1 ?  str2[1].split('Employment Quota')[0] : str2[0];
			acc.temp__c = tempStringValue;
			system.debug('Account -------'+acc);
            update acc;
        }
    }
    global void finish(Database.BatchableContext BC) {
		
    }
    
    public static String getAccountBalance(String BP_Number){
        String accBalance = '';
        if(Test.isRunningTest()){accBalance = 'Available Portal balance is : AED 27600.00  PSA Available : 0.0  Employment Quota 9';}
        else accBalance =  AccountBalanceInfoCls.getPortalBalance(BP_Number);
        return accBalance;
    }
}