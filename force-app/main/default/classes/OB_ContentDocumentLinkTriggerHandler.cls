/******************************************************************************************
 *  Name        : OB_ContentDocumentLinkTriggerHandler 
 *  Author      : Durga Kandula
 *  Company     : PwC
 *  Date        : 06-Nov-2019
 *  Description : Trigger Handler for ContentDocumentLinkTrigger to populate
                  Sys Doc Id and status on HexaBPM SR Doc
 ----------------------------------------------------------------------------------------                               
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   06-Nov-2019  Durga         Initial Version
 V1.1   06-Nov-2019  prateek       added is future check
*******************************************************************************************/
public without sharing class OB_ContentDocumentLinkTriggerHandler {
    public static void OnBeforeInsert(list<ContentDocumentLink> TriggerNew){
        
    }   
    public static void onAfterInsert(list<ContentDocumentLink> TriggerNew){
        //UpdateSRDoc(TriggerNew);
        map<string,string> MapContentDocumentLink_CD_Id = new map<string,string>();
        for(ContentDocumentLink CDL:TriggerNew){
            //SetDocumentIds.add(CDL.ContentDocumentId);
            if(Schema.HexaBPM__SR_Doc__c.SObjectType == CDL.LinkedEntityId.getSobjectType())
                MapContentDocumentLink_CD_Id.put(CDL.Id,CDL.ContentDocumentId);
        }
        //V1.1
        if(MapContentDocumentLink_CD_Id.size()>0 && !System.isFuture())
            UpdateSRDoc(MapContentDocumentLink_CD_Id);
    }        
    public static void OnBeforeUpdate(list<ContentDocumentLink> TriggerOld,list<ContentDocumentLink> TriggerNew,map<Id,ContentDocumentLink> TriggerOldMap,map<Id,ContentDocumentLink> TriggerNewMap){
      
    }
    public static void OnAfterUpdate(list<ContentDocumentLink> TriggerOld,list<ContentDocumentLink> TriggerNew,map<Id,ContentDocumentLink> TriggerOldMap,map<Id,ContentDocumentLink> TriggerNewMap){
      
    }
    
    @future
    public static void UpdateSRDoc(map<string,string> TriggerNew){
        if(TriggerNew!=null && TriggerNew.size()>0){
            map<string,HexaBPM__SR_Doc__c> MapSRDocsTBU = new map<string,HexaBPM__SR_Doc__c>();
            map<string,string> MapContentDocumentTitle = new map<string,string>();
            
            for(ContentDocumentLink objCDL:[Select Id,ContentDocument.Title,ContentDocumentId from ContentDocumentLink where ContentDocumentId IN:TriggerNew.values()]){
                MapContentDocumentTitle.put(objCDL.ContentDocumentId,objCDL.ContentDocument.Title);
            }
            system.debug('MapContentDocumentTitle===>' + MapContentDocumentTitle);
            list<ContentDocumentLink> lstCDL = new list<ContentDocumentLink>();
            set<string> setSRDocIds = new set<string>();
            for(ContentDocumentLink CDL:[Select Id,LinkedEntityId,ContentDocumentId from ContentDocumentLink where Id IN:TriggerNew.keyset()]){
                if(Schema.HexaBPM__SR_Doc__c.SObjectType == CDL.LinkedEntityId.getSobjectType()){
                    setSRDocIds.add(CDL.LinkedEntityId);
                    lstCDL.add(CDL);
                }
            }
            map<string,HexaBPM__SR_Doc__c> MapSRDocs = new map<string,HexaBPM__SR_Doc__c>();
            if(setSRDocIds.size()>0){
                for(HexaBPM__SR_Doc__c SRDoc:[Select Id,HexaBPM__Sys_IsGenerated_Doc__c from HexaBPM__SR_Doc__c where Id IN:setSRDocIds]){
                    MapSRDocs.put(SRDoc.Id,SRDoc);
                }
            }
            for(ContentDocumentLink CDL:lstCDL){
                system.debug('CDL===>'+CDL.LinkedEntityId);
                system.debug('HexaBPMsSRDoc===>'+Schema.HexaBPM__SR_Doc__c.SObjectType);
                system.debug('Parent Type===>'+CDL.LinkedEntityId.getSobjectType());
                if(Schema.HexaBPM__SR_Doc__c.SObjectType == CDL.LinkedEntityId.getSobjectType()){
                    HexaBPM__SR_Doc__c objSRDoc = new HexaBPM__SR_Doc__c(Id=CDL.LinkedEntityId);
                    if(MapSRDocs.get(CDL.LinkedEntityId)!=null && MapSRDocs.get(CDL.LinkedEntityId).HexaBPM__Sys_IsGenerated_Doc__c)
                        objSRDoc.HexaBPM__Status__c = 'Generated';
                    else
                        objSRDoc.HexaBPM__Status__c = 'Uploaded';
                    objSRDoc.HexaBPM__Doc_ID__c = CDL.ContentDocumentId;
                    objSRDoc.File_Name__c = MapContentDocumentTitle.get(CDL.ContentDocumentId);
                    MapSRDocsTBU.put(CDL.LinkedEntityId,objSRDoc);
                }
            }
            if(MapSRDocsTBU.size()>0){
                try{
                    update MapSRDocsTBU.values();
                }catch(DMLException e){
                    
                }
            }
        }
    }
}