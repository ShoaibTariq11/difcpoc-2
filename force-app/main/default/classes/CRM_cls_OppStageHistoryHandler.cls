/******************************************************************************************
 *  Name        : CRM_cls_OpportunityHandler 
 *  Author      : Claude Manahan
 *  Company     : PWC Digital Services
 *  Date        : 2017-2-3
 *  Description : Handler class for Opportunity transactions for CRM
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   02-03-2017   Claude        Created
*******************************************************************************************/
public class CRM_cls_OppStageHistoryHandler extends Cls_RecordHandler {

    private static CRM_cls_OppStageHistoryHandler handlerInstance;
    
    private CRM_cls_OppStageHistoryHandler(){
        
        
        
    }
    
    public static CRM_cls_OppStageHistoryHandler getInstance(){
        
        if(handlerInstance == null) handlerInstance = new CRM_cls_OppStageHistoryHandler();
        
        return handlerInstance;
    }
    
    /**
     * Handles all Insert DML Transactions
     */
    public override void handleInsert(){
        
        if(isBefore){
            
             CRM_cls_OpportunityUtils.updatePreviousStageHistory((List<Opportunity_Stage_History__c>) newRecords, new Set<Id>());
            
		}
        
    }
    
    /**
     * Handles all Update DML Transactions
     */
    public override void handleUpdate(){
        // TODO
        if(isAfter){ 
            
             CRM_cls_OpportunityUtils.updatePreviousStageHistory((List<Opportunity_Stage_History__c>) newRecords, oldRecordsMap.keyset());
            
		}
    }
    
    /**
     * Handles all Update DML Transactions
     */
    public override void handleDelete(){
        // TODO
    }
    
    /**
     * Handles all Update DML Transactions
     */
    public override void handleUndelete(){
        // TODO
    }
    
}