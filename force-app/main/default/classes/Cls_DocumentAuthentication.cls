/*****************************************************************************************************************************
    Name        :   Cls_DocumentAuthentication
    Author      :   Durga
    Date        :   27-09-2016
    Description :   This class contains the controller for the document output vf page used for Document Verification
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    --------------------------------------------------------------------------------------------------------------------------             
    
*****************************************************************************************************************************/
global without sharing class Cls_DocumentAuthentication{
    public transient string AttachValBase64{get;set;}
    public transient blob AttachBlobVal{get;set;}
    public string AtchContentType{get;set;}
    public Cls_DocumentAuthentication(){
        string ReferenceNumber = '';
        if(apexpages.currentPage().getparameters().get('ref')!=null && apexpages.currentPage().getparameters().get('ref')!='')
            ReferenceNumber = apexpages.currentPage().getparameters().get('ref');
        
        if(ReferenceNumber!=null && ReferenceNumber!=''){
            list<SR_Doc__c> lstSRDoc = [select id,Name,Doc_ID__c,Status__c,Document_Validity__c from SR_Doc__c where Document_No__c=:ReferenceNumber];
            if(lstSRDoc!= null && lstSRDoc.size() == 1){
                for(Attachment attch:[Select Id,Body,ContentType from Attachment where Id=:lstSRDoc[0].Doc_ID__c]){
                    AttachBlobVal = attch.Body;
                    AtchContentType = attch.ContentType;
                    AttachValBase64 = EncodingUtil.Base64Encode(attch.body);
                }
            }
        }
        if(AttachValBase64==null)
            AttachValBase64 = '0';
    }
    public string searchDoc{get;set;}
    
}