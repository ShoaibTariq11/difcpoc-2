@IsTest
private class TrainingRequestComponentControllerTest {
    static testMethod void validateTrainingRequest() {
        Training_Master__c trainingMaster = new Training_Master__c();
        trainingMaster.Name = 'Employee Service training';
        trainingMaster.Category__c = 'Employee Services';
        trainingMaster.Available_Seats__c = 10;
        trainingMaster.Status__c = 'Approved by HOD';
        trainingMaster.Start_DateTime__c = DateTime.now().addDays(1);
        trainingMaster.To_DateTime__c = DateTime.now().addDays(2);
        insert trainingMaster;
        TrainingRequestComponentController tCntrl = new TrainingRequestComponentController();
        tCntrl.serviceType = 'Employee Services';
        tCntrl.retrieveTrainingAvailable();
    }

}