/**************************************************************************************************
* Name               : CC_CreateCommPermission                                                             
* Description        : CC Code to create Commercial Permission record.                                        
* Created Date       : 23rd March 2020                                                                *
* Created By         : Leeba Shibu (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version    Author    Date           Comment                                                                *
  
**************************************************************************************************/

global without sharing class CC_CreateCommPermission implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
         try{
             if(stp!=null && stp.HexaBPM__SR__c!=null){
                Commercial_Permission__c objCP = new Commercial_Permission__c();
                objCP.Account__c = stp.HexaBPM__SR__r.HexaBPM__Customer__c;
                objCP.Application__c = stp.HexaBPM__SR__c;
                objCP.Status__c = 'Active';
                objCP.Start_Date__c = system.today();
                if(stp.HexaBPM__SR__r.Duration_days__c!=null){
                    Integer days =  Integer.valueOf(stp.HexaBPM__SR__r.Duration_days__c);
                    objCP.End_Date__c = system.today()+days;
                }
                else if(stp.HexaBPM__SR__r.Duration__c!= null){                        
                           
                        if(stp.HexaBPM__SR__r.Duration__c == 'Event - Up to 3 days')
                             objCP.End_Date__c = system.today()+3;
                        if(stp.HexaBPM__SR__r.Duration__c == 'Event - Up to 1 week')
                            objCP.End_Date__c = system.today()+7;
                        if(stp.HexaBPM__SR__r.Duration__c == 'Event - Up to 1 month')
                            objCP.End_Date__c = system.today()+30;
                        if(stp.HexaBPM__SR__r.Duration__c == 'Event - Up to 3 months')
                            objCP.End_Date__c = system.today()+90;
                        if(stp.HexaBPM__SR__r.Duration__c == 'One year')
                            objCP.End_Date__c = system.today()+365;
               }
                                                   
                insert objCP;
            }
        }catch(Exception e){
            strResult = e.getMessage()+'';
        
        }
        
        return strResult;
    }
    
}