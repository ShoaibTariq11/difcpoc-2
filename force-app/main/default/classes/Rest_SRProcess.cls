/*
	Author 		: Durga Prasad
	Date		: 25-Sep-2014
	Description : This is an REST API class which will returns the SR Process records.
	              This class exists in the target where user want to fetch the process 

*/
@RestResource(urlMapping='/Fetch_SR_Process/*')
global without sharing class Rest_SRProcess {
	@HttpPost
    global static SRProcessWrap GetSRProcess(String SRTemplateName) {
    	system.debug('SRTemplateName==>'+SRTemplateName);
    	SRProcessWrap objWrap = new SRProcessWrap();
    	if(SRTemplateName!=null && SRTemplateName!=''){
    		string SRTemplateId = '';
    		string srTemplateQuery = getAccessibleFieldsSoql('SR_Template__c','');
    		if(srTemplateQuery!=null && srTemplateQuery!=''){
    			srTemplateQuery += ' where SR_RecordType_API_Name__c=:SRTemplateName';
    			system.debug('srTemplateQuery===>'+srTemplateQuery);
    			for(SR_Template__c srTempl:Database.query(srTemplateQuery)){
    				objWrap.SRtemplate = srTempl;
    				SRTemplateId = srTempl.Id;
    			}
    			if(SRTemplateId!=null && SRTemplateId!=''){
    				
    				string srSRStepQuery = getAccessibleFieldsSoql('SR_Steps__c','');
    				srSRStepQuery += ' where SR_Template__c=:SRTemplateId order by Step_No__c';
	    			list<SR_Steps__c> lstSteps = new list<SR_Steps__c>();
	    			set<id> setSRStepIds = new set<id>();
	    			for(SR_Steps__c stp:Database.query(srSRStepQuery)){
	    				lstSteps.add(stp);
	    				setSRStepIds.add(stp.Id);
	    			}
	    			objWrap.lstSRSteps = lstSteps;
	    			//get template items whihc are linked to this sr templates
	    			
	    			string TemplItemQuery = getAccessibleFieldsSoql('SR_Template_Item__c','');
    				TemplItemQuery += ' where SR_Template__c=:SRTemplateId';
	    			list<SR_Template_Item__c> lstTemplItems = new list<SR_Template_Item__c>();
	    			for(SR_Template_Item__c TemplItm:Database.query(TemplItemQuery)){
	    				lstTemplItems.add(TemplItm);
	    			}
	    			objWrap.lstSRTemplateItems = lstTemplItems;
	    			
	    			set<Id> setDocIds = new set<Id>();
	    			string TemplDocQuery = getAccessibleFieldsSoql('SR_Template_Docs__c','');
    				TemplDocQuery += ' where SR_Template__c=:SRTemplateId';
	    			list<SR_Template_Docs__c> lstDocs = new list<SR_Template_Docs__c>();
	    			//get sr tempalte docs whihc are linked to this sr templates
	    			for(SR_Template_Docs__c Doc:Database.query(TemplDocQuery)){
	    				lstDocs.add(Doc);
	    				setDocIds.add(Doc.Id);
	    			}
	    			objWrap.lstSRTemplDocs = lstDocs;
	    			
	    			if(setDocIds!=null && setDocIds.size()>0){
	    				string DocCondQuery = getAccessibleFieldsSoql('Condition__c','');
	    				DocCondQuery += ' where SR_Template_Docs__c IN:setDocIds';
		    			list<Condition__c> lstDocCond = new list<Condition__c>();
		    			//template doc conditions
		    			for(Condition__c DocCond:Database.query(DocCondQuery)){
		    				lstDocCond.add(DocCond);
		    			}
		    			objWrap.lstSRDocConditions = lstDocCond;
	    			}
	    			
	    			if(setSRStepIds!=null && setSRStepIds.size()>0){
	    				set<id> setBRID = new set<id>();
		    			string BRQuery = getAccessibleFieldsSoql('Business_Rule__c','');
	    				BRQuery += ' where SR_Steps__c!=null and SR_Steps__c IN:setSRStepIds order by SR_Steps__r.Step_No__c';
		    			// all the nusiness rules 
		    			list<Business_Rule__c> LstBRs = new list<Business_Rule__c>();
		    			for(Business_Rule__c br:Database.query(BRQuery)){
		    				LstBRs.add(br);
		    				setBRID.add(br.Id);
		    			}
		    			objWrap.lstBR = LstBRs;
		    			
		    			string StepTransQuery = getAccessibleFieldsSoql('Step_Transition__c','');
	    				StepTransQuery += ' where SR_Step__c!=null and SR_Step__c IN:setSRStepIds';
		    			list<Step_Transition__c> lstTrans = new list<Step_Transition__c>();
		    			for(Step_Transition__c br:Database.query(StepTransQuery)){
		    				lstTrans.add(br);
		    			}
		    			objWrap.lstStepTransitions = lstTrans;
		    			
		    			if(setBRID!=null && setBRID.size()>0){
		    				string BRCondQuery = getAccessibleFieldsSoql('Condition__c','');
		    				BRCondQuery += ' where Business_Rule__c!=null and Business_Rule__c IN:setBRID';
			    			list<Condition__c> lstBRCond = new list<Condition__c>();
			    			for(Condition__c brCon:Database.query(BRCondQuery)){
			    				lstBRCond.add(brCon);
			    			}
			    			objWrap.lstBRConditions = lstBRCond;
			    			
			    			string BRActnQuery = getAccessibleFieldsSoql('Action__c','');
		    				BRActnQuery += ' where Business_Rule__c IN:setBRID';
			    			list<Action__c> lstBRActns = new list<Action__c>();
			    			for(Action__c brActn:Database.query(BRActnQuery)){
			    				lstBRActns.add(brActn);
			    			}
			    			objWrap.lstBRActions = lstBRActns;
		    			}
	    			}
	    			
    			}
    		}
    		system.debug('objWrap==>'+objWrap);
    	}
    	return objWrap;
    }
    global class SRProcessWrap{
    	public SR_Template__c SRtemplate;
    	public list<SR_Steps__c> lstSRSteps;
    	public list<SR_Template_Item__c> lstSRTemplateItems;
    	public list<SR_Template_Docs__c> lstSRTemplDocs;
    	public list<Business_Rule__c> lstBR;
    	public list<Step_Transition__c> lstStepTransitions;
    	public list<Condition__c> lstBRConditions;
    	public list<Action__c> lstBRActions;
    	public list<Condition__c> lstSRDocConditions;
    	public list<Condition__c> lstPricingConditions;
    	public list<Dated_Pricing__c> lstDatedPricing;
    	public list<Pricing_Range__c> lstPricingRange;
    	
    	public SRProcessWrap(){
    		SRtemplate = new SR_Template__c();
    		lstSRSteps = new list<SR_Steps__c>();
    		lstSRTemplateItems = new list<SR_Template_Item__c>();
    		lstSRTemplDocs = new list<SR_Template_Docs__c>();
    		lstBR = new list<Business_Rule__c>();
    		lstStepTransitions = new list<Step_Transition__c>();
    		lstBRConditions = new list<Condition__c>();
    		lstBRActions = new list<Action__c>();
    		lstSRDocConditions = new list<Condition__c>();
    		lstPricingConditions = new list<Condition__c>();
    		lstDatedPricing = new list<Dated_Pricing__c>();
    		lstPricingRange = new list<Pricing_Range__c>();
    	}
    }
    global static String getAccessibleFieldsSoql(String obj, String whereClause){
      String sql = '';
      String fieldString = '';
      list<String> fieldList = new list<string>();
      MAP<String,Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(obj.toLowerCase()).getDescribe().fields.getMap();
      if (fieldMap != null){
         for (Schema.SObjectField f : fieldMap.values()){
            Schema.DescribeFieldResult fd = f.getDescribe();
            if (fd.isAccessible() && fd.isCustom()){
               fieldList.add(fd.getName());
            }
         }
      }
      // Sort and assemble field list.
      if (!fieldList.isEmpty()){
         fieldList.sort();
         for (string s : fieldList){
            fieldString += s + ', ';
         }
         fieldString += 'Name,Id, ';
         if(obj=='Business_Rule__c'){
         	fieldString += 'SR_Steps__r.Step_No__c, ';
         }
      }
      // Strip terminal comma.
      if (fieldString.endsWith(', ')) 
         fieldString = fieldString.substring(0, fieldString.lastIndexOf(','));
      // Assemble SQL statement.
      sql = 'SELECT ' + fieldString + ' FROM ' + obj;
      // Append WHERE clause if present; if ORDER BY or LIMIT are needed,
      // append them to WHERE clause when calling this method.
      if (whereClause != null && whereClause != '') sql += ' WHERE ' + whereClause;
      return sql;
   }
}