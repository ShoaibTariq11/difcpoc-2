/**
 *Created By Mudasir for clsDraftSRDeleteBatch class
 */
@isTest
private class Test_clsDraftSRDeleteBatch {

    static testMethod void myUnitTest() {
	//SELECT ID, Name, Internal_Status_Name__c, External_Status_Name__c,SR_Template__r.Delete_Draft_Service_Requests__c FROM Service_Request__c 
	//WHERE SR_Group__c = \'Fit-Out & Events\' AND SR_Template__r.Delete_Draft_Service_Requests__c = true AND  External_Status_Name__c = \'Draft\' AND createdDate < LAST_N_DAYS:30 ORDER BY Name asc';        
     Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@dmcc.com';
        objContact.Phone = '1234567890';
        insert objContact;
      
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'General Letter in Arabic';
        objTemplate.SR_RecordType_API_Name__c = 'General_Letter_in_Arabic';
        objTemplate.ORA_Process_Identification__c = 'REGISTRATION';
        objTemplate.ORA_SR_Type__c = 'DMCC Registration Letters';
        objTemplate.Menutext__c = 'Arabic Letter to Third Party (New)';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Delete_Draft_Service_Requests__c = true;
        objTemplate.Active__c = true;
        insert objTemplate;
        //objTemplate.Code__c = 'Tax_Exemption_Letter';
        SR_Status__c srStatus = new SR_Status__c(Name = 'Draft',Code__c = 'DRAFT');
        
        insert srStatus;
        
    	Service_Request__c serv = new Service_Request__c(SR_Group__c='Fit-Out & Events',SR_Template__c=objTemplate.id ,External_SR_Status__c=srStatus.id );
    	insert serv;
    	Test.startTest();
	    	clsDraftSRDeleteBatch abs= new clsDraftSRDeleteBatch();
	    	String cronExpr = '0 0 0 15 3 ? 2022';
			String jobId = System.schedule('myJobTestJobName', cronExpr, abs);
        Test.stopTest();
    }
}