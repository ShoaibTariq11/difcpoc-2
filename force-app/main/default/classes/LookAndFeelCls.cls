/**************************************************************************************************************
Author      : Mudasir
Date        : May 2018
ClassName   : LookAndFeelCls
Description :
    -This class is used for Fit-Out Process 
    -This Class is used to display the units associated with the client or contractor for the look and feel process.

V1.1	03-Dec-2018 Mudasir		  Mudasir added the code to stabilize the look and feel process of showing the units.Assembla #5608   
****************************************************************************************************************/
public class LookAndFeelCls {
    private ApexPages.StandardController controller; 
   	public Service_Request__c serviceRequest;
   	public List<Lease_Partner__c> leasePartners{get;set;}
   	public String amendmentRecordId{get;set;}
   	public boolean showAddButton{get;set;}
    public List<Amendment__c> amendmentRecordList{get;set;}
    public string contractorProfileid{get;set;}
    public string reloadParent{get;set;}
    public final string approvingAuthority = Label.approvingAuthorityLnF;
    public string srId{get;set;}
    public LookAndFeelCls(ApexPages.StandardController controller){
    	srId = controller.getId();
    	contractorProfileid = System.Label.Contractor_Profile_Id;
    	this.controller = controller;
    	serviceRequest = [Select id,name,customer__c from Service_Request__c where id=:controller.getId()];
    	getAllLeasePartners(serviceRequest);
    	setAddButtonVisibility(serviceRequest);
    }
    
    private void getAllLeasePartners(Service_request__c servReq){
    	String profileId = UserInfo.getProfileId();
    	if(profileId.contains(contractorProfileid)){
    		fetchAllLeasePartnersOFContractor();
    	}else{
			leasePartners = [Select id,name,Account__c,Lease__c,Unit__c,Is_Valid_Operating_Location__c,SAP_Lease_Number__c,Type_of_Lease__c,Lease_Types__c from Lease_Partner__c where Account__c =:servReq.Customer__c limit 10];    		
    	}
    }
    public void addAmendments(){
    	//Move all the hardcoded strings to the labels- Pending action
    	//Get the record type of 'Fit - Out Units' for amendments
    	Id fitOutUnitsRecordTypeId = [Select id from RecordType where SObjectType ='Amendment__c' and DeveloperName='Fit_Out_Units'].id;
    	amendmentRecordId = System.currentPageReference().getParameters().get('leasePartnerId'); 
    	Amendment__c amend = new Amendment__c(recordTypeId=fitOutUnitsRecordTypeId,Lease_Partner__c =amendmentRecordId,ServiceRequest__c=serviceRequest.id);
    	insert amend;
    	try{
    		Lease_Partner__c leasePartner = [Select id,name,Lease_Types__c,Unit__c,Unit__r.Unit_Type_DDP__c from Lease_Partner__c where id=:amendmentRecordId];
    		if(leasePartner != Null && leasePartner.Unit__c != Null && leasePartner.Unit__r.Unit_Type_DDP__c != Null ){
    			if(leasePartner.Unit__r.Unit_Type_DDP__c =='Commercial'){serviceRequest.Approving_Authority__c = 'FOSP';}else {serviceRequest.Approving_Authority__c =approvingAuthority;}
    		}else{//In case the unit does not belong to any type
    			serviceRequest.Approving_Authority__c = approvingAuthority;}
    		String profileId = UserInfo.getProfileId(); if(!profileId.contains(contractorProfileid)){serviceRequest.Type_of_Request__c = 'Look & Feel not related to any fit out projects';serviceRequest.Tenant_Type_RORP__c = 'Client';}else{serviceRequest.Tenant_Type_RORP__c = 'Customer';}
    		update serviceRequest;
    		//reloadParent = '<script>  reloadParent(); </script> ';
    	}catch(Exception ex){System.debug(ex.getMessage());}
        setAddButtonVisibility(serviceRequest);
    }
    @TestVisible
    private void setAddButtonVisibility(Service_request__c servReq){
    	amendmentRecordList = [Select recordTypeId,Lease_Partner__c,ServiceRequest__c,Name,Unit_Usage_Type__c,Building__c from Amendment__c where recordType.Name='Fit - Out Units' and ServiceRequest__c=:serviceRequest.id];
        showAddButton =  amendmentRecordList.size() > 0 ? false  : true;
    }
    @TestVisible
    private void fetchAllLeasePartnersOFContractor(){
    	try{
	    	User userdetails = [Select id,name,email,Contact.Passport_No__c,AccountId,Service_Request_Id__c from User where id=:Userinfo.getUserId()];
	    	//Get requests whose record type is Request_Contractor_Access and then fetch the related Fit - Out Units
	    	//Based on the user passpot and email we will fetch the related requests and accordingly we will fetch the lease partners (Units) 
	    	Map<id,Service_Request__c> serviceReqMap; 
	    	//V1.1- Start  Service_Request_Id__c is introduced which is direct reference of the contractor portal access request.
	    	if(userdetails.Service_Request_Id__c != Null){
	    		serviceReqMap = new Map<id,Service_Request__c>([Select id,name,(Select id,Lease_Partner__c from Amendments__r) from service_Request__c where id=:userdetails.Service_Request_Id__c]);
	    	}else{
	    		serviceReqMap = new Map<id,Service_Request__c>([Select id,name,(Select id,Lease_Partner__c from Amendments__r) from service_Request__c where recordType.developername='Request_Contractor_Access' and Passport_Number__c=:userdetails.Contact.Passport_No__c and Email_Address__c=:userdetails.email]);
	    	}
	    	//V1.1 end 
	    	Set<id> leasePartnerIds = new Set<id>();
	    	for(Service_Request__c serv : serviceReqMap.values()){
	    		for(Amendment__c amend : serv.Amendments__r){
		    		leasePartnerIds.add(amend.Lease_Partner__c);
		    	}
	    	}
	    	if(leasePartnerIds!= null && leasePartnerIds.size()> 0) leasePartners = [Select id,name,Account__c,Lease__c,Unit__c,Is_Valid_Operating_Location__c,SAP_Lease_Number__c,Type_of_Lease__c,Lease_Types__c from Lease_Partner__c where id in :leasePartnerIds AND Account__c =:serviceRequest.customer__c];
    	}catch(Exception ex){System.debug(ex.getMessage());}   	
    	
    }
}