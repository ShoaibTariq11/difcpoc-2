/******************************************************************************************
 *  Class Name  : TranslatorJSON
 *  Author      : Sai kalyan Sanisetty 
 *  Company     : DIFC
 *  Date        : 18 NOV 2019        
 *  Description :                
 ----------------------------------------------------------------------
   Version     Date              Author                Remarks                                                 
  =======   ==========        =============    ==================================
    v1.1    18 NOV 2019         Sai                Initial Version  
*******************************************************************************************/

public class TranslatorJSON{

    public Integer code;
    public String lang;
    public List<String> text;

    
    public static TranslatorJSON parse(String json) {
        return (TranslatorJSON) System.JSON.deserialize(json, TranslatorJSON.class);
    }
}