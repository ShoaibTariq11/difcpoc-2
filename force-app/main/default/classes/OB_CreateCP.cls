/******************************************************************************************
 *  Author      : Durga Prasad
 *  Date        : 03-April-2020
 *  Description : Controller to create Commercial Permission Application - invoked from
                  process builder : Create draft SR for lead conversion and contact creation
*********************************************************************************************/
public without sharing class OB_CreateCP 
{
	
	
	@InvocableMethod
	public static void CreateCommercialPermission(list<Contact> lstContact)
	{
    	
    	list<HexaBPM__Service_Request__c> lstCPSR = new list<HexaBPM__Service_Request__c>();
    	string CPRecordTypeId;
    	for(RecordType RT:[Select Id from RecordType where DeveloperName='Commercial_Permission' and IsActive=true and sobjectType='HexaBPM__Service_Request__c']){
    		CPRecordTypeId = RT.Id;
    	}
		if(CPRecordTypeId!=null && lstContact!=null && lstContact.size()>0)
		{
    		set<string> setContactIds = new set<string>();
    		for(Contact contact:lstContact){
    			setContactIds.add(contact.Id);
    		}
				map<string,Contact> MapFullContact = new map<string,Contact>();
				set<id> relAccountId = new set<id>();
			for(Contact con:[SELECT Id,FirstName,LastName,Email,AccountId,Account.Phone,Account.Name 
							   FROM Contact 
							  WHERE Id IN:setContactIds]){
					MapFullContact.put(con.Id,con);
					relAccountId.add(con.AccountId);
				}
				
				map<string,string> MapOpportunities = new map<string,string>();//v1.1
				for(Opportunity Opp:[Select Id,AccountId from Opportunity where AccountId IN: relAccountId ]){//v1.1
					MapOpportunities.put(Opp.AccountId,Opp.Id);
				}

			for(Contact objcon:MapFullContact.values())
			{
				if(MapFullContact.get(objcon.Id)!=null)
				{
		    		HexaBPM__Service_Request__c objSR = new HexaBPM__Service_Request__c();
		    		objSR.RecordTypeId = CPRecordTypeId;
						objSR.HexaBPM__Customer__c = objcon.AccountId;
						objSR.HexaBPM__Contact__c = objcon.id;
						objSR.Opportunity__c = MapOpportunities.get(objcon.AccountId);
		    		objSR.First_Name__c = objcon.FirstName;
		    		objSR.Last_Name__c = objcon.LastName;
		    		objSR.HexaBPM__Email__c = objcon.Email;
		    		objSR.Entity_Name__c = MapFullContact.get(objcon.Id).Account.Name;
		    		objSR.HexaBPM__Send_SMS_to_Mobile__c = MapFullContact.get(objcon.Id).Account.Phone;
		    		lstCPSR.add(objSR);
	    		}
	    	}
	    	if(lstCPSR.size()>0){
	    		try{
	    			insert lstCPSR;
	    		}catch(Exception e){
	                Log__c objlog = new Log__c(Description__c = 'Exception on OB_CreateCP.CreateCommercialPermission() - Line:34 :'+e.getMessage(),Type__c = 'CP Creation Invocable Method');
	                insert objlog;
	    		}
	    	}
    	}else{
    		Log__c objlog = new Log__c(Description__c = 'Exception on OB_CreateCP.CreateCommercialPermission() - Line:15 : No RecordType found : Commercial_Permission',Type__c = 'CP Creation Invocable Method');
            insert objlog;
    	}
    	
	}
	


	// Map<Id,Id>
	
	public static void CreateCP(  Map<Id,account>  mapConIdAccId )
	{
    	
			list<HexaBPM__Service_Request__c> lstCPSR = new list<HexaBPM__Service_Request__c>();
			set<string> setAccountId = new set<string>();
    	string CPRecordTypeId;
		for(RecordType RT:[SELECT Id 
							 FROM RecordType 
							WHERE DeveloperName='Commercial_Permission' 
							  AND IsActive=true 
							  AND sobjectType='HexaBPM__Service_Request__c'])
		{
    		CPRecordTypeId = RT.Id;
		}
		
		if(CPRecordTypeId!=null)
		{
			
			//lstContact
    		map<string,Contact> MapFullContact = new map<string,Contact>();
			for(Contact con :   [SELECT Id,
										FirstName,
										LastName,
										Email,
										AccountId,
										Account.Phone,
										Account.Name 
			        			   FROM Contact 
								  WHERE Id IN: mapConIdAccId.keySet() ]
				)
			{
					MapFullContact.put(con.Id,con);
					setAccountId.add(mapConIdAccId.get(con.id).id);
			}
			
			map<string,string> MapOpportunities = new map<string,string>();//v1.1
			for(Opportunity Opp:[Select Id,AccountId from Opportunity where AccountId IN: setAccountId ]){//v1.1
				MapOpportunities.put(Opp.AccountId,Opp.Id);
			}

			for(Contact objcon : MapFullContact.values())
			{
				if(MapFullContact.get(objcon.Id)!=null)
				{
					account accountObj = mapConIdAccId.get(objcon.id);

		    		HexaBPM__Service_Request__c objSR = new HexaBPM__Service_Request__c();
		    		objSR.RecordTypeId = CPRecordTypeId;
						objSR.HexaBPM__Customer__c = accountObj.Id;
						objSR.HexaBPM__Contact__c = objcon.id;
						objSR.Opportunity__c = MapOpportunities.get(accountObj.Id);
		    		objSR.First_Name__c = objcon.FirstName;
		    		objSR.Last_Name__c = objcon.LastName;
		    		objSR.HexaBPM__Email__c = objcon.Email;
		    		objSR.Entity_Name__c = accountObj.Name;
		    		objSR.HexaBPM__Send_SMS_to_Mobile__c = accountObj.Phone;
		    		lstCPSR.add(objSR);
	    		}
			}
			
			if(lstCPSR.size() > 0)
			{
				try
				{
	    			insert lstCPSR;
				}
				catch(Exception e)
				{
	                Log__c objlog = new Log__c(Description__c = 'Exception on OB_CreateCP.CreateCommercialPermission() - Line:34 :'+e.getMessage(),Type__c = 'CP Creation Invocable Method');
	                insert objlog;
	    		}
	    	}
		}
		else
		{
    		Log__c objlog = new Log__c(Description__c = 'Exception on OB_CreateCP.CreateCommercialPermission() - Line:15 : No RecordType found : Commercial_Permission',Type__c = 'CP Creation Invocable Method');
            insert objlog;
    	}
    	
	}



}