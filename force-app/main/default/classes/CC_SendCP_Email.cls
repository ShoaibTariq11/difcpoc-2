/**************************************************************************************************
* Name               : CC_SendCP_Email                                                               
* Description        : CC Code to send email for Commercial Permission.                                        
* Created Date       : 18th March 2020                                                                *
* Created By         : Leeba Shibu (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version    Author    Date           Comment                                                                *
  1          prateek   4 april        added a cc list to the mail
**************************************************************************************************/

global without sharing class CC_SendCP_Email implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
         if(stp!=null && stp.HexaBPM__SR__c!=null && stp.HexaBPM__SR__r.HexaBPM__Email__c!=null){
            
            Contact tempContact = new Contact(email = stp.HexaBPM__SR__r.HexaBPM__Email__c, firstName = 'test', lastName = 'email',accountID=stp.HexaBPM__SR__r.HexaBPM__Customer__c );
            insert tempContact;
            
            list<Messaging.SingleEmailMessage> lstCPApprovalEmail = new list<Messaging.SingleEmailMessage>();
            
            String[] ToAddresses = new String[]{stp.HexaBPM__SR__r.HexaBPM__Email__c};
            String[] ccList = new String[]{stp.HexaBPM__SR__r.Account_Owner_Email__c};
            system.debug('ccList' + ccList);

            OrgWideEmailAddress[] lstOrgEA = [select Id from OrgWideEmailAddress where Address = 'noreply.portal@difc.ae'];
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId(tempContact.Id);
            mail.setToAddresses(ToAddresses);
            mail.setCCAddresses( ccList);
            mail.setSaveAsActivity(false);

            if(lstOrgEA!=null && lstOrgEA.size()>0)
            mail.setOrgWideEmailAddressId(lstOrgEA[0].Id);
            mail.setWhatId(stp.HexaBPM__SR__c);

            if(stp.HexaBPM__SR__r.HexaBPM__Record_Type_Name__c == 'Commercial_Permission_Renewal'){
                for(EmailTemplate temp:[Select Id from EmailTemplate where DeveloperName='OB_Send_Commercial_Permission_Renewal_Document_to_Customer']){
                    mail.setTemplateId(temp.Id);
                    system.debug('$$$$$$$Template' + temp.Id);
                }
            }else  if(stp.HexaBPM__SR__r.HexaBPM__Record_Type_Name__c == 'Commercial_Permission' ){
                for(EmailTemplate temp:[Select Id from EmailTemplate where DeveloperName='OB_Send_Commercial_Permission_Document_to_Customer']){
                    mail.setTemplateId(temp.Id);
                    system.debug('$$$$$$$Template' + temp.Id);
                }
            }
            
            
            
            list<Messaging.EmailFileAttachment> lstEmailAttachments = new list<Messaging.EmailFileAttachment>();
            string AttachmentId;
            list<string> AttachmentIdList = new list<string>();
            string SRDocId;
            list<string> dualFiles = new list<string>{'Dual_Licensing_Letter','Commercial_Permission_Dual_License'};
            if(stp.HexaBPM__SR__r.Type_of_commercial_permission__c  =='Dual license of DED licensed firms with an affiliate in DIFC'){
                for(HexaBPM__SR_Doc__c SRDoc:[Select Id,name,HexaBPM__Doc_ID__c from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:stp.HexaBPM__SR__c and HexaBPM__Document_Master__r.HexaBPM__Code__c IN:dualFiles ]){
                    if(SRDoc.HexaBPM__Doc_ID__c != null){
                        AttachmentIdList.add(SRDoc.HexaBPM__Doc_ID__c);
                    }else{
                        strResult = SRDoc.Name +' is yet to be genarated please try after few miniutes'; 
                        return strResult;
                    }
                    
                }
            }else{
                for(HexaBPM__SR_Doc__c SRDoc:[Select Id,name,HexaBPM__Doc_ID__c from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:stp.HexaBPM__SR__c and HexaBPM__Document_Master__r.HexaBPM__Code__c='Commercial_Permission' ]){
                    if(SRDoc.HexaBPM__Doc_ID__c != null){
                        AttachmentId = SRDoc.HexaBPM__Doc_ID__c;
                        SRDocId = SRDoc.Id;
                        AttachmentIdList.add(SRDoc.HexaBPM__Doc_ID__c);
                    }else{
                        strResult = SRDoc.Name +' is yet to be genarated please try after few miniutes';  
                        return strResult;
                    }
                    
                }
            }
            system.debug('$$$$AttachList' + AttachmentIdList);
            if(AttachmentIdList.size() > 0){
                /* for(Attachment attach : [Select Id,Name,Body,ContentType from Attachment where Id=:AttachmentIdList]){
                    Messaging.EmailFileAttachment MailAtt = new Messaging.EmailFileAttachment();
                    MailAtt.setFileName(attach.Name);
                    MailAtt.setBody(attach.Body);
                    MailAtt.setContentType(attach.contentType);
                    lstEmailAttachments.add(MailAtt);
                } */
                for(ContentVersion cv : [Select Id,PathOnClient,VersionData,Title,Application_Document__c from ContentVersion 
                                            where ContentDocumentId IN : AttachmentIdList order by CreatedDate desc]){
                    Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                    efa.setFileName(cv.PathOnClient);
                    efa.setBody(cv.VersionData);
                    lstEmailAttachments.add(efa);
                }
                system.debug('$$$$AttachListCv' + lstEmailAttachments);
                if(lstEmailAttachments.size()>0){
                    mail.setFileAttachments(lstEmailAttachments);
                }
                    
            }
            lstCPApprovalEmail.add(mail);
            system.debug('lstCPApprovalEmail==>'+lstCPApprovalEmail);
            try{
                Messaging.SendEmailResult[] results = Messaging.sendEmail(lstCPApprovalEmail);

                if (results[0].success) {
                System.debug('The email was sent successfully.');
                } else {
                System.debug('The email failed to send: ' +  results[0].errors[0].message);
                }
                delete tempContact;
            }catch(Exception e){
                strResult = e.getMessage()+'';
            }
        }
        return strResult;
    }
    
}