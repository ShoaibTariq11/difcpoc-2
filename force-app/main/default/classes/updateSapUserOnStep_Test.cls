/**
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class updateSapUserOnStep_Test {
	 private static testMethod void postToSapMethod(){
        SR_Template__c testSrTemplate = new SR_Template__c();    
        testSrTemplate.Name = 'DIFC Sponsorship Visa-New';
        testSrTemplate.Menutext__c = 'New Employment Visa Package';
        testSrTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_New';
        testSrTemplate.SR_Group__c = 'Fit-Out & Events';
        testSrTemplate.Active__c = true;        
        insert testSrTemplate;
        
        list<Status__c> lstStatus = new list<Status__c>();
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Awaiting Approval';
        objStatus.Type__c = 'Start';
        objStatus.Code__c = 'AWAITING_APPROVAL';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Approved';
        objStatus.Type__c = 'Intermediate';
        objStatus.Code__c = 'APPROVED';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Rejected';
        objStatus.Type__c = 'End';
        objStatus.Code__c = 'REJECTED';
        lstStatus.add(objStatus);
        
        insert lstStatus;
        
        Step_Template__c objStepType = new Step_Template__c();
        objStepType.Name = 'Approve Request';
        objStepType.Code__c = 'APPROVE_REQUEST';
        objStepType.Step_RecordType_API_Name__c = 'General';
        objStepType.Summary__c = 'Approve Request';
        insert objStepType;
		
		SR_Steps__c objSRStep = new SR_Steps__c();
        objSRStep.SR_Template__c = testSrTemplate.Id;
        objSRStep.Step_RecordType_API_Name__c = 'General';
        objSRStep.Step_Template__c = objStepType.Id; // Step Type
        objSRStep.Summary__c = 'Approve Request';
        objSRStep.Step_No__c = 1.0;
        objSRStep.Start_Status__c = lstStatus[0].Id; // Default Step Status
	    insert objSRStep;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Service_Request__c SR1= new Service_Request__c();
        //SR1.RecordTypeId = mapRecordType.get('Fit_Out_Induction_Request');
        SR1.Customer__c = objAccount.Id;
        //SR.Service_Category__c = 'New';    
        insert SR1;
        Step__c objStep = new Step__c();
        objStep.SR__c = SR1.Id;
        objStep.Step_No__c = 1.0;
        objStep.step_notes__c = 'abc';
        objStep.SR_Step__c=objSRStep.id;
        objStep.OwnerId= '00G20000002XcD0';
        objStep.SAP_AENAM__c = 'Mudasir';
        objStep.SAP_ERNAM__c = 'Mudasir';
        objStep.SAP_ERDAT__c = '20191010';
        objStep.SAP_ERTIM__c = '101010';
        objStep.SAP_AEDAT__c = '20191010';
        objStep.SAP_AETIM__c = '101010';
        objStep.step_Template__c = objStepType.id;
        insert objStep;
        
        User u = new User(
		     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
		     LastName = 'last',
		     Email = 'test@difc.ae',
		     Username = 'test@difcusername.com' + System.currentTimeMillis(),
		     CompanyName = 'TEST',
		     Title = 'title',
		     Alias = 'alias',
		     TimeZoneSidKey = 'America/Los_Angeles',
		     EmailEncodingKey = 'UTF-8',
		     LanguageLocaleKey = 'en_US',
		     LocaleSidKey = 'en_US',
		     SAP_User_Id__c ='Mudasir'
		);
   		insert u;
        test.startTest();
         updateSapUserOnStep.updateSapUserDetailsOnStep(new List<Id>{objStep.Id});
        test.stopTest();
     }
    private static testMethod void postToSapMethodTwo(){
        SR_Template__c testSrTemplate = new SR_Template__c();    
        testSrTemplate.Name = 'DIFC Sponsorship Visa-New';
        testSrTemplate.Menutext__c = 'New Employment Visa Package';
        testSrTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_New';
        testSrTemplate.SR_Group__c = 'Fit-Out & Events';
        testSrTemplate.Active__c = true;        
        insert testSrTemplate;
        
        list<Status__c> lstStatus = new list<Status__c>();
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Awaiting Approval';
        objStatus.Type__c = 'Start';
        objStatus.Code__c = 'AWAITING_APPROVAL';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Approved';
        objStatus.Type__c = 'Intermediate';
        objStatus.Code__c = 'APPROVED';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Rejected';
        objStatus.Type__c = 'End';
        objStatus.Code__c = 'REJECTED';
        lstStatus.add(objStatus);
        
        insert lstStatus;
        
        Step_Template__c objStepType = new Step_Template__c();
        objStepType.Name = 'Approve Request';
        objStepType.Code__c = 'APPROVE_REQUEST';
        objStepType.Step_RecordType_API_Name__c = 'General';
        objStepType.Summary__c = 'Approve Request';
        insert objStepType;
		
		SR_Steps__c objSRStep = new SR_Steps__c();
        objSRStep.SR_Template__c = testSrTemplate.Id;
        objSRStep.Step_RecordType_API_Name__c = 'General';
        objSRStep.Step_Template__c = objStepType.Id; // Step Type
        objSRStep.Summary__c = 'Approve Request';
        objSRStep.Step_No__c = 1.0;
        objSRStep.Start_Status__c = lstStatus[0].Id; // Default Step Status
	    insert objSRStep;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Service_Request__c SR1= new Service_Request__c();
        //SR1.RecordTypeId = mapRecordType.get('Fit_Out_Induction_Request');
        SR1.Customer__c = objAccount.Id;
        //SR.Service_Category__c = 'New';    
        insert SR1;
        Step__c objStep = new Step__c();
        objStep.SR__c = SR1.Id;
        objStep.Step_No__c = 1.0;
        objStep.step_notes__c = 'abc';
        objStep.SR_Step__c=objSRStep.id;
        objStep.OwnerId= '00G20000002XcD0';
        objStep.SAP_AENAM__c = '';
        objStep.SAP_ERNAM__c = '';
        objStep.SAP_ERDAT__c = '';
        objStep.SAP_ERTIM__c = '';
        objStep.SAP_AEDAT__c = '';
        objStep.SAP_AETIM__c = '';
        objStep.step_Template__c = objStepType.id;
        insert objStep;
        
        User u = new User(
		     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
		     LastName = 'last',
		     Email = 'test@difc.ae',
		     Username = 'test@difcusername.com' + System.currentTimeMillis(),
		     CompanyName = 'TEST',
		     Title = 'title',
		     Alias = 'alias',
		     TimeZoneSidKey = 'America/Los_Angeles',
		     EmailEncodingKey = 'UTF-8',
		     LanguageLocaleKey = 'en_US',
		     LocaleSidKey = 'en_US',
		     SAP_User_Id__c ='Mudasir'
		);
   		insert u;
        test.startTest();
         updateSapUserOnStep.updateSapUserDetailsOnStep(new List<Id>{objStep.Id});
        test.stopTest();
     }
     
     private static testMethod void postToSapMethodThree(){
        SR_Template__c testSrTemplate = new SR_Template__c();    
        testSrTemplate.Name = 'DIFC Sponsorship Visa-New';
        testSrTemplate.Menutext__c = 'New Employment Visa Package';
        testSrTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_New';
        testSrTemplate.SR_Group__c = 'Fit-Out & Events';
        testSrTemplate.Active__c = true;        
        insert testSrTemplate;
        
        list<Status__c> lstStatus = new list<Status__c>();
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Awaiting Approval';
        objStatus.Type__c = 'Start';
        objStatus.Code__c = 'AWAITING_APPROVAL';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Approved';
        objStatus.Type__c = 'Intermediate';
        objStatus.Code__c = 'APPROVED';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Rejected';
        objStatus.Type__c = 'End';
        objStatus.Code__c = 'REJECTED';
        lstStatus.add(objStatus);
        
        insert lstStatus;
        
        Step_Template__c objStepType = new Step_Template__c();
        objStepType.Name = 'under process';
        objStepType.Code__c = 'under process';
        objStepType.Step_RecordType_API_Name__c = 'General';
        objStepType.Summary__c = 'under process';
        insert objStepType;
		
		SR_Steps__c objSRStep = new SR_Steps__c();
        objSRStep.SR_Template__c = testSrTemplate.Id;
        objSRStep.Step_RecordType_API_Name__c = 'General';
        objSRStep.Step_Template__c = objStepType.Id; // Step Type
        objSRStep.Summary__c = 'under process';
        objSRStep.Step_No__c = 1.0;
        objSRStep.Start_Status__c = lstStatus[0].Id; // Default Step Status
	    insert objSRStep;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Service_Request__c SR1= new Service_Request__c();
        //SR1.RecordTypeId = mapRecordType.get('Fit_Out_Induction_Request');
        SR1.Customer__c = objAccount.Id;
        //SR.Service_Category__c = 'New';    
        insert SR1;
        Step__c objStep = new Step__c();
        objStep.SR__c = SR1.Id;
        objStep.Step_No__c = 1.0;
        objStep.step_notes__c = 'abc';
        objStep.SR_Step__c=objSRStep.id;
        objStep.OwnerId= '00G20000002XcD0';
        objStep.SAP_AENAM__c = '';
        objStep.SAP_ERNAM__c = '';
        objStep.SAP_ERDAT__c = '';
        objStep.SAP_ERTIM__c = '';
        objStep.SAP_AEDAT__c = '';
        objStep.SAP_AETIM__c = '';
        objStep.step_Template__c = objStepType.id;
        insert objStep;
        system.debug('objStep'+[Select id,name,step_Name__c from step__c where id=:objStep.id]);
        User u = new User(
		     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
		     LastName = 'last',
		     Email = 'test@difc.ae',
		     Username = 'test@difcusername.com' + System.currentTimeMillis(),
		     CompanyName = 'TEST',
		     Title = 'title',
		     Alias = 'alias',
		     TimeZoneSidKey = 'America/Los_Angeles',
		     EmailEncodingKey = 'UTF-8',
		     LanguageLocaleKey = 'en_US',
		     LocaleSidKey = 'en_US',
		     SAP_User_Id__c ='MudasirWani'
		);
   		insert u;
        test.startTest();
         updateSapUserOnStep.updateSapUserDetailsOnStep(new List<Id>{objStep.Id});
        test.stopTest();
     }
}