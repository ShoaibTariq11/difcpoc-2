/*
    * Created By    : Ravi on 3rd March,2015
    * Description   : This is a schedule class which will run on daily basis and checks for the expired Document Detail record.
                      In case any document detail record expiry date is past then system will deactive the "Document Active" checkbox on relationship record    
    
    * Modification History :
        # 3/3/2015 - Ravi Created.
	V.No	Date		Updated By    	Description
	--------------------------------------------------------------------------------------------------------------------------             
 	V1.0    20-03-2015	Ravi			Added the logic to add the Renewal SRs in Pipeline calulation
*/

global without sharing class ScheduleCheckDocDetails implements Schedulable {
    global void execute(SchedulableContext ctx) {
        list<Relationship__c> lstRels = new list<Relationship__c>();
        list<Id> lstAccountIds = new list<Id>();
        Date dt = system.today().addDays(-1);
        map<string,Boolean> mapContactIds = new map<string,Boolean>();
        list<Id> lstContactIds = new list<Id>();
        map<Id,Id> mapAccountIds = new map<Id,Id>();
        
        for(Document_Details__c objDD : [select Id,EXPIRY_DATE__c,Document_Type__c,Contact__c from Document_Details__c where Contact__r.RecordType.DeveloperName = 'GS_Contact' AND (Document_Type__c = 'Employment Visa' OR Document_Type__c = 'Seconded Access Card' OR Document_Type__c = 'Local / GCC Smart Card' OR Document_Type__c = 'Temporary Work Permit') AND EXPIRY_DATE__c =:dt]){
            mapContactIds.put(objDD.Contact__c+'-'+objDD.Document_Type__c,false);
            lstContactIds.add(objDD.Contact__c);
        }
        
        for(Relationship__c objRel : [select Id,Object_Contact__c,Subject_Account__c,Subject_Account__r.Hosting_Company__c,Relationship_Type__c  from Relationship__c where Object_Contact__r.RecordType.DeveloperName = 'GS_Contact' AND (Relationship_Type__c = 'Has DIFC Sponsored Employee' OR Relationship_Type__c = 'Has DIFC Seconded' OR Relationship_Type__c = 'Has DIFC Local /GCC Employees' OR Relationship_Type__c = 'Has Temporary Employee') AND Object_Contact__c IN : lstContactIds]){
            if(objRel.Relationship_Type__c == 'Has DIFC Sponsored Employee' && mapContactIds.containsKey(objRel.Object_Contact__c+'-Employment Visa')){
                lstRels.add(new Relationship__c(Id=objRel.Id,Document_Active__c=mapContactIds.get(objRel.Object_Contact__c+'-Employment Visa')));               
                lstAccountIds.add(objRel.Subject_Account__c);
                mapAccountIds.put(objRel.Subject_Account__c,objRel.Subject_Account__r.Hosting_Company__c);
            }else if(objRel.Relationship_Type__c == 'Has DIFC Seconded' && mapContactIds.containsKey(objRel.Object_Contact__c+'-Seconded Access Card')){
                lstRels.add(new Relationship__c(Id=objRel.Id,Document_Active__c=mapContactIds.get(objRel.Object_Contact__c+'-Seconded Access Card')));
                lstAccountIds.add(objRel.Subject_Account__c);
                mapAccountIds.put(objRel.Subject_Account__c,objRel.Subject_Account__r.Hosting_Company__c);
            }else if(objRel.Relationship_Type__c == 'Has DIFC Local /GCC Employees' && mapContactIds.containsKey(objRel.Object_Contact__c+'-Local / GCC Smart Card')){
                lstRels.add(new Relationship__c(Id=objRel.Id,Document_Active__c=mapContactIds.get(objRel.Object_Contact__c+'-Local / GCC Smart Card')));
                lstAccountIds.add(objRel.Subject_Account__c);
                mapAccountIds.put(objRel.Subject_Account__c,objRel.Subject_Account__r.Hosting_Company__c);
            }else if(objRel.Relationship_Type__c == 'Has Temporary Employee' && mapContactIds.containsKey(objRel.Object_Contact__c+'-Temporary Work Permit')){
                lstRels.add(new Relationship__c(Id=objRel.Id,Document_Active__c=mapContactIds.get(objRel.Object_Contact__c+'-Temporary Work Permit')));
                lstAccountIds.add(objRel.Subject_Account__c);
                mapAccountIds.put(objRel.Subject_Account__c,objRel.Subject_Account__r.Hosting_Company__c);
            }
        }
        if(!lstRels.isEmpty())
            update lstRels;
        
        if(!lstAccountIds.isEmpty()){
            map<Id,decimal[]> mapVisaInfo = new map<Id,decimal[]>();// Calculated Visa, Utilized Visa, Exception Viss
            map<Id,Boolean> mapLeaseInfo = new map<Id,Boolean>();
            map<Id,Lease__c> mapLeases = new map<Id,Lease__c>();
            map<Id,Account> mapAccounts = new map<Id,Account>();
            
            for(Lease__c objLease : [select Id,Visa_Factor__c,Square_Feet__c,Account__c from Lease__c where Account__c != null AND Account__c IN :lstAccountIds AND Status__c='Active' AND Lease_Types__c != 'Data Centre Lease']){
                decimal[] dVisaInfo = mapVisaInfo.containsKey(objLease.Account__c) ? mapVisaInfo.get(objLease.Account__c) : new decimal[]{0,0};
                dVisaInfo[0] += objLease.Visa_Factor__c != null ? objLease.Visa_Factor__c : 0;
                mapVisaInfo.put(objLease.Account__c,dVisaInfo);
                if(mapAccountIds.containsKey(objLease.Account__c))
                	mapAccountIds.remove(objLease.Account__c);
            }
            
            if(!mapAccountIds.isEmpty()){
            	map<Id,decimal[]> mapVisaInfoTemp = new map<Id,decimal[]>();
            	for(Lease__c objLease : [select Id,Visa_Factor__c,Lease_Types__c,Account__c from Lease__c where Account__c IN (select Hosting_Company__c from Account where Id IN : mapAccountIds.keySet() AND Hosting_Company__c != null) AND Status__c='Active' AND Lease_Types__c != 'Data Centre Lease']){
					decimal[] dVisaInfo = mapVisaInfoTemp.containsKey(objLease.Account__c) ? mapVisaInfoTemp.get(objLease.Account__c) : new decimal[]{0,0};
	                dVisaInfo[0] += objLease.Visa_Factor__c != null ? objLease.Visa_Factor__c : 0;
		            mapVisaInfoTemp.put(objLease.Account__c,dVisaInfo);
				}
				for(Id AccountId : mapAccountIds.keySet()){
					Id HostingCompId = mapAccountIds.get(AccountId);
					if(HostingCompId != null && mapVisaInfoTemp.containsKey(HostingCompId)){
						mapVisaInfo.put(AccountId,mapVisaInfoTemp.get(HostingCompId));
					}
				}
            }
            
            map<Id,Service_Request__c> mapOpenSRs = new map<Id,Service_Request__c>([select Id,External_Status_Name__c,Customer__c from Service_Request__c where Customer__c IN :lstAccountIds AND (Record_Type_Name__c='DIFC_Sponsorship_Visa_New' OR (Record_Type_Name__c='Company_Temporary_Work_Permit_Request_Letter' AND Service_Category__c = 'New Temporary Work Permit') OR Record_Type_Name__c='Employment_Visa_from_DIFC_to_DIFC' OR Record_Type_Name__c='Employment_Visa_Govt_to_DIFC' OR Record_Type_Name__c='Visa_from_Individual_sponsor_to_DIFC' OR (Record_Type_Name__c = 'Access_Card' AND (Service_Category__c ='New Seconded Employee Card' OR Service_Category__c = 'New Employee Card for UAE / GCC Nationals'))) AND isClosedStatus__c != true AND Is_Rejected__c != true AND External_Status_Name__c != 'Rejected' AND External_Status_Name__c != 'Cancelled' AND External_Status_Name__c != 'Draft']);
            //map<Id,Contact> mapActiveCons = new map<Id,Contact>([select Id from Contact where Id IN (select Object_Contact__c from Relationship__c where Subject_Account__c  IN :lstAccountIds AND Active__c = true AND Document_Active__c = true AND ( Relationship_Type__c='Has DIFC Sponsored Employee' OR Relationship_Type__c = 'Has DIFC Seconded' OR Relationship_Type__c = 'Has DIFC Local /GCC Employees' OR Relationship_Type__c = 'Has Temporary Employee')) AND RecordType.DeveloperName = 'GS_Contact']);
            //list<AggregateResult> lstActiveEmps = [select AccountId,count(Id) ActiveEmps from Contact where RecordType.DeveloperName = 'GS_Contact' AND (Id IN : mapActiveCons.keyset() OR SR__c IN : mapOpenSRs.keyset()) group by AccountId];
            
            /*list<AggregateResult> lstActiveEmps = [select AccountId,count(Id) ActiveEmps from Contact where RecordType.DeveloperName = 'GS_Contact' AND Id IN (select Object_Contact__c from Relationship__c where Subject_Account__c  IN :lstAccountIds AND Active__c = true AND ( Relationship_Type__c='Has DIFC Sponsored Employee' OR (Document_Active__c = true AND (Relationship_Type__c = 'Has DIFC Seconded' OR Relationship_Type__c = 'Has DIFC Local /GCC Employees' OR Relationship_Type__c = 'Has Temporary Employee')) )) group by AccountId];
            for(AggregateResult objCon : lstActiveEmps){
                string AccountId = objCon.get('AccountId') != null ? string.valueOf(objCon.get('AccountId')) : null;
                if(AccountId != null){
                    decimal[] dVisaInfo = mapVisaInfo.containsKey(AccountId) ? mapVisaInfo.get(AccountId) : new decimal[]{0,0};
                    dVisaInfo[1] += objCon.get('ActiveEmps') != null ? decimal.valueOf(string.valueOf(objCon.get('ActiveEmps'))) : 0;
                    mapVisaInfo.put(AccountId,dVisaInfo);
                }
            }*/
            
            //v1.0
            map<Id,Service_Request__c> mapOpenRenewSRs = new map<Id,Service_Request__c>([select Id,External_Status_Name__c,Contact__c,ApplicantId_hidden__c,Passport_Number__c,Nationality_list__c,Customer__c from Service_Request__c where Customer__c IN :lstAccountIds AND 
				((Record_Type_Name__c = 'Access_Card' AND (Service_Category__c ='Renewal Seconded Employee Card' OR Service_Category__c = 'Renewal Employee Card for UAE / GCC Nationals')) 
				OR (Record_Type_Name__c='Company_Temporary_Work_Permit_Request_Letter' AND Service_Category__c = 'Renewal Temporary Work Permit' ) )
				AND isClosedStatus__c != true AND Is_Rejected__c != true AND External_Status_Name__c != 'Rejected' AND External_Status_Name__c != 'Cancelled' AND External_Status_Name__c != 'Draft']);
            
            map<Id,Integer> mapActiveEmps = new map<Id,Integer>();
            map<string,string> mapKey = new map<string,string>();
			for(Relationship__c objRel : [select Subject_Account__c,Object_Contact__c from Relationship__c where Object_Contact__c != null AND Object_Contact__r.RecordType.DeveloperName = 'GS_Contact' AND Subject_Account__c IN :lstAccountIds AND Active__c = true AND ( Relationship_Type__c='Has DIFC Sponsored Employee' OR (Document_Active__c = true AND (Relationship_Type__c = 'Has DIFC Seconded' OR Relationship_Type__c = 'Has DIFC Local /GCC Employees' OR Relationship_Type__c = 'Has Temporary Employee')))]){
				if(mapKey.containsKey(objRel.Subject_Account__c+''+objRel.Object_Contact__c) == false){
					Integer i = mapActiveEmps.containsKey(objRel.Subject_Account__c) ? mapActiveEmps.get(objRel.Subject_Account__c) : 0;
					i++;
					mapActiveEmps.put(objRel.Subject_Account__c,i);
					mapKey.put(objRel.Subject_Account__c+''+objRel.Object_Contact__c,objRel.Subject_Account__c+''+objRel.Object_Contact__c);
				}
			}
            for(Id AccountId : mapActiveEmps.keySet()){
				decimal[] dVisaInfo = mapVisaInfo.containsKey(AccountId) ? mapVisaInfo.get(AccountId) : new decimal[]{0,0};
				dVisaInfo[1] += mapActiveEmps.get(AccountId);
				mapVisaInfo.put(AccountId,dVisaInfo);
			}
            if(!mapOpenSRs.isEmpty()){
                for(Service_Request__c SR : mapOpenSRs.values()){
                    //if(SR.External_Status_Name__c == 'Submitted'){
                        decimal[] dVisaInfo = mapVisaInfo.containsKey(SR.Customer__c) ? mapVisaInfo.get(SR.Customer__c) : new decimal[]{0,0};
                        dVisaInfo[1] += 1;
                        mapVisaInfo.put(SR.Customer__c,dVisaInfo);
                    //}
                }
            }
            //V1.0
            map<Id,set<string>> mapPassNations = new map<Id,set<string>>();
            if(mapOpenRenewSRs != null && !mapOpenRenewSRs.isEmpty()){
				list<Id> lstConIds = new list<Id>();
				list<string> lstPassNations = new list<string>();
				set<string> setPassNation = new set<string>();
				for(Service_Request__c objS : mapOpenRenewSRs.values()){
					if(objS.Contact__c != null){
						lstConIds.add(objS.Contact__c);
					}
					if(objS.Passport_Number__c != null && objS.Nationality_list__c != null){
						setPassNation = mapPassNations.containsKey(objS.Customer__c) ? mapPassNations.get(objS.Customer__c) : new set<string>() ;
						setPassNation.add(objS.Passport_Number__c+''+objS.Nationality_list__c);
						mapPassNations.put(objS.Customer__c,setPassNation);
					}
					
				}
				map<Id,Contact> mapActiveConRenews = new map<Id,Contact>([select Id,Nationality__c,Passport_No__c,(select Id,Relationship_Type__c,Subject_Account__c from Secondary_Contact__r where Subject_Account__c IN :lstAccountIds AND Active__c = true AND Document_Active__c = true AND (End_Date__c = null OR End_Date__c >= TODAY) AND (Relationship_Type__c = 'Has DIFC Seconded' OR Relationship_Type__c = 'Has DIFC Local /GCC Employees' OR Relationship_Type__c = 'Has Temporary Employee') ) from Contact where Id IN (select Object_Contact__c from Relationship__c where Subject_Account__c IN :lstAccountIds AND Active__c = true AND Document_Active__c = true AND End_Date__c >= Today AND (Relationship_Type__c = 'Has DIFC Seconded' OR Relationship_Type__c = 'Has DIFC Local /GCC Employees' OR Relationship_Type__c = 'Has Temporary Employee')) AND RecordType.DeveloperName = 'GS_Contact']);
				if(mapActiveConRenews != null && !mapActiveConRenews.isEmpty()){
					for(Contact objC : mapActiveConRenews.values()){
						/*if(setPassNation.contains(objC.Passport_No__c+''+objC.Nationality__c)){
							setPassNation.remove(objC.Passport_No__c+''+objC.Nationality__c);
						}*/
						if(objC.Secondary_Contact__r != null){
							setPassNation = new set<string>();
							for(Relationship__c objR : objC.Secondary_Contact__r){
								if(mapPassNations.containsKey(objR.Subject_Account__c)){
									setPassNation = mapPassNations.get(objR.Subject_Account__c) ;
									if(setPassNation.contains(objC.Passport_No__c+''+objC.Nationality__c)){
										setPassNation.remove(objC.Passport_No__c+''+objC.Nationality__c);
										mapPassNations.put(objR.Subject_Account__c,setPassNation);
									}
								}
							}
						}
					}
				}
				//if(!setPassNation.isEmpty())
					//dVisaInfo[3] += setPassNation.size();
			}//end of V1.0
            if(!mapVisaInfo.isEmpty()){
                for(Id AccountId : mapVisaInfo.keySet()){
                    Account objAccount = mapAccounts.containsKey(AccountId) ? mapAccounts.get(AccountId) : new Account(Id=AccountId);
                    objAccount.Calculated_Visa_Quota__c = mapVisaInfo.get(AccountId)[0];
                    objAccount.Utilize_Visa_Quota__c = mapVisaInfo.get(AccountId)[1];
                    //v1.0
                    if(mapPassNations != null && mapPassNations.containsKey(AccountId))
                    	objAccount.Utilize_Visa_Quota__c += mapPassNations.get(AccountId).size();
                    mapAccounts.put(objAccount.Id,objAccount);
                }
            }
            if(!mapAccounts.isEmpty()){
                try{
                    update mapAccounts.values();
                }catch(DMLException ex){
                    Log__c objLog = new Log__c();
                    objLog.Type__c = 'Schedule Visa Quota Update';
                    objLog.Description__c = ex.getMessage();
                    insert objLog;
                }   
            }   
        }
        
    }
}